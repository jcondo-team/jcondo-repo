package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.EnterpriseServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class EnterpriseServiceClpInvoker {
    private String _methodName110;
    private String[] _methodParameterTypes110;
    private String _methodName111;
    private String[] _methodParameterTypes111;
    private String _methodName116;
    private String[] _methodParameterTypes116;
    private String _methodName117;
    private String[] _methodParameterTypes117;
    private String _methodName118;
    private String[] _methodParameterTypes118;
    private String _methodName119;
    private String[] _methodParameterTypes119;
    private String _methodName120;
    private String[] _methodParameterTypes120;
    private String _methodName121;
    private String[] _methodParameterTypes121;

    public EnterpriseServiceClpInvoker() {
        _methodName110 = "getBeanIdentifier";

        _methodParameterTypes110 = new String[] {  };

        _methodName111 = "setBeanIdentifier";

        _methodParameterTypes111 = new String[] { "java.lang.String" };

        _methodName116 = "createEnterprise";

        _methodParameterTypes116 = new String[] {  };

        _methodName117 = "addEnterprise";

        _methodParameterTypes117 = new String[] {
                "java.lang.String", "java.lang.String",
                "br.com.atilo.jcondo.datatype.DomainType",
                "br.com.atilo.jcondo.datatype.EnterpriseStatus",
                "java.lang.String"
            };

        _methodName118 = "updateEnterprise";

        _methodParameterTypes118 = new String[] {
                "long", "java.lang.String", "java.lang.String",
                "br.com.atilo.jcondo.datatype.DomainType",
                "br.com.atilo.jcondo.datatype.EnterpriseStatus",
                "java.lang.String"
            };

        _methodName119 = "getEnterprise";

        _methodParameterTypes119 = new String[] { "long" };

        _methodName120 = "getEnterprisesByType";

        _methodParameterTypes120 = new String[] {
                "br.com.atilo.jcondo.datatype.DomainType"
            };

        _methodName121 = "getEnterpriseByIdentity";

        _methodParameterTypes121 = new String[] { "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName110.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
            return EnterpriseServiceUtil.getBeanIdentifier();
        }

        if (_methodName111.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
            EnterpriseServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName116.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes116, parameterTypes)) {
            return EnterpriseServiceUtil.createEnterprise();
        }

        if (_methodName117.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes117, parameterTypes)) {
            return EnterpriseServiceUtil.addEnterprise((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                (br.com.atilo.jcondo.datatype.DomainType) arguments[2],
                (br.com.atilo.jcondo.datatype.EnterpriseStatus) arguments[3],
                (java.lang.String) arguments[4]);
        }

        if (_methodName118.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes118, parameterTypes)) {
            return EnterpriseServiceUtil.updateEnterprise(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                (br.com.atilo.jcondo.datatype.DomainType) arguments[3],
                (br.com.atilo.jcondo.datatype.EnterpriseStatus) arguments[4],
                (java.lang.String) arguments[5]);
        }

        if (_methodName119.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes119, parameterTypes)) {
            return EnterpriseServiceUtil.getEnterprise(((Long) arguments[0]).longValue());
        }

        if (_methodName120.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes120, parameterTypes)) {
            return EnterpriseServiceUtil.getEnterprisesByType((br.com.atilo.jcondo.datatype.DomainType) arguments[0]);
        }

        if (_methodName121.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes121, parameterTypes)) {
            return EnterpriseServiceUtil.getEnterpriseByIdentity((java.lang.String) arguments[0]);
        }

        throw new UnsupportedOperationException();
    }
}
