package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Mail;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Mail in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Mail
 * @generated
 */
public class MailCacheModel implements CacheModel<Mail>, Externalizable {
    public long id;
    public long recipientId;
    public String recipientName;
    public long takerId;
    public String takerName;
    public int typeId;
    public String code;
    public long dateIn;
    public long dateOut;
    public String remark;
    public long signatureId;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(27);

        sb.append("{id=");
        sb.append(id);
        sb.append(", recipientId=");
        sb.append(recipientId);
        sb.append(", recipientName=");
        sb.append(recipientName);
        sb.append(", takerId=");
        sb.append(takerId);
        sb.append(", takerName=");
        sb.append(takerName);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", code=");
        sb.append(code);
        sb.append(", dateIn=");
        sb.append(dateIn);
        sb.append(", dateOut=");
        sb.append(dateOut);
        sb.append(", remark=");
        sb.append(remark);
        sb.append(", signatureId=");
        sb.append(signatureId);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Mail toEntityModel() {
        MailImpl mailImpl = new MailImpl();

        mailImpl.setId(id);
        mailImpl.setRecipientId(recipientId);

        if (recipientName == null) {
            mailImpl.setRecipientName(StringPool.BLANK);
        } else {
            mailImpl.setRecipientName(recipientName);
        }

        mailImpl.setTakerId(takerId);

        if (takerName == null) {
            mailImpl.setTakerName(StringPool.BLANK);
        } else {
            mailImpl.setTakerName(takerName);
        }

        mailImpl.setTypeId(typeId);

        if (code == null) {
            mailImpl.setCode(StringPool.BLANK);
        } else {
            mailImpl.setCode(code);
        }

        if (dateIn == Long.MIN_VALUE) {
            mailImpl.setDateIn(null);
        } else {
            mailImpl.setDateIn(new Date(dateIn));
        }

        if (dateOut == Long.MIN_VALUE) {
            mailImpl.setDateOut(null);
        } else {
            mailImpl.setDateOut(new Date(dateOut));
        }

        if (remark == null) {
            mailImpl.setRemark(StringPool.BLANK);
        } else {
            mailImpl.setRemark(remark);
        }

        mailImpl.setSignatureId(signatureId);

        if (oprDate == Long.MIN_VALUE) {
            mailImpl.setOprDate(null);
        } else {
            mailImpl.setOprDate(new Date(oprDate));
        }

        mailImpl.setOprUser(oprUser);

        mailImpl.resetOriginalValues();

        return mailImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        recipientId = objectInput.readLong();
        recipientName = objectInput.readUTF();
        takerId = objectInput.readLong();
        takerName = objectInput.readUTF();
        typeId = objectInput.readInt();
        code = objectInput.readUTF();
        dateIn = objectInput.readLong();
        dateOut = objectInput.readLong();
        remark = objectInput.readUTF();
        signatureId = objectInput.readLong();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(recipientId);

        if (recipientName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(recipientName);
        }

        objectOutput.writeLong(takerId);

        if (takerName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(takerName);
        }

        objectOutput.writeInt(typeId);

        if (code == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(code);
        }

        objectOutput.writeLong(dateIn);
        objectOutput.writeLong(dateOut);

        if (remark == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(remark);
        }

        objectOutput.writeLong(signatureId);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
