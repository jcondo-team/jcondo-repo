package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.TelephoneServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class TelephoneServiceClpInvoker {
    private String _methodName104;
    private String[] _methodParameterTypes104;
    private String _methodName105;
    private String[] _methodParameterTypes105;
    private String _methodName110;
    private String[] _methodParameterTypes110;
    private String _methodName111;
    private String[] _methodParameterTypes111;
    private String _methodName112;
    private String[] _methodParameterTypes112;
    private String _methodName113;
    private String[] _methodParameterTypes113;
    private String _methodName114;
    private String[] _methodParameterTypes114;
    private String _methodName115;
    private String[] _methodParameterTypes115;
    private String _methodName116;
    private String[] _methodParameterTypes116;
    private String _methodName117;
    private String[] _methodParameterTypes117;
    private String _methodName118;
    private String[] _methodParameterTypes118;

    public TelephoneServiceClpInvoker() {
        _methodName104 = "getBeanIdentifier";

        _methodParameterTypes104 = new String[] {  };

        _methodName105 = "setBeanIdentifier";

        _methodParameterTypes105 = new String[] { "java.lang.String" };

        _methodName110 = "addDomainPhone";

        _methodParameterTypes110 = new String[] {
                "long", "long", "long", "boolean"
            };

        _methodName111 = "addPersonPhone";

        _methodParameterTypes111 = new String[] {
                "long", "long", "br.com.atilo.jcondo.datatype.PhoneType",
                "boolean", "long"
            };

        _methodName112 = "updateEnterprisePhone";

        _methodParameterTypes112 = new String[] {
                "long", "long", "long", "boolean"
            };

        _methodName113 = "updatePersonPhone";

        _methodParameterTypes113 = new String[] {
                "long", "long", "long", "br.com.atilo.jcondo.datatype.PhoneType",
                "boolean"
            };

        _methodName114 = "deletePhone";

        _methodParameterTypes114 = new String[] { "long" };

        _methodName115 = "getPersonPhones";

        _methodParameterTypes115 = new String[] { "long" };

        _methodName116 = "getPersonPhones";

        _methodParameterTypes116 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.PhoneType"
            };

        _methodName117 = "getPersonPhone";

        _methodParameterTypes117 = new String[] { "long" };

        _methodName118 = "getDomainPhones";

        _methodParameterTypes118 = new String[] { "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName104.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes104, parameterTypes)) {
            return TelephoneServiceUtil.getBeanIdentifier();
        }

        if (_methodName105.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes105, parameterTypes)) {
            TelephoneServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName110.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
            return TelephoneServiceUtil.addDomainPhone(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                ((Long) arguments[2]).longValue(),
                ((Boolean) arguments[3]).booleanValue());
        }

        if (_methodName111.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
            return TelephoneServiceUtil.addPersonPhone(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                (br.com.atilo.jcondo.datatype.PhoneType) arguments[2],
                ((Boolean) arguments[3]).booleanValue(),
                ((Long) arguments[4]).longValue());
        }

        if (_methodName112.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes112, parameterTypes)) {
            return TelephoneServiceUtil.updateEnterprisePhone(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                ((Long) arguments[2]).longValue(),
                ((Boolean) arguments[3]).booleanValue());
        }

        if (_methodName113.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes113, parameterTypes)) {
            return TelephoneServiceUtil.updatePersonPhone(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                ((Long) arguments[2]).longValue(),
                (br.com.atilo.jcondo.datatype.PhoneType) arguments[3],
                ((Boolean) arguments[4]).booleanValue());
        }

        if (_methodName114.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes114, parameterTypes)) {
            return TelephoneServiceUtil.deletePhone(((Long) arguments[0]).longValue());
        }

        if (_methodName115.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes115, parameterTypes)) {
            return TelephoneServiceUtil.getPersonPhones(((Long) arguments[0]).longValue());
        }

        if (_methodName116.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes116, parameterTypes)) {
            return TelephoneServiceUtil.getPersonPhones(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.PhoneType) arguments[1]);
        }

        if (_methodName117.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes117, parameterTypes)) {
            return TelephoneServiceUtil.getPersonPhone(((Long) arguments[0]).longValue());
        }

        if (_methodName118.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes118, parameterTypes)) {
            return TelephoneServiceUtil.getDomainPhones(((Long) arguments[0]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
