package br.com.atilo.jcondo.manager.service.http;

import br.com.atilo.jcondo.manager.service.FlatServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.manager.service.FlatServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.manager.model.FlatSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.manager.model.Flat}, that is translated to a
 * {@link br.com.atilo.jcondo.manager.model.FlatSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FlatServiceHttp
 * @see br.com.atilo.jcondo.manager.model.FlatSoap
 * @see br.com.atilo.jcondo.manager.service.FlatServiceUtil
 * @generated
 */
public class FlatServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(FlatServiceSoap.class);

    public static br.com.atilo.jcondo.manager.model.FlatSoap addFlat(
        int number, int block, boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Flat returnValue = FlatServiceUtil.addFlat(number,
                    block, disables, brigade, pets, petTypes);

            return br.com.atilo.jcondo.manager.model.FlatSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.FlatSoap updateFlat(
        long id, boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Flat returnValue = FlatServiceUtil.updateFlat(id,
                    disables, brigade, pets, petTypes);

            return br.com.atilo.jcondo.manager.model.FlatSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.FlatSoap[] getPersonFlats(
        long personId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Flat> returnValue = FlatServiceUtil.getPersonFlats(personId);

            return br.com.atilo.jcondo.manager.model.FlatSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.FlatSoap getFlat(long id)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Flat returnValue = FlatServiceUtil.getFlat(id);

            return br.com.atilo.jcondo.manager.model.FlatSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.FlatSoap[] getFlats()
        throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Flat> returnValue = FlatServiceUtil.getFlats();

            return br.com.atilo.jcondo.manager.model.FlatSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static void setFlatDefaulting(long flatId, boolean isDefaulting)
        throws RemoteException {
        try {
            FlatServiceUtil.setFlatDefaulting(flatId, isDefaulting);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.FlatSoap setFlatPerson(
        long flatId, long personId) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Flat returnValue = FlatServiceUtil.setFlatPerson(flatId,
                    personId);

            return br.com.atilo.jcondo.manager.model.FlatSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
