/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.model.impl;

import java.util.List;

import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.datatype.EnterpriseStatus;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Address;
import com.liferay.portal.model.Organization;

/**
 * The extended model implementation for the Enterprise service. Represents a row in the &quot;jco_enterprise&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Enterprise} interface.
 * </p>
 *
 * @author anderson
 */
public class EnterpriseImpl extends EnterpriseBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a enterprise model instance should use the {@link br.com.atilo.jcondo.manager.model.Enterprise} interface instead.
	 */
	private Organization organization;

	public EnterpriseImpl() {
	}

	public String getName() {
		try {
			return organization.getName();	
		} catch (Exception e) {
			return null;
		}
	}

	public void setName(String name) {
		organization.setName(name);
	}

	public EnterpriseStatus getStatus() throws PortalException, SystemException {
		return EnterpriseStatus.valueOf(getStatusId());
	}

	public void setStatus(EnterpriseStatus status) {
		setStatusId(status.ordinal());
	}

	public DomainType getType() throws PortalException, SystemException {
		return DomainType.parse(organization.getType());
	}

	public void setType(DomainType type) {
		organization.setType(type.getLabel());
	}
	
	public String getDescription() throws PortalException, SystemException {
		return organization.getComments();
	}

	public void setDescription(String description) {
		organization.setComments(description);
	}

	public Address getAddress() throws PortalException, SystemException {
		return organization.getAddress();
	}

	public List<Person> getPeople() throws PortalException, SystemException {
		return PersonLocalServiceUtil.getDomainPeople(getId());
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}