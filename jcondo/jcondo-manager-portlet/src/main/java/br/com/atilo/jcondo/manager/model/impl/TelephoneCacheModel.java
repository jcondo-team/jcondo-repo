package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Telephone;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

/**
 * The cache model class for representing Telephone in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Telephone
 * @generated
 */
public class TelephoneCacheModel implements CacheModel<Telephone>,
    Externalizable {
    public long id;
    public String typeName;
    public long extension;
    public long number;
    public boolean primary;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{id=");
        sb.append(id);
        sb.append(", typeName=");
        sb.append(typeName);
        sb.append(", extension=");
        sb.append(extension);
        sb.append(", number=");
        sb.append(number);
        sb.append(", primary=");
        sb.append(primary);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Telephone toEntityModel() {
        TelephoneImpl telephoneImpl = new TelephoneImpl();

        telephoneImpl.setId(id);

        if (typeName == null) {
            telephoneImpl.setTypeName(StringPool.BLANK);
        } else {
            telephoneImpl.setTypeName(typeName);
        }

        telephoneImpl.setExtension(extension);
        telephoneImpl.setNumber(number);
        telephoneImpl.setPrimary(primary);

        telephoneImpl.resetOriginalValues();

        return telephoneImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        typeName = objectInput.readUTF();
        extension = objectInput.readLong();
        number = objectInput.readLong();
        primary = objectInput.readBoolean();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);

        if (typeName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(typeName);
        }

        objectOutput.writeLong(extension);
        objectOutput.writeLong(number);
        objectOutput.writeBoolean(primary);
    }
}
