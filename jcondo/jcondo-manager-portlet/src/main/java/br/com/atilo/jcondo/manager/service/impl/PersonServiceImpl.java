/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.NoSuchDepartmentException;
import br.com.atilo.jcondo.manager.NoSuchDomainException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.datatype.Gender;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonServiceUtil;
import br.com.atilo.jcondo.manager.service.base.PersonServiceBaseImpl;
import br.com.atilo.jcondo.manager.service.permission.PersonPermission;

/**
 * The implementation of the person remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.PersonService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.PersonServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.PersonServiceUtil
 */
public class PersonServiceImpl extends PersonServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.PersonServiceUtil} to access the person remote service.
	 */
	public Person createPerson() throws PortalException, SystemException {
		return personLocalService.createPerson();
	}

	public Person addPerson(String name, String surname, String identity, String email, Date birthday, Gender gender, String remark, PersonType personType, long domainId) throws Exception {
		if (!PersonPermission.hasPermission(Permission.ADD, personType, domainId)) {
			throw new BusinessException("exception.person.add.denied");
		}

		return personLocalService.addPerson(name, surname, identity, email, birthday, gender, personType, remark, domainId);
	}

	public Person updatePerson(long id, String name, String surname, String identity, String email, Date birthday, Gender gender, String remark, long domainId, Membership membership) throws Exception {
		Person person = PersonServiceUtil.getPerson(id);
		List<Membership> memberships = person.getMemberships();

		for (Membership m : memberships) {
			if (!PersonPermission.hasPermission(Permission.UPDATE, person, m.getDomainId())) {
				throw new BusinessException("exception.person.update.denied");
			}
		}

		return personLocalService.updatePerson(id, name, surname, identity, email, birthday, gender, remark, domainId, membership);
	}

	public Person updatePortrait(long personId, Image portrait) throws PortalException, SystemException {
		Person person = PersonServiceUtil.getPerson(personId);

		for (Membership membership : person.getMemberships()) {
			if (PersonPermission.hasPermission(Permission.UPDATE, person, membership.getDomainId())) {
				return personLocalService.updatePortrait(personId, portrait);
			}
		}

		throw new BusinessException("exception.person.update.denied");
	}

	public void updatePassword(long personId, String password, String newPassword) throws PortalException, SystemException {
		personLocalService.updatePassword(personId, password, newPassword);
	}	

	public Person getPersonByIdentity(String identity) throws PortalException, SystemException {
		Person person = personLocalService.getPersonByIdentity(identity);
		List<Membership> memberships = getPerson().getMemberships();

		for (Membership membership : memberships) {
			if (PersonPermission.hasPermission(Permission.VIEW, person, membership.getDomainId())) {
				return person;
			}
		}

		throw new BusinessException("exception.person.view.denied");
	}

	public Person getPerson(long id) throws PortalException, SystemException {
		Person person = personLocalService.getPerson(id);
		List<Membership> memberships = person.getMemberships();

		if (CollectionUtils.isEmpty(memberships)) {
			return person;
		}

		for (Membership membership : memberships) {
			if (PersonPermission.hasPermission(Permission.VIEW, person, membership.getDomainId())) {
				return person;
			}
		}

		throw new BusinessException("exception.person.view.denied");
	}

	public Person getPerson() throws PortalException, SystemException {
		return personLocalService.getPerson();
	}
	
	public List<PersonType> getPersonTypes(long domainId) throws PortalException, SystemException {
		List<PersonType> types = new ArrayList<PersonType>();

		try {
			FlatLocalServiceUtil.getFlat(domainId);
			types.addAll(Arrays.asList(PersonType.FLAT_TYPES));
		} catch (NoSuchFlatException e) {
			try {
				EnterpriseLocalServiceUtil.getEnterprise(domainId);
				types.addAll(Arrays.asList(PersonType.SUPPLIER_TYPES));
			} catch (NoSuchEnterpriseException e2) {
				try {
					Department department = DepartmentLocalServiceUtil.getDepartment(domainId);
					if (department.getType() == DomainType.ADMINISTRATION) {
						types.addAll(Arrays.asList(PersonType.ADMIN_TYPES));
					} else if (department.getOrganization().getName().trim().equalsIgnoreCase("gateway")) {
						types.addAll(Arrays.asList(PersonType.GATEKEEPER));
					} else {
						types.addAll(Arrays.asList(PersonType.EMPLOYEE));
					}
				} catch (NoSuchDepartmentException e3) {
					throw new NoSuchDomainException();
				}
			}
		}

		for (int i = types.size() - 1; i >= 0; i--) {
			if (!PersonPermission.hasPermission(Permission.ASSIGN_MEMBER, types.get(i), domainId)) {
				types.remove(i);
			}
		}

		return types;
	}
	
	public List<Person> getDomainPeople(long domainId) throws PortalException, SystemException {
		List<Person> people = personLocalService.getDomainPeople(domainId);

		for (int i = people.size() - 1; i >= 0; i--) {
			if (!PersonPermission.hasPermission(Permission.VIEW, people.get(i), domainId)) {
				people.remove(i);
			}
		}

		return people;
	}
	
	public List<Person> getDomainPeopleByType(long domainId, PersonType personType) throws PortalException, SystemException {
		List<Person> people = personLocalService.getDomainPeopleByType(domainId, personType);

		for (int i = people.size() - 1; i >= 0; i--) {
			if (!PersonPermission.hasPermission(Permission.VIEW, people.get(i), domainId)) {
				people.remove(i);
			}
		}

		return people; 
	}

	public List<Person> getDomainPeopleByName(long domainId, String name) throws PortalException, SystemException {
		List<Person> people = personLocalService.getDomainPeopleByName(domainId, name);

		for (int i = people.size() - 1; i >= 0; i--) {
			if (!PersonPermission.hasPermission(Permission.VIEW, people.get(i), domainId)) {
				people.remove(i);
			}
		}

		return people; 
	}

	public void completeUseRegistration(
			long companyId, String emailAddress, boolean autoPassword, boolean sendEmail) throws PortalException, SystemException {
		personLocalService.completePersonRegistration(companyId, emailAddress, autoPassword, sendEmail);
	}
	
	public boolean authenticatePerson(long personId, String password) throws PortalException, SystemException {
		return personLocalService.authenticatePerson(personId, password);
	}
}