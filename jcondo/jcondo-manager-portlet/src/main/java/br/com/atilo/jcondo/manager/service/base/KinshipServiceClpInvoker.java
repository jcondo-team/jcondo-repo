package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.KinshipServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class KinshipServiceClpInvoker {
    private String _methodName104;
    private String[] _methodParameterTypes104;
    private String _methodName105;
    private String[] _methodParameterTypes105;
    private String _methodName110;
    private String[] _methodParameterTypes110;
    private String _methodName111;
    private String[] _methodParameterTypes111;
    private String _methodName112;
    private String[] _methodParameterTypes112;
    private String _methodName113;
    private String[] _methodParameterTypes113;
    private String _methodName114;
    private String[] _methodParameterTypes114;

    public KinshipServiceClpInvoker() {
        _methodName104 = "getBeanIdentifier";

        _methodParameterTypes104 = new String[] {  };

        _methodName105 = "setBeanIdentifier";

        _methodParameterTypes105 = new String[] { "java.lang.String" };

        _methodName110 = "createKinship";

        _methodParameterTypes110 = new String[] {
                "long", "long", "br.com.atilo.jcondo.datatype.KinType"
            };

        _methodName111 = "addKinship";

        _methodParameterTypes111 = new String[] {
                "long", "long", "br.com.atilo.jcondo.datatype.KinType"
            };

        _methodName112 = "updateKinship";

        _methodParameterTypes112 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.KinType"
            };

        _methodName113 = "getKinships";

        _methodParameterTypes113 = new String[] { "long" };

        _methodName114 = "getPersonKinship";

        _methodParameterTypes114 = new String[] { "long", "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName104.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes104, parameterTypes)) {
            return KinshipServiceUtil.getBeanIdentifier();
        }

        if (_methodName105.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes105, parameterTypes)) {
            KinshipServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName110.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
            return KinshipServiceUtil.createKinship(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                (br.com.atilo.jcondo.datatype.KinType) arguments[2]);
        }

        if (_methodName111.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
            return KinshipServiceUtil.addKinship(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                (br.com.atilo.jcondo.datatype.KinType) arguments[2]);
        }

        if (_methodName112.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes112, parameterTypes)) {
            return KinshipServiceUtil.updateKinship(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.KinType) arguments[1]);
        }

        if (_methodName113.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes113, parameterTypes)) {
            return KinshipServiceUtil.getKinships(((Long) arguments[0]).longValue());
        }

        if (_methodName114.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes114, parameterTypes)) {
            return KinshipServiceUtil.getPersonKinship(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
