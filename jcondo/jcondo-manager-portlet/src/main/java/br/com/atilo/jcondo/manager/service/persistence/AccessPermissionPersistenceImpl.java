package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchAccessPermissionException;
import br.com.atilo.jcondo.manager.model.AccessPermission;
import br.com.atilo.jcondo.manager.model.impl.AccessPermissionImpl;
import br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.AccessPermissionPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the access permission service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AccessPermissionPersistence
 * @see AccessPermissionUtil
 * @generated
 */
public class AccessPermissionPersistenceImpl extends BasePersistenceImpl<AccessPermission>
    implements AccessPermissionPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link AccessPermissionUtil} to access the access permission persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = AccessPermissionImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionModelImpl.FINDER_CACHE_ENABLED,
            AccessPermissionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionModelImpl.FINDER_CACHE_ENABLED,
            AccessPermissionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSONANDDOMAIN =
        new FinderPath(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionModelImpl.FINDER_CACHE_ENABLED,
            AccessPermissionImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByPersonAndDomain",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDDOMAIN =
        new FinderPath(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionModelImpl.FINDER_CACHE_ENABLED,
            AccessPermissionImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPersonAndDomain",
            new String[] { Long.class.getName(), Long.class.getName() },
            AccessPermissionModelImpl.PERSONID_COLUMN_BITMASK |
            AccessPermissionModelImpl.DOMAINID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PERSONANDDOMAIN = new FinderPath(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByPersonAndDomain",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_PERSONANDDOMAIN_PERSONID_2 = "accessPermission.personId = ? AND ";
    private static final String _FINDER_COLUMN_PERSONANDDOMAIN_DOMAINID_2 = "accessPermission.domainId = ?";
    public static final FinderPath FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY = new FinderPath(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionModelImpl.FINDER_CACHE_ENABLED,
            AccessPermissionImpl.class, FINDER_CLASS_NAME_ENTITY,
            "fetchByPersonDomainAndWeekDay",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                Integer.class.getName()
            },
            AccessPermissionModelImpl.PERSONID_COLUMN_BITMASK |
            AccessPermissionModelImpl.DOMAINID_COLUMN_BITMASK |
            AccessPermissionModelImpl.WEEKDAY_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PERSONDOMAINANDWEEKDAY = new FinderPath(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByPersonDomainAndWeekDay",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                Integer.class.getName()
            });
    private static final String _FINDER_COLUMN_PERSONDOMAINANDWEEKDAY_PERSONID_2 =
        "accessPermission.personId = ? AND ";
    private static final String _FINDER_COLUMN_PERSONDOMAINANDWEEKDAY_DOMAINID_2 =
        "accessPermission.domainId = ? AND ";
    private static final String _FINDER_COLUMN_PERSONDOMAINANDWEEKDAY_WEEKDAY_2 = "accessPermission.weekDay = ?";
    private static final String _SQL_SELECT_ACCESSPERMISSION = "SELECT accessPermission FROM AccessPermission accessPermission";
    private static final String _SQL_SELECT_ACCESSPERMISSION_WHERE = "SELECT accessPermission FROM AccessPermission accessPermission WHERE ";
    private static final String _SQL_COUNT_ACCESSPERMISSION = "SELECT COUNT(accessPermission) FROM AccessPermission accessPermission";
    private static final String _SQL_COUNT_ACCESSPERMISSION_WHERE = "SELECT COUNT(accessPermission) FROM AccessPermission accessPermission WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "accessPermission.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No AccessPermission exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No AccessPermission exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(AccessPermissionPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static AccessPermission _nullAccessPermission = new AccessPermissionImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<AccessPermission> toCacheModel() {
                return _nullAccessPermissionCacheModel;
            }
        };

    private static CacheModel<AccessPermission> _nullAccessPermissionCacheModel = new CacheModel<AccessPermission>() {
            @Override
            public AccessPermission toEntityModel() {
                return _nullAccessPermission;
            }
        };

    public AccessPermissionPersistenceImpl() {
        setModelClass(AccessPermission.class);
    }

    /**
     * Returns all the access permissions where personId = &#63; and domainId = &#63;.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @return the matching access permissions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<AccessPermission> findByPersonAndDomain(long personId,
        long domainId) throws SystemException {
        return findByPersonAndDomain(personId, domainId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the access permissions where personId = &#63; and domainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param start the lower bound of the range of access permissions
     * @param end the upper bound of the range of access permissions (not inclusive)
     * @return the range of matching access permissions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<AccessPermission> findByPersonAndDomain(long personId,
        long domainId, int start, int end) throws SystemException {
        return findByPersonAndDomain(personId, domainId, start, end, null);
    }

    /**
     * Returns an ordered range of all the access permissions where personId = &#63; and domainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param start the lower bound of the range of access permissions
     * @param end the upper bound of the range of access permissions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching access permissions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<AccessPermission> findByPersonAndDomain(long personId,
        long domainId, int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDDOMAIN;
            finderArgs = new Object[] { personId, domainId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSONANDDOMAIN;
            finderArgs = new Object[] {
                    personId, domainId,
                    
                    start, end, orderByComparator
                };
        }

        List<AccessPermission> list = (List<AccessPermission>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (AccessPermission accessPermission : list) {
                if ((personId != accessPermission.getPersonId()) ||
                        (domainId != accessPermission.getDomainId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_ACCESSPERMISSION_WHERE);

            query.append(_FINDER_COLUMN_PERSONANDDOMAIN_PERSONID_2);

            query.append(_FINDER_COLUMN_PERSONANDDOMAIN_DOMAINID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(AccessPermissionModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                qPos.add(domainId);

                if (!pagination) {
                    list = (List<AccessPermission>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<AccessPermission>(list);
                } else {
                    list = (List<AccessPermission>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first access permission in the ordered set where personId = &#63; and domainId = &#63;.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching access permission
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a matching access permission could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission findByPersonAndDomain_First(long personId,
        long domainId, OrderByComparator orderByComparator)
        throws NoSuchAccessPermissionException, SystemException {
        AccessPermission accessPermission = fetchByPersonAndDomain_First(personId,
                domainId, orderByComparator);

        if (accessPermission != null) {
            return accessPermission;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(", domainId=");
        msg.append(domainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchAccessPermissionException(msg.toString());
    }

    /**
     * Returns the first access permission in the ordered set where personId = &#63; and domainId = &#63;.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching access permission, or <code>null</code> if a matching access permission could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission fetchByPersonAndDomain_First(long personId,
        long domainId, OrderByComparator orderByComparator)
        throws SystemException {
        List<AccessPermission> list = findByPersonAndDomain(personId, domainId,
                0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last access permission in the ordered set where personId = &#63; and domainId = &#63;.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching access permission
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a matching access permission could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission findByPersonAndDomain_Last(long personId,
        long domainId, OrderByComparator orderByComparator)
        throws NoSuchAccessPermissionException, SystemException {
        AccessPermission accessPermission = fetchByPersonAndDomain_Last(personId,
                domainId, orderByComparator);

        if (accessPermission != null) {
            return accessPermission;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(", domainId=");
        msg.append(domainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchAccessPermissionException(msg.toString());
    }

    /**
     * Returns the last access permission in the ordered set where personId = &#63; and domainId = &#63;.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching access permission, or <code>null</code> if a matching access permission could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission fetchByPersonAndDomain_Last(long personId,
        long domainId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByPersonAndDomain(personId, domainId);

        if (count == 0) {
            return null;
        }

        List<AccessPermission> list = findByPersonAndDomain(personId, domainId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the access permissions before and after the current access permission in the ordered set where personId = &#63; and domainId = &#63;.
     *
     * @param id the primary key of the current access permission
     * @param personId the person ID
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next access permission
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission[] findByPersonAndDomain_PrevAndNext(long id,
        long personId, long domainId, OrderByComparator orderByComparator)
        throws NoSuchAccessPermissionException, SystemException {
        AccessPermission accessPermission = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            AccessPermission[] array = new AccessPermissionImpl[3];

            array[0] = getByPersonAndDomain_PrevAndNext(session,
                    accessPermission, personId, domainId, orderByComparator,
                    true);

            array[1] = accessPermission;

            array[2] = getByPersonAndDomain_PrevAndNext(session,
                    accessPermission, personId, domainId, orderByComparator,
                    false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected AccessPermission getByPersonAndDomain_PrevAndNext(
        Session session, AccessPermission accessPermission, long personId,
        long domainId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ACCESSPERMISSION_WHERE);

        query.append(_FINDER_COLUMN_PERSONANDDOMAIN_PERSONID_2);

        query.append(_FINDER_COLUMN_PERSONANDDOMAIN_DOMAINID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(AccessPermissionModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(personId);

        qPos.add(domainId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(accessPermission);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<AccessPermission> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the access permissions where personId = &#63; and domainId = &#63; from the database.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByPersonAndDomain(long personId, long domainId)
        throws SystemException {
        for (AccessPermission accessPermission : findByPersonAndDomain(
                personId, domainId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(accessPermission);
        }
    }

    /**
     * Returns the number of access permissions where personId = &#63; and domainId = &#63;.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @return the number of matching access permissions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByPersonAndDomain(long personId, long domainId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PERSONANDDOMAIN;

        Object[] finderArgs = new Object[] { personId, domainId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_ACCESSPERMISSION_WHERE);

            query.append(_FINDER_COLUMN_PERSONANDDOMAIN_PERSONID_2);

            query.append(_FINDER_COLUMN_PERSONANDDOMAIN_DOMAINID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                qPos.add(domainId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessPermissionException} if it could not be found.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param weekDay the week day
     * @return the matching access permission
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a matching access permission could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission findByPersonDomainAndWeekDay(long personId,
        long domainId, int weekDay)
        throws NoSuchAccessPermissionException, SystemException {
        AccessPermission accessPermission = fetchByPersonDomainAndWeekDay(personId,
                domainId, weekDay);

        if (accessPermission == null) {
            StringBundler msg = new StringBundler(8);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("personId=");
            msg.append(personId);

            msg.append(", domainId=");
            msg.append(domainId);

            msg.append(", weekDay=");
            msg.append(weekDay);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchAccessPermissionException(msg.toString());
        }

        return accessPermission;
    }

    /**
     * Returns the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param weekDay the week day
     * @return the matching access permission, or <code>null</code> if a matching access permission could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission fetchByPersonDomainAndWeekDay(long personId,
        long domainId, int weekDay) throws SystemException {
        return fetchByPersonDomainAndWeekDay(personId, domainId, weekDay, true);
    }

    /**
     * Returns the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param weekDay the week day
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching access permission, or <code>null</code> if a matching access permission could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission fetchByPersonDomainAndWeekDay(long personId,
        long domainId, int weekDay, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { personId, domainId, weekDay };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY,
                    finderArgs, this);
        }

        if (result instanceof AccessPermission) {
            AccessPermission accessPermission = (AccessPermission) result;

            if ((personId != accessPermission.getPersonId()) ||
                    (domainId != accessPermission.getDomainId()) ||
                    (weekDay != accessPermission.getWeekDay())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_SELECT_ACCESSPERMISSION_WHERE);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDWEEKDAY_PERSONID_2);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDWEEKDAY_DOMAINID_2);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDWEEKDAY_WEEKDAY_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                qPos.add(domainId);

                qPos.add(weekDay);

                List<AccessPermission> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "AccessPermissionPersistenceImpl.fetchByPersonDomainAndWeekDay(long, long, int, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    AccessPermission accessPermission = list.get(0);

                    result = accessPermission;

                    cacheResult(accessPermission);

                    if ((accessPermission.getPersonId() != personId) ||
                            (accessPermission.getDomainId() != domainId) ||
                            (accessPermission.getWeekDay() != weekDay)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY,
                            finderArgs, accessPermission);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (AccessPermission) result;
        }
    }

    /**
     * Removes the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; from the database.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param weekDay the week day
     * @return the access permission that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission removeByPersonDomainAndWeekDay(long personId,
        long domainId, int weekDay)
        throws NoSuchAccessPermissionException, SystemException {
        AccessPermission accessPermission = findByPersonDomainAndWeekDay(personId,
                domainId, weekDay);

        return remove(accessPermission);
    }

    /**
     * Returns the number of access permissions where personId = &#63; and domainId = &#63; and weekDay = &#63;.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param weekDay the week day
     * @return the number of matching access permissions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByPersonDomainAndWeekDay(long personId, long domainId,
        int weekDay) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PERSONDOMAINANDWEEKDAY;

        Object[] finderArgs = new Object[] { personId, domainId, weekDay };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_ACCESSPERMISSION_WHERE);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDWEEKDAY_PERSONID_2);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDWEEKDAY_DOMAINID_2);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDWEEKDAY_WEEKDAY_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                qPos.add(domainId);

                qPos.add(weekDay);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the access permission in the entity cache if it is enabled.
     *
     * @param accessPermission the access permission
     */
    @Override
    public void cacheResult(AccessPermission accessPermission) {
        EntityCacheUtil.putResult(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionImpl.class, accessPermission.getPrimaryKey(),
            accessPermission);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY,
            new Object[] {
                accessPermission.getPersonId(), accessPermission.getDomainId(),
                accessPermission.getWeekDay()
            }, accessPermission);

        accessPermission.resetOriginalValues();
    }

    /**
     * Caches the access permissions in the entity cache if it is enabled.
     *
     * @param accessPermissions the access permissions
     */
    @Override
    public void cacheResult(List<AccessPermission> accessPermissions) {
        for (AccessPermission accessPermission : accessPermissions) {
            if (EntityCacheUtil.getResult(
                        AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
                        AccessPermissionImpl.class,
                        accessPermission.getPrimaryKey()) == null) {
                cacheResult(accessPermission);
            } else {
                accessPermission.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all access permissions.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(AccessPermissionImpl.class.getName());
        }

        EntityCacheUtil.clearCache(AccessPermissionImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the access permission.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(AccessPermission accessPermission) {
        EntityCacheUtil.removeResult(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionImpl.class, accessPermission.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(accessPermission);
    }

    @Override
    public void clearCache(List<AccessPermission> accessPermissions) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (AccessPermission accessPermission : accessPermissions) {
            EntityCacheUtil.removeResult(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
                AccessPermissionImpl.class, accessPermission.getPrimaryKey());

            clearUniqueFindersCache(accessPermission);
        }
    }

    protected void cacheUniqueFindersCache(AccessPermission accessPermission) {
        if (accessPermission.isNew()) {
            Object[] args = new Object[] {
                    accessPermission.getPersonId(),
                    accessPermission.getDomainId(),
                    accessPermission.getWeekDay()
                };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PERSONDOMAINANDWEEKDAY,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY,
                args, accessPermission);
        } else {
            AccessPermissionModelImpl accessPermissionModelImpl = (AccessPermissionModelImpl) accessPermission;

            if ((accessPermissionModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        accessPermission.getPersonId(),
                        accessPermission.getDomainId(),
                        accessPermission.getWeekDay()
                    };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PERSONDOMAINANDWEEKDAY,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY,
                    args, accessPermission);
            }
        }
    }

    protected void clearUniqueFindersCache(AccessPermission accessPermission) {
        AccessPermissionModelImpl accessPermissionModelImpl = (AccessPermissionModelImpl) accessPermission;

        Object[] args = new Object[] {
                accessPermission.getPersonId(), accessPermission.getDomainId(),
                accessPermission.getWeekDay()
            };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSONDOMAINANDWEEKDAY,
            args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY,
            args);

        if ((accessPermissionModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY.getColumnBitmask()) != 0) {
            args = new Object[] {
                    accessPermissionModelImpl.getOriginalPersonId(),
                    accessPermissionModelImpl.getOriginalDomainId(),
                    accessPermissionModelImpl.getOriginalWeekDay()
                };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSONDOMAINANDWEEKDAY,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDWEEKDAY,
                args);
        }
    }

    /**
     * Creates a new access permission with the primary key. Does not add the access permission to the database.
     *
     * @param id the primary key for the new access permission
     * @return the new access permission
     */
    @Override
    public AccessPermission create(long id) {
        AccessPermission accessPermission = new AccessPermissionImpl();

        accessPermission.setNew(true);
        accessPermission.setPrimaryKey(id);

        return accessPermission;
    }

    /**
     * Removes the access permission with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the access permission
     * @return the access permission that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission remove(long id)
        throws NoSuchAccessPermissionException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the access permission with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the access permission
     * @return the access permission that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission remove(Serializable primaryKey)
        throws NoSuchAccessPermissionException, SystemException {
        Session session = null;

        try {
            session = openSession();

            AccessPermission accessPermission = (AccessPermission) session.get(AccessPermissionImpl.class,
                    primaryKey);

            if (accessPermission == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchAccessPermissionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(accessPermission);
        } catch (NoSuchAccessPermissionException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected AccessPermission removeImpl(AccessPermission accessPermission)
        throws SystemException {
        accessPermission = toUnwrappedModel(accessPermission);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(accessPermission)) {
                accessPermission = (AccessPermission) session.get(AccessPermissionImpl.class,
                        accessPermission.getPrimaryKeyObj());
            }

            if (accessPermission != null) {
                session.delete(accessPermission);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (accessPermission != null) {
            clearCache(accessPermission);
        }

        return accessPermission;
    }

    @Override
    public AccessPermission updateImpl(
        br.com.atilo.jcondo.manager.model.AccessPermission accessPermission)
        throws SystemException {
        accessPermission = toUnwrappedModel(accessPermission);

        boolean isNew = accessPermission.isNew();

        AccessPermissionModelImpl accessPermissionModelImpl = (AccessPermissionModelImpl) accessPermission;

        Session session = null;

        try {
            session = openSession();

            if (accessPermission.isNew()) {
                session.save(accessPermission);

                accessPermission.setNew(false);
            } else {
                session.merge(accessPermission);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !AccessPermissionModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((accessPermissionModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDDOMAIN.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        accessPermissionModelImpl.getOriginalPersonId(),
                        accessPermissionModelImpl.getOriginalDomainId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSONANDDOMAIN,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDDOMAIN,
                    args);

                args = new Object[] {
                        accessPermissionModelImpl.getPersonId(),
                        accessPermissionModelImpl.getDomainId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSONANDDOMAIN,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDDOMAIN,
                    args);
            }
        }

        EntityCacheUtil.putResult(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
            AccessPermissionImpl.class, accessPermission.getPrimaryKey(),
            accessPermission);

        clearUniqueFindersCache(accessPermission);
        cacheUniqueFindersCache(accessPermission);

        return accessPermission;
    }

    protected AccessPermission toUnwrappedModel(
        AccessPermission accessPermission) {
        if (accessPermission instanceof AccessPermissionImpl) {
            return accessPermission;
        }

        AccessPermissionImpl accessPermissionImpl = new AccessPermissionImpl();

        accessPermissionImpl.setNew(accessPermission.isNew());
        accessPermissionImpl.setPrimaryKey(accessPermission.getPrimaryKey());

        accessPermissionImpl.setId(accessPermission.getId());
        accessPermissionImpl.setPersonId(accessPermission.getPersonId());
        accessPermissionImpl.setDomainId(accessPermission.getDomainId());
        accessPermissionImpl.setWeekDay(accessPermission.getWeekDay());
        accessPermissionImpl.setBeginTime(accessPermission.getBeginTime());
        accessPermissionImpl.setEndTime(accessPermission.getEndTime());
        accessPermissionImpl.setBeginDate(accessPermission.getBeginDate());
        accessPermissionImpl.setEndDate(accessPermission.getEndDate());
        accessPermissionImpl.setOprDate(accessPermission.getOprDate());
        accessPermissionImpl.setOprUser(accessPermission.getOprUser());

        return accessPermissionImpl;
    }

    /**
     * Returns the access permission with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the access permission
     * @return the access permission
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission findByPrimaryKey(Serializable primaryKey)
        throws NoSuchAccessPermissionException, SystemException {
        AccessPermission accessPermission = fetchByPrimaryKey(primaryKey);

        if (accessPermission == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchAccessPermissionException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return accessPermission;
    }

    /**
     * Returns the access permission with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessPermissionException} if it could not be found.
     *
     * @param id the primary key of the access permission
     * @return the access permission
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission findByPrimaryKey(long id)
        throws NoSuchAccessPermissionException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the access permission with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the access permission
     * @return the access permission, or <code>null</code> if a access permission with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        AccessPermission accessPermission = (AccessPermission) EntityCacheUtil.getResult(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
                AccessPermissionImpl.class, primaryKey);

        if (accessPermission == _nullAccessPermission) {
            return null;
        }

        if (accessPermission == null) {
            Session session = null;

            try {
                session = openSession();

                accessPermission = (AccessPermission) session.get(AccessPermissionImpl.class,
                        primaryKey);

                if (accessPermission != null) {
                    cacheResult(accessPermission);
                } else {
                    EntityCacheUtil.putResult(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
                        AccessPermissionImpl.class, primaryKey,
                        _nullAccessPermission);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(AccessPermissionModelImpl.ENTITY_CACHE_ENABLED,
                    AccessPermissionImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return accessPermission;
    }

    /**
     * Returns the access permission with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the access permission
     * @return the access permission, or <code>null</code> if a access permission with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public AccessPermission fetchByPrimaryKey(long id)
        throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the access permissions.
     *
     * @return the access permissions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<AccessPermission> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the access permissions.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of access permissions
     * @param end the upper bound of the range of access permissions (not inclusive)
     * @return the range of access permissions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<AccessPermission> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the access permissions.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of access permissions
     * @param end the upper bound of the range of access permissions (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of access permissions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<AccessPermission> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<AccessPermission> list = (List<AccessPermission>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ACCESSPERMISSION);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ACCESSPERMISSION;

                if (pagination) {
                    sql = sql.concat(AccessPermissionModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<AccessPermission>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<AccessPermission>(list);
                } else {
                    list = (List<AccessPermission>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the access permissions from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (AccessPermission accessPermission : findAll()) {
            remove(accessPermission);
        }
    }

    /**
     * Returns the number of access permissions.
     *
     * @return the number of access permissions
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ACCESSPERMISSION);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the access permission persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.AccessPermission")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<AccessPermission>> listenersList = new ArrayList<ModelListener<AccessPermission>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<AccessPermission>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(AccessPermissionImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
