package br.com.atilo.jcondo.manager.service.http;

import br.com.atilo.jcondo.manager.service.EnterpriseServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.manager.service.EnterpriseServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.manager.model.EnterpriseSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.manager.model.Enterprise}, that is translated to a
 * {@link br.com.atilo.jcondo.manager.model.EnterpriseSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EnterpriseServiceHttp
 * @see br.com.atilo.jcondo.manager.model.EnterpriseSoap
 * @see br.com.atilo.jcondo.manager.service.EnterpriseServiceUtil
 * @generated
 */
public class EnterpriseServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(EnterpriseServiceSoap.class);

    public static br.com.atilo.jcondo.manager.model.EnterpriseSoap createEnterprise()
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Enterprise returnValue = EnterpriseServiceUtil.createEnterprise();

            return br.com.atilo.jcondo.manager.model.EnterpriseSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.EnterpriseSoap addEnterprise(
        java.lang.String identity, java.lang.String name,
        br.com.atilo.jcondo.datatype.DomainType type,
        br.com.atilo.jcondo.datatype.EnterpriseStatus status,
        java.lang.String description) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Enterprise returnValue = EnterpriseServiceUtil.addEnterprise(identity,
                    name, type, status, description);

            return br.com.atilo.jcondo.manager.model.EnterpriseSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.EnterpriseSoap updateEnterprise(
        long enterpriseId, java.lang.String identity, java.lang.String name,
        br.com.atilo.jcondo.datatype.DomainType type,
        br.com.atilo.jcondo.datatype.EnterpriseStatus status,
        java.lang.String description) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Enterprise returnValue = EnterpriseServiceUtil.updateEnterprise(enterpriseId,
                    identity, name, type, status, description);

            return br.com.atilo.jcondo.manager.model.EnterpriseSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.EnterpriseSoap getEnterprise(
        long id) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Enterprise returnValue = EnterpriseServiceUtil.getEnterprise(id);

            return br.com.atilo.jcondo.manager.model.EnterpriseSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.EnterpriseSoap[] getEnterprisesByType(
        br.com.atilo.jcondo.datatype.DomainType type) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> returnValue =
                EnterpriseServiceUtil.getEnterprisesByType(type);

            return br.com.atilo.jcondo.manager.model.EnterpriseSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.EnterpriseSoap getEnterpriseByIdentity(
        java.lang.String identity) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Enterprise returnValue = EnterpriseServiceUtil.getEnterpriseByIdentity(identity);

            return br.com.atilo.jcondo.manager.model.EnterpriseSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
