/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Address;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.AddressLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.service.base.AddressLocalServiceBaseImpl;

/**
 * The implementation of the address local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.AddressLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.AddressLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.AddressLocalServiceUtil
 */
public class AddressLocalServiceImpl extends AddressLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.AddressLocalServiceUtil} to access the address local service.
	 */
	
	public Address createAddress() {
		return AddressLocalServiceUtil.createAddress(0);
	}

	public Address getPersonAddress(long personId) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Person person = personLocalService.getPerson(personId);
		List<Address> addresses = AddressLocalServiceUtil.getAddresses(sc.getCompanyId(), 
																	   Contact.class.getName(), 
																	   person.getUser().getContactId());
		return CollectionUtils.isEmpty(addresses) ? null : addresses.get(0);
	}

	public Address addPersonAddress(long personId, String street, String city, long stateId, String postCode) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Person person = personLocalService.getPerson(personId);
		Address address = AddressLocalServiceUtil.addAddress(sc.getUserId(), Contact.class.getName(), 
				 											 person.getUser().getContactId(), street, null, null, 
				 											 city, postCode, stateId, 48, 11001, true, true);
		return address;
	}

	public Address getEnterpriseAddress(long enterpriseId) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Enterprise enterprise = enterpriseLocalService.getEnterprise(enterpriseId);
		List<Address> addresses = AddressLocalServiceUtil.getAddresses(sc.getCompanyId(), 
																	   Organization.class.getName(), 
																	   enterprise.getOrganizationId());
		return CollectionUtils.isEmpty(addresses) ? null : addresses.get(0);
	}

	public Address addEnterpriseAddress(long enterpriseId, String street, String city, long stateId, String postCode) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Enterprise enterprise = enterpriseLocalService.getEnterprise(enterpriseId);
		Address address = AddressLocalServiceUtil.addAddress(sc.getUserId(), Organization.class.getName(), 
				 											 enterprise.getOrganizationId(), street, null, null, 
				 											 city, postCode, stateId, 48, 12001, true, true);
		return address;
	}

	public Address updateAddress(long addressId, String street, String city, long stateId, String postCode) throws PortalException, SystemException {
		Address address = AddressLocalServiceUtil.getAddress(addressId);
		return AddressLocalServiceUtil.updateAddress(addressId, street, null, null, city, postCode, stateId, address.getCountryId(), address.getTypeId(), true, true);
	}

	public Address deleteAddress(long addressId) throws PortalException, SystemException {
		return AddressLocalServiceUtil.deleteAddress(addressId);
	}

}