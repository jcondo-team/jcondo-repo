package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.ParkingServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class ParkingServiceClpInvoker {
    private String _methodName104;
    private String[] _methodParameterTypes104;
    private String _methodName105;
    private String[] _methodParameterTypes105;
    private String _methodName110;
    private String[] _methodParameterTypes110;
    private String _methodName111;
    private String[] _methodParameterTypes111;
    private String _methodName112;
    private String[] _methodParameterTypes112;
    private String _methodName113;
    private String[] _methodParameterTypes113;
    private String _methodName114;
    private String[] _methodParameterTypes114;
    private String _methodName115;
    private String[] _methodParameterTypes115;
    private String _methodName116;
    private String[] _methodParameterTypes116;

    public ParkingServiceClpInvoker() {
        _methodName104 = "getBeanIdentifier";

        _methodParameterTypes104 = new String[] {  };

        _methodName105 = "setBeanIdentifier";

        _methodParameterTypes105 = new String[] { "java.lang.String" };

        _methodName110 = "addParking";

        _methodParameterTypes110 = new String[] {
                "java.lang.String", "br.com.atilo.jcondo.datatype.ParkingType",
                "long"
            };

        _methodName111 = "updateParking";

        _methodParameterTypes111 = new String[] {
                "long", "java.lang.String",
                "br.com.atilo.jcondo.datatype.ParkingType", "long"
            };

        _methodName112 = "getOwnedParkings";

        _methodParameterTypes112 = new String[] { "long" };

        _methodName113 = "getRentedParkings";

        _methodParameterTypes113 = new String[] { "long" };

        _methodName114 = "getGrantedParkings";

        _methodParameterTypes114 = new String[] { "long" };

        _methodName115 = "rent";

        _methodParameterTypes115 = new String[] { "long", "long" };

        _methodName116 = "unrent";

        _methodParameterTypes116 = new String[] { "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName104.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes104, parameterTypes)) {
            return ParkingServiceUtil.getBeanIdentifier();
        }

        if (_methodName105.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes105, parameterTypes)) {
            ParkingServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName110.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
            return ParkingServiceUtil.addParking((java.lang.String) arguments[0],
                (br.com.atilo.jcondo.datatype.ParkingType) arguments[1],
                ((Long) arguments[2]).longValue());
        }

        if (_methodName111.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
            return ParkingServiceUtil.updateParking(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (br.com.atilo.jcondo.datatype.ParkingType) arguments[2],
                ((Long) arguments[3]).longValue());
        }

        if (_methodName112.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes112, parameterTypes)) {
            return ParkingServiceUtil.getOwnedParkings(((Long) arguments[0]).longValue());
        }

        if (_methodName113.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes113, parameterTypes)) {
            return ParkingServiceUtil.getRentedParkings(((Long) arguments[0]).longValue());
        }

        if (_methodName114.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes114, parameterTypes)) {
            return ParkingServiceUtil.getGrantedParkings(((Long) arguments[0]).longValue());
        }

        if (_methodName115.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes115, parameterTypes)) {
            return ParkingServiceUtil.rent(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        if (_methodName116.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes116, parameterTypes)) {
            return ParkingServiceUtil.unrent(((Long) arguments[0]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
