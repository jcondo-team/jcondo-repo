package br.com.atilo.jcondo.manager.service.http;

import br.com.atilo.jcondo.manager.service.TelephoneServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.manager.service.TelephoneServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.manager.model.TelephoneSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.manager.model.Telephone}, that is translated to a
 * {@link br.com.atilo.jcondo.manager.model.TelephoneSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TelephoneServiceHttp
 * @see br.com.atilo.jcondo.manager.model.TelephoneSoap
 * @see br.com.atilo.jcondo.manager.service.TelephoneServiceUtil
 * @generated
 */
public class TelephoneServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(TelephoneServiceSoap.class);

    public static br.com.atilo.jcondo.manager.model.TelephoneSoap addDomainPhone(
        long domainId, long number, long extension, boolean primary)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Telephone returnValue = TelephoneServiceUtil.addDomainPhone(domainId,
                    number, extension, primary);

            return br.com.atilo.jcondo.manager.model.TelephoneSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.TelephoneSoap addPersonPhone(
        long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary,
        long personId) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Telephone returnValue = TelephoneServiceUtil.addPersonPhone(number,
                    extension, type, primary, personId);

            return br.com.atilo.jcondo.manager.model.TelephoneSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.TelephoneSoap updateEnterprisePhone(
        long phoneId, long number, long extension, boolean primary)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Telephone returnValue = TelephoneServiceUtil.updateEnterprisePhone(phoneId,
                    number, extension, primary);

            return br.com.atilo.jcondo.manager.model.TelephoneSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.TelephoneSoap updatePersonPhone(
        long phoneId, long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Telephone returnValue = TelephoneServiceUtil.updatePersonPhone(phoneId,
                    number, extension, type, primary);

            return br.com.atilo.jcondo.manager.model.TelephoneSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.TelephoneSoap deletePhone(
        long phoneId) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Telephone returnValue = TelephoneServiceUtil.deletePhone(phoneId);

            return br.com.atilo.jcondo.manager.model.TelephoneSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.TelephoneSoap[] getPersonPhones(
        long personId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Telephone> returnValue =
                TelephoneServiceUtil.getPersonPhones(personId);

            return br.com.atilo.jcondo.manager.model.TelephoneSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.TelephoneSoap[] getPersonPhones(
        long personId, br.com.atilo.jcondo.datatype.PhoneType type)
        throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Telephone> returnValue =
                TelephoneServiceUtil.getPersonPhones(personId, type);

            return br.com.atilo.jcondo.manager.model.TelephoneSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.TelephoneSoap getPersonPhone(
        long personId) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Telephone returnValue = TelephoneServiceUtil.getPersonPhone(personId);

            return br.com.atilo.jcondo.manager.model.TelephoneSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.TelephoneSoap[] getDomainPhones(
        long domainId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Telephone> returnValue =
                TelephoneServiceUtil.getDomainPhones(domainId);

            return br.com.atilo.jcondo.manager.model.TelephoneSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
