package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.faces.application.FacesMessage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Telephone;
import br.com.atilo.jcondo.manager.service.TelephoneLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.TelephoneServiceUtil;

public class DomainPhoneBean<Domain extends BaseModel<?>> extends Observable implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(DomainPhoneBean.class);

	private List<Telephone> phones;

	private Domain domain;

	private Telephone phone;

	private String phoneNumber;

	public void init(Domain domain) throws Exception {
		this.domain = domain;
		if (domain.isNew()) {
			this.phones = new ArrayList<Telephone>();
		} else {
			this.phones = TelephoneServiceUtil.getDomainPhones((Long) domain.getPrimaryKeyObj());
		}
	}

	public void onPhoneCreate() {
		phone = TelephoneLocalServiceUtil.createTelephone(0);
		phoneNumber = null;
	}

	public void onPhoneAdd() {
		String pn = phoneNumber.replaceAll("[^0-9]*", "");
		phone.setExtension(Long.valueOf(StringUtils.left(pn, 2)));
		phone.setNumber(Long.valueOf(StringUtils.right(pn, pn.length() - 2)));
		phones.add(phone);
	}

	public void onPhoneDelete(Telephone phone) {
		try {
			phones.remove(phone);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on phone remove", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}	

	@SuppressWarnings("unchecked")
	public void onPhoneSave() {
		try {
			List<Telephone> currentPhones = TelephoneServiceUtil.getDomainPhones((Long) domain.getPrimaryKeyObj());
			List<Telephone> addedPhones = (List<Telephone>) CollectionUtils.subtract(phones, currentPhones);
			List<Telephone> removedPhones = (List<Telephone>) CollectionUtils.subtract(currentPhones, phones);

			for (Telephone phone : addedPhones) {
				TelephoneServiceUtil.addDomainPhone((Long) domain.getPrimaryKeyObj(), phone.getNumber(), phone.getExtension(), phone.isPrimary());
			}

			for (Telephone phone : removedPhones) {
				TelephoneServiceUtil.deletePhone(phone.getId());
			}
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on phone add: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on phone add", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public List<Telephone> getPhones() {
		return phones;
	}

	public void setPhones(List<Telephone> phones) {
		this.phones = phones;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Telephone getPhone() {
		return phone;
	}

	public void setPhone(Telephone phone) {
		this.phone = phone;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}