package br.com.atilo.jcondo.manager.bean.flat;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Parking;
import br.com.atilo.jcondo.manager.service.ParkingServiceUtil;
import br.com.atilo.jcondo.manager.bean.ModelDataModel;

public class ParkingBean<Domain extends BaseModel<Domain>> implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static Logger LOGGER = Logger.getLogger(ParkingBean.class);

	private ModelDataModel<Parking> model;

	private Parking parking;

	private Domain domain;

	private Domain renterDomain;

	private List<Parking> ownedParkings;

	private List<Parking> rentedParkings;

	private List<Parking> grantedParkings;

	public void init(Domain domain) {
		try {
			this.domain = domain;
			ownedParkings = ParkingServiceUtil.getOwnedParkings((Long) domain.getPrimaryKeyObj());
			rentedParkings = ParkingServiceUtil.getRentedParkings((Long) domain.getPrimaryKeyObj());
			grantedParkings = ParkingServiceUtil.getGrantedParkings((Long) domain.getPrimaryKeyObj());
		} catch (Exception e) {
			LOGGER.error("", e);
		}
	}
	
	public void onCreate() {
		
	}
	
	public void onSave() {
		
	}
	
	public void onEdit() {
		
	}

	public void onRent() throws Exception {
		ParkingServiceUtil.updateParking(parking.getId(), parking.getCode(), parking.getType(), parking.getOwnerDomainId());
	}

	public void onRentalCancel() throws Exception {
		ParkingServiceUtil.updateParking(parking.getId(), parking.getCode(), parking.getType(), parking.getOwnerDomainId());
	}
	
	public void onDomainSearch(Domain domain) throws Exception {
		model.clear();
		model.addAll(ParkingServiceUtil.getOwnedParkings((Long) domain.getPrimaryKeyObj()));
		model.addAll(ParkingServiceUtil.getRentedParkings((Long) domain.getPrimaryKeyObj()));
	}

	public void onParkingRent() {
		try {
			parking = ParkingServiceUtil.rent((Long) domain.getPrimaryKeyObj(), 
											  (Long) renterDomain.getPrimaryKeyObj());
			grantedParkings.add(parking);
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.parking.rent", null);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on parking rent: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on parking rent", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}	

		renterDomain = null;	
	}

	public void onParkingUnrent(Parking parking) {
		try {
			ParkingServiceUtil.unrent(parking.getId());
			grantedParkings.remove(parking);
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.parking.unrent", null);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on parking unrent: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on parking unrent", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}	
	}

	public int displayTotalParkings() {
		return ownedParkings.size() + rentedParkings.size() - grantedParkings.size();
	}

	public ModelDataModel<Parking> getModel() {
		return model;
	}

	public Parking getParking() {
		return parking;
	}

	public void setParking(Parking parking) {
		this.parking = parking;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Domain getRenterDomain() {
		return renterDomain;
	}

	public void setRenterDomain(Domain renterDomain) {
		this.renterDomain = renterDomain;
	}

	public List<Parking> getOwnedParkings() {
		return ownedParkings;
	}

	public void setOwnedParkings(List<Parking> ownedParkings) {
		this.ownedParkings = ownedParkings;
	}

	public List<Parking> getRentedParkings() {
		return rentedParkings;
	}

	public void setRentedParkings(List<Parking> rentedParkings) {
		this.rentedParkings = rentedParkings;
	}

	public List<Parking> getGrantedParkings() {
		return grantedParkings;
	}

	public void setGrantedParkings(List<Parking> grantedParkings) {
		this.grantedParkings = grantedParkings;
	}

}
