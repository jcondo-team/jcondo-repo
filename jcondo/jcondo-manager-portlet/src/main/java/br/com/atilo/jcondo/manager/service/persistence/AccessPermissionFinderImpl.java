package br.com.atilo.jcondo.manager.service.persistence;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;

import br.com.atilo.jcondo.manager.model.AccessPermission;
import br.com.atilo.jcondo.manager.model.impl.AccessPermissionImpl;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class AccessPermissionFinderImpl extends BasePersistenceImpl<AccessPermission> implements AccessPermissionFinder {

	public static final String FIND_ACCESS_PERMISSION = AccessPermissionFinder.class.getName() + ".findAccessPermission";
	
	@SuppressWarnings("unchecked")
	public List<AccessPermission> findAccessPermission(long personId, long domainId, Date date) {
		Session session = null;

		try {
			session = openSession();

			String sql = CustomSQLUtil.get(FIND_ACCESS_PERMISSION);

			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("jco_access_permission", AccessPermissionImpl.class);
			
	        QueryPos qPos = QueryPos.getInstance(q); 
	        qPos.add(personId);
	        qPos.add(domainId);
	        qPos.add(DateUtils.toCalendar(date).get(Calendar.DAY_OF_WEEK));
	        qPos.add(DateFormatUtils.format(date, "HH:mm"));
	        qPos.add(DateFormatUtils.format(date, "HH:mm"));
	        qPos.add(DateFormatUtils.format(date, "yyyy-MM-dd"));
	        qPos.add(DateFormatUtils.format(date, "yyyy-MM-dd"));

			return (List<AccessPermission>) QueryUtil.list(q, getDialect(), -1, -1);
		} catch (Exception e) {
			try {
				throw new SystemException(e);
			} catch (SystemException se) {
				se.printStackTrace();
			}
		} finally {
			closeSession(session);
		}

		return null;
	}
}
