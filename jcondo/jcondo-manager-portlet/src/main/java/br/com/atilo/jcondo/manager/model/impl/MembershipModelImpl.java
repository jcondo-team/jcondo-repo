package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.MembershipModel;
import br.com.atilo.jcondo.manager.model.MembershipSoap;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.json.JSON;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;
import com.liferay.portlet.expando.util.ExpandoBridgeFactoryUtil;

import java.io.Serializable;

import java.sql.Types;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The base model implementation for the Membership service. Represents a row in the &quot;jco_membership&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This implementation and its corresponding interface {@link br.com.atilo.jcondo.manager.model.MembershipModel} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link MembershipImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MembershipImpl
 * @see br.com.atilo.jcondo.manager.model.Membership
 * @see br.com.atilo.jcondo.manager.model.MembershipModel
 * @generated
 */
@JSON(strict = true)
public class MembershipModelImpl extends BaseModelImpl<Membership>
    implements MembershipModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a membership model instance should use the {@link br.com.atilo.jcondo.manager.model.Membership} interface instead.
     */
    public static final String TABLE_NAME = "jco_membership";
    public static final Object[][] TABLE_COLUMNS = {
            { "membershipId", Types.BIGINT },
            { "personId", Types.BIGINT },
            { "domainId", Types.BIGINT },
            { "typeId", Types.INTEGER },
            { "oprDate", Types.TIMESTAMP },
            { "oprUser", Types.BIGINT }
        };
    public static final String TABLE_SQL_CREATE = "create table jco_membership (membershipId LONG not null primary key,personId LONG,domainId LONG,typeId INTEGER,oprDate DATE null,oprUser LONG)";
    public static final String TABLE_SQL_DROP = "drop table jco_membership";
    public static final String ORDER_BY_JPQL = " ORDER BY membership.id ASC";
    public static final String ORDER_BY_SQL = " ORDER BY jco_membership.membershipId ASC";
    public static final String DATA_SOURCE = "liferayDataSource";
    public static final String SESSION_FACTORY = "liferaySessionFactory";
    public static final String TX_MANAGER = "liferayTransactionManager";
    public static final boolean ENTITY_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.entity.cache.enabled.br.com.atilo.jcondo.manager.model.Membership"),
            true);
    public static final boolean FINDER_CACHE_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.finder.cache.enabled.br.com.atilo.jcondo.manager.model.Membership"),
            true);
    public static final boolean COLUMN_BITMASK_ENABLED = GetterUtil.getBoolean(com.liferay.util.service.ServiceProps.get(
                "value.object.column.bitmask.enabled.br.com.atilo.jcondo.manager.model.Membership"),
            true);
    public static long DOMAINID_COLUMN_BITMASK = 1L;
    public static long PERSONID_COLUMN_BITMASK = 2L;
    public static long TYPEID_COLUMN_BITMASK = 4L;
    public static long ID_COLUMN_BITMASK = 8L;
    public static final long LOCK_EXPIRATION_TIME = GetterUtil.getLong(com.liferay.util.service.ServiceProps.get(
                "lock.expiration.time.br.com.atilo.jcondo.manager.model.Membership"));
    private static ClassLoader _classLoader = Membership.class.getClassLoader();
    private static Class<?>[] _escapedModelInterfaces = new Class[] {
            Membership.class
        };
    private long _id;
    private long _personId;
    private long _originalPersonId;
    private boolean _setOriginalPersonId;
    private long _domainId;
    private long _originalDomainId;
    private boolean _setOriginalDomainId;
    private int _typeId;
    private int _originalTypeId;
    private boolean _setOriginalTypeId;
    private Date _oprDate;
    private long _oprUser;
    private long _columnBitmask;
    private Membership _escapedModel;

    public MembershipModelImpl() {
    }

    /**
     * Converts the soap model instance into a normal model instance.
     *
     * @param soapModel the soap model instance to convert
     * @return the normal model instance
     */
    public static Membership toModel(MembershipSoap soapModel) {
        if (soapModel == null) {
            return null;
        }

        Membership model = new MembershipImpl();

        model.setId(soapModel.getId());
        model.setPersonId(soapModel.getPersonId());
        model.setDomainId(soapModel.getDomainId());
        model.setTypeId(soapModel.getTypeId());
        model.setOprDate(soapModel.getOprDate());
        model.setOprUser(soapModel.getOprUser());

        return model;
    }

    /**
     * Converts the soap model instances into normal model instances.
     *
     * @param soapModels the soap model instances to convert
     * @return the normal model instances
     */
    public static List<Membership> toModels(MembershipSoap[] soapModels) {
        if (soapModels == null) {
            return null;
        }

        List<Membership> models = new ArrayList<Membership>(soapModels.length);

        for (MembershipSoap soapModel : soapModels) {
            models.add(toModel(soapModel));
        }

        return models;
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Class<?> getModelClass() {
        return Membership.class;
    }

    @Override
    public String getModelClassName() {
        return Membership.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("personId", getPersonId());
        attributes.put("domainId", getDomainId());
        attributes.put("typeId", getTypeId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @JSON
    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;
    }

    @JSON
    @Override
    public long getPersonId() {
        return _personId;
    }

    @Override
    public void setPersonId(long personId) {
        _columnBitmask |= PERSONID_COLUMN_BITMASK;

        if (!_setOriginalPersonId) {
            _setOriginalPersonId = true;

            _originalPersonId = _personId;
        }

        _personId = personId;
    }

    public long getOriginalPersonId() {
        return _originalPersonId;
    }

    @JSON
    @Override
    public long getDomainId() {
        return _domainId;
    }

    @Override
    public void setDomainId(long domainId) {
        _columnBitmask |= DOMAINID_COLUMN_BITMASK;

        if (!_setOriginalDomainId) {
            _setOriginalDomainId = true;

            _originalDomainId = _domainId;
        }

        _domainId = domainId;
    }

    public long getOriginalDomainId() {
        return _originalDomainId;
    }

    @JSON
    @Override
    public int getTypeId() {
        return _typeId;
    }

    @Override
    public void setTypeId(int typeId) {
        _columnBitmask |= TYPEID_COLUMN_BITMASK;

        if (!_setOriginalTypeId) {
            _setOriginalTypeId = true;

            _originalTypeId = _typeId;
        }

        _typeId = typeId;
    }

    public int getOriginalTypeId() {
        return _originalTypeId;
    }

    @JSON
    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    @JSON
    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }

    public long getColumnBitmask() {
        return _columnBitmask;
    }

    @Override
    public ExpandoBridge getExpandoBridge() {
        return ExpandoBridgeFactoryUtil.getExpandoBridge(0,
            Membership.class.getName(), getPrimaryKey());
    }

    @Override
    public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
        ExpandoBridge expandoBridge = getExpandoBridge();

        expandoBridge.setAttributes(serviceContext);
    }

    @Override
    public Membership toEscapedModel() {
        if (_escapedModel == null) {
            _escapedModel = (Membership) ProxyUtil.newProxyInstance(_classLoader,
                    _escapedModelInterfaces, new AutoEscapeBeanHandler(this));
        }

        return _escapedModel;
    }

    @Override
    public Object clone() {
        MembershipImpl membershipImpl = new MembershipImpl();

        membershipImpl.setId(getId());
        membershipImpl.setPersonId(getPersonId());
        membershipImpl.setDomainId(getDomainId());
        membershipImpl.setTypeId(getTypeId());
        membershipImpl.setOprDate(getOprDate());
        membershipImpl.setOprUser(getOprUser());

        membershipImpl.resetOriginalValues();

        return membershipImpl;
    }

    @Override
    public int compareTo(Membership membership) {
        long primaryKey = membership.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof Membership)) {
            return false;
        }

        Membership membership = (Membership) obj;

        long primaryKey = membership.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public void resetOriginalValues() {
        MembershipModelImpl membershipModelImpl = this;

        membershipModelImpl._originalPersonId = membershipModelImpl._personId;

        membershipModelImpl._setOriginalPersonId = false;

        membershipModelImpl._originalDomainId = membershipModelImpl._domainId;

        membershipModelImpl._setOriginalDomainId = false;

        membershipModelImpl._originalTypeId = membershipModelImpl._typeId;

        membershipModelImpl._setOriginalTypeId = false;

        membershipModelImpl._columnBitmask = 0;
    }

    @Override
    public CacheModel<Membership> toCacheModel() {
        MembershipCacheModel membershipCacheModel = new MembershipCacheModel();

        membershipCacheModel.id = getId();

        membershipCacheModel.personId = getPersonId();

        membershipCacheModel.domainId = getDomainId();

        membershipCacheModel.typeId = getTypeId();

        Date oprDate = getOprDate();

        if (oprDate != null) {
            membershipCacheModel.oprDate = oprDate.getTime();
        } else {
            membershipCacheModel.oprDate = Long.MIN_VALUE;
        }

        membershipCacheModel.oprUser = getOprUser();

        return membershipCacheModel;
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", personId=");
        sb.append(getPersonId());
        sb.append(", domainId=");
        sb.append(getDomainId());
        sb.append(", typeId=");
        sb.append(getTypeId());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(22);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Membership");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>personId</column-name><column-value><![CDATA[");
        sb.append(getPersonId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>domainId</column-name><column-value><![CDATA[");
        sb.append(getDomainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>typeId</column-name><column-value><![CDATA[");
        sb.append(getTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
