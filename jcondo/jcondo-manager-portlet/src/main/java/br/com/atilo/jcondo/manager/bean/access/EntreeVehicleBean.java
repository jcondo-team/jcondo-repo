package br.com.atilo.jcondo.manager.bean.access;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import com.liferay.util.portlet.PortletProps;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.datatype.VehicleType;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.VehicleServiceUtil;
import br.com.atilo.jcondo.manager.bean.CameraBean;

public class EntreeVehicleBean extends Observable implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(EntreeVehicleBean.class);

	private CameraBean cameraBean;

	private Vehicle vehicle;

	private List<VehicleType> types;

	private Image image;

	private String[] colors;

	private List<VehicleBrand> brands;

	private VehicleBrand brand;

	private String[] services;

	private String license;

	public EntreeVehicleBean() {
		types = Arrays.asList(VehicleType.CAR, VehicleType.MOTO);
		cameraBean = new CameraBean(400, 300);

		brands = new ArrayList<VehicleBrand>();
		for (String brand : PortletProps.getArray("vehicle.brands")) {
			brands.add(new VehicleBrand(brand, brand.concat(".png")));
		}

		colors = PortletProps.getArray("colors");
		services = PortletProps.getArray("vehicle.services");
	}

	public void onVehicleCreate() throws Exception {
		vehicle = VehicleServiceUtil.createVehicle();
		vehicle.setDomainId(0);
		image = null;
		brand = null;
		license = null;
	}

	public void onVehicleSave() {
		try {
			String msg;
			vehicle.setLicense(vehicle.getLicense().replaceAll("[^A-Za-z0-9]", ""));

			if (vehicle.getId() == 0) {
				vehicle = VehicleServiceUtil.addVehicle(vehicle.getLicense(), vehicle.getType(), vehicle.getDomainId(), 
														image, brand != null ? brand.getName() : null, vehicle.getColor(),
														vehicle.getRemark());
				msg = "msg.vehicle.add";
			} else {
				vehicle = VehicleServiceUtil.updateVehicle(vehicle.getId(), vehicle.getType(), vehicle.getDomainId(), 
														   image, brand != null ? brand.getName() : null, vehicle.getColor(), 
														   vehicle.getRemark());
				msg = "msg.vehicle.update";
			}

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, msg, null);

			setChanged();
			notifyObservers(vehicle);
		} catch (BusinessException e) {
			LOGGER.warn("");
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on entree vehicle save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "general.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onSearchByLicense() {
		try {
			vehicle = VehicleLocalServiceUtil.getVehicleByLicense(license);
			for (VehicleBrand b : brands) {
				if (b.getName().equalsIgnoreCase(vehicle.getBrand())) {
					brand = b;
					break;
				}
			}
			image = vehicle.getPortrait();
		} catch (Exception e) {
			vehicle.setLicense(license);
		}
	}
	
	public CameraBean getCameraBean() {
		return cameraBean;
	}

	public void setCameraBean(CameraBean cameraBean) {
		this.cameraBean = cameraBean;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public List<VehicleType> getTypes() {
		return types;
	}

	public void setTypes(List<VehicleType> types) {
		this.types = types;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String[] getColors() {
		return colors;
	}

	public void setColors(String[] colors) {
		this.colors = colors;
	}

	public List<VehicleBrand> getBrands() {
		return brands;
	}

	public void setBrands(List<VehicleBrand> vehicles) {
		this.brands = vehicles;
	}

	public VehicleBrand getBrand() {
		return brand;
	}

	public void setBrand(VehicleBrand brand) {
		this.brand = brand;
	}

	public String[] getServices() {
		return services;
	}

	public void setServices(String[] services) {
		this.services = services;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

}