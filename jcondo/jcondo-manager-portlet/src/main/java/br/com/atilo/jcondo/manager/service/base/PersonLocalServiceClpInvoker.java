package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class PersonLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName120;
    private String[] _methodParameterTypes120;
    private String _methodName121;
    private String[] _methodParameterTypes121;
    private String _methodName127;
    private String[] _methodParameterTypes127;
    private String _methodName128;
    private String[] _methodParameterTypes128;
    private String _methodName129;
    private String[] _methodParameterTypes129;
    private String _methodName130;
    private String[] _methodParameterTypes130;
    private String _methodName131;
    private String[] _methodParameterTypes131;
    private String _methodName132;
    private String[] _methodParameterTypes132;
    private String _methodName133;
    private String[] _methodParameterTypes133;
    private String _methodName134;
    private String[] _methodParameterTypes134;
    private String _methodName135;
    private String[] _methodParameterTypes135;
    private String _methodName136;
    private String[] _methodParameterTypes136;
    private String _methodName137;
    private String[] _methodParameterTypes137;
    private String _methodName138;
    private String[] _methodParameterTypes138;
    private String _methodName139;
    private String[] _methodParameterTypes139;
    private String _methodName140;
    private String[] _methodParameterTypes140;
    private String _methodName141;
    private String[] _methodParameterTypes141;
    private String _methodName142;
    private String[] _methodParameterTypes142;

    public PersonLocalServiceClpInvoker() {
        _methodName0 = "addPerson";

        _methodParameterTypes0 = new String[] {
                "br.com.atilo.jcondo.manager.model.Person"
            };

        _methodName1 = "createPerson";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deletePerson";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deletePerson";

        _methodParameterTypes3 = new String[] {
                "br.com.atilo.jcondo.manager.model.Person"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchPerson";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getPerson";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getPersons";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getPersonsCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updatePerson";

        _methodParameterTypes15 = new String[] {
                "br.com.atilo.jcondo.manager.model.Person"
            };

        _methodName120 = "getBeanIdentifier";

        _methodParameterTypes120 = new String[] {  };

        _methodName121 = "setBeanIdentifier";

        _methodParameterTypes121 = new String[] { "java.lang.String" };

        _methodName127 = "createPerson";

        _methodParameterTypes127 = new String[] {  };

        _methodName128 = "addPerson";

        _methodParameterTypes128 = new String[] {
                "java.lang.String", "java.lang.String", "java.lang.String",
                "java.lang.String", "java.util.Date",
                "br.com.atilo.jcondo.datatype.Gender",
                "br.com.atilo.jcondo.datatype.PersonType", "java.lang.String",
                "long"
            };

        _methodName129 = "updatePerson";

        _methodParameterTypes129 = new String[] {
                "long", "java.lang.String", "java.lang.String",
                "java.lang.String", "java.lang.String", "java.util.Date",
                "br.com.atilo.jcondo.datatype.Gender", "java.lang.String",
                "long", "br.com.atilo.jcondo.manager.model.Membership"
            };

        _methodName130 = "updatePortrait";

        _methodParameterTypes130 = new String[] {
                "long", "br.com.atilo.jcondo.Image"
            };

        _methodName131 = "updatePassword";

        _methodParameterTypes131 = new String[] {
                "long", "java.lang.String", "java.lang.String"
            };

        _methodName132 = "updateHome";

        _methodParameterTypes132 = new String[] { "long", "long" };

        _methodName133 = "getDomainPeopleByType";

        _methodParameterTypes133 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.PersonType"
            };

        _methodName134 = "getPerson";

        _methodParameterTypes134 = new String[] { "long" };

        _methodName135 = "getPerson";

        _methodParameterTypes135 = new String[] {  };

        _methodName136 = "getPersonByIdentity";

        _methodParameterTypes136 = new String[] { "java.lang.String" };

        _methodName137 = "getPeopleByType";

        _methodParameterTypes137 = new String[] {
                "br.com.atilo.jcondo.datatype.PersonType"
            };

        _methodName138 = "getDomainPeople";

        _methodParameterTypes138 = new String[] { "long" };

        _methodName139 = "getDomainPeopleByName";

        _methodParameterTypes139 = new String[] { "long", "java.lang.String" };

        _methodName140 = "getPeople";

        _methodParameterTypes140 = new String[] {
                "java.lang.String", "java.lang.String", "long"
            };

        _methodName141 = "completePersonRegistration";

        _methodParameterTypes141 = new String[] {
                "long", "java.lang.String", "boolean", "boolean"
            };

        _methodName142 = "authenticatePerson";

        _methodParameterTypes142 = new String[] { "long", "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return PersonLocalServiceUtil.addPerson((br.com.atilo.jcondo.manager.model.Person) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return PersonLocalServiceUtil.createPerson(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return PersonLocalServiceUtil.deletePerson(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return PersonLocalServiceUtil.deletePerson((br.com.atilo.jcondo.manager.model.Person) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return PersonLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return PersonLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return PersonLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return PersonLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return PersonLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return PersonLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return PersonLocalServiceUtil.fetchPerson(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return PersonLocalServiceUtil.getPerson(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return PersonLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return PersonLocalServiceUtil.getPersons(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return PersonLocalServiceUtil.getPersonsCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return PersonLocalServiceUtil.updatePerson((br.com.atilo.jcondo.manager.model.Person) arguments[0]);
        }

        if (_methodName120.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes120, parameterTypes)) {
            return PersonLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName121.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes121, parameterTypes)) {
            PersonLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName127.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes127, parameterTypes)) {
            return PersonLocalServiceUtil.createPerson();
        }

        if (_methodName128.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes128, parameterTypes)) {
            return PersonLocalServiceUtil.addPerson((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                (java.lang.String) arguments[3], (java.util.Date) arguments[4],
                (br.com.atilo.jcondo.datatype.Gender) arguments[5],
                (br.com.atilo.jcondo.datatype.PersonType) arguments[6],
                (java.lang.String) arguments[7],
                ((Long) arguments[8]).longValue());
        }

        if (_methodName129.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes129, parameterTypes)) {
            return PersonLocalServiceUtil.updatePerson(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                (java.lang.String) arguments[3],
                (java.lang.String) arguments[4], (java.util.Date) arguments[5],
                (br.com.atilo.jcondo.datatype.Gender) arguments[6],
                (java.lang.String) arguments[7],
                ((Long) arguments[8]).longValue(),
                (br.com.atilo.jcondo.manager.model.Membership) arguments[9]);
        }

        if (_methodName130.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes130, parameterTypes)) {
            return PersonLocalServiceUtil.updatePortrait(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[1]);
        }

        if (_methodName131.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes131, parameterTypes)) {
            PersonLocalServiceUtil.updatePassword(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1], (java.lang.String) arguments[2]);

            return null;
        }

        if (_methodName132.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes132, parameterTypes)) {
            return PersonLocalServiceUtil.updateHome(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        if (_methodName133.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes133, parameterTypes)) {
            return PersonLocalServiceUtil.getDomainPeopleByType(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.PersonType) arguments[1]);
        }

        if (_methodName134.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes134, parameterTypes)) {
            return PersonLocalServiceUtil.getPerson(((Long) arguments[0]).longValue());
        }

        if (_methodName135.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes135, parameterTypes)) {
            return PersonLocalServiceUtil.getPerson();
        }

        if (_methodName136.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes136, parameterTypes)) {
            return PersonLocalServiceUtil.getPersonByIdentity((java.lang.String) arguments[0]);
        }

        if (_methodName137.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes137, parameterTypes)) {
            return PersonLocalServiceUtil.getPeopleByType((br.com.atilo.jcondo.datatype.PersonType) arguments[0]);
        }

        if (_methodName138.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes138, parameterTypes)) {
            return PersonLocalServiceUtil.getDomainPeople(((Long) arguments[0]).longValue());
        }

        if (_methodName139.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes139, parameterTypes)) {
            return PersonLocalServiceUtil.getDomainPeopleByName(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName140.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes140, parameterTypes)) {
            return PersonLocalServiceUtil.getPeople((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                ((Long) arguments[2]).longValue());
        }

        if (_methodName141.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes141, parameterTypes)) {
            PersonLocalServiceUtil.completePersonRegistration(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                ((Boolean) arguments[2]).booleanValue(),
                ((Boolean) arguments[3]).booleanValue());

            return null;
        }

        if (_methodName142.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes142, parameterTypes)) {
            return PersonLocalServiceUtil.authenticatePerson(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        throw new UnsupportedOperationException();
    }
}
