package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

import javax.faces.application.FacesMessage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.model.Telephone;
import br.com.atilo.jcondo.datatype.PhoneType;
import br.com.atilo.jcondo.manager.service.TelephoneLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.TelephoneServiceUtil;

public class PersonPhoneBean extends Observable implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(PersonPhoneBean.class);

	private List<Telephone> phones;

	private List<PhoneType> phoneTypes;

	private Person person;

	private Telephone phone;

	private String phoneNumber;

	public PersonPhoneBean() {
		phoneTypes = Arrays.asList(PhoneType.values());
	}

	public void init(Person person) throws Exception {
		this.person = person;
		if (person.isNew()) {
			this.phones = new ArrayList<Telephone>();
		} else {
			this.phones = TelephoneLocalServiceUtil.getPersonPhones(person.getId());
		}
		onPhoneCreate();
	}

	public void onPhoneCreate() {
		phone = TelephoneLocalServiceUtil.createTelephone(0);
		phoneNumber = null;
	}

	public void onPhoneAdd() {
		String pn = phoneNumber.replaceAll("[^0-9]*", "");
		phone.setExtension(Long.valueOf(StringUtils.left(pn, 2)));
		phone.setNumber(Long.valueOf(StringUtils.right(pn, pn.length() - 2)));
		phones.add(phone);
	}

	public void onPhoneDelete(Telephone phone) {
		try {
			phones.remove(phone);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on phone remove", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}	

	@SuppressWarnings("unchecked")
	public void onPhoneSave() {
		try {
			List<Telephone> currentPhones = TelephoneServiceUtil.getPersonPhones(person.getId());
			List<Telephone> addedPhones = (List<Telephone>) CollectionUtils.subtract(phones, currentPhones);
			List<Telephone> removedPhones = (List<Telephone>) CollectionUtils.subtract(currentPhones, phones);

			for (Telephone phone : addedPhones) {
				TelephoneServiceUtil.addPersonPhone(phone.getNumber(), phone.getExtension(), 
													phone.getType(), phone.isPrimary(), person.getId());
			}

			for (Telephone phone : removedPhones) {
				TelephoneServiceUtil.deletePhone(phone.getId());
			}

		} catch (BusinessException e) {
			LOGGER.warn("Business failure on phone add: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on phone add", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public List<Telephone> getPhones() {
		return phones;
	}

	public void setPhones(List<Telephone> phones) {
		this.phones = phones;
	}

	public List<PhoneType> getPhoneTypes() {
		return phoneTypes;
	}

	public void setPhoneTypes(List<PhoneType> phoneTypes) {
		this.phoneTypes = phoneTypes;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Telephone getPhone() {
		return phone;
	}

	public void setPhone(Telephone phone) {
		this.phone = phone;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}
}
