/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.ListType;
import com.liferay.portal.model.ListTypeConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.model.Phone;
import com.liferay.portal.service.ListTypeServiceUtil;
import com.liferay.portal.service.PhoneLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.NoSuchDepartmentException;
import br.com.atilo.jcondo.manager.NoSuchDomainException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.model.Telephone;
import br.com.atilo.jcondo.datatype.PhoneType;
import br.com.atilo.jcondo.manager.model.impl.TelephoneImpl;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.base.TelephoneLocalServiceBaseImpl;

/**
 * The implementation of the telephone local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.TelephoneLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.TelephoneLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.TelephoneLocalServiceUtil
 */
public class TelephoneLocalServiceImpl extends TelephoneLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.TelephoneLocalServiceUtil} to access the telephone local service.
	 */
	public Telephone addDomainPhone(long domainId, long number, long extension, boolean primary) throws PortalException, SystemException {
		long organizationId;

		try {
			organizationId = enterpriseLocalService.getEnterprise(domainId).getOrganizationId();
		} catch (NoSuchEnterpriseException e) {
			try {
				organizationId = departmentLocalService.getDepartment(domainId).getOrganizationId();
			} catch (NoSuchDepartmentException ex) {
				throw new NoSuchDomainException();
			}
		}

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Phone phone = PhoneLocalServiceUtil.addPhone(sc.getUserId(), Organization.class.getName(), 
													 organizationId, String.valueOf(number), 
											  		 String.valueOf(extension), 12009, primary);

		return new TelephoneImpl(phone.getPhoneId(), PhoneType.OTHER, extension, number, primary);
	}	

	public Telephone addPersonPhone(long number, long extension, PhoneType type, boolean primary, long personId) throws PortalException, SystemException {
		Person person = personLocalService.getPerson(personId);
		int typeId = 0;

		for (ListType listType : ListTypeServiceUtil.getListTypes(ListTypeConstants.CONTACT_PHONE)) {
			if (type == PhoneType.parse(listType.getName())) {
				typeId = listType.getListTypeId();
				break;
			}
		}

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Phone phone = PhoneLocalServiceUtil.addPhone(sc.getUserId(), Contact.class.getName(), 
											  		 person.getUser().getContactId(), String.valueOf(number), 
											  		 String.valueOf(extension), typeId, primary);

		return new TelephoneImpl(phone.getPhoneId(), type, extension, number, primary);
	}

	public Telephone updateEnterprisePhone(long phoneId, long number, long extension, boolean primary) throws PortalException, SystemException {
		Phone phone = PhoneLocalServiceUtil.updatePhone(phoneId, String.valueOf(number),	
														String.valueOf(extension), 12009, primary);

		return new TelephoneImpl(phone.getPhoneId(), PhoneType.OTHER, extension, number, primary);
	}
	
	public Telephone updatePersonPhone(long phoneId, long number, long extension, PhoneType type, boolean primary) throws PortalException, SystemException {
		int typeId = 0;

		for (ListType listType : ListTypeServiceUtil.getListTypes(ListTypeConstants.CONTACT_PHONE)) {
			if (type == PhoneType.parse(listType.getName())) {
				typeId = listType.getListTypeId();
				break;
			}
		}

		Phone phone = PhoneLocalServiceUtil.updatePhone(phoneId, String.valueOf(number), 
														String.valueOf(extension), typeId, primary);
		
		return new TelephoneImpl(phone.getPhoneId(), type, extension, number, primary);
	}

	public Telephone deletePhone(long phoneId) throws PortalException, SystemException {
		Phone phone = PhoneLocalServiceUtil.deletePhone(phoneId);
		return new TelephoneImpl(phone.getPhoneId(), PhoneType.parse(phone.getType().getName()), 
								 Long.valueOf(phone.getExtension()), Long.valueOf(phone.getNumber()), 
								 phone.isPrimary());
	}

	public List<Telephone> getPersonPhones(long personId) throws PortalException, SystemException {
		return getPersonPhones(personId, null);
	}
	
	public List<Telephone> getPersonPhones(long personId, PhoneType type) throws PortalException, SystemException {
		List<Telephone> telephones = new ArrayList<Telephone>();
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Person person = personLocalService.getPerson(personId);
		List<Phone> phones = PhoneLocalServiceUtil.getPhones(sc.getCompanyId(), 
															 Contact.class.getName(), 
															 person.getUser().getContactId());
		for (Phone phone : phones) {
			if (type == null || PhoneType.parse(phone.getType().getName()) == type) {
				telephones.add(new TelephoneImpl(phone.getPhoneId(), PhoneType.parse(phone.getType().getName()), 
								 			 	 Long.valueOf(phone.getExtension()), Long.valueOf(phone.getNumber()), 
								 			 	 phone.isPrimary()));
			}
		}

		return telephones; 
	}

	public Telephone getPersonPhone(long personId) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Person person = personLocalService.getPerson(personId);
		List<Phone> phones = PhoneLocalServiceUtil.getPhones(sc.getCompanyId(), 
															 Contact.class.getName(), 
															 person.getUser().getContactId());
		for (Phone phone : phones) {
			if (phone.isPrimary()) {
				return new TelephoneImpl(phone.getPhoneId(), PhoneType.parse(phone.getType().getName()), 
									 Long.valueOf(phone.getExtension()), Long.valueOf(phone.getNumber()), 
									 phone.isPrimary());
			}
		}

		return null;
	}
	
	public List<Telephone> getDomainPhones(long domainId) throws PortalException, SystemException {
		List<Telephone> telephones = new ArrayList<Telephone>();
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		long organizationId;
		
		try {
			organizationId = FlatLocalServiceUtil.getFlat(domainId).getOrganizationId();
		} catch (NoSuchFlatException e) {
			try {
				organizationId = EnterpriseLocalServiceUtil.getEnterprise(domainId).getOrganizationId();
			} catch (NoSuchEnterpriseException e2) {
				try {
					organizationId = DepartmentLocalServiceUtil.getDepartment(domainId).getOrganizationId();
				} catch (NoSuchDepartmentException e3) {
					throw new NoSuchDomainException();
				}
			}
		}	
		
		List<Phone> phones = PhoneLocalServiceUtil.getPhones(sc.getCompanyId(), 
															 Organization.class.getName(), 
															 organizationId);
		for (Phone phone : phones) {
			telephones.add(new TelephoneImpl(phone.getPhoneId(), PhoneType.OTHER, 
							 			 	 Long.valueOf(phone.getExtension()), Long.valueOf(phone.getNumber()), 
							 			 	 phone.isPrimary()));
		}

		return telephones; 
	}

}