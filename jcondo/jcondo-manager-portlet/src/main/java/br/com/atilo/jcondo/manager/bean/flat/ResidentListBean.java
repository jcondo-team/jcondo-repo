package br.com.atilo.jcondo.manager.bean.flat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;

import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.bean.AbstractPersonListBean;
import br.com.atilo.jcondo.manager.bean.access.AccessBean;

public class ResidentListBean extends AbstractPersonListBean<Flat> {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(ResidentListBean.class);

	private AccessBean accessBean;

	public ResidentListBean() {
		accessBean = new AccessBean();
	}

	public void init(Flat flat) {
		try {
			super.init(flat);
			personBean = new ResidentBean();
			personBean.init(flat);
			personBean.addObserver(this);

			personTypesFilter = new ArrayList<PersonType>(Arrays.asList(PersonType.FLAT_TYPES));

			selectedPersonTypes = new PersonType[personTypesFilter.size()];
			personTypesFilter.toArray(selectedPersonTypes);
		} catch (Exception e) {
			LOGGER.fatal("Failure on resident initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public void onAccessEdit() {
		accessBean.init(model.getRowData(), domain);
	}
	
	@Override
	public void update(Observable observable, Object object) {
		super.update(observable, object);

		if (isVisitor((Person) object)) {
			accessBean.init((Person) object, domain);
		}	
	}
	
	public AccessBean getAccessBean() {
		return accessBean;
	}

	public void setAccessBean(AccessBean accessBean) {
		this.accessBean = accessBean;
	}

}
