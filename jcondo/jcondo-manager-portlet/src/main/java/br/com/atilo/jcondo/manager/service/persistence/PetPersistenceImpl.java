package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchPetException;
import br.com.atilo.jcondo.manager.model.Pet;
import br.com.atilo.jcondo.manager.model.impl.PetImpl;
import br.com.atilo.jcondo.manager.model.impl.PetModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.PetPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the pet service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PetPersistence
 * @see PetUtil
 * @generated
 */
public class PetPersistenceImpl extends BasePersistenceImpl<Pet>
    implements PetPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PetUtil} to access the pet persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PetImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetModelImpl.FINDER_CACHE_ENABLED, PetImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetModelImpl.FINDER_CACHE_ENABLED, PetImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FLAT = new FinderPath(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetModelImpl.FINDER_CACHE_ENABLED, PetImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByFlat",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FLAT = new FinderPath(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetModelImpl.FINDER_CACHE_ENABLED, PetImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByFlat",
            new String[] { Long.class.getName() },
            PetModelImpl.FLATID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_FLAT = new FinderPath(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByFlat",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_FLAT_FLATID_2 = "pet.flatId = ?";
    public static final FinderPath FINDER_PATH_FETCH_BY_TYPEANDFLAT = new FinderPath(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetModelImpl.FINDER_CACHE_ENABLED, PetImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByTypeAndFlat",
            new String[] { Integer.class.getName(), Long.class.getName() },
            PetModelImpl.TYPEID_COLUMN_BITMASK |
            PetModelImpl.FLATID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TYPEANDFLAT = new FinderPath(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByTypeAndFlat",
            new String[] { Integer.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_TYPEANDFLAT_TYPEID_2 = "pet.typeId = ? AND ";
    private static final String _FINDER_COLUMN_TYPEANDFLAT_FLATID_2 = "pet.flatId = ?";
    private static final String _SQL_SELECT_PET = "SELECT pet FROM Pet pet";
    private static final String _SQL_SELECT_PET_WHERE = "SELECT pet FROM Pet pet WHERE ";
    private static final String _SQL_COUNT_PET = "SELECT COUNT(pet) FROM Pet pet";
    private static final String _SQL_COUNT_PET_WHERE = "SELECT COUNT(pet) FROM Pet pet WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "pet.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Pet exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Pet exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PetPersistenceImpl.class);
    private static Pet _nullPet = new PetImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Pet> toCacheModel() {
                return _nullPetCacheModel;
            }
        };

    private static CacheModel<Pet> _nullPetCacheModel = new CacheModel<Pet>() {
            @Override
            public Pet toEntityModel() {
                return _nullPet;
            }
        };

    public PetPersistenceImpl() {
        setModelClass(Pet.class);
    }

    /**
     * Returns all the pets where flatId = &#63;.
     *
     * @param flatId the flat ID
     * @return the matching pets
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pet> findByFlat(long flatId) throws SystemException {
        return findByFlat(flatId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the pets where flatId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param flatId the flat ID
     * @param start the lower bound of the range of pets
     * @param end the upper bound of the range of pets (not inclusive)
     * @return the range of matching pets
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pet> findByFlat(long flatId, int start, int end)
        throws SystemException {
        return findByFlat(flatId, start, end, null);
    }

    /**
     * Returns an ordered range of all the pets where flatId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param flatId the flat ID
     * @param start the lower bound of the range of pets
     * @param end the upper bound of the range of pets (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching pets
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pet> findByFlat(long flatId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FLAT;
            finderArgs = new Object[] { flatId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FLAT;
            finderArgs = new Object[] { flatId, start, end, orderByComparator };
        }

        List<Pet> list = (List<Pet>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Pet pet : list) {
                if ((flatId != pet.getFlatId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PET_WHERE);

            query.append(_FINDER_COLUMN_FLAT_FLATID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PetModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(flatId);

                if (!pagination) {
                    list = (List<Pet>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Pet>(list);
                } else {
                    list = (List<Pet>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first pet in the ordered set where flatId = &#63;.
     *
     * @param flatId the flat ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching pet
     * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a matching pet could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet findByFlat_First(long flatId, OrderByComparator orderByComparator)
        throws NoSuchPetException, SystemException {
        Pet pet = fetchByFlat_First(flatId, orderByComparator);

        if (pet != null) {
            return pet;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("flatId=");
        msg.append(flatId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPetException(msg.toString());
    }

    /**
     * Returns the first pet in the ordered set where flatId = &#63;.
     *
     * @param flatId the flat ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching pet, or <code>null</code> if a matching pet could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet fetchByFlat_First(long flatId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Pet> list = findByFlat(flatId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last pet in the ordered set where flatId = &#63;.
     *
     * @param flatId the flat ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching pet
     * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a matching pet could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet findByFlat_Last(long flatId, OrderByComparator orderByComparator)
        throws NoSuchPetException, SystemException {
        Pet pet = fetchByFlat_Last(flatId, orderByComparator);

        if (pet != null) {
            return pet;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("flatId=");
        msg.append(flatId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPetException(msg.toString());
    }

    /**
     * Returns the last pet in the ordered set where flatId = &#63;.
     *
     * @param flatId the flat ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching pet, or <code>null</code> if a matching pet could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet fetchByFlat_Last(long flatId, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByFlat(flatId);

        if (count == 0) {
            return null;
        }

        List<Pet> list = findByFlat(flatId, count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the pets before and after the current pet in the ordered set where flatId = &#63;.
     *
     * @param petId the primary key of the current pet
     * @param flatId the flat ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next pet
     * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet[] findByFlat_PrevAndNext(long petId, long flatId,
        OrderByComparator orderByComparator)
        throws NoSuchPetException, SystemException {
        Pet pet = findByPrimaryKey(petId);

        Session session = null;

        try {
            session = openSession();

            Pet[] array = new PetImpl[3];

            array[0] = getByFlat_PrevAndNext(session, pet, flatId,
                    orderByComparator, true);

            array[1] = pet;

            array[2] = getByFlat_PrevAndNext(session, pet, flatId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Pet getByFlat_PrevAndNext(Session session, Pet pet, long flatId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PET_WHERE);

        query.append(_FINDER_COLUMN_FLAT_FLATID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PetModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(flatId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(pet);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Pet> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the pets where flatId = &#63; from the database.
     *
     * @param flatId the flat ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByFlat(long flatId) throws SystemException {
        for (Pet pet : findByFlat(flatId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
                null)) {
            remove(pet);
        }
    }

    /**
     * Returns the number of pets where flatId = &#63;.
     *
     * @param flatId the flat ID
     * @return the number of matching pets
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByFlat(long flatId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_FLAT;

        Object[] finderArgs = new Object[] { flatId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PET_WHERE);

            query.append(_FINDER_COLUMN_FLAT_FLATID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(flatId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the pet where typeId = &#63; and flatId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchPetException} if it could not be found.
     *
     * @param typeId the type ID
     * @param flatId the flat ID
     * @return the matching pet
     * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a matching pet could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet findByTypeAndFlat(int typeId, long flatId)
        throws NoSuchPetException, SystemException {
        Pet pet = fetchByTypeAndFlat(typeId, flatId);

        if (pet == null) {
            StringBundler msg = new StringBundler(6);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("typeId=");
            msg.append(typeId);

            msg.append(", flatId=");
            msg.append(flatId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchPetException(msg.toString());
        }

        return pet;
    }

    /**
     * Returns the pet where typeId = &#63; and flatId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param typeId the type ID
     * @param flatId the flat ID
     * @return the matching pet, or <code>null</code> if a matching pet could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet fetchByTypeAndFlat(int typeId, long flatId)
        throws SystemException {
        return fetchByTypeAndFlat(typeId, flatId, true);
    }

    /**
     * Returns the pet where typeId = &#63; and flatId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param typeId the type ID
     * @param flatId the flat ID
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching pet, or <code>null</code> if a matching pet could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet fetchByTypeAndFlat(int typeId, long flatId,
        boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] { typeId, flatId };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_TYPEANDFLAT,
                    finderArgs, this);
        }

        if (result instanceof Pet) {
            Pet pet = (Pet) result;

            if ((typeId != pet.getTypeId()) || (flatId != pet.getFlatId())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_SELECT_PET_WHERE);

            query.append(_FINDER_COLUMN_TYPEANDFLAT_TYPEID_2);

            query.append(_FINDER_COLUMN_TYPEANDFLAT_FLATID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                qPos.add(flatId);

                List<Pet> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEANDFLAT,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "PetPersistenceImpl.fetchByTypeAndFlat(int, long, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Pet pet = list.get(0);

                    result = pet;

                    cacheResult(pet);

                    if ((pet.getTypeId() != typeId) ||
                            (pet.getFlatId() != flatId)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEANDFLAT,
                            finderArgs, pet);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TYPEANDFLAT,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Pet) result;
        }
    }

    /**
     * Removes the pet where typeId = &#63; and flatId = &#63; from the database.
     *
     * @param typeId the type ID
     * @param flatId the flat ID
     * @return the pet that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet removeByTypeAndFlat(int typeId, long flatId)
        throws NoSuchPetException, SystemException {
        Pet pet = findByTypeAndFlat(typeId, flatId);

        return remove(pet);
    }

    /**
     * Returns the number of pets where typeId = &#63; and flatId = &#63;.
     *
     * @param typeId the type ID
     * @param flatId the flat ID
     * @return the number of matching pets
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByTypeAndFlat(int typeId, long flatId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TYPEANDFLAT;

        Object[] finderArgs = new Object[] { typeId, flatId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_PET_WHERE);

            query.append(_FINDER_COLUMN_TYPEANDFLAT_TYPEID_2);

            query.append(_FINDER_COLUMN_TYPEANDFLAT_FLATID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                qPos.add(flatId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the pet in the entity cache if it is enabled.
     *
     * @param pet the pet
     */
    @Override
    public void cacheResult(Pet pet) {
        EntityCacheUtil.putResult(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetImpl.class, pet.getPrimaryKey(), pet);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEANDFLAT,
            new Object[] { pet.getTypeId(), pet.getFlatId() }, pet);

        pet.resetOriginalValues();
    }

    /**
     * Caches the pets in the entity cache if it is enabled.
     *
     * @param pets the pets
     */
    @Override
    public void cacheResult(List<Pet> pets) {
        for (Pet pet : pets) {
            if (EntityCacheUtil.getResult(PetModelImpl.ENTITY_CACHE_ENABLED,
                        PetImpl.class, pet.getPrimaryKey()) == null) {
                cacheResult(pet);
            } else {
                pet.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all pets.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PetImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PetImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the pet.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Pet pet) {
        EntityCacheUtil.removeResult(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetImpl.class, pet.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(pet);
    }

    @Override
    public void clearCache(List<Pet> pets) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Pet pet : pets) {
            EntityCacheUtil.removeResult(PetModelImpl.ENTITY_CACHE_ENABLED,
                PetImpl.class, pet.getPrimaryKey());

            clearUniqueFindersCache(pet);
        }
    }

    protected void cacheUniqueFindersCache(Pet pet) {
        if (pet.isNew()) {
            Object[] args = new Object[] { pet.getTypeId(), pet.getFlatId() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TYPEANDFLAT, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEANDFLAT, args,
                pet);
        } else {
            PetModelImpl petModelImpl = (PetModelImpl) pet;

            if ((petModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_TYPEANDFLAT.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { pet.getTypeId(), pet.getFlatId() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TYPEANDFLAT,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TYPEANDFLAT,
                    args, pet);
            }
        }
    }

    protected void clearUniqueFindersCache(Pet pet) {
        PetModelImpl petModelImpl = (PetModelImpl) pet;

        Object[] args = new Object[] { pet.getTypeId(), pet.getFlatId() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPEANDFLAT, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TYPEANDFLAT, args);

        if ((petModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_TYPEANDFLAT.getColumnBitmask()) != 0) {
            args = new Object[] {
                    petModelImpl.getOriginalTypeId(),
                    petModelImpl.getOriginalFlatId()
                };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPEANDFLAT, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TYPEANDFLAT, args);
        }
    }

    /**
     * Creates a new pet with the primary key. Does not add the pet to the database.
     *
     * @param petId the primary key for the new pet
     * @return the new pet
     */
    @Override
    public Pet create(long petId) {
        Pet pet = new PetImpl();

        pet.setNew(true);
        pet.setPrimaryKey(petId);

        return pet;
    }

    /**
     * Removes the pet with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param petId the primary key of the pet
     * @return the pet that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet remove(long petId) throws NoSuchPetException, SystemException {
        return remove((Serializable) petId);
    }

    /**
     * Removes the pet with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the pet
     * @return the pet that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet remove(Serializable primaryKey)
        throws NoSuchPetException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Pet pet = (Pet) session.get(PetImpl.class, primaryKey);

            if (pet == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPetException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(pet);
        } catch (NoSuchPetException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Pet removeImpl(Pet pet) throws SystemException {
        pet = toUnwrappedModel(pet);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(pet)) {
                pet = (Pet) session.get(PetImpl.class, pet.getPrimaryKeyObj());
            }

            if (pet != null) {
                session.delete(pet);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (pet != null) {
            clearCache(pet);
        }

        return pet;
    }

    @Override
    public Pet updateImpl(br.com.atilo.jcondo.manager.model.Pet pet)
        throws SystemException {
        pet = toUnwrappedModel(pet);

        boolean isNew = pet.isNew();

        PetModelImpl petModelImpl = (PetModelImpl) pet;

        Session session = null;

        try {
            session = openSession();

            if (pet.isNew()) {
                session.save(pet);

                pet.setNew(false);
            } else {
                session.merge(pet);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !PetModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((petModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FLAT.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { petModelImpl.getOriginalFlatId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FLAT, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FLAT,
                    args);

                args = new Object[] { petModelImpl.getFlatId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_FLAT, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FLAT,
                    args);
            }
        }

        EntityCacheUtil.putResult(PetModelImpl.ENTITY_CACHE_ENABLED,
            PetImpl.class, pet.getPrimaryKey(), pet);

        clearUniqueFindersCache(pet);
        cacheUniqueFindersCache(pet);

        return pet;
    }

    protected Pet toUnwrappedModel(Pet pet) {
        if (pet instanceof PetImpl) {
            return pet;
        }

        PetImpl petImpl = new PetImpl();

        petImpl.setNew(pet.isNew());
        petImpl.setPrimaryKey(pet.getPrimaryKey());

        petImpl.setPetId(pet.getPetId());
        petImpl.setTypeId(pet.getTypeId());
        petImpl.setFlatId(pet.getFlatId());
        petImpl.setOprDate(pet.getOprDate());
        petImpl.setOprUser(pet.getOprUser());

        return petImpl;
    }

    /**
     * Returns the pet with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the pet
     * @return the pet
     * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet findByPrimaryKey(Serializable primaryKey)
        throws NoSuchPetException, SystemException {
        Pet pet = fetchByPrimaryKey(primaryKey);

        if (pet == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchPetException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return pet;
    }

    /**
     * Returns the pet with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchPetException} if it could not be found.
     *
     * @param petId the primary key of the pet
     * @return the pet
     * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet findByPrimaryKey(long petId)
        throws NoSuchPetException, SystemException {
        return findByPrimaryKey((Serializable) petId);
    }

    /**
     * Returns the pet with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the pet
     * @return the pet, or <code>null</code> if a pet with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Pet pet = (Pet) EntityCacheUtil.getResult(PetModelImpl.ENTITY_CACHE_ENABLED,
                PetImpl.class, primaryKey);

        if (pet == _nullPet) {
            return null;
        }

        if (pet == null) {
            Session session = null;

            try {
                session = openSession();

                pet = (Pet) session.get(PetImpl.class, primaryKey);

                if (pet != null) {
                    cacheResult(pet);
                } else {
                    EntityCacheUtil.putResult(PetModelImpl.ENTITY_CACHE_ENABLED,
                        PetImpl.class, primaryKey, _nullPet);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(PetModelImpl.ENTITY_CACHE_ENABLED,
                    PetImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return pet;
    }

    /**
     * Returns the pet with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param petId the primary key of the pet
     * @return the pet, or <code>null</code> if a pet with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Pet fetchByPrimaryKey(long petId) throws SystemException {
        return fetchByPrimaryKey((Serializable) petId);
    }

    /**
     * Returns all the pets.
     *
     * @return the pets
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pet> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the pets.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of pets
     * @param end the upper bound of the range of pets (not inclusive)
     * @return the range of pets
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pet> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the pets.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of pets
     * @param end the upper bound of the range of pets (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of pets
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Pet> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Pet> list = (List<Pet>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PET);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PET;

                if (pagination) {
                    sql = sql.concat(PetModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Pet>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Pet>(list);
                } else {
                    list = (List<Pet>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the pets from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Pet pet : findAll()) {
            remove(pet);
        }
    }

    /**
     * Returns the number of pets.
     *
     * @return the number of pets
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PET);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the pet persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Pet")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Pet>> listenersList = new ArrayList<ModelListener<Pet>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Pet>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PetImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
