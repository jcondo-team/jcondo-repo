/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.ListTypeConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.datatype.PetType;
import br.com.atilo.jcondo.manager.service.base.FlatLocalServiceBaseImpl;

/**
 * The implementation of the flat local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.FlatLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.FlatLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil
 */
public class FlatLocalServiceImpl extends FlatLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil} to access the flat local service.
	 */

	public Flat addFlat(int number, int block, boolean disables, boolean brigade, boolean pets, List<PetType> petTypes) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Organization admin = organizationLocalService.getOrganizations(sc.getCompanyId(), 0L).get(0);
		Organization organization = organizationLocalService.addOrganization(sc.getUserId(), admin.getOrganizationId(), 
																			 block + "/" + number, DomainType.FLAT.getLabel(), 
																			 true, 0, 0, 
																			 ListTypeConstants.ORGANIZATION_STATUS_DEFAULT, 
																			 null, false, null);

		Flat flat = flatPersistence.create(counterLocalService.increment());
		flat.setOrganizationId(organization.getOrganizationId());
		flat.setNumber(number);
		flat.setBlock(block);
		flat.setBrigade(brigade);
		flat.setDisables(disables);
		flat.setPets(pets);
		flat.setOprDate(new Date());
		flat.setOprUser(sc.getUserId());

		flat = addFlat(flat);

		for (PetType type : petTypes) {
			petLocalService.addPet(type, flat.getId());
		}

		return flat;
	}

	public Flat updateFlat(long id, boolean disables, boolean brigade, boolean pets, List<PetType> petTypes) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Flat flat = flatPersistence.findByPrimaryKey(id);

		flat.setBrigade(brigade);
		flat.setDisables(disables);
		flat.setPets(pets);
		flat.setOprDate(new Date());
		flat.setOprUser(sc.getUserId());

		flat = updateFlat(flat);
		petLocalService.updatePetTypes(petTypes, flat.getId());

		return flat;
	}

	public List<Flat> getPersonFlats(long personId) throws PortalException, SystemException {
		List<Flat> flats = new ArrayList<Flat>();

		for (Organization organization : organizationLocalService
											.getUserOrganizations(personLocalService
																	.getPerson(personId).getUserId())) {
			try {
				flats.add(flatPersistence.findByOrganization(organization.getOrganizationId()));
			} catch (NoSuchFlatException e) {
				// TODO: handle exception
			}
		}

		return flats;
	}
	
	public List<Flat> getFlats() throws SystemException {
		return new ArrayList<Flat>(flatPersistence.findAll());
	}
	
	public Flat setFlatPerson(long flatId, long personId) throws NoSuchFlatException, SystemException {
		Flat flat = flatPersistence.findByPrimaryKey(flatId);
		flat.setPersonId(personId);
		return updateFlat(flat);
	}

	public void setFlatDefaulting(long id, boolean isDefaulting) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();

		Flat flat = flatPersistence.findByPrimaryKey(id);
		flat.setDefaulting(isDefaulting);
		flat.setOprDate(new Date());
		flat.setOprUser(sc.getUserId());
		
		updateFlat(flat);
	}
}