package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.Observable;
import java.util.Observer;

import javax.faces.application.FacesMessage;
import javax.faces.event.ValueChangeEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.VehicleServiceUtil;
import br.com.atilo.jcondo.manager.service.permission.VehiclePermission;
import br.com.atilo.jcondo.manager.bean.ModelDataModel;

public class VehicleListBean<Domain extends BaseModel<Domain>> implements Observer, Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(VehicleBean.class);

	private VehicleBean<Domain> vehicleBean;

	private ModelDataModel<Vehicle> model;

	private Vehicle[] selectedVehicles;

	private Domain domain;

	public void init(Domain domain) {
		try {
			setDomain(domain);
			model = new ModelDataModel<Vehicle>(VehicleServiceUtil.getDomainVehicles((Long) domain.getPrimaryKeyObj()));
			vehicleBean = new VehicleBean<Domain>();
			vehicleBean.init(domain);
			vehicleBean.addObserver(this);
		} catch (Exception e) {
			LOGGER.fatal("Failure on vehicle initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "general.failure", null);
		}
	}

	public void onVehicleSearch(ValueChangeEvent event) throws Exception {
		String license = (String) event.getNewValue();
		if (StringUtils.isEmpty(license)) {
			model.clear();
			model.addAll(VehicleServiceUtil.getDomainVehicles((Long) domain.getPrimaryKeyObj()));
		} else {
			model.clear();
			model.addAll(VehicleServiceUtil.getDomainVehiclesByLicense((Long) domain.getPrimaryKeyObj(), 
																		license.toUpperCase()));
		}
	}

	public void onDomainSearch(BaseModel<?> domain) throws Exception {
		model.clear();
		model.addAll(VehicleServiceUtil.getDomainVehicles((Long) domain.getPrimaryKeyObj()));
	}

	public void onVehicleEdit() {
		try {
			vehicleBean.onVehicleEdit(model.getRowData());
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on person editing", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "general.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onVehicleDelete() throws Exception {
		try {
			VehicleServiceUtil.deleteVehicle(model.getRowData().getId());
			model.remove(model.getRowData());
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.vehicle.delete", null);
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on vehicle delete", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public boolean canCreateVehicle() {
		try {
			return VehiclePermission.hasPermission(Permission.ADD, 0, (Long) domain.getPrimaryKeyObj());
		} catch (Exception e) {
			LOGGER.warn("Failure on vehicle create permission check", e);
		}
		return false;
	}

	@Override
	public void update(Observable observable, Object object) {
		model.update((Vehicle) object);
	}

	public VehicleBean<Domain> getVehicleBean() {
		return vehicleBean;
	}

	public void setVehicleBean(VehicleBean<Domain> vehicleBean) {
		this.vehicleBean = vehicleBean;
	}

	public ModelDataModel<Vehicle> getModel() {
		return model;
	}

	public void setModel(ModelDataModel<Vehicle> model) {
		this.model = model;
	}

	public Vehicle[] getSelectedVehicles() {
		return selectedVehicles;
	}

	public void setSelectedVehicles(Vehicle[] selectedVehicles) {
		this.selectedVehicles = selectedVehicles;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

}