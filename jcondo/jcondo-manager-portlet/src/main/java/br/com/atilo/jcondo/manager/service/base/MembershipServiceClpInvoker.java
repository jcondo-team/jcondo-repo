package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.MembershipServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MembershipServiceClpInvoker {
    private String _methodName110;
    private String[] _methodParameterTypes110;
    private String _methodName111;
    private String[] _methodParameterTypes111;
    private String _methodName116;
    private String[] _methodParameterTypes116;
    private String _methodName117;
    private String[] _methodParameterTypes117;
    private String _methodName118;
    private String[] _methodParameterTypes118;
    private String _methodName119;
    private String[] _methodParameterTypes119;
    private String _methodName120;
    private String[] _methodParameterTypes120;
    private String _methodName121;
    private String[] _methodParameterTypes121;
    private String _methodName122;
    private String[] _methodParameterTypes122;

    public MembershipServiceClpInvoker() {
        _methodName110 = "getBeanIdentifier";

        _methodParameterTypes110 = new String[] {  };

        _methodName111 = "setBeanIdentifier";

        _methodParameterTypes111 = new String[] { "java.lang.String" };

        _methodName116 = "createMembership";

        _methodParameterTypes116 = new String[] { "long" };

        _methodName117 = "addMembership";

        _methodParameterTypes117 = new String[] {
                "long", "long", "br.com.atilo.jcondo.datatype.PersonType"
            };

        _methodName118 = "deleteMembership";

        _methodParameterTypes118 = new String[] { "long" };

        _methodName119 = "updateMembership";

        _methodParameterTypes119 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.PersonType"
            };

        _methodName120 = "getPersonMemberships";

        _methodParameterTypes120 = new String[] { "long" };

        _methodName121 = "getDomainMembershipsByType";

        _methodParameterTypes121 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.PersonType"
            };

        _methodName122 = "getDomainMembershipByPerson";

        _methodParameterTypes122 = new String[] { "long", "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName110.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
            return MembershipServiceUtil.getBeanIdentifier();
        }

        if (_methodName111.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
            MembershipServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName116.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes116, parameterTypes)) {
            return MembershipServiceUtil.createMembership(((Long) arguments[0]).longValue());
        }

        if (_methodName117.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes117, parameterTypes)) {
            return MembershipServiceUtil.addMembership(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                (br.com.atilo.jcondo.datatype.PersonType) arguments[2]);
        }

        if (_methodName118.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes118, parameterTypes)) {
            return MembershipServiceUtil.deleteMembership(((Long) arguments[0]).longValue());
        }

        if (_methodName119.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes119, parameterTypes)) {
            return MembershipServiceUtil.updateMembership(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.PersonType) arguments[1]);
        }

        if (_methodName120.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes120, parameterTypes)) {
            return MembershipServiceUtil.getPersonMemberships(((Long) arguments[0]).longValue());
        }

        if (_methodName121.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes121, parameterTypes)) {
            return MembershipServiceUtil.getDomainMembershipsByType(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.PersonType) arguments[1]);
        }

        if (_methodName122.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes122, parameterTypes)) {
            return MembershipServiceUtil.getDomainMembershipByPerson(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
