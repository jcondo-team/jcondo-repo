package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Enterprise;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Enterprise in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Enterprise
 * @generated
 */
public class EnterpriseCacheModel implements CacheModel<Enterprise>,
    Externalizable {
    public long id;
    public long organizationId;
    public int statusId;
    public String identity;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{id=");
        sb.append(id);
        sb.append(", organizationId=");
        sb.append(organizationId);
        sb.append(", statusId=");
        sb.append(statusId);
        sb.append(", identity=");
        sb.append(identity);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Enterprise toEntityModel() {
        EnterpriseImpl enterpriseImpl = new EnterpriseImpl();

        enterpriseImpl.setId(id);
        enterpriseImpl.setOrganizationId(organizationId);
        enterpriseImpl.setStatusId(statusId);

        if (identity == null) {
            enterpriseImpl.setIdentity(StringPool.BLANK);
        } else {
            enterpriseImpl.setIdentity(identity);
        }

        if (oprDate == Long.MIN_VALUE) {
            enterpriseImpl.setOprDate(null);
        } else {
            enterpriseImpl.setOprDate(new Date(oprDate));
        }

        enterpriseImpl.setOprUser(oprUser);

        enterpriseImpl.resetOriginalValues();

        return enterpriseImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        organizationId = objectInput.readLong();
        statusId = objectInput.readInt();
        identity = objectInput.readUTF();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(organizationId);
        objectOutput.writeInt(statusId);

        if (identity == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(identity);
        }

        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
