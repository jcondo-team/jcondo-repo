/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.NoSuchAccessPermissionException;
import br.com.atilo.jcondo.manager.NoSuchPersonException;
import br.com.atilo.jcondo.manager.model.AccessPermission;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.base.AccessPermissionLocalServiceBaseImpl;
import br.com.atilo.jcondo.manager.service.permission.DomainPermission;

/**
 * The implementation of the access permission local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.AccessPermissionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.AccessPermissionLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.AccessPermissionLocalServiceUtil
 */
public class AccessPermissionLocalServiceImpl
	extends AccessPermissionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.AccessPermissionLocalServiceUtil} to access the access permission local service.
	 */

	public AccessPermission addAccessPermission(long personId, long domainId, int weekDay, String beginTime, String endTime, Date beginDate, Date endDate) throws PortalException, SystemException {
		try {
			personLocalService.getPerson(personId);
		} catch (NoSuchPersonException e) {
			throw new BusinessException("exception.access.permission.person.not.found", personId);
		}
		
		Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		
		if (beginDate != null && beginDate.before(today)) {
			throw new BusinessException("exception.access.permission.begindate.before.today");
		}

		if (endDate != null && endDate.before(today)) {
			throw new BusinessException("exception.access.permission.enddate.before.today");
		}

		if (beginDate != null && endDate != null && endDate.before(beginDate)) {
			throw new BusinessException("exception.access.permission.enddate.before.begindate");
		}

		AccessPermission accessPermission = createAccessPermission(counterLocalService.increment());
		accessPermission.setPersonId(personId);
		accessPermission.setDomainId(domainId);
		accessPermission.setWeekDay(weekDay);
		accessPermission.setBeginTime(StringUtils.isEmpty(beginTime) ? null : beginTime);
		accessPermission.setEndTime(StringUtils.isEmpty(endTime) ? null : endTime);
		accessPermission.setBeginDate(beginDate);
		accessPermission.setEndDate(endDate);
		accessPermission.setOprDate(new Date());
		accessPermission.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());
		
		return super.addAccessPermission(accessPermission);
	}
	
	
	public List<AccessPermission> getPersonAccessPermissions(long personId, long domainId) throws SystemException {
		return accessPermissionPersistence.findByPersonAndDomain(personId, domainId);
	}

	public AccessPermission getPersonAccessPermission(long personId, long domainId, int weekDay) throws PortalException, SystemException {
		try {
			return accessPermissionPersistence.findByPersonDomainAndWeekDay(personId, domainId, weekDay);
		} catch (NoSuchAccessPermissionException e) {
			return null;
		}
	}

	public boolean hasAccess(long personId, long domainId, Date date) throws Exception {
		return DomainPermission.hasDomainPermission(personId, Permission.VIEW, domainId) || 
			   !CollectionUtils.isEmpty(accessPermissionFinder.findAccessPermission(personId, domainId, date));
	}

}