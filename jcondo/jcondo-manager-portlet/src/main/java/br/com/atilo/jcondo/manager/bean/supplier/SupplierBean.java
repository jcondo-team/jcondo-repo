package br.com.atilo.jcondo.manager.bean.supplier;

import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.datatype.EnterpriseStatus;
import br.com.atilo.jcondo.manager.service.EnterpriseServiceUtil;
import br.com.atilo.jcondo.manager.bean.AbstractDomainBean;
import br.com.atilo.jcondo.manager.bean.DomainPhoneBean;

@ViewScoped
@ManagedBean
public class SupplierBean extends AbstractDomainBean<Enterprise> {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(SupplierBean.class);

	private DomainPhoneBean<Enterprise> phoneBean;

	private SupplierAddressBean addressBean;

	private List<EnterpriseStatus> status;

	public SupplierBean() {
		status = Arrays.asList(EnterpriseStatus.values());
	}

	@Override
	public void preInit() throws Exception {
		domains = EnterpriseServiceUtil.getEnterprisesByType(DomainType.SUPPLIER);
		status = Arrays.asList(EnterpriseStatus.values());
		personListBean = new EmployeeListBean();
		phoneBean = new DomainPhoneBean<Enterprise>();
		addressBean = new SupplierAddressBean();
	}

	@Override
	public void onDomainSelect() throws Exception {
		super.onDomainSelect();
		addressBean.init(domain);
		phoneBean.init(domain);
	}
	
	@Override
	public void onDomainSave() {
		try {
			if (domain.isNew()) {
				domain = EnterpriseServiceUtil.addEnterprise(domain.getIdentity(), domain.getName(), 
															 domain.getType(), domain.getStatus(), 
															 domain.getDescription());
			} else {
				domain = EnterpriseServiceUtil.updateEnterprise(domain.getId(), domain.getIdentity(), 
																domain.getName(), domain.getType(), 
																domain.getStatus(), domain.getDescription());
			}

			domains.add(domain);
			onDomainSelect();

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.enterprise.save", null);
			
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on address save: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on address save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.enterprise.save", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	@Override
	public void onDomainCreate() throws Exception {
		domain = EnterpriseServiceUtil.createEnterprise();
	}

	public void onContactSave() {
		phoneBean.onPhoneSave();
		addressBean.onAddressSave();
	}
	
	public DomainPhoneBean<Enterprise> getPhoneBean() {
		return phoneBean;
	}

	public void setPhoneBean(DomainPhoneBean<Enterprise> phoneBean) {
		this.phoneBean = phoneBean;
	}

	public SupplierAddressBean getAddressBean() {
		return addressBean;
	}

	public void setAddressBean(SupplierAddressBean addressBean) {
		this.addressBean = addressBean;
	}

	public List<EnterpriseStatus> getStatus() {
		return status;
	}

	public void setStatus(List<EnterpriseStatus> status) {
		this.status = status;
	}

}