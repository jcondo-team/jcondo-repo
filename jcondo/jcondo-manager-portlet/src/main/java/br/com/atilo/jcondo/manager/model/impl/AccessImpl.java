/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.model.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;

/**
 * The extended model implementation for the Access service. Represents a row in the &quot;jco_access&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Access} interface.
 * </p>
 *
 * @author anderson
 */
public class AccessImpl extends AccessBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a access model instance should use the {@link br.com.atilo.jcondo.manager.model.Access} interface instead.
	 */
	public AccessImpl() {
	}
	
	public Vehicle getVehicle() throws PortalException, SystemException {
		return getVehicleId() == 0 ? null : VehicleLocalServiceUtil.getVehicle(getVehicleId());
	}
}