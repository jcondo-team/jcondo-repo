/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.model.Telephone;
import br.com.atilo.jcondo.datatype.PhoneType;
import br.com.atilo.jcondo.manager.service.base.TelephoneServiceBaseImpl;

/**
 * The implementation of the telephone remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.TelephoneService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.TelephoneServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.TelephoneServiceUtil
 */
public class TelephoneServiceImpl extends TelephoneServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.TelephoneServiceUtil} to access the telephone remote service.
	 */
	public Telephone addDomainPhone(long domainId, long number, long extension, boolean primary) throws PortalException, SystemException {
		return telephoneLocalService.addDomainPhone(domainId, number, extension, primary);
	}
	
	public Telephone addPersonPhone(long number, long extension, PhoneType type, boolean primary, long personId) throws PortalException, SystemException {
		return telephoneLocalService.addPersonPhone(number, extension, type, primary, personId);
	}

	public Telephone updateEnterprisePhone(long phoneId, long number, long extension, boolean primary) throws PortalException, SystemException {
		return telephoneLocalService.updateEnterprisePhone(phoneId, number, extension, primary);
	}

	public Telephone updatePersonPhone(long phoneId, long number, long extension, PhoneType type, boolean primary) throws PortalException, SystemException {
		return telephoneLocalService.updatePersonPhone(phoneId, number, extension, type, primary);
	}

	public Telephone deletePhone(long phoneId) throws PortalException, SystemException {
		return telephoneLocalService.deletePhone(phoneId);
	}

	public List<Telephone> getPersonPhones(long personId) throws PortalException, SystemException {
		return telephoneLocalService.getPersonPhones(personId);
	}
	
	public List<Telephone> getPersonPhones(long personId, PhoneType type) throws PortalException, SystemException {
		return telephoneLocalService.getPersonPhones(personId, type);
	}
	
	public Telephone getPersonPhone(long personId) throws PortalException, SystemException {
		return telephoneLocalService.getPersonPhone(personId);
	}

	public List<Telephone> getDomainPhones(long domainId) throws PortalException, SystemException {
		return telephoneLocalService.getDomainPhones(domainId);
	}

}