package br.com.atilo.jcondo.manager.bean.department;

import java.util.List;

import javax.faces.application.FacesMessage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;

import br.com.atilo.jcondo.commons.collections.PersonTypePredicate;
import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.bean.AbstractPersonListBean;

public class MemberListBean extends AbstractPersonListBean<Department> {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(MemberListBean.class);

	public MemberListBean() {
		personBean = new MemberBean();
		personBean.addObserver(this);
	}
	
	@SuppressWarnings("unchecked")
	public void init(Department department) {
		try {
			super.init(department);

			List<Person> people = (List<Person>) CollectionUtils
													.selectRejected((List<Person>) model.getWrappedData(), 
																	new PersonTypePredicate(PersonType.VISITOR, department.getId()));

			model.clear();
			model.addAll(people);
			personBean.init(department);
			personTypesFilter = personBean.getPersonTypes();
			selectedPersonTypes = new PersonType[personTypesFilter.size()];
			personTypesFilter.toArray(selectedPersonTypes);
		} catch (Exception e) {
			LOGGER.fatal("Failure on member list initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

}