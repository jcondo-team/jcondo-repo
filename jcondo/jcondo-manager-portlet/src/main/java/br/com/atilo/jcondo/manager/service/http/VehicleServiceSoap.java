package br.com.atilo.jcondo.manager.service.http;

import br.com.atilo.jcondo.manager.service.VehicleServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.manager.service.VehicleServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.manager.model.VehicleSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.manager.model.Vehicle}, that is translated to a
 * {@link br.com.atilo.jcondo.manager.model.VehicleSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VehicleServiceHttp
 * @see br.com.atilo.jcondo.manager.model.VehicleSoap
 * @see br.com.atilo.jcondo.manager.service.VehicleServiceUtil
 * @generated
 */
public class VehicleServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(VehicleServiceSoap.class);

    public static br.com.atilo.jcondo.manager.model.VehicleSoap createVehicle()
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Vehicle returnValue = VehicleServiceUtil.createVehicle();

            return br.com.atilo.jcondo.manager.model.VehicleSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.VehicleSoap addVehicle(
        java.lang.String license,
        br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Vehicle returnValue = VehicleServiceUtil.addVehicle(license,
                    type, domainId, portrait, brand, color, remark);

            return br.com.atilo.jcondo.manager.model.VehicleSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.VehicleSoap updateVehicle(
        long id, br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Vehicle returnValue = VehicleServiceUtil.updateVehicle(id,
                    type, domainId, portrait, brand, color, remark);

            return br.com.atilo.jcondo.manager.model.VehicleSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.VehicleSoap updateVehiclePortrait(
        long id, br.com.atilo.jcondo.Image portrait) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Vehicle returnValue = VehicleServiceUtil.updateVehiclePortrait(id,
                    portrait);

            return br.com.atilo.jcondo.manager.model.VehicleSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.VehicleSoap deleteVehicle(
        long id) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Vehicle returnValue = VehicleServiceUtil.deleteVehicle(id);

            return br.com.atilo.jcondo.manager.model.VehicleSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.VehicleSoap getVehicleByLicense(
        java.lang.String license) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Vehicle returnValue = VehicleServiceUtil.getVehicleByLicense(license);

            return br.com.atilo.jcondo.manager.model.VehicleSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.VehicleSoap[] getDomainVehicles(
        long domainId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> returnValue =
                VehicleServiceUtil.getDomainVehicles(domainId);

            return br.com.atilo.jcondo.manager.model.VehicleSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.VehicleSoap[] getDomainVehiclesByLicense(
        long domainId, java.lang.String license) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> returnValue =
                VehicleServiceUtil.getDomainVehiclesByLicense(domainId, license);

            return br.com.atilo.jcondo.manager.model.VehicleSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
