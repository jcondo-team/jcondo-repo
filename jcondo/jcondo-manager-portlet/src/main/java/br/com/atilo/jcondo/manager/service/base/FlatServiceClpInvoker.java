package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.FlatServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class FlatServiceClpInvoker {
    private String _methodName110;
    private String[] _methodParameterTypes110;
    private String _methodName111;
    private String[] _methodParameterTypes111;
    private String _methodName116;
    private String[] _methodParameterTypes116;
    private String _methodName117;
    private String[] _methodParameterTypes117;
    private String _methodName118;
    private String[] _methodParameterTypes118;
    private String _methodName119;
    private String[] _methodParameterTypes119;
    private String _methodName120;
    private String[] _methodParameterTypes120;
    private String _methodName121;
    private String[] _methodParameterTypes121;
    private String _methodName122;
    private String[] _methodParameterTypes122;

    public FlatServiceClpInvoker() {
        _methodName110 = "getBeanIdentifier";

        _methodParameterTypes110 = new String[] {  };

        _methodName111 = "setBeanIdentifier";

        _methodParameterTypes111 = new String[] { "java.lang.String" };

        _methodName116 = "addFlat";

        _methodParameterTypes116 = new String[] {
                "int", "int", "boolean", "boolean", "boolean", "java.util.List"
            };

        _methodName117 = "updateFlat";

        _methodParameterTypes117 = new String[] {
                "long", "boolean", "boolean", "boolean", "java.util.List"
            };

        _methodName118 = "getPersonFlats";

        _methodParameterTypes118 = new String[] { "long" };

        _methodName119 = "getFlat";

        _methodParameterTypes119 = new String[] { "long" };

        _methodName120 = "getFlats";

        _methodParameterTypes120 = new String[] {  };

        _methodName121 = "setFlatDefaulting";

        _methodParameterTypes121 = new String[] { "long", "boolean" };

        _methodName122 = "setFlatPerson";

        _methodParameterTypes122 = new String[] { "long", "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName110.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
            return FlatServiceUtil.getBeanIdentifier();
        }

        if (_methodName111.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
            FlatServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName116.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes116, parameterTypes)) {
            return FlatServiceUtil.addFlat(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue(),
                ((Boolean) arguments[2]).booleanValue(),
                ((Boolean) arguments[3]).booleanValue(),
                ((Boolean) arguments[4]).booleanValue(),
                (java.util.List<br.com.atilo.jcondo.datatype.PetType>) arguments[5]);
        }

        if (_methodName117.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes117, parameterTypes)) {
            return FlatServiceUtil.updateFlat(((Long) arguments[0]).longValue(),
                ((Boolean) arguments[1]).booleanValue(),
                ((Boolean) arguments[2]).booleanValue(),
                ((Boolean) arguments[3]).booleanValue(),
                (java.util.List<br.com.atilo.jcondo.datatype.PetType>) arguments[4]);
        }

        if (_methodName118.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes118, parameterTypes)) {
            return FlatServiceUtil.getPersonFlats(((Long) arguments[0]).longValue());
        }

        if (_methodName119.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes119, parameterTypes)) {
            return FlatServiceUtil.getFlat(((Long) arguments[0]).longValue());
        }

        if (_methodName120.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes120, parameterTypes)) {
            return FlatServiceUtil.getFlats();
        }

        if (_methodName121.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes121, parameterTypes)) {
            FlatServiceUtil.setFlatDefaulting(((Long) arguments[0]).longValue(),
                ((Boolean) arguments[1]).booleanValue());

            return null;
        }

        if (_methodName122.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes122, parameterTypes)) {
            return FlatServiceUtil.setFlatPerson(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
