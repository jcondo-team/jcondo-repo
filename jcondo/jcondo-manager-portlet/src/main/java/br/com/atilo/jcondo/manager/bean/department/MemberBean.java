package br.com.atilo.jcondo.manager.bean.department;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.bean.AbstractPersonBean;
import br.com.atilo.jcondo.manager.bean.PersonAddressBean;

public class MemberBean extends AbstractPersonBean<Department> {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(MemberBean.class);

	private PersonAddressBean addressBean;

	public MemberBean() {
		super();
		addressBean = new PersonAddressBean();
	}

	@Override
	public void init(Department department) throws Exception {
		super.init(department);
		addressBean.init(person);
	}

	@Override
	public void onPersonSave() {
		try {
			super.onPersonSave();
			addressBean.onAddressSave();
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on employee saving: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on employee saving", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}	
	}

	public PersonAddressBean getAddressBean() {
		return addressBean;
	}

	public void setAddressBean(PersonAddressBean addressBean) {
		this.addressBean = addressBean;
	}

}