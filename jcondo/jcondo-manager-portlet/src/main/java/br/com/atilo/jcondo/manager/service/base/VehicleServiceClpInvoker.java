package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.VehicleServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class VehicleServiceClpInvoker {
    private String _methodName104;
    private String[] _methodParameterTypes104;
    private String _methodName105;
    private String[] _methodParameterTypes105;
    private String _methodName110;
    private String[] _methodParameterTypes110;
    private String _methodName111;
    private String[] _methodParameterTypes111;
    private String _methodName112;
    private String[] _methodParameterTypes112;
    private String _methodName113;
    private String[] _methodParameterTypes113;
    private String _methodName114;
    private String[] _methodParameterTypes114;
    private String _methodName115;
    private String[] _methodParameterTypes115;
    private String _methodName116;
    private String[] _methodParameterTypes116;
    private String _methodName117;
    private String[] _methodParameterTypes117;

    public VehicleServiceClpInvoker() {
        _methodName104 = "getBeanIdentifier";

        _methodParameterTypes104 = new String[] {  };

        _methodName105 = "setBeanIdentifier";

        _methodParameterTypes105 = new String[] { "java.lang.String" };

        _methodName110 = "createVehicle";

        _methodParameterTypes110 = new String[] {  };

        _methodName111 = "addVehicle";

        _methodParameterTypes111 = new String[] {
                "java.lang.String", "br.com.atilo.jcondo.datatype.VehicleType",
                "long", "br.com.atilo.jcondo.Image", "java.lang.String",
                "java.lang.String", "java.lang.String"
            };

        _methodName112 = "updateVehicle";

        _methodParameterTypes112 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.VehicleType", "long",
                "br.com.atilo.jcondo.Image", "java.lang.String",
                "java.lang.String", "java.lang.String"
            };

        _methodName113 = "updateVehiclePortrait";

        _methodParameterTypes113 = new String[] {
                "long", "br.com.atilo.jcondo.Image"
            };

        _methodName114 = "deleteVehicle";

        _methodParameterTypes114 = new String[] { "long" };

        _methodName115 = "getVehicleByLicense";

        _methodParameterTypes115 = new String[] { "java.lang.String" };

        _methodName116 = "getDomainVehicles";

        _methodParameterTypes116 = new String[] { "long" };

        _methodName117 = "getDomainVehiclesByLicense";

        _methodParameterTypes117 = new String[] { "long", "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName104.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes104, parameterTypes)) {
            return VehicleServiceUtil.getBeanIdentifier();
        }

        if (_methodName105.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes105, parameterTypes)) {
            VehicleServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName110.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
            return VehicleServiceUtil.createVehicle();
        }

        if (_methodName111.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
            return VehicleServiceUtil.addVehicle((java.lang.String) arguments[0],
                (br.com.atilo.jcondo.datatype.VehicleType) arguments[1],
                ((Long) arguments[2]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[3],
                (java.lang.String) arguments[4],
                (java.lang.String) arguments[5], (java.lang.String) arguments[6]);
        }

        if (_methodName112.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes112, parameterTypes)) {
            return VehicleServiceUtil.updateVehicle(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.VehicleType) arguments[1],
                ((Long) arguments[2]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[3],
                (java.lang.String) arguments[4],
                (java.lang.String) arguments[5], (java.lang.String) arguments[6]);
        }

        if (_methodName113.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes113, parameterTypes)) {
            return VehicleServiceUtil.updateVehiclePortrait(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[1]);
        }

        if (_methodName114.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes114, parameterTypes)) {
            return VehicleServiceUtil.deleteVehicle(((Long) arguments[0]).longValue());
        }

        if (_methodName115.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes115, parameterTypes)) {
            return VehicleServiceUtil.getVehicleByLicense((java.lang.String) arguments[0]);
        }

        if (_methodName116.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes116, parameterTypes)) {
            return VehicleServiceUtil.getDomainVehicles(((Long) arguments[0]).longValue());
        }

        if (_methodName117.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes117, parameterTypes)) {
            return VehicleServiceUtil.getDomainVehiclesByLicense(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        throw new UnsupportedOperationException();
    }
}
