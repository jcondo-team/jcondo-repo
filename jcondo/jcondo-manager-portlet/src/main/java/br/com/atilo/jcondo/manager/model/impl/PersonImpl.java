/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.model.impl;

import java.util.Date;
import java.util.List;

import br.com.atilo.jcondo.manager.NoSuchDepartmentException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Telephone;
import br.com.atilo.jcondo.datatype.Gender;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.MembershipLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.TelephoneLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.Contact;
import com.liferay.portal.model.User;
import com.liferay.portal.model.UserConstants;
import com.liferay.portal.service.ContactLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

/**
 * The extended model implementation for the Person service. Represents a row in the &quot;jco_person&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Person} interface.
 * </p>
 *
 * @author anderson
 */
public class PersonImpl extends PersonBaseImpl {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a person model instance should use the {@link br.com.atilo.jcondo.manager.model.Person} interface instead.
	 */
	private User user;

	private Contact contact;

	public PersonImpl() {
		
	}

	public String getName() throws PortalException, SystemException {
		return user.getFirstName();
	}
	
	public void setName(String name) {
		user.setFirstName(name);
	}

	public String getSurname() throws PortalException, SystemException {
		return user.getLastName();
	}
	
	public void setSurname(String surname) {
		user.setLastName(surname);
	}

	public String getFullName() throws PortalException, SystemException {
		return user.getFullName();
	}

	public Gender getGender() throws PortalException, SystemException {
		return contact.isMale() ? Gender.MALE : Gender.FEMALE;
	}
	
	public void setGender(Gender gender) throws PortalException, SystemException {
		contact.setMale(gender == Gender.MALE);
	}

	public Date getBirthday() throws PortalException, SystemException {
		return contact.getBirthday();
	}
	
	public void setBirthday(Date birthday) throws PortalException, SystemException {
		contact.setBirthday(birthday);
	}

	public String getEmail() throws PortalException, SystemException {
		if (user.getEmailAddress() == null || user.getEmailAddress().endsWith(PropsUtil.get("users.email.address.auto.suffix"))) {
			return null;
		}
		return user.getEmailAddress();
	}

	public void setEmail(String email) throws PortalException, SystemException {
		user.setEmailAddress(email);
	}

	public Image getPortrait() throws PortalException, SystemException {
		String path = UserConstants.getPortraitURL(PortalUtil.getPathImage(), contact.isMale(), user != null ? user.getPortraitId() : 0L);
		return new Image(user.getPortraitId(), path);
	}
	
	public void setPortrait(Image image) {
		user.setPortraitId(image.getId());
	}

	public List<Membership> getMemberships() throws SystemException {
		return MembershipLocalServiceUtil.getPersonMemberships(getId());
	}

	public List<Telephone> getTelephones() throws PortalException, SystemException {
		return TelephoneLocalServiceUtil.getPersonPhones(getId());
	}
	
	public PersonType getType(long domainId) throws PortalException, SystemException {
		return MembershipLocalServiceUtil.getDomainMembershipByPerson(domainId, getId()).getType();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) throws PortalException, SystemException {
		this.user = user;
		this.contact = user.getContactId() == 0 ? ContactLocalServiceUtil.createContact(0) : user.getContact();
	}

	public BaseModel<?> getDomain() throws PortalException, SystemException {
		if (getDomainId() <= 0) {
			return null;
		}

		try {
			return FlatLocalServiceUtil.getFlat(getDomainId());
		} catch (NoSuchFlatException e) {
			try {
				return EnterpriseLocalServiceUtil.getEnterprise(getDomainId());
			} catch (NoSuchEnterpriseException ex) {
				try {
					return DepartmentLocalServiceUtil.getDepartment(getDomainId());
				} catch (NoSuchDepartmentException ex2) {
					return null;
				}
			}
		}
	}
	
	public void setDomain(BaseModel<?> domain) throws PortalException, SystemException {
		setDomainId(domain == null ? 0 : (Long) domain.getPrimaryKeyObj());
	}

}