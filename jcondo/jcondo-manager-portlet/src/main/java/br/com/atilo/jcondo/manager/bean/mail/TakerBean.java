package br.com.atilo.jcondo.manager.bean.mail;

import java.util.Arrays;
import java.util.List;
import java.util.Observable;

import javax.faces.application.FacesMessage;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.NoSuchPersonException;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.Gender;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.service.MembershipServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonServiceUtil;
import br.com.atilo.jcondo.manager.bean.CameraBean;

public class TakerBean extends Observable {

	private static Logger LOGGER = Logger.getLogger(TakerBean.class);

	private CameraBean cameraBean;	
	
	private List<PersonType> personTypes;

	private List<Gender> genders;

	private Person person;

	private Membership membership;

	private Image image;
	
	private String identity;
	
	public TakerBean() {
		genders = Arrays.asList(Gender.values());
		cameraBean = new CameraBean(158, 240);
	}
	
	public void init(Person person) {
		try {
			this.person = person != null ? person : PersonLocalServiceUtil.createPerson();
			this.image = person.getPortrait();
			this.identity = person.getIdentity();
		} catch (Exception e) {
			LOGGER.fatal("Failure on mail initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}
	
	public void onPersonCreate() throws Exception {
		person = PersonServiceUtil.createPerson();
		person.setGender(Gender.MALE);
		membership = MembershipServiceUtil.createMembership(0);
		membership.setType(PersonType.VISITOR);
		image = null;
		identity = null;
	}

	public void onPersonSave() throws Exception {
		try {
			if (person.isNew()) {
				person = PersonServiceUtil.addPerson(person.getName(), person.getSurname(), person.getIdentity(), 
													 person.getEmail(), person.getBirthday(), person.getGender(),
													 person.getRemark(), membership.getType(), membership.getDomainId());
	
				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.person.add", null);
			} else {
				person = PersonServiceUtil.updatePerson(person.getId(), person.getName(), person.getSurname(), 
														person.getIdentity(), person.getEmail(), person.getBirthday(), 
														person.getGender(), person.getRemark(), person.getDomainId(), membership);
	
				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.person.update", null);
			}
	
			person = PersonServiceUtil.updatePortrait(person.getId(), image);
	
			setChanged();
			notifyObservers(person);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on person save : " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on person save ", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}	

	public void onSearchByCPF(AjaxBehaviorEvent event) throws Exception {
		if (StringUtils.isEmpty(identity)) {
			person.setId(0);
			person.setUserId(0);
			person.setIdentity(null);
			person.setName(null);
			person.setSurname(null);
			person.setGender(Gender.MALE);
			cameraBean.setImage(person.getPortrait());
			return;
		}

		try {
			person = PersonServiceUtil.getPersonByIdentity(identity);
			image = person.getPortrait();
			membership = MembershipServiceUtil.createMembership(0);
			membership.setType(PersonType.VISITOR);
		} catch (NoSuchPersonException e) {
			LOGGER.debug("failure on onSearchByCPF method", e);
			person.setIdentity(identity);
		} catch (BusinessException e) {
			LOGGER.debug("failure on onSearchByCPF method", e);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}	

	public CameraBean getCameraBean() {
		return cameraBean;
	}

	public void setCameraBean(CameraBean cameraBean) {
		this.cameraBean = cameraBean;
	}

	public List<PersonType> getPersonTypes() {
		return personTypes;
	}

	public void setPersonTypes(List<PersonType> personTypes) {
		this.personTypes = personTypes;
	}

	public List<Gender> getGenders() {
		return genders;
	}

	public void setGenders(List<Gender> genders) {
		this.genders = genders;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Membership getMembership() {
		return membership;
	}

	public void setMembership(Membership membership) {
		this.membership = membership;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

}
