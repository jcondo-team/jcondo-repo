/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.NoSuchPetException;
import br.com.atilo.jcondo.manager.model.Pet;
import br.com.atilo.jcondo.datatype.PetType;
import br.com.atilo.jcondo.manager.service.base.PetLocalServiceBaseImpl;

/**
 * The implementation of the pet local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.PetLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.PetLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.PetLocalServiceUtil
 */
public class PetLocalServiceImpl extends PetLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.PetLocalServiceUtil} to access the pet local service.
	 */

	public void updatePetTypes(List<PetType> petTypes, long flatId) throws PortalException, SystemException {
		// Deleting...
		for (Pet pet : petPersistence.findByFlat(flatId)) {
			if (!petTypes.contains(PetType.valueOf(pet.getTypeId()))) {
				deletePet(pet.getPetId());
			}
		}

		// Adding...
		for (PetType petType : petTypes) {
			try {
				petPersistence.findByTypeAndFlat(petType.ordinal(), flatId);
			} catch (NoSuchPetException e) {
				addPet(petType, flatId);
			}
		}
	}	

	public void addPet(PetType type, long flatId) throws PortalException, SystemException {
		Pet pet = petPersistence.create(counterLocalService.increment());
		pet.setTypeId(type.ordinal());
		pet.setFlatId(flatId);
		addPet(pet);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Pet updatePet(Pet pet) throws SystemException {
		pet.setOprDate(new Date());
		pet.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());
		return super.updatePet(pet);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Pet addPet(Pet pet) throws SystemException {
		pet.setOprDate(new Date());
		pet.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());
		return super.addPet(pet);
	}

	public List<PetType> getPetTypes(long flatId) throws SystemException {
		List<PetType> petTypes = new ArrayList<PetType>();

		for (Pet pet : petPersistence.findByFlat(flatId)) {
			petTypes.add(PetType.valueOf(pet.getTypeId()));
		}

		return petTypes;
	}
}