package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Access;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Access in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Access
 * @generated
 */
public class AccessCacheModel implements CacheModel<Access>, Externalizable {
    public long id;
    public long destinationId;
    public long visitorId;
    public long vehicleId;
    public long authorizerId;
    public String authorizerName;
    public String badge;
    public boolean ended;
    public String remark;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{id=");
        sb.append(id);
        sb.append(", destinationId=");
        sb.append(destinationId);
        sb.append(", visitorId=");
        sb.append(visitorId);
        sb.append(", vehicleId=");
        sb.append(vehicleId);
        sb.append(", authorizerId=");
        sb.append(authorizerId);
        sb.append(", authorizerName=");
        sb.append(authorizerName);
        sb.append(", badge=");
        sb.append(badge);
        sb.append(", ended=");
        sb.append(ended);
        sb.append(", remark=");
        sb.append(remark);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Access toEntityModel() {
        AccessImpl accessImpl = new AccessImpl();

        accessImpl.setId(id);
        accessImpl.setDestinationId(destinationId);
        accessImpl.setVisitorId(visitorId);
        accessImpl.setVehicleId(vehicleId);
        accessImpl.setAuthorizerId(authorizerId);

        if (authorizerName == null) {
            accessImpl.setAuthorizerName(StringPool.BLANK);
        } else {
            accessImpl.setAuthorizerName(authorizerName);
        }

        if (badge == null) {
            accessImpl.setBadge(StringPool.BLANK);
        } else {
            accessImpl.setBadge(badge);
        }

        accessImpl.setEnded(ended);

        if (remark == null) {
            accessImpl.setRemark(StringPool.BLANK);
        } else {
            accessImpl.setRemark(remark);
        }

        if (oprDate == Long.MIN_VALUE) {
            accessImpl.setOprDate(null);
        } else {
            accessImpl.setOprDate(new Date(oprDate));
        }

        accessImpl.setOprUser(oprUser);

        accessImpl.resetOriginalValues();

        return accessImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        destinationId = objectInput.readLong();
        visitorId = objectInput.readLong();
        vehicleId = objectInput.readLong();
        authorizerId = objectInput.readLong();
        authorizerName = objectInput.readUTF();
        badge = objectInput.readUTF();
        ended = objectInput.readBoolean();
        remark = objectInput.readUTF();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(destinationId);
        objectOutput.writeLong(visitorId);
        objectOutput.writeLong(vehicleId);
        objectOutput.writeLong(authorizerId);

        if (authorizerName == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(authorizerName);
        }

        if (badge == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(badge);
        }

        objectOutput.writeBoolean(ended);

        if (remark == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(remark);
        }

        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
