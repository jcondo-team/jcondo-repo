package br.com.atilo.jcondo.manager.bean.supplier;

import java.util.ArrayList;
import java.util.Arrays;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;

import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.bean.AbstractPersonListBean;

public class EmployeeListBean extends AbstractPersonListBean<Enterprise> {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(EmployeeListBean.class);

	public EmployeeListBean() {
		personBean = new EmployeeBean();
		personBean.addObserver(this);
	}
	
	public void init(Enterprise enterprise) {
		try {
			super.init(enterprise);
			personBean.init(enterprise);

			personTypesFilter = new ArrayList<PersonType>(Arrays.asList(PersonType.SUPPLIER_TYPES));
			selectedPersonTypes = new PersonType[personTypesFilter.size()];
			personTypesFilter.toArray(selectedPersonTypes);
		} catch (Exception e) {
			LOGGER.fatal("Failure on employee initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

}