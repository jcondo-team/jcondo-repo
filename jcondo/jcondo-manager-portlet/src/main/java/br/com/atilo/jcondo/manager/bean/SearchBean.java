package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;

import com.liferay.util.portlet.PortletProps;

import br.com.atilo.jcondo.commons.collections.PersonNameComparator;
import br.com.atilo.jcondo.commons.collections.VehicleLicenseComparator;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;
import br.com.atilo.jcondo.manager.bean.access.VehicleBrand;

@SessionScoped
@ManagedBean
public class SearchBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(SearchBean.class);

	private String name;

	private String identity;

	private String license;

	private String color;

	private VehicleBrand brand;

	private String service;

	private List<Person> people;

	private List<Vehicle> vehicles;

	private List<VehicleBrand> brands;
	
	private String[] colors;

	private String[] services;

	@PostConstruct
	public void init() {
		try {
			brands = new ArrayList<VehicleBrand>();
			for (String brand : PortletProps.getArray("vehicle.brands")) {
				brands.add(new VehicleBrand(brand, brand.concat(".png")));
			}

			colors = PortletProps.getArray("colors");
			services = PortletProps.getArray("vehicle.services");
		} catch (Exception e) {
			LOGGER.fatal("Failure on flat initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}
	
	public void onPeopleSearch() {
		try {
			people = PersonLocalServiceUtil.getPeople(name, identity, 0);
			Collections.sort(people, new PersonNameComparator());
		} catch (Exception e) {
			LOGGER.fatal("Failure on people search", e);
		}
	}

	public void onVehicleSearch() {
		try {
			HashMap<String, String> params = new HashMap<String, String>();
			params.put("license", license != null ? license.replaceAll("[^A-Za-z0-9]", "") : null);
			params.put("brand", brand != null ? brand.getName() : null);
			params.put("color", color);
			params.put("remark", service);
			vehicles = VehicleLocalServiceUtil.getVehicles(params);
			Collections.sort(vehicles, new VehicleLicenseComparator());
		} catch (Exception e) {
			LOGGER.fatal("Failure on vehicle search", e);
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public VehicleBrand getBrand() {
		return brand;
	}

	public void setBrand(VehicleBrand brand) {
		this.brand = brand;
	}

	public String getService() {
		return service;
	}

	public void setService(String service) {
		this.service = service;
	}

	public List<Person> getPeople() {
		return people;
	}

	public void setPeople(List<Person> people) {
		this.people = people;
	}

	public List<Vehicle> getVehicles() {
		return vehicles;
	}

	public void setVehicles(List<Vehicle> vehicles) {
		this.vehicles = vehicles;
	}

	public List<VehicleBrand> getBrands() {
		return brands;
	}

	public void setBrands(List<VehicleBrand> brands) {
		this.brands = brands;
	}

	public String[] getColors() {
		return colors;
	}

	public void setColors(String[] colors) {
		this.colors = colors;
	}

	public String[] getServices() {
		return services;
	}

	public void setServices(String[] services) {
		this.services = services;
	}

}
