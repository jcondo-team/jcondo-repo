package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchVehicleException;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.model.impl.VehicleImpl;
import br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.VehiclePersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the vehicle service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VehiclePersistence
 * @see VehicleUtil
 * @generated
 */
public class VehiclePersistenceImpl extends BasePersistenceImpl<Vehicle>
    implements VehiclePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link VehicleUtil} to access the vehicle persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = VehicleImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_LICENSE = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByLicense",
            new String[] { String.class.getName() },
            VehicleModelImpl.LICENSE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_LICENSE = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByLicense",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_LICENSE_LICENSE_1 = "vehicle.license IS NULL";
    private static final String _FINDER_COLUMN_LICENSE_LICENSE_2 = "vehicle.license = ?";
    private static final String _FINDER_COLUMN_LICENSE_LICENSE_3 = "(vehicle.license IS NULL OR vehicle.license = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DOMAIN = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDomain",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN =
        new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, VehicleImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByDomain",
            new String[] { Long.class.getName() },
            VehicleModelImpl.DOMAINID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DOMAIN = new FinderPath(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDomain",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_DOMAIN_DOMAINID_2 = "vehicle.domainId = ?";
    private static final String _SQL_SELECT_VEHICLE = "SELECT vehicle FROM Vehicle vehicle";
    private static final String _SQL_SELECT_VEHICLE_WHERE = "SELECT vehicle FROM Vehicle vehicle WHERE ";
    private static final String _SQL_COUNT_VEHICLE = "SELECT COUNT(vehicle) FROM Vehicle vehicle";
    private static final String _SQL_COUNT_VEHICLE_WHERE = "SELECT COUNT(vehicle) FROM Vehicle vehicle WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "vehicle.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Vehicle exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Vehicle exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(VehiclePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Vehicle _nullVehicle = new VehicleImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Vehicle> toCacheModel() {
                return _nullVehicleCacheModel;
            }
        };

    private static CacheModel<Vehicle> _nullVehicleCacheModel = new CacheModel<Vehicle>() {
            @Override
            public Vehicle toEntityModel() {
                return _nullVehicle;
            }
        };

    public VehiclePersistenceImpl() {
        setModelClass(Vehicle.class);
    }

    /**
     * Returns the vehicle where license = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchVehicleException} if it could not be found.
     *
     * @param license the license
     * @return the matching vehicle
     * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByLicense(String license)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByLicense(license);

        if (vehicle == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("license=");
            msg.append(license);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchVehicleException(msg.toString());
        }

        return vehicle;
    }

    /**
     * Returns the vehicle where license = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param license the license
     * @return the matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByLicense(String license) throws SystemException {
        return fetchByLicense(license, true);
    }

    /**
     * Returns the vehicle where license = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param license the license
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByLicense(String license, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { license };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_LICENSE,
                    finderArgs, this);
        }

        if (result instanceof Vehicle) {
            Vehicle vehicle = (Vehicle) result;

            if (!Validator.equals(license, vehicle.getLicense())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            boolean bindLicense = false;

            if (license == null) {
                query.append(_FINDER_COLUMN_LICENSE_LICENSE_1);
            } else if (license.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSE_LICENSE_3);
            } else {
                bindLicense = true;

                query.append(_FINDER_COLUMN_LICENSE_LICENSE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicense) {
                    qPos.add(license);
                }

                List<Vehicle> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LICENSE,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "VehiclePersistenceImpl.fetchByLicense(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Vehicle vehicle = list.get(0);

                    result = vehicle;

                    cacheResult(vehicle);

                    if ((vehicle.getLicense() == null) ||
                            !vehicle.getLicense().equals(license)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LICENSE,
                            finderArgs, vehicle);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LICENSE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Vehicle) result;
        }
    }

    /**
     * Removes the vehicle where license = &#63; from the database.
     *
     * @param license the license
     * @return the vehicle that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle removeByLicense(String license)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByLicense(license);

        return remove(vehicle);
    }

    /**
     * Returns the number of vehicles where license = &#63;.
     *
     * @param license the license
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByLicense(String license) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_LICENSE;

        Object[] finderArgs = new Object[] { license };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            boolean bindLicense = false;

            if (license == null) {
                query.append(_FINDER_COLUMN_LICENSE_LICENSE_1);
            } else if (license.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_LICENSE_LICENSE_3);
            } else {
                bindLicense = true;

                query.append(_FINDER_COLUMN_LICENSE_LICENSE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindLicense) {
                    qPos.add(license);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the vehicles where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @return the matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDomain(long domainId) throws SystemException {
        return findByDomain(domainId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles where domainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param domainId the domain ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDomain(long domainId, int start, int end)
        throws SystemException {
        return findByDomain(domainId, start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles where domainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param domainId the domain ID
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findByDomain(long domainId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN;
            finderArgs = new Object[] { domainId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DOMAIN;
            finderArgs = new Object[] { domainId, start, end, orderByComparator };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Vehicle vehicle : list) {
                if ((domainId != vehicle.getDomainId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_DOMAIN_DOMAINID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(VehicleModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first vehicle in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle
     * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByDomain_First(long domainId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByDomain_First(domainId, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("domainId=");
        msg.append(domainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the first vehicle in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByDomain_First(long domainId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Vehicle> list = findByDomain(domainId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last vehicle in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle
     * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByDomain_Last(long domainId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByDomain_Last(domainId, orderByComparator);

        if (vehicle != null) {
            return vehicle;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("domainId=");
        msg.append(domainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchVehicleException(msg.toString());
    }

    /**
     * Returns the last vehicle in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByDomain_Last(long domainId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDomain(domainId);

        if (count == 0) {
            return null;
        }

        List<Vehicle> list = findByDomain(domainId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the vehicles before and after the current vehicle in the ordered set where domainId = &#63;.
     *
     * @param id the primary key of the current vehicle
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next vehicle
     * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle[] findByDomain_PrevAndNext(long id, long domainId,
        OrderByComparator orderByComparator)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Vehicle[] array = new VehicleImpl[3];

            array[0] = getByDomain_PrevAndNext(session, vehicle, domainId,
                    orderByComparator, true);

            array[1] = vehicle;

            array[2] = getByDomain_PrevAndNext(session, vehicle, domainId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Vehicle getByDomain_PrevAndNext(Session session, Vehicle vehicle,
        long domainId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_VEHICLE_WHERE);

        query.append(_FINDER_COLUMN_DOMAIN_DOMAINID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(VehicleModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(domainId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(vehicle);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Vehicle> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the vehicles where domainId = &#63; from the database.
     *
     * @param domainId the domain ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDomain(long domainId) throws SystemException {
        for (Vehicle vehicle : findByDomain(domainId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @return the number of matching vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDomain(long domainId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DOMAIN;

        Object[] finderArgs = new Object[] { domainId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_VEHICLE_WHERE);

            query.append(_FINDER_COLUMN_DOMAIN_DOMAINID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the vehicle in the entity cache if it is enabled.
     *
     * @param vehicle the vehicle
     */
    @Override
    public void cacheResult(Vehicle vehicle) {
        EntityCacheUtil.putResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleImpl.class, vehicle.getPrimaryKey(), vehicle);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LICENSE,
            new Object[] { vehicle.getLicense() }, vehicle);

        vehicle.resetOriginalValues();
    }

    /**
     * Caches the vehicles in the entity cache if it is enabled.
     *
     * @param vehicles the vehicles
     */
    @Override
    public void cacheResult(List<Vehicle> vehicles) {
        for (Vehicle vehicle : vehicles) {
            if (EntityCacheUtil.getResult(
                        VehicleModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleImpl.class, vehicle.getPrimaryKey()) == null) {
                cacheResult(vehicle);
            } else {
                vehicle.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all vehicles.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(VehicleImpl.class.getName());
        }

        EntityCacheUtil.clearCache(VehicleImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the vehicle.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Vehicle vehicle) {
        EntityCacheUtil.removeResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleImpl.class, vehicle.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(vehicle);
    }

    @Override
    public void clearCache(List<Vehicle> vehicles) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Vehicle vehicle : vehicles) {
            EntityCacheUtil.removeResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
                VehicleImpl.class, vehicle.getPrimaryKey());

            clearUniqueFindersCache(vehicle);
        }
    }

    protected void cacheUniqueFindersCache(Vehicle vehicle) {
        if (vehicle.isNew()) {
            Object[] args = new Object[] { vehicle.getLicense() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LICENSE, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LICENSE, args,
                vehicle);
        } else {
            VehicleModelImpl vehicleModelImpl = (VehicleModelImpl) vehicle;

            if ((vehicleModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_LICENSE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { vehicle.getLicense() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_LICENSE, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_LICENSE, args,
                    vehicle);
            }
        }
    }

    protected void clearUniqueFindersCache(Vehicle vehicle) {
        VehicleModelImpl vehicleModelImpl = (VehicleModelImpl) vehicle;

        Object[] args = new Object[] { vehicle.getLicense() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LICENSE, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LICENSE, args);

        if ((vehicleModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_LICENSE.getColumnBitmask()) != 0) {
            args = new Object[] { vehicleModelImpl.getOriginalLicense() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_LICENSE, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_LICENSE, args);
        }
    }

    /**
     * Creates a new vehicle with the primary key. Does not add the vehicle to the database.
     *
     * @param id the primary key for the new vehicle
     * @return the new vehicle
     */
    @Override
    public Vehicle create(long id) {
        Vehicle vehicle = new VehicleImpl();

        vehicle.setNew(true);
        vehicle.setPrimaryKey(id);

        return vehicle;
    }

    /**
     * Removes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the vehicle
     * @return the vehicle that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle remove(long id)
        throws NoSuchVehicleException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the vehicle
     * @return the vehicle that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle remove(Serializable primaryKey)
        throws NoSuchVehicleException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Vehicle vehicle = (Vehicle) session.get(VehicleImpl.class,
                    primaryKey);

            if (vehicle == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchVehicleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(vehicle);
        } catch (NoSuchVehicleException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Vehicle removeImpl(Vehicle vehicle) throws SystemException {
        vehicle = toUnwrappedModel(vehicle);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(vehicle)) {
                vehicle = (Vehicle) session.get(VehicleImpl.class,
                        vehicle.getPrimaryKeyObj());
            }

            if (vehicle != null) {
                session.delete(vehicle);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (vehicle != null) {
            clearCache(vehicle);
        }

        return vehicle;
    }

    @Override
    public Vehicle updateImpl(br.com.atilo.jcondo.manager.model.Vehicle vehicle)
        throws SystemException {
        vehicle = toUnwrappedModel(vehicle);

        boolean isNew = vehicle.isNew();

        VehicleModelImpl vehicleModelImpl = (VehicleModelImpl) vehicle;

        Session session = null;

        try {
            session = openSession();

            if (vehicle.isNew()) {
                session.save(vehicle);

                vehicle.setNew(false);
            } else {
                session.merge(vehicle);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !VehicleModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((vehicleModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        vehicleModelImpl.getOriginalDomainId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAIN, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN,
                    args);

                args = new Object[] { vehicleModelImpl.getDomainId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAIN, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN,
                    args);
            }
        }

        EntityCacheUtil.putResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
            VehicleImpl.class, vehicle.getPrimaryKey(), vehicle);

        clearUniqueFindersCache(vehicle);
        cacheUniqueFindersCache(vehicle);

        return vehicle;
    }

    protected Vehicle toUnwrappedModel(Vehicle vehicle) {
        if (vehicle instanceof VehicleImpl) {
            return vehicle;
        }

        VehicleImpl vehicleImpl = new VehicleImpl();

        vehicleImpl.setNew(vehicle.isNew());
        vehicleImpl.setPrimaryKey(vehicle.getPrimaryKey());

        vehicleImpl.setId(vehicle.getId());
        vehicleImpl.setDomainId(vehicle.getDomainId());
        vehicleImpl.setImageId(vehicle.getImageId());
        vehicleImpl.setTypeId(vehicle.getTypeId());
        vehicleImpl.setLicense(vehicle.getLicense());
        vehicleImpl.setBrand(vehicle.getBrand());
        vehicleImpl.setColor(vehicle.getColor());
        vehicleImpl.setRemark(vehicle.getRemark());
        vehicleImpl.setOprDate(vehicle.getOprDate());
        vehicleImpl.setOprUser(vehicle.getOprUser());

        return vehicleImpl;
    }

    /**
     * Returns the vehicle with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle
     * @return the vehicle
     * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByPrimaryKey(Serializable primaryKey)
        throws NoSuchVehicleException, SystemException {
        Vehicle vehicle = fetchByPrimaryKey(primaryKey);

        if (vehicle == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchVehicleException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return vehicle;
    }

    /**
     * Returns the vehicle with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchVehicleException} if it could not be found.
     *
     * @param id the primary key of the vehicle
     * @return the vehicle
     * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle findByPrimaryKey(long id)
        throws NoSuchVehicleException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the vehicle with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the vehicle
     * @return the vehicle, or <code>null</code> if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Vehicle vehicle = (Vehicle) EntityCacheUtil.getResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
                VehicleImpl.class, primaryKey);

        if (vehicle == _nullVehicle) {
            return null;
        }

        if (vehicle == null) {
            Session session = null;

            try {
                session = openSession();

                vehicle = (Vehicle) session.get(VehicleImpl.class, primaryKey);

                if (vehicle != null) {
                    cacheResult(vehicle);
                } else {
                    EntityCacheUtil.putResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
                        VehicleImpl.class, primaryKey, _nullVehicle);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(VehicleModelImpl.ENTITY_CACHE_ENABLED,
                    VehicleImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return vehicle;
    }

    /**
     * Returns the vehicle with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the vehicle
     * @return the vehicle, or <code>null</code> if a vehicle with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Vehicle fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the vehicles.
     *
     * @return the vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the vehicles.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @return the range of vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the vehicles.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of vehicles
     * @param end the upper bound of the range of vehicles (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Vehicle> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Vehicle> list = (List<Vehicle>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_VEHICLE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_VEHICLE;

                if (pagination) {
                    sql = sql.concat(VehicleModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Vehicle>(list);
                } else {
                    list = (List<Vehicle>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the vehicles from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Vehicle vehicle : findAll()) {
            remove(vehicle);
        }
    }

    /**
     * Returns the number of vehicles.
     *
     * @return the number of vehicles
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_VEHICLE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the vehicle persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Vehicle")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Vehicle>> listenersList = new ArrayList<ModelListener<Vehicle>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Vehicle>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(VehicleImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
