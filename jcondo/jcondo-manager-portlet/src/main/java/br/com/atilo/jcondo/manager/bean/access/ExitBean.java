package br.com.atilo.jcondo.manager.bean.access;

import java.io.Serializable;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Access;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.service.AccessLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;


@SessionScoped
@ManagedBean
public class ExitBean implements Observer, Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(ExitBean.class);

	private EntreeVehicleBean vehicleBean;

	private Vehicle vehicle;

	private String badge;

	private boolean unknownExitTime;

	private Access access;

	@PostConstruct
	public void init() {
		try {
			vehicleBean = new EntreeVehicleBean();
			//vehicleBean.onVehicleCreate();
			vehicleBean.addObserver(this);
		} catch (Exception e) {
			LOGGER.fatal("Failure on exit access initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public void onBadgeAccessRegister() {
		try {
			Access access = AccessLocalServiceUtil.getAccessByBadge(badge);

			if (access == null) {
				throw new BusinessException("exception.access.badge.available", badge);
			}

			AccessLocalServiceUtil.endAccess(access.getId(), unknownExitTime ? "unknown exit time" : null);

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.access.exit.register.success", null);

			badge = null;
			unknownExitTime = false;
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on access register : " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on access register", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "general.failure", null);
		}
	}

	public void onVehicleAccessRegister() {
		try {
			String remark = unknownExitTime ? "unknown exit time" : null;

			if (access == null) {
				AccessLocalServiceUtil.addVehicleExitAccess(vehicle.getId(), remark);
			} else {
				AccessLocalServiceUtil.endAccess(access.getId(), remark);	
			}

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.access.exit.register.success", null);

			access = null;
			vehicle = null;
			unknownExitTime = false;
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on access register : " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on access register", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "general.failure", null);
		}
	}
	
	public List<Vehicle> onVehicleComplete(String license) {
		try {
			return VehicleLocalServiceUtil.getVehicles(license);
		} catch (Exception e) {
			LOGGER.warn("Failure on vehicle complete : " + e.getMessage());
		}
		
		return null;
	}

	public void onVehicleSelect(SelectEvent event) {
		try {
			access = AccessLocalServiceUtil.getAccessByVehicle(((Vehicle) event.getObject()).getId());
		} catch (Exception e) {
			LOGGER.warn("Failure on vehicle complete : " + e.getMessage());
		}
	}

	public void onVehicleUnSelect(AjaxBehaviorEvent event) {
		if (((AutoComplete) event.getSource()).getSubmittedValue().equals("")) {
			vehicle = null;
			access = null;
		}
	}

	@Override
	public void update(Observable observable, Object object) {
		if (object instanceof Vehicle) {
			vehicle = (Vehicle) object;
		}
	}

	public EntreeVehicleBean getVehicleBean() {
		return vehicleBean;
	}

	public void setVehicleBean(EntreeVehicleBean vehicleBean) {
		this.vehicleBean = vehicleBean;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getBadge() {
		return badge;
	}

	public void setBadge(String badge) {
		this.badge = badge;
	}

	public boolean isUnknownExitTime() {
		return unknownExitTime;
	}

	public void setUnknownExitTime(boolean unknownExitTime) {
		this.unknownExitTime = unknownExitTime;
	}

	public Access getAccess() {
		return access;
	}

	public void setAccess(Access access) {
		this.access = access;
	}

}