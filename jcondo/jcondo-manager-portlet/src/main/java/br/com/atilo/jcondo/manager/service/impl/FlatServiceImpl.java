/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.permission.PortalPermissionUtil;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.datatype.PetType;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.base.FlatServiceBaseImpl;
import br.com.atilo.jcondo.manager.service.permission.DomainPermission;
import br.com.atilo.jcondo.manager.service.permission.FlatPermission;

/**
 * The implementation of the flat remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.FlatService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.FlatServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.FlatServiceUtil
 */
public class FlatServiceImpl extends FlatServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.FlatServiceUtil} to access the flat remote service.
	 */

	public Flat addFlat(int number, int block, boolean disables, boolean brigade, boolean pets, List<PetType> petTypes) throws PortalException, SystemException {
		PortalPermissionUtil.check(getPermissionChecker(), ActionKeys.ADD_ORGANIZATION);
		return flatLocalService.addFlat(number, block, disables, brigade, pets, petTypes);
	}

	public Flat updateFlat(long id, boolean disables, boolean brigade, boolean pets, List<PetType> petTypes) throws PortalException, SystemException {
		if (!DomainPermission.hasDomainPermission(Permission.UPDATE, id)) {
			throw new BusinessException("exception.flat.update.denied", id);
		}
		return flatLocalService.updateFlat(id, disables, brigade, pets, petTypes);
	}

	public List<Flat> getPersonFlats(long personId) throws PortalException, SystemException {
		List<Flat> flats = flatLocalService.getPersonFlats(personId);

		if (!CollectionUtils.isEmpty(flats)) {
			for (int i = flats.size() - 1; i >=0; i--) {
				if (!DomainPermission.hasDomainPermission(Permission.VIEW, flats.get(i).getId())) {
					flats.remove(i);
				}
			}
		} else {
			flats = getFlats();
		}

		return flats;
	}

	public Flat getFlat(long id) throws PortalException, SystemException {
		if (!DomainPermission.hasDomainPermission(Permission.VIEW, id)) {
			throw new BusinessException("exception.flat.view.denied", id);
		}
		return flatLocalService.getFlat(id);
	}

	public List<Flat> getFlats() throws PortalException, SystemException {
		List<Flat> flats = flatLocalService.getFlats();
		for (int i = flats.size() - 1; i >=0; i--) {
			if (!DomainPermission.hasDomainPermission(Permission.VIEW, flats.get(i).getId())) {
				flats.remove(i);
			}
		}
		return flats;
	}

	public void setFlatDefaulting(long flatId, boolean isDefaulting) throws PortalException, SystemException {
		if (!FlatPermission.hasFlatPermission(Permission.SET_DEFAULTING, flatId)) {
			throw new BusinessException("exception.flat.set.defaulting.denied", flatId);
		}
		
		flatLocalService.setFlatDefaulting(flatId, isDefaulting);
	}

	public Flat setFlatPerson(long flatId, long personId) throws PortalException, SystemException {
		if (!DomainPermission.hasDomainPermission(Permission.UPDATE, flatId)) {
			throw new BusinessException("exception.flat.update.denied", flatId);
		}
		return flatLocalService.setFlatPerson(flatId, personId);
	}

}