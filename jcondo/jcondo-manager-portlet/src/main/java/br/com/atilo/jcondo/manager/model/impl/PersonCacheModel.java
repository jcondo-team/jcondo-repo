package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Person;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Person in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Person
 * @generated
 */
public class PersonCacheModel implements CacheModel<Person>, Externalizable {
    public long id;
    public long userId;
    public long domainId;
    public String identity;
    public String remark;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{id=");
        sb.append(id);
        sb.append(", userId=");
        sb.append(userId);
        sb.append(", domainId=");
        sb.append(domainId);
        sb.append(", identity=");
        sb.append(identity);
        sb.append(", remark=");
        sb.append(remark);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Person toEntityModel() {
        PersonImpl personImpl = new PersonImpl();

        personImpl.setId(id);
        personImpl.setUserId(userId);
        personImpl.setDomainId(domainId);

        if (identity == null) {
            personImpl.setIdentity(StringPool.BLANK);
        } else {
            personImpl.setIdentity(identity);
        }

        if (remark == null) {
            personImpl.setRemark(StringPool.BLANK);
        } else {
            personImpl.setRemark(remark);
        }

        if (oprDate == Long.MIN_VALUE) {
            personImpl.setOprDate(null);
        } else {
            personImpl.setOprDate(new Date(oprDate));
        }

        personImpl.setOprUser(oprUser);

        personImpl.resetOriginalValues();

        return personImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        userId = objectInput.readLong();
        domainId = objectInput.readLong();
        identity = objectInput.readUTF();
        remark = objectInput.readUTF();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(userId);
        objectOutput.writeLong(domainId);

        if (identity == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(identity);
        }

        if (remark == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(remark);
        }

        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
