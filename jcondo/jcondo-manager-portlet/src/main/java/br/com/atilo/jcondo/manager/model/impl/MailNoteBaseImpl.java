package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.MailNote;
import br.com.atilo.jcondo.manager.service.MailNoteLocalServiceUtil;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The extended model base implementation for the MailNote service. Represents a row in the &quot;jco_mail_note&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link MailNoteImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MailNoteImpl
 * @see br.com.atilo.jcondo.manager.model.MailNote
 * @generated
 */
public abstract class MailNoteBaseImpl extends MailNoteModelImpl
    implements MailNote {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a mail note model instance should use the {@link MailNote} interface instead.
     */
    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            MailNoteLocalServiceUtil.addMailNote(this);
        } else {
            MailNoteLocalServiceUtil.updateMailNote(this);
        }
    }
}
