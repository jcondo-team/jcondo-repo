package br.com.atilo.jcondo.manager.bean.flat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.commons.collections.DomainPredicate;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Kinship;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.KinType;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.KinshipServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonServiceUtil;
import br.com.atilo.jcondo.manager.bean.AbstractPersonBean;
import br.com.atilo.jcondo.manager.bean.PersonAddressBean;

public class ResidentBean extends AbstractPersonBean<Flat> {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(ResidentBean.class);

	private PersonAddressBean addressBean;

	private Kinship kinship;

	private List<KinType> kinTypes;

	private List<Flat> flats;

	public ResidentBean() {
		super();
		addressBean = new PersonAddressBean();
		kinTypes = Arrays.asList(KinType.PARENT, KinType.SPOUSE, KinType.CHILD, KinType.OTHER);
	}
	
	@Override
	public void init(Flat flat) throws Exception {
		super.init(flat);
		addressBean.init(person);
	}

	public void onPersonCreate() throws Exception {
		super.onPersonCreate();
		flats = new ArrayList<Flat>();
		flats.add(domain);

		Membership m = (Membership) CollectionUtils.find(PersonServiceUtil.getPerson().getMemberships(), 
				  										 new DomainPredicate(getDomain().getId()));			
		if (m != null) {
			kinship = KinshipServiceUtil.createKinship(PersonServiceUtil.getPerson().getId(), person.getId(), null);
		} else {
			kinship = null;
		}
	}

	@Override
	public void onPersonSave() {
		try {
			if ((membership.getType() == PersonType.OWNER || membership.getType() == PersonType.RENTER) 
					&& CollectionUtils.isEmpty(phoneBean.getPhones())) {
				throw new BusinessException("telefone � obrigatorio para proprietario");
			}

			super.onPersonSave();
			addressBean.onAddressSave();

			if (kinship != null) {
				if (kinship.getId() > 0) {
					kinship = KinshipServiceUtil.updateKinship(kinship.getId(), 
															   kinship.getType());
				} else if (kinship.getType() != null) {
					kinship = KinshipServiceUtil.addKinship(kinship.getPersonId(), 
															kinship.getRelativeId(), 
															kinship.getType());

				}
			}

		} catch (BusinessException e) {
			LOGGER.warn("Business failure on person saving: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on person saving", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}	
	}

	@Override
	public void onPersonEdit(Person person) throws Exception {
		super.onPersonEdit(person);
		addressBean.init(this.person);
		flats = FlatLocalServiceUtil.getPersonFlats(person.getId());
		if (!flats.contains(domain)) {
			flats.add(domain);
		}
	}

	public void onPersonAddressChange() {
		try {
			if (addressBean.isDweller()) {
				person.setDomain(flats.get(0));
			} else {
				person.setDomain(null);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public Kinship getKinship() {
		return kinship;
	}

	public void setKinship(Kinship kinship) {
		this.kinship = kinship;
	}

	public List<KinType> getKinTypes() {
		return kinTypes;
	}

	public void setKinTypes(List<KinType> kinTypes) {
		this.kinTypes = kinTypes;
	}

	public PersonAddressBean getAddressBean() {
		return addressBean;
	}

	public void setAddressBean(PersonAddressBean addressBean) {
		this.addressBean = addressBean;
	}

	public List<Flat> getFlats() {
		return flats;
	}

	public void setFlats(List<Flat> flats) {
		this.flats = flats;
	}

}
