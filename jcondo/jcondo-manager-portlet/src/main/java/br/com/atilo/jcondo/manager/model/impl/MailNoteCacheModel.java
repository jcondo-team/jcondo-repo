package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.MailNote;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing MailNote in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see MailNote
 * @generated
 */
public class MailNoteCacheModel implements CacheModel<MailNote>, Externalizable {
    public long id;
    public long mailId;
    public String description;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{id=");
        sb.append(id);
        sb.append(", mailId=");
        sb.append(mailId);
        sb.append(", description=");
        sb.append(description);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public MailNote toEntityModel() {
        MailNoteImpl mailNoteImpl = new MailNoteImpl();

        mailNoteImpl.setId(id);
        mailNoteImpl.setMailId(mailId);

        if (description == null) {
            mailNoteImpl.setDescription(StringPool.BLANK);
        } else {
            mailNoteImpl.setDescription(description);
        }

        if (oprDate == Long.MIN_VALUE) {
            mailNoteImpl.setOprDate(null);
        } else {
            mailNoteImpl.setOprDate(new Date(oprDate));
        }

        mailNoteImpl.setOprUser(oprUser);

        mailNoteImpl.resetOriginalValues();

        return mailNoteImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        mailId = objectInput.readLong();
        description = objectInput.readUTF();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(mailId);

        if (description == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(description);
        }

        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
