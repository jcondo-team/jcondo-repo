package br.com.atilo.jcondo.manager.service.impl;

import br.com.atilo.jcondo.manager.service.base.MailNoteServiceBaseImpl;

/**
 * The implementation of the mail note remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.MailNoteService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.base.MailNoteServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.MailNoteServiceUtil
 */
public class MailNoteServiceImpl extends MailNoteServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.MailNoteServiceUtil} to access the mail note remote service.
     */
}
