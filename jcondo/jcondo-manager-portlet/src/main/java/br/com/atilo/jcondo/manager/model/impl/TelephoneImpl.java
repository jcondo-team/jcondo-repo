/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.datatype.PhoneType;

/**
 * The extended model implementation for the Telephone service. Represents a row in the &quot;Telephone&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Telephone} interface.
 * </p>
 *
 * @author anderson
 */
public class TelephoneImpl extends TelephoneBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a telephone model instance should use the {@link br.com.atilo.jcondo.manager.model.Telephone} interface instead.
	 */
	public TelephoneImpl() {
	}
	
	public TelephoneImpl(long id, PhoneType type, long extension, long number, boolean primary) {
		super();
		setId(id);
		setType(type);
		setExtension(extension);
		setNumber(number);
		setPrimary(primary);
	}

	public PhoneType getType() {
		return PhoneType.parse(getTypeName());
	}

	public void setType(PhoneType type) {
		setTypeName(type.getTypeName());
	}
}