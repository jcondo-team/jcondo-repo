package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchKinshipException;
import br.com.atilo.jcondo.manager.model.Kinship;
import br.com.atilo.jcondo.manager.model.impl.KinshipImpl;
import br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.KinshipPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the kinship service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see KinshipPersistence
 * @see KinshipUtil
 * @generated
 */
public class KinshipPersistenceImpl extends BasePersistenceImpl<Kinship>
    implements KinshipPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link KinshipUtil} to access the kinship persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = KinshipImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(KinshipModelImpl.ENTITY_CACHE_ENABLED,
            KinshipModelImpl.FINDER_CACHE_ENABLED, KinshipImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(KinshipModelImpl.ENTITY_CACHE_ENABLED,
            KinshipModelImpl.FINDER_CACHE_ENABLED, KinshipImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(KinshipModelImpl.ENTITY_CACHE_ENABLED,
            KinshipModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON = new FinderPath(KinshipModelImpl.ENTITY_CACHE_ENABLED,
            KinshipModelImpl.FINDER_CACHE_ENABLED, KinshipImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPerson",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON =
        new FinderPath(KinshipModelImpl.ENTITY_CACHE_ENABLED,
            KinshipModelImpl.FINDER_CACHE_ENABLED, KinshipImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPerson",
            new String[] { Long.class.getName() },
            KinshipModelImpl.PERSONID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PERSON = new FinderPath(KinshipModelImpl.ENTITY_CACHE_ENABLED,
            KinshipModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPerson",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_PERSON_PERSONID_2 = "kinship.personId = ?";
    private static final String _SQL_SELECT_KINSHIP = "SELECT kinship FROM Kinship kinship";
    private static final String _SQL_SELECT_KINSHIP_WHERE = "SELECT kinship FROM Kinship kinship WHERE ";
    private static final String _SQL_COUNT_KINSHIP = "SELECT COUNT(kinship) FROM Kinship kinship";
    private static final String _SQL_COUNT_KINSHIP_WHERE = "SELECT COUNT(kinship) FROM Kinship kinship WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "kinship.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Kinship exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Kinship exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(KinshipPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Kinship _nullKinship = new KinshipImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Kinship> toCacheModel() {
                return _nullKinshipCacheModel;
            }
        };

    private static CacheModel<Kinship> _nullKinshipCacheModel = new CacheModel<Kinship>() {
            @Override
            public Kinship toEntityModel() {
                return _nullKinship;
            }
        };

    public KinshipPersistenceImpl() {
        setModelClass(Kinship.class);
    }

    /**
     * Returns all the kinships where personId = &#63;.
     *
     * @param personId the person ID
     * @return the matching kinships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Kinship> findByPerson(long personId) throws SystemException {
        return findByPerson(personId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the kinships where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of kinships
     * @param end the upper bound of the range of kinships (not inclusive)
     * @return the range of matching kinships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Kinship> findByPerson(long personId, int start, int end)
        throws SystemException {
        return findByPerson(personId, start, end, null);
    }

    /**
     * Returns an ordered range of all the kinships where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of kinships
     * @param end the upper bound of the range of kinships (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching kinships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Kinship> findByPerson(long personId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId, start, end, orderByComparator };
        }

        List<Kinship> list = (List<Kinship>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Kinship kinship : list) {
                if ((personId != kinship.getPersonId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_KINSHIP_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(KinshipModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                if (!pagination) {
                    list = (List<Kinship>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Kinship>(list);
                } else {
                    list = (List<Kinship>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first kinship in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching kinship
     * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a matching kinship could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship findByPerson_First(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchKinshipException, SystemException {
        Kinship kinship = fetchByPerson_First(personId, orderByComparator);

        if (kinship != null) {
            return kinship;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchKinshipException(msg.toString());
    }

    /**
     * Returns the first kinship in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching kinship, or <code>null</code> if a matching kinship could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship fetchByPerson_First(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Kinship> list = findByPerson(personId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last kinship in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching kinship
     * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a matching kinship could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship findByPerson_Last(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchKinshipException, SystemException {
        Kinship kinship = fetchByPerson_Last(personId, orderByComparator);

        if (kinship != null) {
            return kinship;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchKinshipException(msg.toString());
    }

    /**
     * Returns the last kinship in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching kinship, or <code>null</code> if a matching kinship could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship fetchByPerson_Last(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByPerson(personId);

        if (count == 0) {
            return null;
        }

        List<Kinship> list = findByPerson(personId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the kinships before and after the current kinship in the ordered set where personId = &#63;.
     *
     * @param id the primary key of the current kinship
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next kinship
     * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a kinship with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship[] findByPerson_PrevAndNext(long id, long personId,
        OrderByComparator orderByComparator)
        throws NoSuchKinshipException, SystemException {
        Kinship kinship = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Kinship[] array = new KinshipImpl[3];

            array[0] = getByPerson_PrevAndNext(session, kinship, personId,
                    orderByComparator, true);

            array[1] = kinship;

            array[2] = getByPerson_PrevAndNext(session, kinship, personId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Kinship getByPerson_PrevAndNext(Session session, Kinship kinship,
        long personId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_KINSHIP_WHERE);

        query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(KinshipModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(personId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(kinship);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Kinship> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the kinships where personId = &#63; from the database.
     *
     * @param personId the person ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByPerson(long personId) throws SystemException {
        for (Kinship kinship : findByPerson(personId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(kinship);
        }
    }

    /**
     * Returns the number of kinships where personId = &#63;.
     *
     * @param personId the person ID
     * @return the number of matching kinships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByPerson(long personId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PERSON;

        Object[] finderArgs = new Object[] { personId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_KINSHIP_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the kinship in the entity cache if it is enabled.
     *
     * @param kinship the kinship
     */
    @Override
    public void cacheResult(Kinship kinship) {
        EntityCacheUtil.putResult(KinshipModelImpl.ENTITY_CACHE_ENABLED,
            KinshipImpl.class, kinship.getPrimaryKey(), kinship);

        kinship.resetOriginalValues();
    }

    /**
     * Caches the kinships in the entity cache if it is enabled.
     *
     * @param kinships the kinships
     */
    @Override
    public void cacheResult(List<Kinship> kinships) {
        for (Kinship kinship : kinships) {
            if (EntityCacheUtil.getResult(
                        KinshipModelImpl.ENTITY_CACHE_ENABLED,
                        KinshipImpl.class, kinship.getPrimaryKey()) == null) {
                cacheResult(kinship);
            } else {
                kinship.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all kinships.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(KinshipImpl.class.getName());
        }

        EntityCacheUtil.clearCache(KinshipImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the kinship.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Kinship kinship) {
        EntityCacheUtil.removeResult(KinshipModelImpl.ENTITY_CACHE_ENABLED,
            KinshipImpl.class, kinship.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Kinship> kinships) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Kinship kinship : kinships) {
            EntityCacheUtil.removeResult(KinshipModelImpl.ENTITY_CACHE_ENABLED,
                KinshipImpl.class, kinship.getPrimaryKey());
        }
    }

    /**
     * Creates a new kinship with the primary key. Does not add the kinship to the database.
     *
     * @param id the primary key for the new kinship
     * @return the new kinship
     */
    @Override
    public Kinship create(long id) {
        Kinship kinship = new KinshipImpl();

        kinship.setNew(true);
        kinship.setPrimaryKey(id);

        return kinship;
    }

    /**
     * Removes the kinship with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the kinship
     * @return the kinship that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a kinship with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship remove(long id)
        throws NoSuchKinshipException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the kinship with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the kinship
     * @return the kinship that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a kinship with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship remove(Serializable primaryKey)
        throws NoSuchKinshipException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Kinship kinship = (Kinship) session.get(KinshipImpl.class,
                    primaryKey);

            if (kinship == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchKinshipException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(kinship);
        } catch (NoSuchKinshipException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Kinship removeImpl(Kinship kinship) throws SystemException {
        kinship = toUnwrappedModel(kinship);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(kinship)) {
                kinship = (Kinship) session.get(KinshipImpl.class,
                        kinship.getPrimaryKeyObj());
            }

            if (kinship != null) {
                session.delete(kinship);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (kinship != null) {
            clearCache(kinship);
        }

        return kinship;
    }

    @Override
    public Kinship updateImpl(br.com.atilo.jcondo.manager.model.Kinship kinship)
        throws SystemException {
        kinship = toUnwrappedModel(kinship);

        boolean isNew = kinship.isNew();

        KinshipModelImpl kinshipModelImpl = (KinshipModelImpl) kinship;

        Session session = null;

        try {
            session = openSession();

            if (kinship.isNew()) {
                session.save(kinship);

                kinship.setNew(false);
            } else {
                session.merge(kinship);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !KinshipModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((kinshipModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        kinshipModelImpl.getOriginalPersonId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);

                args = new Object[] { kinshipModelImpl.getPersonId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);
            }
        }

        EntityCacheUtil.putResult(KinshipModelImpl.ENTITY_CACHE_ENABLED,
            KinshipImpl.class, kinship.getPrimaryKey(), kinship);

        return kinship;
    }

    protected Kinship toUnwrappedModel(Kinship kinship) {
        if (kinship instanceof KinshipImpl) {
            return kinship;
        }

        KinshipImpl kinshipImpl = new KinshipImpl();

        kinshipImpl.setNew(kinship.isNew());
        kinshipImpl.setPrimaryKey(kinship.getPrimaryKey());

        kinshipImpl.setId(kinship.getId());
        kinshipImpl.setPersonId(kinship.getPersonId());
        kinshipImpl.setRelativeId(kinship.getRelativeId());
        kinshipImpl.setTypeId(kinship.getTypeId());
        kinshipImpl.setOprDate(kinship.getOprDate());
        kinshipImpl.setOprUser(kinship.getOprUser());

        return kinshipImpl;
    }

    /**
     * Returns the kinship with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the kinship
     * @return the kinship
     * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a kinship with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship findByPrimaryKey(Serializable primaryKey)
        throws NoSuchKinshipException, SystemException {
        Kinship kinship = fetchByPrimaryKey(primaryKey);

        if (kinship == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchKinshipException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return kinship;
    }

    /**
     * Returns the kinship with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchKinshipException} if it could not be found.
     *
     * @param id the primary key of the kinship
     * @return the kinship
     * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a kinship with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship findByPrimaryKey(long id)
        throws NoSuchKinshipException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the kinship with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the kinship
     * @return the kinship, or <code>null</code> if a kinship with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Kinship kinship = (Kinship) EntityCacheUtil.getResult(KinshipModelImpl.ENTITY_CACHE_ENABLED,
                KinshipImpl.class, primaryKey);

        if (kinship == _nullKinship) {
            return null;
        }

        if (kinship == null) {
            Session session = null;

            try {
                session = openSession();

                kinship = (Kinship) session.get(KinshipImpl.class, primaryKey);

                if (kinship != null) {
                    cacheResult(kinship);
                } else {
                    EntityCacheUtil.putResult(KinshipModelImpl.ENTITY_CACHE_ENABLED,
                        KinshipImpl.class, primaryKey, _nullKinship);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(KinshipModelImpl.ENTITY_CACHE_ENABLED,
                    KinshipImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return kinship;
    }

    /**
     * Returns the kinship with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the kinship
     * @return the kinship, or <code>null</code> if a kinship with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Kinship fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the kinships.
     *
     * @return the kinships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Kinship> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the kinships.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of kinships
     * @param end the upper bound of the range of kinships (not inclusive)
     * @return the range of kinships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Kinship> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the kinships.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of kinships
     * @param end the upper bound of the range of kinships (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of kinships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Kinship> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Kinship> list = (List<Kinship>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_KINSHIP);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_KINSHIP;

                if (pagination) {
                    sql = sql.concat(KinshipModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Kinship>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Kinship>(list);
                } else {
                    list = (List<Kinship>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the kinships from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Kinship kinship : findAll()) {
            remove(kinship);
        }
    }

    /**
     * Returns the number of kinships.
     *
     * @return the number of kinships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_KINSHIP);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the kinship persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Kinship")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Kinship>> listenersList = new ArrayList<ModelListener<Kinship>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Kinship>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(KinshipImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
