package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchPersonException;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.model.impl.PersonImpl;
import br.com.atilo.jcondo.manager.model.impl.PersonModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.PersonPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the person service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PersonPersistence
 * @see PersonUtil
 * @generated
 */
public class PersonPersistenceImpl extends BasePersistenceImpl<Person>
    implements PersonPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PersonUtil} to access the person persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PersonImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonModelImpl.FINDER_CACHE_ENABLED, PersonImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonModelImpl.FINDER_CACHE_ENABLED, PersonImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_IDENTITY = new FinderPath(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonModelImpl.FINDER_CACHE_ENABLED, PersonImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByIdentity",
            new String[] { String.class.getName() },
            PersonModelImpl.IDENTITY_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_IDENTITY = new FinderPath(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdentity",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_IDENTITY_IDENTITY_1 = "person.identity IS NULL";
    private static final String _FINDER_COLUMN_IDENTITY_IDENTITY_2 = "person.identity = ?";
    private static final String _FINDER_COLUMN_IDENTITY_IDENTITY_3 = "(person.identity IS NULL OR person.identity = '')";
    public static final FinderPath FINDER_PATH_FETCH_BY_USER = new FinderPath(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonModelImpl.FINDER_CACHE_ENABLED, PersonImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByUser",
            new String[] { Long.class.getName() },
            PersonModelImpl.USERID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_USER = new FinderPath(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByUser",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_USER_USERID_2 = "person.userId = ?";
    private static final String _SQL_SELECT_PERSON = "SELECT person FROM Person person";
    private static final String _SQL_SELECT_PERSON_WHERE = "SELECT person FROM Person person WHERE ";
    private static final String _SQL_COUNT_PERSON = "SELECT COUNT(person) FROM Person person";
    private static final String _SQL_COUNT_PERSON_WHERE = "SELECT COUNT(person) FROM Person person WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "person.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Person exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Person exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PersonPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Person _nullPerson = new PersonImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Person> toCacheModel() {
                return _nullPersonCacheModel;
            }
        };

    private static CacheModel<Person> _nullPersonCacheModel = new CacheModel<Person>() {
            @Override
            public Person toEntityModel() {
                return _nullPerson;
            }
        };

    public PersonPersistenceImpl() {
        setModelClass(Person.class);
    }

    /**
     * Returns the person where identity = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchPersonException} if it could not be found.
     *
     * @param identity the identity
     * @return the matching person
     * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a matching person could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person findByIdentity(String identity)
        throws NoSuchPersonException, SystemException {
        Person person = fetchByIdentity(identity);

        if (person == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("identity=");
            msg.append(identity);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchPersonException(msg.toString());
        }

        return person;
    }

    /**
     * Returns the person where identity = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param identity the identity
     * @return the matching person, or <code>null</code> if a matching person could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person fetchByIdentity(String identity) throws SystemException {
        return fetchByIdentity(identity, true);
    }

    /**
     * Returns the person where identity = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param identity the identity
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching person, or <code>null</code> if a matching person could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person fetchByIdentity(String identity, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { identity };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_IDENTITY,
                    finderArgs, this);
        }

        if (result instanceof Person) {
            Person person = (Person) result;

            if (!Validator.equals(identity, person.getIdentity())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_PERSON_WHERE);

            boolean bindIdentity = false;

            if (identity == null) {
                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_1);
            } else if (identity.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_3);
            } else {
                bindIdentity = true;

                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindIdentity) {
                    qPos.add(identity);
                }

                List<Person> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "PersonPersistenceImpl.fetchByIdentity(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Person person = list.get(0);

                    result = person;

                    cacheResult(person);

                    if ((person.getIdentity() == null) ||
                            !person.getIdentity().equals(identity)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY,
                            finderArgs, person);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDENTITY,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Person) result;
        }
    }

    /**
     * Removes the person where identity = &#63; from the database.
     *
     * @param identity the identity
     * @return the person that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person removeByIdentity(String identity)
        throws NoSuchPersonException, SystemException {
        Person person = findByIdentity(identity);

        return remove(person);
    }

    /**
     * Returns the number of persons where identity = &#63;.
     *
     * @param identity the identity
     * @return the number of matching persons
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByIdentity(String identity) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_IDENTITY;

        Object[] finderArgs = new Object[] { identity };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PERSON_WHERE);

            boolean bindIdentity = false;

            if (identity == null) {
                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_1);
            } else if (identity.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_3);
            } else {
                bindIdentity = true;

                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindIdentity) {
                    qPos.add(identity);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the person where userId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchPersonException} if it could not be found.
     *
     * @param userId the user ID
     * @return the matching person
     * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a matching person could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person findByUser(long userId)
        throws NoSuchPersonException, SystemException {
        Person person = fetchByUser(userId);

        if (person == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("userId=");
            msg.append(userId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchPersonException(msg.toString());
        }

        return person;
    }

    /**
     * Returns the person where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param userId the user ID
     * @return the matching person, or <code>null</code> if a matching person could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person fetchByUser(long userId) throws SystemException {
        return fetchByUser(userId, true);
    }

    /**
     * Returns the person where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param userId the user ID
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching person, or <code>null</code> if a matching person could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person fetchByUser(long userId, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { userId };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_USER,
                    finderArgs, this);
        }

        if (result instanceof Person) {
            Person person = (Person) result;

            if ((userId != person.getUserId())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_PERSON_WHERE);

            query.append(_FINDER_COLUMN_USER_USERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(userId);

                List<Person> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USER,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "PersonPersistenceImpl.fetchByUser(long, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Person person = list.get(0);

                    result = person;

                    cacheResult(person);

                    if ((person.getUserId() != userId)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USER,
                            finderArgs, person);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USER,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Person) result;
        }
    }

    /**
     * Removes the person where userId = &#63; from the database.
     *
     * @param userId the user ID
     * @return the person that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person removeByUser(long userId)
        throws NoSuchPersonException, SystemException {
        Person person = findByUser(userId);

        return remove(person);
    }

    /**
     * Returns the number of persons where userId = &#63;.
     *
     * @param userId the user ID
     * @return the number of matching persons
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByUser(long userId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_USER;

        Object[] finderArgs = new Object[] { userId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PERSON_WHERE);

            query.append(_FINDER_COLUMN_USER_USERID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(userId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the person in the entity cache if it is enabled.
     *
     * @param person the person
     */
    @Override
    public void cacheResult(Person person) {
        EntityCacheUtil.putResult(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonImpl.class, person.getPrimaryKey(), person);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY,
            new Object[] { person.getIdentity() }, person);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USER,
            new Object[] { person.getUserId() }, person);

        person.resetOriginalValues();
    }

    /**
     * Caches the persons in the entity cache if it is enabled.
     *
     * @param persons the persons
     */
    @Override
    public void cacheResult(List<Person> persons) {
        for (Person person : persons) {
            if (EntityCacheUtil.getResult(
                        PersonModelImpl.ENTITY_CACHE_ENABLED, PersonImpl.class,
                        person.getPrimaryKey()) == null) {
                cacheResult(person);
            } else {
                person.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all persons.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PersonImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PersonImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the person.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Person person) {
        EntityCacheUtil.removeResult(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonImpl.class, person.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(person);
    }

    @Override
    public void clearCache(List<Person> persons) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Person person : persons) {
            EntityCacheUtil.removeResult(PersonModelImpl.ENTITY_CACHE_ENABLED,
                PersonImpl.class, person.getPrimaryKey());

            clearUniqueFindersCache(person);
        }
    }

    protected void cacheUniqueFindersCache(Person person) {
        if (person.isNew()) {
            Object[] args = new Object[] { person.getIdentity() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDENTITY, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY, args,
                person);

            args = new Object[] { person.getUserId() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USER, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USER, args, person);
        } else {
            PersonModelImpl personModelImpl = (PersonModelImpl) person;

            if ((personModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_IDENTITY.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { person.getIdentity() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDENTITY, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY, args,
                    person);
            }

            if ((personModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_USER.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { person.getUserId() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_USER, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_USER, args,
                    person);
            }
        }
    }

    protected void clearUniqueFindersCache(Person person) {
        PersonModelImpl personModelImpl = (PersonModelImpl) person;

        Object[] args = new Object[] { person.getIdentity() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDENTITY, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDENTITY, args);

        if ((personModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_IDENTITY.getColumnBitmask()) != 0) {
            args = new Object[] { personModelImpl.getOriginalIdentity() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDENTITY, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDENTITY, args);
        }

        args = new Object[] { person.getUserId() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USER, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USER, args);

        if ((personModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_USER.getColumnBitmask()) != 0) {
            args = new Object[] { personModelImpl.getOriginalUserId() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_USER, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_USER, args);
        }
    }

    /**
     * Creates a new person with the primary key. Does not add the person to the database.
     *
     * @param id the primary key for the new person
     * @return the new person
     */
    @Override
    public Person create(long id) {
        Person person = new PersonImpl();

        person.setNew(true);
        person.setPrimaryKey(id);

        return person;
    }

    /**
     * Removes the person with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the person
     * @return the person that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a person with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person remove(long id) throws NoSuchPersonException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the person with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the person
     * @return the person that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a person with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person remove(Serializable primaryKey)
        throws NoSuchPersonException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Person person = (Person) session.get(PersonImpl.class, primaryKey);

            if (person == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPersonException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(person);
        } catch (NoSuchPersonException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Person removeImpl(Person person) throws SystemException {
        person = toUnwrappedModel(person);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(person)) {
                person = (Person) session.get(PersonImpl.class,
                        person.getPrimaryKeyObj());
            }

            if (person != null) {
                session.delete(person);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (person != null) {
            clearCache(person);
        }

        return person;
    }

    @Override
    public Person updateImpl(br.com.atilo.jcondo.manager.model.Person person)
        throws SystemException {
        person = toUnwrappedModel(person);

        boolean isNew = person.isNew();

        Session session = null;

        try {
            session = openSession();

            if (person.isNew()) {
                session.save(person);

                person.setNew(false);
            } else {
                session.merge(person);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !PersonModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(PersonModelImpl.ENTITY_CACHE_ENABLED,
            PersonImpl.class, person.getPrimaryKey(), person);

        clearUniqueFindersCache(person);
        cacheUniqueFindersCache(person);

        return person;
    }

    protected Person toUnwrappedModel(Person person) {
        if (person instanceof PersonImpl) {
            return person;
        }

        PersonImpl personImpl = new PersonImpl();

        personImpl.setNew(person.isNew());
        personImpl.setPrimaryKey(person.getPrimaryKey());

        personImpl.setId(person.getId());
        personImpl.setUserId(person.getUserId());
        personImpl.setDomainId(person.getDomainId());
        personImpl.setIdentity(person.getIdentity());
        personImpl.setRemark(person.getRemark());
        personImpl.setOprDate(person.getOprDate());
        personImpl.setOprUser(person.getOprUser());

        return personImpl;
    }

    /**
     * Returns the person with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the person
     * @return the person
     * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a person with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person findByPrimaryKey(Serializable primaryKey)
        throws NoSuchPersonException, SystemException {
        Person person = fetchByPrimaryKey(primaryKey);

        if (person == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchPersonException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return person;
    }

    /**
     * Returns the person with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchPersonException} if it could not be found.
     *
     * @param id the primary key of the person
     * @return the person
     * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a person with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person findByPrimaryKey(long id)
        throws NoSuchPersonException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the person with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the person
     * @return the person, or <code>null</code> if a person with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Person person = (Person) EntityCacheUtil.getResult(PersonModelImpl.ENTITY_CACHE_ENABLED,
                PersonImpl.class, primaryKey);

        if (person == _nullPerson) {
            return null;
        }

        if (person == null) {
            Session session = null;

            try {
                session = openSession();

                person = (Person) session.get(PersonImpl.class, primaryKey);

                if (person != null) {
                    cacheResult(person);
                } else {
                    EntityCacheUtil.putResult(PersonModelImpl.ENTITY_CACHE_ENABLED,
                        PersonImpl.class, primaryKey, _nullPerson);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(PersonModelImpl.ENTITY_CACHE_ENABLED,
                    PersonImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return person;
    }

    /**
     * Returns the person with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the person
     * @return the person, or <code>null</code> if a person with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Person fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the persons.
     *
     * @return the persons
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Person> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the persons.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PersonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of persons
     * @param end the upper bound of the range of persons (not inclusive)
     * @return the range of persons
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Person> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the persons.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PersonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of persons
     * @param end the upper bound of the range of persons (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of persons
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Person> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Person> list = (List<Person>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PERSON);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PERSON;

                if (pagination) {
                    sql = sql.concat(PersonModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Person>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Person>(list);
                } else {
                    list = (List<Person>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the persons from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Person person : findAll()) {
            remove(person);
        }
    }

    /**
     * Returns the number of persons.
     *
     * @return the number of persons
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PERSON);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the person persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Person")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Person>> listenersList = new ArrayList<ModelListener<Person>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Person>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PersonImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
