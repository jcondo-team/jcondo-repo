/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.datatype.KinType;

/**
 * The extended model implementation for the Kinship service. Represents a row in the &quot;jco_kinship&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Kinship} interface.
 * </p>
 *
 * @author anderson
 */
public class KinshipImpl extends KinshipBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a kinship model instance should use the {@link br.com.atilo.jcondo.manager.model.Kinship} interface instead.
	 */
	public KinshipImpl() {
	}

	public KinType getType() {
		return KinType.valueOf(getTypeId());
	}
	
	public void setType(KinType type) {
		setTypeId(type != null ? type.ordinal() : -1);
	}

}