/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.ImageLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.DuplicateVehicleLicenseException;
import br.com.atilo.jcondo.manager.NoSuchDomainException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.NoSuchVehicleException;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.datatype.VehicleType;
import br.com.atilo.jcondo.manager.service.base.VehicleLocalServiceBaseImpl;

/**
 * The implementation of the vehicle local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.VehicleLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.VehicleLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil
 */
public class VehicleLocalServiceImpl extends VehicleLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil} to access the vehicle local service.
	 */
	private static final String LICENSE_PATTERN = "[A-Za-z]{3,3}[0-9]{1,1}[A-Za-z0-9]{1,1}[0-9]{2,2}";
	
	public Vehicle addVehicle(String license, VehicleType type, long domainId, Image portrait, String brand, String color, String remark) throws PortalException, SystemException {
		if (StringUtils.isEmpty(license)) {
			throw new BusinessException("exception.vehicle.license.empty");
		}

		String l = license.toUpperCase().replaceAll("[^A-Za-z0-9]", "");

		if(type != VehicleType.BIKE && !l.matches(LICENSE_PATTERN)) {
			throw new BusinessException("exception.vehicle.license.invalid", license);
		}

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		try {
			throw new DuplicateVehicleLicenseException("exception.vehicle.duplicate.license", 
													   vehiclePersistence.findByLicense(l));
		} catch (NoSuchVehicleException e) {
		}

		validateDomain(domainId);

		if (domainId > 0 && type == VehicleType.CAR) {
			parkingLocalService.checkParkingAvailability(domainId);
		}

		Vehicle v = vehiclePersistence.create(counterLocalService.increment());
		v.setDomainId(domainId);
		v.setLicense(l);
		v.setType(type);
		v.setBrand(brand);
		v.setColor(color);
		v.setRemark(remark);
		v.setOprDate(new Date());
		v.setOprUser(sc.getUserId());
		v = addVehicle(v);

		return updateVehiclePortrait(v.getId(), portrait);
	}

	public Vehicle updateVehicle(long id, VehicleType type, long domainId, Image portrait, String brand, String color, String remark) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Vehicle v = getVehicle(id);

		validateDomain(domainId);

		if (domainId > 0 && v.getDomainId() != domainId && type == VehicleType.CAR) {
			parkingLocalService.checkParkingAvailability(domainId);
		}		

		v.setDomainId(domainId);
		v.setType(type);
		v.setBrand(brand);
		v.setColor(color);
		v.setRemark(remark);
		v.setOprDate(new Date());
		v.setOprUser(sc.getUserId());

		updateVehicle(v);

		return updateVehiclePortrait(id, portrait);
	}

	public Vehicle updateVehiclePortrait(long id, Image portrait) throws PortalException, SystemException {
		try {	
			Vehicle v = getVehicle(id);
			long imageId = 0;
			
			if(portrait == null) {
				if (v.getImageId() > 0) {
					ImageLocalServiceUtil.deleteImage(v.getImageId());
				} else {
					return v;
				}
			} else {
				Image image = v.getPortrait();

				if (image == null || !portrait.getPath().equals(image.getPath())) {
					URL url = new URL(portrait.getPath());
					imageId = image == null ? counterLocalService.increment() : image.getId();
					ImageLocalServiceUtil.updateImage(imageId, url.openStream());
				} else {
					return v;
				}
			}

			v.setImageId(imageId);
			v.setOprDate(new Date());
			v.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());

			return updateVehicle(v);
		} catch (MalformedURLException e) {
			throw new SystemException(e);
		} catch (IOException e) {
			throw new SystemException(e);
		}		
	}

	public Vehicle deleteVehicle(long id) throws PortalException, SystemException {
		Vehicle v = getVehicle(id);

		if (v.getDomainId() == 0) {
			return v;
		}

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();

		v.setDomainId(0);
		v.setOprDate(new Date());
		v.setOprUser(sc.getUserId());

		return updateVehicle(v);
	}
	
	public Vehicle getVehicleByLicense(String license) throws PortalException, SystemException {
		return vehiclePersistence.findByLicense(license.replaceAll("[^A-Za-z0-9]", "").toUpperCase());
	}

	@SuppressWarnings("unchecked")
	public List<Vehicle> getVehicles(String license) throws PortalException, SystemException {
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.like("license", "%" + license.replaceAll("[^A-Za-z0-9]", "").toUpperCase() + "%"));

		return new ArrayList<Vehicle>(vehiclePersistence.findWithDynamicQuery(dq));
	}	

	@SuppressWarnings("unchecked")
	public List<Vehicle> getVehicles(Map<String, String> params) throws PortalException, SystemException {
		DynamicQuery dq = dynamicQuery();

		for (String param : params.keySet()) {
			if (params.get(param) != null) {
				dq.add(RestrictionsFactoryUtil.like(param, "%" + params.get(param) + "%"));
			}
		}

		return new ArrayList<Vehicle>(vehiclePersistence.findWithDynamicQuery(dq));
	}	

	@SuppressWarnings("unchecked")
	public List<Vehicle> getDomainVehiclesByLicense(long domainId, String license) throws PortalException, SystemException {
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.eq("domainId", domainId), 
										   RestrictionsFactoryUtil.like("license", "%" + license.replaceAll("[^A-Za-z0-9]", "").toUpperCase() + "%")));

		return new ArrayList<Vehicle>(vehiclePersistence.findWithDynamicQuery(dq));
	}	

	public List<Vehicle> getDomainVehicles(long domainId) throws SystemException {
		return new ArrayList<Vehicle>(vehiclePersistence.findByDomain(domainId));
	}

	private void validateDomain(long domainId) throws PortalException, SystemException {
		if (domainId == 0) {
			return;
		}

		try {
			flatLocalService.getFlat(domainId);
		} catch (NoSuchFlatException e) {
			try {
				enterpriseLocalService.getEnterprise(domainId);
			} catch (NoSuchEnterpriseException ex) {
				throw new NoSuchDomainException();
			}
		}
	}
}