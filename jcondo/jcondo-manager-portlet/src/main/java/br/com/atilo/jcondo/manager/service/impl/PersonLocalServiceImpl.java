/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import com.liferay.portal.DuplicateUserEmailAddressException;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.User;
import com.liferay.portal.security.auth.Authenticator;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;

import br.com.atilo.jcondo.commons.collections.PersonTransformer;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.NoSuchMembershipException;
import br.com.atilo.jcondo.manager.NoSuchPersonException;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.Gender;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.service.base.PersonLocalServiceBaseImpl;
import br.com.caelum.stella.validation.CPFValidator;

/**
 * The implementation of the person local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.PersonLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.PersonLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil
 */
public class PersonLocalServiceImpl extends PersonLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil} to access the person local service.
	 */
	private void validateIdentity(long personId, String identity, PersonType personType) throws PortalException, SystemException {
		if (!StringUtils.isEmpty(identity)) {
			try {
				new CPFValidator().assertValid(identity.replaceAll("[^0-9]+", ""));
			} catch (Exception e) {
				throw new BusinessException("exception.person.identity.not.valid", identity);
			}

			try {
				Person p = personPersistence.findByIdentity(identity);
				if (p.getId() != personId) {
					throw new BusinessException("exception.person.identity.exists", p.getIdentity());
				}
			} catch (NoSuchPersonException e) {
			}
//		} else {
//			if (personType != PersonType.DEPENDENT) {
//				throw new BusinessException("exception.person.identity.empty", identity);
//			}
		}	
	}

	public Person createPerson() throws PortalException, SystemException {
		Person p = super.createPerson(0);
		p.setUser(userLocalService.createUser(0));
		return p;
	}
	
	public Person addPerson(String name, String surname, String identity, String email, Date birthday, Gender gender, PersonType personType, String remark, long domainId) throws Exception {
		validateIdentity(0, identity, personType);
		
		try {
			ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(birthday == null ? new Date() : birthday);		
			boolean isMale = gender == Gender.MALE;

			User user = userLocalService.addUser(sc.getUserId(), sc.getCompanyId(), true, StringUtils.EMPTY, StringUtils.EMPTY, 
												 true, StringUtils.EMPTY, StringUtils.defaultString(email), 0, StringUtils.EMPTY, 
												 sc.getLocale(), WordUtils.capitalizeFully(name.trim()), StringUtils.EMPTY, 
												 WordUtils.capitalizeFully(surname.trim()), 0, 0, isMale, 
												 calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 
												 calendar.get(Calendar.YEAR), StringUtils.EMPTY, null, null, null, null, false, 
												 new ServiceContext());

			Person person = personPersistence.create(counterLocalService.increment());
			person.setDomainId(domainId);
			person.setIdentity(identity);
			person.setUserId(user.getUserId());
			person.setRemark(remark);
			person.setOprDate(new Date());
			person.setOprUser(sc.getUserId());
			person.setUser(user);
			person = addPerson(person);

			membershipLocalService.addMembership(person.getId(), domainId, personType);

	    	if (!StringUtils.isEmpty(person.getEmail()) && 
					userLocalService.getUser(user.getUserId()).getStatus() == WorkflowConstants.STATUS_APPROVED) {
    			sc.setAttribute("autoPassword", true);
    			sc.setAttribute("sendEmail", true);
    			UserLocalServiceUtil.completeUserRegistration(person.getUser(), sc);
	    	}

			return person;
		} catch (DuplicateUserEmailAddressException e) {
			throw new BusinessException("person.email.already.exists", email);
		}
	}

	public Person updatePerson(long id, String name, String surname, String identity, String email, Date birthday, Gender gender, String remark, long domainId, Membership membership) throws Exception {
		validateIdentity(id, identity, membership.getType());

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Person person = getPerson(id);
		User user = person.getUser();

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(birthday == null ? user.getBirthday() : birthday);
		String emailAddress = email == null ? StringUtils.EMPTY : email;
		boolean isMale = gender == Gender.MALE;

		user = UserLocalServiceUtil.updateUser(person.getUserId(), user.getPassword(), StringUtils.EMPTY, StringUtils.EMPTY, true, 
											   user.getReminderQueryQuestion(), user.getReminderQueryAnswer(), user.getScreenName(), 
											   emailAddress, user.getFacebookId(), user.getOpenId(), user.getLanguageId(), 
											   user.getTimeZoneId(), user.getGreeting(), user.getComments(), 
											   WordUtils.capitalizeFully(name), StringUtils.EMPTY, WordUtils.capitalizeFully(surname), 
											   0, 0, isMale, calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), 
											   calendar.get(Calendar.YEAR), StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, 
											   StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, 
											   StringUtils.EMPTY, StringUtils.EMPTY, StringUtils.EMPTY, user.getJobTitle(), 
											   null, null, null, null, null, new ServiceContext());

		person.setDomainId(domainId);
		person.setIdentity(identity);
		person.setRemark(remark);
		person.setOprDate(new Date());
		person.setOprUser(sc.getUserId());

		person = updatePerson(person);

		person.setUser(user);

		try {
			Membership m = membershipLocalService.getDomainMembershipByPerson(membership.getDomainId(), person.getId());
			if (membership.getType() != m.getType()) {
				membershipLocalService.updateMembership(m.getId(), membership.getType());
			}
		} catch (NoSuchMembershipException e) {
			membershipLocalService.addMembership(person.getId(), membership.getDomainId(), membership.getType());
		}

		return person;
	}

	public Person updatePortrait(long personId, Image portrait) throws PortalException, SystemException {
		try {
			ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
			Person person = getPerson(personId);
			User user;

			if(portrait == null) { 
				if (person.getPortrait().getId() > 0) {
					UserLocalServiceUtil.deletePortrait(person.getUserId());
				} else {
					return person;
				}
			} else if (!StringUtils.isEmpty(portrait.getPath()) && 
							!portrait.getPath().equals(person.getPortrait().getPath())) {
				user = UserLocalServiceUtil.updatePortrait(person.getUserId(), 
														   IOUtils.toByteArray(new URL(portrait.getPath()).openStream()));
				person.setUser(user);
				new File(new URL(portrait.getPath()).getPath()).delete();
			} else {
				return person;
			}

			person.setOprDate(new Date());
			person.setOprUser(sc.getUserId());
			return updatePerson(person);
		} catch (MalformedURLException e) {
			throw new SystemException(e);
		} catch (IOException e) {
			throw new SystemException(e);
		}
	}
	
	public void updatePassword(long personId, String password, String newPassword) throws PortalException, SystemException {
		
	}

	public Person updateHome(long id, long flatId) throws PortalException, SystemException {
		Person person = personPersistence.findByPrimaryKey(id);
		person.setDomain(flatLocalService.getFlat(flatId));
		person.setOprDate(new Date());
		person.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());
		return updatePerson(person);
	}

	@SuppressWarnings("unchecked")
	public List<Person> getDomainPeopleByType(long domainId, PersonType personType) throws PortalException, SystemException {
		List<Membership> memberships = membershipLocalService.getDomainMembershipsByType(domainId, personType);
		
		if (memberships == Collections.EMPTY_LIST) {
			return new ArrayList<Person>();
		} else {
			return (List<Person>) CollectionUtils.collect(memberships, new PersonTransformer());
		}
	}

	@Override
	public Person getPerson(long id) throws PortalException, SystemException {
		Person p = super.getPerson(id);
		p.setUser(userLocalService.getUser(p.getUserId()));
		return p;
	}
	
	public Person getPerson() throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Person p = personPersistence.findByUser(sc.getUserId());
		p.setUser(userLocalService.getUser(sc.getUserId()));
		return p;
	}
	
	public Person getPersonByIdentity(String identity) throws PortalException, SystemException {
		Person p = personPersistence.findByIdentity(identity);
		p.setUser(userLocalService.getUser(p.getUserId()));
		return p;
	}

	@SuppressWarnings("unchecked")
	public List<Person> getPeopleByType(PersonType type) throws PortalException, SystemException {
		List<Membership> memberships = membershipPersistence.findByType(type.ordinal());
		return (List<Person>) CollectionUtils.collect(memberships, new PersonTransformer());
	}

	@SuppressWarnings("unchecked")
	public List<Person> getDomainPeople(long domainId) throws PortalException, SystemException {
		List<Membership> memberships = membershipPersistence.findByDomain(domainId);
		return (List<Person>) CollectionUtils.collect(memberships, new PersonTransformer());
	}

	public List<Person> getDomainPeopleByName(long domainId, String name) throws PortalException, SystemException {
		String[] names = name.toLowerCase().split(" ");
		List<Person> people = getDomainPeople(domainId);
		for (int i = people.size() - 1; i >= 0; i--) {
			String pn = people.get(i).getName().toLowerCase();
			String ps = people.get(i).getSurname().toLowerCase();
			for (String n : names) {
				if (!(pn.contains(n) || ps.contains(n))) {
					people.remove(i);
				}
			}
		}
		return people;
	}

	public List<Person> getPeople(String name, String identity, long domainId) throws PortalException, SystemException {
		List<Person> people = new ArrayList<Person>();

		for (Person person : personFinder.findPersonByNameIdentityDomain(name, identity, domainId)) {
			person.setUser(userLocalService.getUser(person.getUserId()));
			people.add(person);
		}

		return people;
	}	
	
	public void completePersonRegistration(
			long companyId, String emailAddress, boolean autoPassword, boolean sendEmail) throws PortalException, SystemException {
		ServiceContext sc = new ServiceContext();
		sc.setAttribute("autoPassword", autoPassword);
		sc.setAttribute("sendEmail", sendEmail);

		if (emailAddress == null || emailAddress.isEmpty()) {
			List<User> users = UserLocalServiceUtil.getUsers(-1, -1);
			for (User user : users) {
				if (user.getStatus() == WorkflowConstants.STATUS_DRAFT) {
					UserLocalServiceUtil.updateStatus(user.getUserId(), WorkflowConstants.STATUS_APPROVED);
					UserLocalServiceUtil.completeUserRegistration(user, sc);			
				}
			}
		} else {
			User user = UserLocalServiceUtil.getUserByEmailAddress(companyId, emailAddress);
			UserLocalServiceUtil.updateStatus(user.getUserId(), WorkflowConstants.STATUS_APPROVED);
			UserLocalServiceUtil.completeUserRegistration(user, sc);
		}
	}
	
	public boolean authenticatePerson(long personId, String password) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		return UserLocalServiceUtil.authenticateByUserId(sc.getCompanyId(), 
				 										 getPerson(personId).getUserId(), 
				 										 password, null, null, null) == Authenticator.SUCCESS ? true : false;

	}
}