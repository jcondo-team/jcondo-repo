package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Flat;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Flat in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Flat
 * @generated
 */
public class FlatCacheModel implements CacheModel<Flat>, Externalizable {
    public long id;
    public long organizationId;
    public long folderId;
    public long personId;
    public int block;
    public int number;
    public boolean pets;
    public boolean disables;
    public boolean brigade;
    public boolean defaulting;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(25);

        sb.append("{id=");
        sb.append(id);
        sb.append(", organizationId=");
        sb.append(organizationId);
        sb.append(", folderId=");
        sb.append(folderId);
        sb.append(", personId=");
        sb.append(personId);
        sb.append(", block=");
        sb.append(block);
        sb.append(", number=");
        sb.append(number);
        sb.append(", pets=");
        sb.append(pets);
        sb.append(", disables=");
        sb.append(disables);
        sb.append(", brigade=");
        sb.append(brigade);
        sb.append(", defaulting=");
        sb.append(defaulting);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Flat toEntityModel() {
        FlatImpl flatImpl = new FlatImpl();

        flatImpl.setId(id);
        flatImpl.setOrganizationId(organizationId);
        flatImpl.setFolderId(folderId);
        flatImpl.setPersonId(personId);
        flatImpl.setBlock(block);
        flatImpl.setNumber(number);
        flatImpl.setPets(pets);
        flatImpl.setDisables(disables);
        flatImpl.setBrigade(brigade);
        flatImpl.setDefaulting(defaulting);

        if (oprDate == Long.MIN_VALUE) {
            flatImpl.setOprDate(null);
        } else {
            flatImpl.setOprDate(new Date(oprDate));
        }

        flatImpl.setOprUser(oprUser);

        flatImpl.resetOriginalValues();

        return flatImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        organizationId = objectInput.readLong();
        folderId = objectInput.readLong();
        personId = objectInput.readLong();
        block = objectInput.readInt();
        number = objectInput.readInt();
        pets = objectInput.readBoolean();
        disables = objectInput.readBoolean();
        brigade = objectInput.readBoolean();
        defaulting = objectInput.readBoolean();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(organizationId);
        objectOutput.writeLong(folderId);
        objectOutput.writeLong(personId);
        objectOutput.writeInt(block);
        objectOutput.writeInt(number);
        objectOutput.writeBoolean(pets);
        objectOutput.writeBoolean(disables);
        objectOutput.writeBoolean(brigade);
        objectOutput.writeBoolean(defaulting);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
