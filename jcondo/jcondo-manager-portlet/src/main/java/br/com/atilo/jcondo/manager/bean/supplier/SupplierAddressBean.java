package br.com.atilo.jcondo.manager.bean.supplier;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.service.AddressLocalServiceUtil;

import com.liferay.portal.model.Address;
import com.liferay.portal.model.Region;
import com.liferay.portal.service.CountryServiceUtil;
import com.liferay.portal.service.RegionServiceUtil;

public class SupplierAddressBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(SupplierAddressBean.class);

	private Enterprise enterprise;

	private Address address;

	private List<Region> states;
	
	public void init(Enterprise enterprise) throws Exception {
		this.enterprise = enterprise;

		address = AddressLocalServiceUtil.getEnterpriseAddress(enterprise.getId());
		if (address == null) {
			address = AddressLocalServiceUtil.createAddress();
		}

		states = RegionServiceUtil.getRegions(CountryServiceUtil.getCountryByName("Brazil").getCountryId());
	}

	public void onAddressSave() {
		try {
			if (address.isNew()) {
				address = AddressLocalServiceUtil.addEnterpriseAddress(enterprise.getId(), address.getStreet1(), 
																	   address.getCity(), address.getRegionId(), 
																	   address.getZip());
			} else {
				address = AddressLocalServiceUtil.updateAddress(address.getAddressId(), address.getStreet1(), 
																address.getCity(), address.getRegionId(), 
																address.getZip());
			}

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.address.save", null);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on address save: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on address save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.address.save", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Region> getStates() {
		return states;
	}

	public void setStates(List<Region> states) {
		this.states = states;
	}

}