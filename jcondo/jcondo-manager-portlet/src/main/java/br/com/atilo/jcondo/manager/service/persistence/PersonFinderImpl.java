package br.com.atilo.jcondo.manager.service.persistence;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.model.impl.PersonImpl;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class PersonFinderImpl extends BasePersistenceImpl<Person> implements PersonFinder {

	public static final String FIND_OVERDUE_BOOKINGS = PersonFinder.class.getName() + ".findPersonByNameIdentityDomain";
	
	@SuppressWarnings("unchecked")
	public List<Person> findPersonByNameIdentityDomain(String name, String identity, long domainId) {
		Session session = null;

		try {
			session = openSession();

			String sql = CustomSQLUtil.get(FIND_OVERDUE_BOOKINGS);

			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("jco_person", PersonImpl.class);
			
	        QueryPos qPos = QueryPos.getInstance(q); 
	        qPos.add(name);
	        qPos.add(StringUtils.isEmpty(name) ? name : "%" + name.replaceAll(" ", "%").toLowerCase() + "%");
	        qPos.add(identity);
	        qPos.add(identity);
	        qPos.add(domainId);
	        qPos.add(domainId);

			return (List<Person>) QueryUtil.list(q, getDialect(), -1, -1);
		} catch (Exception e) {
			try {
				throw new SystemException(e);
			} catch (SystemException se) {
				se.printStackTrace();
			}
		} finally {
			closeSession(session);
		}

		return null;
	}
}
