package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The extended model base implementation for the Enterprise service. Represents a row in the &quot;jco_enterprise&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link EnterpriseImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EnterpriseImpl
 * @see br.com.atilo.jcondo.manager.model.Enterprise
 * @generated
 */
public abstract class EnterpriseBaseImpl extends EnterpriseModelImpl
    implements Enterprise {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a enterprise model instance should use the {@link Enterprise} interface instead.
     */
    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            EnterpriseLocalServiceUtil.addEnterprise(this);
        } else {
            EnterpriseLocalServiceUtil.updateEnterprise(this);
        }
    }
}
