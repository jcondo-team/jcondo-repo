package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchTelephoneException;
import br.com.atilo.jcondo.manager.model.Telephone;
import br.com.atilo.jcondo.manager.model.impl.TelephoneImpl;
import br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.TelephonePersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the telephone service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see TelephonePersistence
 * @see TelephoneUtil
 * @generated
 */
public class TelephonePersistenceImpl extends BasePersistenceImpl<Telephone>
    implements TelephonePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link TelephoneUtil} to access the telephone persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = TelephoneImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
            TelephoneModelImpl.FINDER_CACHE_ENABLED, TelephoneImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
            TelephoneModelImpl.FINDER_CACHE_ENABLED, TelephoneImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
            TelephoneModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_TELEPHONE = "SELECT telephone FROM Telephone telephone";
    private static final String _SQL_COUNT_TELEPHONE = "SELECT COUNT(telephone) FROM Telephone telephone";
    private static final String _ORDER_BY_ENTITY_ALIAS = "telephone.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Telephone exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(TelephonePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id", "number", "primary"
            });
    private static Telephone _nullTelephone = new TelephoneImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Telephone> toCacheModel() {
                return _nullTelephoneCacheModel;
            }
        };

    private static CacheModel<Telephone> _nullTelephoneCacheModel = new CacheModel<Telephone>() {
            @Override
            public Telephone toEntityModel() {
                return _nullTelephone;
            }
        };

    public TelephonePersistenceImpl() {
        setModelClass(Telephone.class);
    }

    /**
     * Caches the telephone in the entity cache if it is enabled.
     *
     * @param telephone the telephone
     */
    @Override
    public void cacheResult(Telephone telephone) {
        EntityCacheUtil.putResult(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
            TelephoneImpl.class, telephone.getPrimaryKey(), telephone);

        telephone.resetOriginalValues();
    }

    /**
     * Caches the telephones in the entity cache if it is enabled.
     *
     * @param telephones the telephones
     */
    @Override
    public void cacheResult(List<Telephone> telephones) {
        for (Telephone telephone : telephones) {
            if (EntityCacheUtil.getResult(
                        TelephoneModelImpl.ENTITY_CACHE_ENABLED,
                        TelephoneImpl.class, telephone.getPrimaryKey()) == null) {
                cacheResult(telephone);
            } else {
                telephone.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all telephones.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(TelephoneImpl.class.getName());
        }

        EntityCacheUtil.clearCache(TelephoneImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the telephone.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Telephone telephone) {
        EntityCacheUtil.removeResult(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
            TelephoneImpl.class, telephone.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Telephone> telephones) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Telephone telephone : telephones) {
            EntityCacheUtil.removeResult(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
                TelephoneImpl.class, telephone.getPrimaryKey());
        }
    }

    /**
     * Creates a new telephone with the primary key. Does not add the telephone to the database.
     *
     * @param id the primary key for the new telephone
     * @return the new telephone
     */
    @Override
    public Telephone create(long id) {
        Telephone telephone = new TelephoneImpl();

        telephone.setNew(true);
        telephone.setPrimaryKey(id);

        return telephone;
    }

    /**
     * Removes the telephone with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the telephone
     * @return the telephone that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchTelephoneException if a telephone with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Telephone remove(long id)
        throws NoSuchTelephoneException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the telephone with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the telephone
     * @return the telephone that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchTelephoneException if a telephone with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Telephone remove(Serializable primaryKey)
        throws NoSuchTelephoneException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Telephone telephone = (Telephone) session.get(TelephoneImpl.class,
                    primaryKey);

            if (telephone == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchTelephoneException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(telephone);
        } catch (NoSuchTelephoneException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Telephone removeImpl(Telephone telephone)
        throws SystemException {
        telephone = toUnwrappedModel(telephone);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(telephone)) {
                telephone = (Telephone) session.get(TelephoneImpl.class,
                        telephone.getPrimaryKeyObj());
            }

            if (telephone != null) {
                session.delete(telephone);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (telephone != null) {
            clearCache(telephone);
        }

        return telephone;
    }

    @Override
    public Telephone updateImpl(
        br.com.atilo.jcondo.manager.model.Telephone telephone)
        throws SystemException {
        telephone = toUnwrappedModel(telephone);

        boolean isNew = telephone.isNew();

        Session session = null;

        try {
            session = openSession();

            if (telephone.isNew()) {
                session.save(telephone);

                telephone.setNew(false);
            } else {
                session.merge(telephone);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
            TelephoneImpl.class, telephone.getPrimaryKey(), telephone);

        return telephone;
    }

    protected Telephone toUnwrappedModel(Telephone telephone) {
        if (telephone instanceof TelephoneImpl) {
            return telephone;
        }

        TelephoneImpl telephoneImpl = new TelephoneImpl();

        telephoneImpl.setNew(telephone.isNew());
        telephoneImpl.setPrimaryKey(telephone.getPrimaryKey());

        telephoneImpl.setId(telephone.getId());
        telephoneImpl.setTypeName(telephone.getTypeName());
        telephoneImpl.setExtension(telephone.getExtension());
        telephoneImpl.setNumber(telephone.getNumber());
        telephoneImpl.setPrimary(telephone.isPrimary());

        return telephoneImpl;
    }

    /**
     * Returns the telephone with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the telephone
     * @return the telephone
     * @throws br.com.atilo.jcondo.manager.NoSuchTelephoneException if a telephone with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Telephone findByPrimaryKey(Serializable primaryKey)
        throws NoSuchTelephoneException, SystemException {
        Telephone telephone = fetchByPrimaryKey(primaryKey);

        if (telephone == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchTelephoneException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return telephone;
    }

    /**
     * Returns the telephone with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchTelephoneException} if it could not be found.
     *
     * @param id the primary key of the telephone
     * @return the telephone
     * @throws br.com.atilo.jcondo.manager.NoSuchTelephoneException if a telephone with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Telephone findByPrimaryKey(long id)
        throws NoSuchTelephoneException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the telephone with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the telephone
     * @return the telephone, or <code>null</code> if a telephone with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Telephone fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Telephone telephone = (Telephone) EntityCacheUtil.getResult(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
                TelephoneImpl.class, primaryKey);

        if (telephone == _nullTelephone) {
            return null;
        }

        if (telephone == null) {
            Session session = null;

            try {
                session = openSession();

                telephone = (Telephone) session.get(TelephoneImpl.class,
                        primaryKey);

                if (telephone != null) {
                    cacheResult(telephone);
                } else {
                    EntityCacheUtil.putResult(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
                        TelephoneImpl.class, primaryKey, _nullTelephone);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(TelephoneModelImpl.ENTITY_CACHE_ENABLED,
                    TelephoneImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return telephone;
    }

    /**
     * Returns the telephone with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the telephone
     * @return the telephone, or <code>null</code> if a telephone with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Telephone fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the telephones.
     *
     * @return the telephones
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Telephone> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the telephones.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of telephones
     * @param end the upper bound of the range of telephones (not inclusive)
     * @return the range of telephones
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Telephone> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the telephones.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of telephones
     * @param end the upper bound of the range of telephones (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of telephones
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Telephone> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Telephone> list = (List<Telephone>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_TELEPHONE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_TELEPHONE;

                if (pagination) {
                    sql = sql.concat(TelephoneModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Telephone>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Telephone>(list);
                } else {
                    list = (List<Telephone>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the telephones from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Telephone telephone : findAll()) {
            remove(telephone);
        }
    }

    /**
     * Returns the number of telephones.
     *
     * @return the number of telephones
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_TELEPHONE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the telephone persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Telephone")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Telephone>> listenersList = new ArrayList<ModelListener<Telephone>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Telephone>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(TelephoneImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
