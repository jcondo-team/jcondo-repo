/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.model.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.OrganizationLocalServiceUtil;

import br.com.atilo.jcondo.manager.NoSuchPersonException;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PetType;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PetLocalServiceUtil;

/**
 * The extended model implementation for the Flat service. Represents a row in the &quot;jco_flat&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Flat} interface.
 * </p>
 *
 * @author anderson
 */
public class FlatImpl extends FlatBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a flat model instance should use the {@link br.com.atilo.jcondo.manager.model.Flat} interface instead.
	 */
	
	public FlatImpl() {
	}

	public String getName() throws PortalException, SystemException {
		return OrganizationLocalServiceUtil.getOrganization(getOrganizationId()).getName();
	}
	
	public Person getPerson() throws PortalException, SystemException {
		if (getPersonId() <= 0) {
			return null;
		}

		try {
			return PersonLocalServiceUtil.getPerson(getPersonId());
		} catch (NoSuchPersonException e) {
			return null;
		}
	}

	public List<PetType> getPetTypes() throws SystemException {
		return PetLocalServiceUtil.getPetTypes(getId());
	}

	public List<Person> getPeople() throws PortalException, SystemException {
		return PersonLocalServiceUtil.getDomainPeople(getId());
	}
}