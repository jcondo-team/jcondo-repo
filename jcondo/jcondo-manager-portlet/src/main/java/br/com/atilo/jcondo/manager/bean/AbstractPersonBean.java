package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

import javax.faces.application.FacesMessage;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.commons.collections.DomainPredicate;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.NoSuchPersonException;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.Gender;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.MembershipServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonServiceUtil;
import br.com.atilo.jcondo.manager.service.permission.PersonPermission;
import br.com.atilo.jcondo.manager.bean.CameraBean;
import br.com.atilo.jcondo.manager.bean.ImageUploadBean;

public abstract class AbstractPersonBean<Domain extends BaseModel<Domain>> extends Observable implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(AbstractPersonBean.class);

	protected ImageUploadBean imageUploadBean;

	protected CameraBean cameraBean;
	
	protected PersonPhoneBean phoneBean;

	protected List<PersonType> personTypes;

	protected List<Gender> genders;

	protected String identity;

	protected Person person;

	protected Membership membership;

	protected Domain domain;

	protected Image image;
	
	public AbstractPersonBean() {
		imageUploadBean = new ImageUploadBean(158, 240);
		cameraBean = new CameraBean(158, 240);
		phoneBean = new PersonPhoneBean();
		genders = Arrays.asList(Gender.values());
	}

	public void init(Domain domain) throws Exception {
		setDomain(domain);
		personTypes = PersonServiceUtil.getPersonTypes((Long) domain.getPrimaryKeyObj());
		onPersonCreate();
	}

	public void onPersonCreate() throws Exception {
		person = PersonServiceUtil.createPerson();
		person.setGender(Gender.MALE);
		person.setDomain(domain);
		membership = MembershipServiceUtil.createMembership(0);
		membership.setType(personTypes.get(0));
		membership.setDomain(domain);
		image = null;
		identity = null;
		phoneBean.init(person);
	}

	public void onPersonSave() throws Exception {
		Person p;
		if (person.isNew()) {
			p = PersonServiceUtil.addPerson(person.getName(), person.getSurname(), person.getIdentity(), 
											person.getEmail(), person.getBirthday(), person.getGender(),
											person.getRemark(), membership.getType(), membership.getDomainId());

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.person.add", null);
		} else {
			p = PersonServiceUtil.updatePerson(person.getId(), person.getName(), person.getSurname(), 
											   person.getIdentity(), person.getEmail(), person.getBirthday(), 
											   person.getGender(), person.getRemark(), person.getDomainId(), membership);

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.person.update", null);
		}

		p = PersonServiceUtil.updatePortrait(p.getId(), image);

		phoneBean.setPerson(p);
		phoneBean.onPhoneSave();

		setChanged();
		notifyObservers(p);
		
		RequestContext.getCurrentInstance().addCallbackParam("visitor", membership.getType() == PersonType.VISITOR);
	}

	public void onSearchByCPF(AjaxBehaviorEvent event) throws Exception {
		if (StringUtils.isEmpty(identity)) {
			person.setId(0);
			person.setUserId(0);
			person.setIdentity(null);
			person.setName(null);
			person.setSurname(null);
			person.setGender(Gender.MALE);
			imageUploadBean.setImage(person.getPortrait());
			return;
		}

		try {
			Person p = PersonServiceUtil.getPersonByIdentity(identity);
			PersonPermission.hasPermission(Permission.VIEW, p, (Long) domain.getPrimaryKeyObj());
			onPersonEdit(p);
//			if (model.getRowData(String.valueOf(p.getId())) != null) {
//				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "person.domain.exists", null, ":tabs:person-form:personMsg");
//			}
		} catch (NoSuchPersonException e) {
			LOGGER.debug("failure on onSearchByCPF method", e);
		} catch (BusinessException e) {
			LOGGER.debug("failure on onSearchByCPF method", e);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}

		person.setIdentity(identity);
	}

	public void onPersonEdit(Person person) throws Exception {
		this.person = PersonLocalServiceUtil.getPerson(person.getId());
		identity = this.person.getIdentity();
		imageUploadBean.setImage(this.person.getPortrait());
		image = imageUploadBean.getImage();

		Membership m = (Membership) CollectionUtils.find(this.person.getMemberships(), 
														 new DomainPredicate((Long) getDomain().getPrimaryKeyObj()));

		if (m != null) {
			membership = m;
		}
		
		phoneBean.init(this.person);
	}

	public boolean isVisitor(Person person) {
		try {
			Membership membership = (Membership) CollectionUtils.find(person.getMemberships(), 
																	  new DomainPredicate((Long) getDomain().getPrimaryKeyObj()));
	
			return membership != null ? membership.getType() == PersonType.VISITOR : false;				
		} catch (Exception e) {
			LOGGER.warn("Failure on display memberships", e);
		}
		
		return false;
	}
		
	public boolean canChangeType() throws Exception {
		return membership.getType() == null ? true : personTypes != null ? personTypes.contains(membership.getType()) : false;
	}

	public boolean canEditPerson(Person person) throws Exception {
		if (person.getId() <= 0) {
			return true;
		}
		return PersonPermission.hasPermission(Permission.UPDATE, person, (Long) domain.getPrimaryKeyObj());
	}

	public boolean canDeletePerson(Person person) throws Exception {
		long domainId = (Long) getDomain().getPrimaryKeyObj();
		return PersonPermission.hasPermission(Permission.REMOVE_MEMBER, person.getType(domainId), domainId);
	}

	public boolean canEditInfo() throws Exception {
		return person.getId() == 0 || person.getId() == PersonServiceUtil.getPerson().getId();
	}

	public ImageUploadBean getImageUploadBean() {
		return imageUploadBean;
	}

	public void setImageUploadBean(ImageUploadBean imageUploadBean) {
		this.imageUploadBean = imageUploadBean;
	}

	public CameraBean getCameraBean() {
		return cameraBean;
	}

	public void setCameraBean(CameraBean cameraBean) {
		this.cameraBean = cameraBean;
	}

	public PersonPhoneBean getPhoneBean() {
		return phoneBean;
	}

	public void setPhoneBean(PersonPhoneBean phoneBean) {
		this.phoneBean = phoneBean;
	}	
	
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public List<PersonType> getPersonTypes() {
		return personTypes;
	}

	public void setPersonTypes(List<PersonType> personTypes) {
		this.personTypes = personTypes;
	}

	public List<Gender> getGenders() {
		return genders;
	}

	public void setGenders(List<Gender> genders) {
		this.genders = genders;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Membership getMembership() {
		return membership;
	}

	public void setMembership(Membership membership) {
		this.membership = membership;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

}
