package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Kinship;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Kinship in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Kinship
 * @generated
 */
public class KinshipCacheModel implements CacheModel<Kinship>, Externalizable {
    public long id;
    public long personId;
    public long relativeId;
    public int typeId;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{id=");
        sb.append(id);
        sb.append(", personId=");
        sb.append(personId);
        sb.append(", relativeId=");
        sb.append(relativeId);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Kinship toEntityModel() {
        KinshipImpl kinshipImpl = new KinshipImpl();

        kinshipImpl.setId(id);
        kinshipImpl.setPersonId(personId);
        kinshipImpl.setRelativeId(relativeId);
        kinshipImpl.setTypeId(typeId);

        if (oprDate == Long.MIN_VALUE) {
            kinshipImpl.setOprDate(null);
        } else {
            kinshipImpl.setOprDate(new Date(oprDate));
        }

        kinshipImpl.setOprUser(oprUser);

        kinshipImpl.resetOriginalValues();

        return kinshipImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        personId = objectInput.readLong();
        relativeId = objectInput.readLong();
        typeId = objectInput.readInt();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(personId);
        objectOutput.writeLong(relativeId);
        objectOutput.writeInt(typeId);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
