package br.com.atilo.jcondo.manager.bean.access;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.component.autocomplete.AutoComplete;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.UnselectEvent;

import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil;
import br.com.atilo.jcondo.commons.collections.PersonNameComparator;
import br.com.atilo.jcondo.commons.collections.PersonNameTransformer;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.manager.service.AccessLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.AccessPermissionLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;


@SessionScoped
@ManagedBean
public class EntranceBean implements Observer {

	private static Logger LOGGER = Logger.getLogger(EntranceBean.class);
	
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("Language");
	
	private BaseModel<?> destination;

	private Vehicle vehicle;

	private String badge;

	private Person authorizer;

	private String authorizerName;

	private List<BaseModel<?>> domains;
	
	private EntreeVehicleBean vehicleBean;

	private EnterpriseBean enterpriseBean;

	private List<Enterprise> enterprises;

	private Enterprise enterprise;
	
	private PersonBean personBean;

	private Person visitor;

	private List<Person> visitors;
	
	private Guest guest;

	private List<Guest> guests;

	private String license;

	@PostConstruct
	public void init() {
		try {
			domains = new ArrayList<BaseModel<?>>();
			domains.addAll(DepartmentLocalServiceUtil.getDepartments());
			domains.addAll(FlatServiceUtil.getFlats());

			enterprises = EnterpriseLocalServiceUtil.getEnterprisesByType(DomainType.SUPPLIER);

			personBean = new PersonBean();
			personBean.addObserver(this);

			vehicleBean = new EntreeVehicleBean();
			vehicleBean.addObserver(this);
			
			enterpriseBean = new EnterpriseBean();
			enterpriseBean.addObserver(this);
		} catch (Exception e) {
			LOGGER.fatal("Failure on entrance access initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public void onAccessRegister() {
		try {
			if (vehicle == null && license != null) {
				RequestContext.getCurrentInstance().addCallbackParam("signon", true);
				onVehicleCreate();
				vehicleBean.getVehicle().setLicense(license);
				vehicleBean.setLicense(license);
				throw new BusinessException("veiculo nao encontrado"); 
			}

			AccessLocalServiceUtil.addAccess((Long) destination.getPrimaryKeyObj(), vehicle == null ? 0 : vehicle.getId(), 
											 authorizer == null ? 0 : authorizer.getId(), authorizerName, 
											 guest != null ? guest.getGuestId() : visitor.getId(), badge, 
											 guest != null ? "guest" : null);

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.access.register.success", null);

			destination = null;
			visitor = null;
			guest = null;
			guests = null;
			vehicle = null;
			enterprise = null;
			authorizer = null;
			authorizerName = null;
			badge = null;
			personBean.setDomain(null);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on access register : " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on access register", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public void onDestinationSelect() {
		try {
			if (enterprise != null) {
				return;
			}

			if (destination != null) {
				visitors = PersonLocalServiceUtil.getDomainPeople((Long) destination.getPrimaryKeyObj());
				guests = new ArrayList<Guest>();
				for (Booking b : BookingLocalServiceUtil.getDomainBookings((Long) destination.getPrimaryKeyObj(), new Date())) {
					CollectionUtils.addAll(guests, GuestLocalServiceUtil.getBookingGuests(b.getId()).iterator());
				}
				Collections.sort(visitors, new PersonNameComparator());
			} else {
				visitors = null;
				guests = null;
			}

			authorizer = null;
			authorizerName = null;
			visitor = null;
			guest = null;
			personBean.setDomain(destination);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void onEnterpriseSelect() {
		try {
			if (enterprise != null) {
				personBean.setDomain(enterprise);
				visitors = PersonLocalServiceUtil.getDomainPeople(enterprise.getId());
			} else {
				personBean.setDomain(destination);
				visitors = PersonLocalServiceUtil.getDomainPeople((Long) destination.getPrimaryKeyObj());
			}

			Collections.sort(visitors, new PersonNameComparator());

			authorizer = null;
			authorizerName = null;
			visitor = null;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}	

	public void onAuthorizerSelect(SelectEvent event) {
		try {
			authorizer = PersonLocalServiceUtil.getPeople((String) event.getObject(), "", (Long) destination.getPrimaryKeyObj()).get(0);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void onAuthorizerUnselect(UnselectEvent event) {
		try {
			authorizer = null;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}	
	
	@SuppressWarnings("unchecked")
	public List<String> onAuthorizerComplete(String name) {
		try {
			List<Person> people = PersonLocalServiceUtil.getDomainPeopleByName((Long) destination.getPrimaryKeyObj(), name);
			Collections.sort(people, new PersonNameComparator());

			Date today = new Date();
			for (int i = people.size() - 1; i >=0; i--) {
				Person p = people.get(i);
				if (!AccessPermissionLocalServiceUtil.hasAccess(p.getId(), (Long) destination.getPrimaryKeyObj(), today)) {
					people.remove(i);
				}
			}

			return (List<String>) CollectionUtils.collect(people, new PersonNameTransformer());
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
	}

	public List<Person> onVisitorComplete(String visitorName) {
		try {
			if (enterprise != null) {
				return PersonLocalServiceUtil.getPeople(visitorName, "", enterprise.getId()); 
			} else if (destination != null) {
				return PersonLocalServiceUtil.getPeople(visitorName, "", (Long) destination.getPrimaryKeyObj());
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

		return null;
	}
	
	public List<Vehicle> onVehicleComplete(String license) {
		try {
			setLicense(license);
			return VehicleLocalServiceUtil.getVehicles(license);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return null;
	}

	public void onVehicleUnselect(AjaxBehaviorEvent event) {
		if (((AutoComplete) event.getSource()).getSubmittedValue().equals("")) {
			setLicense(null);
		}
	}	

	public void onVehicleCreate() {
		try {
			vehicleBean.onVehicleCreate();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void onVisitorUnselect(AjaxBehaviorEvent event) {
		if (((AutoComplete) event.getSource()).getSubmittedValue().equals("")) {
			visitor = null;
		}
	}
	
	public void onGuestSelect() {
		try {
			if (guest != null) {
				long personId = BookingLocalServiceUtil.getBooking(guest.getBookingId()).getPersonId();
				authorizer = PersonLocalServiceUtil.getPerson(personId);
				authorizerName = authorizer.getFullName();				
			} else {
				authorizer = null;
				authorizerName = null;
			}

			visitor = null;
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String displayDomainName(Object domain) {
		try {
			if (domain instanceof Flat) {
				Flat flat = (Flat) domain;
				return MessageFormat.format(BUNDLE.getString("flat.name"), String.valueOf(flat.getNumber()), flat.getBlock());
			} else if (domain instanceof Enterprise) {
				return ((Enterprise) domain).getName();
			} else if (domain instanceof Department) {
				return ((Department) domain).getName();
			}
		} catch (Exception e) {
		}

		return null;
	}
	
	public boolean hasAccess(Person person) {
		try {
			return AccessPermissionLocalServiceUtil.hasAccess(person.getId(), (Long) destination.getPrimaryKeyObj(), new Date());
		} catch (Exception e) {
		}
		return false;
	}

	public boolean needAuthorization() {
		try {
			return visitor != null && !hasAccess(visitor);
		} catch (Exception e) {
		}

		return true;
	}

	public boolean needBadge() {
		try {
			return vehicle == null && visitor != null && 
						visitor.getDomain() instanceof Enterprise && 
							((Enterprise) visitor.getDomain()).getType() == DomainType.SUPPLIER;
		} catch (Exception e) {
		}

		return true;
	}	

	@Override
	public void update(Observable observable, Object object) {
		if (object instanceof Person) {
			visitor = (Person) object;
			visitors.add(visitor);
		} else if (object instanceof Vehicle) {
			vehicle = (Vehicle) object;
		} else if (object instanceof Enterprise) {
			enterprise = (Enterprise) object;
			enterprises.add(enterprise);
			onEnterpriseSelect();
		}
	}

	public BaseModel<?> getDestination() {
		return destination;
	}

	public void setDestination(BaseModel<?> destination) {
		this.destination = destination;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getBadge() {
		return badge;
	}

	public void setBadge(String badge) {
		this.badge = badge;
	}

	public Person getAuthorizer() {
		return authorizer;
	}

	public void setAuthorizer(Person authorizer) {
		this.authorizer = authorizer;
	}

	public String getAuthorizerName() {
		return authorizerName;
	}

	public void setAuthorizerName(String authorizerName) {
		this.authorizerName = authorizerName;
	}

	public List<BaseModel<?>> getDomains() {
		return domains;
	}

	public void setDomains(List<BaseModel<?>> domains) {
		this.domains = domains;
	}

	public EntreeVehicleBean getVehicleBean() {
		return vehicleBean;
	}

	public void setVehicleBean(EntreeVehicleBean vehicleBean) {
		this.vehicleBean = vehicleBean;
	}

	public EnterpriseBean getEnterpriseBean() {
		return enterpriseBean;
	}

	public void setEnterpriseBean(EnterpriseBean enterpriseBean) {
		this.enterpriseBean = enterpriseBean;
	}

	public List<Enterprise> getEnterprises() {
		return enterprises;
	}

	public void setEnterprises(List<Enterprise> enterprises) {
		this.enterprises = enterprises;
	}

	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

	public PersonBean getPersonBean() {
		return personBean;
	}

	public void setPersonBean(PersonBean personBean) {
		this.personBean = personBean;
	}

	public Person getVisitor() {
		return visitor;
	}

	public void setVisitor(Person visitor) {
		this.visitor = visitor;
	}

	public List<Person> getVisitors() {
		return visitors;
	}

	public void setVisitors(List<Person> visitors) {
		this.visitors = visitors;
	}

	public Guest getGuest() {
		return guest;
	}

	public void setGuest(Guest guest) {
		this.guest = guest;
	}

	public List<Guest> getGuests() {
		return guests;
	}

	public void setGuests(List<Guest> guests) {
		this.guests = guests;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

}
