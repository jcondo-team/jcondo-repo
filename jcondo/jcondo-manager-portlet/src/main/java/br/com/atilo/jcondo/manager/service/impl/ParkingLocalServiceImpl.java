/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.ArrayList;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Parking;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.datatype.ParkingType;
import br.com.atilo.jcondo.datatype.VehicleType;
import br.com.atilo.jcondo.manager.service.base.ParkingLocalServiceBaseImpl;

/**
 * The implementation of the parking local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.ParkingLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.ParkingLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.ParkingLocalServiceUtil
 */
public class ParkingLocalServiceImpl extends ParkingLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.ParkingLocalServiceUtil} to access the parking local service.
	 */

	public Parking addParking(String code, ParkingType type, long ownerDomainId) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Parking parking = createParking(counterLocalService.increment());
		parking.setCode(code);
		parking.setTypeId(type.ordinal());
		parking.setOwnerDomainId(ownerDomainId);
		parking.setOprDate(new Date());
		parking.setOprUser(sc.getUserId());
		return addParking(parking);
	}

	public Parking updateParking(long parkingId, String code, ParkingType type, long ownerDomainId) throws PortalException, SystemException {
		Parking parking = getParking(parkingId);
		parking.setCode(code);
		parking.setTypeId(type.ordinal());
		parking.setOwnerDomainId(ownerDomainId);
		return updateParking(parking);
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Parking updateParking(Parking parking) throws SystemException {
		parking.setOprDate(new Date());
		parking.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());
		return super.updateParking(parking);
	}

	public List<Parking> getOwnedParkings(long ownerDomainId) throws PortalException, SystemException {
		List<Parking> parkings = parkingPersistence.findByOwnerDomain(ownerDomainId);
		return  parkings == Collections.EMPTY_LIST ? new ArrayList<Parking>() : parkings;
	}

	public List<Parking> getRentedParkings(long renterDomainId) throws PortalException, SystemException{
		List<Parking> parkings = parkingPersistence.findByRenterDomain(renterDomainId);
		return  parkings == Collections.EMPTY_LIST ? new ArrayList<Parking>() : parkings;
	}

	public List<Parking> getGrantedParkings(long ownerDomainId) throws PortalException, SystemException {
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil
				.and(RestrictionsFactoryUtil.eq("ownerDomainId", ownerDomainId), 
					 RestrictionsFactoryUtil.gt("renterDomainId", 0L)));
		List<Parking> parkings = new ArrayList<Parking>(); 
		CollectionUtils.addAll(parkings, parkingPersistence.findWithDynamicQuery(dq).iterator());
		return parkings;
	}

	@SuppressWarnings("unchecked")
	public Parking rent(long ownerDomainId, long renterDomainId) throws PortalException, SystemException{
		List<Parking> availableParkings = (List<Parking>) CollectionUtils.subtract(getOwnedParkings(ownerDomainId), getGrantedParkings(ownerDomainId));
		int availableParkingAmount = availableParkings.size();
		int rentedParkingAmount = getRentedParkings(ownerDomainId).size();
		int vehicleAmount = vehicleLocalService.getDomainVehicles(ownerDomainId).size();

		if (availableParkingAmount - Math.max(vehicleAmount - rentedParkingAmount, 0) > 0) {
			Parking parking = availableParkings.get(0);
			parking.setRenterDomainId(renterDomainId);
			return updateParking(parking);
		}

		throw new BusinessException("exception.no.parking.to.rent", ownerDomainId);
	}

	public Parking unrent(long parkingId) throws PortalException, SystemException{
		Parking parking = getParking(parkingId);

		if (parking.getRenterDomainId() > 0) {
			parking.setRenterDomainId(0);
			parking = updateParking(parking);
		}

		return parking;
	}

	public void checkParkingAvailability(long domainId) throws PortalException, SystemException {
		int ownedParkingAmount = getOwnedParkings(domainId).size();
		int rentedParkingAmount = getRentedParkings(domainId).size();
		int grantedParkingAmount = getGrantedParkings(domainId).size();
		
		int parkingAmount = ownedParkingAmount + rentedParkingAmount - grantedParkingAmount;

		List<Vehicle> vehicles = vehicleLocalService.getDomainVehicles(domainId);

		for (int i = vehicles.size() - 1; i >= 0; i--) {
			if (vehicles.get(i).getType() != VehicleType.CAR) {
				vehicles.remove(i);
			}
		}

		int vehicleAmount = vehicles.size();

		if (parkingAmount - vehicleAmount <= 0) {
			throw new BusinessException("exception.parking.limit.exceeded", parkingAmount, vehicleAmount);
		}
	}

}