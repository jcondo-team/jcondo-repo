package br.com.atilo.jcondo.manager.service.impl;

import br.com.atilo.jcondo.manager.service.base.MailServiceBaseImpl;

/**
 * The implementation of the mail remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.MailService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.base.MailServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.MailServiceUtil
 */
public class MailServiceImpl extends MailServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.MailServiceUtil} to access the mail remote service.
     */
}
