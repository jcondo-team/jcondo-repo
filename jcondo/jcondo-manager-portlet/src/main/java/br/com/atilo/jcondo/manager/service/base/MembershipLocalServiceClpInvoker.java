package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.MembershipLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class MembershipLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName126;
    private String[] _methodParameterTypes126;
    private String _methodName127;
    private String[] _methodParameterTypes127;
    private String _methodName138;
    private String[] _methodParameterTypes138;
    private String _methodName139;
    private String[] _methodParameterTypes139;
    private String _methodName140;
    private String[] _methodParameterTypes140;
    private String _methodName142;
    private String[] _methodParameterTypes142;
    private String _methodName143;
    private String[] _methodParameterTypes143;
    private String _methodName144;
    private String[] _methodParameterTypes144;

    public MembershipLocalServiceClpInvoker() {
        _methodName0 = "addMembership";

        _methodParameterTypes0 = new String[] {
                "br.com.atilo.jcondo.manager.model.Membership"
            };

        _methodName1 = "createMembership";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteMembership";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteMembership";

        _methodParameterTypes3 = new String[] {
                "br.com.atilo.jcondo.manager.model.Membership"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchMembership";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getMembership";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getMemberships";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getMembershipsCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateMembership";

        _methodParameterTypes15 = new String[] {
                "br.com.atilo.jcondo.manager.model.Membership"
            };

        _methodName126 = "getBeanIdentifier";

        _methodParameterTypes126 = new String[] {  };

        _methodName127 = "setBeanIdentifier";

        _methodParameterTypes127 = new String[] { "java.lang.String" };

        _methodName138 = "addMembership";

        _methodParameterTypes138 = new String[] {
                "long", "long", "br.com.atilo.jcondo.datatype.PersonType"
            };

        _methodName139 = "updateMembership";

        _methodParameterTypes139 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.PersonType"
            };

        _methodName140 = "deleteMembership";

        _methodParameterTypes140 = new String[] { "long" };

        _methodName142 = "getPersonMemberships";

        _methodParameterTypes142 = new String[] { "long" };

        _methodName143 = "getDomainMembershipsByType";

        _methodParameterTypes143 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.PersonType"
            };

        _methodName144 = "getDomainMembershipByPerson";

        _methodParameterTypes144 = new String[] { "long", "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return MembershipLocalServiceUtil.addMembership((br.com.atilo.jcondo.manager.model.Membership) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return MembershipLocalServiceUtil.createMembership(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return MembershipLocalServiceUtil.deleteMembership(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return MembershipLocalServiceUtil.deleteMembership((br.com.atilo.jcondo.manager.model.Membership) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return MembershipLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return MembershipLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return MembershipLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return MembershipLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return MembershipLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return MembershipLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return MembershipLocalServiceUtil.fetchMembership(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return MembershipLocalServiceUtil.getMembership(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return MembershipLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return MembershipLocalServiceUtil.getMemberships(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return MembershipLocalServiceUtil.getMembershipsCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return MembershipLocalServiceUtil.updateMembership((br.com.atilo.jcondo.manager.model.Membership) arguments[0]);
        }

        if (_methodName126.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes126, parameterTypes)) {
            return MembershipLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName127.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes127, parameterTypes)) {
            MembershipLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName138.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes138, parameterTypes)) {
            return MembershipLocalServiceUtil.addMembership(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                (br.com.atilo.jcondo.datatype.PersonType) arguments[2]);
        }

        if (_methodName139.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes139, parameterTypes)) {
            return MembershipLocalServiceUtil.updateMembership(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.PersonType) arguments[1]);
        }

        if (_methodName140.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes140, parameterTypes)) {
            return MembershipLocalServiceUtil.deleteMembership(((Long) arguments[0]).longValue());
        }

        if (_methodName142.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes142, parameterTypes)) {
            return MembershipLocalServiceUtil.getPersonMemberships(((Long) arguments[0]).longValue());
        }

        if (_methodName143.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes143, parameterTypes)) {
            return MembershipLocalServiceUtil.getDomainMembershipsByType(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.PersonType) arguments[1]);
        }

        if (_methodName144.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes144, parameterTypes)) {
            return MembershipLocalServiceUtil.getDomainMembershipByPerson(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
