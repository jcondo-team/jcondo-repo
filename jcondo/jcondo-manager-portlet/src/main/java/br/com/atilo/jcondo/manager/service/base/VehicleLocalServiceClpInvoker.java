package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class VehicleLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName120;
    private String[] _methodParameterTypes120;
    private String _methodName121;
    private String[] _methodParameterTypes121;
    private String _methodName126;
    private String[] _methodParameterTypes126;
    private String _methodName127;
    private String[] _methodParameterTypes127;
    private String _methodName128;
    private String[] _methodParameterTypes128;
    private String _methodName129;
    private String[] _methodParameterTypes129;
    private String _methodName130;
    private String[] _methodParameterTypes130;
    private String _methodName131;
    private String[] _methodParameterTypes131;
    private String _methodName132;
    private String[] _methodParameterTypes132;
    private String _methodName133;
    private String[] _methodParameterTypes133;
    private String _methodName134;
    private String[] _methodParameterTypes134;

    public VehicleLocalServiceClpInvoker() {
        _methodName0 = "addVehicle";

        _methodParameterTypes0 = new String[] {
                "br.com.atilo.jcondo.manager.model.Vehicle"
            };

        _methodName1 = "createVehicle";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteVehicle";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteVehicle";

        _methodParameterTypes3 = new String[] {
                "br.com.atilo.jcondo.manager.model.Vehicle"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchVehicle";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getVehicle";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getVehicles";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getVehiclesCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateVehicle";

        _methodParameterTypes15 = new String[] {
                "br.com.atilo.jcondo.manager.model.Vehicle"
            };

        _methodName120 = "getBeanIdentifier";

        _methodParameterTypes120 = new String[] {  };

        _methodName121 = "setBeanIdentifier";

        _methodParameterTypes121 = new String[] { "java.lang.String" };

        _methodName126 = "addVehicle";

        _methodParameterTypes126 = new String[] {
                "java.lang.String", "br.com.atilo.jcondo.datatype.VehicleType",
                "long", "br.com.atilo.jcondo.Image", "java.lang.String",
                "java.lang.String", "java.lang.String"
            };

        _methodName127 = "updateVehicle";

        _methodParameterTypes127 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.VehicleType", "long",
                "br.com.atilo.jcondo.Image", "java.lang.String",
                "java.lang.String", "java.lang.String"
            };

        _methodName128 = "updateVehiclePortrait";

        _methodParameterTypes128 = new String[] {
                "long", "br.com.atilo.jcondo.Image"
            };

        _methodName129 = "deleteVehicle";

        _methodParameterTypes129 = new String[] { "long" };

        _methodName130 = "getVehicleByLicense";

        _methodParameterTypes130 = new String[] { "java.lang.String" };

        _methodName131 = "getVehicles";

        _methodParameterTypes131 = new String[] { "java.lang.String" };

        _methodName132 = "getVehicles";

        _methodParameterTypes132 = new String[] { "java.util.Map" };

        _methodName133 = "getDomainVehiclesByLicense";

        _methodParameterTypes133 = new String[] { "long", "java.lang.String" };

        _methodName134 = "getDomainVehicles";

        _methodParameterTypes134 = new String[] { "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return VehicleLocalServiceUtil.addVehicle((br.com.atilo.jcondo.manager.model.Vehicle) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return VehicleLocalServiceUtil.createVehicle(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return VehicleLocalServiceUtil.deleteVehicle(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return VehicleLocalServiceUtil.deleteVehicle((br.com.atilo.jcondo.manager.model.Vehicle) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return VehicleLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return VehicleLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return VehicleLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return VehicleLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return VehicleLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return VehicleLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return VehicleLocalServiceUtil.fetchVehicle(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return VehicleLocalServiceUtil.getVehicle(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return VehicleLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return VehicleLocalServiceUtil.getVehicles(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return VehicleLocalServiceUtil.getVehiclesCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return VehicleLocalServiceUtil.updateVehicle((br.com.atilo.jcondo.manager.model.Vehicle) arguments[0]);
        }

        if (_methodName120.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes120, parameterTypes)) {
            return VehicleLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName121.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes121, parameterTypes)) {
            VehicleLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName126.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes126, parameterTypes)) {
            return VehicleLocalServiceUtil.addVehicle((java.lang.String) arguments[0],
                (br.com.atilo.jcondo.datatype.VehicleType) arguments[1],
                ((Long) arguments[2]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[3],
                (java.lang.String) arguments[4],
                (java.lang.String) arguments[5], (java.lang.String) arguments[6]);
        }

        if (_methodName127.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes127, parameterTypes)) {
            return VehicleLocalServiceUtil.updateVehicle(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.VehicleType) arguments[1],
                ((Long) arguments[2]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[3],
                (java.lang.String) arguments[4],
                (java.lang.String) arguments[5], (java.lang.String) arguments[6]);
        }

        if (_methodName128.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes128, parameterTypes)) {
            return VehicleLocalServiceUtil.updateVehiclePortrait(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[1]);
        }

        if (_methodName129.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes129, parameterTypes)) {
            return VehicleLocalServiceUtil.deleteVehicle(((Long) arguments[0]).longValue());
        }

        if (_methodName130.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes130, parameterTypes)) {
            return VehicleLocalServiceUtil.getVehicleByLicense((java.lang.String) arguments[0]);
        }

        if (_methodName131.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes131, parameterTypes)) {
            return VehicleLocalServiceUtil.getVehicles((java.lang.String) arguments[0]);
        }

        if (_methodName132.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes132, parameterTypes)) {
            return VehicleLocalServiceUtil.getVehicles((java.util.Map<java.lang.String, java.lang.String>) arguments[0]);
        }

        if (_methodName133.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes133, parameterTypes)) {
            return VehicleLocalServiceUtil.getDomainVehiclesByLicense(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName134.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes134, parameterTypes)) {
            return VehicleLocalServiceUtil.getDomainVehicles(((Long) arguments[0]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
