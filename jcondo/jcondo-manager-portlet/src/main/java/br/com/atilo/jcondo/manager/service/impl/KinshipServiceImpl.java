/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.model.Kinship;
import br.com.atilo.jcondo.datatype.KinType;
import br.com.atilo.jcondo.manager.service.base.KinshipServiceBaseImpl;

/**
 * The implementation of the kinship remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.KinshipService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.KinshipServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.KinshipServiceUtil
 */
public class KinshipServiceImpl extends KinshipServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.KinshipServiceUtil} to access the kinship remote service.
	 */
	
	public Kinship createKinship(long personId, long relativeId, KinType type) {
		return kinshipLocalService.createKinship(personId, relativeId, type);
	}

	public Kinship addKinship(long personId, long relativeId, KinType type) throws PortalException, SystemException {
		return kinshipLocalService.addKinship(personId, relativeId, type);
	}

	public Kinship updateKinship(long kinshipId, KinType type) throws PortalException, SystemException {
		return kinshipLocalService.updateKinship(kinshipId, type);
	}

	public List<Kinship> getKinships(long personId) throws PortalException, SystemException {
		return kinshipLocalService.getKinships(personId);
	}
	
	public Kinship getPersonKinship(long personId, long relativeId) throws PortalException, SystemException {
		return kinshipLocalService.getPersonKinship(personId, relativeId);
	}
}