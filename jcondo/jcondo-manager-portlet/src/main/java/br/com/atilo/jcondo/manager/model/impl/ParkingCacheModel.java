package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Parking;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Parking in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Parking
 * @generated
 */
public class ParkingCacheModel implements CacheModel<Parking>, Externalizable {
    public long id;
    public String code;
    public int typeId;
    public long ownerDomainId;
    public long renterDomainId;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{id=");
        sb.append(id);
        sb.append(", code=");
        sb.append(code);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", ownerDomainId=");
        sb.append(ownerDomainId);
        sb.append(", renterDomainId=");
        sb.append(renterDomainId);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Parking toEntityModel() {
        ParkingImpl parkingImpl = new ParkingImpl();

        parkingImpl.setId(id);

        if (code == null) {
            parkingImpl.setCode(StringPool.BLANK);
        } else {
            parkingImpl.setCode(code);
        }

        parkingImpl.setTypeId(typeId);
        parkingImpl.setOwnerDomainId(ownerDomainId);
        parkingImpl.setRenterDomainId(renterDomainId);

        if (oprDate == Long.MIN_VALUE) {
            parkingImpl.setOprDate(null);
        } else {
            parkingImpl.setOprDate(new Date(oprDate));
        }

        parkingImpl.setOprUser(oprUser);

        parkingImpl.resetOriginalValues();

        return parkingImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        code = objectInput.readUTF();
        typeId = objectInput.readInt();
        ownerDomainId = objectInput.readLong();
        renterDomainId = objectInput.readLong();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);

        if (code == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(code);
        }

        objectOutput.writeInt(typeId);
        objectOutput.writeLong(ownerDomainId);
        objectOutput.writeLong(renterDomainId);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
