/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.ListTypeConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.datatype.EnterpriseStatus;
import br.com.atilo.jcondo.manager.service.base.EnterpriseLocalServiceBaseImpl;
import br.com.caelum.stella.validation.CNPJValidator;

/**
 * The implementation of the enterprise local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.EnterpriseLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.EnterpriseLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil
 */
public class EnterpriseLocalServiceImpl extends EnterpriseLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil} to access the enterprise local service.
	 */

	public Enterprise createEnterprise() {
		Enterprise enterprise = super.createEnterprise(0);
		enterprise.setOrganization(organizationLocalService.createOrganization(0));
		enterprise.setType(DomainType.SUPPLIER);
		enterprise.setStatus(EnterpriseStatus.ENABLED);
		return enterprise;
	}

	private void validateIdentity(long enterpriseId, String identity) throws PortalException, SystemException {
		if (!StringUtils.isEmpty(identity)) {
			try {
				new CNPJValidator().assertValid(identity.replaceAll("[^0-9]+", ""));
			} catch (Exception e) {
				throw new BusinessException("enterprise.identity.not.valid", identity);
			}

			try {
				Enterprise enterprise = enterprisePersistence.findByIdentity(identity);
				if (enterprise.getId() != enterpriseId) {
					throw new BusinessException("enterprise.identity.exists", enterprise.getIdentity());
				}
			} catch (NoSuchEnterpriseException e) {
			}
//		} else {
//			throw new BusinessException("enterprise.identity.empty", identity);
		}	
	}

	public Enterprise addEnterprise(String identity, String name, DomainType type, EnterpriseStatus status, String description) throws PortalException, SystemException {
		validateIdentity(0, identity);

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Organization administration = organizationLocalService.getOrganizations(sc.getCompanyId(), 0L).get(0);
		Organization organization = organizationLocalService
										.addOrganization(sc.getUserId(), administration.getOrganizationId(), 
														 WordUtils.capitalizeFully(name.trim()), type.getLabel(), true, 0, 0, 
														 ListTypeConstants.ORGANIZATION_STATUS_DEFAULT, 
														 description, false, null);

		Enterprise enterprise = enterprisePersistence.create(counterLocalService.increment());
		enterprise.setOrganizationId(organization.getOrganizationId());
		enterprise.setOrganization(organization);
		enterprise.setStatusId(status.ordinal());
		enterprise.setIdentity(identity);
		enterprise.setOprDate(new Date());
		enterprise.setOprUser(sc.getUserId());

		return addEnterprise(enterprise);
	}

	public Enterprise updateEnterprise(long enterpriseId, String identity, String name, DomainType type, EnterpriseStatus status, String description) throws PortalException, SystemException {
		validateIdentity(enterpriseId, identity);

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Enterprise enterprise = getEnterprise(enterpriseId);
		Organization organization = enterprise.getOrganization();

		organization.setName(WordUtils.capitalizeFully(name.trim()));
		organization.setType(type.getLabel());
		organization.setComments(description);

		organizationLocalService.updateOrganization(organization);

		enterprise.setStatusId(status.ordinal());
		enterprise.setIdentity(identity);
		enterprise.setOprDate(new Date());
		enterprise.setOprUser(sc.getUserId());

		return updateEnterprise(enterprise);
	}

	@Override
	public Enterprise getEnterprise(long id) throws PortalException, SystemException {
		Enterprise enterprise = enterprisePersistence.findByPrimaryKey(id);
		enterprise.setOrganization(organizationLocalService.getOrganization(enterprise.getOrganizationId()));
		return enterprise;
	}
	
	public Enterprise getEnterpriseByIdentity(String identity) throws PortalException, SystemException {
		return enterprisePersistence.findByIdentity(identity);
	}

	@SuppressWarnings("rawtypes")
	public List<Enterprise> getEnterprisesByType(DomainType type) throws PortalException, SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Organization.class);
		dynamicQuery.add(RestrictionsFactoryUtil.eq("type", type.getLabel()));
		dynamicQuery.addOrder(OrderFactoryUtil.asc("name"));
		List organizations = organizationLocalService.dynamicQuery(dynamicQuery);
		List<Enterprise> enterprises = new ArrayList<Enterprise>();

		for (int i = 0; i < organizations.size(); i++) {
			Organization o = (Organization) organizations.get(i);
			try {
				Enterprise enterprise = enterprisePersistence.findByOrganizationId(o.getOrganizationId());
				enterprise.setOrganization(o);
				enterprises.add(enterprise);
			} catch (NoSuchEnterpriseException e) {
				// TODO: handle exception
			}
		}

		return enterprises;
	}

	@SuppressWarnings("rawtypes")
	public List<Enterprise> getEnterpriseByName(String name) throws PortalException, SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Organization.class);
		dynamicQuery.add(RestrictionsFactoryUtil.ilike("name", "%" + name + "%"));
		List organizations = organizationLocalService.dynamicQuery(dynamicQuery);
		List<Enterprise> enterprises = new ArrayList<Enterprise>();

		for (int i = 0; i < organizations.size(); i++) {
			Organization o = (Organization) organizations.get(i);
			try {
				Enterprise enterprise = enterprisePersistence.findByOrganizationId(o.getOrganizationId());
				enterprise.setOrganization(o);
				enterprises.add(enterprise);
			} catch (NoSuchEnterpriseException e) {
				// TODO: handle exception
			}
		}

		return enterprises;
	}
}