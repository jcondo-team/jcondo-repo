/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.model.Parking;
import br.com.atilo.jcondo.datatype.ParkingType;
import br.com.atilo.jcondo.manager.service.base.ParkingServiceBaseImpl;

/**
 * The implementation of the parking remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.ParkingService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.ParkingServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.ParkingServiceUtil
 */
public class ParkingServiceImpl extends ParkingServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.ParkingServiceUtil} to access the parking remote service.
	 */

	public Parking addParking(String code, ParkingType type, long ownerDomainId) throws PortalException, SystemException {
		return parkingLocalService.addParking(code, type, ownerDomainId);
	}

	public Parking updateParking(long parkingId, String code, ParkingType type, long ownerDomainId) throws PortalException, SystemException {
		return parkingLocalService.updateParking(parkingId, code, type, ownerDomainId);
	}

	public List<Parking> getOwnedParkings(long ownerDomainId) throws PortalException, SystemException {
		return parkingLocalService.getOwnedParkings(ownerDomainId);
	}

	public List<Parking> getRentedParkings(long renterDomainId) throws PortalException, SystemException {
		return parkingLocalService.getRentedParkings(renterDomainId);
	}

	public List<Parking> getGrantedParkings(long ownerDomainId) throws PortalException, SystemException {
		return parkingLocalService.getGrantedParkings(ownerDomainId);
	}

	public Parking rent(long ownerDomainId, long renterDomainId) throws PortalException, SystemException {
		return parkingLocalService.rent(ownerDomainId, renterDomainId);
	}

	public Parking unrent(long parkingId) throws PortalException, SystemException {
		return parkingLocalService.unrent(parkingId);
	}

}