package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

import javax.faces.application.FacesMessage;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import com.liferay.portal.model.BaseModel;
import com.liferay.util.portlet.PortletProps;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.DuplicateVehicleLicenseException;
import br.com.atilo.jcondo.datatype.VehicleType;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.service.FlatServiceUtil;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.VehicleServiceUtil;
import br.com.atilo.jcondo.manager.bean.ImageUploadBean;
import br.com.atilo.jcondo.manager.bean.access.VehicleBrand;

public class VehicleBean<Domain extends BaseModel<Domain>> extends Observable implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(VehicleBean.class);	

	private ImageUploadBean imageUploadBean;

	private CameraBean cameraBean;

	private Vehicle vehicle;

	private String license;

	private List<VehicleType> types;

	private Domain domain;
	
	private Image image;

	private String[] colors;

	private List<VehicleBrand> brands;

	private VehicleBrand brand;

	public VehicleBean() {
		types = Arrays.asList(VehicleType.CAR, VehicleType.MOTO);

		brands = new ArrayList<VehicleBrand>();
		for (String brand : PortletProps.getArray("vehicle.brands")) {
			brands.add(new VehicleBrand(brand, brand.concat(".png")));
		}

		colors = PortletProps.getArray("colors");

		imageUploadBean = new ImageUploadBean(400, 300);
		cameraBean = new CameraBean(400, 300);
	}

	public void init(Domain domain) throws Exception {
		setDomain(domain);
		onVehicleCreate();
	}

	public void onVehicleCreate() throws Exception {
		vehicle = VehicleServiceUtil.createVehicle();
		vehicle.setDomainId((Long) getDomain().getPrimaryKeyObj());
		image = null;
		brand = null;
		license = null;
	}

	public void onVehicleSave() {
		try {
			String msg;
			Vehicle v;
			vehicle.setLicense(vehicle.getLicense().replaceAll("[^A-Za-z0-9]", ""));

			if (vehicle.getId() == 0) {
				v = VehicleServiceUtil.addVehicle(vehicle.getLicense(), vehicle.getType(), (Long) getDomain().getPrimaryKeyObj(), 
												  image, brand != null ? brand.getName() : null, vehicle.getColor(), vehicle.getRemark());
				msg = "msg.vehicle.add";
			} else {
				v = VehicleServiceUtil.updateVehicle(vehicle.getId(), vehicle.getType(), (Long) getDomain().getPrimaryKeyObj(), 
													 image,  brand != null ? brand.getName() : null, vehicle.getColor(), vehicle.getRemark());
				msg = "msg.vehicle.update";
			}

			setChanged();
			notifyObservers(v);

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, msg, null);
		} catch (DuplicateVehicleLicenseException e) {
			Vehicle v = (Vehicle) e.getArgs()[0];

			// Vehicle is not associated to any domain. So, it can be taken
			if (v.getDomainId() == 0) {
				vehicle.setId(v.getId());
				if (image == null || StringUtils.isEmpty(image.getPath())) {
					image = v.getPortrait();
				}
				onVehicleSave();
			} else {
				try {
					Flat f = FlatServiceUtil.getFlat(v.getDomainId());
					MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), 
											new Object[]{f.getNumber(), f.getBlock()}, 
											":tabs:vehicle-details-form:alertMsg");
					RequestContext.getCurrentInstance().addCallbackParam("alert", true);
					RequestContext.getCurrentInstance().addCallbackParam("exception", true);
				} catch (Exception ex) {
					MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
					RequestContext.getCurrentInstance().addCallbackParam("exception", true);
				}
			}
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onVehicleEdit(Vehicle vehicle) throws Exception {
		BeanUtils.copyProperties(this.vehicle, vehicle);
		image = vehicle.getPortrait();
		brand = new VehicleBrand(vehicle.getBrand(), vehicle.getBrand().concat(".png"));
		license = vehicle.getLicense();
	}

	public void onSearchByLicense() {
		try {
			vehicle = VehicleLocalServiceUtil.getVehicleByLicense(license);
			for (VehicleBrand b : brands) {
				if (b.getName().equalsIgnoreCase(vehicle.getBrand())) {
					brand = b;
					break;
				}
			}
			image = vehicle.getPortrait();
		} catch (Exception e) {
			vehicle.setLicense(license);
		}
	}	

	public ImageUploadBean getImageUploadBean() {
		return imageUploadBean;
	}

	public void setImageUploadBean(ImageUploadBean imageUploadBean) {
		this.imageUploadBean = imageUploadBean;
	}

	public CameraBean getCameraBean() {
		return cameraBean;
	}

	public void setCameraBean(CameraBean cameraBean) {
		this.cameraBean = cameraBean;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}

	public List<VehicleType> getTypes() {
		return types;
	}

	public void setTypes(List<VehicleType> types) {
		this.types = types;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String[] getColors() {
		return colors;
	}

	public void setColors(String[] colors) {
		this.colors = colors;
	}

	public List<VehicleBrand> getBrands() {
		return brands;
	}

	public void setBrands(List<VehicleBrand> brands) {
		this.brands = brands;
	}

	public VehicleBrand getBrand() {
		return brand;
	}

	public void setBrand(VehicleBrand brand) {
		this.brand = brand;
	}

}
