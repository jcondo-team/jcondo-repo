/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.model.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.manager.NoSuchDepartmentException;
import br.com.atilo.jcondo.manager.NoSuchDomainException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

/**
 * The extended model implementation for the Membership service. Represents a row in the &quot;jco_membership&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Membership} interface.
 * </p>
 *
 * @author anderson
 */
public class MembershipImpl extends MembershipBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a membership model instance should use the {@link br.com.atilo.jcondo.manager.model.Membership} interface instead.
	 */
	public MembershipImpl() {
	}

	public PersonType getType() {
		return PersonType.valueOf(getTypeId());
	}

	public void setType(PersonType type) {
		setTypeId(type != null ? type.ordinal() : -1);
	}

	public Person getPerson() throws PortalException, SystemException {
		return PersonLocalServiceUtil.getPerson(getPersonId());
	}

	public BaseModel<?> getDomain() throws PortalException, SystemException {
		try {
			return FlatLocalServiceUtil.getFlat(getDomainId());
		} catch (NoSuchFlatException e) {
			try {
				return EnterpriseLocalServiceUtil.getEnterprise(getDomainId());
			} catch (NoSuchEnterpriseException ex) {
				try {
					return DepartmentLocalServiceUtil.getDepartment(getDomainId());
				} catch (NoSuchDepartmentException exx) {
					throw new NoSuchDomainException();
				}				
			}
		}
	}

	public void setDomain(BaseModel<?> domain) {
		setDomainId((Long) domain.getPrimaryKeyObj());
	}
}