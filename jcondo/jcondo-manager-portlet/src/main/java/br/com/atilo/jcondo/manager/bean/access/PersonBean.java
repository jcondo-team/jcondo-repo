package br.com.atilo.jcondo.manager.bean.access;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Observable;

import javax.faces.application.FacesMessage;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.commons.collections.DomainPredicate;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.NoSuchPersonException;
import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.Gender;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.MembershipServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonServiceUtil;
import br.com.atilo.jcondo.manager.bean.CameraBean;

public class PersonBean extends Observable implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(PersonBean.class);

	private CameraBean cameraBean;	
	
	private List<PersonType> personTypes;

	private List<Gender> genders;

	private Person person;

	private Membership membership;
	
	private BaseModel<?> domain;

	private Image image;
	
	private String identity;
	
	public PersonBean() {
		genders = Arrays.asList(Gender.values());
		cameraBean = new CameraBean(158, 240);
	}
	
	public void onPersonCreate() throws Exception {
		if (domain == null) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, "msg.person.domain.not.selected", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
			return;
		}

		person = PersonServiceUtil.createPerson();
		person.setGender(Gender.MALE);
		membership = MembershipServiceUtil.createMembership(0);
		membership.setDomain(domain);

		if (domain instanceof Flat || domain instanceof Department) {
			membership.setType(PersonType.VISITOR);
		} else {
			membership.setType(PersonType.EMPLOYEE);
		}

		image = null;
		identity = null;
	}

	public void onPersonSave() throws Exception {
		try {
			if (person.isNew()) {
				person = PersonServiceUtil.addPerson(person.getName(), person.getSurname(), person.getIdentity(), 
													 person.getEmail(), person.getBirthday(), person.getGender(),
													 person.getRemark(), membership.getType(), membership.getDomainId());
	
				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.person.add", null);
			} else {
				person = PersonServiceUtil.updatePerson(person.getId(), person.getName(), person.getSurname(), 
														person.getIdentity(), person.getEmail(), person.getBirthday(), 
														person.getGender(), person.getRemark(), person.getDomainId(), membership);
	
				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.person.update", null);
			}
	
			person = PersonServiceUtil.updatePortrait(person.getId(), image);
	
			setChanged();
			notifyObservers(person);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on person save : " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on person save ", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}	

	public void onSearchByCPF(AjaxBehaviorEvent event) throws Exception {
		if (StringUtils.isEmpty(identity)) {
			person.setId(0);
			person.setUserId(0);
			person.setIdentity(null);
			person.setName(null);
			person.setSurname(null);
			person.setGender(Gender.MALE);
			cameraBean.setImage(person.getPortrait());
			return;
		}

		try {
			person = PersonServiceUtil.getPersonByIdentity(identity);
			image = person.getPortrait();

			Membership m = (Membership) CollectionUtils.find(person.getMemberships(), 
					 										 new DomainPredicate((Long) getDomain().getPrimaryKeyObj()));

			if (m != null) {
				membership = m;
			}
		} catch (NoSuchPersonException e) {
			LOGGER.debug("failure on onSearchByCPF method", e);
			person.setIdentity(identity);
		} catch (BusinessException e) {
			LOGGER.debug("failure on onSearchByCPF method", e);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}	

	public List<Enterprise> onEnterpriseComplete(String enterpriseName) {
		try {
			return EnterpriseLocalServiceUtil.getEnterpriseByName(enterpriseName);
		} catch (Exception e) {
			LOGGER.error("", e);
		}

		return null;
	}

	public CameraBean getCameraBean() {
		return cameraBean;
	}

	public void setCameraBean(CameraBean cameraBean) {
		this.cameraBean = cameraBean;
	}

	public List<PersonType> getPersonTypes() {
		return personTypes;
	}

	public void setPersonTypes(List<PersonType> personTypes) {
		this.personTypes = personTypes;
	}

	public List<Gender> getGenders() {
		return genders;
	}

	public void setGenders(List<Gender> genders) {
		this.genders = genders;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Membership getMembership() {
		return membership;
	}

	public void setMembership(Membership membership) {
		this.membership = membership;
	}

	public BaseModel<?> getDomain() {
		return domain;
	}

	public void setDomain(BaseModel<?> domain) {
		this.domain = domain;
	}

	public Image getImage() {
		return image;
	}

	public void setImage(Image image) {
		this.image = image;
	}

	public String getIdentity() {
		return identity;
	}

	public void setIdentity(String identity) {
		this.identity = identity;
	}

}
