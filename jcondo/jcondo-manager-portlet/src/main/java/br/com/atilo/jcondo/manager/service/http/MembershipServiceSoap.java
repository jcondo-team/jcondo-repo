package br.com.atilo.jcondo.manager.service.http;

import br.com.atilo.jcondo.manager.service.MembershipServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.manager.service.MembershipServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.manager.model.MembershipSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.manager.model.Membership}, that is translated to a
 * {@link br.com.atilo.jcondo.manager.model.MembershipSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MembershipServiceHttp
 * @see br.com.atilo.jcondo.manager.model.MembershipSoap
 * @see br.com.atilo.jcondo.manager.service.MembershipServiceUtil
 * @generated
 */
public class MembershipServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(MembershipServiceSoap.class);

    public static br.com.atilo.jcondo.manager.model.MembershipSoap createMembership(
        long id) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Membership returnValue = MembershipServiceUtil.createMembership(id);

            return br.com.atilo.jcondo.manager.model.MembershipSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.MembershipSoap addMembership(
        long personId, long domainId,
        br.com.atilo.jcondo.datatype.PersonType type) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Membership returnValue = MembershipServiceUtil.addMembership(personId,
                    domainId, type);

            return br.com.atilo.jcondo.manager.model.MembershipSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.MembershipSoap deleteMembership(
        long id) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Membership returnValue = MembershipServiceUtil.deleteMembership(id);

            return br.com.atilo.jcondo.manager.model.MembershipSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.MembershipSoap updateMembership(
        long id, br.com.atilo.jcondo.datatype.PersonType type)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Membership returnValue = MembershipServiceUtil.updateMembership(id,
                    type);

            return br.com.atilo.jcondo.manager.model.MembershipSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.MembershipSoap[] getPersonMemberships(
        long personId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Membership> returnValue =
                MembershipServiceUtil.getPersonMemberships(personId);

            return br.com.atilo.jcondo.manager.model.MembershipSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.MembershipSoap[] getDomainMembershipsByType(
        long domainId, br.com.atilo.jcondo.datatype.PersonType type)
        throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Membership> returnValue =
                MembershipServiceUtil.getDomainMembershipsByType(domainId, type);

            return br.com.atilo.jcondo.manager.model.MembershipSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.MembershipSoap getDomainMembershipByPerson(
        long domainId, long personId) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Membership returnValue = MembershipServiceUtil.getDomainMembershipByPerson(domainId,
                    personId);

            return br.com.atilo.jcondo.manager.model.MembershipSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
