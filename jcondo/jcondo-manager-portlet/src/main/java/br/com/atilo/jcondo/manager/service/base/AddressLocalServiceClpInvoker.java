package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.AddressLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class AddressLocalServiceClpInvoker {
    private String _methodName104;
    private String[] _methodParameterTypes104;
    private String _methodName105;
    private String[] _methodParameterTypes105;
    private String _methodName108;
    private String[] _methodParameterTypes108;
    private String _methodName109;
    private String[] _methodParameterTypes109;
    private String _methodName110;
    private String[] _methodParameterTypes110;
    private String _methodName111;
    private String[] _methodParameterTypes111;
    private String _methodName112;
    private String[] _methodParameterTypes112;
    private String _methodName113;
    private String[] _methodParameterTypes113;
    private String _methodName114;
    private String[] _methodParameterTypes114;

    public AddressLocalServiceClpInvoker() {
        _methodName104 = "getBeanIdentifier";

        _methodParameterTypes104 = new String[] {  };

        _methodName105 = "setBeanIdentifier";

        _methodParameterTypes105 = new String[] { "java.lang.String" };

        _methodName108 = "createAddress";

        _methodParameterTypes108 = new String[] {  };

        _methodName109 = "getPersonAddress";

        _methodParameterTypes109 = new String[] { "long" };

        _methodName110 = "addPersonAddress";

        _methodParameterTypes110 = new String[] {
                "long", "java.lang.String", "java.lang.String", "long",
                "java.lang.String"
            };

        _methodName111 = "getEnterpriseAddress";

        _methodParameterTypes111 = new String[] { "long" };

        _methodName112 = "addEnterpriseAddress";

        _methodParameterTypes112 = new String[] {
                "long", "java.lang.String", "java.lang.String", "long",
                "java.lang.String"
            };

        _methodName113 = "updateAddress";

        _methodParameterTypes113 = new String[] {
                "long", "java.lang.String", "java.lang.String", "long",
                "java.lang.String"
            };

        _methodName114 = "deleteAddress";

        _methodParameterTypes114 = new String[] { "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName104.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes104, parameterTypes)) {
            return AddressLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName105.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes105, parameterTypes)) {
            AddressLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName108.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes108, parameterTypes)) {
            return AddressLocalServiceUtil.createAddress();
        }

        if (_methodName109.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes109, parameterTypes)) {
            return AddressLocalServiceUtil.getPersonAddress(((Long) arguments[0]).longValue());
        }

        if (_methodName110.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
            return AddressLocalServiceUtil.addPersonAddress(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                ((Long) arguments[3]).longValue(),
                (java.lang.String) arguments[4]);
        }

        if (_methodName111.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
            return AddressLocalServiceUtil.getEnterpriseAddress(((Long) arguments[0]).longValue());
        }

        if (_methodName112.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes112, parameterTypes)) {
            return AddressLocalServiceUtil.addEnterpriseAddress(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                ((Long) arguments[3]).longValue(),
                (java.lang.String) arguments[4]);
        }

        if (_methodName113.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes113, parameterTypes)) {
            return AddressLocalServiceUtil.updateAddress(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                ((Long) arguments[3]).longValue(),
                (java.lang.String) arguments[4]);
        }

        if (_methodName114.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes114, parameterTypes)) {
            return AddressLocalServiceUtil.deleteAddress(((Long) arguments[0]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
