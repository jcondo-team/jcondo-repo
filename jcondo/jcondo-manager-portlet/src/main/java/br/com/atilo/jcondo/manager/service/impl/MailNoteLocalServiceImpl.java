package br.com.atilo.jcondo.manager.service.impl;

import br.com.atilo.jcondo.manager.service.base.MailNoteLocalServiceBaseImpl;

/**
 * The implementation of the mail note local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.MailNoteLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.base.MailNoteLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.MailNoteLocalServiceUtil
 */
public class MailNoteLocalServiceImpl extends MailNoteLocalServiceBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.MailNoteLocalServiceUtil} to access the mail note local service.
     */
}
