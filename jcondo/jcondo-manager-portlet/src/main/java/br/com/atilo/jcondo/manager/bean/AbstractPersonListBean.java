package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.faces.application.FacesMessage;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.component.selectcheckboxmenu.SelectCheckboxMenu;
import org.primefaces.context.RequestContext;

import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.commons.collections.DomainPredicate;
import br.com.atilo.jcondo.commons.collections.PersonNameComparator;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.AccessPermissionLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.MembershipServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonServiceUtil;
import br.com.atilo.jcondo.manager.service.permission.PersonPermission;
import br.com.atilo.jcondo.manager.bean.ModelDataModel;

public abstract class AbstractPersonListBean<Domain extends BaseModel<Domain>> extends Observable implements Observer, Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(AbstractPersonListBean.class);

	protected AbstractPersonBean<Domain> personBean;

	protected ModelDataModel<Person> model;

	protected List<PersonType> personTypesFilter;

	protected PersonType[] selectedPersonTypes;

	protected Domain domain;

	public AbstractPersonListBean() {
	}

	public void init(Domain domain) throws Exception {
		setDomain(domain);
		List<Person> people = PersonServiceUtil.getDomainPeople((Long) domain.getPrimaryKeyObj());
		Collections.sort(people, new PersonNameComparator());
		model = new ModelDataModel<Person>(people);
	}

	public void onPersonCreate() throws Exception {
		personBean.onPersonCreate();
	}

	public void onPersonEdit() {
		try {
			personBean.onPersonEdit(model.getRowData());
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on person editing", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onPersonDelete() throws Exception {
		try {
			Person p = model.getRowData();
			Membership m = (Membership) CollectionUtils.find(p.getMemberships(), 
															 new DomainPredicate((Long) getDomain().getPrimaryKeyObj()));
			MembershipServiceUtil.deleteMembership(m.getId());
			model.remove(p);
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.person.delete", null);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on person delete: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on person delete", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onPersonSearch(ValueChangeEvent event) throws Exception {
		String name = (String) event.getNewValue();
		List<Person> people;

		if (StringUtils.isEmpty(name)) {
			people = PersonServiceUtil.getDomainPeople((Long) domain.getPrimaryKeyObj());
		} else {
			people = PersonServiceUtil.getDomainPeopleByName((Long) domain.getPrimaryKeyObj(), name);
		}

		Collections.sort(people, new PersonNameComparator());

		model.clear();
		model.addAll(people);
	}

	public void onPersonTypeSelect(AjaxBehaviorEvent event) throws Exception {
		List<Person> people = new ArrayList<Person>();
		PersonType[] types = (PersonType[]) ((SelectCheckboxMenu) event.getSource()).getSelectedValues();

		for (PersonType type : types) {
			CollectionUtils.addAll(people, 
								   PersonServiceUtil.getDomainPeopleByType((Long) domain.getPrimaryKeyObj(), 
										   								   type).iterator());
		}

		Collections.sort(people, new PersonNameComparator());
		
		model = new ModelDataModel<Person>(people);
		selectedPersonTypes = types;
	}

	public boolean isVisitor(Person person) {
		try {
			Membership membership = (Membership) CollectionUtils.find(person.getMemberships(), 
																	  new DomainPredicate((Long) getDomain().getPrimaryKeyObj()));
	
			return membership != null ? membership.getType() == PersonType.VISITOR : false;				
		} catch (Exception e) {
			LOGGER.warn("Failure on display memberships", e);
		}
		
		return false;
	}
		
	public boolean canChangeType() throws Exception {
		//return membership.getType() == null ? true : personTypes != null ? personTypes.contains(membership.getType()) : false;
		return true;
	}

	public boolean canEditPerson(Person person) throws Exception {
		if (person == null || person.getId() <= 0) {
			return true;
		}
		return PersonPermission.hasPermission(Permission.UPDATE, person, (Long) domain.getPrimaryKeyObj());
	}

	public boolean canDeletePerson(Person person) throws Exception {
		long domainId = (Long) getDomain().getPrimaryKeyObj();
		return PersonPermission.hasPermission(Permission.REMOVE_MEMBER, person.getType(domainId), domainId);
	}

	public boolean canEditInfo() throws Exception {
		//return person.getId() == 0 || person.getId() == PersonServiceUtil.getPerson().getId();
		return true;
	}

	public String displayDomain(Domain domain) {
		try {
			if (domain instanceof Flat) {
				Flat flat = (Flat) domain;
				return "Apartamento " + flat.getNumber() + " - Bloco " + flat.getBlock();
			} else {
				Enterprise enterprise = (Enterprise) domain;
				return enterprise.getName();	
			}
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on display domain", e);
		}
		
		return null;
	}	

	public String displayMembership(Person person) {
		try {
			if (person != null && !CollectionUtils.isEmpty(person.getMemberships())) {
				Membership membership = (Membership) CollectionUtils.find(person.getMemberships(), 
																		  new DomainPredicate((Long) getDomain().getPrimaryKeyObj()));

				if (membership != null) {
					if (membership.getType() == PersonType.VISITOR && 
							!CollectionUtils.isEmpty(
									AccessPermissionLocalServiceUtil.getPersonAccessPermissions(person.getId(), 
																								membership.getDomainId()))) {
						return "person.type.guest";
					} else {
						return membership.getType().getLabel();
					}
				}
			}
		} catch (Exception e) {
			LOGGER.warn("Failure on display memberships", e);
		}

		return null;
	}	

	@Override
	public void update(Observable observable, Object object) {
		model.update((Person) object);
	}

	public AbstractPersonBean<Domain> getPersonBean() {
		return personBean;
	}

	public void setPersonBean(AbstractPersonBean<Domain> personBean) {
		this.personBean = personBean;
	}

	public ModelDataModel<Person> getModel() {
		return model;
	}

	public void setModel(ModelDataModel<Person> model) {
		this.model = model;
	}

	public List<PersonType> getPersonTypesFilter() {
		return personTypesFilter;
	}

	public void setPersonTypesFilter(List<PersonType> personTypesFilter) {
		this.personTypesFilter = personTypesFilter;
	}

	public PersonType[] getSelectedPersonTypes() {
		return selectedPersonTypes;
	}

	public void setSelectedPersonTypes(PersonType[] selectedPersonTypes) {
		this.selectedPersonTypes = selectedPersonTypes;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

}
