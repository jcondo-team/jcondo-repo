package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;

import com.liferay.portal.model.BaseModel;

public abstract class AbstractDomainBean<Domain extends BaseModel<Domain>> implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(AbstractDomainBean.class);

	protected AbstractPersonListBean<Domain> personListBean;

	protected VehicleListBean<Domain> vehicleListBean;

	protected List<Domain> domains;

	protected Domain domain;

	public abstract void preInit() throws Exception;

	public abstract void onDomainSave();

	public abstract void onDomainCreate() throws Exception;

	@PostConstruct
	public void init() {
		try {
			preInit();
			
			if (!CollectionUtils.isEmpty(domains)) {
				setDomain(domains.get(0));
			}

			vehicleListBean = new VehicleListBean<Domain>();

			if (domain != null) {
				onDomainSelect();
			}
		} catch (Exception e) {
			LOGGER.fatal("Failure on flat initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public void onDomainSelect() throws Exception {
		personListBean.init(getDomain());
		vehicleListBean.init(getDomain());
	}

	public AbstractPersonListBean<Domain> getPersonListBean() {
		return personListBean;
	}

	public void setPersonListBean(AbstractPersonListBean<Domain> personListBean) {
		this.personListBean = personListBean;
	}

	public VehicleListBean<Domain> getVehicleListBean() {
		return vehicleListBean;
	}

	public void setVehicleListBean(VehicleListBean<Domain> vehicleListBean) {
		this.vehicleListBean = vehicleListBean;
	}

	public List<Domain> getDomains() {
		return domains;
	}

	public void setDomains(List<Domain> domains) {
		this.domains = domains;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

}

