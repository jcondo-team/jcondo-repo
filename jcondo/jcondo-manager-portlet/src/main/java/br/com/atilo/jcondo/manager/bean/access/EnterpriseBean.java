package br.com.atilo.jcondo.manager.bean.access;

import java.io.Serializable;
import java.util.Observable;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.service.EnterpriseServiceUtil;

public class EnterpriseBean extends Observable implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(EnterpriseBean.class);	

	private Enterprise enterprise;

	public EnterpriseBean() {
	}

	public void onEnterpriseSave() {
		try {
			enterprise = EnterpriseServiceUtil.addEnterprise(enterprise.getIdentity(), enterprise.getName(), 
															 enterprise.getType(), enterprise.getStatus(), 
															 enterprise.getDescription());

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.enterprise.save", null);

			setChanged();
			notifyObservers(enterprise);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on address save: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on address save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.enterprise.save", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onEnterpriseCreate() throws Exception {
		enterprise = EnterpriseServiceUtil.createEnterprise();
	}

	public Enterprise getEnterprise() {
		return enterprise;
	}

	public void setEnterprise(Enterprise enterprise) {
		this.enterprise = enterprise;
	}

}
