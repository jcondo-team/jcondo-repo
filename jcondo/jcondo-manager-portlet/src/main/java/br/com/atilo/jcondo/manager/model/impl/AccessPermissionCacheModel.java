package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.AccessPermission;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing AccessPermission in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see AccessPermission
 * @generated
 */
public class AccessPermissionCacheModel implements CacheModel<AccessPermission>,
    Externalizable {
    public long id;
    public long personId;
    public long domainId;
    public int weekDay;
    public String beginTime;
    public String endTime;
    public long beginDate;
    public long endDate;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(id);
        sb.append(", personId=");
        sb.append(personId);
        sb.append(", domainId=");
        sb.append(domainId);
        sb.append(", weekDay=");
        sb.append(weekDay);
        sb.append(", beginTime=");
        sb.append(beginTime);
        sb.append(", endTime=");
        sb.append(endTime);
        sb.append(", beginDate=");
        sb.append(beginDate);
        sb.append(", endDate=");
        sb.append(endDate);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public AccessPermission toEntityModel() {
        AccessPermissionImpl accessPermissionImpl = new AccessPermissionImpl();

        accessPermissionImpl.setId(id);
        accessPermissionImpl.setPersonId(personId);
        accessPermissionImpl.setDomainId(domainId);
        accessPermissionImpl.setWeekDay(weekDay);

        if (beginTime == null) {
            accessPermissionImpl.setBeginTime(StringPool.BLANK);
        } else {
            accessPermissionImpl.setBeginTime(beginTime);
        }

        if (endTime == null) {
            accessPermissionImpl.setEndTime(StringPool.BLANK);
        } else {
            accessPermissionImpl.setEndTime(endTime);
        }

        if (beginDate == Long.MIN_VALUE) {
            accessPermissionImpl.setBeginDate(null);
        } else {
            accessPermissionImpl.setBeginDate(new Date(beginDate));
        }

        if (endDate == Long.MIN_VALUE) {
            accessPermissionImpl.setEndDate(null);
        } else {
            accessPermissionImpl.setEndDate(new Date(endDate));
        }

        if (oprDate == Long.MIN_VALUE) {
            accessPermissionImpl.setOprDate(null);
        } else {
            accessPermissionImpl.setOprDate(new Date(oprDate));
        }

        accessPermissionImpl.setOprUser(oprUser);

        accessPermissionImpl.resetOriginalValues();

        return accessPermissionImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        personId = objectInput.readLong();
        domainId = objectInput.readLong();
        weekDay = objectInput.readInt();
        beginTime = objectInput.readUTF();
        endTime = objectInput.readUTF();
        beginDate = objectInput.readLong();
        endDate = objectInput.readLong();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(personId);
        objectOutput.writeLong(domainId);
        objectOutput.writeInt(weekDay);

        if (beginTime == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(beginTime);
        }

        if (endTime == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(endTime);
        }

        objectOutput.writeLong(beginDate);
        objectOutput.writeLong(endDate);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
