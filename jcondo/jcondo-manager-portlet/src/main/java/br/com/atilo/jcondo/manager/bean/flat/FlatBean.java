package br.com.atilo.jcondo.manager.bean.flat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.event.AjaxBehaviorEvent;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.component.selectoneradio.SelectOneRadio;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.model.Telephone;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.datatype.PetType;
import br.com.atilo.jcondo.datatype.PhoneType;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonServiceUtil;
import br.com.atilo.jcondo.manager.service.TelephoneLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.permission.FlatPermission;
import br.com.atilo.jcondo.manager.bean.AbstractDomainBean;

@SessionScoped
@ManagedBean
public class FlatBean extends AbstractDomainBean<Flat> {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(FlatBean.class);

	private ParkingBean<Flat> parkingBean;

	private List<PetType> petTypes;

	private List<PetType> selectedPetTypes;

	private List<Telephone> phones;	
	
	@Override
	public void preInit() throws Exception {
		domains = FlatServiceUtil.getFlats();
		petTypes = Arrays.asList(PetType.DOG, PetType.OTHER);
		parkingBean = new ParkingBean<Flat>();
		personListBean = new ResidentListBean();
	}

	@Override
	public void onDomainSave() {
		try {
			domain = FlatServiceUtil.updateFlat(domain.getId(), domain.getDisables(), 
												domain.getBrigade(), domain.getPets(), 
												selectedPetTypes);
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.flat.save", null);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on flat update: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on flat update", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	@Override
	public void onDomainCreate() {
		// TODO Auto-generated method stub
	}
	
	@Override
	public void onDomainSelect() throws Exception {
		super.onDomainSelect();
		parkingBean.init(domain);

		if (domain.getPerson() != null) {
			setPhones(TelephoneLocalServiceUtil.getPersonPhones(domain.getPerson().getId(), PhoneType.MOBILE));
		} else {
			setPhones(null);
		}

		selectedPetTypes = domain.getPetTypes();
	}

	public void onFlatDefaulting() {
		try {
			FlatServiceUtil.setFlatDefaulting(domain.getId(), domain.getDefaulting());
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on flat update: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on flat update", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}

	}	

	public void onOptionSelect(AjaxBehaviorEvent event) {
		Boolean value = (Boolean) ((SelectOneRadio) event.getSource()).getValue();
		RequestContext.getCurrentInstance().addCallbackParam("value", value);
	}	

	public void onPrimaryPersonSelect(Person person) {
		try {
			domain = FlatServiceUtil.setFlatPerson(domain.getId(), person.getId());
			setPhones(TelephoneLocalServiceUtil.getPersonPhones(person.getId(), PhoneType.MOBILE));
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.flat.primary.user.change", null);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on primary person change: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on primary person change", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}
	
	public List<Flat> showFlats() {
		List<Flat> flats = null;
		try {
			flats = FlatLocalServiceUtil.getFlats();
			flats.remove(domain);
		} catch (Exception e) {
			LOGGER.error("Failure on getting all flats", e);
		}
		return flats;
	}

	public List<Person> displayResidents() throws Exception {
		List<Person> people = new ArrayList<Person>();
		long domainId = domain.getId();

		try {
			CollectionUtils.addAll(people, PersonServiceUtil.getDomainPeopleByType(domainId, PersonType.OWNER).iterator());
		} catch (Exception e) {
		}

		try {
			CollectionUtils.addAll(people, PersonServiceUtil.getDomainPeopleByType(domainId, PersonType.RENTER).iterator());
		} catch (Exception e) {
		}

		try {
			CollectionUtils.addAll(people, PersonServiceUtil.getDomainPeopleByType(domainId, PersonType.MANAGER).iterator());
		} catch (Exception e) {
		}

		return people;
	}

	public boolean canSetDefaulting() {
		try {
			return FlatPermission.hasFlatPermission(Permission.SET_DEFAULTING, domain.getId());
		} catch (Exception e) {
			LOGGER.error("Failure on checking defaulting permission", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}

		return false;
	}

	public ParkingBean<Flat> getParkingBean() {
		return parkingBean;
	}

	public void setParkingBean(ParkingBean<Flat> parkingBean) {
		this.parkingBean = parkingBean;
	}

	public List<PetType> getPetTypes() {
		return petTypes;
	}

	public void setPetTypes(List<PetType> petTypes) {
		this.petTypes = petTypes;
	}

	public List<PetType> getSelectedPetTypes() {
		return selectedPetTypes;
	}

	public void setSelectedPetTypes(List<PetType> selectedPetTypes) {
		this.selectedPetTypes = selectedPetTypes;
	}

	public List<Telephone> getPhones() {
		return phones;
	}

	public void setPhones(List<Telephone> phones) {
		this.phones = phones;
	}

}
