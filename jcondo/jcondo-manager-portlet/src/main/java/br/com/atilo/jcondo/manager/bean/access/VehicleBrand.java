package br.com.atilo.jcondo.manager.bean.access;

import java.io.Serializable;

public class VehicleBrand implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String name;
	
	private String logo;

	public VehicleBrand() {
		super();
	}

	public VehicleBrand(String name, String logo) {
		this.name = name;
		this.logo = logo;
	}

	@Override
	public boolean equals(Object obj) {
		return obj != null && (super.equals(obj) || (obj instanceof VehicleBrand && ((VehicleBrand) obj).getName().equals(name)));
	}

	@Override
	public int hashCode() {
		return super.hashCode() + name.hashCode();
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}
}

