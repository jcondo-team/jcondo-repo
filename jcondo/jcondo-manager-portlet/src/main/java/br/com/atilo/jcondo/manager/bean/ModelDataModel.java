package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.model.ListDataModel;

import org.primefaces.model.SelectableDataModel;

import com.liferay.portal.model.BaseModel;

public class ModelDataModel<Model extends BaseModel<Model>> extends ListDataModel<Model> implements SelectableDataModel<Model>, Serializable {

	private static final long serialVersionUID = 1L;

	public static final Integer ASCENDING_ORDER = 0;

	public static final Integer DESCENDING_ORDER = 1;

	public ModelDataModel(List<Model> models) {
		super(models);
	}

	@SuppressWarnings("unchecked")
	public void remove(Model model) {
		((List<Model>) this.getWrappedData()).remove(model);
	}

	@SuppressWarnings("unchecked")
	public void update(Model model) {
		int index = ((List<Model>) this.getWrappedData()).indexOf(model);

		if (index >= 0) {
			((List<Model>) this.getWrappedData()).set(index, model);
		} else {
			((List<Model>) this.getWrappedData()).add(model);
		}
	}

    @SuppressWarnings("unchecked")
    public Model getRowData(String rowKey) {
        for(Model model : ((List<Model>) this.getWrappedData())) {
            if (Long.valueOf(rowKey) == model.getPrimaryKeyObj()) {
            	return model;
            }
        }

        return null;
    }

    public Object getRowKey(Model model) {
        return model.getPrimaryKeyObj();
    }

    @SuppressWarnings("unchecked")
	public void clear() {
    	((List<Model>) this.getWrappedData()).clear();
	}
    
    @SuppressWarnings("unchecked")
	public void addAll(List<Model> models) {
    	((List<Model>) this.getWrappedData()).addAll(models);
	}
}