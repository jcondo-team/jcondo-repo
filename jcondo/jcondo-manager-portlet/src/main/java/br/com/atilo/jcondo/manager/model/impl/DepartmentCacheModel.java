package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Department;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Department in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Department
 * @generated
 */
public class DepartmentCacheModel implements CacheModel<Department>,
    Externalizable {
    public long id;
    public long organizationId;
    public String name;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{id=");
        sb.append(id);
        sb.append(", organizationId=");
        sb.append(organizationId);
        sb.append(", name=");
        sb.append(name);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Department toEntityModel() {
        DepartmentImpl departmentImpl = new DepartmentImpl();

        departmentImpl.setId(id);
        departmentImpl.setOrganizationId(organizationId);

        if (name == null) {
            departmentImpl.setName(StringPool.BLANK);
        } else {
            departmentImpl.setName(name);
        }

        if (oprDate == Long.MIN_VALUE) {
            departmentImpl.setOprDate(null);
        } else {
            departmentImpl.setOprDate(new Date(oprDate));
        }

        departmentImpl.setOprUser(oprUser);

        departmentImpl.resetOriginalValues();

        return departmentImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        organizationId = objectInput.readLong();
        name = objectInput.readUTF();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(organizationId);

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
