package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchMembershipException;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.impl.MembershipImpl;
import br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.MembershipPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the membership service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MembershipPersistence
 * @see MembershipUtil
 * @generated
 */
public class MembershipPersistenceImpl extends BasePersistenceImpl<Membership>
    implements MembershipPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link MembershipUtil} to access the membership persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = MembershipImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPerson",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON =
        new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPerson",
            new String[] { Long.class.getName() },
            MembershipModelImpl.PERSONID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PERSON = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPerson",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_PERSON_PERSONID_2 = "membership.personId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DOMAIN = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDomain",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN =
        new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByDomain",
            new String[] { Long.class.getName() },
            MembershipModelImpl.DOMAINID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DOMAIN = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDomain",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_DOMAIN_DOMAINID_2 = "membership.domainId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPE = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByType",
            new String[] {
                Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPE = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByType",
            new String[] { Integer.class.getName() },
            MembershipModelImpl.TYPEID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TYPE = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByType",
            new String[] { Integer.class.getName() });
    private static final String _FINDER_COLUMN_TYPE_TYPEID_2 = "membership.typeId = ?";
    public static final FinderPath FINDER_PATH_FETCH_BY_DOMAINANDPERSON = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByDomainAndPerson",
            new String[] { Long.class.getName(), Long.class.getName() },
            MembershipModelImpl.DOMAINID_COLUMN_BITMASK |
            MembershipModelImpl.PERSONID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DOMAINANDPERSON = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByDomainAndPerson",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_DOMAINANDPERSON_DOMAINID_2 = "membership.domainId = ? AND ";
    private static final String _FINDER_COLUMN_DOMAINANDPERSON_PERSONID_2 = "membership.personId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DOMAINANDTYPE =
        new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDomainAndType",
            new String[] {
                Long.class.getName(), Integer.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAINANDTYPE =
        new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByDomainAndType",
            new String[] { Long.class.getName(), Integer.class.getName() },
            MembershipModelImpl.DOMAINID_COLUMN_BITMASK |
            MembershipModelImpl.TYPEID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DOMAINANDTYPE = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDomainAndType",
            new String[] { Long.class.getName(), Integer.class.getName() });
    private static final String _FINDER_COLUMN_DOMAINANDTYPE_DOMAINID_2 = "membership.domainId = ? AND ";
    private static final String _FINDER_COLUMN_DOMAINANDTYPE_TYPEID_2 = "membership.typeId = ?";
    public static final FinderPath FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, MembershipImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByPersonDomainAndType",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                Integer.class.getName()
            },
            MembershipModelImpl.PERSONID_COLUMN_BITMASK |
            MembershipModelImpl.DOMAINID_COLUMN_BITMASK |
            MembershipModelImpl.TYPEID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PERSONDOMAINANDTYPE = new FinderPath(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByPersonDomainAndType",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                Integer.class.getName()
            });
    private static final String _FINDER_COLUMN_PERSONDOMAINANDTYPE_PERSONID_2 = "membership.personId = ? AND ";
    private static final String _FINDER_COLUMN_PERSONDOMAINANDTYPE_DOMAINID_2 = "membership.domainId = ? AND ";
    private static final String _FINDER_COLUMN_PERSONDOMAINANDTYPE_TYPEID_2 = "membership.typeId = ?";
    private static final String _SQL_SELECT_MEMBERSHIP = "SELECT membership FROM Membership membership";
    private static final String _SQL_SELECT_MEMBERSHIP_WHERE = "SELECT membership FROM Membership membership WHERE ";
    private static final String _SQL_COUNT_MEMBERSHIP = "SELECT COUNT(membership) FROM Membership membership";
    private static final String _SQL_COUNT_MEMBERSHIP_WHERE = "SELECT COUNT(membership) FROM Membership membership WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "membership.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Membership exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Membership exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(MembershipPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Membership _nullMembership = new MembershipImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Membership> toCacheModel() {
                return _nullMembershipCacheModel;
            }
        };

    private static CacheModel<Membership> _nullMembershipCacheModel = new CacheModel<Membership>() {
            @Override
            public Membership toEntityModel() {
                return _nullMembership;
            }
        };

    public MembershipPersistenceImpl() {
        setModelClass(Membership.class);
    }

    /**
     * Returns all the memberships where personId = &#63;.
     *
     * @param personId the person ID
     * @return the matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByPerson(long personId)
        throws SystemException {
        return findByPerson(personId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the memberships where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @return the range of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByPerson(long personId, int start, int end)
        throws SystemException {
        return findByPerson(personId, start, end, null);
    }

    /**
     * Returns an ordered range of all the memberships where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByPerson(long personId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId, start, end, orderByComparator };
        }

        List<Membership> list = (List<Membership>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Membership membership : list) {
                if ((personId != membership.getPersonId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(MembershipModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                if (!pagination) {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Membership>(list);
                } else {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first membership in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByPerson_First(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByPerson_First(personId, orderByComparator);

        if (membership != null) {
            return membership;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMembershipException(msg.toString());
    }

    /**
     * Returns the first membership in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByPerson_First(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Membership> list = findByPerson(personId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last membership in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByPerson_Last(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByPerson_Last(personId, orderByComparator);

        if (membership != null) {
            return membership;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMembershipException(msg.toString());
    }

    /**
     * Returns the last membership in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByPerson_Last(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByPerson(personId);

        if (count == 0) {
            return null;
        }

        List<Membership> list = findByPerson(personId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the memberships before and after the current membership in the ordered set where personId = &#63;.
     *
     * @param id the primary key of the current membership
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership[] findByPerson_PrevAndNext(long id, long personId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Membership[] array = new MembershipImpl[3];

            array[0] = getByPerson_PrevAndNext(session, membership, personId,
                    orderByComparator, true);

            array[1] = membership;

            array[2] = getByPerson_PrevAndNext(session, membership, personId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Membership getByPerson_PrevAndNext(Session session,
        Membership membership, long personId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

        query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(MembershipModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(personId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(membership);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Membership> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the memberships where personId = &#63; from the database.
     *
     * @param personId the person ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByPerson(long personId) throws SystemException {
        for (Membership membership : findByPerson(personId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(membership);
        }
    }

    /**
     * Returns the number of memberships where personId = &#63;.
     *
     * @param personId the person ID
     * @return the number of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByPerson(long personId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PERSON;

        Object[] finderArgs = new Object[] { personId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the memberships where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @return the matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByDomain(long domainId)
        throws SystemException {
        return findByDomain(domainId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the memberships where domainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param domainId the domain ID
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @return the range of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByDomain(long domainId, int start, int end)
        throws SystemException {
        return findByDomain(domainId, start, end, null);
    }

    /**
     * Returns an ordered range of all the memberships where domainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param domainId the domain ID
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByDomain(long domainId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN;
            finderArgs = new Object[] { domainId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DOMAIN;
            finderArgs = new Object[] { domainId, start, end, orderByComparator };
        }

        List<Membership> list = (List<Membership>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Membership membership : list) {
                if ((domainId != membership.getDomainId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_DOMAIN_DOMAINID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(MembershipModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                if (!pagination) {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Membership>(list);
                } else {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first membership in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByDomain_First(long domainId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByDomain_First(domainId, orderByComparator);

        if (membership != null) {
            return membership;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("domainId=");
        msg.append(domainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMembershipException(msg.toString());
    }

    /**
     * Returns the first membership in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByDomain_First(long domainId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Membership> list = findByDomain(domainId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last membership in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByDomain_Last(long domainId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByDomain_Last(domainId, orderByComparator);

        if (membership != null) {
            return membership;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("domainId=");
        msg.append(domainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMembershipException(msg.toString());
    }

    /**
     * Returns the last membership in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByDomain_Last(long domainId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDomain(domainId);

        if (count == 0) {
            return null;
        }

        List<Membership> list = findByDomain(domainId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the memberships before and after the current membership in the ordered set where domainId = &#63;.
     *
     * @param id the primary key of the current membership
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership[] findByDomain_PrevAndNext(long id, long domainId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Membership[] array = new MembershipImpl[3];

            array[0] = getByDomain_PrevAndNext(session, membership, domainId,
                    orderByComparator, true);

            array[1] = membership;

            array[2] = getByDomain_PrevAndNext(session, membership, domainId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Membership getByDomain_PrevAndNext(Session session,
        Membership membership, long domainId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

        query.append(_FINDER_COLUMN_DOMAIN_DOMAINID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(MembershipModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(domainId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(membership);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Membership> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the memberships where domainId = &#63; from the database.
     *
     * @param domainId the domain ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDomain(long domainId) throws SystemException {
        for (Membership membership : findByDomain(domainId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(membership);
        }
    }

    /**
     * Returns the number of memberships where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @return the number of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDomain(long domainId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DOMAIN;

        Object[] finderArgs = new Object[] { domainId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_DOMAIN_DOMAINID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the memberships where typeId = &#63;.
     *
     * @param typeId the type ID
     * @return the matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByType(int typeId) throws SystemException {
        return findByType(typeId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the memberships where typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param typeId the type ID
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @return the range of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByType(int typeId, int start, int end)
        throws SystemException {
        return findByType(typeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the memberships where typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param typeId the type ID
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByType(int typeId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPE;
            finderArgs = new Object[] { typeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_TYPE;
            finderArgs = new Object[] { typeId, start, end, orderByComparator };
        }

        List<Membership> list = (List<Membership>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Membership membership : list) {
                if ((typeId != membership.getTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_TYPE_TYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(MembershipModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                if (!pagination) {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Membership>(list);
                } else {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first membership in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByType_First(int typeId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByType_First(typeId, orderByComparator);

        if (membership != null) {
            return membership;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMembershipException(msg.toString());
    }

    /**
     * Returns the first membership in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByType_First(int typeId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Membership> list = findByType(typeId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last membership in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByType_Last(int typeId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByType_Last(typeId, orderByComparator);

        if (membership != null) {
            return membership;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMembershipException(msg.toString());
    }

    /**
     * Returns the last membership in the ordered set where typeId = &#63;.
     *
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByType_Last(int typeId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByType(typeId);

        if (count == 0) {
            return null;
        }

        List<Membership> list = findByType(typeId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the memberships before and after the current membership in the ordered set where typeId = &#63;.
     *
     * @param id the primary key of the current membership
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership[] findByType_PrevAndNext(long id, int typeId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Membership[] array = new MembershipImpl[3];

            array[0] = getByType_PrevAndNext(session, membership, typeId,
                    orderByComparator, true);

            array[1] = membership;

            array[2] = getByType_PrevAndNext(session, membership, typeId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Membership getByType_PrevAndNext(Session session,
        Membership membership, int typeId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

        query.append(_FINDER_COLUMN_TYPE_TYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(MembershipModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(typeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(membership);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Membership> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the memberships where typeId = &#63; from the database.
     *
     * @param typeId the type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByType(int typeId) throws SystemException {
        for (Membership membership : findByType(typeId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(membership);
        }
    }

    /**
     * Returns the number of memberships where typeId = &#63;.
     *
     * @param typeId the type ID
     * @return the number of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByType(int typeId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TYPE;

        Object[] finderArgs = new Object[] { typeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_TYPE_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(typeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the membership where domainId = &#63; and personId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchMembershipException} if it could not be found.
     *
     * @param domainId the domain ID
     * @param personId the person ID
     * @return the matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByDomainAndPerson(long domainId, long personId)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByDomainAndPerson(domainId, personId);

        if (membership == null) {
            StringBundler msg = new StringBundler(6);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("domainId=");
            msg.append(domainId);

            msg.append(", personId=");
            msg.append(personId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchMembershipException(msg.toString());
        }

        return membership;
    }

    /**
     * Returns the membership where domainId = &#63; and personId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param domainId the domain ID
     * @param personId the person ID
     * @return the matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByDomainAndPerson(long domainId, long personId)
        throws SystemException {
        return fetchByDomainAndPerson(domainId, personId, true);
    }

    /**
     * Returns the membership where domainId = &#63; and personId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param domainId the domain ID
     * @param personId the person ID
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByDomainAndPerson(long domainId, long personId,
        boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] { domainId, personId };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_DOMAINANDPERSON,
                    finderArgs, this);
        }

        if (result instanceof Membership) {
            Membership membership = (Membership) result;

            if ((domainId != membership.getDomainId()) ||
                    (personId != membership.getPersonId())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_DOMAINANDPERSON_DOMAINID_2);

            query.append(_FINDER_COLUMN_DOMAINANDPERSON_PERSONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                qPos.add(personId);

                List<Membership> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOMAINANDPERSON,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "MembershipPersistenceImpl.fetchByDomainAndPerson(long, long, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Membership membership = list.get(0);

                    result = membership;

                    cacheResult(membership);

                    if ((membership.getDomainId() != domainId) ||
                            (membership.getPersonId() != personId)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOMAINANDPERSON,
                            finderArgs, membership);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DOMAINANDPERSON,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Membership) result;
        }
    }

    /**
     * Removes the membership where domainId = &#63; and personId = &#63; from the database.
     *
     * @param domainId the domain ID
     * @param personId the person ID
     * @return the membership that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership removeByDomainAndPerson(long domainId, long personId)
        throws NoSuchMembershipException, SystemException {
        Membership membership = findByDomainAndPerson(domainId, personId);

        return remove(membership);
    }

    /**
     * Returns the number of memberships where domainId = &#63; and personId = &#63;.
     *
     * @param domainId the domain ID
     * @param personId the person ID
     * @return the number of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDomainAndPerson(long domainId, long personId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DOMAINANDPERSON;

        Object[] finderArgs = new Object[] { domainId, personId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_DOMAINANDPERSON_DOMAINID_2);

            query.append(_FINDER_COLUMN_DOMAINANDPERSON_PERSONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                qPos.add(personId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the memberships where domainId = &#63; and typeId = &#63;.
     *
     * @param domainId the domain ID
     * @param typeId the type ID
     * @return the matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByDomainAndType(long domainId, int typeId)
        throws SystemException {
        return findByDomainAndType(domainId, typeId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the memberships where domainId = &#63; and typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param domainId the domain ID
     * @param typeId the type ID
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @return the range of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByDomainAndType(long domainId, int typeId,
        int start, int end) throws SystemException {
        return findByDomainAndType(domainId, typeId, start, end, null);
    }

    /**
     * Returns an ordered range of all the memberships where domainId = &#63; and typeId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param domainId the domain ID
     * @param typeId the type ID
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findByDomainAndType(long domainId, int typeId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAINANDTYPE;
            finderArgs = new Object[] { domainId, typeId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DOMAINANDTYPE;
            finderArgs = new Object[] {
                    domainId, typeId,
                    
                    start, end, orderByComparator
                };
        }

        List<Membership> list = (List<Membership>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Membership membership : list) {
                if ((domainId != membership.getDomainId()) ||
                        (typeId != membership.getTypeId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_DOMAINANDTYPE_DOMAINID_2);

            query.append(_FINDER_COLUMN_DOMAINANDTYPE_TYPEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(MembershipModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                qPos.add(typeId);

                if (!pagination) {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Membership>(list);
                } else {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first membership in the ordered set where domainId = &#63; and typeId = &#63;.
     *
     * @param domainId the domain ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByDomainAndType_First(long domainId, int typeId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByDomainAndType_First(domainId, typeId,
                orderByComparator);

        if (membership != null) {
            return membership;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("domainId=");
        msg.append(domainId);

        msg.append(", typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMembershipException(msg.toString());
    }

    /**
     * Returns the first membership in the ordered set where domainId = &#63; and typeId = &#63;.
     *
     * @param domainId the domain ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByDomainAndType_First(long domainId, int typeId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Membership> list = findByDomainAndType(domainId, typeId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last membership in the ordered set where domainId = &#63; and typeId = &#63;.
     *
     * @param domainId the domain ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByDomainAndType_Last(long domainId, int typeId,
        OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByDomainAndType_Last(domainId, typeId,
                orderByComparator);

        if (membership != null) {
            return membership;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("domainId=");
        msg.append(domainId);

        msg.append(", typeId=");
        msg.append(typeId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchMembershipException(msg.toString());
    }

    /**
     * Returns the last membership in the ordered set where domainId = &#63; and typeId = &#63;.
     *
     * @param domainId the domain ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByDomainAndType_Last(long domainId, int typeId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDomainAndType(domainId, typeId);

        if (count == 0) {
            return null;
        }

        List<Membership> list = findByDomainAndType(domainId, typeId,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the memberships before and after the current membership in the ordered set where domainId = &#63; and typeId = &#63;.
     *
     * @param id the primary key of the current membership
     * @param domainId the domain ID
     * @param typeId the type ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership[] findByDomainAndType_PrevAndNext(long id, long domainId,
        int typeId, OrderByComparator orderByComparator)
        throws NoSuchMembershipException, SystemException {
        Membership membership = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Membership[] array = new MembershipImpl[3];

            array[0] = getByDomainAndType_PrevAndNext(session, membership,
                    domainId, typeId, orderByComparator, true);

            array[1] = membership;

            array[2] = getByDomainAndType_PrevAndNext(session, membership,
                    domainId, typeId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Membership getByDomainAndType_PrevAndNext(Session session,
        Membership membership, long domainId, int typeId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

        query.append(_FINDER_COLUMN_DOMAINANDTYPE_DOMAINID_2);

        query.append(_FINDER_COLUMN_DOMAINANDTYPE_TYPEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(MembershipModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(domainId);

        qPos.add(typeId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(membership);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Membership> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the memberships where domainId = &#63; and typeId = &#63; from the database.
     *
     * @param domainId the domain ID
     * @param typeId the type ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDomainAndType(long domainId, int typeId)
        throws SystemException {
        for (Membership membership : findByDomainAndType(domainId, typeId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(membership);
        }
    }

    /**
     * Returns the number of memberships where domainId = &#63; and typeId = &#63;.
     *
     * @param domainId the domain ID
     * @param typeId the type ID
     * @return the number of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDomainAndType(long domainId, int typeId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DOMAINANDTYPE;

        Object[] finderArgs = new Object[] { domainId, typeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_DOMAINANDTYPE_DOMAINID_2);

            query.append(_FINDER_COLUMN_DOMAINANDTYPE_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                qPos.add(typeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the membership where personId = &#63; and domainId = &#63; and typeId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchMembershipException} if it could not be found.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param typeId the type ID
     * @return the matching membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByPersonDomainAndType(long personId, long domainId,
        int typeId) throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByPersonDomainAndType(personId, domainId,
                typeId);

        if (membership == null) {
            StringBundler msg = new StringBundler(8);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("personId=");
            msg.append(personId);

            msg.append(", domainId=");
            msg.append(domainId);

            msg.append(", typeId=");
            msg.append(typeId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchMembershipException(msg.toString());
        }

        return membership;
    }

    /**
     * Returns the membership where personId = &#63; and domainId = &#63; and typeId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param typeId the type ID
     * @return the matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByPersonDomainAndType(long personId, long domainId,
        int typeId) throws SystemException {
        return fetchByPersonDomainAndType(personId, domainId, typeId, true);
    }

    /**
     * Returns the membership where personId = &#63; and domainId = &#63; and typeId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param typeId the type ID
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching membership, or <code>null</code> if a matching membership could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByPersonDomainAndType(long personId, long domainId,
        int typeId, boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] { personId, domainId, typeId };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE,
                    finderArgs, this);
        }

        if (result instanceof Membership) {
            Membership membership = (Membership) result;

            if ((personId != membership.getPersonId()) ||
                    (domainId != membership.getDomainId()) ||
                    (typeId != membership.getTypeId())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(5);

            query.append(_SQL_SELECT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDTYPE_PERSONID_2);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDTYPE_DOMAINID_2);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDTYPE_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                qPos.add(domainId);

                qPos.add(typeId);

                List<Membership> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "MembershipPersistenceImpl.fetchByPersonDomainAndType(long, long, int, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Membership membership = list.get(0);

                    result = membership;

                    cacheResult(membership);

                    if ((membership.getPersonId() != personId) ||
                            (membership.getDomainId() != domainId) ||
                            (membership.getTypeId() != typeId)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE,
                            finderArgs, membership);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Membership) result;
        }
    }

    /**
     * Removes the membership where personId = &#63; and domainId = &#63; and typeId = &#63; from the database.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param typeId the type ID
     * @return the membership that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership removeByPersonDomainAndType(long personId, long domainId,
        int typeId) throws NoSuchMembershipException, SystemException {
        Membership membership = findByPersonDomainAndType(personId, domainId,
                typeId);

        return remove(membership);
    }

    /**
     * Returns the number of memberships where personId = &#63; and domainId = &#63; and typeId = &#63;.
     *
     * @param personId the person ID
     * @param domainId the domain ID
     * @param typeId the type ID
     * @return the number of matching memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByPersonDomainAndType(long personId, long domainId,
        int typeId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PERSONDOMAINANDTYPE;

        Object[] finderArgs = new Object[] { personId, domainId, typeId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(4);

            query.append(_SQL_COUNT_MEMBERSHIP_WHERE);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDTYPE_PERSONID_2);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDTYPE_DOMAINID_2);

            query.append(_FINDER_COLUMN_PERSONDOMAINANDTYPE_TYPEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                qPos.add(domainId);

                qPos.add(typeId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the membership in the entity cache if it is enabled.
     *
     * @param membership the membership
     */
    @Override
    public void cacheResult(Membership membership) {
        EntityCacheUtil.putResult(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipImpl.class, membership.getPrimaryKey(), membership);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOMAINANDPERSON,
            new Object[] { membership.getDomainId(), membership.getPersonId() },
            membership);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE,
            new Object[] {
                membership.getPersonId(), membership.getDomainId(),
                membership.getTypeId()
            }, membership);

        membership.resetOriginalValues();
    }

    /**
     * Caches the memberships in the entity cache if it is enabled.
     *
     * @param memberships the memberships
     */
    @Override
    public void cacheResult(List<Membership> memberships) {
        for (Membership membership : memberships) {
            if (EntityCacheUtil.getResult(
                        MembershipModelImpl.ENTITY_CACHE_ENABLED,
                        MembershipImpl.class, membership.getPrimaryKey()) == null) {
                cacheResult(membership);
            } else {
                membership.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all memberships.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(MembershipImpl.class.getName());
        }

        EntityCacheUtil.clearCache(MembershipImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the membership.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Membership membership) {
        EntityCacheUtil.removeResult(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipImpl.class, membership.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(membership);
    }

    @Override
    public void clearCache(List<Membership> memberships) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Membership membership : memberships) {
            EntityCacheUtil.removeResult(MembershipModelImpl.ENTITY_CACHE_ENABLED,
                MembershipImpl.class, membership.getPrimaryKey());

            clearUniqueFindersCache(membership);
        }
    }

    protected void cacheUniqueFindersCache(Membership membership) {
        if (membership.isNew()) {
            Object[] args = new Object[] {
                    membership.getDomainId(), membership.getPersonId()
                };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DOMAINANDPERSON,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOMAINANDPERSON,
                args, membership);

            args = new Object[] {
                    membership.getPersonId(), membership.getDomainId(),
                    membership.getTypeId()
                };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PERSONDOMAINANDTYPE,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE,
                args, membership);
        } else {
            MembershipModelImpl membershipModelImpl = (MembershipModelImpl) membership;

            if ((membershipModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_DOMAINANDPERSON.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        membership.getDomainId(), membership.getPersonId()
                    };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_DOMAINANDPERSON,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_DOMAINANDPERSON,
                    args, membership);
            }

            if ((membershipModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        membership.getPersonId(), membership.getDomainId(),
                        membership.getTypeId()
                    };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_PERSONDOMAINANDTYPE,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE,
                    args, membership);
            }
        }
    }

    protected void clearUniqueFindersCache(Membership membership) {
        MembershipModelImpl membershipModelImpl = (MembershipModelImpl) membership;

        Object[] args = new Object[] {
                membership.getDomainId(), membership.getPersonId()
            };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAINANDPERSON, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DOMAINANDPERSON, args);

        if ((membershipModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_DOMAINANDPERSON.getColumnBitmask()) != 0) {
            args = new Object[] {
                    membershipModelImpl.getOriginalDomainId(),
                    membershipModelImpl.getOriginalPersonId()
                };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAINANDPERSON,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_DOMAINANDPERSON,
                args);
        }

        args = new Object[] {
                membership.getPersonId(), membership.getDomainId(),
                membership.getTypeId()
            };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSONDOMAINANDTYPE,
            args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE,
            args);

        if ((membershipModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE.getColumnBitmask()) != 0) {
            args = new Object[] {
                    membershipModelImpl.getOriginalPersonId(),
                    membershipModelImpl.getOriginalDomainId(),
                    membershipModelImpl.getOriginalTypeId()
                };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSONDOMAINANDTYPE,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_PERSONDOMAINANDTYPE,
                args);
        }
    }

    /**
     * Creates a new membership with the primary key. Does not add the membership to the database.
     *
     * @param id the primary key for the new membership
     * @return the new membership
     */
    @Override
    public Membership create(long id) {
        Membership membership = new MembershipImpl();

        membership.setNew(true);
        membership.setPrimaryKey(id);

        return membership;
    }

    /**
     * Removes the membership with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the membership
     * @return the membership that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership remove(long id)
        throws NoSuchMembershipException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the membership with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the membership
     * @return the membership that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership remove(Serializable primaryKey)
        throws NoSuchMembershipException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Membership membership = (Membership) session.get(MembershipImpl.class,
                    primaryKey);

            if (membership == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchMembershipException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(membership);
        } catch (NoSuchMembershipException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Membership removeImpl(Membership membership)
        throws SystemException {
        membership = toUnwrappedModel(membership);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(membership)) {
                membership = (Membership) session.get(MembershipImpl.class,
                        membership.getPrimaryKeyObj());
            }

            if (membership != null) {
                session.delete(membership);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (membership != null) {
            clearCache(membership);
        }

        return membership;
    }

    @Override
    public Membership updateImpl(
        br.com.atilo.jcondo.manager.model.Membership membership)
        throws SystemException {
        membership = toUnwrappedModel(membership);

        boolean isNew = membership.isNew();

        MembershipModelImpl membershipModelImpl = (MembershipModelImpl) membership;

        Session session = null;

        try {
            session = openSession();

            if (membership.isNew()) {
                session.save(membership);

                membership.setNew(false);
            } else {
                session.merge(membership);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !MembershipModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((membershipModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        membershipModelImpl.getOriginalPersonId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);

                args = new Object[] { membershipModelImpl.getPersonId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);
            }

            if ((membershipModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        membershipModelImpl.getOriginalDomainId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAIN, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN,
                    args);

                args = new Object[] { membershipModelImpl.getDomainId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAIN, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN,
                    args);
            }

            if ((membershipModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        membershipModelImpl.getOriginalTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPE,
                    args);

                args = new Object[] { membershipModelImpl.getTypeId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TYPE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_TYPE,
                    args);
            }

            if ((membershipModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAINANDTYPE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        membershipModelImpl.getOriginalDomainId(),
                        membershipModelImpl.getOriginalTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAINANDTYPE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAINANDTYPE,
                    args);

                args = new Object[] {
                        membershipModelImpl.getDomainId(),
                        membershipModelImpl.getTypeId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAINANDTYPE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAINANDTYPE,
                    args);
            }
        }

        EntityCacheUtil.putResult(MembershipModelImpl.ENTITY_CACHE_ENABLED,
            MembershipImpl.class, membership.getPrimaryKey(), membership);

        clearUniqueFindersCache(membership);
        cacheUniqueFindersCache(membership);

        return membership;
    }

    protected Membership toUnwrappedModel(Membership membership) {
        if (membership instanceof MembershipImpl) {
            return membership;
        }

        MembershipImpl membershipImpl = new MembershipImpl();

        membershipImpl.setNew(membership.isNew());
        membershipImpl.setPrimaryKey(membership.getPrimaryKey());

        membershipImpl.setId(membership.getId());
        membershipImpl.setPersonId(membership.getPersonId());
        membershipImpl.setDomainId(membership.getDomainId());
        membershipImpl.setTypeId(membership.getTypeId());
        membershipImpl.setOprDate(membership.getOprDate());
        membershipImpl.setOprUser(membership.getOprUser());

        return membershipImpl;
    }

    /**
     * Returns the membership with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the membership
     * @return the membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByPrimaryKey(Serializable primaryKey)
        throws NoSuchMembershipException, SystemException {
        Membership membership = fetchByPrimaryKey(primaryKey);

        if (membership == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchMembershipException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return membership;
    }

    /**
     * Returns the membership with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchMembershipException} if it could not be found.
     *
     * @param id the primary key of the membership
     * @return the membership
     * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership findByPrimaryKey(long id)
        throws NoSuchMembershipException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the membership with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the membership
     * @return the membership, or <code>null</code> if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Membership membership = (Membership) EntityCacheUtil.getResult(MembershipModelImpl.ENTITY_CACHE_ENABLED,
                MembershipImpl.class, primaryKey);

        if (membership == _nullMembership) {
            return null;
        }

        if (membership == null) {
            Session session = null;

            try {
                session = openSession();

                membership = (Membership) session.get(MembershipImpl.class,
                        primaryKey);

                if (membership != null) {
                    cacheResult(membership);
                } else {
                    EntityCacheUtil.putResult(MembershipModelImpl.ENTITY_CACHE_ENABLED,
                        MembershipImpl.class, primaryKey, _nullMembership);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(MembershipModelImpl.ENTITY_CACHE_ENABLED,
                    MembershipImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return membership;
    }

    /**
     * Returns the membership with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the membership
     * @return the membership, or <code>null</code> if a membership with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Membership fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the memberships.
     *
     * @return the memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the memberships.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @return the range of memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the memberships.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of memberships
     * @param end the upper bound of the range of memberships (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Membership> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Membership> list = (List<Membership>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_MEMBERSHIP);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_MEMBERSHIP;

                if (pagination) {
                    sql = sql.concat(MembershipModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Membership>(list);
                } else {
                    list = (List<Membership>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the memberships from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Membership membership : findAll()) {
            remove(membership);
        }
    }

    /**
     * Returns the number of memberships.
     *
     * @return the number of memberships
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_MEMBERSHIP);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the membership persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Membership")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Membership>> listenersList = new ArrayList<ModelListener<Membership>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Membership>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(MembershipImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
