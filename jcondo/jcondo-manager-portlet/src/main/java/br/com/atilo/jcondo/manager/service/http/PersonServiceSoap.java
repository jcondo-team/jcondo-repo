/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.http;

import br.com.atilo.jcondo.manager.service.PersonServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * <p>
 * This class provides a SOAP utility for the
 * {@link br.com.atilo.jcondo.manager.service.PersonServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 * </p>
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.manager.model.PersonSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.manager.model.Person}, that is translated to a
 * {@link br.com.atilo.jcondo.manager.model.PersonSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at
 * http://localhost:8080/api/secure/axis. Set the property
 * <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author    anderson
 * @see       PersonServiceHttp
 * @see       br.com.atilo.jcondo.manager.model.PersonSoap
 * @see       br.com.atilo.jcondo.manager.service.PersonServiceUtil
 * @generated
 */
public class PersonServiceSoap {
	public static br.com.atilo.jcondo.manager.model.PersonSoap createPerson()
		throws RemoteException {
		try {
			br.com.atilo.jcondo.manager.model.Person returnValue = PersonServiceUtil.createPerson();

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.manager.model.PersonSoap addPerson(
		java.lang.String name, java.lang.String surname,
		java.lang.String identity, java.lang.String email,
		java.util.Date birthday,
		br.com.atilo.jcondo.datatype.Gender gender,
		java.lang.String remark,
		br.com.atilo.jcondo.datatype.PersonType personType,
		long domainId) throws RemoteException {
		try {
			br.com.atilo.jcondo.manager.model.Person returnValue = PersonServiceUtil.addPerson(name,
					surname, identity, email, birthday, gender, remark,
					personType, domainId);

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.manager.model.PersonSoap updatePerson(
		long id, java.lang.String name, java.lang.String surname,
		java.lang.String identity, java.lang.String email,
		java.util.Date birthday,
		br.com.atilo.jcondo.datatype.Gender gender,
		java.lang.String remark, long domainId,
		br.com.atilo.jcondo.manager.model.MembershipSoap membership)
		throws RemoteException {
		try {
			br.com.atilo.jcondo.manager.model.Person returnValue = PersonServiceUtil.updatePerson(id,
					name, surname, identity, email, birthday, gender, remark,
					domainId,
					br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl.toModel(
						membership));

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.manager.model.PersonSoap updatePortrait(
		long personId, br.com.atilo.jcondo.Image portrait)
		throws RemoteException {
		try {
			br.com.atilo.jcondo.manager.model.Person returnValue = PersonServiceUtil.updatePortrait(personId,
					portrait);

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static void updatePassword(long personId, java.lang.String password,
		java.lang.String newPassword) throws RemoteException {
		try {
			PersonServiceUtil.updatePassword(personId, password, newPassword);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.manager.model.PersonSoap getPersonByIdentity(
		java.lang.String identity) throws RemoteException {
		try {
			br.com.atilo.jcondo.manager.model.Person returnValue = PersonServiceUtil.getPersonByIdentity(identity);

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.manager.model.PersonSoap getPerson(long id)
		throws RemoteException {
		try {
			br.com.atilo.jcondo.manager.model.Person returnValue = PersonServiceUtil.getPerson(id);

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.manager.model.PersonSoap getPerson()
		throws RemoteException {
		try {
			br.com.atilo.jcondo.manager.model.Person returnValue = PersonServiceUtil.getPerson();

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModel(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.datatype.PersonType[] getPersonTypes(
		long domainId) throws RemoteException {
		try {
			java.util.List<br.com.atilo.jcondo.datatype.PersonType> returnValue =
				PersonServiceUtil.getPersonTypes(domainId);

			return returnValue.toArray(new br.com.atilo.jcondo.datatype.PersonType[returnValue.size()]);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.manager.model.PersonSoap[] getDomainPeople(
		long domainId) throws RemoteException {
		try {
			java.util.List<br.com.atilo.jcondo.manager.model.Person> returnValue = PersonServiceUtil.getDomainPeople(domainId);

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.manager.model.PersonSoap[] getDomainPeopleByType(
		long domainId,
		br.com.atilo.jcondo.datatype.PersonType personType)
		throws RemoteException {
		try {
			java.util.List<br.com.atilo.jcondo.manager.model.Person> returnValue = PersonServiceUtil.getDomainPeopleByType(domainId,
					personType);

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static br.com.atilo.jcondo.manager.model.PersonSoap[] getDomainPeopleByName(
		long domainId, java.lang.String name) throws RemoteException {
		try {
			java.util.List<br.com.atilo.jcondo.manager.model.Person> returnValue = PersonServiceUtil.getDomainPeopleByName(domainId,
					name);

			return br.com.atilo.jcondo.manager.model.PersonSoap.toSoapModels(returnValue);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static void completeUseRegistration(long companyId,
		java.lang.String emailAddress, boolean autoPassword, boolean sendEmail)
		throws RemoteException {
		try {
			PersonServiceUtil.completeUseRegistration(companyId, emailAddress,
				autoPassword, sendEmail);
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	public static boolean authenticatePerson(long personId,
		java.lang.String password) throws RemoteException {
		try {
			boolean returnValue = PersonServiceUtil.authenticatePerson(personId,
					password);

			return returnValue;
		}
		catch (Exception e) {
			_log.error(e, e);

			throw new RemoteException(e.getMessage());
		}
	}

	private static Log _log = LogFactoryUtil.getLog(PersonServiceSoap.class);
}