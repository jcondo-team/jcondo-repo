package br.com.atilo.jcondo.manager.service.http;

import br.com.atilo.jcondo.manager.service.KinshipServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.manager.service.KinshipServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.manager.model.KinshipSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.manager.model.Kinship}, that is translated to a
 * {@link br.com.atilo.jcondo.manager.model.KinshipSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see KinshipServiceHttp
 * @see br.com.atilo.jcondo.manager.model.KinshipSoap
 * @see br.com.atilo.jcondo.manager.service.KinshipServiceUtil
 * @generated
 */
public class KinshipServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(KinshipServiceSoap.class);

    public static br.com.atilo.jcondo.manager.model.KinshipSoap createKinship(
        long personId, long relativeId,
        br.com.atilo.jcondo.datatype.KinType type) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Kinship returnValue = KinshipServiceUtil.createKinship(personId,
                    relativeId, type);

            return br.com.atilo.jcondo.manager.model.KinshipSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.KinshipSoap addKinship(
        long personId, long relativeId,
        br.com.atilo.jcondo.datatype.KinType type) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Kinship returnValue = KinshipServiceUtil.addKinship(personId,
                    relativeId, type);

            return br.com.atilo.jcondo.manager.model.KinshipSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.KinshipSoap updateKinship(
        long kinshipId, br.com.atilo.jcondo.datatype.KinType type)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Kinship returnValue = KinshipServiceUtil.updateKinship(kinshipId,
                    type);

            return br.com.atilo.jcondo.manager.model.KinshipSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.KinshipSoap[] getKinships(
        long personId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Kinship> returnValue =
                KinshipServiceUtil.getKinships(personId);

            return br.com.atilo.jcondo.manager.model.KinshipSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.KinshipSoap getPersonKinship(
        long personId, long relativeId) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Kinship returnValue = KinshipServiceUtil.getPersonKinship(personId,
                    relativeId);

            return br.com.atilo.jcondo.manager.model.KinshipSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
