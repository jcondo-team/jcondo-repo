/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.NoSuchAccessException;
import br.com.atilo.jcondo.manager.NoSuchDepartmentException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.NoSuchPersonException;
import br.com.atilo.jcondo.manager.NoSuchVehicleException;
import br.com.atilo.jcondo.manager.model.Access;
import br.com.atilo.jcondo.manager.service.base.AccessLocalServiceBaseImpl;

/**
 * The implementation of the access local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.AccessLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.AccessLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.AccessLocalServiceUtil
 */
public class AccessLocalServiceImpl extends AccessLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.AccessLocalServiceUtil} to access the access local service.
	 */

	public Access addAccess(long destinationId, long vehicleId, long authorizerId, String authorizerName, long visitorId, String badge, String remark) throws Exception {
		try {
			flatLocalService.getFlat(destinationId);
		} catch (NoSuchFlatException e) {
			try {
				departmentLocalService.getDepartment(destinationId);
			} catch (NoSuchDepartmentException ex) {
				throw new BusinessException("exception.access.destination.not.found");
			}
		}

		if (visitorId <= 0) {
			throw new BusinessException("exception.access.visitor.not.definied");
		}

		if (remark != null && !remark.equalsIgnoreCase("guest") && 
				!accessPermissionLocalService.hasAccess(visitorId, destinationId, new Date())) {
			if (authorizerId > 0) {
				try {
					personLocalService.getPerson(authorizerId);
				} catch (NoSuchPersonException e) {
					throw new BusinessException("exception.access.authorizer.not.found");
				}
			} else {
				if (StringUtils.isEmpty(authorizerName)) {
					throw new BusinessException("exception.access.authorizer.name.empty");
				}
			}
		}

		if (!StringUtils.isEmpty(badge) && getAccessByBadge(badge) != null) {
			throw new BusinessException("exception.access.badge.in.use");
		}

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Access access;

		if (vehicleId > 0) {
			try {
				vehicleLocalService.getVehicle(vehicleId);
				access = getAccessByVehicle(vehicleId);

				if (access != null) {
					endAccess(access.getId(), "unknown exit time", userLocalService.getDefaultUserId(sc.getCompanyId()));
				}

			} catch (NoSuchVehicleException e) {
				throw new BusinessException("exception.access.vehicle.not.found");
			}
		}

		if (visitorId > 0) {
			try {
				access = accessPersistence.findByVisitor(visitorId);
				if (access.getVehicleId() == 0) {
					endAccess(access.getId(), "unknown exit time", userLocalService.getDefaultUserId(sc.getCompanyId()));
				}
			} catch (NoSuchAccessException e) {
			}
		}

		access = createAccess(counterLocalService.increment());
		access.setDestinationId(destinationId);
		access.setVehicleId(vehicleId);
		access.setAuthorizerId(authorizerId);
		access.setAuthorizerName(authorizerName);
		access.setVisitorId(visitorId);
		access.setBadge(badge);
		access.setEnded(false);
		access.setRemark(remark);
		access.setOprDate(new Date());
		access.setOprUser(sc.getUserId());

		return super.addAccess(access);
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Access addAccess(Access access) throws SystemException {
		access.setOprDate(new Date());
		access.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());
		return super.addAccess(access);
	}

	public Access addVehicleExitAccess(long vehicleId, String remark) throws PortalException, SystemException {
		try {
			vehicleLocalService.getVehicle(vehicleId);
		} catch (NoSuchVehicleException e) {
			throw new BusinessException("exception.access.vehicle.not.found");
		}		
		
		Access access;
		access = createAccess(counterLocalService.increment());
		access.setVehicleId(vehicleId);
		access.setEnded(false);
		access.setRemark("unknown entrance time");
		access.setOprDate(new Date());
		access.setOprUser(userLocalService.getDefaultUserId(ServiceContextThreadLocal.getServiceContext().getCompanyId()));

		access = super.addAccess(access);

		endAccess(access.getId(), remark);

		return access;
	}
	
	public Access getAccessByBadge(String badge) throws SystemException {
		try {
			return accessPersistence.findByBadge(badge);
		} catch (NoSuchAccessException e) {
			return null;
		}
	}

	public Access getAccessByVehicle(long vehicleId) throws SystemException {
		try {
			return accessPersistence.findByVehicle(vehicleId);
		} catch (NoSuchAccessException e) {
			return null;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Access> getVehicleAccesses() throws SystemException {
		DynamicQuery dynamicQuery = DynamicQueryFactoryUtil.forClass(Access.class);
		dynamicQuery.add(RestrictionsFactoryUtil.gt("vehicleId", 0L));
		return dynamicQuery(dynamicQuery);
	}

	public void endAccess(long accessId, String remark) throws PortalException, SystemException {
		endAccess(accessId, remark, ServiceContextThreadLocal.getServiceContext().getUserId());
	}

	private void endAccess(long accessId, String remark, long userId) throws PortalException, SystemException {
		Access access = getAccess(accessId);
		access.setEnded(true);
		access.setRemark(remark);
		access.setOprDate(new Date());
		access.setOprUser(userId);

		updateAccess(access);
		deleteAccess(access);
	}	
	
}