package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.impl.FlatImpl;
import br.com.atilo.jcondo.manager.model.impl.FlatModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.FlatPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the flat service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FlatPersistence
 * @see FlatUtil
 * @generated
 */
public class FlatPersistenceImpl extends BasePersistenceImpl<Flat>
    implements FlatPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link FlatUtil} to access the flat persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = FlatImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatModelImpl.FINDER_CACHE_ENABLED, FlatImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatModelImpl.FINDER_CACHE_ENABLED, FlatImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON = new FinderPath(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatModelImpl.FINDER_CACHE_ENABLED, FlatImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPerson",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON =
        new FinderPath(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatModelImpl.FINDER_CACHE_ENABLED, FlatImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPerson",
            new String[] { Long.class.getName() },
            FlatModelImpl.PERSONID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PERSON = new FinderPath(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPerson",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_PERSON_PERSONID_2 = "flat.personId = ?";
    public static final FinderPath FINDER_PATH_FETCH_BY_ORGANIZATION = new FinderPath(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatModelImpl.FINDER_CACHE_ENABLED, FlatImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByOrganization",
            new String[] { Long.class.getName() },
            FlatModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATION = new FinderPath(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganization",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2 = "flat.organizationId = ?";
    private static final String _SQL_SELECT_FLAT = "SELECT flat FROM Flat flat";
    private static final String _SQL_SELECT_FLAT_WHERE = "SELECT flat FROM Flat flat WHERE ";
    private static final String _SQL_COUNT_FLAT = "SELECT COUNT(flat) FROM Flat flat";
    private static final String _SQL_COUNT_FLAT_WHERE = "SELECT COUNT(flat) FROM Flat flat WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "flat.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Flat exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Flat exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(FlatPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Flat _nullFlat = new FlatImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Flat> toCacheModel() {
                return _nullFlatCacheModel;
            }
        };

    private static CacheModel<Flat> _nullFlatCacheModel = new CacheModel<Flat>() {
            @Override
            public Flat toEntityModel() {
                return _nullFlat;
            }
        };

    public FlatPersistenceImpl() {
        setModelClass(Flat.class);
    }

    /**
     * Returns all the flats where personId = &#63;.
     *
     * @param personId the person ID
     * @return the matching flats
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Flat> findByPerson(long personId) throws SystemException {
        return findByPerson(personId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the flats where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of flats
     * @param end the upper bound of the range of flats (not inclusive)
     * @return the range of matching flats
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Flat> findByPerson(long personId, int start, int end)
        throws SystemException {
        return findByPerson(personId, start, end, null);
    }

    /**
     * Returns an ordered range of all the flats where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of flats
     * @param end the upper bound of the range of flats (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching flats
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Flat> findByPerson(long personId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId, start, end, orderByComparator };
        }

        List<Flat> list = (List<Flat>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Flat flat : list) {
                if ((personId != flat.getPersonId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_FLAT_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(FlatModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                if (!pagination) {
                    list = (List<Flat>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Flat>(list);
                } else {
                    list = (List<Flat>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first flat in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching flat
     * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a matching flat could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat findByPerson_First(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchFlatException, SystemException {
        Flat flat = fetchByPerson_First(personId, orderByComparator);

        if (flat != null) {
            return flat;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchFlatException(msg.toString());
    }

    /**
     * Returns the first flat in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching flat, or <code>null</code> if a matching flat could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat fetchByPerson_First(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Flat> list = findByPerson(personId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last flat in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching flat
     * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a matching flat could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat findByPerson_Last(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchFlatException, SystemException {
        Flat flat = fetchByPerson_Last(personId, orderByComparator);

        if (flat != null) {
            return flat;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchFlatException(msg.toString());
    }

    /**
     * Returns the last flat in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching flat, or <code>null</code> if a matching flat could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat fetchByPerson_Last(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByPerson(personId);

        if (count == 0) {
            return null;
        }

        List<Flat> list = findByPerson(personId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the flats before and after the current flat in the ordered set where personId = &#63;.
     *
     * @param id the primary key of the current flat
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next flat
     * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a flat with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat[] findByPerson_PrevAndNext(long id, long personId,
        OrderByComparator orderByComparator)
        throws NoSuchFlatException, SystemException {
        Flat flat = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Flat[] array = new FlatImpl[3];

            array[0] = getByPerson_PrevAndNext(session, flat, personId,
                    orderByComparator, true);

            array[1] = flat;

            array[2] = getByPerson_PrevAndNext(session, flat, personId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Flat getByPerson_PrevAndNext(Session session, Flat flat,
        long personId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_FLAT_WHERE);

        query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(FlatModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(personId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(flat);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Flat> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the flats where personId = &#63; from the database.
     *
     * @param personId the person ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByPerson(long personId) throws SystemException {
        for (Flat flat : findByPerson(personId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(flat);
        }
    }

    /**
     * Returns the number of flats where personId = &#63;.
     *
     * @param personId the person ID
     * @return the number of matching flats
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByPerson(long personId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PERSON;

        Object[] finderArgs = new Object[] { personId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_FLAT_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the flat where organizationId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchFlatException} if it could not be found.
     *
     * @param organizationId the organization ID
     * @return the matching flat
     * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a matching flat could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat findByOrganization(long organizationId)
        throws NoSuchFlatException, SystemException {
        Flat flat = fetchByOrganization(organizationId);

        if (flat == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("organizationId=");
            msg.append(organizationId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchFlatException(msg.toString());
        }

        return flat;
    }

    /**
     * Returns the flat where organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param organizationId the organization ID
     * @return the matching flat, or <code>null</code> if a matching flat could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat fetchByOrganization(long organizationId)
        throws SystemException {
        return fetchByOrganization(organizationId, true);
    }

    /**
     * Returns the flat where organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param organizationId the organization ID
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching flat, or <code>null</code> if a matching flat could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat fetchByOrganization(long organizationId,
        boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] { organizationId };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_ORGANIZATION,
                    finderArgs, this);
        }

        if (result instanceof Flat) {
            Flat flat = (Flat) result;

            if ((organizationId != flat.getOrganizationId())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_FLAT_WHERE);

            query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(organizationId);

                List<Flat> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATION,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "FlatPersistenceImpl.fetchByOrganization(long, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Flat flat = list.get(0);

                    result = flat;

                    cacheResult(flat);

                    if ((flat.getOrganizationId() != organizationId)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATION,
                            finderArgs, flat);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGANIZATION,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Flat) result;
        }
    }

    /**
     * Removes the flat where organizationId = &#63; from the database.
     *
     * @param organizationId the organization ID
     * @return the flat that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat removeByOrganization(long organizationId)
        throws NoSuchFlatException, SystemException {
        Flat flat = findByOrganization(organizationId);

        return remove(flat);
    }

    /**
     * Returns the number of flats where organizationId = &#63;.
     *
     * @param organizationId the organization ID
     * @return the number of matching flats
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByOrganization(long organizationId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ORGANIZATION;

        Object[] finderArgs = new Object[] { organizationId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_FLAT_WHERE);

            query.append(_FINDER_COLUMN_ORGANIZATION_ORGANIZATIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(organizationId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the flat in the entity cache if it is enabled.
     *
     * @param flat the flat
     */
    @Override
    public void cacheResult(Flat flat) {
        EntityCacheUtil.putResult(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatImpl.class, flat.getPrimaryKey(), flat);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATION,
            new Object[] { flat.getOrganizationId() }, flat);

        flat.resetOriginalValues();
    }

    /**
     * Caches the flats in the entity cache if it is enabled.
     *
     * @param flats the flats
     */
    @Override
    public void cacheResult(List<Flat> flats) {
        for (Flat flat : flats) {
            if (EntityCacheUtil.getResult(FlatModelImpl.ENTITY_CACHE_ENABLED,
                        FlatImpl.class, flat.getPrimaryKey()) == null) {
                cacheResult(flat);
            } else {
                flat.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all flats.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(FlatImpl.class.getName());
        }

        EntityCacheUtil.clearCache(FlatImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the flat.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Flat flat) {
        EntityCacheUtil.removeResult(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatImpl.class, flat.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(flat);
    }

    @Override
    public void clearCache(List<Flat> flats) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Flat flat : flats) {
            EntityCacheUtil.removeResult(FlatModelImpl.ENTITY_CACHE_ENABLED,
                FlatImpl.class, flat.getPrimaryKey());

            clearUniqueFindersCache(flat);
        }
    }

    protected void cacheUniqueFindersCache(Flat flat) {
        if (flat.isNew()) {
            Object[] args = new Object[] { flat.getOrganizationId() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATION, args,
                flat);
        } else {
            FlatModelImpl flatModelImpl = (FlatModelImpl) flat;

            if ((flatModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_ORGANIZATION.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { flat.getOrganizationId() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATION,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATION,
                    args, flat);
            }
        }
    }

    protected void clearUniqueFindersCache(Flat flat) {
        FlatModelImpl flatModelImpl = (FlatModelImpl) flat;

        Object[] args = new Object[] { flat.getOrganizationId() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGANIZATION, args);

        if ((flatModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_ORGANIZATION.getColumnBitmask()) != 0) {
            args = new Object[] { flatModelImpl.getOriginalOrganizationId() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATION, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGANIZATION, args);
        }
    }

    /**
     * Creates a new flat with the primary key. Does not add the flat to the database.
     *
     * @param id the primary key for the new flat
     * @return the new flat
     */
    @Override
    public Flat create(long id) {
        Flat flat = new FlatImpl();

        flat.setNew(true);
        flat.setPrimaryKey(id);

        return flat;
    }

    /**
     * Removes the flat with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the flat
     * @return the flat that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a flat with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat remove(long id) throws NoSuchFlatException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the flat with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the flat
     * @return the flat that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a flat with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat remove(Serializable primaryKey)
        throws NoSuchFlatException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Flat flat = (Flat) session.get(FlatImpl.class, primaryKey);

            if (flat == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchFlatException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(flat);
        } catch (NoSuchFlatException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Flat removeImpl(Flat flat) throws SystemException {
        flat = toUnwrappedModel(flat);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(flat)) {
                flat = (Flat) session.get(FlatImpl.class,
                        flat.getPrimaryKeyObj());
            }

            if (flat != null) {
                session.delete(flat);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (flat != null) {
            clearCache(flat);
        }

        return flat;
    }

    @Override
    public Flat updateImpl(br.com.atilo.jcondo.manager.model.Flat flat)
        throws SystemException {
        flat = toUnwrappedModel(flat);

        boolean isNew = flat.isNew();

        FlatModelImpl flatModelImpl = (FlatModelImpl) flat;

        Session session = null;

        try {
            session = openSession();

            if (flat.isNew()) {
                session.save(flat);

                flat.setNew(false);
            } else {
                session.merge(flat);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !FlatModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((flatModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { flatModelImpl.getOriginalPersonId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);

                args = new Object[] { flatModelImpl.getPersonId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);
            }
        }

        EntityCacheUtil.putResult(FlatModelImpl.ENTITY_CACHE_ENABLED,
            FlatImpl.class, flat.getPrimaryKey(), flat);

        clearUniqueFindersCache(flat);
        cacheUniqueFindersCache(flat);

        return flat;
    }

    protected Flat toUnwrappedModel(Flat flat) {
        if (flat instanceof FlatImpl) {
            return flat;
        }

        FlatImpl flatImpl = new FlatImpl();

        flatImpl.setNew(flat.isNew());
        flatImpl.setPrimaryKey(flat.getPrimaryKey());

        flatImpl.setId(flat.getId());
        flatImpl.setOrganizationId(flat.getOrganizationId());
        flatImpl.setFolderId(flat.getFolderId());
        flatImpl.setPersonId(flat.getPersonId());
        flatImpl.setBlock(flat.getBlock());
        flatImpl.setNumber(flat.getNumber());
        flatImpl.setPets(flat.isPets());
        flatImpl.setDisables(flat.isDisables());
        flatImpl.setBrigade(flat.isBrigade());
        flatImpl.setDefaulting(flat.isDefaulting());
        flatImpl.setOprDate(flat.getOprDate());
        flatImpl.setOprUser(flat.getOprUser());

        return flatImpl;
    }

    /**
     * Returns the flat with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the flat
     * @return the flat
     * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a flat with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat findByPrimaryKey(Serializable primaryKey)
        throws NoSuchFlatException, SystemException {
        Flat flat = fetchByPrimaryKey(primaryKey);

        if (flat == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchFlatException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return flat;
    }

    /**
     * Returns the flat with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchFlatException} if it could not be found.
     *
     * @param id the primary key of the flat
     * @return the flat
     * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a flat with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat findByPrimaryKey(long id)
        throws NoSuchFlatException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the flat with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the flat
     * @return the flat, or <code>null</code> if a flat with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Flat flat = (Flat) EntityCacheUtil.getResult(FlatModelImpl.ENTITY_CACHE_ENABLED,
                FlatImpl.class, primaryKey);

        if (flat == _nullFlat) {
            return null;
        }

        if (flat == null) {
            Session session = null;

            try {
                session = openSession();

                flat = (Flat) session.get(FlatImpl.class, primaryKey);

                if (flat != null) {
                    cacheResult(flat);
                } else {
                    EntityCacheUtil.putResult(FlatModelImpl.ENTITY_CACHE_ENABLED,
                        FlatImpl.class, primaryKey, _nullFlat);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(FlatModelImpl.ENTITY_CACHE_ENABLED,
                    FlatImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return flat;
    }

    /**
     * Returns the flat with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the flat
     * @return the flat, or <code>null</code> if a flat with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Flat fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the flats.
     *
     * @return the flats
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Flat> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the flats.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of flats
     * @param end the upper bound of the range of flats (not inclusive)
     * @return the range of flats
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Flat> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the flats.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of flats
     * @param end the upper bound of the range of flats (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of flats
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Flat> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Flat> list = (List<Flat>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_FLAT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_FLAT;

                if (pagination) {
                    sql = sql.concat(FlatModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Flat>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Flat>(list);
                } else {
                    list = (List<Flat>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the flats from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Flat flat : findAll()) {
            remove(flat);
        }
    }

    /**
     * Returns the number of flats.
     *
     * @return the number of flats
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_FLAT);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the flat persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Flat")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Flat>> listenersList = new ArrayList<ModelListener<Flat>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Flat>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(FlatImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
