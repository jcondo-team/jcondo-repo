/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.service.permission.OrganizationPermissionUtil;

import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.datatype.EnterpriseStatus;
import br.com.atilo.jcondo.manager.service.base.EnterpriseServiceBaseImpl;

/**
 * The implementation of the enterprise remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.EnterpriseService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.EnterpriseServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.EnterpriseServiceUtil
 */
public class EnterpriseServiceImpl extends EnterpriseServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.EnterpriseServiceUtil} to access the enterprise remote service.
	 */

	public Enterprise createEnterprise() {
		return enterpriseLocalService.createEnterprise();
	}

	public Enterprise addEnterprise(String identity, String name, DomainType type, EnterpriseStatus status, String description) throws PortalException, SystemException {
		return enterpriseLocalService.addEnterprise(identity, name, type, status, description);
	}

	public Enterprise updateEnterprise(long enterpriseId, String identity, String name, DomainType type, EnterpriseStatus status, String description) throws PortalException, SystemException {
		return enterpriseLocalService.updateEnterprise(enterpriseId, identity, name, type, status, description);
	}

	public Enterprise getEnterprise(long id) throws PortalException, SystemException {
		return enterpriseLocalService.getEnterprise(id);
	}

	public List<Enterprise> getEnterprisesByType(DomainType type) throws PortalException, SystemException {
		List<Enterprise> enterprises = enterpriseLocalService.getEnterprisesByType(type);

		for (int i = enterprises.size() - 1; i >=0; i--) {
			if (!OrganizationPermissionUtil.contains(getPermissionChecker(), 
													 enterprises.get(i).getOrganizationId(), ActionKeys.VIEW)) {
				enterprises.remove(i);
			}
		}
		
		return enterprises;
	}

	public Enterprise getEnterpriseByIdentity(String identity) throws PortalException, SystemException {
		Enterprise enterprise = enterpriseLocalService.getEnterpriseByIdentity(identity);
		OrganizationPermissionUtil.check(getPermissionChecker(), enterprise.getOrganizationId(), ActionKeys.VIEW);
		return enterprise;
	}

}