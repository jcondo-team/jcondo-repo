package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchParkingException;
import br.com.atilo.jcondo.manager.model.Parking;
import br.com.atilo.jcondo.manager.model.impl.ParkingImpl;
import br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.ParkingPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the parking service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ParkingPersistence
 * @see ParkingUtil
 * @generated
 */
public class ParkingPersistenceImpl extends BasePersistenceImpl<Parking>
    implements ParkingPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link ParkingUtil} to access the parking persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = ParkingImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingModelImpl.FINDER_CACHE_ENABLED, ParkingImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingModelImpl.FINDER_CACHE_ENABLED, ParkingImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_OWNERDOMAIN =
        new FinderPath(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingModelImpl.FINDER_CACHE_ENABLED, ParkingImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByOwnerDomain",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERDOMAIN =
        new FinderPath(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingModelImpl.FINDER_CACHE_ENABLED, ParkingImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByOwnerDomain",
            new String[] { Long.class.getName() },
            ParkingModelImpl.OWNERDOMAINID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_OWNERDOMAIN = new FinderPath(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOwnerDomain",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_OWNERDOMAIN_OWNERDOMAINID_2 = "parking.ownerDomainId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_RENTERDOMAIN =
        new FinderPath(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingModelImpl.FINDER_CACHE_ENABLED, ParkingImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByRenterDomain",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RENTERDOMAIN =
        new FinderPath(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingModelImpl.FINDER_CACHE_ENABLED, ParkingImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByRenterDomain",
            new String[] { Long.class.getName() },
            ParkingModelImpl.RENTERDOMAINID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_RENTERDOMAIN = new FinderPath(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByRenterDomain",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_RENTERDOMAIN_RENTERDOMAINID_2 = "parking.renterDomainId = ?";
    private static final String _SQL_SELECT_PARKING = "SELECT parking FROM Parking parking";
    private static final String _SQL_SELECT_PARKING_WHERE = "SELECT parking FROM Parking parking WHERE ";
    private static final String _SQL_COUNT_PARKING = "SELECT COUNT(parking) FROM Parking parking";
    private static final String _SQL_COUNT_PARKING_WHERE = "SELECT COUNT(parking) FROM Parking parking WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "parking.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Parking exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Parking exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(ParkingPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Parking _nullParking = new ParkingImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Parking> toCacheModel() {
                return _nullParkingCacheModel;
            }
        };

    private static CacheModel<Parking> _nullParkingCacheModel = new CacheModel<Parking>() {
            @Override
            public Parking toEntityModel() {
                return _nullParking;
            }
        };

    public ParkingPersistenceImpl() {
        setModelClass(Parking.class);
    }

    /**
     * Returns all the parkings where ownerDomainId = &#63;.
     *
     * @param ownerDomainId the owner domain ID
     * @return the matching parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Parking> findByOwnerDomain(long ownerDomainId)
        throws SystemException {
        return findByOwnerDomain(ownerDomainId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the parkings where ownerDomainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param ownerDomainId the owner domain ID
     * @param start the lower bound of the range of parkings
     * @param end the upper bound of the range of parkings (not inclusive)
     * @return the range of matching parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Parking> findByOwnerDomain(long ownerDomainId, int start,
        int end) throws SystemException {
        return findByOwnerDomain(ownerDomainId, start, end, null);
    }

    /**
     * Returns an ordered range of all the parkings where ownerDomainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param ownerDomainId the owner domain ID
     * @param start the lower bound of the range of parkings
     * @param end the upper bound of the range of parkings (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Parking> findByOwnerDomain(long ownerDomainId, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERDOMAIN;
            finderArgs = new Object[] { ownerDomainId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_OWNERDOMAIN;
            finderArgs = new Object[] {
                    ownerDomainId,
                    
                    start, end, orderByComparator
                };
        }

        List<Parking> list = (List<Parking>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Parking parking : list) {
                if ((ownerDomainId != parking.getOwnerDomainId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PARKING_WHERE);

            query.append(_FINDER_COLUMN_OWNERDOMAIN_OWNERDOMAINID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ParkingModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(ownerDomainId);

                if (!pagination) {
                    list = (List<Parking>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Parking>(list);
                } else {
                    list = (List<Parking>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first parking in the ordered set where ownerDomainId = &#63;.
     *
     * @param ownerDomainId the owner domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching parking
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking findByOwnerDomain_First(long ownerDomainId,
        OrderByComparator orderByComparator)
        throws NoSuchParkingException, SystemException {
        Parking parking = fetchByOwnerDomain_First(ownerDomainId,
                orderByComparator);

        if (parking != null) {
            return parking;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("ownerDomainId=");
        msg.append(ownerDomainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchParkingException(msg.toString());
    }

    /**
     * Returns the first parking in the ordered set where ownerDomainId = &#63;.
     *
     * @param ownerDomainId the owner domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching parking, or <code>null</code> if a matching parking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking fetchByOwnerDomain_First(long ownerDomainId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Parking> list = findByOwnerDomain(ownerDomainId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last parking in the ordered set where ownerDomainId = &#63;.
     *
     * @param ownerDomainId the owner domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching parking
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking findByOwnerDomain_Last(long ownerDomainId,
        OrderByComparator orderByComparator)
        throws NoSuchParkingException, SystemException {
        Parking parking = fetchByOwnerDomain_Last(ownerDomainId,
                orderByComparator);

        if (parking != null) {
            return parking;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("ownerDomainId=");
        msg.append(ownerDomainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchParkingException(msg.toString());
    }

    /**
     * Returns the last parking in the ordered set where ownerDomainId = &#63;.
     *
     * @param ownerDomainId the owner domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching parking, or <code>null</code> if a matching parking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking fetchByOwnerDomain_Last(long ownerDomainId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByOwnerDomain(ownerDomainId);

        if (count == 0) {
            return null;
        }

        List<Parking> list = findByOwnerDomain(ownerDomainId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the parkings before and after the current parking in the ordered set where ownerDomainId = &#63;.
     *
     * @param id the primary key of the current parking
     * @param ownerDomainId the owner domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next parking
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking[] findByOwnerDomain_PrevAndNext(long id, long ownerDomainId,
        OrderByComparator orderByComparator)
        throws NoSuchParkingException, SystemException {
        Parking parking = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Parking[] array = new ParkingImpl[3];

            array[0] = getByOwnerDomain_PrevAndNext(session, parking,
                    ownerDomainId, orderByComparator, true);

            array[1] = parking;

            array[2] = getByOwnerDomain_PrevAndNext(session, parking,
                    ownerDomainId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Parking getByOwnerDomain_PrevAndNext(Session session,
        Parking parking, long ownerDomainId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PARKING_WHERE);

        query.append(_FINDER_COLUMN_OWNERDOMAIN_OWNERDOMAINID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(ParkingModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(ownerDomainId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(parking);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Parking> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the parkings where ownerDomainId = &#63; from the database.
     *
     * @param ownerDomainId the owner domain ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByOwnerDomain(long ownerDomainId)
        throws SystemException {
        for (Parking parking : findByOwnerDomain(ownerDomainId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(parking);
        }
    }

    /**
     * Returns the number of parkings where ownerDomainId = &#63;.
     *
     * @param ownerDomainId the owner domain ID
     * @return the number of matching parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByOwnerDomain(long ownerDomainId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_OWNERDOMAIN;

        Object[] finderArgs = new Object[] { ownerDomainId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PARKING_WHERE);

            query.append(_FINDER_COLUMN_OWNERDOMAIN_OWNERDOMAINID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(ownerDomainId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the parkings where renterDomainId = &#63;.
     *
     * @param renterDomainId the renter domain ID
     * @return the matching parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Parking> findByRenterDomain(long renterDomainId)
        throws SystemException {
        return findByRenterDomain(renterDomainId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the parkings where renterDomainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param renterDomainId the renter domain ID
     * @param start the lower bound of the range of parkings
     * @param end the upper bound of the range of parkings (not inclusive)
     * @return the range of matching parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Parking> findByRenterDomain(long renterDomainId, int start,
        int end) throws SystemException {
        return findByRenterDomain(renterDomainId, start, end, null);
    }

    /**
     * Returns an ordered range of all the parkings where renterDomainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param renterDomainId the renter domain ID
     * @param start the lower bound of the range of parkings
     * @param end the upper bound of the range of parkings (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Parking> findByRenterDomain(long renterDomainId, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RENTERDOMAIN;
            finderArgs = new Object[] { renterDomainId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_RENTERDOMAIN;
            finderArgs = new Object[] {
                    renterDomainId,
                    
                    start, end, orderByComparator
                };
        }

        List<Parking> list = (List<Parking>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Parking parking : list) {
                if ((renterDomainId != parking.getRenterDomainId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PARKING_WHERE);

            query.append(_FINDER_COLUMN_RENTERDOMAIN_RENTERDOMAINID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(ParkingModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(renterDomainId);

                if (!pagination) {
                    list = (List<Parking>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Parking>(list);
                } else {
                    list = (List<Parking>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first parking in the ordered set where renterDomainId = &#63;.
     *
     * @param renterDomainId the renter domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching parking
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking findByRenterDomain_First(long renterDomainId,
        OrderByComparator orderByComparator)
        throws NoSuchParkingException, SystemException {
        Parking parking = fetchByRenterDomain_First(renterDomainId,
                orderByComparator);

        if (parking != null) {
            return parking;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("renterDomainId=");
        msg.append(renterDomainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchParkingException(msg.toString());
    }

    /**
     * Returns the first parking in the ordered set where renterDomainId = &#63;.
     *
     * @param renterDomainId the renter domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching parking, or <code>null</code> if a matching parking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking fetchByRenterDomain_First(long renterDomainId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Parking> list = findByRenterDomain(renterDomainId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last parking in the ordered set where renterDomainId = &#63;.
     *
     * @param renterDomainId the renter domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching parking
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking findByRenterDomain_Last(long renterDomainId,
        OrderByComparator orderByComparator)
        throws NoSuchParkingException, SystemException {
        Parking parking = fetchByRenterDomain_Last(renterDomainId,
                orderByComparator);

        if (parking != null) {
            return parking;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("renterDomainId=");
        msg.append(renterDomainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchParkingException(msg.toString());
    }

    /**
     * Returns the last parking in the ordered set where renterDomainId = &#63;.
     *
     * @param renterDomainId the renter domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching parking, or <code>null</code> if a matching parking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking fetchByRenterDomain_Last(long renterDomainId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByRenterDomain(renterDomainId);

        if (count == 0) {
            return null;
        }

        List<Parking> list = findByRenterDomain(renterDomainId, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the parkings before and after the current parking in the ordered set where renterDomainId = &#63;.
     *
     * @param id the primary key of the current parking
     * @param renterDomainId the renter domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next parking
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking[] findByRenterDomain_PrevAndNext(long id,
        long renterDomainId, OrderByComparator orderByComparator)
        throws NoSuchParkingException, SystemException {
        Parking parking = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Parking[] array = new ParkingImpl[3];

            array[0] = getByRenterDomain_PrevAndNext(session, parking,
                    renterDomainId, orderByComparator, true);

            array[1] = parking;

            array[2] = getByRenterDomain_PrevAndNext(session, parking,
                    renterDomainId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Parking getByRenterDomain_PrevAndNext(Session session,
        Parking parking, long renterDomainId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PARKING_WHERE);

        query.append(_FINDER_COLUMN_RENTERDOMAIN_RENTERDOMAINID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(ParkingModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(renterDomainId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(parking);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Parking> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the parkings where renterDomainId = &#63; from the database.
     *
     * @param renterDomainId the renter domain ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByRenterDomain(long renterDomainId)
        throws SystemException {
        for (Parking parking : findByRenterDomain(renterDomainId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(parking);
        }
    }

    /**
     * Returns the number of parkings where renterDomainId = &#63;.
     *
     * @param renterDomainId the renter domain ID
     * @return the number of matching parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByRenterDomain(long renterDomainId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_RENTERDOMAIN;

        Object[] finderArgs = new Object[] { renterDomainId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PARKING_WHERE);

            query.append(_FINDER_COLUMN_RENTERDOMAIN_RENTERDOMAINID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(renterDomainId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the parking in the entity cache if it is enabled.
     *
     * @param parking the parking
     */
    @Override
    public void cacheResult(Parking parking) {
        EntityCacheUtil.putResult(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingImpl.class, parking.getPrimaryKey(), parking);

        parking.resetOriginalValues();
    }

    /**
     * Caches the parkings in the entity cache if it is enabled.
     *
     * @param parkings the parkings
     */
    @Override
    public void cacheResult(List<Parking> parkings) {
        for (Parking parking : parkings) {
            if (EntityCacheUtil.getResult(
                        ParkingModelImpl.ENTITY_CACHE_ENABLED,
                        ParkingImpl.class, parking.getPrimaryKey()) == null) {
                cacheResult(parking);
            } else {
                parking.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all parkings.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(ParkingImpl.class.getName());
        }

        EntityCacheUtil.clearCache(ParkingImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the parking.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Parking parking) {
        EntityCacheUtil.removeResult(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingImpl.class, parking.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Parking> parkings) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Parking parking : parkings) {
            EntityCacheUtil.removeResult(ParkingModelImpl.ENTITY_CACHE_ENABLED,
                ParkingImpl.class, parking.getPrimaryKey());
        }
    }

    /**
     * Creates a new parking with the primary key. Does not add the parking to the database.
     *
     * @param id the primary key for the new parking
     * @return the new parking
     */
    @Override
    public Parking create(long id) {
        Parking parking = new ParkingImpl();

        parking.setNew(true);
        parking.setPrimaryKey(id);

        return parking;
    }

    /**
     * Removes the parking with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the parking
     * @return the parking that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking remove(long id)
        throws NoSuchParkingException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the parking with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the parking
     * @return the parking that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking remove(Serializable primaryKey)
        throws NoSuchParkingException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Parking parking = (Parking) session.get(ParkingImpl.class,
                    primaryKey);

            if (parking == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchParkingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(parking);
        } catch (NoSuchParkingException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Parking removeImpl(Parking parking) throws SystemException {
        parking = toUnwrappedModel(parking);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(parking)) {
                parking = (Parking) session.get(ParkingImpl.class,
                        parking.getPrimaryKeyObj());
            }

            if (parking != null) {
                session.delete(parking);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (parking != null) {
            clearCache(parking);
        }

        return parking;
    }

    @Override
    public Parking updateImpl(br.com.atilo.jcondo.manager.model.Parking parking)
        throws SystemException {
        parking = toUnwrappedModel(parking);

        boolean isNew = parking.isNew();

        ParkingModelImpl parkingModelImpl = (ParkingModelImpl) parking;

        Session session = null;

        try {
            session = openSession();

            if (parking.isNew()) {
                session.save(parking);

                parking.setNew(false);
            } else {
                session.merge(parking);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !ParkingModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((parkingModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERDOMAIN.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        parkingModelImpl.getOriginalOwnerDomainId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_OWNERDOMAIN,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERDOMAIN,
                    args);

                args = new Object[] { parkingModelImpl.getOwnerDomainId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_OWNERDOMAIN,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_OWNERDOMAIN,
                    args);
            }

            if ((parkingModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RENTERDOMAIN.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        parkingModelImpl.getOriginalRenterDomainId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RENTERDOMAIN,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RENTERDOMAIN,
                    args);

                args = new Object[] { parkingModelImpl.getRenterDomainId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RENTERDOMAIN,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RENTERDOMAIN,
                    args);
            }
        }

        EntityCacheUtil.putResult(ParkingModelImpl.ENTITY_CACHE_ENABLED,
            ParkingImpl.class, parking.getPrimaryKey(), parking);

        return parking;
    }

    protected Parking toUnwrappedModel(Parking parking) {
        if (parking instanceof ParkingImpl) {
            return parking;
        }

        ParkingImpl parkingImpl = new ParkingImpl();

        parkingImpl.setNew(parking.isNew());
        parkingImpl.setPrimaryKey(parking.getPrimaryKey());

        parkingImpl.setId(parking.getId());
        parkingImpl.setCode(parking.getCode());
        parkingImpl.setTypeId(parking.getTypeId());
        parkingImpl.setOwnerDomainId(parking.getOwnerDomainId());
        parkingImpl.setRenterDomainId(parking.getRenterDomainId());
        parkingImpl.setOprDate(parking.getOprDate());
        parkingImpl.setOprUser(parking.getOprUser());

        return parkingImpl;
    }

    /**
     * Returns the parking with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the parking
     * @return the parking
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking findByPrimaryKey(Serializable primaryKey)
        throws NoSuchParkingException, SystemException {
        Parking parking = fetchByPrimaryKey(primaryKey);

        if (parking == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchParkingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return parking;
    }

    /**
     * Returns the parking with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchParkingException} if it could not be found.
     *
     * @param id the primary key of the parking
     * @return the parking
     * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking findByPrimaryKey(long id)
        throws NoSuchParkingException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the parking with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the parking
     * @return the parking, or <code>null</code> if a parking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Parking parking = (Parking) EntityCacheUtil.getResult(ParkingModelImpl.ENTITY_CACHE_ENABLED,
                ParkingImpl.class, primaryKey);

        if (parking == _nullParking) {
            return null;
        }

        if (parking == null) {
            Session session = null;

            try {
                session = openSession();

                parking = (Parking) session.get(ParkingImpl.class, primaryKey);

                if (parking != null) {
                    cacheResult(parking);
                } else {
                    EntityCacheUtil.putResult(ParkingModelImpl.ENTITY_CACHE_ENABLED,
                        ParkingImpl.class, primaryKey, _nullParking);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(ParkingModelImpl.ENTITY_CACHE_ENABLED,
                    ParkingImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return parking;
    }

    /**
     * Returns the parking with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the parking
     * @return the parking, or <code>null</code> if a parking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Parking fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the parkings.
     *
     * @return the parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Parking> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the parkings.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of parkings
     * @param end the upper bound of the range of parkings (not inclusive)
     * @return the range of parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Parking> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the parkings.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of parkings
     * @param end the upper bound of the range of parkings (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Parking> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Parking> list = (List<Parking>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PARKING);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PARKING;

                if (pagination) {
                    sql = sql.concat(ParkingModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Parking>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Parking>(list);
                } else {
                    list = (List<Parking>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the parkings from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Parking parking : findAll()) {
            remove(parking);
        }
    }

    /**
     * Returns the number of parkings.
     *
     * @return the number of parkings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PARKING);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the parking persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Parking")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Parking>> listenersList = new ArrayList<ModelListener<Parking>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Parking>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(ParkingImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
