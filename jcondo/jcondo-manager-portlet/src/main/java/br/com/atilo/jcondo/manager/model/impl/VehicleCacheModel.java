package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Vehicle;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Vehicle in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Vehicle
 * @generated
 */
public class VehicleCacheModel implements CacheModel<Vehicle>, Externalizable {
    public long id;
    public long domainId;
    public long imageId;
    public int typeId;
    public String license;
    public String brand;
    public String color;
    public String remark;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(id);
        sb.append(", domainId=");
        sb.append(domainId);
        sb.append(", imageId=");
        sb.append(imageId);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", license=");
        sb.append(license);
        sb.append(", brand=");
        sb.append(brand);
        sb.append(", color=");
        sb.append(color);
        sb.append(", remark=");
        sb.append(remark);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Vehicle toEntityModel() {
        VehicleImpl vehicleImpl = new VehicleImpl();

        vehicleImpl.setId(id);
        vehicleImpl.setDomainId(domainId);
        vehicleImpl.setImageId(imageId);
        vehicleImpl.setTypeId(typeId);

        if (license == null) {
            vehicleImpl.setLicense(StringPool.BLANK);
        } else {
            vehicleImpl.setLicense(license);
        }

        if (brand == null) {
            vehicleImpl.setBrand(StringPool.BLANK);
        } else {
            vehicleImpl.setBrand(brand);
        }

        if (color == null) {
            vehicleImpl.setColor(StringPool.BLANK);
        } else {
            vehicleImpl.setColor(color);
        }

        if (remark == null) {
            vehicleImpl.setRemark(StringPool.BLANK);
        } else {
            vehicleImpl.setRemark(remark);
        }

        if (oprDate == Long.MIN_VALUE) {
            vehicleImpl.setOprDate(null);
        } else {
            vehicleImpl.setOprDate(new Date(oprDate));
        }

        vehicleImpl.setOprUser(oprUser);

        vehicleImpl.resetOriginalValues();

        return vehicleImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        domainId = objectInput.readLong();
        imageId = objectInput.readLong();
        typeId = objectInput.readInt();
        license = objectInput.readUTF();
        brand = objectInput.readUTF();
        color = objectInput.readUTF();
        remark = objectInput.readUTF();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(domainId);
        objectOutput.writeLong(imageId);
        objectOutput.writeInt(typeId);

        if (license == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(license);
        }

        if (brand == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(brand);
        }

        if (color == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(color);
        }

        if (remark == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(remark);
        }

        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
