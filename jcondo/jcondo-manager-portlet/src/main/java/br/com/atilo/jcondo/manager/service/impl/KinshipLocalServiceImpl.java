/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.model.Kinship;
import br.com.atilo.jcondo.datatype.KinType;
import br.com.atilo.jcondo.manager.service.base.KinshipLocalServiceBaseImpl;

/**
 * The implementation of the kinship local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.KinshipLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.KinshipLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.KinshipLocalServiceUtil
 */
public class KinshipLocalServiceImpl extends KinshipLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.KinshipLocalServiceUtil} to access the kinship local service.
	 */

	public Kinship createKinship(long personId, long relativeId, KinType type) {
		Kinship kinship = kinshipLocalService.createKinship(0);
		kinship.setPersonId(personId);
		kinship.setRelativeId(relativeId);

		if (type != null) {
			kinship.setTypeId(type.ordinal());
		} else {
			kinship.setTypeId(-1);
		}

		return kinship;
	}

	public Kinship addKinship(long personId, long relativeId, KinType type) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Kinship kinship = kinshipLocalService.createKinship(counterLocalService.increment());
		kinship.setPersonId(personId);
		kinship.setRelativeId(relativeId);
		kinship.setTypeId(type.ordinal());
		kinship.setOprDate(new Date());
		kinship.setOprUser(sc.getUserId());

		return addKinship(kinship);
	}

	public Kinship updateKinship(long kinshipId, KinType type) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Kinship kinship = getKinship(kinshipId);

		if (type != null) {
			kinship.setTypeId(type.ordinal());
		} else {
			kinship.setTypeId(-1);
		}

		kinship.setOprDate(new Date());
		kinship.setOprUser(sc.getUserId());
		kinship = updateKinship(kinship);

		if (type == null) {
			deleteKinship(kinship);
		}

		return kinship;
	}	

	public List<Kinship> getKinships(long personId) throws PortalException, SystemException {
		List<Kinship> kinships = kinshipPersistence.findByPerson(personId);
		return kinships == Collections.EMPTY_LIST ? new ArrayList<Kinship>() : kinships;
	}

	@SuppressWarnings("unchecked")
	public Kinship getPersonKinship(long personId, long relativeId) throws PortalException, SystemException {
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.eq("personId", personId),
										   RestrictionsFactoryUtil.eq("relativeId", relativeId)));
		List<Kinship> kinships = kinshipPersistence.findWithDynamicQuery(dq);
		return kinships.isEmpty() ? null : kinships.get(0);
	}

}