package br.com.atilo.jcondo.manager.service.http;

import br.com.atilo.jcondo.manager.service.ParkingServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.manager.service.ParkingServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.manager.model.ParkingSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.manager.model.Parking}, that is translated to a
 * {@link br.com.atilo.jcondo.manager.model.ParkingSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ParkingServiceHttp
 * @see br.com.atilo.jcondo.manager.model.ParkingSoap
 * @see br.com.atilo.jcondo.manager.service.ParkingServiceUtil
 * @generated
 */
public class ParkingServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(ParkingServiceSoap.class);

    public static br.com.atilo.jcondo.manager.model.ParkingSoap addParking(
        java.lang.String code, br.com.atilo.jcondo.datatype.ParkingType type,
        long ownerDomainId) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Parking returnValue = ParkingServiceUtil.addParking(code,
                    type, ownerDomainId);

            return br.com.atilo.jcondo.manager.model.ParkingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.ParkingSoap updateParking(
        long parkingId, java.lang.String code,
        br.com.atilo.jcondo.datatype.ParkingType type, long ownerDomainId)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Parking returnValue = ParkingServiceUtil.updateParking(parkingId,
                    code, type, ownerDomainId);

            return br.com.atilo.jcondo.manager.model.ParkingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.ParkingSoap[] getOwnedParkings(
        long ownerDomainId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Parking> returnValue =
                ParkingServiceUtil.getOwnedParkings(ownerDomainId);

            return br.com.atilo.jcondo.manager.model.ParkingSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.ParkingSoap[] getRentedParkings(
        long renterDomainId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Parking> returnValue =
                ParkingServiceUtil.getRentedParkings(renterDomainId);

            return br.com.atilo.jcondo.manager.model.ParkingSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.ParkingSoap[] getGrantedParkings(
        long ownerDomainId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.manager.model.Parking> returnValue =
                ParkingServiceUtil.getGrantedParkings(ownerDomainId);

            return br.com.atilo.jcondo.manager.model.ParkingSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.ParkingSoap rent(
        long ownerDomainId, long renterDomainId) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Parking returnValue = ParkingServiceUtil.rent(ownerDomainId,
                    renterDomainId);

            return br.com.atilo.jcondo.manager.model.ParkingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.manager.model.ParkingSoap unrent(
        long parkingId) throws RemoteException {
        try {
            br.com.atilo.jcondo.manager.model.Parking returnValue = ParkingServiceUtil.unrent(parkingId);

            return br.com.atilo.jcondo.manager.model.ParkingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
