package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchMailException;
import br.com.atilo.jcondo.manager.model.Mail;
import br.com.atilo.jcondo.manager.model.impl.MailImpl;
import br.com.atilo.jcondo.manager.model.impl.MailModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.MailPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the mail service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MailPersistence
 * @see MailUtil
 * @generated
 */
public class MailPersistenceImpl extends BasePersistenceImpl<Mail>
    implements MailPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link MailUtil} to access the mail persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = MailImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(MailModelImpl.ENTITY_CACHE_ENABLED,
            MailModelImpl.FINDER_CACHE_ENABLED, MailImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(MailModelImpl.ENTITY_CACHE_ENABLED,
            MailModelImpl.FINDER_CACHE_ENABLED, MailImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(MailModelImpl.ENTITY_CACHE_ENABLED,
            MailModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_MAIL = "SELECT mail FROM Mail mail";
    private static final String _SQL_COUNT_MAIL = "SELECT COUNT(mail) FROM Mail mail";
    private static final String _ORDER_BY_ENTITY_ALIAS = "mail.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Mail exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(MailPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Mail _nullMail = new MailImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Mail> toCacheModel() {
                return _nullMailCacheModel;
            }
        };

    private static CacheModel<Mail> _nullMailCacheModel = new CacheModel<Mail>() {
            @Override
            public Mail toEntityModel() {
                return _nullMail;
            }
        };

    public MailPersistenceImpl() {
        setModelClass(Mail.class);
    }

    /**
     * Caches the mail in the entity cache if it is enabled.
     *
     * @param mail the mail
     */
    @Override
    public void cacheResult(Mail mail) {
        EntityCacheUtil.putResult(MailModelImpl.ENTITY_CACHE_ENABLED,
            MailImpl.class, mail.getPrimaryKey(), mail);

        mail.resetOriginalValues();
    }

    /**
     * Caches the mails in the entity cache if it is enabled.
     *
     * @param mails the mails
     */
    @Override
    public void cacheResult(List<Mail> mails) {
        for (Mail mail : mails) {
            if (EntityCacheUtil.getResult(MailModelImpl.ENTITY_CACHE_ENABLED,
                        MailImpl.class, mail.getPrimaryKey()) == null) {
                cacheResult(mail);
            } else {
                mail.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all mails.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(MailImpl.class.getName());
        }

        EntityCacheUtil.clearCache(MailImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the mail.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Mail mail) {
        EntityCacheUtil.removeResult(MailModelImpl.ENTITY_CACHE_ENABLED,
            MailImpl.class, mail.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Mail> mails) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Mail mail : mails) {
            EntityCacheUtil.removeResult(MailModelImpl.ENTITY_CACHE_ENABLED,
                MailImpl.class, mail.getPrimaryKey());
        }
    }

    /**
     * Creates a new mail with the primary key. Does not add the mail to the database.
     *
     * @param id the primary key for the new mail
     * @return the new mail
     */
    @Override
    public Mail create(long id) {
        Mail mail = new MailImpl();

        mail.setNew(true);
        mail.setPrimaryKey(id);

        return mail;
    }

    /**
     * Removes the mail with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the mail
     * @return the mail that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchMailException if a mail with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Mail remove(long id) throws NoSuchMailException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the mail with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the mail
     * @return the mail that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchMailException if a mail with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Mail remove(Serializable primaryKey)
        throws NoSuchMailException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Mail mail = (Mail) session.get(MailImpl.class, primaryKey);

            if (mail == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchMailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(mail);
        } catch (NoSuchMailException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Mail removeImpl(Mail mail) throws SystemException {
        mail = toUnwrappedModel(mail);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(mail)) {
                mail = (Mail) session.get(MailImpl.class,
                        mail.getPrimaryKeyObj());
            }

            if (mail != null) {
                session.delete(mail);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (mail != null) {
            clearCache(mail);
        }

        return mail;
    }

    @Override
    public Mail updateImpl(br.com.atilo.jcondo.manager.model.Mail mail)
        throws SystemException {
        mail = toUnwrappedModel(mail);

        boolean isNew = mail.isNew();

        Session session = null;

        try {
            session = openSession();

            if (mail.isNew()) {
                session.save(mail);

                mail.setNew(false);
            } else {
                session.merge(mail);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(MailModelImpl.ENTITY_CACHE_ENABLED,
            MailImpl.class, mail.getPrimaryKey(), mail);

        return mail;
    }

    protected Mail toUnwrappedModel(Mail mail) {
        if (mail instanceof MailImpl) {
            return mail;
        }

        MailImpl mailImpl = new MailImpl();

        mailImpl.setNew(mail.isNew());
        mailImpl.setPrimaryKey(mail.getPrimaryKey());

        mailImpl.setId(mail.getId());
        mailImpl.setRecipientId(mail.getRecipientId());
        mailImpl.setRecipientName(mail.getRecipientName());
        mailImpl.setTakerId(mail.getTakerId());
        mailImpl.setTakerName(mail.getTakerName());
        mailImpl.setTypeId(mail.getTypeId());
        mailImpl.setCode(mail.getCode());
        mailImpl.setDateIn(mail.getDateIn());
        mailImpl.setDateOut(mail.getDateOut());
        mailImpl.setRemark(mail.getRemark());
        mailImpl.setSignatureId(mail.getSignatureId());
        mailImpl.setOprDate(mail.getOprDate());
        mailImpl.setOprUser(mail.getOprUser());

        return mailImpl;
    }

    /**
     * Returns the mail with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the mail
     * @return the mail
     * @throws br.com.atilo.jcondo.manager.NoSuchMailException if a mail with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Mail findByPrimaryKey(Serializable primaryKey)
        throws NoSuchMailException, SystemException {
        Mail mail = fetchByPrimaryKey(primaryKey);

        if (mail == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchMailException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return mail;
    }

    /**
     * Returns the mail with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchMailException} if it could not be found.
     *
     * @param id the primary key of the mail
     * @return the mail
     * @throws br.com.atilo.jcondo.manager.NoSuchMailException if a mail with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Mail findByPrimaryKey(long id)
        throws NoSuchMailException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the mail with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the mail
     * @return the mail, or <code>null</code> if a mail with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Mail fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Mail mail = (Mail) EntityCacheUtil.getResult(MailModelImpl.ENTITY_CACHE_ENABLED,
                MailImpl.class, primaryKey);

        if (mail == _nullMail) {
            return null;
        }

        if (mail == null) {
            Session session = null;

            try {
                session = openSession();

                mail = (Mail) session.get(MailImpl.class, primaryKey);

                if (mail != null) {
                    cacheResult(mail);
                } else {
                    EntityCacheUtil.putResult(MailModelImpl.ENTITY_CACHE_ENABLED,
                        MailImpl.class, primaryKey, _nullMail);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(MailModelImpl.ENTITY_CACHE_ENABLED,
                    MailImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return mail;
    }

    /**
     * Returns the mail with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the mail
     * @return the mail, or <code>null</code> if a mail with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Mail fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the mails.
     *
     * @return the mails
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Mail> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the mails.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of mails
     * @param end the upper bound of the range of mails (not inclusive)
     * @return the range of mails
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Mail> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the mails.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of mails
     * @param end the upper bound of the range of mails (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of mails
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Mail> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Mail> list = (List<Mail>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_MAIL);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_MAIL;

                if (pagination) {
                    sql = sql.concat(MailModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Mail>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Mail>(list);
                } else {
                    list = (List<Mail>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the mails from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Mail mail : findAll()) {
            remove(mail);
        }
    }

    /**
     * Returns the number of mails.
     *
     * @return the number of mails
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_MAIL);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the mail persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Mail")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Mail>> listenersList = new ArrayList<ModelListener<Mail>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Mail>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(MailImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
