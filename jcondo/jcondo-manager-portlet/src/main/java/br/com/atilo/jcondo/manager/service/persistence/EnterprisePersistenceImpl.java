package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.model.impl.EnterpriseImpl;
import br.com.atilo.jcondo.manager.model.impl.EnterpriseModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.EnterprisePersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the enterprise service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EnterprisePersistence
 * @see EnterpriseUtil
 * @generated
 */
public class EnterprisePersistenceImpl extends BasePersistenceImpl<Enterprise>
    implements EnterprisePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link EnterpriseUtil} to access the enterprise persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = EnterpriseImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseModelImpl.FINDER_CACHE_ENABLED, EnterpriseImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseModelImpl.FINDER_CACHE_ENABLED, EnterpriseImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_IDENTITY = new FinderPath(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseModelImpl.FINDER_CACHE_ENABLED, EnterpriseImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByIdentity",
            new String[] { String.class.getName() },
            EnterpriseModelImpl.IDENTITY_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_IDENTITY = new FinderPath(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByIdentity",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_IDENTITY_IDENTITY_1 = "enterprise.identity IS NULL";
    private static final String _FINDER_COLUMN_IDENTITY_IDENTITY_2 = "enterprise.identity = ?";
    private static final String _FINDER_COLUMN_IDENTITY_IDENTITY_3 = "(enterprise.identity IS NULL OR enterprise.identity = '')";
    public static final FinderPath FINDER_PATH_FETCH_BY_ORGANIZATIONID = new FinderPath(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseModelImpl.FINDER_CACHE_ENABLED, EnterpriseImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByOrganizationId",
            new String[] { Long.class.getName() },
            EnterpriseModelImpl.ORGANIZATIONID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ORGANIZATIONID = new FinderPath(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByOrganizationId",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2 = "enterprise.organizationId = ?";
    private static final String _SQL_SELECT_ENTERPRISE = "SELECT enterprise FROM Enterprise enterprise";
    private static final String _SQL_SELECT_ENTERPRISE_WHERE = "SELECT enterprise FROM Enterprise enterprise WHERE ";
    private static final String _SQL_COUNT_ENTERPRISE = "SELECT COUNT(enterprise) FROM Enterprise enterprise";
    private static final String _SQL_COUNT_ENTERPRISE_WHERE = "SELECT COUNT(enterprise) FROM Enterprise enterprise WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "enterprise.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Enterprise exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Enterprise exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(EnterprisePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Enterprise _nullEnterprise = new EnterpriseImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Enterprise> toCacheModel() {
                return _nullEnterpriseCacheModel;
            }
        };

    private static CacheModel<Enterprise> _nullEnterpriseCacheModel = new CacheModel<Enterprise>() {
            @Override
            public Enterprise toEntityModel() {
                return _nullEnterprise;
            }
        };

    public EnterprisePersistenceImpl() {
        setModelClass(Enterprise.class);
    }

    /**
     * Returns the enterprise where identity = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchEnterpriseException} if it could not be found.
     *
     * @param identity the identity
     * @return the matching enterprise
     * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a matching enterprise could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise findByIdentity(String identity)
        throws NoSuchEnterpriseException, SystemException {
        Enterprise enterprise = fetchByIdentity(identity);

        if (enterprise == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("identity=");
            msg.append(identity);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchEnterpriseException(msg.toString());
        }

        return enterprise;
    }

    /**
     * Returns the enterprise where identity = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param identity the identity
     * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise fetchByIdentity(String identity)
        throws SystemException {
        return fetchByIdentity(identity, true);
    }

    /**
     * Returns the enterprise where identity = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param identity the identity
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise fetchByIdentity(String identity, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { identity };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_IDENTITY,
                    finderArgs, this);
        }

        if (result instanceof Enterprise) {
            Enterprise enterprise = (Enterprise) result;

            if (!Validator.equals(identity, enterprise.getIdentity())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_ENTERPRISE_WHERE);

            boolean bindIdentity = false;

            if (identity == null) {
                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_1);
            } else if (identity.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_3);
            } else {
                bindIdentity = true;

                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindIdentity) {
                    qPos.add(identity);
                }

                List<Enterprise> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "EnterprisePersistenceImpl.fetchByIdentity(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Enterprise enterprise = list.get(0);

                    result = enterprise;

                    cacheResult(enterprise);

                    if ((enterprise.getIdentity() == null) ||
                            !enterprise.getIdentity().equals(identity)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY,
                            finderArgs, enterprise);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDENTITY,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Enterprise) result;
        }
    }

    /**
     * Removes the enterprise where identity = &#63; from the database.
     *
     * @param identity the identity
     * @return the enterprise that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise removeByIdentity(String identity)
        throws NoSuchEnterpriseException, SystemException {
        Enterprise enterprise = findByIdentity(identity);

        return remove(enterprise);
    }

    /**
     * Returns the number of enterprises where identity = &#63;.
     *
     * @param identity the identity
     * @return the number of matching enterprises
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByIdentity(String identity) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_IDENTITY;

        Object[] finderArgs = new Object[] { identity };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ENTERPRISE_WHERE);

            boolean bindIdentity = false;

            if (identity == null) {
                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_1);
            } else if (identity.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_3);
            } else {
                bindIdentity = true;

                query.append(_FINDER_COLUMN_IDENTITY_IDENTITY_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindIdentity) {
                    qPos.add(identity);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the enterprise where organizationId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchEnterpriseException} if it could not be found.
     *
     * @param organizationId the organization ID
     * @return the matching enterprise
     * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a matching enterprise could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise findByOrganizationId(long organizationId)
        throws NoSuchEnterpriseException, SystemException {
        Enterprise enterprise = fetchByOrganizationId(organizationId);

        if (enterprise == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("organizationId=");
            msg.append(organizationId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchEnterpriseException(msg.toString());
        }

        return enterprise;
    }

    /**
     * Returns the enterprise where organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param organizationId the organization ID
     * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise fetchByOrganizationId(long organizationId)
        throws SystemException {
        return fetchByOrganizationId(organizationId, true);
    }

    /**
     * Returns the enterprise where organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param organizationId the organization ID
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise fetchByOrganizationId(long organizationId,
        boolean retrieveFromCache) throws SystemException {
        Object[] finderArgs = new Object[] { organizationId };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_ORGANIZATIONID,
                    finderArgs, this);
        }

        if (result instanceof Enterprise) {
            Enterprise enterprise = (Enterprise) result;

            if ((organizationId != enterprise.getOrganizationId())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_ENTERPRISE_WHERE);

            query.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(organizationId);

                List<Enterprise> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONID,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "EnterprisePersistenceImpl.fetchByOrganizationId(long, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Enterprise enterprise = list.get(0);

                    result = enterprise;

                    cacheResult(enterprise);

                    if ((enterprise.getOrganizationId() != organizationId)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONID,
                            finderArgs, enterprise);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGANIZATIONID,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Enterprise) result;
        }
    }

    /**
     * Removes the enterprise where organizationId = &#63; from the database.
     *
     * @param organizationId the organization ID
     * @return the enterprise that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise removeByOrganizationId(long organizationId)
        throws NoSuchEnterpriseException, SystemException {
        Enterprise enterprise = findByOrganizationId(organizationId);

        return remove(enterprise);
    }

    /**
     * Returns the number of enterprises where organizationId = &#63;.
     *
     * @param organizationId the organization ID
     * @return the number of matching enterprises
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByOrganizationId(long organizationId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ORGANIZATIONID;

        Object[] finderArgs = new Object[] { organizationId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ENTERPRISE_WHERE);

            query.append(_FINDER_COLUMN_ORGANIZATIONID_ORGANIZATIONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(organizationId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the enterprise in the entity cache if it is enabled.
     *
     * @param enterprise the enterprise
     */
    @Override
    public void cacheResult(Enterprise enterprise) {
        EntityCacheUtil.putResult(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseImpl.class, enterprise.getPrimaryKey(), enterprise);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY,
            new Object[] { enterprise.getIdentity() }, enterprise);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONID,
            new Object[] { enterprise.getOrganizationId() }, enterprise);

        enterprise.resetOriginalValues();
    }

    /**
     * Caches the enterprises in the entity cache if it is enabled.
     *
     * @param enterprises the enterprises
     */
    @Override
    public void cacheResult(List<Enterprise> enterprises) {
        for (Enterprise enterprise : enterprises) {
            if (EntityCacheUtil.getResult(
                        EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
                        EnterpriseImpl.class, enterprise.getPrimaryKey()) == null) {
                cacheResult(enterprise);
            } else {
                enterprise.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all enterprises.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(EnterpriseImpl.class.getName());
        }

        EntityCacheUtil.clearCache(EnterpriseImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the enterprise.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Enterprise enterprise) {
        EntityCacheUtil.removeResult(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseImpl.class, enterprise.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(enterprise);
    }

    @Override
    public void clearCache(List<Enterprise> enterprises) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Enterprise enterprise : enterprises) {
            EntityCacheUtil.removeResult(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
                EnterpriseImpl.class, enterprise.getPrimaryKey());

            clearUniqueFindersCache(enterprise);
        }
    }

    protected void cacheUniqueFindersCache(Enterprise enterprise) {
        if (enterprise.isNew()) {
            Object[] args = new Object[] { enterprise.getIdentity() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDENTITY, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY, args,
                enterprise);

            args = new Object[] { enterprise.getOrganizationId() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID,
                args, Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONID,
                args, enterprise);
        } else {
            EnterpriseModelImpl enterpriseModelImpl = (EnterpriseModelImpl) enterprise;

            if ((enterpriseModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_IDENTITY.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { enterprise.getIdentity() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_IDENTITY, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_IDENTITY, args,
                    enterprise);
            }

            if ((enterpriseModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_ORGANIZATIONID.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { enterprise.getOrganizationId() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID,
                    args, Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_ORGANIZATIONID,
                    args, enterprise);
            }
        }
    }

    protected void clearUniqueFindersCache(Enterprise enterprise) {
        EnterpriseModelImpl enterpriseModelImpl = (EnterpriseModelImpl) enterprise;

        Object[] args = new Object[] { enterprise.getIdentity() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDENTITY, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDENTITY, args);

        if ((enterpriseModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_IDENTITY.getColumnBitmask()) != 0) {
            args = new Object[] { enterpriseModelImpl.getOriginalIdentity() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_IDENTITY, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_IDENTITY, args);
        }

        args = new Object[] { enterprise.getOrganizationId() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGANIZATIONID, args);

        if ((enterpriseModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_ORGANIZATIONID.getColumnBitmask()) != 0) {
            args = new Object[] { enterpriseModelImpl.getOriginalOrganizationId() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ORGANIZATIONID,
                args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_ORGANIZATIONID,
                args);
        }
    }

    /**
     * Creates a new enterprise with the primary key. Does not add the enterprise to the database.
     *
     * @param id the primary key for the new enterprise
     * @return the new enterprise
     */
    @Override
    public Enterprise create(long id) {
        Enterprise enterprise = new EnterpriseImpl();

        enterprise.setNew(true);
        enterprise.setPrimaryKey(id);

        return enterprise;
    }

    /**
     * Removes the enterprise with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the enterprise
     * @return the enterprise that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a enterprise with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise remove(long id)
        throws NoSuchEnterpriseException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the enterprise with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the enterprise
     * @return the enterprise that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a enterprise with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise remove(Serializable primaryKey)
        throws NoSuchEnterpriseException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Enterprise enterprise = (Enterprise) session.get(EnterpriseImpl.class,
                    primaryKey);

            if (enterprise == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchEnterpriseException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(enterprise);
        } catch (NoSuchEnterpriseException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Enterprise removeImpl(Enterprise enterprise)
        throws SystemException {
        enterprise = toUnwrappedModel(enterprise);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(enterprise)) {
                enterprise = (Enterprise) session.get(EnterpriseImpl.class,
                        enterprise.getPrimaryKeyObj());
            }

            if (enterprise != null) {
                session.delete(enterprise);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (enterprise != null) {
            clearCache(enterprise);
        }

        return enterprise;
    }

    @Override
    public Enterprise updateImpl(
        br.com.atilo.jcondo.manager.model.Enterprise enterprise)
        throws SystemException {
        enterprise = toUnwrappedModel(enterprise);

        boolean isNew = enterprise.isNew();

        Session session = null;

        try {
            session = openSession();

            if (enterprise.isNew()) {
                session.save(enterprise);

                enterprise.setNew(false);
            } else {
                session.merge(enterprise);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !EnterpriseModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
            EnterpriseImpl.class, enterprise.getPrimaryKey(), enterprise);

        clearUniqueFindersCache(enterprise);
        cacheUniqueFindersCache(enterprise);

        return enterprise;
    }

    protected Enterprise toUnwrappedModel(Enterprise enterprise) {
        if (enterprise instanceof EnterpriseImpl) {
            return enterprise;
        }

        EnterpriseImpl enterpriseImpl = new EnterpriseImpl();

        enterpriseImpl.setNew(enterprise.isNew());
        enterpriseImpl.setPrimaryKey(enterprise.getPrimaryKey());

        enterpriseImpl.setId(enterprise.getId());
        enterpriseImpl.setOrganizationId(enterprise.getOrganizationId());
        enterpriseImpl.setStatusId(enterprise.getStatusId());
        enterpriseImpl.setIdentity(enterprise.getIdentity());
        enterpriseImpl.setOprDate(enterprise.getOprDate());
        enterpriseImpl.setOprUser(enterprise.getOprUser());

        return enterpriseImpl;
    }

    /**
     * Returns the enterprise with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the enterprise
     * @return the enterprise
     * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a enterprise with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise findByPrimaryKey(Serializable primaryKey)
        throws NoSuchEnterpriseException, SystemException {
        Enterprise enterprise = fetchByPrimaryKey(primaryKey);

        if (enterprise == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchEnterpriseException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return enterprise;
    }

    /**
     * Returns the enterprise with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchEnterpriseException} if it could not be found.
     *
     * @param id the primary key of the enterprise
     * @return the enterprise
     * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a enterprise with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise findByPrimaryKey(long id)
        throws NoSuchEnterpriseException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the enterprise with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the enterprise
     * @return the enterprise, or <code>null</code> if a enterprise with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Enterprise enterprise = (Enterprise) EntityCacheUtil.getResult(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
                EnterpriseImpl.class, primaryKey);

        if (enterprise == _nullEnterprise) {
            return null;
        }

        if (enterprise == null) {
            Session session = null;

            try {
                session = openSession();

                enterprise = (Enterprise) session.get(EnterpriseImpl.class,
                        primaryKey);

                if (enterprise != null) {
                    cacheResult(enterprise);
                } else {
                    EntityCacheUtil.putResult(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
                        EnterpriseImpl.class, primaryKey, _nullEnterprise);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(EnterpriseModelImpl.ENTITY_CACHE_ENABLED,
                    EnterpriseImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return enterprise;
    }

    /**
     * Returns the enterprise with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the enterprise
     * @return the enterprise, or <code>null</code> if a enterprise with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Enterprise fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the enterprises.
     *
     * @return the enterprises
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Enterprise> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the enterprises.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.EnterpriseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of enterprises
     * @param end the upper bound of the range of enterprises (not inclusive)
     * @return the range of enterprises
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Enterprise> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the enterprises.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.EnterpriseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of enterprises
     * @param end the upper bound of the range of enterprises (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of enterprises
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Enterprise> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Enterprise> list = (List<Enterprise>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ENTERPRISE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ENTERPRISE;

                if (pagination) {
                    sql = sql.concat(EnterpriseModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Enterprise>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Enterprise>(list);
                } else {
                    list = (List<Enterprise>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the enterprises from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Enterprise enterprise : findAll()) {
            remove(enterprise);
        }
    }

    /**
     * Returns the number of enterprises.
     *
     * @return the number of enterprises
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ENTERPRISE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the enterprise persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Enterprise")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Enterprise>> listenersList = new ArrayList<ModelListener<Enterprise>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Enterprise>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(EnterpriseImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
