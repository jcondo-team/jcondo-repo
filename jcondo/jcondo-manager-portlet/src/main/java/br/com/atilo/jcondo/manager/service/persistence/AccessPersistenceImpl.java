package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.NoSuchAccessException;
import br.com.atilo.jcondo.manager.model.Access;
import br.com.atilo.jcondo.manager.model.impl.AccessImpl;
import br.com.atilo.jcondo.manager.model.impl.AccessModelImpl;
import br.com.atilo.jcondo.manager.service.persistence.AccessPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the access service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AccessPersistence
 * @see AccessUtil
 * @generated
 */
public class AccessPersistenceImpl extends BasePersistenceImpl<Access>
    implements AccessPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link AccessUtil} to access the access persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = AccessImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessModelImpl.FINDER_CACHE_ENABLED, AccessImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessModelImpl.FINDER_CACHE_ENABLED, AccessImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_BADGE = new FinderPath(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessModelImpl.FINDER_CACHE_ENABLED, AccessImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByBadge",
            new String[] { String.class.getName() },
            AccessModelImpl.BADGE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_BADGE = new FinderPath(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBadge",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_BADGE_BADGE_1 = "access.badge IS NULL";
    private static final String _FINDER_COLUMN_BADGE_BADGE_2 = "access.badge = ?";
    private static final String _FINDER_COLUMN_BADGE_BADGE_3 = "(access.badge IS NULL OR access.badge = '')";
    public static final FinderPath FINDER_PATH_FETCH_BY_VEHICLE = new FinderPath(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessModelImpl.FINDER_CACHE_ENABLED, AccessImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByVehicle",
            new String[] { Long.class.getName() },
            AccessModelImpl.VEHICLEID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VEHICLE = new FinderPath(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByVehicle",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_VEHICLE_VEHICLEID_2 = "access.vehicleId = ?";
    public static final FinderPath FINDER_PATH_FETCH_BY_VISITOR = new FinderPath(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessModelImpl.FINDER_CACHE_ENABLED, AccessImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByVisitor",
            new String[] { Long.class.getName() },
            AccessModelImpl.VISITORID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_VISITOR = new FinderPath(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByVisitor",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_VISITOR_VISITORID_2 = "access.visitorId = ?";
    private static final String _SQL_SELECT_ACCESS = "SELECT access FROM Access access";
    private static final String _SQL_SELECT_ACCESS_WHERE = "SELECT access FROM Access access WHERE ";
    private static final String _SQL_COUNT_ACCESS = "SELECT COUNT(access) FROM Access access";
    private static final String _SQL_COUNT_ACCESS_WHERE = "SELECT COUNT(access) FROM Access access WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "access.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Access exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Access exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(AccessPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Access _nullAccess = new AccessImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Access> toCacheModel() {
                return _nullAccessCacheModel;
            }
        };

    private static CacheModel<Access> _nullAccessCacheModel = new CacheModel<Access>() {
            @Override
            public Access toEntityModel() {
                return _nullAccess;
            }
        };

    public AccessPersistenceImpl() {
        setModelClass(Access.class);
    }

    /**
     * Returns the access where badge = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessException} if it could not be found.
     *
     * @param badge the badge
     * @return the matching access
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a matching access could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access findByBadge(String badge)
        throws NoSuchAccessException, SystemException {
        Access access = fetchByBadge(badge);

        if (access == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("badge=");
            msg.append(badge);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchAccessException(msg.toString());
        }

        return access;
    }

    /**
     * Returns the access where badge = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param badge the badge
     * @return the matching access, or <code>null</code> if a matching access could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access fetchByBadge(String badge) throws SystemException {
        return fetchByBadge(badge, true);
    }

    /**
     * Returns the access where badge = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param badge the badge
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching access, or <code>null</code> if a matching access could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access fetchByBadge(String badge, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { badge };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_BADGE,
                    finderArgs, this);
        }

        if (result instanceof Access) {
            Access access = (Access) result;

            if (!Validator.equals(badge, access.getBadge())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_ACCESS_WHERE);

            boolean bindBadge = false;

            if (badge == null) {
                query.append(_FINDER_COLUMN_BADGE_BADGE_1);
            } else if (badge.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_BADGE_BADGE_3);
            } else {
                bindBadge = true;

                query.append(_FINDER_COLUMN_BADGE_BADGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindBadge) {
                    qPos.add(badge);
                }

                List<Access> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BADGE,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "AccessPersistenceImpl.fetchByBadge(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Access access = list.get(0);

                    result = access;

                    cacheResult(access);

                    if ((access.getBadge() == null) ||
                            !access.getBadge().equals(badge)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BADGE,
                            finderArgs, access);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BADGE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Access) result;
        }
    }

    /**
     * Removes the access where badge = &#63; from the database.
     *
     * @param badge the badge
     * @return the access that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access removeByBadge(String badge)
        throws NoSuchAccessException, SystemException {
        Access access = findByBadge(badge);

        return remove(access);
    }

    /**
     * Returns the number of accesses where badge = &#63;.
     *
     * @param badge the badge
     * @return the number of matching accesses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByBadge(String badge) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_BADGE;

        Object[] finderArgs = new Object[] { badge };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ACCESS_WHERE);

            boolean bindBadge = false;

            if (badge == null) {
                query.append(_FINDER_COLUMN_BADGE_BADGE_1);
            } else if (badge.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_BADGE_BADGE_3);
            } else {
                bindBadge = true;

                query.append(_FINDER_COLUMN_BADGE_BADGE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindBadge) {
                    qPos.add(badge);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the access where vehicleId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessException} if it could not be found.
     *
     * @param vehicleId the vehicle ID
     * @return the matching access
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a matching access could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access findByVehicle(long vehicleId)
        throws NoSuchAccessException, SystemException {
        Access access = fetchByVehicle(vehicleId);

        if (access == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("vehicleId=");
            msg.append(vehicleId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchAccessException(msg.toString());
        }

        return access;
    }

    /**
     * Returns the access where vehicleId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param vehicleId the vehicle ID
     * @return the matching access, or <code>null</code> if a matching access could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access fetchByVehicle(long vehicleId) throws SystemException {
        return fetchByVehicle(vehicleId, true);
    }

    /**
     * Returns the access where vehicleId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param vehicleId the vehicle ID
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching access, or <code>null</code> if a matching access could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access fetchByVehicle(long vehicleId, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { vehicleId };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_VEHICLE,
                    finderArgs, this);
        }

        if (result instanceof Access) {
            Access access = (Access) result;

            if ((vehicleId != access.getVehicleId())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_ACCESS_WHERE);

            query.append(_FINDER_COLUMN_VEHICLE_VEHICLEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleId);

                List<Access> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VEHICLE,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "AccessPersistenceImpl.fetchByVehicle(long, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Access access = list.get(0);

                    result = access;

                    cacheResult(access);

                    if ((access.getVehicleId() != vehicleId)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VEHICLE,
                            finderArgs, access);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_VEHICLE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Access) result;
        }
    }

    /**
     * Removes the access where vehicleId = &#63; from the database.
     *
     * @param vehicleId the vehicle ID
     * @return the access that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access removeByVehicle(long vehicleId)
        throws NoSuchAccessException, SystemException {
        Access access = findByVehicle(vehicleId);

        return remove(access);
    }

    /**
     * Returns the number of accesses where vehicleId = &#63;.
     *
     * @param vehicleId the vehicle ID
     * @return the number of matching accesses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVehicle(long vehicleId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VEHICLE;

        Object[] finderArgs = new Object[] { vehicleId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ACCESS_WHERE);

            query.append(_FINDER_COLUMN_VEHICLE_VEHICLEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(vehicleId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the access where visitorId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessException} if it could not be found.
     *
     * @param visitorId the visitor ID
     * @return the matching access
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a matching access could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access findByVisitor(long visitorId)
        throws NoSuchAccessException, SystemException {
        Access access = fetchByVisitor(visitorId);

        if (access == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("visitorId=");
            msg.append(visitorId);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchAccessException(msg.toString());
        }

        return access;
    }

    /**
     * Returns the access where visitorId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param visitorId the visitor ID
     * @return the matching access, or <code>null</code> if a matching access could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access fetchByVisitor(long visitorId) throws SystemException {
        return fetchByVisitor(visitorId, true);
    }

    /**
     * Returns the access where visitorId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param visitorId the visitor ID
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching access, or <code>null</code> if a matching access could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access fetchByVisitor(long visitorId, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { visitorId };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_VISITOR,
                    finderArgs, this);
        }

        if (result instanceof Access) {
            Access access = (Access) result;

            if ((visitorId != access.getVisitorId())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_ACCESS_WHERE);

            query.append(_FINDER_COLUMN_VISITOR_VISITORID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(visitorId);

                List<Access> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VISITOR,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "AccessPersistenceImpl.fetchByVisitor(long, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Access access = list.get(0);

                    result = access;

                    cacheResult(access);

                    if ((access.getVisitorId() != visitorId)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VISITOR,
                            finderArgs, access);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_VISITOR,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Access) result;
        }
    }

    /**
     * Removes the access where visitorId = &#63; from the database.
     *
     * @param visitorId the visitor ID
     * @return the access that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access removeByVisitor(long visitorId)
        throws NoSuchAccessException, SystemException {
        Access access = findByVisitor(visitorId);

        return remove(access);
    }

    /**
     * Returns the number of accesses where visitorId = &#63;.
     *
     * @param visitorId the visitor ID
     * @return the number of matching accesses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByVisitor(long visitorId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_VISITOR;

        Object[] finderArgs = new Object[] { visitorId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ACCESS_WHERE);

            query.append(_FINDER_COLUMN_VISITOR_VISITORID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(visitorId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the access in the entity cache if it is enabled.
     *
     * @param access the access
     */
    @Override
    public void cacheResult(Access access) {
        EntityCacheUtil.putResult(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessImpl.class, access.getPrimaryKey(), access);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BADGE,
            new Object[] { access.getBadge() }, access);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VEHICLE,
            new Object[] { access.getVehicleId() }, access);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VISITOR,
            new Object[] { access.getVisitorId() }, access);

        access.resetOriginalValues();
    }

    /**
     * Caches the accesses in the entity cache if it is enabled.
     *
     * @param accesses the accesses
     */
    @Override
    public void cacheResult(List<Access> accesses) {
        for (Access access : accesses) {
            if (EntityCacheUtil.getResult(
                        AccessModelImpl.ENTITY_CACHE_ENABLED, AccessImpl.class,
                        access.getPrimaryKey()) == null) {
                cacheResult(access);
            } else {
                access.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all accesses.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(AccessImpl.class.getName());
        }

        EntityCacheUtil.clearCache(AccessImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the access.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Access access) {
        EntityCacheUtil.removeResult(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessImpl.class, access.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(access);
    }

    @Override
    public void clearCache(List<Access> accesses) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Access access : accesses) {
            EntityCacheUtil.removeResult(AccessModelImpl.ENTITY_CACHE_ENABLED,
                AccessImpl.class, access.getPrimaryKey());

            clearUniqueFindersCache(access);
        }
    }

    protected void cacheUniqueFindersCache(Access access) {
        if (access.isNew()) {
            Object[] args = new Object[] { access.getBadge() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BADGE, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BADGE, args, access);

            args = new Object[] { access.getVehicleId() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_VEHICLE, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VEHICLE, args, access);

            args = new Object[] { access.getVisitorId() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_VISITOR, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VISITOR, args, access);
        } else {
            AccessModelImpl accessModelImpl = (AccessModelImpl) access;

            if ((accessModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_BADGE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { access.getBadge() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_BADGE, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_BADGE, args,
                    access);
            }

            if ((accessModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_VEHICLE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { access.getVehicleId() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_VEHICLE, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VEHICLE, args,
                    access);
            }

            if ((accessModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_VISITOR.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { access.getVisitorId() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_VISITOR, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_VISITOR, args,
                    access);
            }
        }
    }

    protected void clearUniqueFindersCache(Access access) {
        AccessModelImpl accessModelImpl = (AccessModelImpl) access;

        Object[] args = new Object[] { access.getBadge() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BADGE, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BADGE, args);

        if ((accessModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_BADGE.getColumnBitmask()) != 0) {
            args = new Object[] { accessModelImpl.getOriginalBadge() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BADGE, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_BADGE, args);
        }

        args = new Object[] { access.getVehicleId() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLE, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_VEHICLE, args);

        if ((accessModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_VEHICLE.getColumnBitmask()) != 0) {
            args = new Object[] { accessModelImpl.getOriginalVehicleId() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VEHICLE, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_VEHICLE, args);
        }

        args = new Object[] { access.getVisitorId() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VISITOR, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_VISITOR, args);

        if ((accessModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_VISITOR.getColumnBitmask()) != 0) {
            args = new Object[] { accessModelImpl.getOriginalVisitorId() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_VISITOR, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_VISITOR, args);
        }
    }

    /**
     * Creates a new access with the primary key. Does not add the access to the database.
     *
     * @param id the primary key for the new access
     * @return the new access
     */
    @Override
    public Access create(long id) {
        Access access = new AccessImpl();

        access.setNew(true);
        access.setPrimaryKey(id);

        return access;
    }

    /**
     * Removes the access with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the access
     * @return the access that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a access with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access remove(long id) throws NoSuchAccessException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the access with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the access
     * @return the access that was removed
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a access with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access remove(Serializable primaryKey)
        throws NoSuchAccessException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Access access = (Access) session.get(AccessImpl.class, primaryKey);

            if (access == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchAccessException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(access);
        } catch (NoSuchAccessException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Access removeImpl(Access access) throws SystemException {
        access = toUnwrappedModel(access);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(access)) {
                access = (Access) session.get(AccessImpl.class,
                        access.getPrimaryKeyObj());
            }

            if (access != null) {
                session.delete(access);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (access != null) {
            clearCache(access);
        }

        return access;
    }

    @Override
    public Access updateImpl(br.com.atilo.jcondo.manager.model.Access access)
        throws SystemException {
        access = toUnwrappedModel(access);

        boolean isNew = access.isNew();

        Session session = null;

        try {
            session = openSession();

            if (access.isNew()) {
                session.save(access);

                access.setNew(false);
            } else {
                session.merge(access);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !AccessModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(AccessModelImpl.ENTITY_CACHE_ENABLED,
            AccessImpl.class, access.getPrimaryKey(), access);

        clearUniqueFindersCache(access);
        cacheUniqueFindersCache(access);

        return access;
    }

    protected Access toUnwrappedModel(Access access) {
        if (access instanceof AccessImpl) {
            return access;
        }

        AccessImpl accessImpl = new AccessImpl();

        accessImpl.setNew(access.isNew());
        accessImpl.setPrimaryKey(access.getPrimaryKey());

        accessImpl.setId(access.getId());
        accessImpl.setDestinationId(access.getDestinationId());
        accessImpl.setVisitorId(access.getVisitorId());
        accessImpl.setVehicleId(access.getVehicleId());
        accessImpl.setAuthorizerId(access.getAuthorizerId());
        accessImpl.setAuthorizerName(access.getAuthorizerName());
        accessImpl.setBadge(access.getBadge());
        accessImpl.setEnded(access.isEnded());
        accessImpl.setRemark(access.getRemark());
        accessImpl.setOprDate(access.getOprDate());
        accessImpl.setOprUser(access.getOprUser());

        return accessImpl;
    }

    /**
     * Returns the access with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the access
     * @return the access
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a access with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access findByPrimaryKey(Serializable primaryKey)
        throws NoSuchAccessException, SystemException {
        Access access = fetchByPrimaryKey(primaryKey);

        if (access == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchAccessException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return access;
    }

    /**
     * Returns the access with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessException} if it could not be found.
     *
     * @param id the primary key of the access
     * @return the access
     * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a access with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access findByPrimaryKey(long id)
        throws NoSuchAccessException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the access with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the access
     * @return the access, or <code>null</code> if a access with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Access access = (Access) EntityCacheUtil.getResult(AccessModelImpl.ENTITY_CACHE_ENABLED,
                AccessImpl.class, primaryKey);

        if (access == _nullAccess) {
            return null;
        }

        if (access == null) {
            Session session = null;

            try {
                session = openSession();

                access = (Access) session.get(AccessImpl.class, primaryKey);

                if (access != null) {
                    cacheResult(access);
                } else {
                    EntityCacheUtil.putResult(AccessModelImpl.ENTITY_CACHE_ENABLED,
                        AccessImpl.class, primaryKey, _nullAccess);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(AccessModelImpl.ENTITY_CACHE_ENABLED,
                    AccessImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return access;
    }

    /**
     * Returns the access with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the access
     * @return the access, or <code>null</code> if a access with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Access fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the accesses.
     *
     * @return the accesses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Access> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the accesses.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of accesses
     * @param end the upper bound of the range of accesses (not inclusive)
     * @return the range of accesses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Access> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the accesses.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of accesses
     * @param end the upper bound of the range of accesses (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of accesses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Access> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Access> list = (List<Access>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ACCESS);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ACCESS;

                if (pagination) {
                    sql = sql.concat(AccessModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Access>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Access>(list);
                } else {
                    list = (List<Access>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the accesses from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Access access : findAll()) {
            remove(access);
        }
    }

    /**
     * Returns the number of accesses.
     *
     * @return the number of accesses
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ACCESS);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the access persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.manager.model.Access")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Access>> listenersList = new ArrayList<ModelListener<Access>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Access>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(AccessImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
