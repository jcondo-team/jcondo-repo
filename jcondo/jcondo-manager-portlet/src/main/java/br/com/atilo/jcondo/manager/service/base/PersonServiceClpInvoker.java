package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.PersonServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class PersonServiceClpInvoker {
    private String _methodName104;
    private String[] _methodParameterTypes104;
    private String _methodName105;
    private String[] _methodParameterTypes105;
    private String _methodName110;
    private String[] _methodParameterTypes110;
    private String _methodName111;
    private String[] _methodParameterTypes111;
    private String _methodName112;
    private String[] _methodParameterTypes112;
    private String _methodName113;
    private String[] _methodParameterTypes113;
    private String _methodName114;
    private String[] _methodParameterTypes114;
    private String _methodName115;
    private String[] _methodParameterTypes115;
    private String _methodName116;
    private String[] _methodParameterTypes116;
    private String _methodName117;
    private String[] _methodParameterTypes117;
    private String _methodName118;
    private String[] _methodParameterTypes118;
    private String _methodName119;
    private String[] _methodParameterTypes119;
    private String _methodName120;
    private String[] _methodParameterTypes120;
    private String _methodName121;
    private String[] _methodParameterTypes121;
    private String _methodName122;
    private String[] _methodParameterTypes122;
    private String _methodName123;
    private String[] _methodParameterTypes123;

    public PersonServiceClpInvoker() {
        _methodName104 = "getBeanIdentifier";

        _methodParameterTypes104 = new String[] {  };

        _methodName105 = "setBeanIdentifier";

        _methodParameterTypes105 = new String[] { "java.lang.String" };

        _methodName110 = "createPerson";

        _methodParameterTypes110 = new String[] {  };

        _methodName111 = "addPerson";

        _methodParameterTypes111 = new String[] {
                "java.lang.String", "java.lang.String", "java.lang.String",
                "java.lang.String", "java.util.Date",
                "br.com.atilo.jcondo.datatype.Gender", "java.lang.String",
                "br.com.atilo.jcondo.datatype.PersonType", "long"
            };

        _methodName112 = "updatePerson";

        _methodParameterTypes112 = new String[] {
                "long", "java.lang.String", "java.lang.String",
                "java.lang.String", "java.lang.String", "java.util.Date",
                "br.com.atilo.jcondo.datatype.Gender", "java.lang.String",
                "long", "br.com.atilo.jcondo.manager.model.Membership"
            };

        _methodName113 = "updatePortrait";

        _methodParameterTypes113 = new String[] {
                "long", "br.com.atilo.jcondo.Image"
            };

        _methodName114 = "updatePassword";

        _methodParameterTypes114 = new String[] {
                "long", "java.lang.String", "java.lang.String"
            };

        _methodName115 = "getPersonByIdentity";

        _methodParameterTypes115 = new String[] { "java.lang.String" };

        _methodName116 = "getPerson";

        _methodParameterTypes116 = new String[] { "long" };

        _methodName117 = "getPerson";

        _methodParameterTypes117 = new String[] {  };

        _methodName118 = "getPersonTypes";

        _methodParameterTypes118 = new String[] { "long" };

        _methodName119 = "getDomainPeople";

        _methodParameterTypes119 = new String[] { "long" };

        _methodName120 = "getDomainPeopleByType";

        _methodParameterTypes120 = new String[] {
                "long", "br.com.atilo.jcondo.datatype.PersonType"
            };

        _methodName121 = "getDomainPeopleByName";

        _methodParameterTypes121 = new String[] { "long", "java.lang.String" };

        _methodName122 = "completeUseRegistration";

        _methodParameterTypes122 = new String[] {
                "long", "java.lang.String", "boolean", "boolean"
            };

        _methodName123 = "authenticatePerson";

        _methodParameterTypes123 = new String[] { "long", "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName104.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes104, parameterTypes)) {
            return PersonServiceUtil.getBeanIdentifier();
        }

        if (_methodName105.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes105, parameterTypes)) {
            PersonServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName110.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes110, parameterTypes)) {
            return PersonServiceUtil.createPerson();
        }

        if (_methodName111.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes111, parameterTypes)) {
            return PersonServiceUtil.addPerson((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                (java.lang.String) arguments[3], (java.util.Date) arguments[4],
                (br.com.atilo.jcondo.datatype.Gender) arguments[5],
                (java.lang.String) arguments[6],
                (br.com.atilo.jcondo.datatype.PersonType) arguments[7],
                ((Long) arguments[8]).longValue());
        }

        if (_methodName112.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes112, parameterTypes)) {
            return PersonServiceUtil.updatePerson(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                (java.lang.String) arguments[3],
                (java.lang.String) arguments[4], (java.util.Date) arguments[5],
                (br.com.atilo.jcondo.datatype.Gender) arguments[6],
                (java.lang.String) arguments[7],
                ((Long) arguments[8]).longValue(),
                (br.com.atilo.jcondo.manager.model.Membership) arguments[9]);
        }

        if (_methodName113.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes113, parameterTypes)) {
            return PersonServiceUtil.updatePortrait(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[1]);
        }

        if (_methodName114.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes114, parameterTypes)) {
            PersonServiceUtil.updatePassword(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1], (java.lang.String) arguments[2]);

            return null;
        }

        if (_methodName115.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes115, parameterTypes)) {
            return PersonServiceUtil.getPersonByIdentity((java.lang.String) arguments[0]);
        }

        if (_methodName116.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes116, parameterTypes)) {
            return PersonServiceUtil.getPerson(((Long) arguments[0]).longValue());
        }

        if (_methodName117.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes117, parameterTypes)) {
            return PersonServiceUtil.getPerson();
        }

        if (_methodName118.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes118, parameterTypes)) {
            return PersonServiceUtil.getPersonTypes(((Long) arguments[0]).longValue());
        }

        if (_methodName119.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes119, parameterTypes)) {
            return PersonServiceUtil.getDomainPeople(((Long) arguments[0]).longValue());
        }

        if (_methodName120.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes120, parameterTypes)) {
            return PersonServiceUtil.getDomainPeopleByType(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.datatype.PersonType) arguments[1]);
        }

        if (_methodName121.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes121, parameterTypes)) {
            return PersonServiceUtil.getDomainPeopleByName(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        if (_methodName122.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes122, parameterTypes)) {
            PersonServiceUtil.completeUseRegistration(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                ((Boolean) arguments[2]).booleanValue(),
                ((Boolean) arguments[3]).booleanValue());

            return null;
        }

        if (_methodName123.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes123, parameterTypes)) {
            return PersonServiceUtil.authenticatePerson(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        throw new UnsupportedOperationException();
    }
}
