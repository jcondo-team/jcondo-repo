/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.model.impl;

import java.util.List;

import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;

/**
 * The extended model implementation for the Department service. Represents a row in the &quot;Department&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Department} interface.
 * </p>
 *
 * @author anderson
 */
public class DepartmentImpl extends DepartmentBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a department model instance should use the {@link br.com.atilo.jcondo.manager.model.Department} interface instead.
	 */

	private Organization organization;

	public DepartmentImpl() {
	}
	
	public void setType(DomainType type) {
		organization.setType(type.getLabel());
	}
	
	public DomainType getType() throws PortalException, SystemException {
		return DomainType.parse(organization.getType());
	}

	public void setDescription(String description) {
		organization.setComments(description);
	}

	public String getDescription() throws PortalException, SystemException {
		return organization.getComments();
	}
	
	public List<Person> getMembers() throws PortalException, SystemException {
		return PersonLocalServiceUtil.getDomainPeople(getId());
	}

	public Organization getOrganization() {
		return organization;
	}

	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

}