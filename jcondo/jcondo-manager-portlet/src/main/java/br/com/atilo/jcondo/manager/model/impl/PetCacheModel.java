package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Pet;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Pet in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Pet
 * @generated
 */
public class PetCacheModel implements CacheModel<Pet>, Externalizable {
    public long petId;
    public int typeId;
    public long flatId;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{petId=");
        sb.append(petId);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", flatId=");
        sb.append(flatId);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Pet toEntityModel() {
        PetImpl petImpl = new PetImpl();

        petImpl.setPetId(petId);
        petImpl.setTypeId(typeId);
        petImpl.setFlatId(flatId);

        if (oprDate == Long.MIN_VALUE) {
            petImpl.setOprDate(null);
        } else {
            petImpl.setOprDate(new Date(oprDate));
        }

        petImpl.setOprUser(oprUser);

        petImpl.resetOriginalValues();

        return petImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        petId = objectInput.readLong();
        typeId = objectInput.readInt();
        flatId = objectInput.readLong();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(petId);
        objectOutput.writeInt(typeId);
        objectOutput.writeLong(flatId);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
