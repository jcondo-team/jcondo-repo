/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.datatype.VehicleType;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.base.VehicleServiceBaseImpl;
import br.com.atilo.jcondo.manager.service.permission.VehiclePermission;

/**
 * The implementation of the vehicle remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.VehicleService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.VehicleServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.VehicleServiceUtil
 */
public class VehicleServiceImpl extends VehicleServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.VehicleServiceUtil} to access the vehicle remote service.
	 */
	
	public Vehicle createVehicle() {
		return vehicleLocalService.createVehicle(0);
	}

	public Vehicle addVehicle(String license, VehicleType type, long domainId, Image portrait, String brand, String color, String remark) throws PortalException, SystemException {
		if (domainId > 0 && !VehiclePermission.hasPermission(Permission.ADD, -1, domainId)) {
			throw new BusinessException("exception.vehicle.add.denied");
		}
		return vehicleLocalService.addVehicle(license, type, domainId, portrait, brand, color, remark);
	}

	public Vehicle updateVehicle(long id, VehicleType type, long domainId, Image portrait, String brand, String color, String remark) throws PortalException, SystemException {
		if (!VehiclePermission.hasPermission(Permission.UPDATE, id, domainId)) {
			throw new BusinessException("exception.vehicle.update.denied");
		}		
		return vehicleLocalService.updateVehicle(id, type, domainId, portrait, brand, color, remark);
	}

	public Vehicle updateVehiclePortrait(long id, Image portrait) throws PortalException, SystemException {
		Vehicle vehicle = vehicleLocalService.getVehicle(id);
		if (!VehiclePermission.hasPermission(Permission.UPDATE, id, vehicle.getDomainId())) {
			throw new BusinessException("exception.vehicle.update.denied");
		}
		return vehicleLocalService.updateVehiclePortrait(id, portrait);
	}

	public Vehicle deleteVehicle(long id) throws PortalException, SystemException {
		Vehicle vehicle = vehicleLocalService.getVehicle(id);
		if (!VehiclePermission.hasPermission(Permission.DELETE, id, vehicle.getDomainId())) {
			throw new BusinessException("exception.vehicle.delete.denied");
		}		
		return vehicleLocalService.deleteVehicle(id);
	}

	public Vehicle getVehicleByLicense(String license) throws PortalException, SystemException {
		Vehicle vehicle = vehicleLocalService.getVehicleByLicense(license);
		if (!VehiclePermission.hasPermission(Permission.VIEW, vehicle.getId(), vehicle.getDomainId())) {
			throw new BusinessException("exception.vehicle.view.denied");
		}
		return vehicle;
	}

	public List<Vehicle> getDomainVehicles(long domainId) throws SystemException {
		return vehicleLocalService.getDomainVehicles(domainId);
	}
	
	public List<Vehicle> getDomainVehiclesByLicense(long domainId, String license) throws PortalException, SystemException {
		return vehicleLocalService.getDomainVehiclesByLicense(domainId, license);
	}
}