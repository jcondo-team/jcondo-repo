package br.com.atilo.jcondo.manager.model.impl;

/**
 * The extended model implementation for the Mail service. Represents a row in the &quot;jco_mail&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Mail} interface.
 * </p>
 *
 * @author Brian Wing Shun Chan
 */
public class MailImpl extends MailBaseImpl {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never reference this class directly. All methods that expect a mail model instance should use the {@link br.com.atilo.jcondo.manager.model.Mail} interface instead.
     */
    public MailImpl() {
    }
}
