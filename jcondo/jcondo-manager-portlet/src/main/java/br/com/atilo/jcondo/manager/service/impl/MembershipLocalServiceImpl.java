/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.reflect.MethodUtils;

import br.com.atilo.jcondo.manager.NoSuchDepartmentException;
import br.com.atilo.jcondo.manager.NoSuchDomainException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.NoSuchMembershipException;
import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.security.Role;
import br.com.atilo.jcondo.manager.service.base.MembershipLocalServiceBaseImpl;
import br.com.atilo.jcondo.manager.service.permission.DomainPermission;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.RoleLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.service.UserGroupRoleLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;

/**
 * The implementation of the membership local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.MembershipLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.MembershipLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.MembershipLocalServiceUtil
 */
public class MembershipLocalServiceImpl extends MembershipLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.MembershipLocalServiceUtil} to access the membership local service.
	 */

	private void addRole(long userId, Role role) throws PortalException, SystemException  {
		addRole(userId, 0, role);
	}

	private void addRole(long userId, long organizationId, Role role) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();

		long roleId = RoleLocalServiceUtil.getRole(sc.getCompanyId(), role.getLabel()).getRoleId();
		
		if (organizationId <= 0) {
			RoleLocalServiceUtil.addUserRoles(userId, new long[] {roleId});
		} else {
			long groupId = GroupLocalServiceUtil.getOrganizationGroup(sc.getCompanyId(), organizationId).getGroupId();
			UserGroupRoleLocalServiceUtil.addUserGroupRoles(userId, groupId, new long[] {roleId});	
		}
	}	

	private void removeRole(long userId, Role role) throws PortalException, SystemException {
		removeRole(userId, 0, role);
	}
	
	private void removeRole(long userId, long organizationId, Role role) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
	
		long roleId = RoleLocalServiceUtil.getRole(sc.getCompanyId(), role.getLabel()).getRoleId();
		if (organizationId <= 0) {
			RoleLocalServiceUtil.unsetUserRoles(userId, new long[] {roleId});
		} else {
			long groupId = GroupLocalServiceUtil.getOrganizationGroup(sc.getCompanyId(), organizationId).getGroupId();
			UserGroupRoleLocalServiceUtil.deleteUserGroupRoles(userId, groupId, new long[] {roleId});	
		}
	}

	private void addRoles(Membership membership) throws Exception {
		boolean domainFound = false;

		Person person = personLocalService.getPerson(membership.getPersonId());

		try {
			Flat flat = flatLocalService.getFlat(membership.getDomainId());

			if (membership.getType() == PersonType.OWNER) {
				List<Person> renters = personLocalService.getDomainPeopleByType(membership.getDomainId(), 
																				PersonType.RENTER);

				// Se o apartamento estiver alugado, o proprietario nao tem pepel no apartamento
				if (renters.isEmpty()) {
					addRole(person.getUserId(), Role.CONDOMINIUM_MEMBER);
					addRole(person.getUserId(), flat.getOrganizationId(), Role.FLAT_MANAGER);
					UserLocalServiceUtil.updateStatus(person.getUserId(), WorkflowConstants.STATUS_APPROVED);
				}
			}

			if (membership.getType() == PersonType.RENTER) {
				List<Person> owners = personLocalService.getDomainPeopleByType(membership.getDomainId(), 
																			   PersonType.OWNER);

				for (Person owner : owners) {
					removeRoles(getDomainMembershipByPerson(membership.getDomainId(), owner.getId()));
				}

				addRole(person.getUserId(), Role.CONDOMINIUM_MEMBER);
				addRole(person.getUserId(), flat.getOrganizationId(), Role.FLAT_MANAGER);
				UserLocalServiceUtil.updateStatus(person.getUserId(), WorkflowConstants.STATUS_APPROVED);
			}

			if (membership.getType() == PersonType.DEPENDENT) {
				addRole(person.getUserId(), Role.CONDOMINIUM_MEMBER);
				addRole(person.getUserId(), flat.getOrganizationId(), Role.FLAT_ASSISTANT);
				UserLocalServiceUtil.updateStatus(person.getUserId(), WorkflowConstants.STATUS_APPROVED);
			}

			if (membership.getType() == PersonType.MANAGER) {
				addRole(person.getUserId(), flat.getOrganizationId(), Role.FLAT_ASSISTANT);
			}
			
			domainFound = true;
		} catch (NoSuchFlatException e) {
			try {
				Enterprise enterprise = enterpriseLocalService.getEnterprise(membership.getDomainId());

				if (membership.getType() == PersonType.MANAGER) {
					addRole(person.getUserId(), enterprise.getOrganizationId(), Role.SUPPLIER_MANAGER);
				}

				if (membership.getType() == PersonType.EMPLOYEE) {
					addRole(person.getUserId(), enterprise.getOrganizationId(), Role.SUPPLIER_MANAGER);
				}

				domainFound = true;
			} catch (NoSuchEnterpriseException ex) {
				try {
					Department department = departmentLocalService.getDepartment(membership.getDomainId());

					if (membership.getType() == PersonType.SYNCDIC || membership.getType() == PersonType.SUB_SYNDIC) {
						addRole(person.getUserId(), department.getOrganizationId(), Role.ADMIN_MANAGER);
					}

					if (membership.getType() == PersonType.ADMIN_ASSISTANT) {
						addRole(person.getUserId(), department.getOrganizationId(), Role.ADMIN_ASSISTANT);
					}

					if (membership.getType() == PersonType.ADMIN_ADVISOR || membership.getType() == PersonType.TAX_ADVISOR) {
						addRole(person.getUserId(), department.getOrganizationId(), Role.ADMIN_MEMBER);
					}

					if (membership.getType() == PersonType.GATEKEEPER) {
						addRole(person.getUserId(), Role.SECURITY_ASSISTANT);
					}

					if (membership.getType() == PersonType.EMPLOYEE || membership.getType() == PersonType.OUTSOURCED) {
						if (department.getType() == DomainType.ADMINISTRATION) {
							addRole(person.getUserId(), department.getOrganizationId(), Role.ADMIN_MEMBER);
						} else {
							addRole(person.getUserId(), department.getOrganizationId(), Role.DEPARTMENT_MEMBER);
						}
					}

					if (membership.getType() != PersonType.VISITOR) {
						addRole(person.getUserId(), Role.CONDOMINIUM_MEMBER);
						UserLocalServiceUtil.updateStatus(person.getUserId(), WorkflowConstants.STATUS_APPROVED);
					}

					domainFound = true;
				} catch (NoSuchDepartmentException exx) {
					// TODO: handle exception
				}
			}
		}

		if (!domainFound) {
			throw new NoSuchDomainException("No domain with id " + membership.getDomainId());
		}
	}

	private void removeRoles(Membership membership) throws Exception {
		boolean domainFound = true;

		Person person = personLocalService.getPerson(membership.getPersonId());

		try {
			Flat flat = flatLocalService.getFlat(membership.getDomainId());

			if (membership.getType() == PersonType.OWNER || 
					membership.getType() == PersonType.RENTER) {
				removeRole(person.getUserId(), flat.getOrganizationId(), Role.FLAT_MANAGER);
			}

			if (membership.getType() == PersonType.DEPENDENT || membership.getType() == PersonType.MANAGER) {
				removeRole(person.getUserId(), flat.getOrganizationId(), Role.FLAT_ASSISTANT);
			}

			if (membership.getType() == PersonType.RENTER) {
				List<Person> renters = personLocalService.getDomainPeopleByType(membership.getDomainId(), 
																				PersonType.RENTER);

				// Se nao houver outros locatarios, devolver papeis para os proprietarios
				if (renters.isEmpty() || renters.size() == 1) {
					List<Person> owners = personLocalService.getDomainPeopleByType(membership.getDomainId(), 
																				   PersonType.OWNER);

					for (Person owner : owners) {
						addRoles(getDomainMembershipByPerson(membership.getDomainId(), owner.getId()));
					}
				}
			}

			domainFound = true;
		} catch (NoSuchFlatException e) {
			try {
				Enterprise enterprise = enterpriseLocalService.getEnterprise(membership.getDomainId());

				if (membership.getType() == PersonType.MANAGER) {
					removeRole(person.getUserId(), enterprise.getOrganizationId(), Role.SUPPLIER_MANAGER);
				}

				if (membership.getType() == PersonType.EMPLOYEE) {
					removeRole(person.getUserId(), enterprise.getOrganizationId(), Role.SUPPLIER_MANAGER);
				}
				
				domainFound = true;
			} catch (NoSuchEnterpriseException ex) {
				try {
					Department department = departmentLocalService.getDepartment(membership.getDomainId());

					if (membership.getType() == PersonType.SYNCDIC || membership.getType() == PersonType.SUB_SYNDIC) {
						removeRole(person.getUserId(), department.getOrganizationId(), Role.ADMIN_MANAGER);
					}
	
					if (membership.getType() == PersonType.ADMIN_ASSISTANT) {
						removeRole(person.getUserId(), department.getOrganizationId(), Role.ADMIN_ASSISTANT);
					}
	
					if (membership.getType() == PersonType.ADMIN_ADVISOR || membership.getType() == PersonType.TAX_ADVISOR) {
						removeRole(person.getUserId(), department.getOrganizationId(), Role.ADMIN_MEMBER);
					}
	
					if (membership.getType() == PersonType.GATEKEEPER) {
						removeRole(person.getUserId(), department.getOrganizationId(), Role.SECURITY_ASSISTANT);
					}

					if (membership.getType() == PersonType.EMPLOYEE) {
						if (department.getType() == DomainType.ADMINISTRATION) {
							removeRole(person.getUserId(), department.getOrganizationId(), Role.ADMIN_MEMBER);
						} else {
							removeRole(person.getUserId(), department.getOrganizationId(), Role.DEPARTMENT_MEMBER);
						}
					}
					
					domainFound = true;
				} catch (NoSuchDepartmentException exx) {
				}
			}
		}

		if (!domainFound) {
			throw new NoSuchDomainException("No domain with id " + membership.getDomainId());
		}

		domainFound = false;
		for (Membership m : person.getMemberships()) {
			if (DomainPermission.hasDomainPermission(person.getId(), Permission.VIEW, m.getDomainId())) {
				domainFound = true;
				break;
			}
		}

		if (!domainFound) {
			removeRole(person.getUserId(), Role.CONDOMINIUM_MEMBER);
			UserLocalServiceUtil.updateStatus(person.getUserId(), WorkflowConstants.STATUS_DENIED);
		}		

	}
	
	public Membership addMembership(long personId, long domainId, PersonType type) throws Exception {
		if (domainId == 0) {
			return null;
		}

		if (type == null) {
			throw new SystemException("person.type.empty");
		}

		try {
			getDomainMembershipByPerson(domainId, personId);
			throw new SystemException("person.type.already.exists");
		} catch (NoSuchMembershipException e) {
		}

		Membership membership = membershipPersistence.create(counterLocalService.increment());
		membership.setPersonId(personId);
		membership.setDomainId(domainId);
		membership.setType(type);
		membership.setOprDate(new Date());
		membership.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());

		addRoles(membership);
		UserLocalServiceUtil.addOrganizationUsers(getDomainOrganizationId(membership.getDomain()), 
												  new long[] {membership.getPerson().getUserId()});
		
		return addMembership(membership);
	}
	
	public Membership updateMembership(long id, PersonType type) throws Exception {
		if (type == null) {
			throw new SystemException("person.type.empty");
		}

		Membership membership = membershipPersistence.findByPrimaryKey(id);

		removeRoles(membership);

		membership.setType(type);
		membership.setOprDate(new Date());
		membership.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());

		addRoles(membership);

		return updateMembership(membership);
	}

	public Membership deleteMembership(long id) throws Exception {
		Membership membership = this.getMembership(id);

		membership.setOprDate(new Date());
		membership.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());

		updateMembership(membership);

		membership = super.deleteMembership(id);

		removeRoles(membership);

		UserLocalServiceUtil.unsetOrganizationUsers(getDomainOrganizationId(membership.getDomain()), 
													new long[] {membership.getPerson().getUserId()});
		return membership;
	}

	private long getDomainOrganizationId(BaseModel<?> domain) throws Exception {
		return (Long) MethodUtils.invokeMethod(domain, "getOrganizationId", null);
	}
	
	public List<Membership> getPersonMemberships(long personId) throws SystemException {
		return membershipPersistence.findByPerson(personId);
	}
	
	public List<Membership> getDomainMembershipsByType(long domainId, PersonType type) throws SystemException {
		return membershipPersistence.findByDomainAndType(domainId, type.ordinal());
	}

	public Membership getDomainMembershipByPerson(long domainId, long personId) throws PortalException, SystemException {
		return membershipPersistence.findByDomainAndPerson(domainId, personId);
	}
}