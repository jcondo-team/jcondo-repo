package br.com.atilo.jcondo.manager.model.impl;

import br.com.atilo.jcondo.manager.model.Membership;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Membership in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Membership
 * @generated
 */
public class MembershipCacheModel implements CacheModel<Membership>,
    Externalizable {
    public long id;
    public long personId;
    public long domainId;
    public int typeId;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{id=");
        sb.append(id);
        sb.append(", personId=");
        sb.append(personId);
        sb.append(", domainId=");
        sb.append(domainId);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Membership toEntityModel() {
        MembershipImpl membershipImpl = new MembershipImpl();

        membershipImpl.setId(id);
        membershipImpl.setPersonId(personId);
        membershipImpl.setDomainId(domainId);
        membershipImpl.setTypeId(typeId);

        if (oprDate == Long.MIN_VALUE) {
            membershipImpl.setOprDate(null);
        } else {
            membershipImpl.setOprDate(new Date(oprDate));
        }

        membershipImpl.setOprUser(oprUser);

        membershipImpl.resetOriginalValues();

        return membershipImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        personId = objectInput.readLong();
        domainId = objectInput.readLong();
        typeId = objectInput.readInt();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(personId);
        objectOutput.writeLong(domainId);
        objectOutput.writeInt(typeId);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
