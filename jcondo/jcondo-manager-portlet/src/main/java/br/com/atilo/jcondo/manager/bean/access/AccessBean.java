package br.com.atilo.jcondo.manager.bean.access;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.AccessPermission;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.service.AccessPermissionLocalServiceUtil;
import br.com.atilo.jcondo.manager.bean.ModelDataModel;

public class AccessBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(AccessBean.class);

	private static final int ALL_WEEK = 0;
	
	private static final int NO_ACCESS = 0;
	
	private static final int FREE_ACCESS = 1;

	private static final int RESTRICTED_ACCESS = 2;
	
	private ModelDataModel<AccessPermission> accessPermissions;

	private Date beginDate;

	private Date endDate;
	
	private List<String> times;
	
	private int accessType;

	private AccessPermission[] selectedAccessPermissions;

	private Person person;
	
	private BaseModel<?> domain;

	public AccessBean() {
		times = new ArrayList<String>();
		for (int time = 0; time <= 24; time++) {
			if (time == 24) {
				times.add("23:59");
			} else {
				times.add(StringUtils.leftPad(time + ":00", 5, "0"));
			}
		}
		
		accessPermissions = new ModelDataModel<AccessPermission>(new ArrayList<AccessPermission>());
	}
	
	public void init(Person person, BaseModel<?> domain) {
		try {
			this.person = person;
			this.domain = domain;
			this.beginDate = null;
			this.endDate = null;
			this.selectedAccessPermissions = null;
			this.accessType = NO_ACCESS;

			accessPermissions.clear();

			AccessPermission accessPermission = AccessPermissionLocalServiceUtil.getPersonAccessPermission(person.getId(), 
																										   (Long) domain.getPrimaryKeyObj(), 
																										   ALL_WEEK);			

			if (accessPermission != null) {
				accessType = FREE_ACCESS;
			}
			
			for (int weekDay = Calendar.SUNDAY; weekDay <= Calendar.SATURDAY; weekDay++) {
				accessPermission = AccessPermissionLocalServiceUtil.getPersonAccessPermission(person.getId(), 
																						      (Long) domain.getPrimaryKeyObj(), 
																						      weekDay);
				if (accessPermission == null) {
					accessPermission = AccessPermissionLocalServiceUtil.createAccessPermission(weekDay);
					accessPermission.setWeekDay(weekDay);
					accessPermission.setBeginTime("00:00");
					accessPermission.setEndTime("23:59");
				} else {
					selectedAccessPermissions = (AccessPermission[]) ArrayUtils.add(selectedAccessPermissions, accessPermission);
					beginDate = accessPermission.getBeginDate();
					endDate = accessPermission.getEndDate();
				}

				accessPermissions.update(accessPermission);
			}

			if (!ArrayUtils.isEmpty(selectedAccessPermissions)) {
				accessType = RESTRICTED_ACCESS;
			}

		} catch (Exception e) {
			LOGGER.fatal("Failure on access permission initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	@SuppressWarnings("unchecked")
	public void onAccessPermissionSave() {
		try {
			List<AccessPermission> accessPermissions; 
			
			accessPermissions = AccessPermissionLocalServiceUtil.getPersonAccessPermissions(person.getId(), 
																						    (Long) domain.getPrimaryKeyObj());

			if (accessType == RESTRICTED_ACCESS) {
				if (selectedAccessPermissions.length == 0) {
					throw new BusinessException("exception.access.permission.no.weekday");
				}

				for (AccessPermission ap : accessPermissions) {
					if (ap.getWeekDay() == ALL_WEEK) {
						AccessPermissionLocalServiceUtil.deleteAccessPermission(ap);
					}
				}

				List<AccessPermission> newAccessPermissions; 

				newAccessPermissions = new ArrayList<AccessPermission>(Arrays.asList(selectedAccessPermissions));

				for (AccessPermission ap : (List<AccessPermission>) CollectionUtils.subtract(accessPermissions, newAccessPermissions)) {
					AccessPermissionLocalServiceUtil.deleteAccessPermission(ap);
				}	

				for (AccessPermission ap : (List<AccessPermission>) CollectionUtils.subtract(newAccessPermissions, accessPermissions)) {
					AccessPermissionLocalServiceUtil.addAccessPermission(person.getId(), (Long) domain.getPrimaryKeyObj(), 
																		 ap.getWeekDay(), ap.getBeginTime(), ap.getEndTime(), 
																		 beginDate, endDate);
				}

				for (AccessPermission ap : (List<AccessPermission>) CollectionUtils.intersection(accessPermissions, newAccessPermissions)) {
					ap.setBeginDate(beginDate);
					ap.setEndDate(endDate);
					AccessPermissionLocalServiceUtil.updateAccessPermission(ap);
				}
			} else if (accessType == NO_ACCESS) {
				for (AccessPermission ap : accessPermissions) {
					AccessPermissionLocalServiceUtil.deleteAccessPermission(ap);
				}
			} else if (accessType == FREE_ACCESS) {
				for (int i = accessPermissions.size() - 1; i >= 0; i--) {
					AccessPermission ap = accessPermissions.get(i);
					if (ap.getWeekDay() != ALL_WEEK) {
						AccessPermissionLocalServiceUtil.deleteAccessPermission(ap);
						accessPermissions.remove(i);
					}
				}

				if (CollectionUtils.isEmpty(accessPermissions)) {
					AccessPermissionLocalServiceUtil.addAccessPermission(person.getId(), 
																		 (Long) domain.getPrimaryKeyObj(), 
																		 ALL_WEEK, null, null, null, null);
				}
			}

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.access.permission.update.success", null);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on access permission save : " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on access permission save ", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public String displayWeekDayName(int weekDay) {
		return DateFormatSymbols.getInstance().getWeekdays()[weekDay];
	}

	public ModelDataModel<AccessPermission> getAccessPermissions() {
		return accessPermissions;
	}

	public void setAccessPermissions(
			ModelDataModel<AccessPermission> accessPermissions) {
		this.accessPermissions = accessPermissions;
	}

	public Date getBeginDate() {
		return beginDate;
	}

	public void setBeginDate(Date beginDate) {
		this.beginDate = beginDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public List<String> getTimes() {
		return times;
	}

	public void setTimes(List<String> times) {
		this.times = times;
	}

	public int getAccessType() {
		return accessType;
	}

	public void setAccessType(int accessType) {
		this.accessType = accessType;
	}

	public AccessPermission[] getSelectedAccessPermissions() {
		return selectedAccessPermissions;
	}

	public void setSelectedAccessPermissions(
			AccessPermission[] selectedAccessPermissions) {
		this.selectedAccessPermissions = selectedAccessPermissions;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public BaseModel<?> getDomain() {
		return domain;
	}

	public void setDomain(BaseModel<?> domain) {
		this.domain = domain;
	}

}