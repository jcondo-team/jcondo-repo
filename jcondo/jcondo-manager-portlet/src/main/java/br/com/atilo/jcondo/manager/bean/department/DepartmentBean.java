package br.com.atilo.jcondo.manager.bean.department;

import javax.faces.application.FacesMessage;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;
import br.com.atilo.jcondo.manager.bean.AbstractDomainBean;
import br.com.atilo.jcondo.manager.bean.DomainPhoneBean;

@ViewScoped
@ManagedBean
public class DepartmentBean extends AbstractDomainBean<Department> {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(DepartmentBean.class);

	private DomainPhoneBean<Department> phoneBean;

	private Department department;

	@Override
	public void preInit() throws Exception {
		domains = DepartmentLocalServiceUtil.getDepartments();
		personListBean = new MemberListBean();
		phoneBean = new DomainPhoneBean<Department>();
	}

	@Override
	public void onDomainSelect() throws Exception {
		super.onDomainSelect();
		phoneBean.init(domain);
	}
	
	@Override
	public void onDomainCreate() throws Exception {
		setDepartment(domain);
		domain = DepartmentLocalServiceUtil.createDepartment();
	}

	@Override
	public void onDomainSave() {
		try {
			if (domain.isNew()) {
				domain = DepartmentLocalServiceUtil.addDepartment(domain.getName(), DomainType.SUB_ADMINISTRATION, domain.getDescription());
			} else {
				domain = DepartmentLocalServiceUtil.updateDepartment(domain.getId(), domain.getName(), domain.getType(), domain.getDescription());
				phoneBean.onPhoneSave();
			}

			domains.add(domain);
			onDomainSelect();

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.department.save", null);
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on address save: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on address save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.department.save", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public DomainPhoneBean<Department> getPhoneBean() {
		return phoneBean;
	}

	public void setPhoneBean(DomainPhoneBean<Department> phoneBean) {
		this.phoneBean = phoneBean;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}