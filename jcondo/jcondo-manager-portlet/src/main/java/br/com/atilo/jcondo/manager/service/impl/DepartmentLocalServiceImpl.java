/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.WordUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.ListTypeConstants;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.manager.service.base.DepartmentLocalServiceBaseImpl;

/**
 * The implementation of the department local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.DepartmentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.DepartmentLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil
 */
public class DepartmentLocalServiceImpl extends DepartmentLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil} to access the department local service.
	 */

	public Department createDepartment() {
		Department department = super.createDepartment(0);
		department.setOrganization(OrganizationLocalServiceUtil.createOrganization(0));
		return department;
	}

	@Override
	public Department getDepartment(long id) throws PortalException, SystemException {
		Department department = super.getDepartment(id);
		department.setOrganization(OrganizationLocalServiceUtil.getOrganization(department.getOrganizationId()));
		return department;
	}

	public Department addDepartment(String name, DomainType type, String description) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Organization admin = OrganizationLocalServiceUtil.getOrganizations(sc.getCompanyId(), 0L).get(0);
		Organization organization = OrganizationLocalServiceUtil.addOrganization(sc.getUserId(), admin.getOrganizationId(), 
																				 name, type.getLabel(), 
																				 true, 0, 0, 
																				 ListTypeConstants.ORGANIZATION_STATUS_DEFAULT, 
																				 description, false, null);
		
		Department department = createDepartment(counterLocalService.increment());
		department.setOrganizationId(organization.getOrganizationId());
		department.setName(WordUtils.capitalizeFully(name.trim()));
		department.setOprDate(new Date());
		department.setOprUser(sc.getUserId());

		department = super.addDepartment(department); 
		department.setOrganization(organization);

		return department;
	}

	public Department updateDepartment(long departmentId, String name, DomainType type, String description) throws PortalException, SystemException {
		Department department = getDepartment(departmentId);
		department.setName(WordUtils.capitalizeFully(name.trim()));
		department.setType(type);
		department.setDescription(description);
		department.setOprDate(new Date());
		department.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());
		
		department = super.updateDepartment(department);

		department.setOrganization(OrganizationLocalServiceUtil.updateOrganization(department.getOrganization()));

		return department;
	}

	public List<Department> getDepartments() throws PortalException, SystemException {
		List<Department> departments = departmentPersistence.findAll();

		for (Department department : departments) {
			department.setOrganization(OrganizationLocalServiceUtil.getOrganization(department.getOrganizationId()));
		}

		return departments;
	}

}