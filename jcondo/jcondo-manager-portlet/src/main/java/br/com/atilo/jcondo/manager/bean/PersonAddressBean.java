package br.com.atilo.jcondo.manager.bean;

import java.io.Serializable;
import java.util.List;

import javax.faces.application.FacesMessage;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.service.AddressLocalServiceUtil;

import com.liferay.portal.model.Address;
import com.liferay.portal.model.Region;
import com.liferay.portal.service.CountryServiceUtil;
import com.liferay.portal.service.RegionServiceUtil;

public class PersonAddressBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(PersonAddressBean.class);

	private Person person;

	private Address address;

	private List<Region> states;

	private boolean dweller;

	public PersonAddressBean() {
	}
	
	public void init(Person person) throws Exception {
		this.person = person;
		this.address = null;

		if (!person.isNew() && person.getDomain() == null) {
			address = AddressLocalServiceUtil.getPersonAddress(person.getId());
		} 

		if (address == null) {
			address = AddressLocalServiceUtil.createAddress();
			dweller = true;
		} else {
			dweller = false;
		}

		states = RegionServiceUtil.getRegions(CountryServiceUtil.getCountryByName("Brazil").getCountryId());
	}

	public void onAddressSave() {
		try {
			if (dweller) {
				if (address.getAddressId() > 0) {
					AddressLocalServiceUtil.deleteAddress(address.getAddressId());
				}
			} else {
				if (address.getAddressId() > 0) {
					address = AddressLocalServiceUtil.updateAddress(address.getAddressId(), address.getStreet1(),
														  			address.getCity(), address.getRegionId(), 
														  			address.getZip());
				} else {
					address = AddressLocalServiceUtil.addPersonAddress(person.getId(), address.getStreet1(), 
																	   address.getCity(), address.getRegionId(), 
																	   address.getZip());
				}
			}
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on address save: " + e.getMessage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Unexpected failure on address save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_ERROR, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Region> getStates() {
		return states;
	}

	public void setStates(List<Region> states) {
		this.states = states;
	}

	public boolean isDweller() {
		return dweller;
	}

	public void setDweller(boolean dweller) {
		this.dweller = dweller;
	}
}
