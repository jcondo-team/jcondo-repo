/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.model.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.manager.NoSuchDomainException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.datatype.ParkingType;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;

/**
 * The extended model implementation for the Parking service. Represents a row in the &quot;jco_parking&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.model.Parking} interface.
 * </p>
 *
 * @author anderson
 */
public class ParkingImpl extends ParkingBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a parking model instance should use the {@link br.com.atilo.jcondo.manager.model.Parking} interface instead.
	 */
	public ParkingImpl() {
	}

	public ParkingType getType() {
		return ParkingType.valueOf(getTypeId());
	}

	public BaseModel<?> getOwnerDomain() throws PortalException, SystemException {
		try {
			return FlatLocalServiceUtil.getFlat(getOwnerDomainId());
		} catch (NoSuchFlatException e) {
			try {
				return EnterpriseLocalServiceUtil.getEnterprise(getOwnerDomainId());
			} catch (NoSuchEnterpriseException ex) {
				throw new NoSuchDomainException();
			}
		}
	}

	public BaseModel<?> getRenterDomain() throws PortalException, SystemException {
		try {
			return FlatLocalServiceUtil.getFlat(getRenterDomainId());
		} catch (NoSuchFlatException e) {
			try {
				return EnterpriseLocalServiceUtil.getEnterprise(getRenterDomainId());
			} catch (NoSuchEnterpriseException ex) {
				throw new NoSuchDomainException();
			}
		}
	}

}