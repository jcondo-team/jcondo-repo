package br.com.atilo.jcondo.manager.service.base;

import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class FlatLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName126;
    private String[] _methodParameterTypes126;
    private String _methodName127;
    private String[] _methodParameterTypes127;
    private String _methodName132;
    private String[] _methodParameterTypes132;
    private String _methodName133;
    private String[] _methodParameterTypes133;
    private String _methodName134;
    private String[] _methodParameterTypes134;
    private String _methodName135;
    private String[] _methodParameterTypes135;
    private String _methodName136;
    private String[] _methodParameterTypes136;
    private String _methodName137;
    private String[] _methodParameterTypes137;

    public FlatLocalServiceClpInvoker() {
        _methodName0 = "addFlat";

        _methodParameterTypes0 = new String[] {
                "br.com.atilo.jcondo.manager.model.Flat"
            };

        _methodName1 = "createFlat";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteFlat";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteFlat";

        _methodParameterTypes3 = new String[] {
                "br.com.atilo.jcondo.manager.model.Flat"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchFlat";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getFlat";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getFlats";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getFlatsCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateFlat";

        _methodParameterTypes15 = new String[] {
                "br.com.atilo.jcondo.manager.model.Flat"
            };

        _methodName126 = "getBeanIdentifier";

        _methodParameterTypes126 = new String[] {  };

        _methodName127 = "setBeanIdentifier";

        _methodParameterTypes127 = new String[] { "java.lang.String" };

        _methodName132 = "addFlat";

        _methodParameterTypes132 = new String[] {
                "int", "int", "boolean", "boolean", "boolean", "java.util.List"
            };

        _methodName133 = "updateFlat";

        _methodParameterTypes133 = new String[] {
                "long", "boolean", "boolean", "boolean", "java.util.List"
            };

        _methodName134 = "getPersonFlats";

        _methodParameterTypes134 = new String[] { "long" };

        _methodName135 = "getFlats";

        _methodParameterTypes135 = new String[] {  };

        _methodName136 = "setFlatPerson";

        _methodParameterTypes136 = new String[] { "long", "long" };

        _methodName137 = "setFlatDefaulting";

        _methodParameterTypes137 = new String[] { "long", "boolean" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return FlatLocalServiceUtil.addFlat((br.com.atilo.jcondo.manager.model.Flat) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return FlatLocalServiceUtil.createFlat(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return FlatLocalServiceUtil.deleteFlat(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return FlatLocalServiceUtil.deleteFlat((br.com.atilo.jcondo.manager.model.Flat) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return FlatLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return FlatLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return FlatLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return FlatLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return FlatLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return FlatLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return FlatLocalServiceUtil.fetchFlat(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return FlatLocalServiceUtil.getFlat(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return FlatLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return FlatLocalServiceUtil.getFlats(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return FlatLocalServiceUtil.getFlatsCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return FlatLocalServiceUtil.updateFlat((br.com.atilo.jcondo.manager.model.Flat) arguments[0]);
        }

        if (_methodName126.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes126, parameterTypes)) {
            return FlatLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName127.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes127, parameterTypes)) {
            FlatLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName132.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes132, parameterTypes)) {
            return FlatLocalServiceUtil.addFlat(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue(),
                ((Boolean) arguments[2]).booleanValue(),
                ((Boolean) arguments[3]).booleanValue(),
                ((Boolean) arguments[4]).booleanValue(),
                (java.util.List<br.com.atilo.jcondo.datatype.PetType>) arguments[5]);
        }

        if (_methodName133.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes133, parameterTypes)) {
            return FlatLocalServiceUtil.updateFlat(((Long) arguments[0]).longValue(),
                ((Boolean) arguments[1]).booleanValue(),
                ((Boolean) arguments[2]).booleanValue(),
                ((Boolean) arguments[3]).booleanValue(),
                (java.util.List<br.com.atilo.jcondo.datatype.PetType>) arguments[4]);
        }

        if (_methodName134.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes134, parameterTypes)) {
            return FlatLocalServiceUtil.getPersonFlats(((Long) arguments[0]).longValue());
        }

        if (_methodName135.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes135, parameterTypes)) {
            return FlatLocalServiceUtil.getFlats();
        }

        if (_methodName136.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes136, parameterTypes)) {
            return FlatLocalServiceUtil.setFlatPerson(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        if (_methodName137.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes137, parameterTypes)) {
            FlatLocalServiceUtil.setFlatDefaulting(((Long) arguments[0]).longValue(),
                ((Boolean) arguments[1]).booleanValue());

            return null;
        }

        throw new UnsupportedOperationException();
    }
}
