/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.manager.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.base.MembershipServiceBaseImpl;
import br.com.atilo.jcondo.manager.service.permission.PersonPermission;

/**
 * The implementation of the membership remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.manager.service.MembershipService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.manager.service.base.MembershipServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.MembershipServiceUtil
 */
public class MembershipServiceImpl extends MembershipServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.manager.service.MembershipServiceUtil} to access the membership remote service.
	 */

	public Membership createMembership(long id) {
		return membershipLocalService.createMembership(id); 
	}
	
	public Membership addMembership(long personId, long domainId, PersonType type) throws Exception {
		if (!PersonPermission.hasPermission(Permission.ASSIGN_MEMBER, type, domainId)) {
			throw new BusinessException("exception.person.add.denied");
		}

		return membershipLocalService.addMembership(personId, domainId, type);
	}
	
	public Membership deleteMembership(long id) throws Exception {
		return membershipLocalService.deleteMembership(id);
	}
	
	public Membership updateMembership(long id, PersonType type) throws Exception {

		return membershipLocalService.updateMembership(id, type); 
	}
	
	public List<Membership> getPersonMemberships(long personId) throws SystemException {
		return membershipLocalService.getPersonMemberships(personId);
	}
	
	public List<Membership> getDomainMembershipsByType(long domainId, PersonType type) throws SystemException {
		return membershipLocalService.getDomainMembershipsByType(domainId, type);
	}
	
	public Membership getDomainMembershipByPerson(long domainId, long personId) throws PortalException, SystemException {
		return membershipLocalService.getDomainMembershipByPerson(domainId, personId);
	}
}