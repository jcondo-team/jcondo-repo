create table jco_access (
	accessId LONG not null primary key,
	destinationId LONG,
	visitorId LONG,
	vehicleId LONG,
	authorizerId LONG,
	authorizerName VARCHAR(75) null,
	badge VARCHAR(75) null,
	ended BOOLEAN,
	remark VARCHAR(75) null,
	oprDate DATE null,
	oprUser LONG
);

create table jco_access_permission (
	accessPermissionId LONG not null primary key,
	personId LONG,
	domainId LONG,
	weekDay INTEGER,
	beginTime VARCHAR(75) null,
	endTime VARCHAR(75) null,
	beginDate DATE null,
	endDate DATE null,
	oprDate DATE null,
	oprUser LONG
);

create table jco_department (
	departmentId LONG not null primary key,
	organizationId LONG,
	name VARCHAR(75) null,
	oprDate DATE null,
	oprUser LONG
);

create table jco_enterprise (
	enterpriseId LONG not null primary key,
	organizationId LONG,
	statusId INTEGER,
	identity VARCHAR(75) null,
	oprDate DATE null,
	oprUser LONG
);

create table jco_flat (
	flatId LONG not null primary key,
	organizationId LONG,
	folderId LONG,
	personId LONG,
	block INTEGER,
	number INTEGER,
	pets BOOLEAN,
	disables BOOLEAN,
	brigade BOOLEAN,
	defaulting BOOLEAN,
	oprDate DATE null,
	oprUser LONG
);

create table jco_kinship (
	kinshipId LONG not null primary key,
	personId LONG,
	relativeId LONG,
	typeId INTEGER,
	oprDate DATE null,
	oprUser LONG
);

create table jco_mail (
	mailId LONG not null primary key,
	recipientId LONG,
	recipientName VARCHAR(75) null,
	takerId LONG,
	takerName VARCHAR(75) null,
	typeId INTEGER,
	code VARCHAR(75) null,
	dateIn DATE null,
	dateOut DATE null,
	remark VARCHAR(75) null,
	signatureId LONG,
	oprDate DATE null,
	oprUser LONG
);

create table jco_mail_note (
	mailNoteId LONG not null primary key,
	mailId LONG,
	description VARCHAR(75) null,
	oprDate DATE null,
	oprUser LONG
);

create table jco_membership (
	membershipId LONG not null primary key,
	personId LONG,
	domainId LONG,
	typeId INTEGER,
	oprDate DATE null,
	oprUser LONG
);

create table jco_mgr_Telephone (
	id_ LONG not null primary key,
	typeName VARCHAR(75) null,
	extension LONG,
	number_ LONG,
	primary_ BOOLEAN
);

create table jco_parking (
	parkingId LONG not null primary key,
	code VARCHAR(75) null,
	typeId INTEGER,
	ownerDomainId LONG,
	renterDomainId LONG,
	oprDate DATE null,
	oprUser LONG
);

create table jco_person (
	personId LONG not null primary key,
	userId LONG,
	domainId LONG,
	identity VARCHAR(75) null,
	remark VARCHAR(75) null,
	oprDate DATE null,
	oprUser LONG
);

create table jco_pet (
	petId LONG not null primary key,
	typeId INTEGER,
	flatId LONG,
	oprDate DATE null,
	oprUser LONG
);

create table jco_vehicle (
	vehicleId LONG not null primary key,
	domainId LONG,
	imageId LONG,
	typeId INTEGER,
	license VARCHAR(75) null,
	brand VARCHAR(75) null,
	color VARCHAR(75) null,
	remark VARCHAR(75) null,
	oprDate DATE null,
	oprUser LONG
);