create index IX_D53AC640 on jco_access (badge);
create index IX_30C03984 on jco_access (vehicleId);
create index IX_E9F66DC6 on jco_access (visitorId);

create index IX_D1E6495E on jco_access_permission (personId, domainId);
create index IX_1100E49A on jco_access_permission (personId, domainId, weekDay);

create index IX_3C79A783 on jco_department (organizationId);

create index IX_D0D70662 on jco_enterprise (identity);
create index IX_6CB251D2 on jco_enterprise (organizationId);

create index IX_4ED432AA on jco_flat (organizationId);
create index IX_6FA7BF4C on jco_flat (personId);

create index IX_9DFF2E2D on jco_kinship (personId);

create index IX_86189358 on jco_membership (domainId);
create index IX_D4BEEF9C on jco_membership (domainId, personId);
create index IX_42AB6781 on jco_membership (domainId, typeId);
create index IX_F8B0A969 on jco_membership (personId);
create index IX_BA04DE85 on jco_membership (personId, domainId, typeId);
create index IX_976B308E on jco_membership (typeId);

create index IX_4718D8D on jco_parking (ownerDomainId);
create index IX_84CD3DCE on jco_parking (renterDomainId);

create index IX_3C408CD6 on jco_person (identity);
create index IX_6D7669DE on jco_person (userId);

create index IX_57FD76E4 on jco_pet (flatId);
create index IX_66132E0D on jco_pet (typeId, flatId);

create index IX_E072E63C on jco_vehicle (domainId);
create index IX_59827008 on jco_vehicle (license);