package br.com.atilo.jcondo.docs.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.lar.StagedModelType;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portlet.expando.model.ExpandoBridge;

public class DefaultFolder implements Folder {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String name;
	
	public DefaultFolder(String name) {
		this.name = name;
	}
	
	public Object clone() {
		return null;
	}
	
	@Override
	public Map<String, Serializable> getAttributes() {
		return null;
	}

	@Override
	public Object getModel() {
		return null;
	}

	@Override
	public long getPrimaryKey() {
		return 0;
	}

	@Override
	public boolean isEscapedModel() {
		return false;
	}

	@Override
	public Folder toEscapedModel() {
		return null;
	}

	@Override
	public Folder toUnescapedModel() {
		return null;
	}

	@Override
	public void setGroupId(long groupId) {

	}

	@Override
	public void setCompanyId(long companyId) {

	}

	@Override
	public void setCreateDate(Date date) {

	}

	@Override
	public void setModifiedDate(Date date) {

	}

	@Override
	public void setUserId(long userId) {

	}

	@Override
	public void setUserName(String userName) {

	}

	@Override
	public void setUserUuid(String userUuid) {

	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return null;
	}

	@Override
	public Class<?> getModelClass() {
		return null;
	}

	@Override
	public String getModelClassName() {
		return null;
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return null;
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {

	}

	@Override
	public StagedModelType getStagedModelType() {
		return null;
	}

	@Override
	public void setUuid(String uuid) {

	}

	@Override
	public boolean containsPermission(PermissionChecker permissionChecker, String actionId)
			throws PortalException, SystemException {
		return false;
	}

	@Override
	public List<Long> getAncestorFolderIds() throws PortalException, SystemException {
		return null;
	}

	@Override
	public List<Folder> getAncestors() throws PortalException, SystemException {
		return null;
	}

	@Override
	public long getCompanyId() {
		return 0;
	}

	@Override
	public Date getCreateDate() {
		return null;
	}

	@Override
	public String getDescription() {
		return null;
	}

	@Override
	public long getFolderId() {
		return 0;
	}

	@Override
	public long getGroupId() {
		return 0;
	}

	@Override
	public Date getLastPostDate() {
		return null;
	}

	@Override
	public Date getModifiedDate() {
		return null;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public Folder getParentFolder() throws PortalException, SystemException {
		return null;
	}

	@Override
	public long getParentFolderId() {
		return 0;
	}

	@Override
	public long getRepositoryId() {
		return 0;
	}

	@Override
	public long getUserId() {
		return 0;
	}

	@Override
	public String getUserName() {
		return null;
	}

	@Override
	public String getUserUuid() throws SystemException {
		return null;
	}

	@Override
	public String getUuid() {
		return null;
	}

	@Override
	public boolean hasInheritableLock() {
		return false;
	}

	@Override
	public boolean hasLock() {
		return false;
	}

	@Override
	public boolean isDefaultRepository() {
		return false;
	}

	@Override
	public boolean isLocked() {
		return false;
	}

	@Override
	public boolean isMountPoint() {
		return false;
	}

	@Override
	public boolean isRoot() {
		return false;
	}

	@Override
	public boolean isSupportsLocking() {
		return false;
	}

	@Override
	public boolean isSupportsMetadata() {
		return false;
	}

	@Override
	public boolean isSupportsMultipleUpload() {
		return false;
	}

	@Override
	public boolean isSupportsShortcuts() {
		return false;
	}

	@Override
	public boolean isSupportsSocial() {
		return false;
	}

	@Override
	public boolean isSupportsSubscribing() {
		return false;
	}

}
