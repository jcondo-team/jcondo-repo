package br.com.atilo.jcondo.docs.bean;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.util.DLUtil;

public class FileDataModel extends LazyDataModel<String> {

	private static final long serialVersionUID = 1L;
	
	private FileEntry file;
	
	private ThemeDisplay themeDisplay;
	
	public FileDataModel(FileEntry file) {
		this.file = file;
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		this.themeDisplay = (ThemeDisplay) sc.getRequest().getAttribute(WebKeys.THEME_DISPLAY);
	}

	@Override
	public List<String> load(int first, int pageSize, String sortField, SortOrder sortOrder, Map<String, Object> filters) {
		List<String> pages = new ArrayList<String>();

		try {
			pages.add(DLUtil.getPreviewURL(file, file.getFileVersion(), themeDisplay, "&previewFileIndex=" + (first + 1)));
		} catch (PortalException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SystemException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return pages;
	}
}
