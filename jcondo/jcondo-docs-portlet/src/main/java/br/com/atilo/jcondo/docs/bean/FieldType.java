package br.com.atilo.jcondo.docs.bean;

public enum FieldType {

	DATE("ddm-date"),
 	RADIO("radio"),
 	SELECT("select"),
 	TEXT("text"),
 	CHECKBOX("checkbox"),
 	DECIMAL("ddm-decimal"),
 	NUMBER("ddm-number");

	private String label;

	private FieldType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}
