package br.com.atilo.jcondo.docs.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.DefaultTreeNode;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.TreeNode;
import org.primefaces.model.UploadedFile;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.MenuModel;

import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.FileVersion;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.kernel.workflow.WorkflowConstants;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portal.util.PortalUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntryMetadata;
import com.liferay.portlet.documentlibrary.model.DLFileEntryType;
import com.liferay.portlet.documentlibrary.model.DLFileVersion;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryMetadataLocalServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryTypeLocalServiceUtil;
import com.liferay.portlet.documentlibrary.util.DLUtil;
import com.liferay.portlet.documentlibrary.util.ImageProcessorUtil;
import com.liferay.portlet.documentlibrary.util.PDFProcessorUtil;
import com.liferay.portlet.dynamicdatamapping.model.DDMStructure;
import com.liferay.portlet.dynamicdatamapping.storage.Field;
import com.liferay.portlet.dynamicdatamapping.storage.Fields;
import com.liferay.portlet.dynamicdatamapping.storage.StorageEngineUtil;
import com.liferay.portlet.dynamicdatamapping.util.DDMUtil;

import br.com.atilo.jcondo.docs.model.DefaultFolder;

@ManagedBean(name="repoBean")
@SessionScoped
public class RepositoryBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private static Logger LOGGER = Logger.getLogger(RepositoryBean.class);

	private long repositoryId;

	private List<Folder> folders;

	private List<FileEntry> files;

	private MenuModel menu;
	
	private long folderId;
	
	private String folderName;

	private String folderDescription;	

	private long selectedFolderId;
	
	private String fileName;
	
	private List<String> pages;

	private ThemeDisplay themeDisplay;
	
	private StreamedContent fileStreamedContent;

	private String fileURL;

	private TreeNode folderTree;

	private List<Long> selectedFiles;

	private List<Long> selectedFolders;

	private TreeNode selectedTreeNode;

	private List<DLFileEntryType> fileTypes;

	private FileVersion fileVersion;
	
	private List<String> fileVersions;
	
	private List<Field> fields;

	public RepositoryBean() {
		pages = new ArrayList<String>();
		selectedFolders = new ArrayList<Long>();
		selectedFiles = new ArrayList<Long>();
		fileVersions = new ArrayList<String>();
		fields = new ArrayList<Field>();
	}
	
	@PostConstruct
	public void init() {
		try {
			repositoryId = GroupLocalServiceUtil.getGroup(PortalUtil.getDefaultCompanyId(), GroupConstants.GUEST).getGroupId();
			folders = DLAppServiceUtil.getFolders(repositoryId, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
			files = DLAppServiceUtil.getFileEntries(repositoryId, DLFolderConstants.DEFAULT_PARENT_FOLDER_ID);
			folderId = DLFolderConstants.DEFAULT_PARENT_FOLDER_ID;
			generateMenu();

			ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
			themeDisplay = (ThemeDisplay) sc.getRequest().getAttribute(WebKeys.THEME_DISPLAY);

			fileTypes = DLFileEntryTypeLocalServiceUtil.getFileEntryTypes(new long[] {GroupLocalServiceUtil.getGroup(PortalUtil.getDefaultCompanyId(), 
																		  String.valueOf(PortalUtil.getDefaultCompanyId())).getGroupId()});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateMenu() {
		try {
			menu = new DefaultMenuModel();

			DefaultMenuItem item = new DefaultMenuItem("");
			item.setCommand("#{repoBean.onFolderOpen(" + DLFolderConstants.DEFAULT_PARENT_FOLDER_ID + ")}");
			item.setUpdate("@form");
			item.setPartialSubmit(true);
			item.setProcess("@this");
			menu.addElement(item);

			if (folderId != DLFolderConstants.DEFAULT_PARENT_FOLDER_ID) {
				Folder folder = DLAppServiceUtil.getFolder(folderId);
				
				List<Folder> parentFolders = folder.getAncestors();

				for (int i = parentFolders.size() - 1; i >= 0; i--) {
					item = new DefaultMenuItem(parentFolders.get(i).getName());
					item.setCommand("#{repoBean.onFolderOpen(" + parentFolders.get(i).getFolderId() + ")}");
					item.setUpdate("@form");
					item.setPartialSubmit(true);
					item.setProcess("@this");
					menu.addElement(item);
				}

				item = new DefaultMenuItem(folder.getName());
				item.setCommand("#{repoBean.onFolderOpen(" + folder.getFolderId() + ")}");
				item.setUpdate("@form");
				item.setPartialSubmit(true);
				item.setProcess("@this");
				menu.addElement(item);
			}

			menu.generateUniqueIds();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void generateFolderTree(TreeNode parentNode) {
		try {
			long folderId = ((Folder) parentNode.getData()).getFolderId();
			List<Folder> folders = DLAppServiceUtil.getFolders(repositoryId, folderId);

			for (Folder folder : folders) {
				TreeNode folderNode = new DefaultTreeNode(folder, parentNode);
				generateFolderTree(folderNode);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onFolderSelect(long folderId) {
		try {
			if (selectedFolders.contains(folderId)) {
				selectedFolders.remove(folderId);
			} else {
				selectedFolders.add(folderId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onFolderOpen(long folderId) {
		try {
			folders = DLAppServiceUtil.getFolders(repositoryId, folderId);
			files = DLAppServiceUtil.getFileEntries(repositoryId, folderId);

			setFolderId(folderId);
			generateMenu();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onFolderEvent() {
		try {
			if (selectedFolderId == 0) {
				DLAppServiceUtil.addFolder(repositoryId, folderId, folderName, folderDescription, new ServiceContext());
			} else {
				DLAppServiceUtil.updateFolder(selectedFolderId, folderName, folderDescription, new ServiceContext());
			}

			folders = DLAppServiceUtil.getFolders(repositoryId, folderId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onFolderDelete(long folderId) {
		try {
			DLAppServiceUtil.deleteFolder(folderId);
			folders = DLAppServiceUtil.getFolders(repositoryId, this.folderId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onFolderTreeCreate() {
		try {
			folderTree = new DefaultTreeNode(null, null);
			folderTree.setExpanded(true);

			TreeNode repository = new DefaultTreeNode(new DefaultFolder("Repositorio"), folderTree);
			repository.setExpanded(true);

			generateFolderTree(repository);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onFolderItemsMove() {
		try {
			long destFolderId = ((Folder) selectedTreeNode.getData()).getFolderId();
			ServiceContext sc = new ServiceContext();
			sc.setScopeGroupId(repositoryId);

			if (!selectedFolders.isEmpty()) {
				for (Long folderId : selectedFolders) {
					DLAppServiceUtil.moveFolder(folderId, destFolderId, sc);
				}

				folders = DLAppServiceUtil.getFolders(repositoryId, folderId);
			}

			if (!selectedFiles.isEmpty()) {
				for (Long fileId : selectedFiles) {
					DLAppServiceUtil.moveFileEntry(fileId, destFolderId, sc);
				}			

				files = DLAppServiceUtil.getFileEntries(repositoryId, folderId);
			}
			
			onUnselectAll();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onFileSelect(long fileId) {
		try {
			if (selectedFiles.contains(fileId)) {
				selectedFiles.remove(fileId);
			} else {
				selectedFiles.add(fileId);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void onFileOpen(long fileId, String version) {
		try {
			fileVersion = DLAppServiceUtil.getFileEntry(fileId).getFileVersion(version);
			long pageCount = 0;

			if (PDFProcessorUtil.hasImages(fileVersion)) {
				pageCount = PDFProcessorUtil.getPreviewFileCount(fileVersion);
				pages.clear();
				
				for (int i = 1; i <= pageCount; i++) {
					pages.add(DLUtil.getPreviewURL(fileVersion.getFileEntry(), fileVersion, themeDisplay, "&previewFileIndex=" + i));
				}
			} else if (ImageProcessorUtil.hasImages(fileVersion)) {
				pages.clear();
				pages.add(DLUtil.getImagePreviewURL(fileVersion.getFileEntry(), themeDisplay));
			}

			fileVersions.clear();

			for (FileVersion fileVersion : fileVersion.getFileEntry().getFileVersions(WorkflowConstants.STATUS_ANY)) {
				fileVersions.add(fileVersion.getVersion());
			}

			onFileTypeSelect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onFileSave() {
		try {
			boolean publish = false;
			boolean majorVersion = false;
			FileEntry fileEntry = fileVersion.getFileEntry();
			long fileEntryTypeId = ((DLFileVersion) fileVersion.getModel()).getFileEntryTypeId();
			
			ServiceContext sc = new ServiceContext();
			sc.setScopeGroupId(repositoryId);
			sc.setAttribute("fileEntryTypeId", fileEntryTypeId);

			if (fileEntryTypeId > 0) {
				DLFileEntryType fileEntryType = DLFileEntryTypeLocalServiceUtil.getDLFileEntryType(fileEntryTypeId);
				for (DDMStructure structure : fileEntryType.getDDMStructures()) {
					Fields fds = new Fields();
					for (Field f : fields) {
						if (f.getDDMStructureId() == structure.getStructureId()) {
							if (FieldType.RADIO.getLabel().equals(f.getType()) || FieldType.SELECT.getLabel().equals(f.getType())) {
								f.setValue(JSONFactoryUtil.serialize(new String[] {String.valueOf(f.getValue())}));
							}
							fds.put(f);
						}
						//sc.setAttribute(f.getName(), f.getValue().toString());
					}
					//Fields fds = DDMUtil.getFields(structure.getStructureId(), sc);

					sc.setAttribute(Fields.class.getName() + structure.getStructureId(), fds);
				}
			}

			if (fileEntry.getFolder().getName().equals("temp")) {
				DLAppServiceUtil.moveFileEntry(fileVersion.getFileEntryId(), folderId, sc);
//				DLAppServiceUtil.addFileEntry(repositoryId, folderId, fileEntry.getTitle(), fileVersion.getMimeType(), 
//											  fileVersion.getTitle(), null, null, fileVersion.getContentStream(false), 
//											  fileVersion.getSize(), sc);
//				DLAppServiceUtil.deleteTempFileEntry(repositoryId, fileEntry.getFolderId(), fileEntry.getTitle(), "temp");
			} else {
				
			}

			if (publish) {
				//DLFileEntryLocalServiceUtil.updateStatus(userId, fileVersion.getFileVersionId(), WorkflowConstants.STATUS_APPROVED, workflowContext, sc);			
			}

			DLAppServiceUtil.updateFileEntry(fileVersion.getFileEntryId(), null, fileVersion.getMimeType(), 
											 fileVersion.getTitle(), null, null, majorVersion, 
											 fileVersion.getContentStream(false), fileVersion.getSize(), sc);

			files = DLAppServiceUtil.getFileEntries(repositoryId, folderId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onFileDelete(long fileId) {
		try {
			DLAppServiceUtil.moveFileEntryToTrash(fileId);
			files = DLAppServiceUtil.getFileEntries(repositoryId, folderId);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onFileUpload(FileUploadEvent event) {
		try {
			UploadedFile file = event.getFile();

			FileEntry fileEntry = DLAppServiceUtil.addTempFileEntry(repositoryId, folderId, file.getFileName(), 
																	"temp", file.getInputstream(), file.getContentType());

			fileVersion = fileEntry.getFileVersion();
			onFileTypeSelect();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void onUnselectAll() {
		selectedFiles.clear();
		selectedFolders.clear();
	}

	public void onFileTypeSelect() {
		try {
			DLFileEntryType type = DLFileEntryTypeLocalServiceUtil.getDLFileEntryType(((DLFileVersion) fileVersion.getModel()).getFileEntryTypeId());
			fields.clear();

			for (DDMStructure structure : type.getDDMStructures()) {
				Fields fds;

				try {
					DLFileEntryMetadata femd = DLFileEntryMetadataLocalServiceUtil.getFileEntryMetadata(structure.getStructureId(), fileVersion.getFileVersionId());
					fds = StorageEngineUtil.getFields(femd.getDDMStorageId());
				} catch (Exception e) {
					ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
					for (String name : structure.getFieldNames()) sc.setAttribute(name, "");
					fds = DDMUtil.getFields(structure.getStructureId(), sc);
				}

				for (String name : structure.getFieldNames()) {
					Field f = fds.get(name);
					if (f != null && f.getValue() != null && !f.isPrivate()) {
						f.setValue(DDMUtil.getDisplayFieldValue(themeDisplay, f.getValue(), f.getType()));
					}
					fields.add(f);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public String getFilePreview(long fileId) {
		try {
			FileEntry file = DLAppServiceUtil.getFileEntry(fileId);
			return DLUtil.getImagePreviewURL(file, themeDisplay);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public String getFileURL() {
		try {
			if (fileVersion != null) {
				String mimeType = fileVersion.getMimeType();
				return DLUtil.getPreviewURL(fileVersion.getFileEntry(), fileVersion, themeDisplay,
											mimeType.contains("pdf") || mimeType.contains("image") ? "" : "&targetExtension=pdf", true, true);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public StreamedContent getFileStreamedContent() {
		try {
			return new DefaultStreamedContent(fileVersion.getContentStream(false), 
											  fileVersion.getMimeType(), 
											  DLUtil.getTitleWithExtension(fileVersion.getTitle(), fileVersion.getExtension()));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}

	public void setFileStreamedContent(StreamedContent fileStreamedContent) {
		this.fileStreamedContent = fileStreamedContent;
	}

	public List<Folder> getFolders() {
		return folders;
	}

	public void setFolders(List<Folder> folders) {
		this.folders = folders;
	}

	public List<FileEntry> getFiles() {
		return files;
	}

	public void setFiles(List<FileEntry> files) {
		this.files = files;
	}

	public MenuModel getMenu() {
		return menu;
	}

	public void setMenu(MenuModel menu) {
		this.menu = menu;
	}

	public long getFolderId() {
		return folderId;
	}

	public void setFolderId(long folderId) {
		this.folderId = folderId;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}

	public String getFolderDescription() {
		return folderDescription;
	}

	public void setFolderDescription(String folderDescription) {
		this.folderDescription = folderDescription;
	}

	public long getSelectedFolderId() {
		return selectedFolderId;
	}

	public void setSelectedFolderId(long selectedFolderId) {
		this.selectedFolderId = selectedFolderId;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<String> getPages() {
		return pages;
	}

	public void setPages(List<String> pages) {
		this.pages = pages;
	}

	public TreeNode getFolderTree() {
		return folderTree;
	}

	public void setFolderTree(TreeNode folderTree) {
		this.folderTree = folderTree;
	}

	public List<Long> getSelectedFolders() {
		return selectedFolders;
	}

	public void setSelectedFolders(List<Long> selectedFolders) {
		this.selectedFolders = selectedFolders;
	}

	public List<Long> getSelectedFiles() {
		return selectedFiles;
	}

	public void setSelectedFiles(List<Long> selectedFiles) {
		this.selectedFiles = selectedFiles;
	}

	public TreeNode getSelectedTreeNode() {
		return selectedTreeNode;
	}

	public void setSelectedTreeNode(TreeNode selectedTreeNode) {
		this.selectedTreeNode = selectedTreeNode;
	}

	public List<DLFileEntryType> getFileTypes() {
		return fileTypes;
	}

	public void setFileTypes(List<DLFileEntryType> fileTypes) {
		this.fileTypes = fileTypes;
	}

	public FileVersion getFileVersion() {
		return fileVersion;
	}

	public void setFileVersion(FileVersion fileVersion) {
		this.fileVersion = fileVersion;
	}

	public List<String> getFileVersions() {
		return fileVersions;
	}

	public void setFileVersions(List<String> fileVersions) {
		this.fileVersions = fileVersions;
	}

	public List<Field> getFields() {
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

}
