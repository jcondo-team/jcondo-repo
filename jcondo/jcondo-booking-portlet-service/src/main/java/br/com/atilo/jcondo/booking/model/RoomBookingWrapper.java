package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link RoomBooking}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomBooking
 * @generated
 */
public class RoomBookingWrapper implements RoomBooking,
    ModelWrapper<RoomBooking> {
    private RoomBooking _roomBooking;

    public RoomBookingWrapper(RoomBooking roomBooking) {
        _roomBooking = roomBooking;
    }

    @Override
    public Class<?> getModelClass() {
        return RoomBooking.class;
    }

    @Override
    public String getModelClassName() {
        return RoomBooking.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("roomId", getRoomId());
        attributes.put("weekDay", getWeekDay());
        attributes.put("openHour", getOpenHour());
        attributes.put("closeHour", getCloseHour());
        attributes.put("period", getPeriod());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long roomId = (Long) attributes.get("roomId");

        if (roomId != null) {
            setRoomId(roomId);
        }

        Integer weekDay = (Integer) attributes.get("weekDay");

        if (weekDay != null) {
            setWeekDay(weekDay);
        }

        Integer openHour = (Integer) attributes.get("openHour");

        if (openHour != null) {
            setOpenHour(openHour);
        }

        Integer closeHour = (Integer) attributes.get("closeHour");

        if (closeHour != null) {
            setCloseHour(closeHour);
        }

        Integer period = (Integer) attributes.get("period");

        if (period != null) {
            setPeriod(period);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this room booking.
    *
    * @return the primary key of this room booking
    */
    @Override
    public long getPrimaryKey() {
        return _roomBooking.getPrimaryKey();
    }

    /**
    * Sets the primary key of this room booking.
    *
    * @param primaryKey the primary key of this room booking
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _roomBooking.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this room booking.
    *
    * @return the ID of this room booking
    */
    @Override
    public long getId() {
        return _roomBooking.getId();
    }

    /**
    * Sets the ID of this room booking.
    *
    * @param id the ID of this room booking
    */
    @Override
    public void setId(long id) {
        _roomBooking.setId(id);
    }

    /**
    * Returns the room ID of this room booking.
    *
    * @return the room ID of this room booking
    */
    @Override
    public long getRoomId() {
        return _roomBooking.getRoomId();
    }

    /**
    * Sets the room ID of this room booking.
    *
    * @param roomId the room ID of this room booking
    */
    @Override
    public void setRoomId(long roomId) {
        _roomBooking.setRoomId(roomId);
    }

    /**
    * Returns the week day of this room booking.
    *
    * @return the week day of this room booking
    */
    @Override
    public int getWeekDay() {
        return _roomBooking.getWeekDay();
    }

    /**
    * Sets the week day of this room booking.
    *
    * @param weekDay the week day of this room booking
    */
    @Override
    public void setWeekDay(int weekDay) {
        _roomBooking.setWeekDay(weekDay);
    }

    /**
    * Returns the open hour of this room booking.
    *
    * @return the open hour of this room booking
    */
    @Override
    public int getOpenHour() {
        return _roomBooking.getOpenHour();
    }

    /**
    * Sets the open hour of this room booking.
    *
    * @param openHour the open hour of this room booking
    */
    @Override
    public void setOpenHour(int openHour) {
        _roomBooking.setOpenHour(openHour);
    }

    /**
    * Returns the close hour of this room booking.
    *
    * @return the close hour of this room booking
    */
    @Override
    public int getCloseHour() {
        return _roomBooking.getCloseHour();
    }

    /**
    * Sets the close hour of this room booking.
    *
    * @param closeHour the close hour of this room booking
    */
    @Override
    public void setCloseHour(int closeHour) {
        _roomBooking.setCloseHour(closeHour);
    }

    /**
    * Returns the period of this room booking.
    *
    * @return the period of this room booking
    */
    @Override
    public int getPeriod() {
        return _roomBooking.getPeriod();
    }

    /**
    * Sets the period of this room booking.
    *
    * @param period the period of this room booking
    */
    @Override
    public void setPeriod(int period) {
        _roomBooking.setPeriod(period);
    }

    /**
    * Returns the opr date of this room booking.
    *
    * @return the opr date of this room booking
    */
    @Override
    public java.util.Date getOprDate() {
        return _roomBooking.getOprDate();
    }

    /**
    * Sets the opr date of this room booking.
    *
    * @param oprDate the opr date of this room booking
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _roomBooking.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this room booking.
    *
    * @return the opr user of this room booking
    */
    @Override
    public long getOprUser() {
        return _roomBooking.getOprUser();
    }

    /**
    * Sets the opr user of this room booking.
    *
    * @param oprUser the opr user of this room booking
    */
    @Override
    public void setOprUser(long oprUser) {
        _roomBooking.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _roomBooking.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _roomBooking.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _roomBooking.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _roomBooking.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _roomBooking.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _roomBooking.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _roomBooking.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _roomBooking.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _roomBooking.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _roomBooking.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _roomBooking.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new RoomBookingWrapper((RoomBooking) _roomBooking.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.booking.model.RoomBooking roomBooking) {
        return _roomBooking.compareTo(roomBooking);
    }

    @Override
    public int hashCode() {
        return _roomBooking.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.booking.model.RoomBooking> toCacheModel() {
        return _roomBooking.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBooking toEscapedModel() {
        return new RoomBookingWrapper(_roomBooking.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBooking toUnescapedModel() {
        return new RoomBookingWrapper(_roomBooking.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _roomBooking.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _roomBooking.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _roomBooking.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RoomBookingWrapper)) {
            return false;
        }

        RoomBookingWrapper roomBookingWrapper = (RoomBookingWrapper) obj;

        if (Validator.equals(_roomBooking, roomBookingWrapper._roomBooking)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public RoomBooking getWrappedRoomBooking() {
        return _roomBooking;
    }

    @Override
    public RoomBooking getWrappedModel() {
        return _roomBooking;
    }

    @Override
    public void resetOriginalValues() {
        _roomBooking.resetOriginalValues();
    }
}
