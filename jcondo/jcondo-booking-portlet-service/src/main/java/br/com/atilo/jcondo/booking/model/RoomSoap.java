package br.com.atilo.jcondo.booking.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.booking.service.http.RoomServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.booking.service.http.RoomServiceSoap
 * @generated
 */
public class RoomSoap implements Serializable {
    private long _id;
    private long _folderId;
    private long _agreementId;
    private String _name;
    private String _description;
    private boolean _available;
    private boolean _bookable;
    private double _price;
    private int _capacity;
    private Date _oprDate;
    private long _oprUser;

    public RoomSoap() {
    }

    public static RoomSoap toSoapModel(Room model) {
        RoomSoap soapModel = new RoomSoap();

        soapModel.setId(model.getId());
        soapModel.setFolderId(model.getFolderId());
        soapModel.setAgreementId(model.getAgreementId());
        soapModel.setName(model.getName());
        soapModel.setDescription(model.getDescription());
        soapModel.setAvailable(model.getAvailable());
        soapModel.setBookable(model.getBookable());
        soapModel.setPrice(model.getPrice());
        soapModel.setCapacity(model.getCapacity());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static RoomSoap[] toSoapModels(Room[] models) {
        RoomSoap[] soapModels = new RoomSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static RoomSoap[][] toSoapModels(Room[][] models) {
        RoomSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new RoomSoap[models.length][models[0].length];
        } else {
            soapModels = new RoomSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static RoomSoap[] toSoapModels(List<Room> models) {
        List<RoomSoap> soapModels = new ArrayList<RoomSoap>(models.size());

        for (Room model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new RoomSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getFolderId() {
        return _folderId;
    }

    public void setFolderId(long folderId) {
        _folderId = folderId;
    }

    public long getAgreementId() {
        return _agreementId;
    }

    public void setAgreementId(long agreementId) {
        _agreementId = agreementId;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public boolean getAvailable() {
        return _available;
    }

    public boolean isAvailable() {
        return _available;
    }

    public void setAvailable(boolean available) {
        _available = available;
    }

    public boolean getBookable() {
        return _bookable;
    }

    public boolean isBookable() {
        return _bookable;
    }

    public void setBookable(boolean bookable) {
        _bookable = bookable;
    }

    public double getPrice() {
        return _price;
    }

    public void setPrice(double price) {
        _price = price;
    }

    public int getCapacity() {
        return _capacity;
    }

    public void setCapacity(int capacity) {
        _capacity = capacity;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
