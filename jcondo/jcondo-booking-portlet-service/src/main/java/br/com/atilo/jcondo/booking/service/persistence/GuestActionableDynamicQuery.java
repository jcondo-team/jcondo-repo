package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class GuestActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public GuestActionableDynamicQuery() throws SystemException {
        setBaseLocalService(GuestLocalServiceUtil.getService());
        setClass(Guest.class);

        setClassLoader(br.com.atilo.jcondo.booking.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("guestId");
    }
}
