package br.com.atilo.jcondo.booking.model;

import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class BookingClp extends BaseModelImpl<Booking> implements Booking {
    private long _id;
    private long _roomId;
    private long _domainId;
    private long _personId;
    private Date _beginDate;
    private Date _endDate;
    private int _statusId;
    private long _noteId;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _bookingRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.booking.service.ClpSerializer.class;

    public BookingClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Booking.class;
    }

    @Override
    public String getModelClassName() {
        return Booking.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("roomId", getRoomId());
        attributes.put("domainId", getDomainId());
        attributes.put("personId", getPersonId());
        attributes.put("beginDate", getBeginDate());
        attributes.put("endDate", getEndDate());
        attributes.put("statusId", getStatusId());
        attributes.put("noteId", getNoteId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long roomId = (Long) attributes.get("roomId");

        if (roomId != null) {
            setRoomId(roomId);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Date beginDate = (Date) attributes.get("beginDate");

        if (beginDate != null) {
            setBeginDate(beginDate);
        }

        Date endDate = (Date) attributes.get("endDate");

        if (endDate != null) {
            setEndDate(endDate);
        }

        Integer statusId = (Integer) attributes.get("statusId");

        if (statusId != null) {
            setStatusId(statusId);
        }

        Long noteId = (Long) attributes.get("noteId");

        if (noteId != null) {
            setNoteId(noteId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_bookingRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getRoomId() {
        return _roomId;
    }

    @Override
    public void setRoomId(long roomId) {
        _roomId = roomId;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setRoomId", long.class);

                method.invoke(_bookingRemoteModel, roomId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getDomainId() {
        return _domainId;
    }

    @Override
    public void setDomainId(long domainId) {
        _domainId = domainId;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setDomainId", long.class);

                method.invoke(_bookingRemoteModel, domainId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getPersonId() {
        return _personId;
    }

    @Override
    public void setPersonId(long personId) {
        _personId = personId;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setPersonId", long.class);

                method.invoke(_bookingRemoteModel, personId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getBeginDate() {
        return _beginDate;
    }

    @Override
    public void setBeginDate(Date beginDate) {
        _beginDate = beginDate;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setBeginDate", Date.class);

                method.invoke(_bookingRemoteModel, beginDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getEndDate() {
        return _endDate;
    }

    @Override
    public void setEndDate(Date endDate) {
        _endDate = endDate;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setEndDate", Date.class);

                method.invoke(_bookingRemoteModel, endDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getStatusId() {
        return _statusId;
    }

    @Override
    public void setStatusId(int statusId) {
        _statusId = statusId;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setStatusId", int.class);

                method.invoke(_bookingRemoteModel, statusId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getNoteId() {
        return _noteId;
    }

    @Override
    public void setNoteId(long noteId) {
        _noteId = noteId;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setNoteId", long.class);

                method.invoke(_bookingRemoteModel, noteId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_bookingRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_bookingRemoteModel != null) {
            try {
                Class<?> clazz = _bookingRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_bookingRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public void setNote(br.com.atilo.jcondo.booking.model.BookingNote note) {
        try {
            String methodName = "setNote";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.booking.model.BookingNote.class
                };

            Object[] parameterValues = new Object[] { note };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setRoom(br.com.atilo.jcondo.booking.model.Room room) {
        try {
            String methodName = "setRoom";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.booking.model.Room.class
                };

            Object[] parameterValues = new Object[] { room };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote getNote() {
        try {
            String methodName = "getNote";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.booking.model.BookingNote returnObj = (br.com.atilo.jcondo.booking.model.BookingNote) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setStatus(
        br.com.atilo.jcondo.booking.model.datatype.BookingStatus status) {
        try {
            String methodName = "setStatus";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.booking.model.datatype.BookingStatus.class
                };

            Object[] parameterValues = new Object[] { status };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room getRoom() {
        try {
            String methodName = "getRoom";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.booking.model.Room returnObj = (br.com.atilo.jcondo.booking.model.Room) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson() {
        try {
            String methodName = "getPerson";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.manager.model.Person returnObj = (br.com.atilo.jcondo.manager.model.Person) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice getInvoice() {
        try {
            String methodName = "getInvoice";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.booking.model.BookingInvoice returnObj = (br.com.atilo.jcondo.booking.model.BookingInvoice) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setDomain(com.liferay.portal.model.BaseModel<?> domain) {
        try {
            String methodName = "setDomain";

            Class<?>[] parameterTypes = new Class<?>[] {
                    com.liferay.portal.model.BaseModel.class
                };

            Object[] parameterValues = new Object[] { domain };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.booking.model.datatype.BookingStatus getStatus() {
        try {
            String methodName = "getStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.booking.model.datatype.BookingStatus returnObj = (br.com.atilo.jcondo.booking.model.datatype.BookingStatus) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getDomain() {
        try {
            String methodName = "getDomain";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.liferay.portal.model.BaseModel<?> returnObj = (com.liferay.portal.model.BaseModel<?>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> getGuests() {
        try {
            String methodName = "getGuests";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.booking.model.Guest> returnObj = (java.util.List<br.com.atilo.jcondo.booking.model.Guest>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setPerson(br.com.atilo.jcondo.manager.model.Person person) {
        try {
            String methodName = "setPerson";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.manager.model.Person.class
                };

            Object[] parameterValues = new Object[] { person };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getBookingRemoteModel() {
        return _bookingRemoteModel;
    }

    public void setBookingRemoteModel(BaseModel<?> bookingRemoteModel) {
        _bookingRemoteModel = bookingRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _bookingRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_bookingRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            BookingLocalServiceUtil.addBooking(this);
        } else {
            BookingLocalServiceUtil.updateBooking(this);
        }
    }

    @Override
    public Booking toEscapedModel() {
        return (Booking) ProxyUtil.newProxyInstance(Booking.class.getClassLoader(),
            new Class[] { Booking.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        BookingClp clone = new BookingClp();

        clone.setId(getId());
        clone.setRoomId(getRoomId());
        clone.setDomainId(getDomainId());
        clone.setPersonId(getPersonId());
        clone.setBeginDate(getBeginDate());
        clone.setEndDate(getEndDate());
        clone.setStatusId(getStatusId());
        clone.setNoteId(getNoteId());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Booking booking) {
        int value = 0;

        value = DateUtil.compareTo(getBeginDate(), booking.getBeginDate());

        value = value * -1;

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BookingClp)) {
            return false;
        }

        BookingClp booking = (BookingClp) obj;

        long primaryKey = booking.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", roomId=");
        sb.append(getRoomId());
        sb.append(", domainId=");
        sb.append(getDomainId());
        sb.append(", personId=");
        sb.append(getPersonId());
        sb.append(", beginDate=");
        sb.append(getBeginDate());
        sb.append(", endDate=");
        sb.append(getEndDate());
        sb.append(", statusId=");
        sb.append(getStatusId());
        sb.append(", noteId=");
        sb.append(getNoteId());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(34);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.booking.model.Booking");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>roomId</column-name><column-value><![CDATA[");
        sb.append(getRoomId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>domainId</column-name><column-value><![CDATA[");
        sb.append(getDomainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>personId</column-name><column-value><![CDATA[");
        sb.append(getPersonId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>beginDate</column-name><column-value><![CDATA[");
        sb.append(getBeginDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>endDate</column-name><column-value><![CDATA[");
        sb.append(getEndDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>statusId</column-name><column-value><![CDATA[");
        sb.append(getStatusId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>noteId</column-name><column-value><![CDATA[");
        sb.append(getNoteId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
