package br.com.atilo.jcondo.booking.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.booking.service.http.GuestServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.booking.service.http.GuestServiceSoap
 * @generated
 */
public class GuestSoap implements Serializable {
    private long _guestId;
    private long _bookingId;
    private String _name;
    private String _surname;
    private boolean _checkedIn;
    private Date _oprDate;
    private long _oprUser;

    public GuestSoap() {
    }

    public static GuestSoap toSoapModel(Guest model) {
        GuestSoap soapModel = new GuestSoap();

        soapModel.setGuestId(model.getGuestId());
        soapModel.setBookingId(model.getBookingId());
        soapModel.setName(model.getName());
        soapModel.setSurname(model.getSurname());
        soapModel.setCheckedIn(model.getCheckedIn());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static GuestSoap[] toSoapModels(Guest[] models) {
        GuestSoap[] soapModels = new GuestSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static GuestSoap[][] toSoapModels(Guest[][] models) {
        GuestSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new GuestSoap[models.length][models[0].length];
        } else {
            soapModels = new GuestSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static GuestSoap[] toSoapModels(List<Guest> models) {
        List<GuestSoap> soapModels = new ArrayList<GuestSoap>(models.size());

        for (Guest model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new GuestSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _guestId;
    }

    public void setPrimaryKey(long pk) {
        setGuestId(pk);
    }

    public long getGuestId() {
        return _guestId;
    }

    public void setGuestId(long guestId) {
        _guestId = guestId;
    }

    public long getBookingId() {
        return _bookingId;
    }

    public void setBookingId(long bookingId) {
        _bookingId = bookingId;
    }

    public String getName() {
        return _name;
    }

    public void setName(String name) {
        _name = name;
    }

    public String getSurname() {
        return _surname;
    }

    public void setSurname(String surname) {
        _surname = surname;
    }

    public boolean getCheckedIn() {
        return _checkedIn;
    }

    public boolean isCheckedIn() {
        return _checkedIn;
    }

    public void setCheckedIn(boolean checkedIn) {
        _checkedIn = checkedIn;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
