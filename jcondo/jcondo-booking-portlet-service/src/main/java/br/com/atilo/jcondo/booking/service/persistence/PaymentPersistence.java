package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.Payment;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the payment service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PaymentPersistenceImpl
 * @see PaymentUtil
 * @generated
 */
public interface PaymentPersistence extends BasePersistence<Payment> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link PaymentUtil} to access the payment persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the payments where invoiceId = &#63;.
    *
    * @param invoiceId the invoice ID
    * @return the matching payments
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> findByInvoice(
        long invoiceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the payments where invoiceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param invoiceId the invoice ID
    * @param start the lower bound of the range of payments
    * @param end the upper bound of the range of payments (not inclusive)
    * @return the range of matching payments
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> findByInvoice(
        long invoiceId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the payments where invoiceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param invoiceId the invoice ID
    * @param start the lower bound of the range of payments
    * @param end the upper bound of the range of payments (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching payments
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> findByInvoice(
        long invoiceId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first payment in the ordered set where invoiceId = &#63;.
    *
    * @param invoiceId the invoice ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching payment
    * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a matching payment could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment findByInvoice_First(
        long invoiceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchPaymentException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first payment in the ordered set where invoiceId = &#63;.
    *
    * @param invoiceId the invoice ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching payment, or <code>null</code> if a matching payment could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment fetchByInvoice_First(
        long invoiceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last payment in the ordered set where invoiceId = &#63;.
    *
    * @param invoiceId the invoice ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching payment
    * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a matching payment could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment findByInvoice_Last(
        long invoiceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchPaymentException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last payment in the ordered set where invoiceId = &#63;.
    *
    * @param invoiceId the invoice ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching payment, or <code>null</code> if a matching payment could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment fetchByInvoice_Last(
        long invoiceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the payments before and after the current payment in the ordered set where invoiceId = &#63;.
    *
    * @param id the primary key of the current payment
    * @param invoiceId the invoice ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next payment
    * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a payment with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment[] findByInvoice_PrevAndNext(
        long id, long invoiceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchPaymentException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the payments where invoiceId = &#63; from the database.
    *
    * @param invoiceId the invoice ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByInvoice(long invoiceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of payments where invoiceId = &#63;.
    *
    * @param invoiceId the invoice ID
    * @return the number of matching payments
    * @throws SystemException if a system exception occurred
    */
    public int countByInvoice(long invoiceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the payment where token = &#63; or throws a {@link br.com.atilo.jcondo.booking.NoSuchPaymentException} if it could not be found.
    *
    * @param token the token
    * @return the matching payment
    * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a matching payment could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment findByToken(
        java.lang.String token)
        throws br.com.atilo.jcondo.booking.NoSuchPaymentException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the payment where token = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param token the token
    * @return the matching payment, or <code>null</code> if a matching payment could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment fetchByToken(
        java.lang.String token)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the payment where token = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param token the token
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching payment, or <code>null</code> if a matching payment could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment fetchByToken(
        java.lang.String token, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the payment where token = &#63; from the database.
    *
    * @param token the token
    * @return the payment that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment removeByToken(
        java.lang.String token)
        throws br.com.atilo.jcondo.booking.NoSuchPaymentException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of payments where token = &#63;.
    *
    * @param token the token
    * @return the number of matching payments
    * @throws SystemException if a system exception occurred
    */
    public int countByToken(java.lang.String token)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the payment in the entity cache if it is enabled.
    *
    * @param payment the payment
    */
    public void cacheResult(br.com.atilo.jcondo.booking.model.Payment payment);

    /**
    * Caches the payments in the entity cache if it is enabled.
    *
    * @param payments the payments
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.Payment> payments);

    /**
    * Creates a new payment with the primary key. Does not add the payment to the database.
    *
    * @param id the primary key for the new payment
    * @return the new payment
    */
    public br.com.atilo.jcondo.booking.model.Payment create(long id);

    /**
    * Removes the payment with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the payment
    * @return the payment that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a payment with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment remove(long id)
        throws br.com.atilo.jcondo.booking.NoSuchPaymentException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.booking.model.Payment updateImpl(
        br.com.atilo.jcondo.booking.model.Payment payment)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the payment with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchPaymentException} if it could not be found.
    *
    * @param id the primary key of the payment
    * @return the payment
    * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a payment with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment findByPrimaryKey(long id)
        throws br.com.atilo.jcondo.booking.NoSuchPaymentException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the payment with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the payment
    * @return the payment, or <code>null</code> if a payment with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Payment fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the payments.
    *
    * @return the payments
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the payments.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of payments
    * @param end the upper bound of the range of payments (not inclusive)
    * @return the range of payments
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the payments.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of payments
    * @param end the upper bound of the range of payments (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of payments
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the payments from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of payments.
    *
    * @return the number of payments
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
