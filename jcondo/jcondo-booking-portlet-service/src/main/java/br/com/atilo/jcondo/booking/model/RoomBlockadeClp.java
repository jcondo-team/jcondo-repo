package br.com.atilo.jcondo.booking.model;

import br.com.atilo.jcondo.booking.service.ClpSerializer;
import br.com.atilo.jcondo.booking.service.RoomBlockadeLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class RoomBlockadeClp extends BaseModelImpl<RoomBlockade>
    implements RoomBlockade {
    private long _id;
    private long _roomId;
    private Date _beginDate;
    private Date _endDate;
    private String _description;
    private boolean _recursive;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _roomBlockadeRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.booking.service.ClpSerializer.class;

    public RoomBlockadeClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return RoomBlockade.class;
    }

    @Override
    public String getModelClassName() {
        return RoomBlockade.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("roomId", getRoomId());
        attributes.put("beginDate", getBeginDate());
        attributes.put("endDate", getEndDate());
        attributes.put("description", getDescription());
        attributes.put("recursive", getRecursive());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long roomId = (Long) attributes.get("roomId");

        if (roomId != null) {
            setRoomId(roomId);
        }

        Date beginDate = (Date) attributes.get("beginDate");

        if (beginDate != null) {
            setBeginDate(beginDate);
        }

        Date endDate = (Date) attributes.get("endDate");

        if (endDate != null) {
            setEndDate(endDate);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean recursive = (Boolean) attributes.get("recursive");

        if (recursive != null) {
            setRecursive(recursive);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_roomBlockadeRemoteModel != null) {
            try {
                Class<?> clazz = _roomBlockadeRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_roomBlockadeRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getRoomId() {
        return _roomId;
    }

    @Override
    public void setRoomId(long roomId) {
        _roomId = roomId;

        if (_roomBlockadeRemoteModel != null) {
            try {
                Class<?> clazz = _roomBlockadeRemoteModel.getClass();

                Method method = clazz.getMethod("setRoomId", long.class);

                method.invoke(_roomBlockadeRemoteModel, roomId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getBeginDate() {
        return _beginDate;
    }

    @Override
    public void setBeginDate(Date beginDate) {
        _beginDate = beginDate;

        if (_roomBlockadeRemoteModel != null) {
            try {
                Class<?> clazz = _roomBlockadeRemoteModel.getClass();

                Method method = clazz.getMethod("setBeginDate", Date.class);

                method.invoke(_roomBlockadeRemoteModel, beginDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getEndDate() {
        return _endDate;
    }

    @Override
    public void setEndDate(Date endDate) {
        _endDate = endDate;

        if (_roomBlockadeRemoteModel != null) {
            try {
                Class<?> clazz = _roomBlockadeRemoteModel.getClass();

                Method method = clazz.getMethod("setEndDate", Date.class);

                method.invoke(_roomBlockadeRemoteModel, endDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescription() {
        return _description;
    }

    @Override
    public void setDescription(String description) {
        _description = description;

        if (_roomBlockadeRemoteModel != null) {
            try {
                Class<?> clazz = _roomBlockadeRemoteModel.getClass();

                Method method = clazz.getMethod("setDescription", String.class);

                method.invoke(_roomBlockadeRemoteModel, description);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getRecursive() {
        return _recursive;
    }

    @Override
    public boolean isRecursive() {
        return _recursive;
    }

    @Override
    public void setRecursive(boolean recursive) {
        _recursive = recursive;

        if (_roomBlockadeRemoteModel != null) {
            try {
                Class<?> clazz = _roomBlockadeRemoteModel.getClass();

                Method method = clazz.getMethod("setRecursive", boolean.class);

                method.invoke(_roomBlockadeRemoteModel, recursive);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_roomBlockadeRemoteModel != null) {
            try {
                Class<?> clazz = _roomBlockadeRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_roomBlockadeRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_roomBlockadeRemoteModel != null) {
            try {
                Class<?> clazz = _roomBlockadeRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_roomBlockadeRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public void setRoom(br.com.atilo.jcondo.booking.model.Room room) {
        try {
            String methodName = "setRoom";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.booking.model.Room.class
                };

            Object[] parameterValues = new Object[] { room };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room getRoom() {
        try {
            String methodName = "getRoom";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.booking.model.Room returnObj = (br.com.atilo.jcondo.booking.model.Room) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getRoomBlockadeRemoteModel() {
        return _roomBlockadeRemoteModel;
    }

    public void setRoomBlockadeRemoteModel(BaseModel<?> roomBlockadeRemoteModel) {
        _roomBlockadeRemoteModel = roomBlockadeRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _roomBlockadeRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_roomBlockadeRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            RoomBlockadeLocalServiceUtil.addRoomBlockade(this);
        } else {
            RoomBlockadeLocalServiceUtil.updateRoomBlockade(this);
        }
    }

    @Override
    public RoomBlockade toEscapedModel() {
        return (RoomBlockade) ProxyUtil.newProxyInstance(RoomBlockade.class.getClassLoader(),
            new Class[] { RoomBlockade.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        RoomBlockadeClp clone = new RoomBlockadeClp();

        clone.setId(getId());
        clone.setRoomId(getRoomId());
        clone.setBeginDate(getBeginDate());
        clone.setEndDate(getEndDate());
        clone.setDescription(getDescription());
        clone.setRecursive(getRecursive());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(RoomBlockade roomBlockade) {
        int value = 0;

        value = DateUtil.compareTo(getBeginDate(), roomBlockade.getBeginDate());

        value = value * -1;

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RoomBlockadeClp)) {
            return false;
        }

        RoomBlockadeClp roomBlockade = (RoomBlockadeClp) obj;

        long primaryKey = roomBlockade.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", roomId=");
        sb.append(getRoomId());
        sb.append(", beginDate=");
        sb.append(getBeginDate());
        sb.append(", endDate=");
        sb.append(getEndDate());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", recursive=");
        sb.append(getRecursive());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(28);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.booking.model.RoomBlockade");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>roomId</column-name><column-value><![CDATA[");
        sb.append(getRoomId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>beginDate</column-name><column-value><![CDATA[");
        sb.append(getBeginDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>endDate</column-name><column-value><![CDATA[");
        sb.append(getEndDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>recursive</column-name><column-value><![CDATA[");
        sb.append(getRecursive());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
