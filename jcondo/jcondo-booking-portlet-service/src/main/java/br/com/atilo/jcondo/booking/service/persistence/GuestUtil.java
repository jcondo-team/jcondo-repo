package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.Guest;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the guest service. This utility wraps {@link GuestPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GuestPersistence
 * @see GuestPersistenceImpl
 * @generated
 */
public class GuestUtil {
    private static GuestPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Guest guest) {
        getPersistence().clearCache(guest);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Guest> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Guest> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Guest> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Guest update(Guest guest) throws SystemException {
        return getPersistence().update(guest);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Guest update(Guest guest, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(guest, serviceContext);
    }

    /**
    * Returns all the guests where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @return the matching guests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Guest> findByBooking(
        long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBooking(bookingId);
    }

    /**
    * Returns a range of all the guests where bookingId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookingId the booking ID
    * @param start the lower bound of the range of guests
    * @param end the upper bound of the range of guests (not inclusive)
    * @return the range of matching guests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Guest> findByBooking(
        long bookingId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBooking(bookingId, start, end);
    }

    /**
    * Returns an ordered range of all the guests where bookingId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookingId the booking ID
    * @param start the lower bound of the range of guests
    * @param end the upper bound of the range of guests (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching guests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Guest> findByBooking(
        long bookingId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBooking(bookingId, start, end, orderByComparator);
    }

    /**
    * Returns the first guest in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching guest
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a matching guest could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest findByBooking_First(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBooking_First(bookingId, orderByComparator);
    }

    /**
    * Returns the first guest in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching guest, or <code>null</code> if a matching guest could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest fetchByBooking_First(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByBooking_First(bookingId, orderByComparator);
    }

    /**
    * Returns the last guest in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching guest
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a matching guest could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest findByBooking_Last(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBooking_Last(bookingId, orderByComparator);
    }

    /**
    * Returns the last guest in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching guest, or <code>null</code> if a matching guest could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest fetchByBooking_Last(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByBooking_Last(bookingId, orderByComparator);
    }

    /**
    * Returns the guests before and after the current guest in the ordered set where bookingId = &#63;.
    *
    * @param guestId the primary key of the current guest
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next guest
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest[] findByBooking_PrevAndNext(
        long guestId, long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBooking_PrevAndNext(guestId, bookingId,
            orderByComparator);
    }

    /**
    * Removes all the guests where bookingId = &#63; from the database.
    *
    * @param bookingId the booking ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByBooking(long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByBooking(bookingId);
    }

    /**
    * Returns the number of guests where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @return the number of matching guests
    * @throws SystemException if a system exception occurred
    */
    public static int countByBooking(long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByBooking(bookingId);
    }

    /**
    * Caches the guest in the entity cache if it is enabled.
    *
    * @param guest the guest
    */
    public static void cacheResult(
        br.com.atilo.jcondo.booking.model.Guest guest) {
        getPersistence().cacheResult(guest);
    }

    /**
    * Caches the guests in the entity cache if it is enabled.
    *
    * @param guests the guests
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.Guest> guests) {
        getPersistence().cacheResult(guests);
    }

    /**
    * Creates a new guest with the primary key. Does not add the guest to the database.
    *
    * @param guestId the primary key for the new guest
    * @return the new guest
    */
    public static br.com.atilo.jcondo.booking.model.Guest create(long guestId) {
        return getPersistence().create(guestId);
    }

    /**
    * Removes the guest with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param guestId the primary key of the guest
    * @return the guest that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest remove(long guestId)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(guestId);
    }

    public static br.com.atilo.jcondo.booking.model.Guest updateImpl(
        br.com.atilo.jcondo.booking.model.Guest guest)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(guest);
    }

    /**
    * Returns the guest with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchGuestException} if it could not be found.
    *
    * @param guestId the primary key of the guest
    * @return the guest
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest findByPrimaryKey(
        long guestId)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(guestId);
    }

    /**
    * Returns the guest with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param guestId the primary key of the guest
    * @return the guest, or <code>null</code> if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest fetchByPrimaryKey(
        long guestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(guestId);
    }

    /**
    * Returns all the guests.
    *
    * @return the guests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Guest> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the guests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of guests
    * @param end the upper bound of the range of guests (not inclusive)
    * @return the range of guests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Guest> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the guests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of guests
    * @param end the upper bound of the range of guests (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of guests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Guest> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the guests from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of guests.
    *
    * @return the number of guests
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static GuestPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (GuestPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.booking.service.ClpSerializer.getServletContextName(),
                    GuestPersistence.class.getName());

            ReferenceRegistry.registerReference(GuestUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(GuestPersistence persistence) {
    }
}
