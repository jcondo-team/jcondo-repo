package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for RoomBlockade. This utility wraps
 * {@link br.com.atilo.jcondo.booking.service.impl.RoomBlockadeServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see RoomBlockadeService
 * @see br.com.atilo.jcondo.booking.service.base.RoomBlockadeServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.impl.RoomBlockadeServiceImpl
 * @generated
 */
public class RoomBlockadeServiceUtil {
    private static RoomBlockadeService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.booking.service.impl.RoomBlockadeServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.booking.model.RoomBlockade addRoomBlockade(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addRoomBlockade(roomBlockade);
    }

    public static br.com.atilo.jcondo.booking.model.RoomBlockade deleteRoomBlockade(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteRoomBlockade(roomBlockade);
    }

    public static void clearService() {
        _service = null;
    }

    public static RoomBlockadeService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    RoomBlockadeService.class.getName());

            if (invokableService instanceof RoomBlockadeService) {
                _service = (RoomBlockadeService) invokableService;
            } else {
                _service = new RoomBlockadeServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(RoomBlockadeServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(RoomBlockadeService service) {
    }
}
