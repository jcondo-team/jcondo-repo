package br.com.atilo.jcondo.booking.model;

import br.com.atilo.jcondo.booking.service.ClpSerializer;
import br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class RoomClp extends BaseModelImpl<Room> implements Room {
    private long _id;
    private long _folderId;
    private long _agreementId;
    private String _name;
    private String _description;
    private boolean _available;
    private boolean _bookable;
    private double _price;
    private int _capacity;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _roomRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.booking.service.ClpSerializer.class;

    public RoomClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Room.class;
    }

    @Override
    public String getModelClassName() {
        return Room.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("folderId", getFolderId());
        attributes.put("agreementId", getAgreementId());
        attributes.put("name", getName());
        attributes.put("description", getDescription());
        attributes.put("available", getAvailable());
        attributes.put("bookable", getBookable());
        attributes.put("price", getPrice());
        attributes.put("capacity", getCapacity());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long folderId = (Long) attributes.get("folderId");

        if (folderId != null) {
            setFolderId(folderId);
        }

        Long agreementId = (Long) attributes.get("agreementId");

        if (agreementId != null) {
            setAgreementId(agreementId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean available = (Boolean) attributes.get("available");

        if (available != null) {
            setAvailable(available);
        }

        Boolean bookable = (Boolean) attributes.get("bookable");

        if (bookable != null) {
            setBookable(bookable);
        }

        Double price = (Double) attributes.get("price");

        if (price != null) {
            setPrice(price);
        }

        Integer capacity = (Integer) attributes.get("capacity");

        if (capacity != null) {
            setCapacity(capacity);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_roomRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getFolderId() {
        return _folderId;
    }

    @Override
    public void setFolderId(long folderId) {
        _folderId = folderId;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setFolderId", long.class);

                method.invoke(_roomRemoteModel, folderId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getAgreementId() {
        return _agreementId;
    }

    @Override
    public void setAgreementId(long agreementId) {
        _agreementId = agreementId;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setAgreementId", long.class);

                method.invoke(_roomRemoteModel, agreementId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setName(String name) {
        _name = name;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_roomRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getDescription() {
        return _description;
    }

    @Override
    public void setDescription(String description) {
        _description = description;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setDescription", String.class);

                method.invoke(_roomRemoteModel, description);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getAvailable() {
        return _available;
    }

    @Override
    public boolean isAvailable() {
        return _available;
    }

    @Override
    public void setAvailable(boolean available) {
        _available = available;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setAvailable", boolean.class);

                method.invoke(_roomRemoteModel, available);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getBookable() {
        return _bookable;
    }

    @Override
    public boolean isBookable() {
        return _bookable;
    }

    @Override
    public void setBookable(boolean bookable) {
        _bookable = bookable;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setBookable", boolean.class);

                method.invoke(_roomRemoteModel, bookable);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getPrice() {
        return _price;
    }

    @Override
    public void setPrice(double price) {
        _price = price;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setPrice", double.class);

                method.invoke(_roomRemoteModel, price);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getCapacity() {
        return _capacity;
    }

    @Override
    public void setCapacity(int capacity) {
        _capacity = capacity;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setCapacity", int.class);

                method.invoke(_roomRemoteModel, capacity);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_roomRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_roomRemoteModel != null) {
            try {
                Class<?> clazz = _roomRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_roomRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.Image> getImages() {
        try {
            String methodName = "getImages";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.Image> returnObj = (java.util.List<br.com.atilo.jcondo.Image>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> getRoomBlockades() {
        try {
            String methodName = "getRoomBlockades";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> returnObj =
                (java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.lang.String getAgreementURL() {
        try {
            String methodName = "getAgreementURL";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.lang.String getAgreementName() {
        try {
            String methodName = "getAgreementName";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> getRoomBookings() {
        try {
            String methodName = "getRoomBookings";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> returnObj =
                (java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getRoomRemoteModel() {
        return _roomRemoteModel;
    }

    public void setRoomRemoteModel(BaseModel<?> roomRemoteModel) {
        _roomRemoteModel = roomRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _roomRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_roomRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            RoomLocalServiceUtil.addRoom(this);
        } else {
            RoomLocalServiceUtil.updateRoom(this);
        }
    }

    @Override
    public Room toEscapedModel() {
        return (Room) ProxyUtil.newProxyInstance(Room.class.getClassLoader(),
            new Class[] { Room.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        RoomClp clone = new RoomClp();

        clone.setId(getId());
        clone.setFolderId(getFolderId());
        clone.setAgreementId(getAgreementId());
        clone.setName(getName());
        clone.setDescription(getDescription());
        clone.setAvailable(getAvailable());
        clone.setBookable(getBookable());
        clone.setPrice(getPrice());
        clone.setCapacity(getCapacity());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Room room) {
        int value = 0;

        if (getId() < room.getId()) {
            value = -1;
        } else if (getId() > room.getId()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RoomClp)) {
            return false;
        }

        RoomClp room = (RoomClp) obj;

        long primaryKey = room.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", folderId=");
        sb.append(getFolderId());
        sb.append(", agreementId=");
        sb.append(getAgreementId());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", description=");
        sb.append(getDescription());
        sb.append(", available=");
        sb.append(getAvailable());
        sb.append(", bookable=");
        sb.append(getBookable());
        sb.append(", price=");
        sb.append(getPrice());
        sb.append(", capacity=");
        sb.append(getCapacity());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(37);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.booking.model.Room");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>folderId</column-name><column-value><![CDATA[");
        sb.append(getFolderId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>agreementId</column-name><column-value><![CDATA[");
        sb.append(getAgreementId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>description</column-name><column-value><![CDATA[");
        sb.append(getDescription());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>available</column-name><column-value><![CDATA[");
        sb.append(getAvailable());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>bookable</column-name><column-value><![CDATA[");
        sb.append(getBookable());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>price</column-name><column-value><![CDATA[");
        sb.append(getPrice());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>capacity</column-name><column-value><![CDATA[");
        sb.append(getCapacity());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
