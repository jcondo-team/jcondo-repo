package br.com.atilo.jcondo.booking.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.booking.service.http.BookingInvoiceServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.booking.service.http.BookingInvoiceServiceSoap
 * @generated
 */
public class BookingInvoiceSoap implements Serializable {
    private long _id;
    private long _bookingId;
    private long _providerId;
    private String _code;
    private double _amount;
    private double _tax;
    private Date _date;
    private String _link;
    private Date _oprDate;
    private long _oprUser;

    public BookingInvoiceSoap() {
    }

    public static BookingInvoiceSoap toSoapModel(BookingInvoice model) {
        BookingInvoiceSoap soapModel = new BookingInvoiceSoap();

        soapModel.setId(model.getId());
        soapModel.setBookingId(model.getBookingId());
        soapModel.setProviderId(model.getProviderId());
        soapModel.setCode(model.getCode());
        soapModel.setAmount(model.getAmount());
        soapModel.setTax(model.getTax());
        soapModel.setDate(model.getDate());
        soapModel.setLink(model.getLink());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static BookingInvoiceSoap[] toSoapModels(BookingInvoice[] models) {
        BookingInvoiceSoap[] soapModels = new BookingInvoiceSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static BookingInvoiceSoap[][] toSoapModels(BookingInvoice[][] models) {
        BookingInvoiceSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new BookingInvoiceSoap[models.length][models[0].length];
        } else {
            soapModels = new BookingInvoiceSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static BookingInvoiceSoap[] toSoapModels(List<BookingInvoice> models) {
        List<BookingInvoiceSoap> soapModels = new ArrayList<BookingInvoiceSoap>(models.size());

        for (BookingInvoice model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new BookingInvoiceSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getBookingId() {
        return _bookingId;
    }

    public void setBookingId(long bookingId) {
        _bookingId = bookingId;
    }

    public long getProviderId() {
        return _providerId;
    }

    public void setProviderId(long providerId) {
        _providerId = providerId;
    }

    public String getCode() {
        return _code;
    }

    public void setCode(String code) {
        _code = code;
    }

    public double getAmount() {
        return _amount;
    }

    public void setAmount(double amount) {
        _amount = amount;
    }

    public double getTax() {
        return _tax;
    }

    public void setTax(double tax) {
        _tax = tax;
    }

    public Date getDate() {
        return _date;
    }

    public void setDate(Date date) {
        _date = date;
    }

    public String getLink() {
        return _link;
    }

    public void setLink(String link) {
        _link = link;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
