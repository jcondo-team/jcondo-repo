package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Guest}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Guest
 * @generated
 */
public class GuestWrapper implements Guest, ModelWrapper<Guest> {
    private Guest _guest;

    public GuestWrapper(Guest guest) {
        _guest = guest;
    }

    @Override
    public Class<?> getModelClass() {
        return Guest.class;
    }

    @Override
    public String getModelClassName() {
        return Guest.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("guestId", getGuestId());
        attributes.put("bookingId", getBookingId());
        attributes.put("name", getName());
        attributes.put("surname", getSurname());
        attributes.put("checkedIn", getCheckedIn());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long guestId = (Long) attributes.get("guestId");

        if (guestId != null) {
            setGuestId(guestId);
        }

        Long bookingId = (Long) attributes.get("bookingId");

        if (bookingId != null) {
            setBookingId(bookingId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String surname = (String) attributes.get("surname");

        if (surname != null) {
            setSurname(surname);
        }

        Boolean checkedIn = (Boolean) attributes.get("checkedIn");

        if (checkedIn != null) {
            setCheckedIn(checkedIn);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this guest.
    *
    * @return the primary key of this guest
    */
    @Override
    public long getPrimaryKey() {
        return _guest.getPrimaryKey();
    }

    /**
    * Sets the primary key of this guest.
    *
    * @param primaryKey the primary key of this guest
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _guest.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the guest ID of this guest.
    *
    * @return the guest ID of this guest
    */
    @Override
    public long getGuestId() {
        return _guest.getGuestId();
    }

    /**
    * Sets the guest ID of this guest.
    *
    * @param guestId the guest ID of this guest
    */
    @Override
    public void setGuestId(long guestId) {
        _guest.setGuestId(guestId);
    }

    /**
    * Returns the booking ID of this guest.
    *
    * @return the booking ID of this guest
    */
    @Override
    public long getBookingId() {
        return _guest.getBookingId();
    }

    /**
    * Sets the booking ID of this guest.
    *
    * @param bookingId the booking ID of this guest
    */
    @Override
    public void setBookingId(long bookingId) {
        _guest.setBookingId(bookingId);
    }

    /**
    * Returns the name of this guest.
    *
    * @return the name of this guest
    */
    @Override
    public java.lang.String getName() {
        return _guest.getName();
    }

    /**
    * Sets the name of this guest.
    *
    * @param name the name of this guest
    */
    @Override
    public void setName(java.lang.String name) {
        _guest.setName(name);
    }

    /**
    * Returns the surname of this guest.
    *
    * @return the surname of this guest
    */
    @Override
    public java.lang.String getSurname() {
        return _guest.getSurname();
    }

    /**
    * Sets the surname of this guest.
    *
    * @param surname the surname of this guest
    */
    @Override
    public void setSurname(java.lang.String surname) {
        _guest.setSurname(surname);
    }

    /**
    * Returns the checked in of this guest.
    *
    * @return the checked in of this guest
    */
    @Override
    public boolean getCheckedIn() {
        return _guest.getCheckedIn();
    }

    /**
    * Returns <code>true</code> if this guest is checked in.
    *
    * @return <code>true</code> if this guest is checked in; <code>false</code> otherwise
    */
    @Override
    public boolean isCheckedIn() {
        return _guest.isCheckedIn();
    }

    /**
    * Sets whether this guest is checked in.
    *
    * @param checkedIn the checked in of this guest
    */
    @Override
    public void setCheckedIn(boolean checkedIn) {
        _guest.setCheckedIn(checkedIn);
    }

    /**
    * Returns the opr date of this guest.
    *
    * @return the opr date of this guest
    */
    @Override
    public java.util.Date getOprDate() {
        return _guest.getOprDate();
    }

    /**
    * Sets the opr date of this guest.
    *
    * @param oprDate the opr date of this guest
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _guest.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this guest.
    *
    * @return the opr user of this guest
    */
    @Override
    public long getOprUser() {
        return _guest.getOprUser();
    }

    /**
    * Sets the opr user of this guest.
    *
    * @param oprUser the opr user of this guest
    */
    @Override
    public void setOprUser(long oprUser) {
        _guest.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _guest.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _guest.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _guest.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _guest.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _guest.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _guest.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _guest.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _guest.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _guest.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _guest.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _guest.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new GuestWrapper((Guest) _guest.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.booking.model.Guest guest) {
        return _guest.compareTo(guest);
    }

    @Override
    public int hashCode() {
        return _guest.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.booking.model.Guest> toCacheModel() {
        return _guest.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Guest toEscapedModel() {
        return new GuestWrapper(_guest.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Guest toUnescapedModel() {
        return new GuestWrapper(_guest.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _guest.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _guest.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _guest.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof GuestWrapper)) {
            return false;
        }

        GuestWrapper guestWrapper = (GuestWrapper) obj;

        if (Validator.equals(_guest, guestWrapper._guest)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Guest getWrappedGuest() {
        return _guest;
    }

    @Override
    public Guest getWrappedModel() {
        return _guest;
    }

    @Override
    public void resetOriginalValues() {
        _guest.resetOriginalValues();
    }
}
