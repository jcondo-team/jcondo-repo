package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.BookingNote;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the booking note service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingNotePersistenceImpl
 * @see BookingNoteUtil
 * @generated
 */
public interface BookingNotePersistence extends BasePersistence<BookingNote> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link BookingNoteUtil} to access the booking note persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the booking note in the entity cache if it is enabled.
    *
    * @param bookingNote the booking note
    */
    public void cacheResult(
        br.com.atilo.jcondo.booking.model.BookingNote bookingNote);

    /**
    * Caches the booking notes in the entity cache if it is enabled.
    *
    * @param bookingNotes the booking notes
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.BookingNote> bookingNotes);

    /**
    * Creates a new booking note with the primary key. Does not add the booking note to the database.
    *
    * @param id the primary key for the new booking note
    * @return the new booking note
    */
    public br.com.atilo.jcondo.booking.model.BookingNote create(long id);

    /**
    * Removes the booking note with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the booking note
    * @return the booking note that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingNoteException if a booking note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingNote remove(long id)
        throws br.com.atilo.jcondo.booking.NoSuchBookingNoteException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.booking.model.BookingNote updateImpl(
        br.com.atilo.jcondo.booking.model.BookingNote bookingNote)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the booking note with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingNoteException} if it could not be found.
    *
    * @param id the primary key of the booking note
    * @return the booking note
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingNoteException if a booking note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingNote findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchBookingNoteException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the booking note with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the booking note
    * @return the booking note, or <code>null</code> if a booking note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingNote fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the booking notes.
    *
    * @return the booking notes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingNote> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the booking notes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of booking notes
    * @param end the upper bound of the range of booking notes (not inclusive)
    * @return the range of booking notes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingNote> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the booking notes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of booking notes
    * @param end the upper bound of the range of booking notes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of booking notes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingNote> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the booking notes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of booking notes.
    *
    * @return the number of booking notes
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
