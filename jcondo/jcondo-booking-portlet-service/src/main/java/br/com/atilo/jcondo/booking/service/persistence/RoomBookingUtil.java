package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.RoomBooking;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the room booking service. This utility wraps {@link RoomBookingPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomBookingPersistence
 * @see RoomBookingPersistenceImpl
 * @generated
 */
public class RoomBookingUtil {
    private static RoomBookingPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(RoomBooking roomBooking) {
        getPersistence().clearCache(roomBooking);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<RoomBooking> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<RoomBooking> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<RoomBooking> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static RoomBooking update(RoomBooking roomBooking)
        throws SystemException {
        return getPersistence().update(roomBooking);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static RoomBooking update(RoomBooking roomBooking,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(roomBooking, serviceContext);
    }

    /**
    * Returns all the room bookings where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @return the matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoomAndWeekDay(
        long roomId, int weekDay)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoomAndWeekDay(roomId, weekDay);
    }

    /**
    * Returns a range of all the room bookings where roomId = &#63; and weekDay = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @return the range of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoomAndWeekDay(
        long roomId, int weekDay, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoomAndWeekDay(roomId, weekDay, start, end);
    }

    /**
    * Returns an ordered range of all the room bookings where roomId = &#63; and weekDay = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoomAndWeekDay(
        long roomId, int weekDay, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRoomAndWeekDay(roomId, weekDay, start, end,
            orderByComparator);
    }

    /**
    * Returns the first room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking findByRoomAndWeekDay_First(
        long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRoomAndWeekDay_First(roomId, weekDay,
            orderByComparator);
    }

    /**
    * Returns the first room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room booking, or <code>null</code> if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking fetchByRoomAndWeekDay_First(
        long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByRoomAndWeekDay_First(roomId, weekDay,
            orderByComparator);
    }

    /**
    * Returns the last room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking findByRoomAndWeekDay_Last(
        long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRoomAndWeekDay_Last(roomId, weekDay, orderByComparator);
    }

    /**
    * Returns the last room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room booking, or <code>null</code> if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking fetchByRoomAndWeekDay_Last(
        long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByRoomAndWeekDay_Last(roomId, weekDay,
            orderByComparator);
    }

    /**
    * Returns the room bookings before and after the current room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param id the primary key of the current room booking
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking[] findByRoomAndWeekDay_PrevAndNext(
        long id, long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRoomAndWeekDay_PrevAndNext(id, roomId, weekDay,
            orderByComparator);
    }

    /**
    * Removes all the room bookings where roomId = &#63; and weekDay = &#63; from the database.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @throws SystemException if a system exception occurred
    */
    public static void removeByRoomAndWeekDay(long roomId, int weekDay)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByRoomAndWeekDay(roomId, weekDay);
    }

    /**
    * Returns the number of room bookings where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @return the number of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public static int countByRoomAndWeekDay(long roomId, int weekDay)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByRoomAndWeekDay(roomId, weekDay);
    }

    /**
    * Returns all the room bookings where roomId = &#63;.
    *
    * @param roomId the room ID
    * @return the matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoom(
        long roomId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom(roomId);
    }

    /**
    * Returns a range of all the room bookings where roomId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @return the range of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoom(
        long roomId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom(roomId, start, end);
    }

    /**
    * Returns an ordered range of all the room bookings where roomId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoom(
        long roomId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom(roomId, start, end, orderByComparator);
    }

    /**
    * Returns the first room booking in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking findByRoom_First(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom_First(roomId, orderByComparator);
    }

    /**
    * Returns the first room booking in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room booking, or <code>null</code> if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking fetchByRoom_First(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByRoom_First(roomId, orderByComparator);
    }

    /**
    * Returns the last room booking in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking findByRoom_Last(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom_Last(roomId, orderByComparator);
    }

    /**
    * Returns the last room booking in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room booking, or <code>null</code> if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking fetchByRoom_Last(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByRoom_Last(roomId, orderByComparator);
    }

    /**
    * Returns the room bookings before and after the current room booking in the ordered set where roomId = &#63;.
    *
    * @param id the primary key of the current room booking
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking[] findByRoom_PrevAndNext(
        long id, long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRoom_PrevAndNext(id, roomId, orderByComparator);
    }

    /**
    * Removes all the room bookings where roomId = &#63; from the database.
    *
    * @param roomId the room ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByRoom(long roomId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByRoom(roomId);
    }

    /**
    * Returns the number of room bookings where roomId = &#63;.
    *
    * @param roomId the room ID
    * @return the number of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public static int countByRoom(long roomId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByRoom(roomId);
    }

    /**
    * Caches the room booking in the entity cache if it is enabled.
    *
    * @param roomBooking the room booking
    */
    public static void cacheResult(
        br.com.atilo.jcondo.booking.model.RoomBooking roomBooking) {
        getPersistence().cacheResult(roomBooking);
    }

    /**
    * Caches the room bookings in the entity cache if it is enabled.
    *
    * @param roomBookings the room bookings
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> roomBookings) {
        getPersistence().cacheResult(roomBookings);
    }

    /**
    * Creates a new room booking with the primary key. Does not add the room booking to the database.
    *
    * @param id the primary key for the new room booking
    * @return the new room booking
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the room booking with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the room booking
    * @return the room booking that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking remove(long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.booking.model.RoomBooking updateImpl(
        br.com.atilo.jcondo.booking.model.RoomBooking roomBooking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(roomBooking);
    }

    /**
    * Returns the room booking with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchRoomBookingException} if it could not be found.
    *
    * @param id the primary key of the room booking
    * @return the room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the room booking with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the room booking
    * @return the room booking, or <code>null</code> if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBooking fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the room bookings.
    *
    * @return the room bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the room bookings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @return the range of room bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the room bookings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of room bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the room bookings from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of room bookings.
    *
    * @return the number of room bookings
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static RoomBookingPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (RoomBookingPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.booking.service.ClpSerializer.getServletContextName(),
                    RoomBookingPersistence.class.getName());

            ReferenceRegistry.registerReference(RoomBookingUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(RoomBookingPersistence persistence) {
    }
}
