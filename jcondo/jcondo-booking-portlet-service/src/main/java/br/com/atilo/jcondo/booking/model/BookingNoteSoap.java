package br.com.atilo.jcondo.booking.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.booking.service.http.BookingNoteServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.booking.service.http.BookingNoteServiceSoap
 * @generated
 */
public class BookingNoteSoap implements Serializable {
    private long _id;
    private String _text;
    private Date _oprDate;
    private long _oprUser;

    public BookingNoteSoap() {
    }

    public static BookingNoteSoap toSoapModel(BookingNote model) {
        BookingNoteSoap soapModel = new BookingNoteSoap();

        soapModel.setId(model.getId());
        soapModel.setText(model.getText());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static BookingNoteSoap[] toSoapModels(BookingNote[] models) {
        BookingNoteSoap[] soapModels = new BookingNoteSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static BookingNoteSoap[][] toSoapModels(BookingNote[][] models) {
        BookingNoteSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new BookingNoteSoap[models.length][models[0].length];
        } else {
            soapModels = new BookingNoteSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static BookingNoteSoap[] toSoapModels(List<BookingNote> models) {
        List<BookingNoteSoap> soapModels = new ArrayList<BookingNoteSoap>(models.size());

        for (BookingNote model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new BookingNoteSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public String getText() {
        return _text;
    }

    public void setText(String text) {
        _text = text;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
