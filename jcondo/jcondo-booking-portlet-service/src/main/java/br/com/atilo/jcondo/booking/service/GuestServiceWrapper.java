package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link GuestService}.
 *
 * @author Brian Wing Shun Chan
 * @see GuestService
 * @generated
 */
public class GuestServiceWrapper implements GuestService,
    ServiceWrapper<GuestService> {
    private GuestService _guestService;

    public GuestServiceWrapper(GuestService guestService) {
        _guestService = guestService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _guestService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _guestService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _guestService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Guest addGuest(long bookingId,
        java.lang.String name, java.lang.String surname)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _guestService.addGuest(bookingId, name, surname);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Guest updateGuest(long guestId,
        long bookingId, java.lang.String name, java.lang.String surname,
        boolean checkedIn)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _guestService.updateGuest(guestId, bookingId, name, surname,
            checkedIn);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Guest deleteGuest(long guestId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _guestService.deleteGuest(guestId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> getBookingGuests(
        long bookingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _guestService.getBookingGuests(bookingId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public GuestService getWrappedGuestService() {
        return _guestService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedGuestService(GuestService guestService) {
        _guestService = guestService;
    }

    @Override
    public GuestService getWrappedService() {
        return _guestService;
    }

    @Override
    public void setWrappedService(GuestService guestService) {
        _guestService = guestService;
    }
}
