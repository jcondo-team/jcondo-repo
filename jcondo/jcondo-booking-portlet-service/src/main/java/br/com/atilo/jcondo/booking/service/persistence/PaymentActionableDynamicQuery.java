package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.Payment;
import br.com.atilo.jcondo.booking.service.PaymentLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class PaymentActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PaymentActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PaymentLocalServiceUtil.getService());
        setClass(Payment.class);

        setClassLoader(br.com.atilo.jcondo.booking.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
