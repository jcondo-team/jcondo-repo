package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BookingNote}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingNote
 * @generated
 */
public class BookingNoteWrapper implements BookingNote,
    ModelWrapper<BookingNote> {
    private BookingNote _bookingNote;

    public BookingNoteWrapper(BookingNote bookingNote) {
        _bookingNote = bookingNote;
    }

    @Override
    public Class<?> getModelClass() {
        return BookingNote.class;
    }

    @Override
    public String getModelClassName() {
        return BookingNote.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("text", getText());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String text = (String) attributes.get("text");

        if (text != null) {
            setText(text);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this booking note.
    *
    * @return the primary key of this booking note
    */
    @Override
    public long getPrimaryKey() {
        return _bookingNote.getPrimaryKey();
    }

    /**
    * Sets the primary key of this booking note.
    *
    * @param primaryKey the primary key of this booking note
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _bookingNote.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this booking note.
    *
    * @return the ID of this booking note
    */
    @Override
    public long getId() {
        return _bookingNote.getId();
    }

    /**
    * Sets the ID of this booking note.
    *
    * @param id the ID of this booking note
    */
    @Override
    public void setId(long id) {
        _bookingNote.setId(id);
    }

    /**
    * Returns the text of this booking note.
    *
    * @return the text of this booking note
    */
    @Override
    public java.lang.String getText() {
        return _bookingNote.getText();
    }

    /**
    * Sets the text of this booking note.
    *
    * @param text the text of this booking note
    */
    @Override
    public void setText(java.lang.String text) {
        _bookingNote.setText(text);
    }

    /**
    * Returns the opr date of this booking note.
    *
    * @return the opr date of this booking note
    */
    @Override
    public java.util.Date getOprDate() {
        return _bookingNote.getOprDate();
    }

    /**
    * Sets the opr date of this booking note.
    *
    * @param oprDate the opr date of this booking note
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _bookingNote.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this booking note.
    *
    * @return the opr user of this booking note
    */
    @Override
    public long getOprUser() {
        return _bookingNote.getOprUser();
    }

    /**
    * Sets the opr user of this booking note.
    *
    * @param oprUser the opr user of this booking note
    */
    @Override
    public void setOprUser(long oprUser) {
        _bookingNote.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _bookingNote.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _bookingNote.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _bookingNote.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _bookingNote.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _bookingNote.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _bookingNote.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _bookingNote.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _bookingNote.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _bookingNote.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _bookingNote.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _bookingNote.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new BookingNoteWrapper((BookingNote) _bookingNote.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.booking.model.BookingNote bookingNote) {
        return _bookingNote.compareTo(bookingNote);
    }

    @Override
    public int hashCode() {
        return _bookingNote.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.booking.model.BookingNote> toCacheModel() {
        return _bookingNote.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote toEscapedModel() {
        return new BookingNoteWrapper(_bookingNote.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote toUnescapedModel() {
        return new BookingNoteWrapper(_bookingNote.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _bookingNote.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _bookingNote.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _bookingNote.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BookingNoteWrapper)) {
            return false;
        }

        BookingNoteWrapper bookingNoteWrapper = (BookingNoteWrapper) obj;

        if (Validator.equals(_bookingNote, bookingNoteWrapper._bookingNote)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public BookingNote getWrappedBookingNote() {
        return _bookingNote;
    }

    @Override
    public BookingNote getWrappedModel() {
        return _bookingNote;
    }

    @Override
    public void resetOriginalValues() {
        _bookingNote.resetOriginalValues();
    }
}
