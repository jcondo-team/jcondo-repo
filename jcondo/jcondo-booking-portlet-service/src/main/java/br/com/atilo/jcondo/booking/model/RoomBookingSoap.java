package br.com.atilo.jcondo.booking.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.booking.service.http.RoomBookingServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.booking.service.http.RoomBookingServiceSoap
 * @generated
 */
public class RoomBookingSoap implements Serializable {
    private long _id;
    private long _roomId;
    private int _weekDay;
    private int _openHour;
    private int _closeHour;
    private int _period;
    private Date _oprDate;
    private long _oprUser;

    public RoomBookingSoap() {
    }

    public static RoomBookingSoap toSoapModel(RoomBooking model) {
        RoomBookingSoap soapModel = new RoomBookingSoap();

        soapModel.setId(model.getId());
        soapModel.setRoomId(model.getRoomId());
        soapModel.setWeekDay(model.getWeekDay());
        soapModel.setOpenHour(model.getOpenHour());
        soapModel.setCloseHour(model.getCloseHour());
        soapModel.setPeriod(model.getPeriod());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static RoomBookingSoap[] toSoapModels(RoomBooking[] models) {
        RoomBookingSoap[] soapModels = new RoomBookingSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static RoomBookingSoap[][] toSoapModels(RoomBooking[][] models) {
        RoomBookingSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new RoomBookingSoap[models.length][models[0].length];
        } else {
            soapModels = new RoomBookingSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static RoomBookingSoap[] toSoapModels(List<RoomBooking> models) {
        List<RoomBookingSoap> soapModels = new ArrayList<RoomBookingSoap>(models.size());

        for (RoomBooking model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new RoomBookingSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getRoomId() {
        return _roomId;
    }

    public void setRoomId(long roomId) {
        _roomId = roomId;
    }

    public int getWeekDay() {
        return _weekDay;
    }

    public void setWeekDay(int weekDay) {
        _weekDay = weekDay;
    }

    public int getOpenHour() {
        return _openHour;
    }

    public void setOpenHour(int openHour) {
        _openHour = openHour;
    }

    public int getCloseHour() {
        return _closeHour;
    }

    public void setCloseHour(int closeHour) {
        _closeHour = closeHour;
    }

    public int getPeriod() {
        return _period;
    }

    public void setPeriod(int period) {
        _period = period;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
