package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the BookingNote service. Represents a row in the &quot;jco_booking_note&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see BookingNoteModel
 * @see br.com.atilo.jcondo.booking.model.impl.BookingNoteImpl
 * @see br.com.atilo.jcondo.booking.model.impl.BookingNoteModelImpl
 * @generated
 */
public interface BookingNote extends BookingNoteModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.booking.model.impl.BookingNoteImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
