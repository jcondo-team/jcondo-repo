package br.com.atilo.jcondo.booking.model;

import br.com.atilo.jcondo.booking.service.ClpSerializer;
import br.com.atilo.jcondo.booking.service.RoomBookingLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class RoomBookingClp extends BaseModelImpl<RoomBooking>
    implements RoomBooking {
    private long _id;
    private long _roomId;
    private int _weekDay;
    private int _openHour;
    private int _closeHour;
    private int _period;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _roomBookingRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.booking.service.ClpSerializer.class;

    public RoomBookingClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return RoomBooking.class;
    }

    @Override
    public String getModelClassName() {
        return RoomBooking.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("roomId", getRoomId());
        attributes.put("weekDay", getWeekDay());
        attributes.put("openHour", getOpenHour());
        attributes.put("closeHour", getCloseHour());
        attributes.put("period", getPeriod());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long roomId = (Long) attributes.get("roomId");

        if (roomId != null) {
            setRoomId(roomId);
        }

        Integer weekDay = (Integer) attributes.get("weekDay");

        if (weekDay != null) {
            setWeekDay(weekDay);
        }

        Integer openHour = (Integer) attributes.get("openHour");

        if (openHour != null) {
            setOpenHour(openHour);
        }

        Integer closeHour = (Integer) attributes.get("closeHour");

        if (closeHour != null) {
            setCloseHour(closeHour);
        }

        Integer period = (Integer) attributes.get("period");

        if (period != null) {
            setPeriod(period);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_roomBookingRemoteModel != null) {
            try {
                Class<?> clazz = _roomBookingRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_roomBookingRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getRoomId() {
        return _roomId;
    }

    @Override
    public void setRoomId(long roomId) {
        _roomId = roomId;

        if (_roomBookingRemoteModel != null) {
            try {
                Class<?> clazz = _roomBookingRemoteModel.getClass();

                Method method = clazz.getMethod("setRoomId", long.class);

                method.invoke(_roomBookingRemoteModel, roomId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getWeekDay() {
        return _weekDay;
    }

    @Override
    public void setWeekDay(int weekDay) {
        _weekDay = weekDay;

        if (_roomBookingRemoteModel != null) {
            try {
                Class<?> clazz = _roomBookingRemoteModel.getClass();

                Method method = clazz.getMethod("setWeekDay", int.class);

                method.invoke(_roomBookingRemoteModel, weekDay);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getOpenHour() {
        return _openHour;
    }

    @Override
    public void setOpenHour(int openHour) {
        _openHour = openHour;

        if (_roomBookingRemoteModel != null) {
            try {
                Class<?> clazz = _roomBookingRemoteModel.getClass();

                Method method = clazz.getMethod("setOpenHour", int.class);

                method.invoke(_roomBookingRemoteModel, openHour);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getCloseHour() {
        return _closeHour;
    }

    @Override
    public void setCloseHour(int closeHour) {
        _closeHour = closeHour;

        if (_roomBookingRemoteModel != null) {
            try {
                Class<?> clazz = _roomBookingRemoteModel.getClass();

                Method method = clazz.getMethod("setCloseHour", int.class);

                method.invoke(_roomBookingRemoteModel, closeHour);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getPeriod() {
        return _period;
    }

    @Override
    public void setPeriod(int period) {
        _period = period;

        if (_roomBookingRemoteModel != null) {
            try {
                Class<?> clazz = _roomBookingRemoteModel.getClass();

                Method method = clazz.getMethod("setPeriod", int.class);

                method.invoke(_roomBookingRemoteModel, period);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_roomBookingRemoteModel != null) {
            try {
                Class<?> clazz = _roomBookingRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_roomBookingRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_roomBookingRemoteModel != null) {
            try {
                Class<?> clazz = _roomBookingRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_roomBookingRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getRoomBookingRemoteModel() {
        return _roomBookingRemoteModel;
    }

    public void setRoomBookingRemoteModel(BaseModel<?> roomBookingRemoteModel) {
        _roomBookingRemoteModel = roomBookingRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _roomBookingRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_roomBookingRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            RoomBookingLocalServiceUtil.addRoomBooking(this);
        } else {
            RoomBookingLocalServiceUtil.updateRoomBooking(this);
        }
    }

    @Override
    public RoomBooking toEscapedModel() {
        return (RoomBooking) ProxyUtil.newProxyInstance(RoomBooking.class.getClassLoader(),
            new Class[] { RoomBooking.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        RoomBookingClp clone = new RoomBookingClp();

        clone.setId(getId());
        clone.setRoomId(getRoomId());
        clone.setWeekDay(getWeekDay());
        clone.setOpenHour(getOpenHour());
        clone.setCloseHour(getCloseHour());
        clone.setPeriod(getPeriod());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(RoomBooking roomBooking) {
        int value = 0;

        if (getWeekDay() < roomBooking.getWeekDay()) {
            value = -1;
        } else if (getWeekDay() > roomBooking.getWeekDay()) {
            value = 1;
        } else {
            value = 0;
        }

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RoomBookingClp)) {
            return false;
        }

        RoomBookingClp roomBooking = (RoomBookingClp) obj;

        long primaryKey = roomBooking.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", roomId=");
        sb.append(getRoomId());
        sb.append(", weekDay=");
        sb.append(getWeekDay());
        sb.append(", openHour=");
        sb.append(getOpenHour());
        sb.append(", closeHour=");
        sb.append(getCloseHour());
        sb.append(", period=");
        sb.append(getPeriod());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(28);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.booking.model.RoomBooking");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>roomId</column-name><column-value><![CDATA[");
        sb.append(getRoomId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>weekDay</column-name><column-value><![CDATA[");
        sb.append(getWeekDay());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>openHour</column-name><column-value><![CDATA[");
        sb.append(getOpenHour());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>closeHour</column-name><column-value><![CDATA[");
        sb.append(getCloseHour());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>period</column-name><column-value><![CDATA[");
        sb.append(getPeriod());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
