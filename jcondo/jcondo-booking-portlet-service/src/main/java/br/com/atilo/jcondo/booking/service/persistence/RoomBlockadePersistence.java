package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.RoomBlockade;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the room blockade service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomBlockadePersistenceImpl
 * @see RoomBlockadeUtil
 * @generated
 */
public interface RoomBlockadePersistence extends BasePersistence<RoomBlockade> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link RoomBlockadeUtil} to access the room blockade persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the room blockades where roomId = &#63;.
    *
    * @param roomId the room ID
    * @return the matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRoom(
        long roomId) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the room blockades where roomId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @return the range of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRoom(
        long roomId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the room blockades where roomId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRoom(
        long roomId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first room blockade in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade findByRoom_First(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first room blockade in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room blockade, or <code>null</code> if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade fetchByRoom_First(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last room blockade in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade findByRoom_Last(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last room blockade in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room blockade, or <code>null</code> if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade fetchByRoom_Last(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the room blockades before and after the current room blockade in the ordered set where roomId = &#63;.
    *
    * @param id the primary key of the current room blockade
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade[] findByRoom_PrevAndNext(
        long id, long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the room blockades where roomId = &#63; from the database.
    *
    * @param roomId the room ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByRoom(long roomId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of room blockades where roomId = &#63;.
    *
    * @param roomId the room ID
    * @return the number of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public int countByRoom(long roomId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the room blockades where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @return the matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRecursive(
        long roomId, boolean recursive)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the room blockades where roomId = &#63; and recursive = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @return the range of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRecursive(
        long roomId, boolean recursive, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the room blockades where roomId = &#63; and recursive = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRecursive(
        long roomId, boolean recursive, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade findByRecursive_First(
        long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room blockade, or <code>null</code> if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade fetchByRecursive_First(
        long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade findByRecursive_Last(
        long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room blockade, or <code>null</code> if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade fetchByRecursive_Last(
        long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the room blockades before and after the current room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param id the primary key of the current room blockade
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade[] findByRecursive_PrevAndNext(
        long id, long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the room blockades where roomId = &#63; and recursive = &#63; from the database.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @throws SystemException if a system exception occurred
    */
    public void removeByRecursive(long roomId, boolean recursive)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of room blockades where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @return the number of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public int countByRecursive(long roomId, boolean recursive)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the room blockade in the entity cache if it is enabled.
    *
    * @param roomBlockade the room blockade
    */
    public void cacheResult(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade);

    /**
    * Caches the room blockades in the entity cache if it is enabled.
    *
    * @param roomBlockades the room blockades
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> roomBlockades);

    /**
    * Creates a new room blockade with the primary key. Does not add the room blockade to the database.
    *
    * @param id the primary key for the new room blockade
    * @return the new room blockade
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade create(long id);

    /**
    * Removes the room blockade with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the room blockade
    * @return the room blockade that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade remove(long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.booking.model.RoomBlockade updateImpl(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the room blockade with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException} if it could not be found.
    *
    * @param id the primary key of the room blockade
    * @return the room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the room blockade with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the room blockade
    * @return the room blockade, or <code>null</code> if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBlockade fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the room blockades.
    *
    * @return the room blockades
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the room blockades.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @return the range of room blockades
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the room blockades.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of room blockades
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the room blockades from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of room blockades.
    *
    * @return the number of room blockades
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
