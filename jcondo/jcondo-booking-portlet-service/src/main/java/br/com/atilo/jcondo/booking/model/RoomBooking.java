package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the RoomBooking service. Represents a row in the &quot;jco_room_booking&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see RoomBookingModel
 * @see br.com.atilo.jcondo.booking.model.impl.RoomBookingImpl
 * @see br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl
 * @generated
 */
public interface RoomBooking extends RoomBookingModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
