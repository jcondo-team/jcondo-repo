package br.com.atilo.jcondo.booking.model;

import br.com.atilo.jcondo.booking.service.ClpSerializer;
import br.com.atilo.jcondo.booking.service.PaymentLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PaymentClp extends BaseModelImpl<Payment> implements Payment {
    private long _id;
    private long _invoiceId;
    private String _token;
    private double _amount;
    private Date _date;
    private boolean _checked;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _paymentRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.booking.service.ClpSerializer.class;

    public PaymentClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Payment.class;
    }

    @Override
    public String getModelClassName() {
        return Payment.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("invoiceId", getInvoiceId());
        attributes.put("token", getToken());
        attributes.put("amount", getAmount());
        attributes.put("date", getDate());
        attributes.put("checked", getChecked());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long invoiceId = (Long) attributes.get("invoiceId");

        if (invoiceId != null) {
            setInvoiceId(invoiceId);
        }

        String token = (String) attributes.get("token");

        if (token != null) {
            setToken(token);
        }

        Double amount = (Double) attributes.get("amount");

        if (amount != null) {
            setAmount(amount);
        }

        Date date = (Date) attributes.get("date");

        if (date != null) {
            setDate(date);
        }

        Boolean checked = (Boolean) attributes.get("checked");

        if (checked != null) {
            setChecked(checked);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_paymentRemoteModel != null) {
            try {
                Class<?> clazz = _paymentRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_paymentRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getInvoiceId() {
        return _invoiceId;
    }

    @Override
    public void setInvoiceId(long invoiceId) {
        _invoiceId = invoiceId;

        if (_paymentRemoteModel != null) {
            try {
                Class<?> clazz = _paymentRemoteModel.getClass();

                Method method = clazz.getMethod("setInvoiceId", long.class);

                method.invoke(_paymentRemoteModel, invoiceId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getToken() {
        return _token;
    }

    @Override
    public void setToken(String token) {
        _token = token;

        if (_paymentRemoteModel != null) {
            try {
                Class<?> clazz = _paymentRemoteModel.getClass();

                Method method = clazz.getMethod("setToken", String.class);

                method.invoke(_paymentRemoteModel, token);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getAmount() {
        return _amount;
    }

    @Override
    public void setAmount(double amount) {
        _amount = amount;

        if (_paymentRemoteModel != null) {
            try {
                Class<?> clazz = _paymentRemoteModel.getClass();

                Method method = clazz.getMethod("setAmount", double.class);

                method.invoke(_paymentRemoteModel, amount);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDate() {
        return _date;
    }

    @Override
    public void setDate(Date date) {
        _date = date;

        if (_paymentRemoteModel != null) {
            try {
                Class<?> clazz = _paymentRemoteModel.getClass();

                Method method = clazz.getMethod("setDate", Date.class);

                method.invoke(_paymentRemoteModel, date);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getChecked() {
        return _checked;
    }

    @Override
    public boolean isChecked() {
        return _checked;
    }

    @Override
    public void setChecked(boolean checked) {
        _checked = checked;

        if (_paymentRemoteModel != null) {
            try {
                Class<?> clazz = _paymentRemoteModel.getClass();

                Method method = clazz.getMethod("setChecked", boolean.class);

                method.invoke(_paymentRemoteModel, checked);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_paymentRemoteModel != null) {
            try {
                Class<?> clazz = _paymentRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_paymentRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_paymentRemoteModel != null) {
            try {
                Class<?> clazz = _paymentRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_paymentRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getPaymentRemoteModel() {
        return _paymentRemoteModel;
    }

    public void setPaymentRemoteModel(BaseModel<?> paymentRemoteModel) {
        _paymentRemoteModel = paymentRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _paymentRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_paymentRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            PaymentLocalServiceUtil.addPayment(this);
        } else {
            PaymentLocalServiceUtil.updatePayment(this);
        }
    }

    @Override
    public Payment toEscapedModel() {
        return (Payment) ProxyUtil.newProxyInstance(Payment.class.getClassLoader(),
            new Class[] { Payment.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        PaymentClp clone = new PaymentClp();

        clone.setId(getId());
        clone.setInvoiceId(getInvoiceId());
        clone.setToken(getToken());
        clone.setAmount(getAmount());
        clone.setDate(getDate());
        clone.setChecked(getChecked());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Payment payment) {
        long primaryKey = payment.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PaymentClp)) {
            return false;
        }

        PaymentClp payment = (PaymentClp) obj;

        long primaryKey = payment.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", invoiceId=");
        sb.append(getInvoiceId());
        sb.append(", token=");
        sb.append(getToken());
        sb.append(", amount=");
        sb.append(getAmount());
        sb.append(", date=");
        sb.append(getDate());
        sb.append(", checked=");
        sb.append(getChecked());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(28);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.booking.model.Payment");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>invoiceId</column-name><column-value><![CDATA[");
        sb.append(getInvoiceId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>token</column-name><column-value><![CDATA[");
        sb.append(getToken());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>amount</column-name><column-value><![CDATA[");
        sb.append(getAmount());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>date</column-name><column-value><![CDATA[");
        sb.append(getDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>checked</column-name><column-value><![CDATA[");
        sb.append(getChecked());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
