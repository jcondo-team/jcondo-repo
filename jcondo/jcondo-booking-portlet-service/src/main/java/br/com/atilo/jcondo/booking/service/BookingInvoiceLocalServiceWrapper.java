package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookingInvoiceLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see BookingInvoiceLocalService
 * @generated
 */
public class BookingInvoiceLocalServiceWrapper
    implements BookingInvoiceLocalService,
        ServiceWrapper<BookingInvoiceLocalService> {
    private BookingInvoiceLocalService _bookingInvoiceLocalService;

    public BookingInvoiceLocalServiceWrapper(
        BookingInvoiceLocalService bookingInvoiceLocalService) {
        _bookingInvoiceLocalService = bookingInvoiceLocalService;
    }

    /**
    * Adds the booking invoice to the database. Also notifies the appropriate model listeners.
    *
    * @param bookingInvoice the booking invoice
    * @return the booking invoice that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice addBookingInvoice(
        br.com.atilo.jcondo.booking.model.BookingInvoice bookingInvoice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.addBookingInvoice(bookingInvoice);
    }

    /**
    * Creates a new booking invoice with the primary key. Does not add the booking invoice to the database.
    *
    * @param id the primary key for the new booking invoice
    * @return the new booking invoice
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice createBookingInvoice(
        long id) {
        return _bookingInvoiceLocalService.createBookingInvoice(id);
    }

    /**
    * Deletes the booking invoice with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the booking invoice
    * @return the booking invoice that was removed
    * @throws PortalException if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice deleteBookingInvoice(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.deleteBookingInvoice(id);
    }

    /**
    * Deletes the booking invoice from the database. Also notifies the appropriate model listeners.
    *
    * @param bookingInvoice the booking invoice
    * @return the booking invoice that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice deleteBookingInvoice(
        br.com.atilo.jcondo.booking.model.BookingInvoice bookingInvoice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.deleteBookingInvoice(bookingInvoice);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _bookingInvoiceLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice fetchBookingInvoice(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.fetchBookingInvoice(id);
    }

    /**
    * Returns the booking invoice with the primary key.
    *
    * @param id the primary key of the booking invoice
    * @return the booking invoice
    * @throws PortalException if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice getBookingInvoice(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.getBookingInvoice(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the booking invoices.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of booking invoices
    * @param end the upper bound of the range of booking invoices (not inclusive)
    * @return the range of booking invoices
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> getBookingInvoices(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.getBookingInvoices(start, end);
    }

    /**
    * Returns the number of booking invoices.
    *
    * @return the number of booking invoices
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getBookingInvoicesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.getBookingInvoicesCount();
    }

    /**
    * Updates the booking invoice in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param bookingInvoice the booking invoice
    * @return the booking invoice that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice updateBookingInvoice(
        br.com.atilo.jcondo.booking.model.BookingInvoice bookingInvoice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.updateBookingInvoice(bookingInvoice);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _bookingInvoiceLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _bookingInvoiceLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _bookingInvoiceLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice addBookingInvoice(
        br.com.atilo.jcondo.booking.model.Booking booking)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.addBookingInvoice(booking);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice updateBookingInvoice(
        br.com.atilo.jcondo.booking.model.BookingInvoice invoice, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.updateBookingInvoice(invoice, merge);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> getByBooking(
        long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.getByBooking(bookingId);
    }

    @Override
    public boolean isPrePaidMode(long invoiceId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoiceLocalService.isPrePaidMode(invoiceId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public BookingInvoiceLocalService getWrappedBookingInvoiceLocalService() {
        return _bookingInvoiceLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedBookingInvoiceLocalService(
        BookingInvoiceLocalService bookingInvoiceLocalService) {
        _bookingInvoiceLocalService = bookingInvoiceLocalService;
    }

    @Override
    public BookingInvoiceLocalService getWrappedService() {
        return _bookingInvoiceLocalService;
    }

    @Override
    public void setWrappedService(
        BookingInvoiceLocalService bookingInvoiceLocalService) {
        _bookingInvoiceLocalService = bookingInvoiceLocalService;
    }
}
