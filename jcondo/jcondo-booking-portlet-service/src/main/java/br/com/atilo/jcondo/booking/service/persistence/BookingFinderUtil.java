package br.com.atilo.jcondo.booking.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class BookingFinderUtil {
    private static BookingFinder _finder;

    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findOverdueBookings() {
        return getFinder().findOverdueBookings();
    }

    public static BookingFinder getFinder() {
        if (_finder == null) {
            _finder = (BookingFinder) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.booking.service.ClpSerializer.getServletContextName(),
                    BookingFinder.class.getName());

            ReferenceRegistry.registerReference(BookingFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(BookingFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(BookingFinderUtil.class, "_finder");
    }
}
