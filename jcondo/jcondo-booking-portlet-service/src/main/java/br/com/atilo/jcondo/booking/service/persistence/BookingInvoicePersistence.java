package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.BookingInvoice;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the booking invoice service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingInvoicePersistenceImpl
 * @see BookingInvoiceUtil
 * @generated
 */
public interface BookingInvoicePersistence extends BasePersistence<BookingInvoice> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link BookingInvoiceUtil} to access the booking invoice persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns the booking invoice where code = &#63; or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException} if it could not be found.
    *
    * @param code the code
    * @return the matching booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice findByCode(
        java.lang.String code)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the booking invoice where code = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param code the code
    * @return the matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice fetchByCode(
        java.lang.String code)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the booking invoice where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param code the code
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice fetchByCode(
        java.lang.String code, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the booking invoice where code = &#63; from the database.
    *
    * @param code the code
    * @return the booking invoice that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice removeByCode(
        java.lang.String code)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of booking invoices where code = &#63;.
    *
    * @param code the code
    * @return the number of matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public int countByCode(java.lang.String code)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the booking invoices where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @return the matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findByBooking(
        long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the booking invoices where bookingId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookingId the booking ID
    * @param start the lower bound of the range of booking invoices
    * @param end the upper bound of the range of booking invoices (not inclusive)
    * @return the range of matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findByBooking(
        long bookingId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the booking invoices where bookingId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookingId the booking ID
    * @param start the lower bound of the range of booking invoices
    * @param end the upper bound of the range of booking invoices (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findByBooking(
        long bookingId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice findByBooking_First(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice fetchByBooking_First(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice findByBooking_Last(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice fetchByBooking_Last(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the booking invoices before and after the current booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param id the primary key of the current booking invoice
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice[] findByBooking_PrevAndNext(
        long id, long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the booking invoices where bookingId = &#63; from the database.
    *
    * @param bookingId the booking ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByBooking(long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of booking invoices where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @return the number of matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public int countByBooking(long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the booking invoice in the entity cache if it is enabled.
    *
    * @param bookingInvoice the booking invoice
    */
    public void cacheResult(
        br.com.atilo.jcondo.booking.model.BookingInvoice bookingInvoice);

    /**
    * Caches the booking invoices in the entity cache if it is enabled.
    *
    * @param bookingInvoices the booking invoices
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> bookingInvoices);

    /**
    * Creates a new booking invoice with the primary key. Does not add the booking invoice to the database.
    *
    * @param id the primary key for the new booking invoice
    * @return the new booking invoice
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice create(long id);

    /**
    * Removes the booking invoice with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the booking invoice
    * @return the booking invoice that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice remove(long id)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.booking.model.BookingInvoice updateImpl(
        br.com.atilo.jcondo.booking.model.BookingInvoice bookingInvoice)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the booking invoice with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException} if it could not be found.
    *
    * @param id the primary key of the booking invoice
    * @return the booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the booking invoice with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the booking invoice
    * @return the booking invoice, or <code>null</code> if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.BookingInvoice fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the booking invoices.
    *
    * @return the booking invoices
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the booking invoices.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of booking invoices
    * @param end the upper bound of the range of booking invoices (not inclusive)
    * @return the range of booking invoices
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the booking invoices.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of booking invoices
    * @param end the upper bound of the range of booking invoices (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of booking invoices
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the booking invoices from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of booking invoices.
    *
    * @return the number of booking invoices
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
