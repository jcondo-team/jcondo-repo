package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Payment}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Payment
 * @generated
 */
public class PaymentWrapper implements Payment, ModelWrapper<Payment> {
    private Payment _payment;

    public PaymentWrapper(Payment payment) {
        _payment = payment;
    }

    @Override
    public Class<?> getModelClass() {
        return Payment.class;
    }

    @Override
    public String getModelClassName() {
        return Payment.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("invoiceId", getInvoiceId());
        attributes.put("token", getToken());
        attributes.put("amount", getAmount());
        attributes.put("date", getDate());
        attributes.put("checked", getChecked());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long invoiceId = (Long) attributes.get("invoiceId");

        if (invoiceId != null) {
            setInvoiceId(invoiceId);
        }

        String token = (String) attributes.get("token");

        if (token != null) {
            setToken(token);
        }

        Double amount = (Double) attributes.get("amount");

        if (amount != null) {
            setAmount(amount);
        }

        Date date = (Date) attributes.get("date");

        if (date != null) {
            setDate(date);
        }

        Boolean checked = (Boolean) attributes.get("checked");

        if (checked != null) {
            setChecked(checked);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this payment.
    *
    * @return the primary key of this payment
    */
    @Override
    public long getPrimaryKey() {
        return _payment.getPrimaryKey();
    }

    /**
    * Sets the primary key of this payment.
    *
    * @param primaryKey the primary key of this payment
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _payment.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this payment.
    *
    * @return the ID of this payment
    */
    @Override
    public long getId() {
        return _payment.getId();
    }

    /**
    * Sets the ID of this payment.
    *
    * @param id the ID of this payment
    */
    @Override
    public void setId(long id) {
        _payment.setId(id);
    }

    /**
    * Returns the invoice ID of this payment.
    *
    * @return the invoice ID of this payment
    */
    @Override
    public long getInvoiceId() {
        return _payment.getInvoiceId();
    }

    /**
    * Sets the invoice ID of this payment.
    *
    * @param invoiceId the invoice ID of this payment
    */
    @Override
    public void setInvoiceId(long invoiceId) {
        _payment.setInvoiceId(invoiceId);
    }

    /**
    * Returns the token of this payment.
    *
    * @return the token of this payment
    */
    @Override
    public java.lang.String getToken() {
        return _payment.getToken();
    }

    /**
    * Sets the token of this payment.
    *
    * @param token the token of this payment
    */
    @Override
    public void setToken(java.lang.String token) {
        _payment.setToken(token);
    }

    /**
    * Returns the amount of this payment.
    *
    * @return the amount of this payment
    */
    @Override
    public double getAmount() {
        return _payment.getAmount();
    }

    /**
    * Sets the amount of this payment.
    *
    * @param amount the amount of this payment
    */
    @Override
    public void setAmount(double amount) {
        _payment.setAmount(amount);
    }

    /**
    * Returns the date of this payment.
    *
    * @return the date of this payment
    */
    @Override
    public java.util.Date getDate() {
        return _payment.getDate();
    }

    /**
    * Sets the date of this payment.
    *
    * @param date the date of this payment
    */
    @Override
    public void setDate(java.util.Date date) {
        _payment.setDate(date);
    }

    /**
    * Returns the checked of this payment.
    *
    * @return the checked of this payment
    */
    @Override
    public boolean getChecked() {
        return _payment.getChecked();
    }

    /**
    * Returns <code>true</code> if this payment is checked.
    *
    * @return <code>true</code> if this payment is checked; <code>false</code> otherwise
    */
    @Override
    public boolean isChecked() {
        return _payment.isChecked();
    }

    /**
    * Sets whether this payment is checked.
    *
    * @param checked the checked of this payment
    */
    @Override
    public void setChecked(boolean checked) {
        _payment.setChecked(checked);
    }

    /**
    * Returns the opr date of this payment.
    *
    * @return the opr date of this payment
    */
    @Override
    public java.util.Date getOprDate() {
        return _payment.getOprDate();
    }

    /**
    * Sets the opr date of this payment.
    *
    * @param oprDate the opr date of this payment
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _payment.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this payment.
    *
    * @return the opr user of this payment
    */
    @Override
    public long getOprUser() {
        return _payment.getOprUser();
    }

    /**
    * Sets the opr user of this payment.
    *
    * @param oprUser the opr user of this payment
    */
    @Override
    public void setOprUser(long oprUser) {
        _payment.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _payment.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _payment.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _payment.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _payment.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _payment.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _payment.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _payment.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _payment.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _payment.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _payment.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _payment.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PaymentWrapper((Payment) _payment.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.booking.model.Payment payment) {
        return _payment.compareTo(payment);
    }

    @Override
    public int hashCode() {
        return _payment.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.booking.model.Payment> toCacheModel() {
        return _payment.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Payment toEscapedModel() {
        return new PaymentWrapper(_payment.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Payment toUnescapedModel() {
        return new PaymentWrapper(_payment.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _payment.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _payment.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _payment.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PaymentWrapper)) {
            return false;
        }

        PaymentWrapper paymentWrapper = (PaymentWrapper) obj;

        if (Validator.equals(_payment, paymentWrapper._payment)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Payment getWrappedPayment() {
        return _payment;
    }

    @Override
    public Payment getWrappedModel() {
        return _payment;
    }

    @Override
    public void resetOriginalValues() {
        _payment.resetOriginalValues();
    }
}
