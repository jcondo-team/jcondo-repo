package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class BookingActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public BookingActionableDynamicQuery() throws SystemException {
        setBaseLocalService(BookingLocalServiceUtil.getService());
        setClass(Booking.class);

        setClassLoader(br.com.atilo.jcondo.booking.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
