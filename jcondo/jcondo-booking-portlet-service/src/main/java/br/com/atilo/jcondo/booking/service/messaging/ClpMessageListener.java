package br.com.atilo.jcondo.booking.service.messaging;

import br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingInvoiceServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingNoteLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingNoteServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingServiceUtil;
import br.com.atilo.jcondo.booking.service.ClpSerializer;
import br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.GuestServiceUtil;
import br.com.atilo.jcondo.booking.service.PaymentLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.PaymentServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomBlockadeLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomBlockadeServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomBookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomBookingServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomServiceUtil;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;


public class ClpMessageListener extends BaseMessageListener {
    public static String getServletContextName() {
        return ClpSerializer.getServletContextName();
    }

    @Override
    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(getServletContextName())) {
            BookingLocalServiceUtil.clearService();

            BookingServiceUtil.clearService();
            BookingInvoiceLocalServiceUtil.clearService();

            BookingInvoiceServiceUtil.clearService();
            BookingNoteLocalServiceUtil.clearService();

            BookingNoteServiceUtil.clearService();
            GuestLocalServiceUtil.clearService();

            GuestServiceUtil.clearService();
            PaymentLocalServiceUtil.clearService();

            PaymentServiceUtil.clearService();
            RoomLocalServiceUtil.clearService();

            RoomServiceUtil.clearService();
            RoomBlockadeLocalServiceUtil.clearService();

            RoomBlockadeServiceUtil.clearService();
            RoomBookingLocalServiceUtil.clearService();

            RoomBookingServiceUtil.clearService();
        }
    }
}
