package br.com.atilo.jcondo.booking.service.permission;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;

import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.permission.BasePermission;

public class RoomPermission extends BasePermission {

	public static boolean hasPermission(Permission permission, long roomId) throws PortalException, SystemException {
		Person person = PersonLocalServiceUtil.getPerson();
		Organization organization = OrganizationLocalServiceUtil.getOrganization(getOrganizationDomain(person.getDomainId()));
		return hasPermission(permission, Room.class, roomId, organization);
	}

}
