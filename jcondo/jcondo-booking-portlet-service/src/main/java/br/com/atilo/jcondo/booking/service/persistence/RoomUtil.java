package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.Room;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the room service. This utility wraps {@link RoomPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomPersistence
 * @see RoomPersistenceImpl
 * @generated
 */
public class RoomUtil {
    private static RoomPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Room room) {
        getPersistence().clearCache(room);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Room> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Room> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Room> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Room update(Room room) throws SystemException {
        return getPersistence().update(room);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Room update(Room room, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(room, serviceContext);
    }

    /**
    * Returns the room where name = &#63; or throws a {@link br.com.atilo.jcondo.booking.NoSuchRoomException} if it could not be found.
    *
    * @param name the name
    * @return the matching room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room findByName(
        java.lang.String name)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByName(name);
    }

    /**
    * Returns the room where name = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param name the name
    * @return the matching room, or <code>null</code> if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room fetchByName(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByName(name);
    }

    /**
    * Returns the room where name = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param name the name
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching room, or <code>null</code> if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room fetchByName(
        java.lang.String name, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByName(name, retrieveFromCache);
    }

    /**
    * Removes the room where name = &#63; from the database.
    *
    * @param name the name
    * @return the room that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room removeByName(
        java.lang.String name)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().removeByName(name);
    }

    /**
    * Returns the number of rooms where name = &#63;.
    *
    * @param name the name
    * @return the number of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static int countByName(java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByName(name);
    }

    /**
    * Returns all the rooms where bookable = &#63;.
    *
    * @param bookable the bookable
    * @return the matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findByBookable(
        boolean bookable)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBookable(bookable);
    }

    /**
    * Returns a range of all the rooms where bookable = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookable the bookable
    * @param start the lower bound of the range of rooms
    * @param end the upper bound of the range of rooms (not inclusive)
    * @return the range of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findByBookable(
        boolean bookable, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBookable(bookable, start, end);
    }

    /**
    * Returns an ordered range of all the rooms where bookable = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookable the bookable
    * @param start the lower bound of the range of rooms
    * @param end the upper bound of the range of rooms (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findByBookable(
        boolean bookable, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBookable(bookable, start, end, orderByComparator);
    }

    /**
    * Returns the first room in the ordered set where bookable = &#63;.
    *
    * @param bookable the bookable
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room findByBookable_First(
        boolean bookable,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBookable_First(bookable, orderByComparator);
    }

    /**
    * Returns the first room in the ordered set where bookable = &#63;.
    *
    * @param bookable the bookable
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room, or <code>null</code> if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room fetchByBookable_First(
        boolean bookable,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByBookable_First(bookable, orderByComparator);
    }

    /**
    * Returns the last room in the ordered set where bookable = &#63;.
    *
    * @param bookable the bookable
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room findByBookable_Last(
        boolean bookable,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBookable_Last(bookable, orderByComparator);
    }

    /**
    * Returns the last room in the ordered set where bookable = &#63;.
    *
    * @param bookable the bookable
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room, or <code>null</code> if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room fetchByBookable_Last(
        boolean bookable,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByBookable_Last(bookable, orderByComparator);
    }

    /**
    * Returns the rooms before and after the current room in the ordered set where bookable = &#63;.
    *
    * @param id the primary key of the current room
    * @param bookable the bookable
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room[] findByBookable_PrevAndNext(
        long id, boolean bookable,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBookable_PrevAndNext(id, bookable, orderByComparator);
    }

    /**
    * Removes all the rooms where bookable = &#63; from the database.
    *
    * @param bookable the bookable
    * @throws SystemException if a system exception occurred
    */
    public static void removeByBookable(boolean bookable)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByBookable(bookable);
    }

    /**
    * Returns the number of rooms where bookable = &#63;.
    *
    * @param bookable the bookable
    * @return the number of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static int countByBookable(boolean bookable)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByBookable(bookable);
    }

    /**
    * Returns all the rooms where available = &#63;.
    *
    * @param available the available
    * @return the matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findByAvailable(
        boolean available)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAvailable(available);
    }

    /**
    * Returns a range of all the rooms where available = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param available the available
    * @param start the lower bound of the range of rooms
    * @param end the upper bound of the range of rooms (not inclusive)
    * @return the range of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findByAvailable(
        boolean available, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByAvailable(available, start, end);
    }

    /**
    * Returns an ordered range of all the rooms where available = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param available the available
    * @param start the lower bound of the range of rooms
    * @param end the upper bound of the range of rooms (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findByAvailable(
        boolean available, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAvailable(available, start, end, orderByComparator);
    }

    /**
    * Returns the first room in the ordered set where available = &#63;.
    *
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room findByAvailable_First(
        boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAvailable_First(available, orderByComparator);
    }

    /**
    * Returns the first room in the ordered set where available = &#63;.
    *
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room, or <code>null</code> if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room fetchByAvailable_First(
        boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAvailable_First(available, orderByComparator);
    }

    /**
    * Returns the last room in the ordered set where available = &#63;.
    *
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room findByAvailable_Last(
        boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAvailable_Last(available, orderByComparator);
    }

    /**
    * Returns the last room in the ordered set where available = &#63;.
    *
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room, or <code>null</code> if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room fetchByAvailable_Last(
        boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByAvailable_Last(available, orderByComparator);
    }

    /**
    * Returns the rooms before and after the current room in the ordered set where available = &#63;.
    *
    * @param id the primary key of the current room
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room[] findByAvailable_PrevAndNext(
        long id, boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByAvailable_PrevAndNext(id, available, orderByComparator);
    }

    /**
    * Removes all the rooms where available = &#63; from the database.
    *
    * @param available the available
    * @throws SystemException if a system exception occurred
    */
    public static void removeByAvailable(boolean available)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByAvailable(available);
    }

    /**
    * Returns the number of rooms where available = &#63;.
    *
    * @param available the available
    * @return the number of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static int countByAvailable(boolean available)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByAvailable(available);
    }

    /**
    * Returns all the rooms where bookable = &#63; and available = &#63;.
    *
    * @param bookable the bookable
    * @param available the available
    * @return the matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findByBookableAndAvailable(
        boolean bookable, boolean available)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBookableAndAvailable(bookable, available);
    }

    /**
    * Returns a range of all the rooms where bookable = &#63; and available = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookable the bookable
    * @param available the available
    * @param start the lower bound of the range of rooms
    * @param end the upper bound of the range of rooms (not inclusive)
    * @return the range of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findByBookableAndAvailable(
        boolean bookable, boolean available, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBookableAndAvailable(bookable, available, start, end);
    }

    /**
    * Returns an ordered range of all the rooms where bookable = &#63; and available = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookable the bookable
    * @param available the available
    * @param start the lower bound of the range of rooms
    * @param end the upper bound of the range of rooms (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findByBookableAndAvailable(
        boolean bookable, boolean available, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBookableAndAvailable(bookable, available, start, end,
            orderByComparator);
    }

    /**
    * Returns the first room in the ordered set where bookable = &#63; and available = &#63;.
    *
    * @param bookable the bookable
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room findByBookableAndAvailable_First(
        boolean bookable, boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBookableAndAvailable_First(bookable, available,
            orderByComparator);
    }

    /**
    * Returns the first room in the ordered set where bookable = &#63; and available = &#63;.
    *
    * @param bookable the bookable
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room, or <code>null</code> if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room fetchByBookableAndAvailable_First(
        boolean bookable, boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByBookableAndAvailable_First(bookable, available,
            orderByComparator);
    }

    /**
    * Returns the last room in the ordered set where bookable = &#63; and available = &#63;.
    *
    * @param bookable the bookable
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room findByBookableAndAvailable_Last(
        boolean bookable, boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBookableAndAvailable_Last(bookable, available,
            orderByComparator);
    }

    /**
    * Returns the last room in the ordered set where bookable = &#63; and available = &#63;.
    *
    * @param bookable the bookable
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room, or <code>null</code> if a matching room could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room fetchByBookableAndAvailable_Last(
        boolean bookable, boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByBookableAndAvailable_Last(bookable, available,
            orderByComparator);
    }

    /**
    * Returns the rooms before and after the current room in the ordered set where bookable = &#63; and available = &#63;.
    *
    * @param id the primary key of the current room
    * @param bookable the bookable
    * @param available the available
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room[] findByBookableAndAvailable_PrevAndNext(
        long id, boolean bookable, boolean available,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBookableAndAvailable_PrevAndNext(id, bookable,
            available, orderByComparator);
    }

    /**
    * Removes all the rooms where bookable = &#63; and available = &#63; from the database.
    *
    * @param bookable the bookable
    * @param available the available
    * @throws SystemException if a system exception occurred
    */
    public static void removeByBookableAndAvailable(boolean bookable,
        boolean available)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByBookableAndAvailable(bookable, available);
    }

    /**
    * Returns the number of rooms where bookable = &#63; and available = &#63;.
    *
    * @param bookable the bookable
    * @param available the available
    * @return the number of matching rooms
    * @throws SystemException if a system exception occurred
    */
    public static int countByBookableAndAvailable(boolean bookable,
        boolean available)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByBookableAndAvailable(bookable, available);
    }

    /**
    * Caches the room in the entity cache if it is enabled.
    *
    * @param room the room
    */
    public static void cacheResult(br.com.atilo.jcondo.booking.model.Room room) {
        getPersistence().cacheResult(room);
    }

    /**
    * Caches the rooms in the entity cache if it is enabled.
    *
    * @param rooms the rooms
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.Room> rooms) {
        getPersistence().cacheResult(rooms);
    }

    /**
    * Creates a new room with the primary key. Does not add the room to the database.
    *
    * @param id the primary key for the new room
    * @return the new room
    */
    public static br.com.atilo.jcondo.booking.model.Room create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the room with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the room
    * @return the room that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room remove(long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.booking.model.Room updateImpl(
        br.com.atilo.jcondo.booking.model.Room room)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(room);
    }

    /**
    * Returns the room with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchRoomException} if it could not be found.
    *
    * @param id the primary key of the room
    * @return the room
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the room with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the room
    * @return the room, or <code>null</code> if a room with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Room fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the rooms.
    *
    * @return the rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the rooms.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rooms
    * @param end the upper bound of the range of rooms (not inclusive)
    * @return the range of rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the rooms.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of rooms
    * @param end the upper bound of the range of rooms (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of rooms
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the rooms from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of rooms.
    *
    * @return the number of rooms
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static RoomPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (RoomPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.booking.service.ClpSerializer.getServletContextName(),
                    RoomPersistence.class.getName());

            ReferenceRegistry.registerReference(RoomUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(RoomPersistence persistence) {
    }
}
