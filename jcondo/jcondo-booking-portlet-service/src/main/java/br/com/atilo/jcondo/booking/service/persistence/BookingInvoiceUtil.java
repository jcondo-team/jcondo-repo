package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.BookingInvoice;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the booking invoice service. This utility wraps {@link BookingInvoicePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingInvoicePersistence
 * @see BookingInvoicePersistenceImpl
 * @generated
 */
public class BookingInvoiceUtil {
    private static BookingInvoicePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(BookingInvoice bookingInvoice) {
        getPersistence().clearCache(bookingInvoice);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<BookingInvoice> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<BookingInvoice> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<BookingInvoice> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static BookingInvoice update(BookingInvoice bookingInvoice)
        throws SystemException {
        return getPersistence().update(bookingInvoice);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static BookingInvoice update(BookingInvoice bookingInvoice,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(bookingInvoice, serviceContext);
    }

    /**
    * Returns the booking invoice where code = &#63; or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException} if it could not be found.
    *
    * @param code the code
    * @return the matching booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice findByCode(
        java.lang.String code)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCode(code);
    }

    /**
    * Returns the booking invoice where code = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param code the code
    * @return the matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice fetchByCode(
        java.lang.String code)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByCode(code);
    }

    /**
    * Returns the booking invoice where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param code the code
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice fetchByCode(
        java.lang.String code, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByCode(code, retrieveFromCache);
    }

    /**
    * Removes the booking invoice where code = &#63; from the database.
    *
    * @param code the code
    * @return the booking invoice that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice removeByCode(
        java.lang.String code)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().removeByCode(code);
    }

    /**
    * Returns the number of booking invoices where code = &#63;.
    *
    * @param code the code
    * @return the number of matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public static int countByCode(java.lang.String code)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCode(code);
    }

    /**
    * Returns all the booking invoices where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @return the matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findByBooking(
        long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBooking(bookingId);
    }

    /**
    * Returns a range of all the booking invoices where bookingId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookingId the booking ID
    * @param start the lower bound of the range of booking invoices
    * @param end the upper bound of the range of booking invoices (not inclusive)
    * @return the range of matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findByBooking(
        long bookingId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBooking(bookingId, start, end);
    }

    /**
    * Returns an ordered range of all the booking invoices where bookingId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookingId the booking ID
    * @param start the lower bound of the range of booking invoices
    * @param end the upper bound of the range of booking invoices (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findByBooking(
        long bookingId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBooking(bookingId, start, end, orderByComparator);
    }

    /**
    * Returns the first booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice findByBooking_First(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBooking_First(bookingId, orderByComparator);
    }

    /**
    * Returns the first booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice fetchByBooking_First(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByBooking_First(bookingId, orderByComparator);
    }

    /**
    * Returns the last booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice findByBooking_Last(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByBooking_Last(bookingId, orderByComparator);
    }

    /**
    * Returns the last booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice fetchByBooking_Last(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByBooking_Last(bookingId, orderByComparator);
    }

    /**
    * Returns the booking invoices before and after the current booking invoice in the ordered set where bookingId = &#63;.
    *
    * @param id the primary key of the current booking invoice
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice[] findByBooking_PrevAndNext(
        long id, long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByBooking_PrevAndNext(id, bookingId, orderByComparator);
    }

    /**
    * Removes all the booking invoices where bookingId = &#63; from the database.
    *
    * @param bookingId the booking ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByBooking(long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByBooking(bookingId);
    }

    /**
    * Returns the number of booking invoices where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @return the number of matching booking invoices
    * @throws SystemException if a system exception occurred
    */
    public static int countByBooking(long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByBooking(bookingId);
    }

    /**
    * Caches the booking invoice in the entity cache if it is enabled.
    *
    * @param bookingInvoice the booking invoice
    */
    public static void cacheResult(
        br.com.atilo.jcondo.booking.model.BookingInvoice bookingInvoice) {
        getPersistence().cacheResult(bookingInvoice);
    }

    /**
    * Caches the booking invoices in the entity cache if it is enabled.
    *
    * @param bookingInvoices the booking invoices
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> bookingInvoices) {
        getPersistence().cacheResult(bookingInvoices);
    }

    /**
    * Creates a new booking invoice with the primary key. Does not add the booking invoice to the database.
    *
    * @param id the primary key for the new booking invoice
    * @return the new booking invoice
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice create(
        long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the booking invoice with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the booking invoice
    * @return the booking invoice that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice remove(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.booking.model.BookingInvoice updateImpl(
        br.com.atilo.jcondo.booking.model.BookingInvoice bookingInvoice)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(bookingInvoice);
    }

    /**
    * Returns the booking invoice with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException} if it could not be found.
    *
    * @param id the primary key of the booking invoice
    * @return the booking invoice
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the booking invoice with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the booking invoice
    * @return the booking invoice, or <code>null</code> if a booking invoice with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.BookingInvoice fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the booking invoices.
    *
    * @return the booking invoices
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the booking invoices.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of booking invoices
    * @param end the upper bound of the range of booking invoices (not inclusive)
    * @return the range of booking invoices
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the booking invoices.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of booking invoices
    * @param end the upper bound of the range of booking invoices (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of booking invoices
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.BookingInvoice> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the booking invoices from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of booking invoices.
    *
    * @return the number of booking invoices
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static BookingInvoicePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (BookingInvoicePersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.booking.service.ClpSerializer.getServletContextName(),
                    BookingInvoicePersistence.class.getName());

            ReferenceRegistry.registerReference(BookingInvoiceUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(BookingInvoicePersistence persistence) {
    }
}
