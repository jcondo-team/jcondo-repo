package br.com.atilo.jcondo.booking.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.booking.service.http.BookingServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.booking.service.http.BookingServiceSoap
 * @generated
 */
public class BookingSoap implements Serializable {
    private long _id;
    private long _roomId;
    private long _domainId;
    private long _personId;
    private Date _beginDate;
    private Date _endDate;
    private int _statusId;
    private long _noteId;
    private Date _oprDate;
    private long _oprUser;

    public BookingSoap() {
    }

    public static BookingSoap toSoapModel(Booking model) {
        BookingSoap soapModel = new BookingSoap();

        soapModel.setId(model.getId());
        soapModel.setRoomId(model.getRoomId());
        soapModel.setDomainId(model.getDomainId());
        soapModel.setPersonId(model.getPersonId());
        soapModel.setBeginDate(model.getBeginDate());
        soapModel.setEndDate(model.getEndDate());
        soapModel.setStatusId(model.getStatusId());
        soapModel.setNoteId(model.getNoteId());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static BookingSoap[] toSoapModels(Booking[] models) {
        BookingSoap[] soapModels = new BookingSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static BookingSoap[][] toSoapModels(Booking[][] models) {
        BookingSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new BookingSoap[models.length][models[0].length];
        } else {
            soapModels = new BookingSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static BookingSoap[] toSoapModels(List<Booking> models) {
        List<BookingSoap> soapModels = new ArrayList<BookingSoap>(models.size());

        for (Booking model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new BookingSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getRoomId() {
        return _roomId;
    }

    public void setRoomId(long roomId) {
        _roomId = roomId;
    }

    public long getDomainId() {
        return _domainId;
    }

    public void setDomainId(long domainId) {
        _domainId = domainId;
    }

    public long getPersonId() {
        return _personId;
    }

    public void setPersonId(long personId) {
        _personId = personId;
    }

    public Date getBeginDate() {
        return _beginDate;
    }

    public void setBeginDate(Date beginDate) {
        _beginDate = beginDate;
    }

    public Date getEndDate() {
        return _endDate;
    }

    public void setEndDate(Date endDate) {
        _endDate = endDate;
    }

    public int getStatusId() {
        return _statusId;
    }

    public void setStatusId(int statusId) {
        _statusId = statusId;
    }

    public long getNoteId() {
        return _noteId;
    }

    public void setNoteId(long noteId) {
        _noteId = noteId;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
