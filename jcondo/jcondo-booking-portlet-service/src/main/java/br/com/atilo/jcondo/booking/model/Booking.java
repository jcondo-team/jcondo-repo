package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Booking service. Represents a row in the &quot;jco_booking&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see BookingModel
 * @see br.com.atilo.jcondo.booking.model.impl.BookingImpl
 * @see br.com.atilo.jcondo.booking.model.impl.BookingModelImpl
 * @generated
 */
public interface Booking extends BookingModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.booking.model.impl.BookingImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.booking.model.datatype.BookingStatus getStatus();

    public void setStatus(
        br.com.atilo.jcondo.booking.model.datatype.BookingStatus status);

    public com.liferay.portal.model.BaseModel<?> getDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setDomain(com.liferay.portal.model.BaseModel<?> domain);

    public br.com.atilo.jcondo.booking.model.Room getRoom()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setRoom(br.com.atilo.jcondo.booking.model.Room room);

    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setPerson(br.com.atilo.jcondo.manager.model.Person person);

    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> getGuests()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.booking.model.BookingNote getNote()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setNote(br.com.atilo.jcondo.booking.model.BookingNote note);

    public br.com.atilo.jcondo.booking.model.BookingInvoice getInvoice()
        throws com.liferay.portal.kernel.exception.SystemException;
}
