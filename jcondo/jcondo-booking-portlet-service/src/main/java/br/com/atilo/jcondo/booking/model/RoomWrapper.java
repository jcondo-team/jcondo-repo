package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Room}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Room
 * @generated
 */
public class RoomWrapper implements Room, ModelWrapper<Room> {
    private Room _room;

    public RoomWrapper(Room room) {
        _room = room;
    }

    @Override
    public Class<?> getModelClass() {
        return Room.class;
    }

    @Override
    public String getModelClassName() {
        return Room.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("folderId", getFolderId());
        attributes.put("agreementId", getAgreementId());
        attributes.put("name", getName());
        attributes.put("description", getDescription());
        attributes.put("available", getAvailable());
        attributes.put("bookable", getBookable());
        attributes.put("price", getPrice());
        attributes.put("capacity", getCapacity());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long folderId = (Long) attributes.get("folderId");

        if (folderId != null) {
            setFolderId(folderId);
        }

        Long agreementId = (Long) attributes.get("agreementId");

        if (agreementId != null) {
            setAgreementId(agreementId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean available = (Boolean) attributes.get("available");

        if (available != null) {
            setAvailable(available);
        }

        Boolean bookable = (Boolean) attributes.get("bookable");

        if (bookable != null) {
            setBookable(bookable);
        }

        Double price = (Double) attributes.get("price");

        if (price != null) {
            setPrice(price);
        }

        Integer capacity = (Integer) attributes.get("capacity");

        if (capacity != null) {
            setCapacity(capacity);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this room.
    *
    * @return the primary key of this room
    */
    @Override
    public long getPrimaryKey() {
        return _room.getPrimaryKey();
    }

    /**
    * Sets the primary key of this room.
    *
    * @param primaryKey the primary key of this room
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _room.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this room.
    *
    * @return the ID of this room
    */
    @Override
    public long getId() {
        return _room.getId();
    }

    /**
    * Sets the ID of this room.
    *
    * @param id the ID of this room
    */
    @Override
    public void setId(long id) {
        _room.setId(id);
    }

    /**
    * Returns the folder ID of this room.
    *
    * @return the folder ID of this room
    */
    @Override
    public long getFolderId() {
        return _room.getFolderId();
    }

    /**
    * Sets the folder ID of this room.
    *
    * @param folderId the folder ID of this room
    */
    @Override
    public void setFolderId(long folderId) {
        _room.setFolderId(folderId);
    }

    /**
    * Returns the agreement ID of this room.
    *
    * @return the agreement ID of this room
    */
    @Override
    public long getAgreementId() {
        return _room.getAgreementId();
    }

    /**
    * Sets the agreement ID of this room.
    *
    * @param agreementId the agreement ID of this room
    */
    @Override
    public void setAgreementId(long agreementId) {
        _room.setAgreementId(agreementId);
    }

    /**
    * Returns the name of this room.
    *
    * @return the name of this room
    */
    @Override
    public java.lang.String getName() {
        return _room.getName();
    }

    /**
    * Sets the name of this room.
    *
    * @param name the name of this room
    */
    @Override
    public void setName(java.lang.String name) {
        _room.setName(name);
    }

    /**
    * Returns the description of this room.
    *
    * @return the description of this room
    */
    @Override
    public java.lang.String getDescription() {
        return _room.getDescription();
    }

    /**
    * Sets the description of this room.
    *
    * @param description the description of this room
    */
    @Override
    public void setDescription(java.lang.String description) {
        _room.setDescription(description);
    }

    /**
    * Returns the available of this room.
    *
    * @return the available of this room
    */
    @Override
    public boolean getAvailable() {
        return _room.getAvailable();
    }

    /**
    * Returns <code>true</code> if this room is available.
    *
    * @return <code>true</code> if this room is available; <code>false</code> otherwise
    */
    @Override
    public boolean isAvailable() {
        return _room.isAvailable();
    }

    /**
    * Sets whether this room is available.
    *
    * @param available the available of this room
    */
    @Override
    public void setAvailable(boolean available) {
        _room.setAvailable(available);
    }

    /**
    * Returns the bookable of this room.
    *
    * @return the bookable of this room
    */
    @Override
    public boolean getBookable() {
        return _room.getBookable();
    }

    /**
    * Returns <code>true</code> if this room is bookable.
    *
    * @return <code>true</code> if this room is bookable; <code>false</code> otherwise
    */
    @Override
    public boolean isBookable() {
        return _room.isBookable();
    }

    /**
    * Sets whether this room is bookable.
    *
    * @param bookable the bookable of this room
    */
    @Override
    public void setBookable(boolean bookable) {
        _room.setBookable(bookable);
    }

    /**
    * Returns the price of this room.
    *
    * @return the price of this room
    */
    @Override
    public double getPrice() {
        return _room.getPrice();
    }

    /**
    * Sets the price of this room.
    *
    * @param price the price of this room
    */
    @Override
    public void setPrice(double price) {
        _room.setPrice(price);
    }

    /**
    * Returns the capacity of this room.
    *
    * @return the capacity of this room
    */
    @Override
    public int getCapacity() {
        return _room.getCapacity();
    }

    /**
    * Sets the capacity of this room.
    *
    * @param capacity the capacity of this room
    */
    @Override
    public void setCapacity(int capacity) {
        _room.setCapacity(capacity);
    }

    /**
    * Returns the opr date of this room.
    *
    * @return the opr date of this room
    */
    @Override
    public java.util.Date getOprDate() {
        return _room.getOprDate();
    }

    /**
    * Sets the opr date of this room.
    *
    * @param oprDate the opr date of this room
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _room.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this room.
    *
    * @return the opr user of this room
    */
    @Override
    public long getOprUser() {
        return _room.getOprUser();
    }

    /**
    * Sets the opr user of this room.
    *
    * @param oprUser the opr user of this room
    */
    @Override
    public void setOprUser(long oprUser) {
        _room.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _room.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _room.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _room.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _room.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _room.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _room.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _room.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _room.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _room.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _room.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _room.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new RoomWrapper((Room) _room.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.booking.model.Room room) {
        return _room.compareTo(room);
    }

    @Override
    public int hashCode() {
        return _room.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.booking.model.Room> toCacheModel() {
        return _room.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room toEscapedModel() {
        return new RoomWrapper(_room.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room toUnescapedModel() {
        return new RoomWrapper(_room.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _room.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _room.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _room.persist();
    }

    @Override
    public java.lang.String getAgreementName()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _room.getAgreementName();
    }

    @Override
    public java.lang.String getAgreementURL()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _room.getAgreementURL();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.Image> getImages()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _room.getImages();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> getRoomBookings()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _room.getRoomBookings();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> getRoomBlockades()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _room.getRoomBlockades();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RoomWrapper)) {
            return false;
        }

        RoomWrapper roomWrapper = (RoomWrapper) obj;

        if (Validator.equals(_room, roomWrapper._room)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Room getWrappedRoom() {
        return _room;
    }

    @Override
    public Room getWrappedModel() {
        return _room;
    }

    @Override
    public void resetOriginalValues() {
        _room.resetOriginalValues();
    }
}
