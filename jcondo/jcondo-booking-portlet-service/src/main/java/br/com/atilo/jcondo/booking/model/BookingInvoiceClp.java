package br.com.atilo.jcondo.booking.model;

import br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class BookingInvoiceClp extends BaseModelImpl<BookingInvoice>
    implements BookingInvoice {
    private long _id;
    private long _bookingId;
    private long _providerId;
    private String _code;
    private double _amount;
    private double _tax;
    private Date _date;
    private String _link;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _bookingInvoiceRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.booking.service.ClpSerializer.class;

    public BookingInvoiceClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return BookingInvoice.class;
    }

    @Override
    public String getModelClassName() {
        return BookingInvoice.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("bookingId", getBookingId());
        attributes.put("providerId", getProviderId());
        attributes.put("code", getCode());
        attributes.put("amount", getAmount());
        attributes.put("tax", getTax());
        attributes.put("date", getDate());
        attributes.put("link", getLink());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long bookingId = (Long) attributes.get("bookingId");

        if (bookingId != null) {
            setBookingId(bookingId);
        }

        Long providerId = (Long) attributes.get("providerId");

        if (providerId != null) {
            setProviderId(providerId);
        }

        String code = (String) attributes.get("code");

        if (code != null) {
            setCode(code);
        }

        Double amount = (Double) attributes.get("amount");

        if (amount != null) {
            setAmount(amount);
        }

        Double tax = (Double) attributes.get("tax");

        if (tax != null) {
            setTax(tax);
        }

        Date date = (Date) attributes.get("date");

        if (date != null) {
            setDate(date);
        }

        String link = (String) attributes.get("link");

        if (link != null) {
            setLink(link);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_bookingInvoiceRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getBookingId() {
        return _bookingId;
    }

    @Override
    public void setBookingId(long bookingId) {
        _bookingId = bookingId;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setBookingId", long.class);

                method.invoke(_bookingInvoiceRemoteModel, bookingId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getProviderId() {
        return _providerId;
    }

    @Override
    public void setProviderId(long providerId) {
        _providerId = providerId;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setProviderId", long.class);

                method.invoke(_bookingInvoiceRemoteModel, providerId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCode() {
        return _code;
    }

    @Override
    public void setCode(String code) {
        _code = code;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setCode", String.class);

                method.invoke(_bookingInvoiceRemoteModel, code);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getAmount() {
        return _amount;
    }

    @Override
    public void setAmount(double amount) {
        _amount = amount;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setAmount", double.class);

                method.invoke(_bookingInvoiceRemoteModel, amount);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public double getTax() {
        return _tax;
    }

    @Override
    public void setTax(double tax) {
        _tax = tax;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setTax", double.class);

                method.invoke(_bookingInvoiceRemoteModel, tax);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDate() {
        return _date;
    }

    @Override
    public void setDate(Date date) {
        _date = date;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setDate", Date.class);

                method.invoke(_bookingInvoiceRemoteModel, date);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLink() {
        return _link;
    }

    @Override
    public void setLink(String link) {
        _link = link;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setLink", String.class);

                method.invoke(_bookingInvoiceRemoteModel, link);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_bookingInvoiceRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_bookingInvoiceRemoteModel != null) {
            try {
                Class<?> clazz = _bookingInvoiceRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_bookingInvoiceRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> getPayments() {
        try {
            String methodName = "getPayments";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.booking.model.Payment> returnObj = (java.util.List<br.com.atilo.jcondo.booking.model.Payment>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking getBooking() {
        try {
            String methodName = "getBooking";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.booking.model.Booking returnObj = (br.com.atilo.jcondo.booking.model.Booking) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setBooking(br.com.atilo.jcondo.booking.model.Booking booking) {
        try {
            String methodName = "setBooking";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.booking.model.Booking.class
                };

            Object[] parameterValues = new Object[] { booking };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getBookingInvoiceRemoteModel() {
        return _bookingInvoiceRemoteModel;
    }

    public void setBookingInvoiceRemoteModel(
        BaseModel<?> bookingInvoiceRemoteModel) {
        _bookingInvoiceRemoteModel = bookingInvoiceRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _bookingInvoiceRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_bookingInvoiceRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            BookingInvoiceLocalServiceUtil.addBookingInvoice(this);
        } else {
            BookingInvoiceLocalServiceUtil.updateBookingInvoice(this);
        }
    }

    @Override
    public BookingInvoice toEscapedModel() {
        return (BookingInvoice) ProxyUtil.newProxyInstance(BookingInvoice.class.getClassLoader(),
            new Class[] { BookingInvoice.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        BookingInvoiceClp clone = new BookingInvoiceClp();

        clone.setId(getId());
        clone.setBookingId(getBookingId());
        clone.setProviderId(getProviderId());
        clone.setCode(getCode());
        clone.setAmount(getAmount());
        clone.setTax(getTax());
        clone.setDate(getDate());
        clone.setLink(getLink());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(BookingInvoice bookingInvoice) {
        int value = 0;

        value = DateUtil.compareTo(getDate(), bookingInvoice.getDate());

        value = value * -1;

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BookingInvoiceClp)) {
            return false;
        }

        BookingInvoiceClp bookingInvoice = (BookingInvoiceClp) obj;

        long primaryKey = bookingInvoice.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", bookingId=");
        sb.append(getBookingId());
        sb.append(", providerId=");
        sb.append(getProviderId());
        sb.append(", code=");
        sb.append(getCode());
        sb.append(", amount=");
        sb.append(getAmount());
        sb.append(", tax=");
        sb.append(getTax());
        sb.append(", date=");
        sb.append(getDate());
        sb.append(", link=");
        sb.append(getLink());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(34);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.booking.model.BookingInvoice");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>bookingId</column-name><column-value><![CDATA[");
        sb.append(getBookingId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>providerId</column-name><column-value><![CDATA[");
        sb.append(getProviderId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>code</column-name><column-value><![CDATA[");
        sb.append(getCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>amount</column-name><column-value><![CDATA[");
        sb.append(getAmount());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>tax</column-name><column-value><![CDATA[");
        sb.append(getTax());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>date</column-name><column-value><![CDATA[");
        sb.append(getDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>link</column-name><column-value><![CDATA[");
        sb.append(getLink());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
