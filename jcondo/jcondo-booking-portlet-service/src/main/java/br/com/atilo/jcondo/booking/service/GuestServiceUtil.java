package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Guest. This utility wraps
 * {@link br.com.atilo.jcondo.booking.service.impl.GuestServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see GuestService
 * @see br.com.atilo.jcondo.booking.service.base.GuestServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.impl.GuestServiceImpl
 * @generated
 */
public class GuestServiceUtil {
    private static GuestService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.booking.service.impl.GuestServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.booking.model.Guest addGuest(
        long bookingId, java.lang.String name, java.lang.String surname)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addGuest(bookingId, name, surname);
    }

    public static br.com.atilo.jcondo.booking.model.Guest updateGuest(
        long guestId, long bookingId, java.lang.String name,
        java.lang.String surname, boolean checkedIn)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateGuest(guestId, bookingId, name, surname, checkedIn);
    }

    public static br.com.atilo.jcondo.booking.model.Guest deleteGuest(
        long guestId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteGuest(guestId);
    }

    public static java.util.List<br.com.atilo.jcondo.booking.model.Guest> getBookingGuests(
        long bookingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getBookingGuests(bookingId);
    }

    public static void clearService() {
        _service = null;
    }

    public static GuestService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    GuestService.class.getName());

            if (invokableService instanceof GuestService) {
                _service = (GuestService) invokableService;
            } else {
                _service = new GuestServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(GuestServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(GuestService service) {
    }
}
