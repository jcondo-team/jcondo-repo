package br.com.atilo.jcondo.booking.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.booking.service.http.RoomBlockadeServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.booking.service.http.RoomBlockadeServiceSoap
 * @generated
 */
public class RoomBlockadeSoap implements Serializable {
    private long _id;
    private long _roomId;
    private Date _beginDate;
    private Date _endDate;
    private String _description;
    private boolean _recursive;
    private Date _oprDate;
    private long _oprUser;

    public RoomBlockadeSoap() {
    }

    public static RoomBlockadeSoap toSoapModel(RoomBlockade model) {
        RoomBlockadeSoap soapModel = new RoomBlockadeSoap();

        soapModel.setId(model.getId());
        soapModel.setRoomId(model.getRoomId());
        soapModel.setBeginDate(model.getBeginDate());
        soapModel.setEndDate(model.getEndDate());
        soapModel.setDescription(model.getDescription());
        soapModel.setRecursive(model.getRecursive());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static RoomBlockadeSoap[] toSoapModels(RoomBlockade[] models) {
        RoomBlockadeSoap[] soapModels = new RoomBlockadeSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static RoomBlockadeSoap[][] toSoapModels(RoomBlockade[][] models) {
        RoomBlockadeSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new RoomBlockadeSoap[models.length][models[0].length];
        } else {
            soapModels = new RoomBlockadeSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static RoomBlockadeSoap[] toSoapModels(List<RoomBlockade> models) {
        List<RoomBlockadeSoap> soapModels = new ArrayList<RoomBlockadeSoap>(models.size());

        for (RoomBlockade model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new RoomBlockadeSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getRoomId() {
        return _roomId;
    }

    public void setRoomId(long roomId) {
        _roomId = roomId;
    }

    public Date getBeginDate() {
        return _beginDate;
    }

    public void setBeginDate(Date beginDate) {
        _beginDate = beginDate;
    }

    public Date getEndDate() {
        return _endDate;
    }

    public void setEndDate(Date endDate) {
        _endDate = endDate;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public boolean getRecursive() {
        return _recursive;
    }

    public boolean isRecursive() {
        return _recursive;
    }

    public void setRecursive(boolean recursive) {
        _recursive = recursive;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
