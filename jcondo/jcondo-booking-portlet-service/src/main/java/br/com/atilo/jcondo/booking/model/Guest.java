package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Guest service. Represents a row in the &quot;jco_booking_guest&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see GuestModel
 * @see br.com.atilo.jcondo.booking.model.impl.GuestImpl
 * @see br.com.atilo.jcondo.booking.model.impl.GuestModelImpl
 * @generated
 */
public interface Guest extends GuestModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.booking.model.impl.GuestImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
