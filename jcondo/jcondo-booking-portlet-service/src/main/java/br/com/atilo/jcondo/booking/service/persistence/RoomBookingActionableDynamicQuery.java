package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.RoomBooking;
import br.com.atilo.jcondo.booking.service.RoomBookingLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class RoomBookingActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public RoomBookingActionableDynamicQuery() throws SystemException {
        setBaseLocalService(RoomBookingLocalServiceUtil.getService());
        setClass(RoomBooking.class);

        setClassLoader(br.com.atilo.jcondo.booking.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
