package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Payment service. Represents a row in the &quot;jco_payment&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see PaymentModel
 * @see br.com.atilo.jcondo.booking.model.impl.PaymentImpl
 * @see br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl
 * @generated
 */
public interface Payment extends PaymentModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.booking.model.impl.PaymentImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
