package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.Guest;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the guest service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GuestPersistenceImpl
 * @see GuestUtil
 * @generated
 */
public interface GuestPersistence extends BasePersistence<Guest> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link GuestUtil} to access the guest persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the guests where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @return the matching guests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> findByBooking(
        long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the guests where bookingId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookingId the booking ID
    * @param start the lower bound of the range of guests
    * @param end the upper bound of the range of guests (not inclusive)
    * @return the range of matching guests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> findByBooking(
        long bookingId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the guests where bookingId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param bookingId the booking ID
    * @param start the lower bound of the range of guests
    * @param end the upper bound of the range of guests (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching guests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> findByBooking(
        long bookingId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first guest in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching guest
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a matching guest could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Guest findByBooking_First(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first guest in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching guest, or <code>null</code> if a matching guest could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Guest fetchByBooking_First(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last guest in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching guest
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a matching guest could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Guest findByBooking_Last(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last guest in the ordered set where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching guest, or <code>null</code> if a matching guest could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Guest fetchByBooking_Last(
        long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the guests before and after the current guest in the ordered set where bookingId = &#63;.
    *
    * @param guestId the primary key of the current guest
    * @param bookingId the booking ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next guest
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Guest[] findByBooking_PrevAndNext(
        long guestId, long bookingId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the guests where bookingId = &#63; from the database.
    *
    * @param bookingId the booking ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByBooking(long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of guests where bookingId = &#63;.
    *
    * @param bookingId the booking ID
    * @return the number of matching guests
    * @throws SystemException if a system exception occurred
    */
    public int countByBooking(long bookingId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the guest in the entity cache if it is enabled.
    *
    * @param guest the guest
    */
    public void cacheResult(br.com.atilo.jcondo.booking.model.Guest guest);

    /**
    * Caches the guests in the entity cache if it is enabled.
    *
    * @param guests the guests
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.Guest> guests);

    /**
    * Creates a new guest with the primary key. Does not add the guest to the database.
    *
    * @param guestId the primary key for the new guest
    * @return the new guest
    */
    public br.com.atilo.jcondo.booking.model.Guest create(long guestId);

    /**
    * Removes the guest with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param guestId the primary key of the guest
    * @return the guest that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Guest remove(long guestId)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.booking.model.Guest updateImpl(
        br.com.atilo.jcondo.booking.model.Guest guest)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the guest with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchGuestException} if it could not be found.
    *
    * @param guestId the primary key of the guest
    * @return the guest
    * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Guest findByPrimaryKey(
        long guestId)
        throws br.com.atilo.jcondo.booking.NoSuchGuestException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the guest with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param guestId the primary key of the guest
    * @return the guest, or <code>null</code> if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.Guest fetchByPrimaryKey(
        long guestId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the guests.
    *
    * @return the guests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the guests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of guests
    * @param end the upper bound of the range of guests (not inclusive)
    * @return the range of guests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the guests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of guests
    * @param end the upper bound of the range of guests (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of guests
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the guests from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of guests.
    *
    * @return the number of guests
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
