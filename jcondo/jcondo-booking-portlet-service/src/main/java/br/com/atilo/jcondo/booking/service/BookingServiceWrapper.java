package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookingService}.
 *
 * @author Brian Wing Shun Chan
 * @see BookingService
 * @generated
 */
public class BookingServiceWrapper implements BookingService,
    ServiceWrapper<BookingService> {
    private BookingService _bookingService;

    public BookingServiceWrapper(BookingService bookingService) {
        _bookingService = bookingService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _bookingService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _bookingService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _bookingService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getDomainBookings(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingService.getDomainBookings(domainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getPersonBookings(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingService.getPersonBookings(personId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        long roomId, java.util.Date beginDate)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingService.getBookings(roomId, beginDate);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        long roomId, java.util.Date fromDate, java.util.Date toDate)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingService.getBookings(roomId, fromDate, toDate);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking addBooking(long roomId,
        long domainId, long personId, java.util.Date beginDate,
        java.util.Date endDate,
        java.util.List<br.com.atilo.jcondo.booking.model.Guest> guests)
        throws java.lang.Exception {
        return _bookingService.addBooking(roomId, domainId, personId,
            beginDate, endDate, guests);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking cancelBooking(
        long bookingId) throws java.lang.Exception {
        return _bookingService.cancelBooking(bookingId);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking deleteBooking(
        long bookingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingService.deleteBooking(bookingId);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking deleteBooking(
        long bookingId, java.lang.String note)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingService.deleteBooking(bookingId, note);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public BookingService getWrappedBookingService() {
        return _bookingService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedBookingService(BookingService bookingService) {
        _bookingService = bookingService;
    }

    @Override
    public BookingService getWrappedService() {
        return _bookingService;
    }

    @Override
    public void setWrappedService(BookingService bookingService) {
        _bookingService = bookingService;
    }
}
