package br.com.atilo.jcondo.booking.model.datatype;

public enum BookingStatus {

	BOOKED("booking.status.booked"),
	SUSPENDED("booking.status.suspended"),
	CANCELLED("booking.status.cancelled"),
	DELETED("booking.status.deleted");

	private String label;

	private BookingStatus(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public static BookingStatus valueOf(int ordinal) {
		for (BookingStatus status : values()) {
			if (status.ordinal() == ordinal) {
				return status;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "@" + this.name();
	}

}
