package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link BookingInvoice}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingInvoice
 * @generated
 */
public class BookingInvoiceWrapper implements BookingInvoice,
    ModelWrapper<BookingInvoice> {
    private BookingInvoice _bookingInvoice;

    public BookingInvoiceWrapper(BookingInvoice bookingInvoice) {
        _bookingInvoice = bookingInvoice;
    }

    @Override
    public Class<?> getModelClass() {
        return BookingInvoice.class;
    }

    @Override
    public String getModelClassName() {
        return BookingInvoice.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("bookingId", getBookingId());
        attributes.put("providerId", getProviderId());
        attributes.put("code", getCode());
        attributes.put("amount", getAmount());
        attributes.put("tax", getTax());
        attributes.put("date", getDate());
        attributes.put("link", getLink());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long bookingId = (Long) attributes.get("bookingId");

        if (bookingId != null) {
            setBookingId(bookingId);
        }

        Long providerId = (Long) attributes.get("providerId");

        if (providerId != null) {
            setProviderId(providerId);
        }

        String code = (String) attributes.get("code");

        if (code != null) {
            setCode(code);
        }

        Double amount = (Double) attributes.get("amount");

        if (amount != null) {
            setAmount(amount);
        }

        Double tax = (Double) attributes.get("tax");

        if (tax != null) {
            setTax(tax);
        }

        Date date = (Date) attributes.get("date");

        if (date != null) {
            setDate(date);
        }

        String link = (String) attributes.get("link");

        if (link != null) {
            setLink(link);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this booking invoice.
    *
    * @return the primary key of this booking invoice
    */
    @Override
    public long getPrimaryKey() {
        return _bookingInvoice.getPrimaryKey();
    }

    /**
    * Sets the primary key of this booking invoice.
    *
    * @param primaryKey the primary key of this booking invoice
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _bookingInvoice.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this booking invoice.
    *
    * @return the ID of this booking invoice
    */
    @Override
    public long getId() {
        return _bookingInvoice.getId();
    }

    /**
    * Sets the ID of this booking invoice.
    *
    * @param id the ID of this booking invoice
    */
    @Override
    public void setId(long id) {
        _bookingInvoice.setId(id);
    }

    /**
    * Returns the booking ID of this booking invoice.
    *
    * @return the booking ID of this booking invoice
    */
    @Override
    public long getBookingId() {
        return _bookingInvoice.getBookingId();
    }

    /**
    * Sets the booking ID of this booking invoice.
    *
    * @param bookingId the booking ID of this booking invoice
    */
    @Override
    public void setBookingId(long bookingId) {
        _bookingInvoice.setBookingId(bookingId);
    }

    /**
    * Returns the provider ID of this booking invoice.
    *
    * @return the provider ID of this booking invoice
    */
    @Override
    public long getProviderId() {
        return _bookingInvoice.getProviderId();
    }

    /**
    * Sets the provider ID of this booking invoice.
    *
    * @param providerId the provider ID of this booking invoice
    */
    @Override
    public void setProviderId(long providerId) {
        _bookingInvoice.setProviderId(providerId);
    }

    /**
    * Returns the code of this booking invoice.
    *
    * @return the code of this booking invoice
    */
    @Override
    public java.lang.String getCode() {
        return _bookingInvoice.getCode();
    }

    /**
    * Sets the code of this booking invoice.
    *
    * @param code the code of this booking invoice
    */
    @Override
    public void setCode(java.lang.String code) {
        _bookingInvoice.setCode(code);
    }

    /**
    * Returns the amount of this booking invoice.
    *
    * @return the amount of this booking invoice
    */
    @Override
    public double getAmount() {
        return _bookingInvoice.getAmount();
    }

    /**
    * Sets the amount of this booking invoice.
    *
    * @param amount the amount of this booking invoice
    */
    @Override
    public void setAmount(double amount) {
        _bookingInvoice.setAmount(amount);
    }

    /**
    * Returns the tax of this booking invoice.
    *
    * @return the tax of this booking invoice
    */
    @Override
    public double getTax() {
        return _bookingInvoice.getTax();
    }

    /**
    * Sets the tax of this booking invoice.
    *
    * @param tax the tax of this booking invoice
    */
    @Override
    public void setTax(double tax) {
        _bookingInvoice.setTax(tax);
    }

    /**
    * Returns the date of this booking invoice.
    *
    * @return the date of this booking invoice
    */
    @Override
    public java.util.Date getDate() {
        return _bookingInvoice.getDate();
    }

    /**
    * Sets the date of this booking invoice.
    *
    * @param date the date of this booking invoice
    */
    @Override
    public void setDate(java.util.Date date) {
        _bookingInvoice.setDate(date);
    }

    /**
    * Returns the link of this booking invoice.
    *
    * @return the link of this booking invoice
    */
    @Override
    public java.lang.String getLink() {
        return _bookingInvoice.getLink();
    }

    /**
    * Sets the link of this booking invoice.
    *
    * @param link the link of this booking invoice
    */
    @Override
    public void setLink(java.lang.String link) {
        _bookingInvoice.setLink(link);
    }

    /**
    * Returns the opr date of this booking invoice.
    *
    * @return the opr date of this booking invoice
    */
    @Override
    public java.util.Date getOprDate() {
        return _bookingInvoice.getOprDate();
    }

    /**
    * Sets the opr date of this booking invoice.
    *
    * @param oprDate the opr date of this booking invoice
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _bookingInvoice.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this booking invoice.
    *
    * @return the opr user of this booking invoice
    */
    @Override
    public long getOprUser() {
        return _bookingInvoice.getOprUser();
    }

    /**
    * Sets the opr user of this booking invoice.
    *
    * @param oprUser the opr user of this booking invoice
    */
    @Override
    public void setOprUser(long oprUser) {
        _bookingInvoice.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _bookingInvoice.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _bookingInvoice.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _bookingInvoice.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _bookingInvoice.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _bookingInvoice.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _bookingInvoice.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _bookingInvoice.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _bookingInvoice.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _bookingInvoice.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _bookingInvoice.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _bookingInvoice.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new BookingInvoiceWrapper((BookingInvoice) _bookingInvoice.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.booking.model.BookingInvoice bookingInvoice) {
        return _bookingInvoice.compareTo(bookingInvoice);
    }

    @Override
    public int hashCode() {
        return _bookingInvoice.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.booking.model.BookingInvoice> toCacheModel() {
        return _bookingInvoice.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice toEscapedModel() {
        return new BookingInvoiceWrapper(_bookingInvoice.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice toUnescapedModel() {
        return new BookingInvoiceWrapper(_bookingInvoice.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _bookingInvoice.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _bookingInvoice.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _bookingInvoice.persist();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking getBooking()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoice.getBooking();
    }

    @Override
    public void setBooking(br.com.atilo.jcondo.booking.model.Booking booking)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _bookingInvoice.setBooking(booking);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> getPayments()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingInvoice.getPayments();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BookingInvoiceWrapper)) {
            return false;
        }

        BookingInvoiceWrapper bookingInvoiceWrapper = (BookingInvoiceWrapper) obj;

        if (Validator.equals(_bookingInvoice,
                    bookingInvoiceWrapper._bookingInvoice)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public BookingInvoice getWrappedBookingInvoice() {
        return _bookingInvoice;
    }

    @Override
    public BookingInvoice getWrappedModel() {
        return _bookingInvoice;
    }

    @Override
    public void resetOriginalValues() {
        _bookingInvoice.resetOriginalValues();
    }
}
