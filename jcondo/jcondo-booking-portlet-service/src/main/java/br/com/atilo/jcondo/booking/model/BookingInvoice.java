package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the BookingInvoice service. Represents a row in the &quot;jco_booking_invoice&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see BookingInvoiceModel
 * @see br.com.atilo.jcondo.booking.model.impl.BookingInvoiceImpl
 * @see br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl
 * @generated
 */
public interface BookingInvoice extends BookingInvoiceModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.booking.model.Booking getBooking()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setBooking(br.com.atilo.jcondo.booking.model.Booking booking)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> getPayments()
        throws com.liferay.portal.kernel.exception.SystemException;
}
