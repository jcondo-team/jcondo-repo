package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PaymentLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PaymentLocalService
 * @generated
 */
public class PaymentLocalServiceWrapper implements PaymentLocalService,
    ServiceWrapper<PaymentLocalService> {
    private PaymentLocalService _paymentLocalService;

    public PaymentLocalServiceWrapper(PaymentLocalService paymentLocalService) {
        _paymentLocalService = paymentLocalService;
    }

    /**
    * Adds the payment to the database. Also notifies the appropriate model listeners.
    *
    * @param payment the payment
    * @return the payment that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Payment addPayment(
        br.com.atilo.jcondo.booking.model.Payment payment)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.addPayment(payment);
    }

    /**
    * Creates a new payment with the primary key. Does not add the payment to the database.
    *
    * @param id the primary key for the new payment
    * @return the new payment
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Payment createPayment(long id) {
        return _paymentLocalService.createPayment(id);
    }

    /**
    * Deletes the payment with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the payment
    * @return the payment that was removed
    * @throws PortalException if a payment with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Payment deletePayment(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.deletePayment(id);
    }

    /**
    * Deletes the payment from the database. Also notifies the appropriate model listeners.
    *
    * @param payment the payment
    * @return the payment that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Payment deletePayment(
        br.com.atilo.jcondo.booking.model.Payment payment)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.deletePayment(payment);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _paymentLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Payment fetchPayment(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.fetchPayment(id);
    }

    /**
    * Returns the payment with the primary key.
    *
    * @param id the primary key of the payment
    * @return the payment
    * @throws PortalException if a payment with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Payment getPayment(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.getPayment(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the payments.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of payments
    * @param end the upper bound of the range of payments (not inclusive)
    * @return the range of payments
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> getPayments(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.getPayments(start, end);
    }

    /**
    * Returns the number of payments.
    *
    * @return the number of payments
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getPaymentsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.getPaymentsCount();
    }

    /**
    * Updates the payment in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param payment the payment
    * @return the payment that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Payment updatePayment(
        br.com.atilo.jcondo.booking.model.Payment payment)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.updatePayment(payment);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _paymentLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _paymentLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _paymentLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Payment addPayment(
        java.lang.String token, long invoiceId, java.lang.String invoiceCode)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.addPayment(token, invoiceId, invoiceCode);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Payment updatePayment(
        br.com.atilo.jcondo.booking.model.Payment payment, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.updatePayment(payment, merge);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Payment> getPayments(
        long invoiceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _paymentLocalService.getPayments(invoiceId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PaymentLocalService getWrappedPaymentLocalService() {
        return _paymentLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPaymentLocalService(
        PaymentLocalService paymentLocalService) {
        _paymentLocalService = paymentLocalService;
    }

    @Override
    public PaymentLocalService getWrappedService() {
        return _paymentLocalService;
    }

    @Override
    public void setWrappedService(PaymentLocalService paymentLocalService) {
        _paymentLocalService = paymentLocalService;
    }
}
