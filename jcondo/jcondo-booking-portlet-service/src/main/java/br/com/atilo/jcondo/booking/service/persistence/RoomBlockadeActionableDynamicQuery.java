package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.RoomBlockade;
import br.com.atilo.jcondo.booking.service.RoomBlockadeLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class RoomBlockadeActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public RoomBlockadeActionableDynamicQuery() throws SystemException {
        setBaseLocalService(RoomBlockadeLocalServiceUtil.getService());
        setClass(RoomBlockade.class);

        setClassLoader(br.com.atilo.jcondo.booking.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
