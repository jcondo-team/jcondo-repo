package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.RoomBlockade;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the room blockade service. This utility wraps {@link RoomBlockadePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomBlockadePersistence
 * @see RoomBlockadePersistenceImpl
 * @generated
 */
public class RoomBlockadeUtil {
    private static RoomBlockadePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(RoomBlockade roomBlockade) {
        getPersistence().clearCache(roomBlockade);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<RoomBlockade> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<RoomBlockade> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<RoomBlockade> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static RoomBlockade update(RoomBlockade roomBlockade)
        throws SystemException {
        return getPersistence().update(roomBlockade);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static RoomBlockade update(RoomBlockade roomBlockade,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(roomBlockade, serviceContext);
    }

    /**
    * Returns all the room blockades where roomId = &#63;.
    *
    * @param roomId the room ID
    * @return the matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRoom(
        long roomId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom(roomId);
    }

    /**
    * Returns a range of all the room blockades where roomId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @return the range of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRoom(
        long roomId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom(roomId, start, end);
    }

    /**
    * Returns an ordered range of all the room blockades where roomId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRoom(
        long roomId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom(roomId, start, end, orderByComparator);
    }

    /**
    * Returns the first room blockade in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade findByRoom_First(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom_First(roomId, orderByComparator);
    }

    /**
    * Returns the first room blockade in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room blockade, or <code>null</code> if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade fetchByRoom_First(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByRoom_First(roomId, orderByComparator);
    }

    /**
    * Returns the last room blockade in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade findByRoom_Last(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRoom_Last(roomId, orderByComparator);
    }

    /**
    * Returns the last room blockade in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room blockade, or <code>null</code> if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade fetchByRoom_Last(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByRoom_Last(roomId, orderByComparator);
    }

    /**
    * Returns the room blockades before and after the current room blockade in the ordered set where roomId = &#63;.
    *
    * @param id the primary key of the current room blockade
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade[] findByRoom_PrevAndNext(
        long id, long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRoom_PrevAndNext(id, roomId, orderByComparator);
    }

    /**
    * Removes all the room blockades where roomId = &#63; from the database.
    *
    * @param roomId the room ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByRoom(long roomId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByRoom(roomId);
    }

    /**
    * Returns the number of room blockades where roomId = &#63;.
    *
    * @param roomId the room ID
    * @return the number of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public static int countByRoom(long roomId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByRoom(roomId);
    }

    /**
    * Returns all the room blockades where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @return the matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRecursive(
        long roomId, boolean recursive)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRecursive(roomId, recursive);
    }

    /**
    * Returns a range of all the room blockades where roomId = &#63; and recursive = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @return the range of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRecursive(
        long roomId, boolean recursive, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRecursive(roomId, recursive, start, end);
    }

    /**
    * Returns an ordered range of all the room blockades where roomId = &#63; and recursive = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findByRecursive(
        long roomId, boolean recursive, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRecursive(roomId, recursive, start, end,
            orderByComparator);
    }

    /**
    * Returns the first room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade findByRecursive_First(
        long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRecursive_First(roomId, recursive, orderByComparator);
    }

    /**
    * Returns the first room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room blockade, or <code>null</code> if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade fetchByRecursive_First(
        long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByRecursive_First(roomId, recursive, orderByComparator);
    }

    /**
    * Returns the last room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade findByRecursive_Last(
        long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRecursive_Last(roomId, recursive, orderByComparator);
    }

    /**
    * Returns the last room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room blockade, or <code>null</code> if a matching room blockade could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade fetchByRecursive_Last(
        long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByRecursive_Last(roomId, recursive, orderByComparator);
    }

    /**
    * Returns the room blockades before and after the current room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
    *
    * @param id the primary key of the current room blockade
    * @param roomId the room ID
    * @param recursive the recursive
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade[] findByRecursive_PrevAndNext(
        long id, long roomId, boolean recursive,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRecursive_PrevAndNext(id, roomId, recursive,
            orderByComparator);
    }

    /**
    * Removes all the room blockades where roomId = &#63; and recursive = &#63; from the database.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @throws SystemException if a system exception occurred
    */
    public static void removeByRecursive(long roomId, boolean recursive)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByRecursive(roomId, recursive);
    }

    /**
    * Returns the number of room blockades where roomId = &#63; and recursive = &#63;.
    *
    * @param roomId the room ID
    * @param recursive the recursive
    * @return the number of matching room blockades
    * @throws SystemException if a system exception occurred
    */
    public static int countByRecursive(long roomId, boolean recursive)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByRecursive(roomId, recursive);
    }

    /**
    * Caches the room blockade in the entity cache if it is enabled.
    *
    * @param roomBlockade the room blockade
    */
    public static void cacheResult(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade) {
        getPersistence().cacheResult(roomBlockade);
    }

    /**
    * Caches the room blockades in the entity cache if it is enabled.
    *
    * @param roomBlockades the room blockades
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> roomBlockades) {
        getPersistence().cacheResult(roomBlockades);
    }

    /**
    * Creates a new room blockade with the primary key. Does not add the room blockade to the database.
    *
    * @param id the primary key for the new room blockade
    * @return the new room blockade
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the room blockade with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the room blockade
    * @return the room blockade that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade remove(long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.booking.model.RoomBlockade updateImpl(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(roomBlockade);
    }

    /**
    * Returns the room blockade with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException} if it could not be found.
    *
    * @param id the primary key of the room blockade
    * @return the room blockade
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the room blockade with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the room blockade
    * @return the room blockade, or <code>null</code> if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.RoomBlockade fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the room blockades.
    *
    * @return the room blockades
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the room blockades.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @return the range of room blockades
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the room blockades.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of room blockades
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the room blockades from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of room blockades.
    *
    * @return the number of room blockades
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static RoomBlockadePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (RoomBlockadePersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.booking.service.ClpSerializer.getServletContextName(),
                    RoomBlockadePersistence.class.getName());

            ReferenceRegistry.registerReference(RoomBlockadeUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(RoomBlockadePersistence persistence) {
    }
}
