package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class RoomActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public RoomActionableDynamicQuery() throws SystemException {
        setBaseLocalService(RoomLocalServiceUtil.getService());
        setClass(Room.class);

        setClassLoader(br.com.atilo.jcondo.booking.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
