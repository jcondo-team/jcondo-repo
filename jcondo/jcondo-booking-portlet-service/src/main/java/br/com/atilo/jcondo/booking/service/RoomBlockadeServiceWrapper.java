package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RoomBlockadeService}.
 *
 * @author Brian Wing Shun Chan
 * @see RoomBlockadeService
 * @generated
 */
public class RoomBlockadeServiceWrapper implements RoomBlockadeService,
    ServiceWrapper<RoomBlockadeService> {
    private RoomBlockadeService _roomBlockadeService;

    public RoomBlockadeServiceWrapper(RoomBlockadeService roomBlockadeService) {
        _roomBlockadeService = roomBlockadeService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _roomBlockadeService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _roomBlockadeService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _roomBlockadeService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade addRoomBlockade(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeService.addRoomBlockade(roomBlockade);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade deleteRoomBlockade(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeService.deleteRoomBlockade(roomBlockade);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public RoomBlockadeService getWrappedRoomBlockadeService() {
        return _roomBlockadeService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedRoomBlockadeService(
        RoomBlockadeService roomBlockadeService) {
        _roomBlockadeService = roomBlockadeService;
    }

    @Override
    public RoomBlockadeService getWrappedService() {
        return _roomBlockadeService;
    }

    @Override
    public void setWrappedService(RoomBlockadeService roomBlockadeService) {
        _roomBlockadeService = roomBlockadeService;
    }
}
