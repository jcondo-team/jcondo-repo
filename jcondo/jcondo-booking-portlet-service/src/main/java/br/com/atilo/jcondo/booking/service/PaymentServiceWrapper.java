package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PaymentService}.
 *
 * @author Brian Wing Shun Chan
 * @see PaymentService
 * @generated
 */
public class PaymentServiceWrapper implements PaymentService,
    ServiceWrapper<PaymentService> {
    private PaymentService _paymentService;

    public PaymentServiceWrapper(PaymentService paymentService) {
        _paymentService = paymentService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _paymentService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _paymentService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _paymentService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public java.lang.String addPayment(java.lang.String paymentToken,
        java.lang.String chargeReference, java.lang.String chargeCode) {
        return _paymentService.addPayment(paymentToken, chargeReference,
            chargeCode);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PaymentService getWrappedPaymentService() {
        return _paymentService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPaymentService(PaymentService paymentService) {
        _paymentService = paymentService;
    }

    @Override
    public PaymentService getWrappedService() {
        return _paymentService;
    }

    @Override
    public void setWrappedService(PaymentService paymentService) {
        _paymentService = paymentService;
    }
}
