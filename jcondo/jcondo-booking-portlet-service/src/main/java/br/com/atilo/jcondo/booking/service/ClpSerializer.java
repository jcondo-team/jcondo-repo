package br.com.atilo.jcondo.booking.service;

import br.com.atilo.jcondo.booking.model.BookingClp;
import br.com.atilo.jcondo.booking.model.BookingInvoiceClp;
import br.com.atilo.jcondo.booking.model.BookingNoteClp;
import br.com.atilo.jcondo.booking.model.GuestClp;
import br.com.atilo.jcondo.booking.model.PaymentClp;
import br.com.atilo.jcondo.booking.model.RoomBlockadeClp;
import br.com.atilo.jcondo.booking.model.RoomBookingClp;
import br.com.atilo.jcondo.booking.model.RoomClp;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;


public class ClpSerializer {
    private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
    private static String _servletContextName;
    private static boolean _useReflectionToTranslateThrowable = true;

    public static String getServletContextName() {
        if (Validator.isNotNull(_servletContextName)) {
            return _servletContextName;
        }

        synchronized (ClpSerializer.class) {
            if (Validator.isNotNull(_servletContextName)) {
                return _servletContextName;
            }

            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Class<?> portletPropsClass = classLoader.loadClass(
                        "com.liferay.util.portlet.PortletProps");

                Method getMethod = portletPropsClass.getMethod("get",
                        new Class<?>[] { String.class });

                String portletPropsServletContextName = (String) getMethod.invoke(null,
                        "jcondo-booking-portlet-deployment-context");

                if (Validator.isNotNull(portletPropsServletContextName)) {
                    _servletContextName = portletPropsServletContextName;
                }
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info(
                        "Unable to locate deployment context from portlet properties");
                }
            }

            if (Validator.isNull(_servletContextName)) {
                try {
                    String propsUtilServletContextName = PropsUtil.get(
                            "jcondo-booking-portlet-deployment-context");

                    if (Validator.isNotNull(propsUtilServletContextName)) {
                        _servletContextName = propsUtilServletContextName;
                    }
                } catch (Throwable t) {
                    if (_log.isInfoEnabled()) {
                        _log.info(
                            "Unable to locate deployment context from portal properties");
                    }
                }
            }

            if (Validator.isNull(_servletContextName)) {
                _servletContextName = "jcondo-booking-portlet";
            }

            return _servletContextName;
        }
    }

    public static Object translateInput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(BookingClp.class.getName())) {
            return translateInputBooking(oldModel);
        }

        if (oldModelClassName.equals(BookingInvoiceClp.class.getName())) {
            return translateInputBookingInvoice(oldModel);
        }

        if (oldModelClassName.equals(BookingNoteClp.class.getName())) {
            return translateInputBookingNote(oldModel);
        }

        if (oldModelClassName.equals(GuestClp.class.getName())) {
            return translateInputGuest(oldModel);
        }

        if (oldModelClassName.equals(PaymentClp.class.getName())) {
            return translateInputPayment(oldModel);
        }

        if (oldModelClassName.equals(RoomClp.class.getName())) {
            return translateInputRoom(oldModel);
        }

        if (oldModelClassName.equals(RoomBlockadeClp.class.getName())) {
            return translateInputRoomBlockade(oldModel);
        }

        if (oldModelClassName.equals(RoomBookingClp.class.getName())) {
            return translateInputRoomBooking(oldModel);
        }

        return oldModel;
    }

    public static Object translateInput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateInput(curObj));
        }

        return newList;
    }

    public static Object translateInputBooking(BaseModel<?> oldModel) {
        BookingClp oldClpModel = (BookingClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getBookingRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputBookingInvoice(BaseModel<?> oldModel) {
        BookingInvoiceClp oldClpModel = (BookingInvoiceClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getBookingInvoiceRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputBookingNote(BaseModel<?> oldModel) {
        BookingNoteClp oldClpModel = (BookingNoteClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getBookingNoteRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputGuest(BaseModel<?> oldModel) {
        GuestClp oldClpModel = (GuestClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getGuestRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPayment(BaseModel<?> oldModel) {
        PaymentClp oldClpModel = (PaymentClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPaymentRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputRoom(BaseModel<?> oldModel) {
        RoomClp oldClpModel = (RoomClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getRoomRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputRoomBlockade(BaseModel<?> oldModel) {
        RoomBlockadeClp oldClpModel = (RoomBlockadeClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getRoomBlockadeRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputRoomBooking(BaseModel<?> oldModel) {
        RoomBookingClp oldClpModel = (RoomBookingClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getRoomBookingRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateInput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateInput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Object translateOutput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.booking.model.impl.BookingImpl")) {
            return translateOutputBooking(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.booking.model.impl.BookingInvoiceImpl")) {
            return translateOutputBookingInvoice(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.booking.model.impl.BookingNoteImpl")) {
            return translateOutputBookingNote(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.booking.model.impl.GuestImpl")) {
            return translateOutputGuest(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.booking.model.impl.PaymentImpl")) {
            return translateOutputPayment(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.booking.model.impl.RoomImpl")) {
            return translateOutputRoom(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.booking.model.impl.RoomBlockadeImpl")) {
            return translateOutputRoomBlockade(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.booking.model.impl.RoomBookingImpl")) {
            return translateOutputRoomBooking(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        return oldModel;
    }

    public static Object translateOutput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateOutput(curObj));
        }

        return newList;
    }

    public static Object translateOutput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateOutput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateOutput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Throwable translateThrowable(Throwable throwable) {
        if (_useReflectionToTranslateThrowable) {
            try {
                UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

                objectOutputStream.writeObject(throwable);

                objectOutputStream.flush();
                objectOutputStream.close();

                UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
                        0, unsyncByteArrayOutputStream.size());

                Thread currentThread = Thread.currentThread();

                ClassLoader contextClassLoader = currentThread.getContextClassLoader();

                ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
                        contextClassLoader);

                throwable = (Throwable) objectInputStream.readObject();

                objectInputStream.close();

                return throwable;
            } catch (SecurityException se) {
                if (_log.isInfoEnabled()) {
                    _log.info("Do not use reflection to translate throwable");
                }

                _useReflectionToTranslateThrowable = false;
            } catch (Throwable throwable2) {
                _log.error(throwable2, throwable2);

                return throwable2;
            }
        }

        Class<?> clazz = throwable.getClass();

        String className = clazz.getName();

        if (className.equals(PortalException.class.getName())) {
            return new PortalException();
        }

        if (className.equals(SystemException.class.getName())) {
            return new SystemException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.booking.NoSuchBookingException")) {
            return new br.com.atilo.jcondo.booking.NoSuchBookingException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException")) {
            return new br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.booking.NoSuchBookingNoteException")) {
            return new br.com.atilo.jcondo.booking.NoSuchBookingNoteException();
        }

        if (className.equals("br.com.atilo.jcondo.booking.NoSuchGuestException")) {
            return new br.com.atilo.jcondo.booking.NoSuchGuestException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.booking.NoSuchPaymentException")) {
            return new br.com.atilo.jcondo.booking.NoSuchPaymentException();
        }

        if (className.equals("br.com.atilo.jcondo.booking.NoSuchRoomException")) {
            return new br.com.atilo.jcondo.booking.NoSuchRoomException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException")) {
            return new br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.booking.NoSuchRoomBookingException")) {
            return new br.com.atilo.jcondo.booking.NoSuchRoomBookingException();
        }

        return throwable;
    }

    public static Object translateOutputBooking(BaseModel<?> oldModel) {
        BookingClp newModel = new BookingClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setBookingRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputBookingInvoice(BaseModel<?> oldModel) {
        BookingInvoiceClp newModel = new BookingInvoiceClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setBookingInvoiceRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputBookingNote(BaseModel<?> oldModel) {
        BookingNoteClp newModel = new BookingNoteClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setBookingNoteRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputGuest(BaseModel<?> oldModel) {
        GuestClp newModel = new GuestClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setGuestRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPayment(BaseModel<?> oldModel) {
        PaymentClp newModel = new PaymentClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPaymentRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputRoom(BaseModel<?> oldModel) {
        RoomClp newModel = new RoomClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setRoomRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputRoomBlockade(BaseModel<?> oldModel) {
        RoomBlockadeClp newModel = new RoomBlockadeClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setRoomBlockadeRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputRoomBooking(BaseModel<?> oldModel) {
        RoomBookingClp newModel = new RoomBookingClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setRoomBookingRemoteModel(oldModel);

        return newModel;
    }
}
