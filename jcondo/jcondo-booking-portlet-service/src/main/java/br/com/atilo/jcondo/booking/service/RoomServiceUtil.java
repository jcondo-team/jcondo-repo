package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Room. This utility wraps
 * {@link br.com.atilo.jcondo.booking.service.impl.RoomServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see RoomService
 * @see br.com.atilo.jcondo.booking.service.base.RoomServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.impl.RoomServiceImpl
 * @generated
 */
public class RoomServiceUtil {
    private static RoomService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.booking.service.impl.RoomServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.booking.model.Room addRoom(
        java.lang.String name, java.lang.String description, double price,
        int capacity, boolean available, boolean bookable,
        java.io.File agreement)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addRoom(name, description, price, capacity, available,
            bookable, agreement);
    }

    public static br.com.atilo.jcondo.booking.model.Room addRoom(
        br.com.atilo.jcondo.booking.model.Room room)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addRoom(room);
    }

    public static br.com.atilo.jcondo.booking.model.Room updateRoom(
        long roomId, java.lang.String name, java.lang.String description,
        double price, int capacity, boolean available, boolean bookable,
        java.io.File agreement)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateRoom(roomId, name, description, price, capacity,
            available, bookable, agreement);
    }

    public static br.com.atilo.jcondo.booking.model.Room updateRoom(
        br.com.atilo.jcondo.booking.model.Room room)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateRoom(room);
    }

    public static br.com.atilo.jcondo.booking.model.Room deleteRoom(
        br.com.atilo.jcondo.booking.model.Room room)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteRoom(room);
    }

    public static br.com.atilo.jcondo.Image addRoomPicture(long roomId,
        br.com.atilo.jcondo.Image image)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addRoomPicture(roomId, image);
    }

    public static br.com.atilo.jcondo.Image deleteRoomPicture(long roomId,
        long imageId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteRoomPicture(roomId, imageId);
    }

    public static java.util.List<br.com.atilo.jcondo.booking.model.Room> getRooms(
        boolean onlyAvailables, boolean onlyBookables)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getRooms(onlyAvailables, onlyBookables);
    }

    public static void checkRoomAvailability(long roomId, java.util.Date date)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService().checkRoomAvailability(roomId, date);
    }

    public static void clearService() {
        _service = null;
    }

    public static RoomService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    RoomService.class.getName());

            if (invokableService instanceof RoomService) {
                _service = (RoomService) invokableService;
            } else {
                _service = new RoomServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(RoomServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(RoomService service) {
    }
}
