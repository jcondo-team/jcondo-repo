package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class BookingInvoiceActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public BookingInvoiceActionableDynamicQuery() throws SystemException {
        setBaseLocalService(BookingInvoiceLocalServiceUtil.getService());
        setClass(BookingInvoice.class);

        setClassLoader(br.com.atilo.jcondo.booking.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
