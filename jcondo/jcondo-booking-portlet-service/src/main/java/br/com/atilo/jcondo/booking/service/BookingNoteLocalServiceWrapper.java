package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookingNoteLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see BookingNoteLocalService
 * @generated
 */
public class BookingNoteLocalServiceWrapper implements BookingNoteLocalService,
    ServiceWrapper<BookingNoteLocalService> {
    private BookingNoteLocalService _bookingNoteLocalService;

    public BookingNoteLocalServiceWrapper(
        BookingNoteLocalService bookingNoteLocalService) {
        _bookingNoteLocalService = bookingNoteLocalService;
    }

    /**
    * Adds the booking note to the database. Also notifies the appropriate model listeners.
    *
    * @param bookingNote the booking note
    * @return the booking note that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote addBookingNote(
        br.com.atilo.jcondo.booking.model.BookingNote bookingNote)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.addBookingNote(bookingNote);
    }

    /**
    * Creates a new booking note with the primary key. Does not add the booking note to the database.
    *
    * @param id the primary key for the new booking note
    * @return the new booking note
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote createBookingNote(
        long id) {
        return _bookingNoteLocalService.createBookingNote(id);
    }

    /**
    * Deletes the booking note with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the booking note
    * @return the booking note that was removed
    * @throws PortalException if a booking note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote deleteBookingNote(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.deleteBookingNote(id);
    }

    /**
    * Deletes the booking note from the database. Also notifies the appropriate model listeners.
    *
    * @param bookingNote the booking note
    * @return the booking note that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote deleteBookingNote(
        br.com.atilo.jcondo.booking.model.BookingNote bookingNote)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.deleteBookingNote(bookingNote);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _bookingNoteLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote fetchBookingNote(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.fetchBookingNote(id);
    }

    /**
    * Returns the booking note with the primary key.
    *
    * @param id the primary key of the booking note
    * @return the booking note
    * @throws PortalException if a booking note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote getBookingNote(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.getBookingNote(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the booking notes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of booking notes
    * @param end the upper bound of the range of booking notes (not inclusive)
    * @return the range of booking notes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.BookingNote> getBookingNotes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.getBookingNotes(start, end);
    }

    /**
    * Returns the number of booking notes.
    *
    * @return the number of booking notes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getBookingNotesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.getBookingNotesCount();
    }

    /**
    * Updates the booking note in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param bookingNote the booking note
    * @return the booking note that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote updateBookingNote(
        br.com.atilo.jcondo.booking.model.BookingNote bookingNote)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteLocalService.updateBookingNote(bookingNote);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _bookingNoteLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _bookingNoteLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _bookingNoteLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public BookingNoteLocalService getWrappedBookingNoteLocalService() {
        return _bookingNoteLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedBookingNoteLocalService(
        BookingNoteLocalService bookingNoteLocalService) {
        _bookingNoteLocalService = bookingNoteLocalService;
    }

    @Override
    public BookingNoteLocalService getWrappedService() {
        return _bookingNoteLocalService;
    }

    @Override
    public void setWrappedService(
        BookingNoteLocalService bookingNoteLocalService) {
        _bookingNoteLocalService = bookingNoteLocalService;
    }
}
