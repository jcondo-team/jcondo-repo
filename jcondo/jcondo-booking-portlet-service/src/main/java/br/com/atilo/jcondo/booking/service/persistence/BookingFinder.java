package br.com.atilo.jcondo.booking.service.persistence;

public interface BookingFinder {
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> findOverdueBookings();
}
