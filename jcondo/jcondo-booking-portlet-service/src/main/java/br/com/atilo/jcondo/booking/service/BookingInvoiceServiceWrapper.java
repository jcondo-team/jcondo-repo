package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookingInvoiceService}.
 *
 * @author Brian Wing Shun Chan
 * @see BookingInvoiceService
 * @generated
 */
public class BookingInvoiceServiceWrapper implements BookingInvoiceService,
    ServiceWrapper<BookingInvoiceService> {
    private BookingInvoiceService _bookingInvoiceService;

    public BookingInvoiceServiceWrapper(
        BookingInvoiceService bookingInvoiceService) {
        _bookingInvoiceService = bookingInvoiceService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _bookingInvoiceService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _bookingInvoiceService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _bookingInvoiceService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public BookingInvoiceService getWrappedBookingInvoiceService() {
        return _bookingInvoiceService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedBookingInvoiceService(
        BookingInvoiceService bookingInvoiceService) {
        _bookingInvoiceService = bookingInvoiceService;
    }

    @Override
    public BookingInvoiceService getWrappedService() {
        return _bookingInvoiceService;
    }

    @Override
    public void setWrappedService(BookingInvoiceService bookingInvoiceService) {
        _bookingInvoiceService = bookingInvoiceService;
    }
}
