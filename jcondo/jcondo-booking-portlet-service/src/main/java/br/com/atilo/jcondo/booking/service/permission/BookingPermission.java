package br.com.atilo.jcondo.booking.service.permission;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.permission.BasePermission;

public class BookingPermission extends BasePermission {

	public static boolean hasPermission(Permission permission, Booking booking) throws PortalException, SystemException {
		Organization organization = OrganizationLocalServiceUtil.getOrganization(getOrganizationDomain(booking.getDomainId()));
		return hasPermission(permission, Booking.class, booking.getId(), organization);
	}

	public static boolean hasPermission(Permission permission, long personId, long domainId) throws Exception {
		try {
			if (FlatLocalServiceUtil.getFlat(domainId).getDefaulting()) {
				throw new BusinessException("exception.booking.flat.defaulting");
			}
		} catch (NoSuchFlatException e) {
		}

		Organization organization = OrganizationLocalServiceUtil.getOrganization(getOrganizationDomain(domainId));
		return hasPermission(permission, personId, Booking.class, -1, organization);
	}

}
