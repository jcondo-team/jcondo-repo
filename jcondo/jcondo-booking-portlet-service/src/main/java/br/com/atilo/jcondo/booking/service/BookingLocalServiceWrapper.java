package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookingLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see BookingLocalService
 * @generated
 */
public class BookingLocalServiceWrapper implements BookingLocalService,
    ServiceWrapper<BookingLocalService> {
    private BookingLocalService _bookingLocalService;

    public BookingLocalServiceWrapper(BookingLocalService bookingLocalService) {
        _bookingLocalService = bookingLocalService;
    }

    /**
    * Adds the booking to the database. Also notifies the appropriate model listeners.
    *
    * @param booking the booking
    * @return the booking that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Booking addBooking(
        br.com.atilo.jcondo.booking.model.Booking booking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.addBooking(booking);
    }

    /**
    * Creates a new booking with the primary key. Does not add the booking to the database.
    *
    * @param id the primary key for the new booking
    * @return the new booking
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Booking createBooking(long id) {
        return _bookingLocalService.createBooking(id);
    }

    /**
    * Deletes the booking with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the booking
    * @return the booking that was removed
    * @throws PortalException if a booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Booking deleteBooking(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.deleteBooking(id);
    }

    /**
    * Deletes the booking from the database. Also notifies the appropriate model listeners.
    *
    * @param booking the booking
    * @return the booking that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Booking deleteBooking(
        br.com.atilo.jcondo.booking.model.Booking booking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.deleteBooking(booking);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _bookingLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking fetchBooking(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.fetchBooking(id);
    }

    /**
    * Returns the booking with the primary key.
    *
    * @param id the primary key of the booking
    * @return the booking
    * @throws PortalException if a booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Booking getBooking(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getBooking(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the bookings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of bookings
    * @param end the upper bound of the range of bookings (not inclusive)
    * @return the range of bookings
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getBookings(start, end);
    }

    /**
    * Returns the number of bookings.
    *
    * @return the number of bookings
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getBookingsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getBookingsCount();
    }

    /**
    * Updates the booking in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param booking the booking
    * @return the booking that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.Booking updateBooking(
        br.com.atilo.jcondo.booking.model.Booking booking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.updateBooking(booking);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _bookingLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _bookingLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _bookingLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking createBooking(
        long roomId, java.util.Date date)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.createBooking(roomId, date);
    }

    @Override
    public java.util.Date getDeadlineDate(java.util.Date beginDate) {
        return _bookingLocalService.getDeadlineDate(beginDate);
    }

    @Override
    public java.util.Date getMaxBookingDate() {
        return _bookingLocalService.getMaxBookingDate();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getDomainBookings(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getDomainBookings(domainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getPersonBookings(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getPersonBookings(personId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getPersonBookings(
        long personId, java.util.Date fromDate, java.util.Date toDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getPersonBookings(personId, fromDate, toDate);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        long roomId, java.util.Date date)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getBookings(roomId, date);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        long roomId, java.util.Date fromDate, java.util.Date toDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getBookings(roomId, fromDate, toDate);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        java.util.Date date)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getBookings(date);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        java.util.Date fromDate, java.util.Date toDate)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getBookings(fromDate, toDate);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking addBooking(long roomId,
        long domainId, long personId, java.util.Date beginDate,
        java.util.Date endDate,
        java.util.List<br.com.atilo.jcondo.booking.model.Guest> guests)
        throws java.lang.Exception {
        return _bookingLocalService.addBooking(roomId, domainId, personId,
            beginDate, endDate, guests);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking suspendBooking(
        br.com.atilo.jcondo.booking.model.Booking booking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.suspendBooking(booking);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking cancelBooking(
        br.com.atilo.jcondo.booking.model.Booking booking)
        throws java.lang.Exception {
        return _bookingLocalService.cancelBooking(booking);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking deleteBooking(
        br.com.atilo.jcondo.booking.model.Booking booking, java.lang.String note)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.deleteBooking(booking, note);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getOverdueBookings() {
        return _bookingLocalService.getOverdueBookings();
    }

    @Override
    public boolean isPaid(long bookingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.isPaid(bookingId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getDomainBookings(
        long domainId, java.util.Date date)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingLocalService.getDomainBookings(domainId, date);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public BookingLocalService getWrappedBookingLocalService() {
        return _bookingLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedBookingLocalService(
        BookingLocalService bookingLocalService) {
        _bookingLocalService = bookingLocalService;
    }

    @Override
    public BookingLocalService getWrappedService() {
        return _bookingLocalService;
    }

    @Override
    public void setWrappedService(BookingLocalService bookingLocalService) {
        _bookingLocalService = bookingLocalService;
    }
}
