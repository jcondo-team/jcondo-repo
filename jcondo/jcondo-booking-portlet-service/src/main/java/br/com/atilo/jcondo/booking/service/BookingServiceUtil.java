package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Booking. This utility wraps
 * {@link br.com.atilo.jcondo.booking.service.impl.BookingServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see BookingService
 * @see br.com.atilo.jcondo.booking.service.base.BookingServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.impl.BookingServiceImpl
 * @generated
 */
public class BookingServiceUtil {
    private static BookingService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.booking.service.impl.BookingServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> getDomainBookings(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainBookings(domainId);
    }

    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> getPersonBookings(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonBookings(personId);
    }

    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        long roomId, java.util.Date beginDate)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getBookings(roomId, beginDate);
    }

    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        long roomId, java.util.Date fromDate, java.util.Date toDate)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getBookings(roomId, fromDate, toDate);
    }

    public static br.com.atilo.jcondo.booking.model.Booking addBooking(
        long roomId, long domainId, long personId, java.util.Date beginDate,
        java.util.Date endDate,
        java.util.List<br.com.atilo.jcondo.booking.model.Guest> guests)
        throws java.lang.Exception {
        return getService()
                   .addBooking(roomId, domainId, personId, beginDate, endDate,
            guests);
    }

    public static br.com.atilo.jcondo.booking.model.Booking cancelBooking(
        long bookingId) throws java.lang.Exception {
        return getService().cancelBooking(bookingId);
    }

    public static br.com.atilo.jcondo.booking.model.Booking deleteBooking(
        long bookingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteBooking(bookingId);
    }

    public static br.com.atilo.jcondo.booking.model.Booking deleteBooking(
        long bookingId, java.lang.String note)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteBooking(bookingId, note);
    }

    public static void clearService() {
        _service = null;
    }

    public static BookingService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    BookingService.class.getName());

            if (invokableService instanceof BookingService) {
                _service = (BookingService) invokableService;
            } else {
                _service = new BookingServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(BookingServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(BookingService service) {
    }
}
