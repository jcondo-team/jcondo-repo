package br.com.atilo.jcondo.booking.model;

import br.com.atilo.jcondo.booking.service.ClpSerializer;
import br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class GuestClp extends BaseModelImpl<Guest> implements Guest {
    private long _guestId;
    private long _bookingId;
    private String _name;
    private String _surname;
    private boolean _checkedIn;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _guestRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.booking.service.ClpSerializer.class;

    public GuestClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Guest.class;
    }

    @Override
    public String getModelClassName() {
        return Guest.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _guestId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setGuestId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _guestId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("guestId", getGuestId());
        attributes.put("bookingId", getBookingId());
        attributes.put("name", getName());
        attributes.put("surname", getSurname());
        attributes.put("checkedIn", getCheckedIn());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long guestId = (Long) attributes.get("guestId");

        if (guestId != null) {
            setGuestId(guestId);
        }

        Long bookingId = (Long) attributes.get("bookingId");

        if (bookingId != null) {
            setBookingId(bookingId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        String surname = (String) attributes.get("surname");

        if (surname != null) {
            setSurname(surname);
        }

        Boolean checkedIn = (Boolean) attributes.get("checkedIn");

        if (checkedIn != null) {
            setCheckedIn(checkedIn);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getGuestId() {
        return _guestId;
    }

    @Override
    public void setGuestId(long guestId) {
        _guestId = guestId;

        if (_guestRemoteModel != null) {
            try {
                Class<?> clazz = _guestRemoteModel.getClass();

                Method method = clazz.getMethod("setGuestId", long.class);

                method.invoke(_guestRemoteModel, guestId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getBookingId() {
        return _bookingId;
    }

    @Override
    public void setBookingId(long bookingId) {
        _bookingId = bookingId;

        if (_guestRemoteModel != null) {
            try {
                Class<?> clazz = _guestRemoteModel.getClass();

                Method method = clazz.getMethod("setBookingId", long.class);

                method.invoke(_guestRemoteModel, bookingId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setName(String name) {
        _name = name;

        if (_guestRemoteModel != null) {
            try {
                Class<?> clazz = _guestRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_guestRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getSurname() {
        return _surname;
    }

    @Override
    public void setSurname(String surname) {
        _surname = surname;

        if (_guestRemoteModel != null) {
            try {
                Class<?> clazz = _guestRemoteModel.getClass();

                Method method = clazz.getMethod("setSurname", String.class);

                method.invoke(_guestRemoteModel, surname);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getCheckedIn() {
        return _checkedIn;
    }

    @Override
    public boolean isCheckedIn() {
        return _checkedIn;
    }

    @Override
    public void setCheckedIn(boolean checkedIn) {
        _checkedIn = checkedIn;

        if (_guestRemoteModel != null) {
            try {
                Class<?> clazz = _guestRemoteModel.getClass();

                Method method = clazz.getMethod("setCheckedIn", boolean.class);

                method.invoke(_guestRemoteModel, checkedIn);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_guestRemoteModel != null) {
            try {
                Class<?> clazz = _guestRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_guestRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_guestRemoteModel != null) {
            try {
                Class<?> clazz = _guestRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_guestRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getGuestRemoteModel() {
        return _guestRemoteModel;
    }

    public void setGuestRemoteModel(BaseModel<?> guestRemoteModel) {
        _guestRemoteModel = guestRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _guestRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_guestRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            GuestLocalServiceUtil.addGuest(this);
        } else {
            GuestLocalServiceUtil.updateGuest(this);
        }
    }

    @Override
    public Guest toEscapedModel() {
        return (Guest) ProxyUtil.newProxyInstance(Guest.class.getClassLoader(),
            new Class[] { Guest.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        GuestClp clone = new GuestClp();

        clone.setGuestId(getGuestId());
        clone.setBookingId(getBookingId());
        clone.setName(getName());
        clone.setSurname(getSurname());
        clone.setCheckedIn(getCheckedIn());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Guest guest) {
        int value = 0;

        value = getName().compareTo(guest.getName());

        if (value != 0) {
            return value;
        }

        value = getSurname().compareTo(guest.getSurname());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof GuestClp)) {
            return false;
        }

        GuestClp guest = (GuestClp) obj;

        long primaryKey = guest.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{guestId=");
        sb.append(getGuestId());
        sb.append(", bookingId=");
        sb.append(getBookingId());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", surname=");
        sb.append(getSurname());
        sb.append(", checkedIn=");
        sb.append(getCheckedIn());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(25);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.booking.model.Guest");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>guestId</column-name><column-value><![CDATA[");
        sb.append(getGuestId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>bookingId</column-name><column-value><![CDATA[");
        sb.append(getBookingId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>surname</column-name><column-value><![CDATA[");
        sb.append(getSurname());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>checkedIn</column-name><column-value><![CDATA[");
        sb.append(getCheckedIn());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
