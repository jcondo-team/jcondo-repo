package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.BookingNote;
import br.com.atilo.jcondo.booking.service.BookingNoteLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class BookingNoteActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public BookingNoteActionableDynamicQuery() throws SystemException {
        setBaseLocalService(BookingNoteLocalServiceUtil.getService());
        setClass(BookingNote.class);

        setClassLoader(br.com.atilo.jcondo.booking.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
