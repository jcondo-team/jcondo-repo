package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link BookingNoteService}.
 *
 * @author Brian Wing Shun Chan
 * @see BookingNoteService
 * @generated
 */
public class BookingNoteServiceWrapper implements BookingNoteService,
    ServiceWrapper<BookingNoteService> {
    private BookingNoteService _bookingNoteService;

    public BookingNoteServiceWrapper(BookingNoteService bookingNoteService) {
        _bookingNoteService = bookingNoteService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _bookingNoteService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _bookingNoteService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _bookingNoteService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote addBookingNote(
        br.com.atilo.jcondo.booking.model.BookingNote bookingNote)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _bookingNoteService.addBookingNote(bookingNote);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public BookingNoteService getWrappedBookingNoteService() {
        return _bookingNoteService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedBookingNoteService(
        BookingNoteService bookingNoteService) {
        _bookingNoteService = bookingNoteService;
    }

    @Override
    public BookingNoteService getWrappedService() {
        return _bookingNoteService;
    }

    @Override
    public void setWrappedService(BookingNoteService bookingNoteService) {
        _bookingNoteService = bookingNoteService;
    }
}
