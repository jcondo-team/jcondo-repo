package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.RoomBooking;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the room booking service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomBookingPersistenceImpl
 * @see RoomBookingUtil
 * @generated
 */
public interface RoomBookingPersistence extends BasePersistence<RoomBooking> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link RoomBookingUtil} to access the room booking persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the room bookings where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @return the matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoomAndWeekDay(
        long roomId, int weekDay)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the room bookings where roomId = &#63; and weekDay = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @return the range of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoomAndWeekDay(
        long roomId, int weekDay, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the room bookings where roomId = &#63; and weekDay = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoomAndWeekDay(
        long roomId, int weekDay, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking findByRoomAndWeekDay_First(
        long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room booking, or <code>null</code> if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking fetchByRoomAndWeekDay_First(
        long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking findByRoomAndWeekDay_Last(
        long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room booking, or <code>null</code> if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking fetchByRoomAndWeekDay_Last(
        long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the room bookings before and after the current room booking in the ordered set where roomId = &#63; and weekDay = &#63;.
    *
    * @param id the primary key of the current room booking
    * @param roomId the room ID
    * @param weekDay the week day
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking[] findByRoomAndWeekDay_PrevAndNext(
        long id, long roomId, int weekDay,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the room bookings where roomId = &#63; and weekDay = &#63; from the database.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @throws SystemException if a system exception occurred
    */
    public void removeByRoomAndWeekDay(long roomId, int weekDay)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of room bookings where roomId = &#63; and weekDay = &#63;.
    *
    * @param roomId the room ID
    * @param weekDay the week day
    * @return the number of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public int countByRoomAndWeekDay(long roomId, int weekDay)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the room bookings where roomId = &#63;.
    *
    * @param roomId the room ID
    * @return the matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoom(
        long roomId) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the room bookings where roomId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @return the range of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoom(
        long roomId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the room bookings where roomId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param roomId the room ID
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findByRoom(
        long roomId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first room booking in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking findByRoom_First(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first room booking in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching room booking, or <code>null</code> if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking fetchByRoom_First(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last room booking in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking findByRoom_Last(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last room booking in the ordered set where roomId = &#63;.
    *
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching room booking, or <code>null</code> if a matching room booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking fetchByRoom_Last(
        long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the room bookings before and after the current room booking in the ordered set where roomId = &#63;.
    *
    * @param id the primary key of the current room booking
    * @param roomId the room ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking[] findByRoom_PrevAndNext(
        long id, long roomId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the room bookings where roomId = &#63; from the database.
    *
    * @param roomId the room ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByRoom(long roomId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of room bookings where roomId = &#63;.
    *
    * @param roomId the room ID
    * @return the number of matching room bookings
    * @throws SystemException if a system exception occurred
    */
    public int countByRoom(long roomId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the room booking in the entity cache if it is enabled.
    *
    * @param roomBooking the room booking
    */
    public void cacheResult(
        br.com.atilo.jcondo.booking.model.RoomBooking roomBooking);

    /**
    * Caches the room bookings in the entity cache if it is enabled.
    *
    * @param roomBookings the room bookings
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> roomBookings);

    /**
    * Creates a new room booking with the primary key. Does not add the room booking to the database.
    *
    * @param id the primary key for the new room booking
    * @return the new room booking
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking create(long id);

    /**
    * Removes the room booking with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the room booking
    * @return the room booking that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking remove(long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.booking.model.RoomBooking updateImpl(
        br.com.atilo.jcondo.booking.model.RoomBooking roomBooking)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the room booking with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchRoomBookingException} if it could not be found.
    *
    * @param id the primary key of the room booking
    * @return the room booking
    * @throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchRoomBookingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the room booking with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the room booking
    * @return the room booking, or <code>null</code> if a room booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.booking.model.RoomBooking fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the room bookings.
    *
    * @return the room bookings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the room bookings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @return the range of room bookings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the room bookings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of room bookings
    * @param end the upper bound of the range of room bookings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of room bookings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the room bookings from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of room bookings.
    *
    * @return the number of room bookings
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
