package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the RoomBlockade service. Represents a row in the &quot;jco_room_blockade&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see RoomBlockadeModel
 * @see br.com.atilo.jcondo.booking.model.impl.RoomBlockadeImpl
 * @see br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl
 * @generated
 */
public interface RoomBlockade extends RoomBlockadeModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.booking.model.Room getRoom()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setRoom(br.com.atilo.jcondo.booking.model.Room room)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;
}
