package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RoomBookingService}.
 *
 * @author Brian Wing Shun Chan
 * @see RoomBookingService
 * @generated
 */
public class RoomBookingServiceWrapper implements RoomBookingService,
    ServiceWrapper<RoomBookingService> {
    private RoomBookingService _roomBookingService;

    public RoomBookingServiceWrapper(RoomBookingService roomBookingService) {
        _roomBookingService = roomBookingService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _roomBookingService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _roomBookingService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _roomBookingService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBooking addRoomBooking(
        long roomId, int weekDay, int openHour, int closeHour, int period)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBookingService.addRoomBooking(roomId, weekDay, openHour,
            closeHour, period);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBooking updateRoomBooking(
        long roomBookingId, int openHour, int closeHour, int period)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBookingService.updateRoomBooking(roomBookingId, openHour,
            closeHour, period);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBooking updateRoomBooking(
        br.com.atilo.jcondo.booking.model.RoomBooking roomBooking)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBookingService.updateRoomBooking(roomBooking);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> getRoomBookings(
        long roomId, int weekDay)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBookingService.getRoomBookings(roomId, weekDay);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> getRoomBookings(
        long roomId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBookingService.getRoomBookings(roomId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public RoomBookingService getWrappedRoomBookingService() {
        return _roomBookingService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedRoomBookingService(
        RoomBookingService roomBookingService) {
        _roomBookingService = roomBookingService;
    }

    @Override
    public RoomBookingService getWrappedService() {
        return _roomBookingService;
    }

    @Override
    public void setWrappedService(RoomBookingService roomBookingService) {
        _roomBookingService = roomBookingService;
    }
}
