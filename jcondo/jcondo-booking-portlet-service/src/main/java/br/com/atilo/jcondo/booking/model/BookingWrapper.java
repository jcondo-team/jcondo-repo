package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Booking}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Booking
 * @generated
 */
public class BookingWrapper implements Booking, ModelWrapper<Booking> {
    private Booking _booking;

    public BookingWrapper(Booking booking) {
        _booking = booking;
    }

    @Override
    public Class<?> getModelClass() {
        return Booking.class;
    }

    @Override
    public String getModelClassName() {
        return Booking.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("roomId", getRoomId());
        attributes.put("domainId", getDomainId());
        attributes.put("personId", getPersonId());
        attributes.put("beginDate", getBeginDate());
        attributes.put("endDate", getEndDate());
        attributes.put("statusId", getStatusId());
        attributes.put("noteId", getNoteId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long roomId = (Long) attributes.get("roomId");

        if (roomId != null) {
            setRoomId(roomId);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Date beginDate = (Date) attributes.get("beginDate");

        if (beginDate != null) {
            setBeginDate(beginDate);
        }

        Date endDate = (Date) attributes.get("endDate");

        if (endDate != null) {
            setEndDate(endDate);
        }

        Integer statusId = (Integer) attributes.get("statusId");

        if (statusId != null) {
            setStatusId(statusId);
        }

        Long noteId = (Long) attributes.get("noteId");

        if (noteId != null) {
            setNoteId(noteId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this booking.
    *
    * @return the primary key of this booking
    */
    @Override
    public long getPrimaryKey() {
        return _booking.getPrimaryKey();
    }

    /**
    * Sets the primary key of this booking.
    *
    * @param primaryKey the primary key of this booking
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _booking.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this booking.
    *
    * @return the ID of this booking
    */
    @Override
    public long getId() {
        return _booking.getId();
    }

    /**
    * Sets the ID of this booking.
    *
    * @param id the ID of this booking
    */
    @Override
    public void setId(long id) {
        _booking.setId(id);
    }

    /**
    * Returns the room ID of this booking.
    *
    * @return the room ID of this booking
    */
    @Override
    public long getRoomId() {
        return _booking.getRoomId();
    }

    /**
    * Sets the room ID of this booking.
    *
    * @param roomId the room ID of this booking
    */
    @Override
    public void setRoomId(long roomId) {
        _booking.setRoomId(roomId);
    }

    /**
    * Returns the domain ID of this booking.
    *
    * @return the domain ID of this booking
    */
    @Override
    public long getDomainId() {
        return _booking.getDomainId();
    }

    /**
    * Sets the domain ID of this booking.
    *
    * @param domainId the domain ID of this booking
    */
    @Override
    public void setDomainId(long domainId) {
        _booking.setDomainId(domainId);
    }

    /**
    * Returns the person ID of this booking.
    *
    * @return the person ID of this booking
    */
    @Override
    public long getPersonId() {
        return _booking.getPersonId();
    }

    /**
    * Sets the person ID of this booking.
    *
    * @param personId the person ID of this booking
    */
    @Override
    public void setPersonId(long personId) {
        _booking.setPersonId(personId);
    }

    /**
    * Returns the begin date of this booking.
    *
    * @return the begin date of this booking
    */
    @Override
    public java.util.Date getBeginDate() {
        return _booking.getBeginDate();
    }

    /**
    * Sets the begin date of this booking.
    *
    * @param beginDate the begin date of this booking
    */
    @Override
    public void setBeginDate(java.util.Date beginDate) {
        _booking.setBeginDate(beginDate);
    }

    /**
    * Returns the end date of this booking.
    *
    * @return the end date of this booking
    */
    @Override
    public java.util.Date getEndDate() {
        return _booking.getEndDate();
    }

    /**
    * Sets the end date of this booking.
    *
    * @param endDate the end date of this booking
    */
    @Override
    public void setEndDate(java.util.Date endDate) {
        _booking.setEndDate(endDate);
    }

    /**
    * Returns the status ID of this booking.
    *
    * @return the status ID of this booking
    */
    @Override
    public int getStatusId() {
        return _booking.getStatusId();
    }

    /**
    * Sets the status ID of this booking.
    *
    * @param statusId the status ID of this booking
    */
    @Override
    public void setStatusId(int statusId) {
        _booking.setStatusId(statusId);
    }

    /**
    * Returns the note ID of this booking.
    *
    * @return the note ID of this booking
    */
    @Override
    public long getNoteId() {
        return _booking.getNoteId();
    }

    /**
    * Sets the note ID of this booking.
    *
    * @param noteId the note ID of this booking
    */
    @Override
    public void setNoteId(long noteId) {
        _booking.setNoteId(noteId);
    }

    /**
    * Returns the opr date of this booking.
    *
    * @return the opr date of this booking
    */
    @Override
    public java.util.Date getOprDate() {
        return _booking.getOprDate();
    }

    /**
    * Sets the opr date of this booking.
    *
    * @param oprDate the opr date of this booking
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _booking.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this booking.
    *
    * @return the opr user of this booking
    */
    @Override
    public long getOprUser() {
        return _booking.getOprUser();
    }

    /**
    * Sets the opr user of this booking.
    *
    * @param oprUser the opr user of this booking
    */
    @Override
    public void setOprUser(long oprUser) {
        _booking.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _booking.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _booking.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _booking.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _booking.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _booking.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _booking.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _booking.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _booking.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _booking.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _booking.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _booking.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new BookingWrapper((Booking) _booking.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.booking.model.Booking booking) {
        return _booking.compareTo(booking);
    }

    @Override
    public int hashCode() {
        return _booking.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.booking.model.Booking> toCacheModel() {
        return _booking.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking toEscapedModel() {
        return new BookingWrapper(_booking.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Booking toUnescapedModel() {
        return new BookingWrapper(_booking.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _booking.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _booking.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _booking.persist();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.datatype.BookingStatus getStatus() {
        return _booking.getStatus();
    }

    @Override
    public void setStatus(
        br.com.atilo.jcondo.booking.model.datatype.BookingStatus status) {
        _booking.setStatus(status);
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _booking.getDomain();
    }

    @Override
    public void setDomain(com.liferay.portal.model.BaseModel<?> domain) {
        _booking.setDomain(domain);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room getRoom()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _booking.getRoom();
    }

    @Override
    public void setRoom(br.com.atilo.jcondo.booking.model.Room room) {
        _booking.setRoom(room);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _booking.getPerson();
    }

    @Override
    public void setPerson(br.com.atilo.jcondo.manager.model.Person person) {
        _booking.setPerson(person);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Guest> getGuests()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _booking.getGuests();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingNote getNote()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _booking.getNote();
    }

    @Override
    public void setNote(br.com.atilo.jcondo.booking.model.BookingNote note) {
        _booking.setNote(note);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.BookingInvoice getInvoice()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _booking.getInvoice();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof BookingWrapper)) {
            return false;
        }

        BookingWrapper bookingWrapper = (BookingWrapper) obj;

        if (Validator.equals(_booking, bookingWrapper._booking)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Booking getWrappedBooking() {
        return _booking;
    }

    @Override
    public Booking getWrappedModel() {
        return _booking;
    }

    @Override
    public void resetOriginalValues() {
        _booking.resetOriginalValues();
    }
}
