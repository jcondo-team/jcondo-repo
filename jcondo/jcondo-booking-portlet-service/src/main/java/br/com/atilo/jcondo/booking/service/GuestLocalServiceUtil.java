package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Guest. This utility wraps
 * {@link br.com.atilo.jcondo.booking.service.impl.GuestLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see GuestLocalService
 * @see br.com.atilo.jcondo.booking.service.base.GuestLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.impl.GuestLocalServiceImpl
 * @generated
 */
public class GuestLocalServiceUtil {
    private static GuestLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.booking.service.impl.GuestLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the guest to the database. Also notifies the appropriate model listeners.
    *
    * @param guest the guest
    * @return the guest that was added
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest addGuest(
        br.com.atilo.jcondo.booking.model.Guest guest)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addGuest(guest);
    }

    /**
    * Creates a new guest with the primary key. Does not add the guest to the database.
    *
    * @param guestId the primary key for the new guest
    * @return the new guest
    */
    public static br.com.atilo.jcondo.booking.model.Guest createGuest(
        long guestId) {
        return getService().createGuest(guestId);
    }

    /**
    * Deletes the guest with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param guestId the primary key of the guest
    * @return the guest that was removed
    * @throws PortalException if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest deleteGuest(
        long guestId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteGuest(guestId);
    }

    /**
    * Deletes the guest from the database. Also notifies the appropriate model listeners.
    *
    * @param guest the guest
    * @return the guest that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest deleteGuest(
        br.com.atilo.jcondo.booking.model.Guest guest)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteGuest(guest);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static br.com.atilo.jcondo.booking.model.Guest fetchGuest(
        long guestId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchGuest(guestId);
    }

    /**
    * Returns the guest with the primary key.
    *
    * @param guestId the primary key of the guest
    * @return the guest
    * @throws PortalException if a guest with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest getGuest(long guestId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getGuest(guestId);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the guests.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of guests
    * @param end the upper bound of the range of guests (not inclusive)
    * @return the range of guests
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Guest> getGuests(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getGuests(start, end);
    }

    /**
    * Returns the number of guests.
    *
    * @return the number of guests
    * @throws SystemException if a system exception occurred
    */
    public static int getGuestsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getGuestsCount();
    }

    /**
    * Updates the guest in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param guest the guest
    * @return the guest that was updated
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Guest updateGuest(
        br.com.atilo.jcondo.booking.model.Guest guest)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateGuest(guest);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.booking.model.Guest addGuest(
        long bookingId, java.lang.String name, java.lang.String surname)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addGuest(bookingId, name, surname);
    }

    public static br.com.atilo.jcondo.booking.model.Guest updateGuest(
        long guestId, long bookingId, java.lang.String name,
        java.lang.String surname, boolean checkedIn)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateGuest(guestId, bookingId, name, surname, checkedIn);
    }

    public static java.util.List<br.com.atilo.jcondo.booking.model.Guest> getBookingGuests(
        long bookingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getBookingGuests(bookingId);
    }

    public static void clearService() {
        _service = null;
    }

    public static GuestLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    GuestLocalService.class.getName());

            if (invokableLocalService instanceof GuestLocalService) {
                _service = (GuestLocalService) invokableLocalService;
            } else {
                _service = new GuestLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(GuestLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(GuestLocalService service) {
    }
}
