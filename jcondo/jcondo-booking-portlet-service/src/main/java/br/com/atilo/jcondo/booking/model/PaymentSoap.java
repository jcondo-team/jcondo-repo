package br.com.atilo.jcondo.booking.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.booking.service.http.PaymentServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.booking.service.http.PaymentServiceSoap
 * @generated
 */
public class PaymentSoap implements Serializable {
    private long _id;
    private long _invoiceId;
    private String _token;
    private double _amount;
    private Date _date;
    private boolean _checked;
    private Date _oprDate;
    private long _oprUser;

    public PaymentSoap() {
    }

    public static PaymentSoap toSoapModel(Payment model) {
        PaymentSoap soapModel = new PaymentSoap();

        soapModel.setId(model.getId());
        soapModel.setInvoiceId(model.getInvoiceId());
        soapModel.setToken(model.getToken());
        soapModel.setAmount(model.getAmount());
        soapModel.setDate(model.getDate());
        soapModel.setChecked(model.getChecked());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static PaymentSoap[] toSoapModels(Payment[] models) {
        PaymentSoap[] soapModels = new PaymentSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PaymentSoap[][] toSoapModels(Payment[][] models) {
        PaymentSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PaymentSoap[models.length][models[0].length];
        } else {
            soapModels = new PaymentSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PaymentSoap[] toSoapModels(List<Payment> models) {
        List<PaymentSoap> soapModels = new ArrayList<PaymentSoap>(models.size());

        for (Payment model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PaymentSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getInvoiceId() {
        return _invoiceId;
    }

    public void setInvoiceId(long invoiceId) {
        _invoiceId = invoiceId;
    }

    public String getToken() {
        return _token;
    }

    public void setToken(String token) {
        _token = token;
    }

    public double getAmount() {
        return _amount;
    }

    public void setAmount(double amount) {
        _amount = amount;
    }

    public Date getDate() {
        return _date;
    }

    public void setDate(Date date) {
        _date = date;
    }

    public boolean getChecked() {
        return _checked;
    }

    public boolean isChecked() {
        return _checked;
    }

    public void setChecked(boolean checked) {
        _checked = checked;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
