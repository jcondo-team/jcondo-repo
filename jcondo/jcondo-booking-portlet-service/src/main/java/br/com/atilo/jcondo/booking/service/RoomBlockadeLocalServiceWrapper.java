package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RoomBlockadeLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see RoomBlockadeLocalService
 * @generated
 */
public class RoomBlockadeLocalServiceWrapper implements RoomBlockadeLocalService,
    ServiceWrapper<RoomBlockadeLocalService> {
    private RoomBlockadeLocalService _roomBlockadeLocalService;

    public RoomBlockadeLocalServiceWrapper(
        RoomBlockadeLocalService roomBlockadeLocalService) {
        _roomBlockadeLocalService = roomBlockadeLocalService;
    }

    /**
    * Adds the room blockade to the database. Also notifies the appropriate model listeners.
    *
    * @param roomBlockade the room blockade
    * @return the room blockade that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade addRoomBlockade(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.addRoomBlockade(roomBlockade);
    }

    /**
    * Creates a new room blockade with the primary key. Does not add the room blockade to the database.
    *
    * @param id the primary key for the new room blockade
    * @return the new room blockade
    */
    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade createRoomBlockade(
        long id) {
        return _roomBlockadeLocalService.createRoomBlockade(id);
    }

    /**
    * Deletes the room blockade with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the room blockade
    * @return the room blockade that was removed
    * @throws PortalException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade deleteRoomBlockade(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.deleteRoomBlockade(id);
    }

    /**
    * Deletes the room blockade from the database. Also notifies the appropriate model listeners.
    *
    * @param roomBlockade the room blockade
    * @return the room blockade that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade deleteRoomBlockade(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.deleteRoomBlockade(roomBlockade);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _roomBlockadeLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade fetchRoomBlockade(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.fetchRoomBlockade(id);
    }

    /**
    * Returns the room blockade with the primary key.
    *
    * @param id the primary key of the room blockade
    * @return the room blockade
    * @throws PortalException if a room blockade with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade getRoomBlockade(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.getRoomBlockade(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the room blockades.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of room blockades
    * @param end the upper bound of the range of room blockades (not inclusive)
    * @return the range of room blockades
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> getRoomBlockades(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.getRoomBlockades(start, end);
    }

    /**
    * Returns the number of room blockades.
    *
    * @return the number of room blockades
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getRoomBlockadesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.getRoomBlockadesCount();
    }

    /**
    * Updates the room blockade in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param roomBlockade the room blockade
    * @return the room blockade that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade updateRoomBlockade(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.updateRoomBlockade(roomBlockade);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _roomBlockadeLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _roomBlockadeLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _roomBlockadeLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.RoomBlockade> getRoomBlockades(
        long roomId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.getRoomBlockades(roomId);
    }

    @Override
    public java.util.List<java.util.Date> getBlockedDates(long roomId,
        java.util.Date fromDate, java.util.Date toDate)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockadeLocalService.getBlockedDates(roomId, fromDate,
            toDate);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public RoomBlockadeLocalService getWrappedRoomBlockadeLocalService() {
        return _roomBlockadeLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedRoomBlockadeLocalService(
        RoomBlockadeLocalService roomBlockadeLocalService) {
        _roomBlockadeLocalService = roomBlockadeLocalService;
    }

    @Override
    public RoomBlockadeLocalService getWrappedService() {
        return _roomBlockadeLocalService;
    }

    @Override
    public void setWrappedService(
        RoomBlockadeLocalService roomBlockadeLocalService) {
        _roomBlockadeLocalService = roomBlockadeLocalService;
    }
}
