package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link RoomService}.
 *
 * @author Brian Wing Shun Chan
 * @see RoomService
 * @generated
 */
public class RoomServiceWrapper implements RoomService,
    ServiceWrapper<RoomService> {
    private RoomService _roomService;

    public RoomServiceWrapper(RoomService roomService) {
        _roomService = roomService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _roomService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _roomService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _roomService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room addRoom(
        java.lang.String name, java.lang.String description, double price,
        int capacity, boolean available, boolean bookable,
        java.io.File agreement)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomService.addRoom(name, description, price, capacity,
            available, bookable, agreement);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room addRoom(
        br.com.atilo.jcondo.booking.model.Room room)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomService.addRoom(room);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room updateRoom(long roomId,
        java.lang.String name, java.lang.String description, double price,
        int capacity, boolean available, boolean bookable,
        java.io.File agreement)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomService.updateRoom(roomId, name, description, price,
            capacity, available, bookable, agreement);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room updateRoom(
        br.com.atilo.jcondo.booking.model.Room room)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomService.updateRoom(room);
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room deleteRoom(
        br.com.atilo.jcondo.booking.model.Room room)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomService.deleteRoom(room);
    }

    @Override
    public br.com.atilo.jcondo.Image addRoomPicture(long roomId,
        br.com.atilo.jcondo.Image image)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomService.addRoomPicture(roomId, image);
    }

    @Override
    public br.com.atilo.jcondo.Image deleteRoomPicture(long roomId, long imageId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomService.deleteRoomPicture(roomId, imageId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.booking.model.Room> getRooms(
        boolean onlyAvailables, boolean onlyBookables)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomService.getRooms(onlyAvailables, onlyBookables);
    }

    @Override
    public void checkRoomAvailability(long roomId, java.util.Date date)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _roomService.checkRoomAvailability(roomId, date);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public RoomService getWrappedRoomService() {
        return _roomService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedRoomService(RoomService roomService) {
        _roomService = roomService;
    }

    @Override
    public RoomService getWrappedService() {
        return _roomService;
    }

    @Override
    public void setWrappedService(RoomService roomService) {
        _roomService = roomService;
    }
}
