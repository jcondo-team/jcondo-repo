package br.com.atilo.jcondo.booking.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link RoomBlockade}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomBlockade
 * @generated
 */
public class RoomBlockadeWrapper implements RoomBlockade,
    ModelWrapper<RoomBlockade> {
    private RoomBlockade _roomBlockade;

    public RoomBlockadeWrapper(RoomBlockade roomBlockade) {
        _roomBlockade = roomBlockade;
    }

    @Override
    public Class<?> getModelClass() {
        return RoomBlockade.class;
    }

    @Override
    public String getModelClassName() {
        return RoomBlockade.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("roomId", getRoomId());
        attributes.put("beginDate", getBeginDate());
        attributes.put("endDate", getEndDate());
        attributes.put("description", getDescription());
        attributes.put("recursive", getRecursive());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long roomId = (Long) attributes.get("roomId");

        if (roomId != null) {
            setRoomId(roomId);
        }

        Date beginDate = (Date) attributes.get("beginDate");

        if (beginDate != null) {
            setBeginDate(beginDate);
        }

        Date endDate = (Date) attributes.get("endDate");

        if (endDate != null) {
            setEndDate(endDate);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Boolean recursive = (Boolean) attributes.get("recursive");

        if (recursive != null) {
            setRecursive(recursive);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this room blockade.
    *
    * @return the primary key of this room blockade
    */
    @Override
    public long getPrimaryKey() {
        return _roomBlockade.getPrimaryKey();
    }

    /**
    * Sets the primary key of this room blockade.
    *
    * @param primaryKey the primary key of this room blockade
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _roomBlockade.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this room blockade.
    *
    * @return the ID of this room blockade
    */
    @Override
    public long getId() {
        return _roomBlockade.getId();
    }

    /**
    * Sets the ID of this room blockade.
    *
    * @param id the ID of this room blockade
    */
    @Override
    public void setId(long id) {
        _roomBlockade.setId(id);
    }

    /**
    * Returns the room ID of this room blockade.
    *
    * @return the room ID of this room blockade
    */
    @Override
    public long getRoomId() {
        return _roomBlockade.getRoomId();
    }

    /**
    * Sets the room ID of this room blockade.
    *
    * @param roomId the room ID of this room blockade
    */
    @Override
    public void setRoomId(long roomId) {
        _roomBlockade.setRoomId(roomId);
    }

    /**
    * Returns the begin date of this room blockade.
    *
    * @return the begin date of this room blockade
    */
    @Override
    public java.util.Date getBeginDate() {
        return _roomBlockade.getBeginDate();
    }

    /**
    * Sets the begin date of this room blockade.
    *
    * @param beginDate the begin date of this room blockade
    */
    @Override
    public void setBeginDate(java.util.Date beginDate) {
        _roomBlockade.setBeginDate(beginDate);
    }

    /**
    * Returns the end date of this room blockade.
    *
    * @return the end date of this room blockade
    */
    @Override
    public java.util.Date getEndDate() {
        return _roomBlockade.getEndDate();
    }

    /**
    * Sets the end date of this room blockade.
    *
    * @param endDate the end date of this room blockade
    */
    @Override
    public void setEndDate(java.util.Date endDate) {
        _roomBlockade.setEndDate(endDate);
    }

    /**
    * Returns the description of this room blockade.
    *
    * @return the description of this room blockade
    */
    @Override
    public java.lang.String getDescription() {
        return _roomBlockade.getDescription();
    }

    /**
    * Sets the description of this room blockade.
    *
    * @param description the description of this room blockade
    */
    @Override
    public void setDescription(java.lang.String description) {
        _roomBlockade.setDescription(description);
    }

    /**
    * Returns the recursive of this room blockade.
    *
    * @return the recursive of this room blockade
    */
    @Override
    public boolean getRecursive() {
        return _roomBlockade.getRecursive();
    }

    /**
    * Returns <code>true</code> if this room blockade is recursive.
    *
    * @return <code>true</code> if this room blockade is recursive; <code>false</code> otherwise
    */
    @Override
    public boolean isRecursive() {
        return _roomBlockade.isRecursive();
    }

    /**
    * Sets whether this room blockade is recursive.
    *
    * @param recursive the recursive of this room blockade
    */
    @Override
    public void setRecursive(boolean recursive) {
        _roomBlockade.setRecursive(recursive);
    }

    /**
    * Returns the opr date of this room blockade.
    *
    * @return the opr date of this room blockade
    */
    @Override
    public java.util.Date getOprDate() {
        return _roomBlockade.getOprDate();
    }

    /**
    * Sets the opr date of this room blockade.
    *
    * @param oprDate the opr date of this room blockade
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _roomBlockade.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this room blockade.
    *
    * @return the opr user of this room blockade
    */
    @Override
    public long getOprUser() {
        return _roomBlockade.getOprUser();
    }

    /**
    * Sets the opr user of this room blockade.
    *
    * @param oprUser the opr user of this room blockade
    */
    @Override
    public void setOprUser(long oprUser) {
        _roomBlockade.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _roomBlockade.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _roomBlockade.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _roomBlockade.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _roomBlockade.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _roomBlockade.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _roomBlockade.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _roomBlockade.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _roomBlockade.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _roomBlockade.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _roomBlockade.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _roomBlockade.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new RoomBlockadeWrapper((RoomBlockade) _roomBlockade.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade) {
        return _roomBlockade.compareTo(roomBlockade);
    }

    @Override
    public int hashCode() {
        return _roomBlockade.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.booking.model.RoomBlockade> toCacheModel() {
        return _roomBlockade.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade toEscapedModel() {
        return new RoomBlockadeWrapper(_roomBlockade.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.booking.model.RoomBlockade toUnescapedModel() {
        return new RoomBlockadeWrapper(_roomBlockade.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _roomBlockade.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _roomBlockade.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _roomBlockade.persist();
    }

    @Override
    public br.com.atilo.jcondo.booking.model.Room getRoom()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _roomBlockade.getRoom();
    }

    @Override
    public void setRoom(br.com.atilo.jcondo.booking.model.Room room)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _roomBlockade.setRoom(room);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof RoomBlockadeWrapper)) {
            return false;
        }

        RoomBlockadeWrapper roomBlockadeWrapper = (RoomBlockadeWrapper) obj;

        if (Validator.equals(_roomBlockade, roomBlockadeWrapper._roomBlockade)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public RoomBlockade getWrappedRoomBlockade() {
        return _roomBlockade;
    }

    @Override
    public RoomBlockade getWrappedModel() {
        return _roomBlockade;
    }

    @Override
    public void resetOriginalValues() {
        _roomBlockade.resetOriginalValues();
    }
}
