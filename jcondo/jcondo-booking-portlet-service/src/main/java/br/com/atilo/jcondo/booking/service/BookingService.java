package br.com.atilo.jcondo.booking.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.security.ac.AccessControlled;
import com.liferay.portal.service.BaseService;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service interface for Booking. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see BookingServiceUtil
 * @see br.com.atilo.jcondo.booking.service.base.BookingServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.impl.BookingServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface BookingService extends BaseService, InvokableService {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link BookingServiceUtil} to access the booking remote service. Add custom service methods to {@link br.com.atilo.jcondo.booking.service.impl.BookingServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier();

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier);

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getDomainBookings(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getPersonBookings(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        long roomId, java.util.Date beginDate)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.booking.model.Booking> getBookings(
        long roomId, java.util.Date fromDate, java.util.Date toDate)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.booking.model.Booking addBooking(long roomId,
        long domainId, long personId, java.util.Date beginDate,
        java.util.Date endDate,
        java.util.List<br.com.atilo.jcondo.booking.model.Guest> guests)
        throws java.lang.Exception;

    public br.com.atilo.jcondo.booking.model.Booking cancelBooking(
        long bookingId) throws java.lang.Exception;

    public br.com.atilo.jcondo.booking.model.Booking deleteBooking(
        long bookingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.booking.model.Booking deleteBooking(
        long bookingId, java.lang.String note)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;
}
