package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.model.Booking;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the booking service. This utility wraps {@link BookingPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingPersistence
 * @see BookingPersistenceImpl
 * @generated
 */
public class BookingUtil {
    private static BookingPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Booking booking) {
        getPersistence().clearCache(booking);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Booking> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Booking> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Booking> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Booking update(Booking booking) throws SystemException {
        return getPersistence().update(booking);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Booking update(Booking booking, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(booking, serviceContext);
    }

    /**
    * Returns all the bookings where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @return the matching bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findByDomain(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomain(domainId);
    }

    /**
    * Returns a range of all the bookings where domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param start the lower bound of the range of bookings
    * @param end the upper bound of the range of bookings (not inclusive)
    * @return the range of matching bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findByDomain(
        long domainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomain(domainId, start, end);
    }

    /**
    * Returns an ordered range of all the bookings where domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param start the lower bound of the range of bookings
    * @param end the upper bound of the range of bookings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findByDomain(
        long domainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDomain(domainId, start, end, orderByComparator);
    }

    /**
    * Returns the first booking in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching booking
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a matching booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking findByDomain_First(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomain_First(domainId, orderByComparator);
    }

    /**
    * Returns the first booking in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching booking, or <code>null</code> if a matching booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking fetchByDomain_First(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByDomain_First(domainId, orderByComparator);
    }

    /**
    * Returns the last booking in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching booking
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a matching booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking findByDomain_Last(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomain_Last(domainId, orderByComparator);
    }

    /**
    * Returns the last booking in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching booking, or <code>null</code> if a matching booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking fetchByDomain_Last(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByDomain_Last(domainId, orderByComparator);
    }

    /**
    * Returns the bookings before and after the current booking in the ordered set where domainId = &#63;.
    *
    * @param id the primary key of the current booking
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next booking
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking[] findByDomain_PrevAndNext(
        long id, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDomain_PrevAndNext(id, domainId, orderByComparator);
    }

    /**
    * Removes all the bookings where domainId = &#63; from the database.
    *
    * @param domainId the domain ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDomain(long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByDomain(domainId);
    }

    /**
    * Returns the number of bookings where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @return the number of matching bookings
    * @throws SystemException if a system exception occurred
    */
    public static int countByDomain(long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByDomain(domainId);
    }

    /**
    * Returns all the bookings where personId = &#63;.
    *
    * @param personId the person ID
    * @return the matching bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findByPerson(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson(personId);
    }

    /**
    * Returns a range of all the bookings where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of bookings
    * @param end the upper bound of the range of bookings (not inclusive)
    * @return the range of matching bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findByPerson(
        long personId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson(personId, start, end);
    }

    /**
    * Returns an ordered range of all the bookings where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of bookings
    * @param end the upper bound of the range of bookings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findByPerson(
        long personId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPerson(personId, start, end, orderByComparator);
    }

    /**
    * Returns the first booking in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching booking
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a matching booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking findByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson_First(personId, orderByComparator);
    }

    /**
    * Returns the first booking in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching booking, or <code>null</code> if a matching booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking fetchByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPerson_First(personId, orderByComparator);
    }

    /**
    * Returns the last booking in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching booking
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a matching booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking findByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson_Last(personId, orderByComparator);
    }

    /**
    * Returns the last booking in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching booking, or <code>null</code> if a matching booking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking fetchByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPerson_Last(personId, orderByComparator);
    }

    /**
    * Returns the bookings before and after the current booking in the ordered set where personId = &#63;.
    *
    * @param id the primary key of the current booking
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next booking
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking[] findByPerson_PrevAndNext(
        long id, long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.booking.NoSuchBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPerson_PrevAndNext(id, personId, orderByComparator);
    }

    /**
    * Removes all the bookings where personId = &#63; from the database.
    *
    * @param personId the person ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByPerson(personId);
    }

    /**
    * Returns the number of bookings where personId = &#63;.
    *
    * @param personId the person ID
    * @return the number of matching bookings
    * @throws SystemException if a system exception occurred
    */
    public static int countByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByPerson(personId);
    }

    /**
    * Caches the booking in the entity cache if it is enabled.
    *
    * @param booking the booking
    */
    public static void cacheResult(
        br.com.atilo.jcondo.booking.model.Booking booking) {
        getPersistence().cacheResult(booking);
    }

    /**
    * Caches the bookings in the entity cache if it is enabled.
    *
    * @param bookings the bookings
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.booking.model.Booking> bookings) {
        getPersistence().cacheResult(bookings);
    }

    /**
    * Creates a new booking with the primary key. Does not add the booking to the database.
    *
    * @param id the primary key for the new booking
    * @return the new booking
    */
    public static br.com.atilo.jcondo.booking.model.Booking create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the booking with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the booking
    * @return the booking that was removed
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking remove(long id)
        throws br.com.atilo.jcondo.booking.NoSuchBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.booking.model.Booking updateImpl(
        br.com.atilo.jcondo.booking.model.Booking booking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(booking);
    }

    /**
    * Returns the booking with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingException} if it could not be found.
    *
    * @param id the primary key of the booking
    * @return the booking
    * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.booking.NoSuchBookingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the booking with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the booking
    * @return the booking, or <code>null</code> if a booking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.booking.model.Booking fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the bookings.
    *
    * @return the bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the bookings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of bookings
    * @param end the upper bound of the range of bookings (not inclusive)
    * @return the range of bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the bookings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of bookings
    * @param end the upper bound of the range of bookings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of bookings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.booking.model.Booking> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the bookings from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of bookings.
    *
    * @return the number of bookings
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static BookingPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (BookingPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.booking.service.ClpSerializer.getServletContextName(),
                    BookingPersistence.class.getName());

            ReferenceRegistry.registerReference(BookingUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(BookingPersistence persistence) {
    }
}
