package br.com.atilo.jcondo.commons.faces;

import java.text.NumberFormat;
import java.text.ParseException;

import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@FacesConverter("currencyConverter")
public class CurrencyConverter implements Converter {

	private static final String WHOLE_DAY = "100";
	
	@Override
	public Object getAsObject(FacesContext facescontext, UIComponent uicomponent, String value) {
		if (value.equals(WHOLE_DAY) || value.equalsIgnoreCase("Dia Inteiro")) {
			return -1;
		} else {
			try {
				return NumberFormat.getInstance().parse(value);
			} catch (ParseException e) {
				return null;
			}
		}
	}

	@Override
	public String getAsString(FacesContext facescontext, UIComponent uicomponent, Object obj) {
		String period = NumberFormat.getInstance().format(obj);
		return period.equals("-1") ? WHOLE_DAY : period;
	}

}
