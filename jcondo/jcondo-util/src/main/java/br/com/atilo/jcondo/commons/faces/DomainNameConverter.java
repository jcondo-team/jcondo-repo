package br.com.atilo.jcondo.commons.faces;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang.StringUtils;

import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Telephone;

public class DomainNameConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Telephone phone = (Telephone) ((UIOutput) component).getValue();
		if (!StringUtils.isEmpty(value)) {
			String fullNumber = value.replaceAll("[^0-9]+", "");
			phone.setExtension(Long.parseLong(fullNumber.substring(0, 2)));
			phone.setNumber(Long.parseLong(fullNumber.substring(2)));
		} else {
			phone.setExtension(0);
			phone.setNumber(0);
		}
		return phone;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object obj) {
		if (obj instanceof Flat) {
			Flat flat = (Flat) obj;
			return "";
		} else {
			Enterprise enterprise = (Enterprise) obj;
			try {
				return enterprise.getName();
			} catch (Exception e) {
				return null;
			}
		}
	}

}
