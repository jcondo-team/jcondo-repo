package br.com.atilo.jcondo.commons.faces;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.atilo.jcondo.manager.NoSuchVehicleException;
import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;

@FacesConverter("vehicleConverter")
public class VehicleConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext facescontext, UIComponent uicomponent, String s) {
		if (s != null && !s.equals("0")) {
			try {
				return VehicleLocalServiceUtil.getVehicleByLicense(s);
			} catch (NoSuchVehicleException e) {
				// Do nothing
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext facescontext, UIComponent uicomponent, Object obj) {
		return obj != null ? ((Vehicle) obj).getLicense() : "0";
	}

}
