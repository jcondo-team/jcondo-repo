package br.com.atilo.jcondo.commons.collections;

import org.apache.commons.collections.Predicate;

import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;


public class PersonTypePredicate implements Predicate {

	private PersonType type;

	private long domainId;

	public PersonTypePredicate(PersonType type, long domainId) {
		this.type = type;
		this.domainId = domainId;
	}	

	public PersonTypePredicate(PersonType type) {
		this(type, 0);
	}
	
	@Override
	public boolean evaluate(Object obj) {
		if (obj instanceof Membership) {
			return ((Membership) obj).getType().equals(type);
		} else if (obj instanceof Person) {
			try {
				return obj != null && obj instanceof Person && ((Person) obj).getType(domainId) == type;
			} catch (Exception e) {
				return false;
			}
		}

		return false;
	}

}
