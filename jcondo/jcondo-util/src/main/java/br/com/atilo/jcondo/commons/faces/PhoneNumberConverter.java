package br.com.atilo.jcondo.commons.faces;

import javax.faces.component.UIComponent;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import org.apache.commons.lang.StringUtils;

import br.com.atilo.jcondo.manager.model.Telephone;

public class PhoneNumberConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Telephone phone = (Telephone) ((UIOutput) component).getValue();
		if (!StringUtils.isEmpty(value)) {
			String fullNumber = value.replaceAll("[^0-9]+", "");
			phone.setExtension(Long.parseLong(fullNumber.substring(0, 2)));
			phone.setNumber(Long.parseLong(fullNumber.substring(2)));
		} else {
			phone.setExtension(0);
			phone.setNumber(0);
		}
		return phone;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object obj) {
		Telephone telephone = (Telephone) obj;
		String fullNumber = String.valueOf(telephone.getExtension()).concat(String.valueOf(telephone.getNumber()));
		return fullNumber.replaceAll("([0-9]{2,2})([0-9]{4,5})([0-9]{4,4})", "($1) $2-$3");
	}

}
