package br.com.atilo.jcondo.commons.faces;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;

import com.liferay.portal.model.BaseModel;

@FacesConverter("enterpriseConverter")
public class EnterpriseConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext facescontext, UIComponent uicomponent, String s) {
		if (s != null && !s.equals("0")) {
			try {
				return EnterpriseLocalServiceUtil.getEnterprise(Long.parseLong(s));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext facescontext, UIComponent uicomponent, Object obj) {
		return obj != null ? String.valueOf(((BaseModel<?>) obj).getPrimaryKeyObj()) : "0";
	}

}
