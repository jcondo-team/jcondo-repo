package br.com.atilo.jcondo.commons.collections;

import java.util.Comparator;

import br.com.atilo.jcondo.manager.model.Vehicle;

public class VehicleLicenseComparator implements Comparator<Vehicle> {

	@Override
	public int compare(Vehicle v1, Vehicle v2) {
		try {
			return v1.getLicense().compareToIgnoreCase(v2.getLicense());
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

}
