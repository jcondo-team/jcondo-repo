package br.com.atilo.jcondo.commons.faces;

import java.util.List;

import javax.faces.component.UIComponent;
import javax.faces.component.UISelectItems;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.InstanceofPredicate;

import br.com.atilo.jcondo.commons.collections.IdPredicate;

import com.liferay.portal.model.BaseModel;

@FacesConverter("selectItemsConverter")
public class SelectItemsConverter implements Converter {

	@Override
	@SuppressWarnings("unchecked")
	public Object getAsObject(FacesContext facescontext, UIComponent uicomponent, String s) {
		if (s != null && !s.equals("0")) {
			UISelectItems items = (UISelectItems) CollectionUtils.find(uicomponent.getChildren(), 
																	   new InstanceofPredicate(UISelectItems.class));

			List<BaseModel<?>> models = (List<BaseModel<?>>) items.getValue();
			return CollectionUtils.find(models, new IdPredicate(Long.parseLong(s)));
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext facescontext, UIComponent uicomponent, Object obj) {
		return obj != null ? String.valueOf(((BaseModel<?>) obj).getPrimaryKeyObj()) : "0";
	}

}
