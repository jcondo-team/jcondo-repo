package br.com.atilo.jcondo.commons.collections;

import org.apache.commons.collections.Transformer;

import br.com.atilo.jcondo.manager.model.Membership;

public class PersonTransformer implements Transformer {

	@Override
	public Object transform(Object obj) {
		if (obj != null) {
			try {
				return ((Membership) obj).getPerson();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
