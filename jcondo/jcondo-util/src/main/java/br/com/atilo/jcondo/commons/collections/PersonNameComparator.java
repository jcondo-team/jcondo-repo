package br.com.atilo.jcondo.commons.collections;

import java.util.Comparator;

import br.com.atilo.jcondo.manager.model.Person;

public class PersonNameComparator implements Comparator<Person> {

	@Override
	public int compare(Person p1, Person p2) {
		try {
			return p1.getFullName().compareToIgnoreCase(p2.getFullName());
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}
	}

}
