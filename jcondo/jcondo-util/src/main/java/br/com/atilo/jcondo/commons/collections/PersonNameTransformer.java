package br.com.atilo.jcondo.commons.collections;

import org.apache.commons.collections.Transformer;

import br.com.atilo.jcondo.manager.model.Person;

public class PersonNameTransformer implements Transformer {

	@Override
	public Object transform(Object obj) {
		if (obj != null && obj instanceof Person) {
			try {
				Person person = (Person) obj;
				return person.getName().concat(" ").concat(person.getSurname());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return null;
	}

}
