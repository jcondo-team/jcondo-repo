package br.com.atilo.jcondo.commons.collections;

import org.apache.commons.collections.Predicate;

import br.com.atilo.jcondo.manager.model.Membership;

public class DomainPredicate implements Predicate {

	private long domainId;
	
	public DomainPredicate(long domainId) {
		this.domainId = domainId;
	}

	@Override
	public boolean evaluate(Object obj) {
		Membership m = (Membership) obj;

		if (m.getDomainId() == domainId) {
			return true;
		}

		return false;
	}

}
