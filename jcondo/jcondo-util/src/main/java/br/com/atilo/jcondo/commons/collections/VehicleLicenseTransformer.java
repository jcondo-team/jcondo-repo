package br.com.atilo.jcondo.commons.collections;

import org.apache.commons.collections.Transformer;

import br.com.atilo.jcondo.manager.model.Access;
import br.com.atilo.jcondo.manager.model.Vehicle;

public class VehicleLicenseTransformer implements Transformer {

	@Override
	public Object transform(Object obj) {
		if (obj != null) {
			if(obj instanceof Vehicle) {
				try {
					Vehicle vehicle = (Vehicle) obj;
					return vehicle.getLicense();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else if(obj instanceof Access) {
				try {
					Vehicle vehicle = ((Access) obj).getVehicle();

					if (vehicle != null) {
						return vehicle.getLicense();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}

}
