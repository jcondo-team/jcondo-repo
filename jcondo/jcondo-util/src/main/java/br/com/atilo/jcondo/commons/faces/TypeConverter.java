package br.com.atilo.jcondo.commons.faces;

import javax.faces.component.UIComponent;

import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import br.com.atilo.jcondo.datatype.PersonType;

@FacesConverter("typeConverter")
public class TypeConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext facescontext, UIComponent uicomponent, String s) {
		if (s != null && !s.equals("null")) {
			try {
				return Enum.valueOf(PersonType.class, s.split("@")[1]);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return null;
	}

	@Override
	public String getAsString(FacesContext facescontext, UIComponent uicomponent, Object obj) {
		return obj != null ? obj.toString() : "null";
	}

}
