package br.com.atilo.jcondo.datatype;

public enum EnterpriseStatus {

	ENABLED("enterprise.status.enabled"),
	DISABLED("enterprise.status.disabled");

	private String label;
	
	private EnterpriseStatus(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static EnterpriseStatus valueOf(int ordinal) {
		for (EnterpriseStatus status : values()) {
			if (status.ordinal() == ordinal) {
				return status;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "@" + this.name();
	}

}
