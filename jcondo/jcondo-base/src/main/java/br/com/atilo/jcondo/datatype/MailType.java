package br.com.atilo.jcondo.datatype;

public enum MailType {

	ENVELOPE("mail.type.envelope"),
	PACKAGE("mail.type.package");

	private String label;

	private MailType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

}
