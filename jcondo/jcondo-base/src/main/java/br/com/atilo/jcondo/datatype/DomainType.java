package br.com.atilo.jcondo.datatype;

public enum DomainType {

	ADMINISTRATION("administration"),
	SUB_ADMINISTRATION("sub-administration"),
	SUPPLIER("supplier"),
	FLAT("flat");

	private String label;

	private DomainType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static DomainType parse(String label) {
		for (DomainType type : values()) {
			if (type.getLabel().equalsIgnoreCase(label)) {
				return type;
			}
		}
		return null;
	}
}
