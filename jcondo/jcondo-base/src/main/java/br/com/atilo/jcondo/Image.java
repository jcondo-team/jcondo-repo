package br.com.atilo.jcondo;

import java.io.Serializable;

import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

public class Image implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long id;
	
	private String path;
	
	private int width;
	
	private int height;

	public Image() {
		
	}

	public Image(long id, String path) {
		super();
		this.id = id;
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		this.path = "http://" + sc.getRequest().getServerName() + path;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}
	
}
