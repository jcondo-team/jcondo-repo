package br.com.atilo.jcondo.datatype;

public enum PhoneType {

	HOME("phone.type.home", "tty"),
	WORK("phone.type.work", "business"),
	MOBILE("phone.type.mobile", "mobile-phone"),
	OTHER("phone.type.other", "other");

	private String label;
	
	private String typeName;

	private PhoneType(String label, String typeName) {
		this.label = label;
		this.typeName = typeName;
	}

	public String getLabel() {
		return label;
	}

	public String getTypeName() {
		return typeName;
	}

	public static PhoneType parse(String typeName) {
		if (typeName != null) {
			for (PhoneType phoneType : values()) {
				if (phoneType.getTypeName().equalsIgnoreCase(typeName)) {
					return phoneType;
				}
			}
		}

		return null;
	}
	
	@Override
	public String toString() {
		return getTypeName();
	}

}
