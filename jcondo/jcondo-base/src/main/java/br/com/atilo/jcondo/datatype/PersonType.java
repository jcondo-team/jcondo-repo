package br.com.atilo.jcondo.datatype;

public enum PersonType {

    OWNER("person.type.owner"),
    RENTER("person.type.renter"),
    DEPENDENT("person.type.dependent"),
    VISITOR("person.type.visitor"),
    EMPLOYEE("person.type.employee"),
    MANAGER("person.type.manager"),
    SYNCDIC("person.type.syndic"),
    SUB_SYNDIC("person.type.sub-syndic"),
    ADMIN_ADVISOR("person.type.admin-advisor"),
    TAX_ADVISOR("person.type.tax-advisor"),
    ADMIN_ASSISTANT("person.type.admin-assistant"),
    GATEKEEPER("person.type.gatekeeper"),
    OUTSOURCED("person.type.outsourced");

	public static PersonType[] FLAT_TYPES = {MANAGER, RENTER, DEPENDENT, OWNER, VISITOR};

	public static PersonType[] SUPPLIER_TYPES = {EMPLOYEE};

	public static PersonType[] ADMIN_TYPES = {ADMIN_ADVISOR, TAX_ADVISOR, EMPLOYEE, ADMIN_ASSISTANT, OUTSOURCED, SYNCDIC, SUB_SYNDIC};

    private String label;

	private PersonType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static PersonType valueOf(int ordinal) {
		for (PersonType type : values()) {
			if (type.ordinal() == ordinal) {
				return type;
			}
		}
		return null;
	}

	@Override
	public String toString() {
		return this.getClass().getName() + "@" + this.name();
	}
}
