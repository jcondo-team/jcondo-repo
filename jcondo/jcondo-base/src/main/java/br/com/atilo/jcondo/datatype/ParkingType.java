package br.com.atilo.jcondo.datatype;

public enum ParkingType {

	RESIDENT("parking.type.resident"),
	VISITOR("parking.type.visitor");

	private String label;

	private ParkingType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	@Override
	public String toString() {
		return label;
	}
	
	public static ParkingType valueOf(int ordinal) {
		for (ParkingType type : values()) {
			if (type.ordinal() == ordinal) {
				return type;
			}
		}
		return null;
	}
}
