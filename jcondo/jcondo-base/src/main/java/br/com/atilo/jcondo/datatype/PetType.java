package br.com.atilo.jcondo.datatype;

public enum PetType {

	DOG("pet.type.dog"),
	CAT("pet.type.cat"),
	BIRD("pet.type.bird"),
	OTHER("pet.type.other");

	private String label;

	private PetType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}
	
	public static PetType valueOf(int ordinal) {
		for (PetType type : values()) {
			if (type.ordinal() == ordinal) {
				return type;
			}
		}
		return null;
	}
	
}
