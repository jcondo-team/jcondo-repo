package br.com.atilo.jcondo.support.bean;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.support.model.Issue;
import br.com.atilo.jcondo.support.model.datatype.IssueSpace;
import br.com.atilo.jcondo.support.model.datatype.IssueType;
import br.com.atilo.jcondo.support.service.IssueServiceUtil;

@ManagedBean(name="issueBean")
@ViewScoped
public class IssueBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(IssueBean.class);

	private Person person;

	private Person assignee;

	private List<Issue> issues;

	private List<IssueType> types;

	private IssueType type;

	private String text;

	@PostConstruct
	public void init() {
		try {
			person = PersonLocalServiceUtil.getPerson();
			assignee = PersonLocalServiceUtil.getPeopleByType(PersonType.SYNCDIC).get(0);
			issues = IssueServiceUtil.getPersonIssuesBySpace(person.getId(), IssueSpace.COMPLAINT_BOOK.ordinal());
			types = Arrays.asList(IssueType.COMPLAINT, IssueType.SUGGESTION);
		} catch (Exception e) {
			LOGGER.fatal("Failure on room initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "general.failure", null);
		}
	}

	public void onSave() {
		try {
			issues.add(0, IssueServiceUtil.addIssue(null, text, type, 
													IssueSpace.COMPLAINT_BOOK, 
													assignee.getId(), null));
			String label = ResourceBundle.getBundle("Language").getString(type.getLabel());
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.issue.add", new String[] {label.toLowerCase()});
			text = null;
			type = null;
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on issue save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.fatal("Failure on issue save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public List<Issue> getIssues() {
		return issues;
	}

	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}

	public List<IssueType> getTypes() {
		return types;
	}

	public void setTypes(List<IssueType> types) {
		this.types = types;
	}

	public IssueType getType() {
		return type;
	}

	public void setType(IssueType type) {
		this.type = type;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
