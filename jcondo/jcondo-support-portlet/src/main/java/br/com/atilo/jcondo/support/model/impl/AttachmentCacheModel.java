package br.com.atilo.jcondo.support.model.impl;

import br.com.atilo.jcondo.support.model.Attachment;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Attachment in entity cache.
 *
 * @author anderson
 * @see Attachment
 * @generated
 */
public class AttachmentCacheModel implements CacheModel<Attachment>,
    Externalizable {
    public long attachmentId;
    public long issueId;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{attachmentId=");
        sb.append(attachmentId);
        sb.append(", issueId=");
        sb.append(issueId);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Attachment toEntityModel() {
        AttachmentImpl attachmentImpl = new AttachmentImpl();

        attachmentImpl.setAttachmentId(attachmentId);
        attachmentImpl.setIssueId(issueId);

        if (oprDate == Long.MIN_VALUE) {
            attachmentImpl.setOprDate(null);
        } else {
            attachmentImpl.setOprDate(new Date(oprDate));
        }

        attachmentImpl.setOprUser(oprUser);

        attachmentImpl.resetOriginalValues();

        return attachmentImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        attachmentId = objectInput.readLong();
        issueId = objectInput.readLong();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(attachmentId);
        objectOutput.writeLong(issueId);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
