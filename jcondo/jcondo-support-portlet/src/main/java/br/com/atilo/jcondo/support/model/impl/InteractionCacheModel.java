package br.com.atilo.jcondo.support.model.impl;

import br.com.atilo.jcondo.support.model.Interaction;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Interaction in entity cache.
 *
 * @author anderson
 * @see Interaction
 * @generated
 */
public class InteractionCacheModel implements CacheModel<Interaction>,
    Externalizable {
    public long id;
    public long issueId;
    public long personId;
    public String text;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{id=");
        sb.append(id);
        sb.append(", issueId=");
        sb.append(issueId);
        sb.append(", personId=");
        sb.append(personId);
        sb.append(", text=");
        sb.append(text);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Interaction toEntityModel() {
        InteractionImpl interactionImpl = new InteractionImpl();

        interactionImpl.setId(id);
        interactionImpl.setIssueId(issueId);
        interactionImpl.setPersonId(personId);

        if (text == null) {
            interactionImpl.setText(StringPool.BLANK);
        } else {
            interactionImpl.setText(text);
        }

        if (oprDate == Long.MIN_VALUE) {
            interactionImpl.setOprDate(null);
        } else {
            interactionImpl.setOprDate(new Date(oprDate));
        }

        interactionImpl.setOprUser(oprUser);

        interactionImpl.resetOriginalValues();

        return interactionImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        issueId = objectInput.readLong();
        personId = objectInput.readLong();
        text = objectInput.readUTF();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(issueId);
        objectOutput.writeLong(personId);

        if (text == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(text);
        }

        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
