package br.com.atilo.jcondo.support.bean;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.portlet.PortletRequest;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.SelectEvent;

import com.liferay.portal.util.PortalUtil;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.support.model.Interaction;
import br.com.atilo.jcondo.support.model.Issue;
import br.com.atilo.jcondo.support.model.datatype.IssueSpace;
import br.com.atilo.jcondo.support.model.datatype.IssueStatus;
import br.com.atilo.jcondo.support.model.datatype.IssueType;
import br.com.atilo.jcondo.support.service.InteractionLocalServiceUtil;
import br.com.atilo.jcondo.support.service.InteractionServiceUtil;
import br.com.atilo.jcondo.support.service.IssueServiceUtil;
import br.com.atilo.jcondo.support.service.permission.IssuePermission;

@ManagedBean
@SessionScoped
public class InteractionBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(InteractionBean.class);

	protected static final ResourceBundle rb = ResourceBundle.getBundle("Language");

	private Interaction interaction;

	private Person person;

	private Flat flat;
	
	private List<Flat> flats;

	private Issue issue;

	private List<Issue> issues;

	private List<IssueType> types;

	private String text;
	
	private String personName;

	@PostConstruct
	public void init() {
		try {
			person = PersonLocalServiceUtil.getPerson();
			issues = IssueServiceUtil.getSpaceIssues(IssueSpace.COMPLAINT_BOOK.ordinal());
			types = Arrays.asList(IssueType.COMPLAINT, IssueType.SUGGESTION);
			flats = FlatLocalServiceUtil.getFlats();
		} catch (Exception e) {
			LOGGER.fatal("Failure on interaction initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public void onLoad() {
		String code = PortalUtil.getOriginalServletRequest(
						PortalUtil.getHttpServletRequest(
							(PortletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest())).getParameter("code");

		if (!StringUtils.isEmpty(code)) {
			try {
				issue = IssueServiceUtil.getIssue(code);
			} catch (Exception e) {
				// TODO: handle exception
			}
		} else {
			issue = issues.isEmpty() ? null : issues.get(0);
		}

		selectInteraction(issue);
	}

	public void onSave(boolean close) {
		try {
			if (interaction.isNew()) {
				interaction = InteractionServiceUtil.addInteraction(interaction);
			} else {
				interaction = InteractionServiceUtil.updateInteraction(interaction);
			}

			if (close) {
				IssueServiceUtil.updateIssueStatus(issue.getId(), IssueStatus.CLOSE);
				issue.setStatus(IssueStatus.CLOSE);
				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.interaction.add", null);
			} else {
				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.interaction.add.as.draft", null);
			}
		} catch (BusinessException e) {
			LOGGER.error("Business failure on interaction save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.fatal("Failure on interaction save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onFilterChange() {
		try {
			if (flat != null) {
				if (!StringUtils.isEmpty(personName)) {
					issues = IssueServiceUtil.getPersonFlatIssues(personName, flat.getId());
				} else {
					issues = IssueServiceUtil.getFlatIssues(flat.getId());	
				}
			} else if (!StringUtils.isEmpty(personName)) {
				issues = IssueServiceUtil.getPersonIssues(personName);
			} else {
				issues = IssueServiceUtil.getSpaceIssues(IssueSpace.COMPLAINT_BOOK.ordinal());
			}
		} catch (BusinessException e) {
			LOGGER.warn("Business failure on flat select", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.fatal("Failure on flat select", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onIssueSelect(SelectEvent event) {
		issue = (Issue) event.getObject();
		selectInteraction(issue);
	}

	public void selectInteraction(Issue issue) {
		try {
			if (CollectionUtils.isEmpty(issue.getInteractions())) {
				interaction = InteractionLocalServiceUtil.createInteraction(0);
				interaction.setIssueId(issue.getId());
				interaction.setPersonId(person.getId());
			} else {
				interaction = issue.getInteractions().get(0);
			}
		} catch (Exception e) {
			LOGGER.error("selectInteraction failure", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public String showIssueStyle(Issue issue) {
		try {
			if (issue.getStatus() != IssueStatus.CLOSE) {
				Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
				Date deadline = DateUtils.addDays(DateUtils.truncate(issue.getDate(), Calendar.DAY_OF_MONTH), 
												  Integer.valueOf(ApplicationBean.CONFIG.getString("issue.answer.deadline")));
				if (today.after(deadline)) {
					return "not-answered red-alert";
				} else if (today.after(DateUtils.addDays(deadline, -4))) {
					return "not-answered orange-alert";
				} else {
					return "not-answered";
				}
			}
		} catch (Exception e) {
			LOGGER.error("showIssueStyle failure", e);
			return "not-answered";
		}

		return "answered";
	}

	public String displayDomain(Person person) {
		try {
			if (person == null || person.getDomain() == null) {
				return null;
			}

			if (person.getDomain() instanceof Flat) {
				Flat flat = (Flat) person.getDomain();
				return MessageFormat.format(rb.getString("person.flat"), String.valueOf(flat.getNumber()), flat.getBlock());
			} else {
				Department department = (Department) person.getDomain();
				return department.getName();
			}
		} catch (Exception e) {
			LOGGER.error("displayDomain failure", e);
			return null;
		}
	}
	
	public boolean canAnswer() {
		try {
			return issue.getStatus() != IssueStatus.CLOSE && 
					IssuePermission.hasPermission(Permission.ADD_INTERACTION, issue.getId());
		} catch (Exception e) {
			return false;
		}
	}

	public Interaction getInteraction() {
		return interaction;
	}

	public void setInteraction(Interaction interaction) {
		this.interaction = interaction;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Flat getFlat() {
		return flat;
	}

	public void setFlat(Flat flat) {
		this.flat = flat;
	}

	public List<Flat> getFlats() {
		return flats;
	}

	public void setFlats(List<Flat> flats) {
		this.flats = flats;
	}

	public Issue getIssue() {
		return issue;
	}

	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	public List<Issue> getIssues() {
		return issues;
	}

	public void setIssues(List<Issue> issues) {
		this.issues = issues;
	}

	public List<IssueType> getTypes() {
		return types;
	}

	public void setTypes(List<IssueType> types) {
		this.types = types;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}
}
