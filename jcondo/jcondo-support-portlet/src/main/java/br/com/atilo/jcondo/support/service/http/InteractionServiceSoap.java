package br.com.atilo.jcondo.support.service.http;

import br.com.atilo.jcondo.support.service.InteractionServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.support.service.InteractionServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.support.model.InteractionSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.support.model.Interaction}, that is translated to a
 * {@link br.com.atilo.jcondo.support.model.InteractionSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author anderson
 * @see InteractionServiceHttp
 * @see br.com.atilo.jcondo.support.model.InteractionSoap
 * @see br.com.atilo.jcondo.support.service.InteractionServiceUtil
 * @generated
 */
public class InteractionServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(InteractionServiceSoap.class);

    public static br.com.atilo.jcondo.support.model.InteractionSoap createInteraction()
        throws RemoteException {
        try {
            br.com.atilo.jcondo.support.model.Interaction returnValue = InteractionServiceUtil.createInteraction();

            return br.com.atilo.jcondo.support.model.InteractionSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.InteractionSoap addInteraction(
        br.com.atilo.jcondo.support.model.InteractionSoap interaction)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.support.model.Interaction returnValue = InteractionServiceUtil.addInteraction(br.com.atilo.jcondo.support.model.impl.InteractionModelImpl.toModel(
                        interaction));

            return br.com.atilo.jcondo.support.model.InteractionSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.InteractionSoap updateInteraction(
        br.com.atilo.jcondo.support.model.InteractionSoap interaction)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.support.model.Interaction returnValue = InteractionServiceUtil.updateInteraction(br.com.atilo.jcondo.support.model.impl.InteractionModelImpl.toModel(
                        interaction));

            return br.com.atilo.jcondo.support.model.InteractionSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.InteractionSoap getInteraction(
        long issueId) throws RemoteException {
        try {
            br.com.atilo.jcondo.support.model.Interaction returnValue = InteractionServiceUtil.getInteraction(issueId);

            return br.com.atilo.jcondo.support.model.InteractionSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
