/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.support.service.impl;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.lang.StringUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portlet.documentlibrary.NoSuchFolderException;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;

import br.com.atilo.jcondo.support.model.Attachment;
import br.com.atilo.jcondo.support.service.base.AttachmentLocalServiceBaseImpl;

/**
 * The implementation of the attachment local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.support.service.AttachmentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.support.service.base.AttachmentLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.support.service.AttachmentLocalServiceUtil
 */
public class AttachmentLocalServiceImpl extends AttachmentLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.support.service.AttachmentLocalServiceUtil} to access the attachment local service.
	 */
	private static final ResourceBundle RB = ResourceBundle.getBundle("application");
	
	private Folder getIssueAttachementsFolder(long issueId) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		long repositoryId = GroupLocalServiceUtil.getGroup(sc.getCompanyId(), GroupConstants.GUEST).getGroupId();
		Folder parentFolder = DLAppServiceUtil.getFolder(repositoryId,
														 DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, 
														 RB.getString("issues.root.folder.name"));
		Folder folder;
		try {
			folder = DLAppServiceUtil.getFolder(repositoryId, parentFolder.getFolderId(), String.valueOf(issueId));
		} catch (NoSuchFolderException e) {
			folder = DLAppServiceUtil.addFolder(parentFolder.getRepositoryId(), 
												parentFolder.getFolderId(), String.valueOf(issueId), null, sc);
			DLAppServiceUtil.addFolder(folder.getRepositoryId(), folder.getFolderId(), "bin", null, sc);
		}

		return folder;
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Attachment addAttachment(Attachment attachment) throws SystemException {
		return super.addAttachment(attachment);
	}
	
	public void addIssueAttachments(long issueId, List<File> attachments) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Folder folder = getIssueAttachementsFolder(issueId);

		for (File file : attachments) {
			FileEntry fileEntry = DLAppServiceUtil.addFileEntry(folder.getRepositoryId(), folder.getFolderId(), 
					  								  			file.getName(), MimeTypesUtil.getContentType(file), 
					  								  			file.getName(), null, StringUtils.EMPTY, file, sc);

			Attachment attachment = createAttachment(fileEntry.getFileEntryId());
			attachment.setIssueId(issueId);
			attachment.setOprDate(new Date());
			attachment.setOprUser(sc.getUserId());
			super.addAttachment(attachment);
		}
	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public Attachment deleteAttachment(Attachment attachment) throws SystemException {
		try {
			return deleteAttachment(attachment.getAttachmentId());
		} catch (PortalException e) {
			throw new SystemException(e);
		}
	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public Attachment deleteAttachment(long attachmentId) throws PortalException, SystemException {
		Attachment attachment = getAttachment(attachmentId); 
		updateAttachment(attachment);

		try {
			Folder folder = getIssueAttachementsFolder(attachment.getIssueId());
			Folder binFolder = DLAppServiceUtil.getFolder(folder.getRepositoryId(), folder.getFolderId(), "bin");
			DLAppServiceUtil.moveFileEntry(attachmentId, binFolder.getFolderId(), ServiceContextThreadLocal.getServiceContext());
		} catch (Exception e) {
			// TODO: handle exception
		}

		return super.deleteAttachment(attachmentId);
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Attachment updateAttachment(Attachment attachment, boolean merge) throws SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		attachment.setOprDate(new Date());
		attachment.setOprUser(sc.getUserId());
		return super.updateAttachment(attachment);
	}
}