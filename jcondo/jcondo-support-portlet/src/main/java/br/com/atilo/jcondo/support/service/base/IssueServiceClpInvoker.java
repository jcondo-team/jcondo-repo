package br.com.atilo.jcondo.support.service.base;

import br.com.atilo.jcondo.support.service.IssueServiceUtil;

import java.util.Arrays;

/**
 * @author anderson
 * @generated
 */
public class IssueServiceClpInvoker {
    private String _methodName32;
    private String[] _methodParameterTypes32;
    private String _methodName33;
    private String[] _methodParameterTypes33;
    private String _methodName38;
    private String[] _methodParameterTypes38;
    private String _methodName39;
    private String[] _methodParameterTypes39;
    private String _methodName40;
    private String[] _methodParameterTypes40;
    private String _methodName41;
    private String[] _methodParameterTypes41;
    private String _methodName42;
    private String[] _methodParameterTypes42;
    private String _methodName43;
    private String[] _methodParameterTypes43;
    private String _methodName44;
    private String[] _methodParameterTypes44;
    private String _methodName45;
    private String[] _methodParameterTypes45;
    private String _methodName46;
    private String[] _methodParameterTypes46;
    private String _methodName47;
    private String[] _methodParameterTypes47;

    public IssueServiceClpInvoker() {
        _methodName32 = "getBeanIdentifier";

        _methodParameterTypes32 = new String[] {  };

        _methodName33 = "setBeanIdentifier";

        _methodParameterTypes33 = new String[] { "java.lang.String" };

        _methodName38 = "addIssue";

        _methodParameterTypes38 = new String[] {
                "java.lang.String", "java.lang.String",
                "br.com.atilo.jcondo.support.model.datatype.IssueType",
                "br.com.atilo.jcondo.support.model.datatype.IssueSpace", "long",
                "java.util.List"
            };

        _methodName39 = "updateIssue";

        _methodParameterTypes39 = new String[] {
                "br.com.atilo.jcondo.support.model.Issue"
            };

        _methodName40 = "updateIssueStatus";

        _methodParameterTypes40 = new String[] {
                "long", "br.com.atilo.jcondo.support.model.datatype.IssueStatus"
            };

        _methodName41 = "getIssue";

        _methodParameterTypes41 = new String[] { "java.lang.String" };

        _methodName42 = "getSpaceIssues";

        _methodParameterTypes42 = new String[] { "long" };

        _methodName43 = "getPersonIssues";

        _methodParameterTypes43 = new String[] { "long" };

        _methodName44 = "getPersonIssuesBySpace";

        _methodParameterTypes44 = new String[] { "long", "long" };

        _methodName45 = "getFlatIssues";

        _methodParameterTypes45 = new String[] { "long" };

        _methodName46 = "getPersonIssues";

        _methodParameterTypes46 = new String[] { "java.lang.String" };

        _methodName47 = "getPersonFlatIssues";

        _methodParameterTypes47 = new String[] { "java.lang.String", "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName32.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes32, parameterTypes)) {
            return IssueServiceUtil.getBeanIdentifier();
        }

        if (_methodName33.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes33, parameterTypes)) {
            IssueServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName38.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes38, parameterTypes)) {
            return IssueServiceUtil.addIssue((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                (br.com.atilo.jcondo.support.model.datatype.IssueType) arguments[2],
                (br.com.atilo.jcondo.support.model.datatype.IssueSpace) arguments[3],
                ((Long) arguments[4]).longValue(),
                (java.util.List<java.io.File>) arguments[5]);
        }

        if (_methodName39.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes39, parameterTypes)) {
            return IssueServiceUtil.updateIssue((br.com.atilo.jcondo.support.model.Issue) arguments[0]);
        }

        if (_methodName40.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes40, parameterTypes)) {
            return IssueServiceUtil.updateIssueStatus(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.support.model.datatype.IssueStatus) arguments[1]);
        }

        if (_methodName41.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes41, parameterTypes)) {
            return IssueServiceUtil.getIssue((java.lang.String) arguments[0]);
        }

        if (_methodName42.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes42, parameterTypes)) {
            return IssueServiceUtil.getSpaceIssues(((Long) arguments[0]).longValue());
        }

        if (_methodName43.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes43, parameterTypes)) {
            return IssueServiceUtil.getPersonIssues(((Long) arguments[0]).longValue());
        }

        if (_methodName44.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes44, parameterTypes)) {
            return IssueServiceUtil.getPersonIssuesBySpace(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        if (_methodName45.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes45, parameterTypes)) {
            return IssueServiceUtil.getFlatIssues(((Long) arguments[0]).longValue());
        }

        if (_methodName46.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes46, parameterTypes)) {
            return IssueServiceUtil.getPersonIssues((java.lang.String) arguments[0]);
        }

        if (_methodName47.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes47, parameterTypes)) {
            return IssueServiceUtil.getPersonFlatIssues((java.lang.String) arguments[0],
                ((Long) arguments[1]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
