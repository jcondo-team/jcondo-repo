/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.support.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.support.NoSuchIssueException;
import br.com.atilo.jcondo.support.model.Issue;
import br.com.atilo.jcondo.support.model.datatype.IssueSpace;
import br.com.atilo.jcondo.support.model.datatype.IssueStatus;
import br.com.atilo.jcondo.support.model.datatype.IssueType;
import br.com.atilo.jcondo.support.service.base.IssueLocalServiceBaseImpl;
import br.com.atilo.jcondo.support.service.persistence.IssueFinderUtil;

/**
 * The implementation of the issue local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.support.service.IssueLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.support.service.base.IssueLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.support.service.IssueLocalServiceUtil
 */
public class IssueLocalServiceImpl extends IssueLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.support.service.IssueLocalServiceUtil} to access the issue local service.
	 */
	
	public Issue addIssue(String title, String text, IssueType type, IssueSpace space, long assigneeId, List<File> attachments) throws PortalException, SystemException {
		if (StringUtils.isEmpty(text)) {
			throw new BusinessException("exception.issue.text.empty");
		}

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Issue issue = createIssue(counterLocalService.increment());
		issue.setTitle(title);
		issue.setText(text);
		issue.setDate(new Date());
		issue.setCode(String.valueOf(issue.getId() + issue.getDate().getTime()));
		issue.setSpaceId(space.ordinal());
		issue.setTypeId(type.ordinal());
		issue.setStatusId(IssueStatus.OPEN.ordinal());
		issue.setPersonId(PersonLocalServiceUtil.getPerson().getId());
		issue.setAssigneeId(assigneeId);
		issue.setOprDate(issue.getDate());
		issue.setOprUser(sc.getUserId());

		issue = super.addIssue(issue);

		if (!CollectionUtils.isEmpty(attachments)) {
			attachmentLocalService.addIssueAttachments(issue.getId(), attachments);
		}
		
		return issue;
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Issue addIssue(Issue issue) throws SystemException {
		try {
			return addIssue(issue.getTitle(), issue.getText(), 
							issue.getType(), issue.getSpace(), 
							issue.getAssigneeId(), null);
		} catch (PortalException e) {
			throw new SystemException(e);
		}
	}
	
	public Issue updateIssueStatus(long issueId, IssueStatus status) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Issue issue = getIssue(issueId);
		issue.setStatus(status);
		issue.setOprDate(new Date());
		issue.setOprUser(sc.getUserId());
		return super.updateIssue(issue);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Issue updateIssue(Issue issue, boolean merge) throws SystemException {
		try {
			ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
			Issue i = getIssue(issue.getId());

			if (!StringUtils.equals(issue.getText(), i.getText())) {
				throw new BusinessException("exception.issue.text.is.unchangeable");
			}

			if (issue.getPersonId() != i.getPersonId()) {
				throw new BusinessException("exception.issue.person.is.unchangeable");
			}

			if (issue.getDate() == null || issue.getDate().getTime() != i.getDate().getTime()) {
				throw new BusinessException("exception.issue.date.is.unchangeable");
			}

			if (!StringUtils.equals(issue.getCode(), i.getCode())) {
				throw new BusinessException("exception.issue.code.is.unchangeable");
			}

			BeanUtils.copyProperties(i, issue);

			i.setOprDate(new Date());
			i.setOprUser(sc.getUserId());

			return super.updateIssue(i);
		} catch (Exception e) {
			throw new SystemException(e);
		}
	}

	public Issue getIssue(String code) throws NoSuchIssueException, SystemException {
		return issuePersistence.findByCode(code);
	}

	public List<Issue> getPersonIssues(long personId) throws SystemException {
		return new ArrayList<Issue>(issuePersistence.findByPerson(personId));
	}

	public List<Issue> getSpaceIssues(long spaceId) throws SystemException {
		return new ArrayList<Issue>(issuePersistence.findBySpace(spaceId));
	}

	public List<Issue> getPersonIssuesBySpace(long personId, long spaceId) throws SystemException {
		return new ArrayList<Issue>(issuePersistence.findByPersonAndSpace(personId, spaceId));
	}

	public List<Issue> getFlatIssues(long flatId) throws SystemException {
		return new ArrayList<Issue>(IssueFinderUtil.findFlatIssues(flatId));
	}

	public List<Issue> getPersonIssues(String name) throws SystemException {
		return new ArrayList<Issue>(IssueFinderUtil.findPersonIssues(name));
	}

	public List<Issue> getPersonFlatIssues(String name, long flatId) throws SystemException {
		return new ArrayList<Issue>(IssueFinderUtil.findPersonFlatIssues(name, flatId));
	}

}