/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.support.service.impl;

import java.io.File;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.support.model.Issue;
import br.com.atilo.jcondo.support.model.datatype.IssueSpace;
import br.com.atilo.jcondo.support.model.datatype.IssueStatus;
import br.com.atilo.jcondo.support.model.datatype.IssueType;
import br.com.atilo.jcondo.support.service.base.IssueServiceBaseImpl;
import br.com.atilo.jcondo.support.service.permission.IssuePermission;

/**
 * The implementation of the issue remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.support.service.IssueService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.support.service.base.IssueServiceBaseImpl
 * @see br.com.atilo.jcondo.support.service.IssueServiceUtil
 */
public class IssueServiceImpl extends IssueServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.support.service.IssueServiceUtil} to access the issue remote service.
	 */

	public Issue addIssue(String title, String text, IssueType type, IssueSpace space, long assigneeId, List<File> attachments) throws PortalException, SystemException {
		if (!IssuePermission.hasPermission(Permission.ADD, 0)) {
			throw new BusinessException("exception.issue.add.denied");
		}
		return issueLocalService.addIssue(title, text, type, space, assigneeId, attachments);
	}

	public Issue updateIssue(Issue issue) throws PortalException, SystemException {
		if (!IssuePermission.hasPermission(Permission.UPDATE, issue.getId())) {
			throw new BusinessException("exception.issue.update.denied");
		}
		return issueLocalService.updateIssue(issue);
	}

	public Issue updateIssueStatus(long issueId, IssueStatus status) throws PortalException, SystemException {
		if (!IssuePermission.hasPermission(Permission.UPDATE, issueId)) {
			throw new BusinessException("exception.issue.update.denied");
		}
		return issueLocalService.updateIssueStatus(issueId, status);
	}	

	public Issue getIssue(String code) throws PortalException, SystemException {
		Issue issue = issueLocalService.getIssue(code);
		if (!IssuePermission.hasPermission(Permission.VIEW, issue.getId())) {
			throw new BusinessException("exception.issue.view.denied");
		}
		return issue;
	}

	public List<Issue> getSpaceIssues(long spaceId) throws SystemException {
		return issueLocalService.getSpaceIssues(spaceId);
	}
	
	public List<Issue> getPersonIssues(long personId) throws SystemException {
		return issueLocalService.getPersonIssues(personId);
	}
	
	public List<Issue> getPersonIssuesBySpace(long personId, long spaceId) throws SystemException {
		return issueLocalService.getPersonIssuesBySpace(personId, spaceId);
	}

	public List<Issue> getFlatIssues(long flatId) throws SystemException {
		return issueLocalService.getFlatIssues(flatId);
	}

	public List<Issue> getPersonIssues(String name) throws SystemException {
		return issueLocalService.getPersonIssues(name);
	}

	public List<Issue> getPersonFlatIssues(String name, long flatId) throws SystemException {
		return issueLocalService.getPersonFlatIssues(name, flatId);
	}

}