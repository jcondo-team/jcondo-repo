package br.com.atilo.jcondo.support.model.impl;

import br.com.atilo.jcondo.support.model.Attachment;
import br.com.atilo.jcondo.support.service.AttachmentLocalServiceUtil;

import com.liferay.portal.kernel.exception.SystemException;

/**
 * The extended model base implementation for the Attachment service. Represents a row in the &quot;jco_issue_attachment&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This class exists only as a container for the default extended model level methods generated by ServiceBuilder. Helper methods and all application logic should be put in {@link AttachmentImpl}.
 * </p>
 *
 * @author anderson
 * @see AttachmentImpl
 * @see br.com.atilo.jcondo.support.model.Attachment
 * @generated
 */
public abstract class AttachmentBaseImpl extends AttachmentModelImpl
    implements Attachment {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. All methods that expect a attachment model instance should use the {@link Attachment} interface instead.
     */
    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            AttachmentLocalServiceUtil.addAttachment(this);
        } else {
            AttachmentLocalServiceUtil.updateAttachment(this);
        }
    }
}
