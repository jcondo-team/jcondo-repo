/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.support.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.support.model.Interaction;
import br.com.atilo.jcondo.support.model.datatype.IssueStatus;
import br.com.atilo.jcondo.support.service.base.InteractionServiceBaseImpl;
import br.com.atilo.jcondo.support.service.permission.IssuePermission;

/**
 * The implementation of the interaction remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.support.service.InteractionService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.support.service.base.InteractionServiceBaseImpl
 * @see br.com.atilo.jcondo.support.service.InteractionServiceUtil
 */
public class InteractionServiceImpl extends InteractionServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.support.service.InteractionServiceUtil} to access the interaction remote service.
	 */
	
	public Interaction createInteraction() {
		return interactionLocalService.createInteraction(0);
	}
	
	public Interaction addInteraction(Interaction interaction) throws PortalException, SystemException {
		if (interaction.getIssue().getStatus() == IssueStatus.CLOSE || 
				!IssuePermission.hasPermission(Permission.ADD, interaction.getIssueId())) {
			throw new BusinessException("exception.issue.interaction.add.denied");
		}
		return interactionLocalService.addInteraction(interaction);
	}

	public Interaction updateInteraction(Interaction interaction) throws PortalException, SystemException {
		return interactionLocalService.updateInteraction(interaction);
	}

	public Interaction getInteraction(long issueId) throws PortalException, SystemException {
		return interactionLocalService.getInteraction(issueId);
	}
}