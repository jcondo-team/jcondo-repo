package br.com.atilo.jcondo.support.model.impl;

import br.com.atilo.jcondo.support.model.Issue;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Issue in entity cache.
 *
 * @author anderson
 * @see Issue
 * @generated
 */
public class IssueCacheModel implements CacheModel<Issue>, Externalizable {
    public long id;
    public long personId;
    public long assigneeId;
    public long spaceId;
    public long typeId;
    public long statusId;
    public String code;
    public String title;
    public String text;
    public long date;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(25);

        sb.append("{id=");
        sb.append(id);
        sb.append(", personId=");
        sb.append(personId);
        sb.append(", assigneeId=");
        sb.append(assigneeId);
        sb.append(", spaceId=");
        sb.append(spaceId);
        sb.append(", typeId=");
        sb.append(typeId);
        sb.append(", statusId=");
        sb.append(statusId);
        sb.append(", code=");
        sb.append(code);
        sb.append(", title=");
        sb.append(title);
        sb.append(", text=");
        sb.append(text);
        sb.append(", date=");
        sb.append(date);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Issue toEntityModel() {
        IssueImpl issueImpl = new IssueImpl();

        issueImpl.setId(id);
        issueImpl.setPersonId(personId);
        issueImpl.setAssigneeId(assigneeId);
        issueImpl.setSpaceId(spaceId);
        issueImpl.setTypeId(typeId);
        issueImpl.setStatusId(statusId);

        if (code == null) {
            issueImpl.setCode(StringPool.BLANK);
        } else {
            issueImpl.setCode(code);
        }

        if (title == null) {
            issueImpl.setTitle(StringPool.BLANK);
        } else {
            issueImpl.setTitle(title);
        }

        if (text == null) {
            issueImpl.setText(StringPool.BLANK);
        } else {
            issueImpl.setText(text);
        }

        if (date == Long.MIN_VALUE) {
            issueImpl.setDate(null);
        } else {
            issueImpl.setDate(new Date(date));
        }

        if (oprDate == Long.MIN_VALUE) {
            issueImpl.setOprDate(null);
        } else {
            issueImpl.setOprDate(new Date(oprDate));
        }

        issueImpl.setOprUser(oprUser);

        issueImpl.resetOriginalValues();

        return issueImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        personId = objectInput.readLong();
        assigneeId = objectInput.readLong();
        spaceId = objectInput.readLong();
        typeId = objectInput.readLong();
        statusId = objectInput.readLong();
        code = objectInput.readUTF();
        title = objectInput.readUTF();
        text = objectInput.readUTF();
        date = objectInput.readLong();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(personId);
        objectOutput.writeLong(assigneeId);
        objectOutput.writeLong(spaceId);
        objectOutput.writeLong(typeId);
        objectOutput.writeLong(statusId);

        if (code == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(code);
        }

        if (title == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(title);
        }

        if (text == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(text);
        }

        objectOutput.writeLong(date);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
