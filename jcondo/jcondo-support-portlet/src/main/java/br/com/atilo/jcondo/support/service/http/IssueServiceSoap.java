package br.com.atilo.jcondo.support.service.http;

import br.com.atilo.jcondo.support.service.IssueServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.support.service.IssueServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.support.model.IssueSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.support.model.Issue}, that is translated to a
 * {@link br.com.atilo.jcondo.support.model.IssueSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author anderson
 * @see IssueServiceHttp
 * @see br.com.atilo.jcondo.support.model.IssueSoap
 * @see br.com.atilo.jcondo.support.service.IssueServiceUtil
 * @generated
 */
public class IssueServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(IssueServiceSoap.class);

    public static br.com.atilo.jcondo.support.model.IssueSoap addIssue(
        java.lang.String title, java.lang.String text,
        br.com.atilo.jcondo.support.model.datatype.IssueType type,
        br.com.atilo.jcondo.support.model.datatype.IssueSpace space,
        long assigneeId, java.util.List<java.io.File> attachments)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.support.model.Issue returnValue = IssueServiceUtil.addIssue(title,
                    text, type, space, assigneeId, attachments);

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.IssueSoap updateIssue(
        br.com.atilo.jcondo.support.model.IssueSoap issue)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.support.model.Issue returnValue = IssueServiceUtil.updateIssue(br.com.atilo.jcondo.support.model.impl.IssueModelImpl.toModel(
                        issue));

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.IssueSoap updateIssueStatus(
        long issueId,
        br.com.atilo.jcondo.support.model.datatype.IssueStatus status)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.support.model.Issue returnValue = IssueServiceUtil.updateIssueStatus(issueId,
                    status);

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.IssueSoap getIssue(
        java.lang.String code) throws RemoteException {
        try {
            br.com.atilo.jcondo.support.model.Issue returnValue = IssueServiceUtil.getIssue(code);

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.IssueSoap[] getSpaceIssues(
        long spaceId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.support.model.Issue> returnValue = IssueServiceUtil.getSpaceIssues(spaceId);

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.IssueSoap[] getPersonIssues(
        long personId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.support.model.Issue> returnValue = IssueServiceUtil.getPersonIssues(personId);

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.IssueSoap[] getPersonIssuesBySpace(
        long personId, long spaceId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.support.model.Issue> returnValue = IssueServiceUtil.getPersonIssuesBySpace(personId,
                    spaceId);

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.IssueSoap[] getFlatIssues(
        long flatId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.support.model.Issue> returnValue = IssueServiceUtil.getFlatIssues(flatId);

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.IssueSoap[] getPersonIssues(
        java.lang.String name) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.support.model.Issue> returnValue = IssueServiceUtil.getPersonIssues(name);

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.support.model.IssueSoap[] getPersonFlatIssues(
        java.lang.String name, long flatId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.support.model.Issue> returnValue = IssueServiceUtil.getPersonFlatIssues(name,
                    flatId);

            return br.com.atilo.jcondo.support.model.IssueSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
