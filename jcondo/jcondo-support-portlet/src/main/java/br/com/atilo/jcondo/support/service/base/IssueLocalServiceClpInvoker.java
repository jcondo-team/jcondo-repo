package br.com.atilo.jcondo.support.service.base;

import br.com.atilo.jcondo.support.service.IssueLocalServiceUtil;

import java.util.Arrays;

/**
 * @author anderson
 * @generated
 */
public class IssueLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName48;
    private String[] _methodParameterTypes48;
    private String _methodName49;
    private String[] _methodParameterTypes49;
    private String _methodName54;
    private String[] _methodParameterTypes54;
    private String _methodName55;
    private String[] _methodParameterTypes55;
    private String _methodName56;
    private String[] _methodParameterTypes56;
    private String _methodName57;
    private String[] _methodParameterTypes57;
    private String _methodName58;
    private String[] _methodParameterTypes58;
    private String _methodName59;
    private String[] _methodParameterTypes59;
    private String _methodName60;
    private String[] _methodParameterTypes60;
    private String _methodName61;
    private String[] _methodParameterTypes61;
    private String _methodName62;
    private String[] _methodParameterTypes62;
    private String _methodName63;
    private String[] _methodParameterTypes63;
    private String _methodName64;
    private String[] _methodParameterTypes64;

    public IssueLocalServiceClpInvoker() {
        _methodName0 = "addIssue";

        _methodParameterTypes0 = new String[] {
                "br.com.atilo.jcondo.support.model.Issue"
            };

        _methodName1 = "createIssue";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteIssue";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteIssue";

        _methodParameterTypes3 = new String[] {
                "br.com.atilo.jcondo.support.model.Issue"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchIssue";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getIssue";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getIssues";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getIssuesCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateIssue";

        _methodParameterTypes15 = new String[] {
                "br.com.atilo.jcondo.support.model.Issue"
            };

        _methodName48 = "getBeanIdentifier";

        _methodParameterTypes48 = new String[] {  };

        _methodName49 = "setBeanIdentifier";

        _methodParameterTypes49 = new String[] { "java.lang.String" };

        _methodName54 = "addIssue";

        _methodParameterTypes54 = new String[] {
                "java.lang.String", "java.lang.String",
                "br.com.atilo.jcondo.support.model.datatype.IssueType",
                "br.com.atilo.jcondo.support.model.datatype.IssueSpace", "long",
                "java.util.List"
            };

        _methodName55 = "addIssue";

        _methodParameterTypes55 = new String[] {
                "br.com.atilo.jcondo.support.model.Issue"
            };

        _methodName56 = "updateIssueStatus";

        _methodParameterTypes56 = new String[] {
                "long", "br.com.atilo.jcondo.support.model.datatype.IssueStatus"
            };

        _methodName57 = "updateIssue";

        _methodParameterTypes57 = new String[] {
                "br.com.atilo.jcondo.support.model.Issue", "boolean"
            };

        _methodName58 = "getIssue";

        _methodParameterTypes58 = new String[] { "java.lang.String" };

        _methodName59 = "getPersonIssues";

        _methodParameterTypes59 = new String[] { "long" };

        _methodName60 = "getSpaceIssues";

        _methodParameterTypes60 = new String[] { "long" };

        _methodName61 = "getPersonIssuesBySpace";

        _methodParameterTypes61 = new String[] { "long", "long" };

        _methodName62 = "getFlatIssues";

        _methodParameterTypes62 = new String[] { "long" };

        _methodName63 = "getPersonIssues";

        _methodParameterTypes63 = new String[] { "java.lang.String" };

        _methodName64 = "getPersonFlatIssues";

        _methodParameterTypes64 = new String[] { "java.lang.String", "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return IssueLocalServiceUtil.addIssue((br.com.atilo.jcondo.support.model.Issue) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return IssueLocalServiceUtil.createIssue(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return IssueLocalServiceUtil.deleteIssue(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return IssueLocalServiceUtil.deleteIssue((br.com.atilo.jcondo.support.model.Issue) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return IssueLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return IssueLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return IssueLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return IssueLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return IssueLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return IssueLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return IssueLocalServiceUtil.fetchIssue(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return IssueLocalServiceUtil.getIssue(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return IssueLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return IssueLocalServiceUtil.getIssues(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return IssueLocalServiceUtil.getIssuesCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return IssueLocalServiceUtil.updateIssue((br.com.atilo.jcondo.support.model.Issue) arguments[0]);
        }

        if (_methodName48.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes48, parameterTypes)) {
            return IssueLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName49.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes49, parameterTypes)) {
            IssueLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName54.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes54, parameterTypes)) {
            return IssueLocalServiceUtil.addIssue((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                (br.com.atilo.jcondo.support.model.datatype.IssueType) arguments[2],
                (br.com.atilo.jcondo.support.model.datatype.IssueSpace) arguments[3],
                ((Long) arguments[4]).longValue(),
                (java.util.List<java.io.File>) arguments[5]);
        }

        if (_methodName55.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes55, parameterTypes)) {
            return IssueLocalServiceUtil.addIssue((br.com.atilo.jcondo.support.model.Issue) arguments[0]);
        }

        if (_methodName56.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes56, parameterTypes)) {
            return IssueLocalServiceUtil.updateIssueStatus(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.support.model.datatype.IssueStatus) arguments[1]);
        }

        if (_methodName57.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes57, parameterTypes)) {
            return IssueLocalServiceUtil.updateIssue((br.com.atilo.jcondo.support.model.Issue) arguments[0],
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName58.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes58, parameterTypes)) {
            return IssueLocalServiceUtil.getIssue((java.lang.String) arguments[0]);
        }

        if (_methodName59.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes59, parameterTypes)) {
            return IssueLocalServiceUtil.getPersonIssues(((Long) arguments[0]).longValue());
        }

        if (_methodName60.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes60, parameterTypes)) {
            return IssueLocalServiceUtil.getSpaceIssues(((Long) arguments[0]).longValue());
        }

        if (_methodName61.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes61, parameterTypes)) {
            return IssueLocalServiceUtil.getPersonIssuesBySpace(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        if (_methodName62.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes62, parameterTypes)) {
            return IssueLocalServiceUtil.getFlatIssues(((Long) arguments[0]).longValue());
        }

        if (_methodName63.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes63, parameterTypes)) {
            return IssueLocalServiceUtil.getPersonIssues((java.lang.String) arguments[0]);
        }

        if (_methodName64.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes64, parameterTypes)) {
            return IssueLocalServiceUtil.getPersonFlatIssues((java.lang.String) arguments[0],
                ((Long) arguments[1]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
