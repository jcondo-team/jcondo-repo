/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.support.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang.StringUtils;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.support.model.Interaction;
import br.com.atilo.jcondo.support.service.base.InteractionLocalServiceBaseImpl;

/**
 * The implementation of the interaction local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.support.service.InteractionLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.support.service.base.InteractionLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.support.service.InteractionLocalServiceUtil
 */
public class InteractionLocalServiceImpl extends InteractionLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.support.service.InteractionLocalServiceUtil} to access the interaction local service.
	 */

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Interaction addInteraction(Interaction interaction) throws SystemException {
		try {
			issueLocalService.getIssue(interaction.getIssueId());
		
			if (StringUtils.isEmpty(interaction.getText())) {
				throw new BusinessException("exception.interaction.text.empty");
			}
			
			ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
			
			Interaction i = createInteraction(counterLocalService.increment());
			i.setIssueId(interaction.getIssueId());
			i.setPersonId(PersonLocalServiceUtil.getPerson().getId());
			i.setText(interaction.getText());
			i.setOprDate(new Date());
			i.setOprUser(sc.getUserId());

			return super.addInteraction(i);
		} catch (Exception e) {
			throw new SystemException(e);
		}
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Interaction updateInteraction(Interaction interaction, boolean merge) throws SystemException {
		Interaction i;

		try {
			i = getInteraction(interaction.getId());
			BeanUtils.copyProperties(i, interaction);
		} catch (Exception e) {
			throw new SystemException(e);
		}

		if (StringUtils.isEmpty(interaction.getText())) {
			throw new BusinessException("exception.interaction.text.empty");
		}

		if (interaction.getIssueId() != i.getIssueId()) {
			throw new BusinessException("exception.interaction.issue.unchangeable");
		}		

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		i.setOprDate(new Date());
		i.setOprUser(sc.getUserId());
		return super.updateInteraction(i);
	}

	public List<Interaction> getIssueInteractions(long issueId) throws SystemException {
		return new ArrayList<Interaction>(interactionPersistence.findByIssue(issueId));
	}

}