package br.com.atilo.jcondo.support.model.listener;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;

import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.support.model.Interaction;
import br.com.atilo.jcondo.support.model.Issue;
import br.com.atilo.jcondo.support.model.datatype.IssueStatus;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.template.StringTemplateResource;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateManagerUtil;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.util.ContentUtil;

public class IssueListener extends BaseModelListener<Issue> {

	private static final Logger LOGGER = Logger.getLogger(IssueListener.class);

	private static final ResourceBundle CONFIG = ResourceBundle.getBundle("application");

	private static final ResourceBundle RB = ResourceBundle.getBundle("Language");

	private StringTemplateResource issueCreatedResource;

	private StringTemplateResource issueConfirmedResource;

	private StringTemplateResource issueAnsweredResource;

	public IssueListener() {
		issueCreatedResource = new StringTemplateResource("IssueCreated", 
															ContentUtil.get("br/com/atilo/jcondo/support/mail/issue-created-notify.vm"));
		issueConfirmedResource = new StringTemplateResource("IssueConfirmed", 
															ContentUtil.get("br/com/atilo/jcondo/support/mail/issue-created-confirm.vm"));
		issueAnsweredResource = new StringTemplateResource("IssueAnswered", 
															ContentUtil.get("br/com/atilo/jcondo/support/mail/issue-answered-notify.vm"));
	}

	@Override
	public void onAfterCreate(Issue issue) throws ModelListenerException {
		notifyIssueCreate(issue);
		notifyIssueConfirm(issue);
	}
	
	@Override
	public void onAfterUpdate(Issue issue) throws ModelListenerException {
		if (issue.getStatus() == IssueStatus.CLOSE) {
			notifyIssueAnswer(issue);
		}
	}

	private void notifyIssueCreate(Issue issue) {
		try {
			LOGGER.info("Enviando notificacao de criacao da ocorrencia " + issue.getId());

			Template template = TemplateManagerUtil.getTemplate("", issueCreatedResource, false);

			template.put("issueDeadline", CONFIG.getString("issue.answer.deadline"));
			template.put("issueText", issue.getText());
			template.put("issueCode", issue.getCode());
			template.put("issuePerson", issue.getPerson().getFullName());

			Flat flat = (Flat) issue.getPerson().getDomain();

			template.put("flatNumber", flat.getNumber());
			template.put("flatBlock", flat.getBlock());

			UnsyncStringWriter writer = new UnsyncStringWriter();			

			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);


			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(CONFIG.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(CONFIG.getString("syndic.mail")));
			mailMessage.setSubject(CONFIG.getString("issue.create.subject"));
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de criacao da ocorrencia " + issue.getId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de criacao da ocorrencia " + issue.getId());
		}
	}

	private void notifyIssueConfirm(Issue issue) {
		try {
			LOGGER.info("Enviando notificacao de confirmacao da ocorrencia " + issue.getId());

			Template template = TemplateManagerUtil.getTemplate("", issueConfirmedResource, false);

			template.put("issueLabel", RB.getString(issue.getType().getLabel()).toLowerCase());
			template.put("issueCode", issue.getCode());

			UnsyncStringWriter writer = new UnsyncStringWriter();

			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);

			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(CONFIG.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(issue.getPerson().getEmail()));
			mailMessage.setSubject(MessageFormat.format(CONFIG.getString("issue.confirm.subject"), 
					  									RB.getString(issue.getType().getLabel())).toLowerCase());
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de confirmacao da ocorrencia " + issue.getId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de confirmacao da ocorrencia " + issue.getId());
		}
	}

	private void notifyIssueAnswer(Issue issue) {
		try {
			LOGGER.info("Enviando notificacao de resposta da ocorrencia " + issue.getId());

			Interaction interaction = issue.getInteractions().get(0);
			
			Template template = TemplateManagerUtil.getTemplate("", issueAnsweredResource, false);

			template.put("issueLabel", RB.getString(issue.getType().getLabel()).toLowerCase());
			template.put("issueCode", issue.getCode());
			template.put("issueText", issue.getText());
			template.put("issueDate", DateFormatUtils.format(issue.getDate(), "dd/MM/yyyy"));
			template.put("answerText", interaction.getText());
			template.put("answerDate", DateFormatUtils.format(interaction.getOprDate(), "dd/MM/yyyy"));
			
			UnsyncStringWriter writer = new UnsyncStringWriter();

			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);

			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(CONFIG.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(issue.getPerson().getEmail()));
			mailMessage.setSubject(MessageFormat.format(CONFIG.getString("issue.answer.subject"), 
														RB.getString(issue.getType().getLabel()).toLowerCase()));
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de resposta da ocorrencia " + issue.getId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de resposta da ocorrencia " + issue.getId());
		}
	}
}
