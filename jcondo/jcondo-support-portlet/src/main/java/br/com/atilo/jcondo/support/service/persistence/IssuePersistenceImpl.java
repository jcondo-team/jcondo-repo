package br.com.atilo.jcondo.support.service.persistence;

import br.com.atilo.jcondo.support.NoSuchIssueException;
import br.com.atilo.jcondo.support.model.Issue;
import br.com.atilo.jcondo.support.model.impl.IssueImpl;
import br.com.atilo.jcondo.support.model.impl.IssueModelImpl;
import br.com.atilo.jcondo.support.service.persistence.IssuePersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the issue service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author anderson
 * @see IssuePersistence
 * @see IssueUtil
 * @generated
 */
public class IssuePersistenceImpl extends BasePersistenceImpl<Issue>
    implements IssuePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link IssueUtil} to access the issue persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = IssueImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, IssueImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, IssueImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_CODE = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, IssueImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByCode",
            new String[] { String.class.getName() },
            IssueModelImpl.CODE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODE = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCode",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODE_CODE_1 = "issue.code IS NULL";
    private static final String _FINDER_COLUMN_CODE_CODE_2 = "issue.code = ?";
    private static final String _FINDER_COLUMN_CODE_CODE_3 = "(issue.code IS NULL OR issue.code = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, IssueImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPerson",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON =
        new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, IssueImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPerson",
            new String[] { Long.class.getName() },
            IssueModelImpl.PERSONID_COLUMN_BITMASK |
            IssueModelImpl.DATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PERSON = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPerson",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_PERSON_PERSONID_2 = "issue.personId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_SPACE = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, IssueImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findBySpace",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SPACE = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, IssueImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findBySpace",
            new String[] { Long.class.getName() },
            IssueModelImpl.SPACEID_COLUMN_BITMASK |
            IssueModelImpl.DATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_SPACE = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countBySpace",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_SPACE_SPACEID_2 = "issue.spaceId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSONANDSPACE =
        new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, IssueImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPersonAndSpace",
            new String[] {
                Long.class.getName(), Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDSPACE =
        new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, IssueImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPersonAndSpace",
            new String[] { Long.class.getName(), Long.class.getName() },
            IssueModelImpl.PERSONID_COLUMN_BITMASK |
            IssueModelImpl.SPACEID_COLUMN_BITMASK |
            IssueModelImpl.DATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PERSONANDSPACE = new FinderPath(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPersonAndSpace",
            new String[] { Long.class.getName(), Long.class.getName() });
    private static final String _FINDER_COLUMN_PERSONANDSPACE_PERSONID_2 = "issue.personId = ? AND ";
    private static final String _FINDER_COLUMN_PERSONANDSPACE_SPACEID_2 = "issue.spaceId = ?";
    private static final String _SQL_SELECT_ISSUE = "SELECT issue FROM Issue issue";
    private static final String _SQL_SELECT_ISSUE_WHERE = "SELECT issue FROM Issue issue WHERE ";
    private static final String _SQL_COUNT_ISSUE = "SELECT COUNT(issue) FROM Issue issue";
    private static final String _SQL_COUNT_ISSUE_WHERE = "SELECT COUNT(issue) FROM Issue issue WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "issue.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Issue exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Issue exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(IssuePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Issue _nullIssue = new IssueImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Issue> toCacheModel() {
                return _nullIssueCacheModel;
            }
        };

    private static CacheModel<Issue> _nullIssueCacheModel = new CacheModel<Issue>() {
            @Override
            public Issue toEntityModel() {
                return _nullIssue;
            }
        };

    public IssuePersistenceImpl() {
        setModelClass(Issue.class);
    }

    /**
     * Returns the issue where code = &#63; or throws a {@link br.com.atilo.jcondo.support.NoSuchIssueException} if it could not be found.
     *
     * @param code the code
     * @return the matching issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue findByCode(String code)
        throws NoSuchIssueException, SystemException {
        Issue issue = fetchByCode(code);

        if (issue == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("code=");
            msg.append(code);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchIssueException(msg.toString());
        }

        return issue;
    }

    /**
     * Returns the issue where code = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param code the code
     * @return the matching issue, or <code>null</code> if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchByCode(String code) throws SystemException {
        return fetchByCode(code, true);
    }

    /**
     * Returns the issue where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param code the code
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching issue, or <code>null</code> if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchByCode(String code, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { code };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CODE,
                    finderArgs, this);
        }

        if (result instanceof Issue) {
            Issue issue = (Issue) result;

            if (!Validator.equals(code, issue.getCode())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_ISSUE_WHERE);

            boolean bindCode = false;

            if (code == null) {
                query.append(_FINDER_COLUMN_CODE_CODE_1);
            } else if (code.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODE_CODE_3);
            } else {
                bindCode = true;

                query.append(_FINDER_COLUMN_CODE_CODE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCode) {
                    qPos.add(code);
                }

                List<Issue> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "IssuePersistenceImpl.fetchByCode(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Issue issue = list.get(0);

                    result = issue;

                    cacheResult(issue);

                    if ((issue.getCode() == null) ||
                            !issue.getCode().equals(code)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE,
                            finderArgs, issue);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Issue) result;
        }
    }

    /**
     * Removes the issue where code = &#63; from the database.
     *
     * @param code the code
     * @return the issue that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue removeByCode(String code)
        throws NoSuchIssueException, SystemException {
        Issue issue = findByCode(code);

        return remove(issue);
    }

    /**
     * Returns the number of issues where code = &#63;.
     *
     * @param code the code
     * @return the number of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCode(String code) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODE;

        Object[] finderArgs = new Object[] { code };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ISSUE_WHERE);

            boolean bindCode = false;

            if (code == null) {
                query.append(_FINDER_COLUMN_CODE_CODE_1);
            } else if (code.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODE_CODE_3);
            } else {
                bindCode = true;

                query.append(_FINDER_COLUMN_CODE_CODE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCode) {
                    qPos.add(code);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the issues where personId = &#63;.
     *
     * @param personId the person ID
     * @return the matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findByPerson(long personId) throws SystemException {
        return findByPerson(personId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the issues where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of issues
     * @param end the upper bound of the range of issues (not inclusive)
     * @return the range of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findByPerson(long personId, int start, int end)
        throws SystemException {
        return findByPerson(personId, start, end, null);
    }

    /**
     * Returns an ordered range of all the issues where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of issues
     * @param end the upper bound of the range of issues (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findByPerson(long personId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId, start, end, orderByComparator };
        }

        List<Issue> list = (List<Issue>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Issue issue : list) {
                if ((personId != issue.getPersonId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_ISSUE_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(IssueModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                if (!pagination) {
                    list = (List<Issue>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Issue>(list);
                } else {
                    list = (List<Issue>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first issue in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue findByPerson_First(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchIssueException, SystemException {
        Issue issue = fetchByPerson_First(personId, orderByComparator);

        if (issue != null) {
            return issue;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchIssueException(msg.toString());
    }

    /**
     * Returns the first issue in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching issue, or <code>null</code> if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchByPerson_First(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Issue> list = findByPerson(personId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last issue in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue findByPerson_Last(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchIssueException, SystemException {
        Issue issue = fetchByPerson_Last(personId, orderByComparator);

        if (issue != null) {
            return issue;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchIssueException(msg.toString());
    }

    /**
     * Returns the last issue in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching issue, or <code>null</code> if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchByPerson_Last(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByPerson(personId);

        if (count == 0) {
            return null;
        }

        List<Issue> list = findByPerson(personId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the issues before and after the current issue in the ordered set where personId = &#63;.
     *
     * @param id the primary key of the current issue
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue[] findByPerson_PrevAndNext(long id, long personId,
        OrderByComparator orderByComparator)
        throws NoSuchIssueException, SystemException {
        Issue issue = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Issue[] array = new IssueImpl[3];

            array[0] = getByPerson_PrevAndNext(session, issue, personId,
                    orderByComparator, true);

            array[1] = issue;

            array[2] = getByPerson_PrevAndNext(session, issue, personId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Issue getByPerson_PrevAndNext(Session session, Issue issue,
        long personId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ISSUE_WHERE);

        query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(IssueModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(personId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(issue);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Issue> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the issues where personId = &#63; from the database.
     *
     * @param personId the person ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByPerson(long personId) throws SystemException {
        for (Issue issue : findByPerson(personId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(issue);
        }
    }

    /**
     * Returns the number of issues where personId = &#63;.
     *
     * @param personId the person ID
     * @return the number of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByPerson(long personId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PERSON;

        Object[] finderArgs = new Object[] { personId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ISSUE_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the issues where spaceId = &#63;.
     *
     * @param spaceId the space ID
     * @return the matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findBySpace(long spaceId) throws SystemException {
        return findBySpace(spaceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the issues where spaceId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param spaceId the space ID
     * @param start the lower bound of the range of issues
     * @param end the upper bound of the range of issues (not inclusive)
     * @return the range of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findBySpace(long spaceId, int start, int end)
        throws SystemException {
        return findBySpace(spaceId, start, end, null);
    }

    /**
     * Returns an ordered range of all the issues where spaceId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param spaceId the space ID
     * @param start the lower bound of the range of issues
     * @param end the upper bound of the range of issues (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findBySpace(long spaceId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SPACE;
            finderArgs = new Object[] { spaceId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_SPACE;
            finderArgs = new Object[] { spaceId, start, end, orderByComparator };
        }

        List<Issue> list = (List<Issue>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Issue issue : list) {
                if ((spaceId != issue.getSpaceId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_ISSUE_WHERE);

            query.append(_FINDER_COLUMN_SPACE_SPACEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(IssueModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(spaceId);

                if (!pagination) {
                    list = (List<Issue>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Issue>(list);
                } else {
                    list = (List<Issue>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first issue in the ordered set where spaceId = &#63;.
     *
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue findBySpace_First(long spaceId,
        OrderByComparator orderByComparator)
        throws NoSuchIssueException, SystemException {
        Issue issue = fetchBySpace_First(spaceId, orderByComparator);

        if (issue != null) {
            return issue;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("spaceId=");
        msg.append(spaceId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchIssueException(msg.toString());
    }

    /**
     * Returns the first issue in the ordered set where spaceId = &#63;.
     *
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching issue, or <code>null</code> if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchBySpace_First(long spaceId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Issue> list = findBySpace(spaceId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last issue in the ordered set where spaceId = &#63;.
     *
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue findBySpace_Last(long spaceId,
        OrderByComparator orderByComparator)
        throws NoSuchIssueException, SystemException {
        Issue issue = fetchBySpace_Last(spaceId, orderByComparator);

        if (issue != null) {
            return issue;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("spaceId=");
        msg.append(spaceId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchIssueException(msg.toString());
    }

    /**
     * Returns the last issue in the ordered set where spaceId = &#63;.
     *
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching issue, or <code>null</code> if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchBySpace_Last(long spaceId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countBySpace(spaceId);

        if (count == 0) {
            return null;
        }

        List<Issue> list = findBySpace(spaceId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the issues before and after the current issue in the ordered set where spaceId = &#63;.
     *
     * @param id the primary key of the current issue
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue[] findBySpace_PrevAndNext(long id, long spaceId,
        OrderByComparator orderByComparator)
        throws NoSuchIssueException, SystemException {
        Issue issue = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Issue[] array = new IssueImpl[3];

            array[0] = getBySpace_PrevAndNext(session, issue, spaceId,
                    orderByComparator, true);

            array[1] = issue;

            array[2] = getBySpace_PrevAndNext(session, issue, spaceId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Issue getBySpace_PrevAndNext(Session session, Issue issue,
        long spaceId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ISSUE_WHERE);

        query.append(_FINDER_COLUMN_SPACE_SPACEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(IssueModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(spaceId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(issue);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Issue> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the issues where spaceId = &#63; from the database.
     *
     * @param spaceId the space ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeBySpace(long spaceId) throws SystemException {
        for (Issue issue : findBySpace(spaceId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(issue);
        }
    }

    /**
     * Returns the number of issues where spaceId = &#63;.
     *
     * @param spaceId the space ID
     * @return the number of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countBySpace(long spaceId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_SPACE;

        Object[] finderArgs = new Object[] { spaceId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ISSUE_WHERE);

            query.append(_FINDER_COLUMN_SPACE_SPACEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(spaceId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the issues where personId = &#63; and spaceId = &#63;.
     *
     * @param personId the person ID
     * @param spaceId the space ID
     * @return the matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findByPersonAndSpace(long personId, long spaceId)
        throws SystemException {
        return findByPersonAndSpace(personId, spaceId, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the issues where personId = &#63; and spaceId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param spaceId the space ID
     * @param start the lower bound of the range of issues
     * @param end the upper bound of the range of issues (not inclusive)
     * @return the range of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findByPersonAndSpace(long personId, long spaceId,
        int start, int end) throws SystemException {
        return findByPersonAndSpace(personId, spaceId, start, end, null);
    }

    /**
     * Returns an ordered range of all the issues where personId = &#63; and spaceId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param spaceId the space ID
     * @param start the lower bound of the range of issues
     * @param end the upper bound of the range of issues (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findByPersonAndSpace(long personId, long spaceId,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDSPACE;
            finderArgs = new Object[] { personId, spaceId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSONANDSPACE;
            finderArgs = new Object[] {
                    personId, spaceId,
                    
                    start, end, orderByComparator
                };
        }

        List<Issue> list = (List<Issue>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Issue issue : list) {
                if ((personId != issue.getPersonId()) ||
                        (spaceId != issue.getSpaceId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_ISSUE_WHERE);

            query.append(_FINDER_COLUMN_PERSONANDSPACE_PERSONID_2);

            query.append(_FINDER_COLUMN_PERSONANDSPACE_SPACEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(IssueModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                qPos.add(spaceId);

                if (!pagination) {
                    list = (List<Issue>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Issue>(list);
                } else {
                    list = (List<Issue>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first issue in the ordered set where personId = &#63; and spaceId = &#63;.
     *
     * @param personId the person ID
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue findByPersonAndSpace_First(long personId, long spaceId,
        OrderByComparator orderByComparator)
        throws NoSuchIssueException, SystemException {
        Issue issue = fetchByPersonAndSpace_First(personId, spaceId,
                orderByComparator);

        if (issue != null) {
            return issue;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(", spaceId=");
        msg.append(spaceId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchIssueException(msg.toString());
    }

    /**
     * Returns the first issue in the ordered set where personId = &#63; and spaceId = &#63;.
     *
     * @param personId the person ID
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching issue, or <code>null</code> if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchByPersonAndSpace_First(long personId, long spaceId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Issue> list = findByPersonAndSpace(personId, spaceId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last issue in the ordered set where personId = &#63; and spaceId = &#63;.
     *
     * @param personId the person ID
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue findByPersonAndSpace_Last(long personId, long spaceId,
        OrderByComparator orderByComparator)
        throws NoSuchIssueException, SystemException {
        Issue issue = fetchByPersonAndSpace_Last(personId, spaceId,
                orderByComparator);

        if (issue != null) {
            return issue;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(", spaceId=");
        msg.append(spaceId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchIssueException(msg.toString());
    }

    /**
     * Returns the last issue in the ordered set where personId = &#63; and spaceId = &#63;.
     *
     * @param personId the person ID
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching issue, or <code>null</code> if a matching issue could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchByPersonAndSpace_Last(long personId, long spaceId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByPersonAndSpace(personId, spaceId);

        if (count == 0) {
            return null;
        }

        List<Issue> list = findByPersonAndSpace(personId, spaceId, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the issues before and after the current issue in the ordered set where personId = &#63; and spaceId = &#63;.
     *
     * @param id the primary key of the current issue
     * @param personId the person ID
     * @param spaceId the space ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue[] findByPersonAndSpace_PrevAndNext(long id, long personId,
        long spaceId, OrderByComparator orderByComparator)
        throws NoSuchIssueException, SystemException {
        Issue issue = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Issue[] array = new IssueImpl[3];

            array[0] = getByPersonAndSpace_PrevAndNext(session, issue,
                    personId, spaceId, orderByComparator, true);

            array[1] = issue;

            array[2] = getByPersonAndSpace_PrevAndNext(session, issue,
                    personId, spaceId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Issue getByPersonAndSpace_PrevAndNext(Session session,
        Issue issue, long personId, long spaceId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ISSUE_WHERE);

        query.append(_FINDER_COLUMN_PERSONANDSPACE_PERSONID_2);

        query.append(_FINDER_COLUMN_PERSONANDSPACE_SPACEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(IssueModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(personId);

        qPos.add(spaceId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(issue);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Issue> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the issues where personId = &#63; and spaceId = &#63; from the database.
     *
     * @param personId the person ID
     * @param spaceId the space ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByPersonAndSpace(long personId, long spaceId)
        throws SystemException {
        for (Issue issue : findByPersonAndSpace(personId, spaceId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(issue);
        }
    }

    /**
     * Returns the number of issues where personId = &#63; and spaceId = &#63;.
     *
     * @param personId the person ID
     * @param spaceId the space ID
     * @return the number of matching issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByPersonAndSpace(long personId, long spaceId)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PERSONANDSPACE;

        Object[] finderArgs = new Object[] { personId, spaceId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_ISSUE_WHERE);

            query.append(_FINDER_COLUMN_PERSONANDSPACE_PERSONID_2);

            query.append(_FINDER_COLUMN_PERSONANDSPACE_SPACEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                qPos.add(spaceId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the issue in the entity cache if it is enabled.
     *
     * @param issue the issue
     */
    @Override
    public void cacheResult(Issue issue) {
        EntityCacheUtil.putResult(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueImpl.class, issue.getPrimaryKey(), issue);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE,
            new Object[] { issue.getCode() }, issue);

        issue.resetOriginalValues();
    }

    /**
     * Caches the issues in the entity cache if it is enabled.
     *
     * @param issues the issues
     */
    @Override
    public void cacheResult(List<Issue> issues) {
        for (Issue issue : issues) {
            if (EntityCacheUtil.getResult(IssueModelImpl.ENTITY_CACHE_ENABLED,
                        IssueImpl.class, issue.getPrimaryKey()) == null) {
                cacheResult(issue);
            } else {
                issue.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all issues.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(IssueImpl.class.getName());
        }

        EntityCacheUtil.clearCache(IssueImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the issue.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Issue issue) {
        EntityCacheUtil.removeResult(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueImpl.class, issue.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(issue);
    }

    @Override
    public void clearCache(List<Issue> issues) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Issue issue : issues) {
            EntityCacheUtil.removeResult(IssueModelImpl.ENTITY_CACHE_ENABLED,
                IssueImpl.class, issue.getPrimaryKey());

            clearUniqueFindersCache(issue);
        }
    }

    protected void cacheUniqueFindersCache(Issue issue) {
        if (issue.isNew()) {
            Object[] args = new Object[] { issue.getCode() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODE, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE, args, issue);
        } else {
            IssueModelImpl issueModelImpl = (IssueModelImpl) issue;

            if ((issueModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_CODE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { issue.getCode() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODE, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE, args, issue);
            }
        }
    }

    protected void clearUniqueFindersCache(Issue issue) {
        IssueModelImpl issueModelImpl = (IssueModelImpl) issue;

        Object[] args = new Object[] { issue.getCode() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODE, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODE, args);

        if ((issueModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_CODE.getColumnBitmask()) != 0) {
            args = new Object[] { issueModelImpl.getOriginalCode() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODE, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODE, args);
        }
    }

    /**
     * Creates a new issue with the primary key. Does not add the issue to the database.
     *
     * @param id the primary key for the new issue
     * @return the new issue
     */
    @Override
    public Issue create(long id) {
        Issue issue = new IssueImpl();

        issue.setNew(true);
        issue.setPrimaryKey(id);

        return issue;
    }

    /**
     * Removes the issue with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the issue
     * @return the issue that was removed
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue remove(long id) throws NoSuchIssueException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the issue with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the issue
     * @return the issue that was removed
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue remove(Serializable primaryKey)
        throws NoSuchIssueException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Issue issue = (Issue) session.get(IssueImpl.class, primaryKey);

            if (issue == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchIssueException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(issue);
        } catch (NoSuchIssueException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Issue removeImpl(Issue issue) throws SystemException {
        issue = toUnwrappedModel(issue);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(issue)) {
                issue = (Issue) session.get(IssueImpl.class,
                        issue.getPrimaryKeyObj());
            }

            if (issue != null) {
                session.delete(issue);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (issue != null) {
            clearCache(issue);
        }

        return issue;
    }

    @Override
    public Issue updateImpl(br.com.atilo.jcondo.support.model.Issue issue)
        throws SystemException {
        issue = toUnwrappedModel(issue);

        boolean isNew = issue.isNew();

        IssueModelImpl issueModelImpl = (IssueModelImpl) issue;

        Session session = null;

        try {
            session = openSession();

            if (issue.isNew()) {
                session.save(issue);

                issue.setNew(false);
            } else {
                session.merge(issue);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !IssueModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((issueModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        issueModelImpl.getOriginalPersonId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);

                args = new Object[] { issueModelImpl.getPersonId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);
            }

            if ((issueModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SPACE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { issueModelImpl.getOriginalSpaceId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SPACE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SPACE,
                    args);

                args = new Object[] { issueModelImpl.getSpaceId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_SPACE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_SPACE,
                    args);
            }

            if ((issueModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDSPACE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        issueModelImpl.getOriginalPersonId(),
                        issueModelImpl.getOriginalSpaceId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSONANDSPACE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDSPACE,
                    args);

                args = new Object[] {
                        issueModelImpl.getPersonId(),
                        issueModelImpl.getSpaceId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSONANDSPACE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSONANDSPACE,
                    args);
            }
        }

        EntityCacheUtil.putResult(IssueModelImpl.ENTITY_CACHE_ENABLED,
            IssueImpl.class, issue.getPrimaryKey(), issue);

        clearUniqueFindersCache(issue);
        cacheUniqueFindersCache(issue);

        return issue;
    }

    protected Issue toUnwrappedModel(Issue issue) {
        if (issue instanceof IssueImpl) {
            return issue;
        }

        IssueImpl issueImpl = new IssueImpl();

        issueImpl.setNew(issue.isNew());
        issueImpl.setPrimaryKey(issue.getPrimaryKey());

        issueImpl.setId(issue.getId());
        issueImpl.setPersonId(issue.getPersonId());
        issueImpl.setAssigneeId(issue.getAssigneeId());
        issueImpl.setSpaceId(issue.getSpaceId());
        issueImpl.setTypeId(issue.getTypeId());
        issueImpl.setStatusId(issue.getStatusId());
        issueImpl.setCode(issue.getCode());
        issueImpl.setTitle(issue.getTitle());
        issueImpl.setText(issue.getText());
        issueImpl.setDate(issue.getDate());
        issueImpl.setOprDate(issue.getOprDate());
        issueImpl.setOprUser(issue.getOprUser());

        return issueImpl;
    }

    /**
     * Returns the issue with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the issue
     * @return the issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue findByPrimaryKey(Serializable primaryKey)
        throws NoSuchIssueException, SystemException {
        Issue issue = fetchByPrimaryKey(primaryKey);

        if (issue == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchIssueException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return issue;
    }

    /**
     * Returns the issue with the primary key or throws a {@link br.com.atilo.jcondo.support.NoSuchIssueException} if it could not be found.
     *
     * @param id the primary key of the issue
     * @return the issue
     * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue findByPrimaryKey(long id)
        throws NoSuchIssueException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the issue with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the issue
     * @return the issue, or <code>null</code> if a issue with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Issue issue = (Issue) EntityCacheUtil.getResult(IssueModelImpl.ENTITY_CACHE_ENABLED,
                IssueImpl.class, primaryKey);

        if (issue == _nullIssue) {
            return null;
        }

        if (issue == null) {
            Session session = null;

            try {
                session = openSession();

                issue = (Issue) session.get(IssueImpl.class, primaryKey);

                if (issue != null) {
                    cacheResult(issue);
                } else {
                    EntityCacheUtil.putResult(IssueModelImpl.ENTITY_CACHE_ENABLED,
                        IssueImpl.class, primaryKey, _nullIssue);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(IssueModelImpl.ENTITY_CACHE_ENABLED,
                    IssueImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return issue;
    }

    /**
     * Returns the issue with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the issue
     * @return the issue, or <code>null</code> if a issue with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Issue fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the issues.
     *
     * @return the issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the issues.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of issues
     * @param end the upper bound of the range of issues (not inclusive)
     * @return the range of issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the issues.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of issues
     * @param end the upper bound of the range of issues (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Issue> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Issue> list = (List<Issue>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ISSUE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ISSUE;

                if (pagination) {
                    sql = sql.concat(IssueModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Issue>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Issue>(list);
                } else {
                    list = (List<Issue>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the issues from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Issue issue : findAll()) {
            remove(issue);
        }
    }

    /**
     * Returns the number of issues.
     *
     * @return the number of issues
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ISSUE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the issue persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.support.model.Issue")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Issue>> listenersList = new ArrayList<ModelListener<Issue>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Issue>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(IssueImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
