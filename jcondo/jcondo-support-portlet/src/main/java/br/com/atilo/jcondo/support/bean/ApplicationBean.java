package br.com.atilo.jcondo.support.bean;

import java.io.Serializable;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.apache.log4j.Logger;

import br.com.atilo.jcondo.support.model.listener.IssueListener;
import br.com.atilo.jcondo.support.service.persistence.IssueUtil;

@ManagedBean
@ApplicationScoped
public class ApplicationBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(ApplicationBean.class);

	public static final ResourceBundle CONFIG = ResourceBundle.getBundle("application");	

	@PostConstruct
	public void init() {
		try {
			IssueUtil.getPersistence().registerListener(new IssueListener());
		} catch (Exception e) {
			LOGGER.error("Failure on application initialization", e);
		}
	}
}
