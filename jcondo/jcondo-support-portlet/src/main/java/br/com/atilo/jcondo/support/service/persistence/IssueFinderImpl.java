package br.com.atilo.jcondo.support.service.persistence;

import java.util.List;

import br.com.atilo.jcondo.support.model.Issue;
import br.com.atilo.jcondo.support.model.impl.IssueImpl;

import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class IssueFinderImpl extends BasePersistenceImpl<Issue> implements IssueFinder {

	public static final String FIND_FLAT_ISSUES = IssueFinder.class.getName() + ".findFlatIssues";	

	public static final String FIND_PERSON_ISSUES = IssueFinder.class.getName() + ".findPersonIssues";	

	public static final String FIND_PERSON_FLAT_ISSUES = IssueFinder.class.getName() + ".findPersonFlatIssues";	

	@SuppressWarnings("unchecked")
	private List<Issue> findIssues(String id, Object ... params) {
		Session session = null;

		try {
			session = openSession();

			String sql = CustomSQLUtil.get(id);

			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("jco_issue", IssueImpl.class);

	        QueryPos qPos = QueryPos.getInstance(q);  

			for (Object param : params) {
		        qPos.add(param);
			}

			return (List<Issue>) QueryUtil.list(q, getDialect(), -1, -1);
		} catch (Exception e) {
			try {
				throw new SystemException(e);
			} catch (SystemException se) {
				se.printStackTrace();
			}
		} finally {
			closeSession(session);
		}

		return null;
	}

	public List<Issue> findFlatIssues(long flatId) {
		return findIssues(FIND_FLAT_ISSUES, flatId);
	}	

	public List<Issue> findPersonIssues(String name) {
		return findIssues(FIND_PERSON_ISSUES, "%" + name.toLowerCase() + "%");
	}	

	public List<Issue> findPersonFlatIssues(String name, long flatId) {
		return findIssues(FIND_PERSON_FLAT_ISSUES, "%" + name.toLowerCase() + "%", flatId);
	}	

}