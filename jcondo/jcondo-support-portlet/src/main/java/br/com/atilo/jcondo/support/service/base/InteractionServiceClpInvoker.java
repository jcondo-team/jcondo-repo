package br.com.atilo.jcondo.support.service.base;

import br.com.atilo.jcondo.support.service.InteractionServiceUtil;

import java.util.Arrays;

/**
 * @author anderson
 * @generated
 */
public class InteractionServiceClpInvoker {
    private String _methodName32;
    private String[] _methodParameterTypes32;
    private String _methodName33;
    private String[] _methodParameterTypes33;
    private String _methodName38;
    private String[] _methodParameterTypes38;
    private String _methodName39;
    private String[] _methodParameterTypes39;
    private String _methodName40;
    private String[] _methodParameterTypes40;
    private String _methodName41;
    private String[] _methodParameterTypes41;

    public InteractionServiceClpInvoker() {
        _methodName32 = "getBeanIdentifier";

        _methodParameterTypes32 = new String[] {  };

        _methodName33 = "setBeanIdentifier";

        _methodParameterTypes33 = new String[] { "java.lang.String" };

        _methodName38 = "createInteraction";

        _methodParameterTypes38 = new String[] {  };

        _methodName39 = "addInteraction";

        _methodParameterTypes39 = new String[] {
                "br.com.atilo.jcondo.support.model.Interaction"
            };

        _methodName40 = "updateInteraction";

        _methodParameterTypes40 = new String[] {
                "br.com.atilo.jcondo.support.model.Interaction"
            };

        _methodName41 = "getInteraction";

        _methodParameterTypes41 = new String[] { "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName32.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes32, parameterTypes)) {
            return InteractionServiceUtil.getBeanIdentifier();
        }

        if (_methodName33.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes33, parameterTypes)) {
            InteractionServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName38.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes38, parameterTypes)) {
            return InteractionServiceUtil.createInteraction();
        }

        if (_methodName39.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes39, parameterTypes)) {
            return InteractionServiceUtil.addInteraction((br.com.atilo.jcondo.support.model.Interaction) arguments[0]);
        }

        if (_methodName40.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes40, parameterTypes)) {
            return InteractionServiceUtil.updateInteraction((br.com.atilo.jcondo.support.model.Interaction) arguments[0]);
        }

        if (_methodName41.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes41, parameterTypes)) {
            return InteractionServiceUtil.getInteraction(((Long) arguments[0]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
