create index IX_A8612797 on jco_issue (code);
create index IX_6820343A on jco_issue (personId);
create index IX_C18CC0B7 on jco_issue (personId, spaceId);
create index IX_8F5F32FB on jco_issue (spaceId);

create index IX_BED7C31B on jco_issue_interaction (issueId);