create table jco_issue (
	issueId LONG not null primary key,
	personId LONG,
	assigneeId LONG,
	spaceId LONG,
	typeId LONG,
	statusId LONG,
	code VARCHAR(75) null,
	title VARCHAR(75) null,
	text VARCHAR(75) null,
	date DATE null,
	oprDate DATE null,
	oprUser LONG
);

create table jco_issue_attachment (
	attachmentId LONG not null primary key,
	issueId LONG,
	oprDate DATE null,
	oprUser LONG
);

create table jco_issue_interaction (
	interactionId LONG not null primary key,
	issueId LONG,
	personId LONG,
	text VARCHAR(75) null,
	oprDate DATE null,
	oprUser LONG
);