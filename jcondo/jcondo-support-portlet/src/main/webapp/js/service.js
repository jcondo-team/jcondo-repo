Liferay.Service.register("Liferay.Service.jco", "br.com.atilo.jcondo.support.service", "jcondo-support-portlet");

Liferay.Service.registerClass(
	Liferay.Service.jco, "Issue",
	{
		addIssue: true,
		getPersonIssues: true
	}
);

Liferay.Service.register("Liferay.Service.jcosup", "br.com.atilo.jcondo.support.service", "jcondo-support-portlet");

Liferay.Service.registerClass(
	Liferay.Service.jcosup, "Interaction",
	{
		createInteraction: true,
		addInteraction: true,
		updateInteraction: true,
		getInteraction: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.jcosup, "Issue",
	{
		addIssue: true,
		updateIssue: true,
		updateIssueStatus: true,
		getIssue: true,
		getSpaceIssues: true,
		getPersonIssues: true,
		getPersonIssuesBySpace: true,
		getFlatIssues: true,
		getPersonFlatIssues: true
	}
);