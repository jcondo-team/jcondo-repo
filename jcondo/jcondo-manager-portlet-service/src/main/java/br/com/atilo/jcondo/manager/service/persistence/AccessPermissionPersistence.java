package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.AccessPermission;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the access permission service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AccessPermissionPersistenceImpl
 * @see AccessPermissionUtil
 * @generated
 */
public interface AccessPermissionPersistence extends BasePersistence<AccessPermission> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link AccessPermissionUtil} to access the access permission persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the access permissions where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @return the matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findByPersonAndDomain(
        long personId, long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the access permissions where personId = &#63; and domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param start the lower bound of the range of access permissions
    * @param end the upper bound of the range of access permissions (not inclusive)
    * @return the range of matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findByPersonAndDomain(
        long personId, long domainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the access permissions where personId = &#63; and domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param start the lower bound of the range of access permissions
    * @param end the upper bound of the range of access permissions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findByPersonAndDomain(
        long personId, long domainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission findByPersonAndDomain_First(
        long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching access permission, or <code>null</code> if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission fetchByPersonAndDomain_First(
        long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission findByPersonAndDomain_Last(
        long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching access permission, or <code>null</code> if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission fetchByPersonAndDomain_Last(
        long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access permissions before and after the current access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param id the primary key of the current access permission
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission[] findByPersonAndDomain_PrevAndNext(
        long id, long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the access permissions where personId = &#63; and domainId = &#63; from the database.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByPersonAndDomain(long personId, long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of access permissions where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @return the number of matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public int countByPersonAndDomain(long personId, long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessPermissionException} if it could not be found.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @return the matching access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission findByPersonDomainAndWeekDay(
        long personId, long domainId, int weekDay)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @return the matching access permission, or <code>null</code> if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission fetchByPersonDomainAndWeekDay(
        long personId, long domainId, int weekDay)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching access permission, or <code>null</code> if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission fetchByPersonDomainAndWeekDay(
        long personId, long domainId, int weekDay, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; from the database.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @return the access permission that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission removeByPersonDomainAndWeekDay(
        long personId, long domainId, int weekDay)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of access permissions where personId = &#63; and domainId = &#63; and weekDay = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @return the number of matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public int countByPersonDomainAndWeekDay(long personId, long domainId,
        int weekDay) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the access permission in the entity cache if it is enabled.
    *
    * @param accessPermission the access permission
    */
    public void cacheResult(
        br.com.atilo.jcondo.manager.model.AccessPermission accessPermission);

    /**
    * Caches the access permissions in the entity cache if it is enabled.
    *
    * @param accessPermissions the access permissions
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> accessPermissions);

    /**
    * Creates a new access permission with the primary key. Does not add the access permission to the database.
    *
    * @param id the primary key for the new access permission
    * @return the new access permission
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission create(long id);

    /**
    * Removes the access permission with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the access permission
    * @return the access permission that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.AccessPermission updateImpl(
        br.com.atilo.jcondo.manager.model.AccessPermission accessPermission)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access permission with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessPermissionException} if it could not be found.
    *
    * @param id the primary key of the access permission
    * @return the access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access permission with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the access permission
    * @return the access permission, or <code>null</code> if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.AccessPermission fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the access permissions.
    *
    * @return the access permissions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the access permissions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of access permissions
    * @param end the upper bound of the range of access permissions (not inclusive)
    * @return the range of access permissions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the access permissions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of access permissions
    * @param end the upper bound of the range of access permissions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of access permissions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the access permissions from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of access permissions.
    *
    * @return the number of access permissions
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
