package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Enterprise service. Represents a row in the &quot;jco_enterprise&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see EnterpriseModel
 * @see br.com.atilo.jcondo.manager.model.impl.EnterpriseImpl
 * @see br.com.atilo.jcondo.manager.model.impl.EnterpriseModelImpl
 * @generated
 */
public interface Enterprise extends EnterpriseModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.EnterpriseImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public java.lang.String getName();

    public void setName(java.lang.String name);

    public br.com.atilo.jcondo.datatype.EnterpriseStatus getStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setStatus(br.com.atilo.jcondo.datatype.EnterpriseStatus status);

    public br.com.atilo.jcondo.datatype.DomainType getType()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setType(br.com.atilo.jcondo.datatype.DomainType type);

    public java.lang.String getDescription()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setDescription(java.lang.String description);

    public com.liferay.portal.model.Address getAddress()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getPeople()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.model.Organization getOrganization();

    public void setOrganization(
        com.liferay.portal.model.Organization organization);
}
