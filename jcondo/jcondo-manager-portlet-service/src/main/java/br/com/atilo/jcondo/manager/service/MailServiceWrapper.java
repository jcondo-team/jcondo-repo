package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MailService}.
 *
 * @author Brian Wing Shun Chan
 * @see MailService
 * @generated
 */
public class MailServiceWrapper implements MailService,
    ServiceWrapper<MailService> {
    private MailService _mailService;

    public MailServiceWrapper(MailService mailService) {
        _mailService = mailService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _mailService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _mailService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _mailService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public MailService getWrappedMailService() {
        return _mailService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedMailService(MailService mailService) {
        _mailService = mailService;
    }

    @Override
    public MailService getWrappedService() {
        return _mailService;
    }

    @Override
    public void setWrappedService(MailService mailService) {
        _mailService = mailService;
    }
}
