package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Flat. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.FlatServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see FlatService
 * @see br.com.atilo.jcondo.manager.service.base.FlatServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.FlatServiceImpl
 * @generated
 */
public class FlatServiceUtil {
    private static FlatService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.FlatServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Flat addFlat(int number,
        int block, boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addFlat(number, block, disables, brigade, pets, petTypes);
    }

    public static br.com.atilo.jcondo.manager.model.Flat updateFlat(long id,
        boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateFlat(id, disables, brigade, pets, petTypes);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Flat> getPersonFlats(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonFlats(personId);
    }

    public static br.com.atilo.jcondo.manager.model.Flat getFlat(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getFlat(id);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Flat> getFlats()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getFlats();
    }

    public static void setFlatDefaulting(long flatId, boolean isDefaulting)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService().setFlatDefaulting(flatId, isDefaulting);
    }

    public static br.com.atilo.jcondo.manager.model.Flat setFlatPerson(
        long flatId, long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().setFlatPerson(flatId, personId);
    }

    public static void clearService() {
        _service = null;
    }

    public static FlatService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    FlatService.class.getName());

            if (invokableService instanceof FlatService) {
                _service = (FlatService) invokableService;
            } else {
                _service = new FlatServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(FlatServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(FlatService service) {
    }
}
