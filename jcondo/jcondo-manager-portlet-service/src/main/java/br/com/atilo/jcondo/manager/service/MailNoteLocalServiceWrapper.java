package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MailNoteLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see MailNoteLocalService
 * @generated
 */
public class MailNoteLocalServiceWrapper implements MailNoteLocalService,
    ServiceWrapper<MailNoteLocalService> {
    private MailNoteLocalService _mailNoteLocalService;

    public MailNoteLocalServiceWrapper(
        MailNoteLocalService mailNoteLocalService) {
        _mailNoteLocalService = mailNoteLocalService;
    }

    /**
    * Adds the mail note to the database. Also notifies the appropriate model listeners.
    *
    * @param mailNote the mail note
    * @return the mail note that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.MailNote addMailNote(
        br.com.atilo.jcondo.manager.model.MailNote mailNote)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.addMailNote(mailNote);
    }

    /**
    * Creates a new mail note with the primary key. Does not add the mail note to the database.
    *
    * @param id the primary key for the new mail note
    * @return the new mail note
    */
    @Override
    public br.com.atilo.jcondo.manager.model.MailNote createMailNote(long id) {
        return _mailNoteLocalService.createMailNote(id);
    }

    /**
    * Deletes the mail note with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the mail note
    * @return the mail note that was removed
    * @throws PortalException if a mail note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.MailNote deleteMailNote(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.deleteMailNote(id);
    }

    /**
    * Deletes the mail note from the database. Also notifies the appropriate model listeners.
    *
    * @param mailNote the mail note
    * @return the mail note that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.MailNote deleteMailNote(
        br.com.atilo.jcondo.manager.model.MailNote mailNote)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.deleteMailNote(mailNote);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _mailNoteLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MailNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MailNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.MailNote fetchMailNote(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.fetchMailNote(id);
    }

    /**
    * Returns the mail note with the primary key.
    *
    * @param id the primary key of the mail note
    * @return the mail note
    * @throws PortalException if a mail note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.MailNote getMailNote(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.getMailNote(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the mail notes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MailNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of mail notes
    * @param end the upper bound of the range of mail notes (not inclusive)
    * @return the range of mail notes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.MailNote> getMailNotes(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.getMailNotes(start, end);
    }

    /**
    * Returns the number of mail notes.
    *
    * @return the number of mail notes
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getMailNotesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.getMailNotesCount();
    }

    /**
    * Updates the mail note in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param mailNote the mail note
    * @return the mail note that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.MailNote updateMailNote(
        br.com.atilo.jcondo.manager.model.MailNote mailNote)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _mailNoteLocalService.updateMailNote(mailNote);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _mailNoteLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _mailNoteLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _mailNoteLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public MailNoteLocalService getWrappedMailNoteLocalService() {
        return _mailNoteLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedMailNoteLocalService(
        MailNoteLocalService mailNoteLocalService) {
        _mailNoteLocalService = mailNoteLocalService;
    }

    @Override
    public MailNoteLocalService getWrappedService() {
        return _mailNoteLocalService;
    }

    @Override
    public void setWrappedService(MailNoteLocalService mailNoteLocalService) {
        _mailNoteLocalService = mailNoteLocalService;
    }
}
