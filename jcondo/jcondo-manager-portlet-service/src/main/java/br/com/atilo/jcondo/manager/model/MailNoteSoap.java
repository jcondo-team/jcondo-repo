package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.MailNoteServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.MailNoteServiceSoap
 * @generated
 */
public class MailNoteSoap implements Serializable {
    private long _id;
    private long _mailId;
    private String _description;
    private Date _oprDate;
    private long _oprUser;

    public MailNoteSoap() {
    }

    public static MailNoteSoap toSoapModel(MailNote model) {
        MailNoteSoap soapModel = new MailNoteSoap();

        soapModel.setId(model.getId());
        soapModel.setMailId(model.getMailId());
        soapModel.setDescription(model.getDescription());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static MailNoteSoap[] toSoapModels(MailNote[] models) {
        MailNoteSoap[] soapModels = new MailNoteSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static MailNoteSoap[][] toSoapModels(MailNote[][] models) {
        MailNoteSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new MailNoteSoap[models.length][models[0].length];
        } else {
            soapModels = new MailNoteSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static MailNoteSoap[] toSoapModels(List<MailNote> models) {
        List<MailNoteSoap> soapModels = new ArrayList<MailNoteSoap>(models.size());

        for (MailNote model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new MailNoteSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getMailId() {
        return _mailId;
    }

    public void setMailId(long mailId) {
        _mailId = mailId;
    }

    public String getDescription() {
        return _description;
    }

    public void setDescription(String description) {
        _description = description;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
