package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ParkingLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see ParkingLocalService
 * @generated
 */
public class ParkingLocalServiceWrapper implements ParkingLocalService,
    ServiceWrapper<ParkingLocalService> {
    private ParkingLocalService _parkingLocalService;

    public ParkingLocalServiceWrapper(ParkingLocalService parkingLocalService) {
        _parkingLocalService = parkingLocalService;
    }

    /**
    * Adds the parking to the database. Also notifies the appropriate model listeners.
    *
    * @param parking the parking
    * @return the parking that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Parking addParking(
        br.com.atilo.jcondo.manager.model.Parking parking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.addParking(parking);
    }

    /**
    * Creates a new parking with the primary key. Does not add the parking to the database.
    *
    * @param id the primary key for the new parking
    * @return the new parking
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Parking createParking(long id) {
        return _parkingLocalService.createParking(id);
    }

    /**
    * Deletes the parking with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the parking
    * @return the parking that was removed
    * @throws PortalException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Parking deleteParking(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.deleteParking(id);
    }

    /**
    * Deletes the parking from the database. Also notifies the appropriate model listeners.
    *
    * @param parking the parking
    * @return the parking that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Parking deleteParking(
        br.com.atilo.jcondo.manager.model.Parking parking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.deleteParking(parking);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _parkingLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking fetchParking(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.fetchParking(id);
    }

    /**
    * Returns the parking with the primary key.
    *
    * @param id the primary key of the parking
    * @return the parking
    * @throws PortalException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Parking getParking(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.getParking(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the parkings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @return the range of parkings
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> getParkings(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.getParkings(start, end);
    }

    /**
    * Returns the number of parkings.
    *
    * @return the number of parkings
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getParkingsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.getParkingsCount();
    }

    /**
    * Updates the parking in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param parking the parking
    * @return the parking that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Parking updateParking(
        br.com.atilo.jcondo.manager.model.Parking parking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.updateParking(parking);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _parkingLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _parkingLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _parkingLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking addParking(
        java.lang.String code, br.com.atilo.jcondo.datatype.ParkingType type,
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.addParking(code, type, ownerDomainId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking updateParking(
        long parkingId, java.lang.String code,
        br.com.atilo.jcondo.datatype.ParkingType type, long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.updateParking(parkingId, code, type,
            ownerDomainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> getOwnedParkings(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.getOwnedParkings(ownerDomainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> getRentedParkings(
        long renterDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.getRentedParkings(renterDomainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> getGrantedParkings(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.getGrantedParkings(ownerDomainId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking rent(long ownerDomainId,
        long renterDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.rent(ownerDomainId, renterDomainId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking unrent(long parkingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingLocalService.unrent(parkingId);
    }

    @Override
    public void checkParkingAvailability(long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _parkingLocalService.checkParkingAvailability(domainId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ParkingLocalService getWrappedParkingLocalService() {
        return _parkingLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedParkingLocalService(
        ParkingLocalService parkingLocalService) {
        _parkingLocalService = parkingLocalService;
    }

    @Override
    public ParkingLocalService getWrappedService() {
        return _parkingLocalService;
    }

    @Override
    public void setWrappedService(ParkingLocalService parkingLocalService) {
        _parkingLocalService = parkingLocalService;
    }
}
