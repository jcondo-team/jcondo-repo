package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AccessLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AccessLocalService
 * @generated
 */
public class AccessLocalServiceWrapper implements AccessLocalService,
    ServiceWrapper<AccessLocalService> {
    private AccessLocalService _accessLocalService;

    public AccessLocalServiceWrapper(AccessLocalService accessLocalService) {
        _accessLocalService = accessLocalService;
    }

    /**
    * Adds the access to the database. Also notifies the appropriate model listeners.
    *
    * @param access the access
    * @return the access that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Access addAccess(
        br.com.atilo.jcondo.manager.model.Access access)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.addAccess(access);
    }

    /**
    * Creates a new access with the primary key. Does not add the access to the database.
    *
    * @param id the primary key for the new access
    * @return the new access
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Access createAccess(long id) {
        return _accessLocalService.createAccess(id);
    }

    /**
    * Deletes the access with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the access
    * @return the access that was removed
    * @throws PortalException if a access with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Access deleteAccess(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.deleteAccess(id);
    }

    /**
    * Deletes the access from the database. Also notifies the appropriate model listeners.
    *
    * @param access the access
    * @return the access that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Access deleteAccess(
        br.com.atilo.jcondo.manager.model.Access access)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.deleteAccess(access);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _accessLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Access fetchAccess(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.fetchAccess(id);
    }

    /**
    * Returns the access with the primary key.
    *
    * @param id the primary key of the access
    * @return the access
    * @throws PortalException if a access with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Access getAccess(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.getAccess(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the accesses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of accesses
    * @param end the upper bound of the range of accesses (not inclusive)
    * @return the range of accesses
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Access> getAccesses(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.getAccesses(start, end);
    }

    /**
    * Returns the number of accesses.
    *
    * @return the number of accesses
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getAccessesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.getAccessesCount();
    }

    /**
    * Updates the access in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param access the access
    * @return the access that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Access updateAccess(
        br.com.atilo.jcondo.manager.model.Access access)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.updateAccess(access);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _accessLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _accessLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _accessLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Access addAccess(
        long destinationId, long vehicleId, long authorizerId,
        java.lang.String authorizerName, long visitorId,
        java.lang.String badge, java.lang.String remark)
        throws java.lang.Exception {
        return _accessLocalService.addAccess(destinationId, vehicleId,
            authorizerId, authorizerName, visitorId, badge, remark);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Access addVehicleExitAccess(
        long vehicleId, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.addVehicleExitAccess(vehicleId, remark);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Access getAccessByBadge(
        java.lang.String badge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.getAccessByBadge(badge);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Access getAccessByVehicle(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.getAccessByVehicle(vehicleId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Access> getVehicleAccesses()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessLocalService.getVehicleAccesses();
    }

    @Override
    public void endAccess(long accessId, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _accessLocalService.endAccess(accessId, remark);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public AccessLocalService getWrappedAccessLocalService() {
        return _accessLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedAccessLocalService(
        AccessLocalService accessLocalService) {
        _accessLocalService = accessLocalService;
    }

    @Override
    public AccessLocalService getWrappedService() {
        return _accessLocalService;
    }

    @Override
    public void setWrappedService(AccessLocalService accessLocalService) {
        _accessLocalService = accessLocalService;
    }
}
