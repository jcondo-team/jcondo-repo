package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the MailNote service. Represents a row in the &quot;jco_mail_note&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see MailNoteModel
 * @see br.com.atilo.jcondo.manager.model.impl.MailNoteImpl
 * @see br.com.atilo.jcondo.manager.model.impl.MailNoteModelImpl
 * @generated
 */
public interface MailNote extends MailNoteModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.MailNoteImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
