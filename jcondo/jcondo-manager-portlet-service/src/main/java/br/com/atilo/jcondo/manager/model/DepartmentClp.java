package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.ClpSerializer;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class DepartmentClp extends BaseModelImpl<Department>
    implements Department {
    private long _id;
    private long _organizationId;
    private String _name;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _departmentRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public DepartmentClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Department.class;
    }

    @Override
    public String getModelClassName() {
        return Department.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("organizationId", getOrganizationId());
        attributes.put("name", getName());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long organizationId = (Long) attributes.get("organizationId");

        if (organizationId != null) {
            setOrganizationId(organizationId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_departmentRemoteModel != null) {
            try {
                Class<?> clazz = _departmentRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_departmentRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOrganizationId() {
        return _organizationId;
    }

    @Override
    public void setOrganizationId(long organizationId) {
        _organizationId = organizationId;

        if (_departmentRemoteModel != null) {
            try {
                Class<?> clazz = _departmentRemoteModel.getClass();

                Method method = clazz.getMethod("setOrganizationId", long.class);

                method.invoke(_departmentRemoteModel, organizationId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getName() {
        return _name;
    }

    @Override
    public void setName(String name) {
        _name = name;

        if (_departmentRemoteModel != null) {
            try {
                Class<?> clazz = _departmentRemoteModel.getClass();

                Method method = clazz.getMethod("setName", String.class);

                method.invoke(_departmentRemoteModel, name);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_departmentRemoteModel != null) {
            try {
                Class<?> clazz = _departmentRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_departmentRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_departmentRemoteModel != null) {
            try {
                Class<?> clazz = _departmentRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_departmentRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getMembers() {
        try {
            String methodName = "getMembers";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.manager.model.Person> returnObj = (java.util.List<br.com.atilo.jcondo.manager.model.Person>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setOrganization(
        com.liferay.portal.model.Organization organization) {
        try {
            String methodName = "setOrganization";

            Class<?>[] parameterTypes = new Class<?>[] {
                    com.liferay.portal.model.Organization.class
                };

            Object[] parameterValues = new Object[] { organization };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.datatype.DomainType getType() {
        try {
            String methodName = "getType";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.datatype.DomainType returnObj = (br.com.atilo.jcondo.datatype.DomainType) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.lang.String getDescription() {
        try {
            String methodName = "getDescription";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setDescription(java.lang.String description) {
        try {
            String methodName = "setDescription";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { description };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public com.liferay.portal.model.Organization getOrganization() {
        try {
            String methodName = "getOrganization";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.liferay.portal.model.Organization returnObj = (com.liferay.portal.model.Organization) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setType(br.com.atilo.jcondo.datatype.DomainType type) {
        try {
            String methodName = "setType";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.datatype.DomainType.class
                };

            Object[] parameterValues = new Object[] { type };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getDepartmentRemoteModel() {
        return _departmentRemoteModel;
    }

    public void setDepartmentRemoteModel(BaseModel<?> departmentRemoteModel) {
        _departmentRemoteModel = departmentRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _departmentRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_departmentRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            DepartmentLocalServiceUtil.addDepartment(this);
        } else {
            DepartmentLocalServiceUtil.updateDepartment(this);
        }
    }

    @Override
    public Department toEscapedModel() {
        return (Department) ProxyUtil.newProxyInstance(Department.class.getClassLoader(),
            new Class[] { Department.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        DepartmentClp clone = new DepartmentClp();

        clone.setId(getId());
        clone.setOrganizationId(getOrganizationId());
        clone.setName(getName());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Department department) {
        int value = 0;

        value = getName().compareTo(department.getName());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof DepartmentClp)) {
            return false;
        }

        DepartmentClp department = (DepartmentClp) obj;

        long primaryKey = department.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", organizationId=");
        sb.append(getOrganizationId());
        sb.append(", name=");
        sb.append(getName());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Department");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>organizationId</column-name><column-value><![CDATA[");
        sb.append(getOrganizationId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>name</column-name><column-value><![CDATA[");
        sb.append(getName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
