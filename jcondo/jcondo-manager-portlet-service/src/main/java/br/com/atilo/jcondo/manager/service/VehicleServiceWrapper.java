package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VehicleService}.
 *
 * @author Brian Wing Shun Chan
 * @see VehicleService
 * @generated
 */
public class VehicleServiceWrapper implements VehicleService,
    ServiceWrapper<VehicleService> {
    private VehicleService _vehicleService;

    public VehicleServiceWrapper(VehicleService vehicleService) {
        _vehicleService = vehicleService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vehicleService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vehicleService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vehicleService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle createVehicle() {
        return _vehicleService.createVehicle();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle addVehicle(
        java.lang.String license,
        br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleService.addVehicle(license, type, domainId, portrait,
            brand, color, remark);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle updateVehicle(long id,
        br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleService.updateVehicle(id, type, domainId, portrait,
            brand, color, remark);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle updateVehiclePortrait(
        long id, br.com.atilo.jcondo.Image portrait)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleService.updateVehiclePortrait(id, portrait);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle deleteVehicle(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleService.deleteVehicle(id);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle getVehicleByLicense(
        java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleService.getVehicleByLicense(license);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getDomainVehicles(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleService.getDomainVehicles(domainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getDomainVehiclesByLicense(
        long domainId, java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleService.getDomainVehiclesByLicense(domainId, license);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VehicleService getWrappedVehicleService() {
        return _vehicleService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVehicleService(VehicleService vehicleService) {
        _vehicleService = vehicleService;
    }

    @Override
    public VehicleService getWrappedService() {
        return _vehicleService;
    }

    @Override
    public void setWrappedService(VehicleService vehicleService) {
        _vehicleService = vehicleService;
    }
}
