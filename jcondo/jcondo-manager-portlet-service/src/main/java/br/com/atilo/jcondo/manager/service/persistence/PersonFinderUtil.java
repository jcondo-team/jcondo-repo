package br.com.atilo.jcondo.manager.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class PersonFinderUtil {
    private static PersonFinder _finder;

    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> findPersonByNameIdentityDomain(
        java.lang.String name, java.lang.String identity, long domainId) {
        return getFinder()
                   .findPersonByNameIdentityDomain(name, identity, domainId);
    }

    public static PersonFinder getFinder() {
        if (_finder == null) {
            _finder = (PersonFinder) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.manager.service.ClpSerializer.getServletContextName(),
                    PersonFinder.class.getName());

            ReferenceRegistry.registerReference(PersonFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(PersonFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(PersonFinderUtil.class, "_finder");
    }
}
