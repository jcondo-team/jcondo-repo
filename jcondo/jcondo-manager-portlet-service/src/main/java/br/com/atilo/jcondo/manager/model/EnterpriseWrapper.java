package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Enterprise}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Enterprise
 * @generated
 */
public class EnterpriseWrapper implements Enterprise, ModelWrapper<Enterprise> {
    private Enterprise _enterprise;

    public EnterpriseWrapper(Enterprise enterprise) {
        _enterprise = enterprise;
    }

    @Override
    public Class<?> getModelClass() {
        return Enterprise.class;
    }

    @Override
    public String getModelClassName() {
        return Enterprise.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("organizationId", getOrganizationId());
        attributes.put("statusId", getStatusId());
        attributes.put("identity", getIdentity());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long organizationId = (Long) attributes.get("organizationId");

        if (organizationId != null) {
            setOrganizationId(organizationId);
        }

        Integer statusId = (Integer) attributes.get("statusId");

        if (statusId != null) {
            setStatusId(statusId);
        }

        String identity = (String) attributes.get("identity");

        if (identity != null) {
            setIdentity(identity);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this enterprise.
    *
    * @return the primary key of this enterprise
    */
    @Override
    public long getPrimaryKey() {
        return _enterprise.getPrimaryKey();
    }

    /**
    * Sets the primary key of this enterprise.
    *
    * @param primaryKey the primary key of this enterprise
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _enterprise.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this enterprise.
    *
    * @return the ID of this enterprise
    */
    @Override
    public long getId() {
        return _enterprise.getId();
    }

    /**
    * Sets the ID of this enterprise.
    *
    * @param id the ID of this enterprise
    */
    @Override
    public void setId(long id) {
        _enterprise.setId(id);
    }

    /**
    * Returns the organization ID of this enterprise.
    *
    * @return the organization ID of this enterprise
    */
    @Override
    public long getOrganizationId() {
        return _enterprise.getOrganizationId();
    }

    /**
    * Sets the organization ID of this enterprise.
    *
    * @param organizationId the organization ID of this enterprise
    */
    @Override
    public void setOrganizationId(long organizationId) {
        _enterprise.setOrganizationId(organizationId);
    }

    /**
    * Returns the status ID of this enterprise.
    *
    * @return the status ID of this enterprise
    */
    @Override
    public int getStatusId() {
        return _enterprise.getStatusId();
    }

    /**
    * Sets the status ID of this enterprise.
    *
    * @param statusId the status ID of this enterprise
    */
    @Override
    public void setStatusId(int statusId) {
        _enterprise.setStatusId(statusId);
    }

    /**
    * Returns the identity of this enterprise.
    *
    * @return the identity of this enterprise
    */
    @Override
    public java.lang.String getIdentity() {
        return _enterprise.getIdentity();
    }

    /**
    * Sets the identity of this enterprise.
    *
    * @param identity the identity of this enterprise
    */
    @Override
    public void setIdentity(java.lang.String identity) {
        _enterprise.setIdentity(identity);
    }

    /**
    * Returns the opr date of this enterprise.
    *
    * @return the opr date of this enterprise
    */
    @Override
    public java.util.Date getOprDate() {
        return _enterprise.getOprDate();
    }

    /**
    * Sets the opr date of this enterprise.
    *
    * @param oprDate the opr date of this enterprise
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _enterprise.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this enterprise.
    *
    * @return the opr user of this enterprise
    */
    @Override
    public long getOprUser() {
        return _enterprise.getOprUser();
    }

    /**
    * Sets the opr user of this enterprise.
    *
    * @param oprUser the opr user of this enterprise
    */
    @Override
    public void setOprUser(long oprUser) {
        _enterprise.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _enterprise.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _enterprise.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _enterprise.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _enterprise.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _enterprise.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _enterprise.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _enterprise.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _enterprise.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _enterprise.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _enterprise.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _enterprise.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new EnterpriseWrapper((Enterprise) _enterprise.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.manager.model.Enterprise enterprise) {
        return _enterprise.compareTo(enterprise);
    }

    @Override
    public int hashCode() {
        return _enterprise.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Enterprise> toCacheModel() {
        return _enterprise.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise toEscapedModel() {
        return new EnterpriseWrapper(_enterprise.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise toUnescapedModel() {
        return new EnterpriseWrapper(_enterprise.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _enterprise.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _enterprise.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _enterprise.persist();
    }

    @Override
    public java.lang.String getName() {
        return _enterprise.getName();
    }

    @Override
    public void setName(java.lang.String name) {
        _enterprise.setName(name);
    }

    @Override
    public br.com.atilo.jcondo.datatype.EnterpriseStatus getStatus()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterprise.getStatus();
    }

    @Override
    public void setStatus(br.com.atilo.jcondo.datatype.EnterpriseStatus status) {
        _enterprise.setStatus(status);
    }

    @Override
    public br.com.atilo.jcondo.datatype.DomainType getType()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterprise.getType();
    }

    @Override
    public void setType(br.com.atilo.jcondo.datatype.DomainType type) {
        _enterprise.setType(type);
    }

    @Override
    public java.lang.String getDescription()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterprise.getDescription();
    }

    @Override
    public void setDescription(java.lang.String description) {
        _enterprise.setDescription(description);
    }

    @Override
    public com.liferay.portal.model.Address getAddress()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterprise.getAddress();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getPeople()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterprise.getPeople();
    }

    @Override
    public com.liferay.portal.model.Organization getOrganization() {
        return _enterprise.getOrganization();
    }

    @Override
    public void setOrganization(
        com.liferay.portal.model.Organization organization) {
        _enterprise.setOrganization(organization);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof EnterpriseWrapper)) {
            return false;
        }

        EnterpriseWrapper enterpriseWrapper = (EnterpriseWrapper) obj;

        if (Validator.equals(_enterprise, enterpriseWrapper._enterprise)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Enterprise getWrappedEnterprise() {
        return _enterprise;
    }

    @Override
    public Enterprise getWrappedModel() {
        return _enterprise;
    }

    @Override
    public void resetOriginalValues() {
        _enterprise.resetOriginalValues();
    }
}
