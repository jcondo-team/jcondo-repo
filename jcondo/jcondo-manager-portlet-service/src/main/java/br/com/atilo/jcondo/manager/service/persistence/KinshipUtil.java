package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Kinship;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the kinship service. This utility wraps {@link KinshipPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see KinshipPersistence
 * @see KinshipPersistenceImpl
 * @generated
 */
public class KinshipUtil {
    private static KinshipPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Kinship kinship) {
        getPersistence().clearCache(kinship);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Kinship> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Kinship> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Kinship> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Kinship update(Kinship kinship) throws SystemException {
        return getPersistence().update(kinship);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Kinship update(Kinship kinship, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(kinship, serviceContext);
    }

    /**
    * Returns all the kinships where personId = &#63;.
    *
    * @param personId the person ID
    * @return the matching kinships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Kinship> findByPerson(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson(personId);
    }

    /**
    * Returns a range of all the kinships where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of kinships
    * @param end the upper bound of the range of kinships (not inclusive)
    * @return the range of matching kinships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Kinship> findByPerson(
        long personId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson(personId, start, end);
    }

    /**
    * Returns an ordered range of all the kinships where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of kinships
    * @param end the upper bound of the range of kinships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching kinships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Kinship> findByPerson(
        long personId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPerson(personId, start, end, orderByComparator);
    }

    /**
    * Returns the first kinship in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching kinship
    * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a matching kinship could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Kinship findByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchKinshipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson_First(personId, orderByComparator);
    }

    /**
    * Returns the first kinship in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching kinship, or <code>null</code> if a matching kinship could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Kinship fetchByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPerson_First(personId, orderByComparator);
    }

    /**
    * Returns the last kinship in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching kinship
    * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a matching kinship could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Kinship findByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchKinshipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson_Last(personId, orderByComparator);
    }

    /**
    * Returns the last kinship in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching kinship, or <code>null</code> if a matching kinship could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Kinship fetchByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPerson_Last(personId, orderByComparator);
    }

    /**
    * Returns the kinships before and after the current kinship in the ordered set where personId = &#63;.
    *
    * @param id the primary key of the current kinship
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next kinship
    * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a kinship with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Kinship[] findByPerson_PrevAndNext(
        long id, long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchKinshipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPerson_PrevAndNext(id, personId, orderByComparator);
    }

    /**
    * Removes all the kinships where personId = &#63; from the database.
    *
    * @param personId the person ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByPerson(personId);
    }

    /**
    * Returns the number of kinships where personId = &#63;.
    *
    * @param personId the person ID
    * @return the number of matching kinships
    * @throws SystemException if a system exception occurred
    */
    public static int countByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByPerson(personId);
    }

    /**
    * Caches the kinship in the entity cache if it is enabled.
    *
    * @param kinship the kinship
    */
    public static void cacheResult(
        br.com.atilo.jcondo.manager.model.Kinship kinship) {
        getPersistence().cacheResult(kinship);
    }

    /**
    * Caches the kinships in the entity cache if it is enabled.
    *
    * @param kinships the kinships
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Kinship> kinships) {
        getPersistence().cacheResult(kinships);
    }

    /**
    * Creates a new kinship with the primary key. Does not add the kinship to the database.
    *
    * @param id the primary key for the new kinship
    * @return the new kinship
    */
    public static br.com.atilo.jcondo.manager.model.Kinship create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the kinship with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the kinship
    * @return the kinship that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a kinship with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Kinship remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchKinshipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.manager.model.Kinship updateImpl(
        br.com.atilo.jcondo.manager.model.Kinship kinship)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(kinship);
    }

    /**
    * Returns the kinship with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchKinshipException} if it could not be found.
    *
    * @param id the primary key of the kinship
    * @return the kinship
    * @throws br.com.atilo.jcondo.manager.NoSuchKinshipException if a kinship with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Kinship findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.manager.NoSuchKinshipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the kinship with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the kinship
    * @return the kinship, or <code>null</code> if a kinship with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Kinship fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the kinships.
    *
    * @return the kinships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Kinship> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the kinships.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of kinships
    * @param end the upper bound of the range of kinships (not inclusive)
    * @return the range of kinships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Kinship> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the kinships.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of kinships
    * @param end the upper bound of the range of kinships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of kinships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Kinship> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the kinships from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of kinships.
    *
    * @return the number of kinships
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static KinshipPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (KinshipPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.manager.service.ClpSerializer.getServletContextName(),
                    KinshipPersistence.class.getName());

            ReferenceRegistry.registerReference(KinshipUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(KinshipPersistence persistence) {
    }
}
