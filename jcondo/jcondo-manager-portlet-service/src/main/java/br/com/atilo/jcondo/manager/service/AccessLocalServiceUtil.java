package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Access. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.AccessLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AccessLocalService
 * @see br.com.atilo.jcondo.manager.service.base.AccessLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.AccessLocalServiceImpl
 * @generated
 */
public class AccessLocalServiceUtil {
    private static AccessLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.AccessLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the access to the database. Also notifies the appropriate model listeners.
    *
    * @param access the access
    * @return the access that was added
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Access addAccess(
        br.com.atilo.jcondo.manager.model.Access access)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addAccess(access);
    }

    /**
    * Creates a new access with the primary key. Does not add the access to the database.
    *
    * @param id the primary key for the new access
    * @return the new access
    */
    public static br.com.atilo.jcondo.manager.model.Access createAccess(long id) {
        return getService().createAccess(id);
    }

    /**
    * Deletes the access with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the access
    * @return the access that was removed
    * @throws PortalException if a access with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Access deleteAccess(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteAccess(id);
    }

    /**
    * Deletes the access from the database. Also notifies the appropriate model listeners.
    *
    * @param access the access
    * @return the access that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Access deleteAccess(
        br.com.atilo.jcondo.manager.model.Access access)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteAccess(access);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static br.com.atilo.jcondo.manager.model.Access fetchAccess(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchAccess(id);
    }

    /**
    * Returns the access with the primary key.
    *
    * @param id the primary key of the access
    * @return the access
    * @throws PortalException if a access with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Access getAccess(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getAccess(id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the accesses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of accesses
    * @param end the upper bound of the range of accesses (not inclusive)
    * @return the range of accesses
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Access> getAccesses(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAccesses(start, end);
    }

    /**
    * Returns the number of accesses.
    *
    * @return the number of accesses
    * @throws SystemException if a system exception occurred
    */
    public static int getAccessesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAccessesCount();
    }

    /**
    * Updates the access in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param access the access
    * @return the access that was updated
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Access updateAccess(
        br.com.atilo.jcondo.manager.model.Access access)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateAccess(access);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Access addAccess(
        long destinationId, long vehicleId, long authorizerId,
        java.lang.String authorizerName, long visitorId,
        java.lang.String badge, java.lang.String remark)
        throws java.lang.Exception {
        return getService()
                   .addAccess(destinationId, vehicleId, authorizerId,
            authorizerName, visitorId, badge, remark);
    }

    public static br.com.atilo.jcondo.manager.model.Access addVehicleExitAccess(
        long vehicleId, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addVehicleExitAccess(vehicleId, remark);
    }

    public static br.com.atilo.jcondo.manager.model.Access getAccessByBadge(
        java.lang.String badge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAccessByBadge(badge);
    }

    public static br.com.atilo.jcondo.manager.model.Access getAccessByVehicle(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getAccessByVehicle(vehicleId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Access> getVehicleAccesses()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleAccesses();
    }

    public static void endAccess(long accessId, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService().endAccess(accessId, remark);
    }

    public static void clearService() {
        _service = null;
    }

    public static AccessLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    AccessLocalService.class.getName());

            if (invokableLocalService instanceof AccessLocalService) {
                _service = (AccessLocalService) invokableLocalService;
            } else {
                _service = new AccessLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(AccessLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(AccessLocalService service) {
    }
}
