package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PetService}.
 *
 * @author Brian Wing Shun Chan
 * @see PetService
 * @generated
 */
public class PetServiceWrapper implements PetService,
    ServiceWrapper<PetService> {
    private PetService _petService;

    public PetServiceWrapper(PetService petService) {
        _petService = petService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _petService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _petService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _petService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PetService getWrappedPetService() {
        return _petService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPetService(PetService petService) {
        _petService = petService;
    }

    @Override
    public PetService getWrappedService() {
        return _petService;
    }

    @Override
    public void setWrappedService(PetService petService) {
        _petService = petService;
    }
}
