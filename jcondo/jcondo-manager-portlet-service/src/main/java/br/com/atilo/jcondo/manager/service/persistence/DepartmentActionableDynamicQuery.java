package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class DepartmentActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public DepartmentActionableDynamicQuery() throws SystemException {
        setBaseLocalService(DepartmentLocalServiceUtil.getService());
        setClass(Department.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
