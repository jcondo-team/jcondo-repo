package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TelephoneService}.
 *
 * @author Brian Wing Shun Chan
 * @see TelephoneService
 * @generated
 */
public class TelephoneServiceWrapper implements TelephoneService,
    ServiceWrapper<TelephoneService> {
    private TelephoneService _telephoneService;

    public TelephoneServiceWrapper(TelephoneService telephoneService) {
        _telephoneService = telephoneService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _telephoneService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _telephoneService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _telephoneService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone addDomainPhone(
        long domainId, long number, long extension, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneService.addDomainPhone(domainId, number, extension,
            primary);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone addPersonPhone(
        long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary,
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneService.addPersonPhone(number, extension, type,
            primary, personId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone updateEnterprisePhone(
        long phoneId, long number, long extension, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneService.updateEnterprisePhone(phoneId, number,
            extension, primary);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone updatePersonPhone(
        long phoneId, long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneService.updatePersonPhone(phoneId, number, extension,
            type, primary);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone deletePhone(long phoneId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneService.deletePhone(phoneId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getPersonPhones(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneService.getPersonPhones(personId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getPersonPhones(
        long personId, br.com.atilo.jcondo.datatype.PhoneType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneService.getPersonPhones(personId, type);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone getPersonPhone(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneService.getPersonPhone(personId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getDomainPhones(
        long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneService.getDomainPhones(domainId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public TelephoneService getWrappedTelephoneService() {
        return _telephoneService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedTelephoneService(TelephoneService telephoneService) {
        _telephoneService = telephoneService;
    }

    @Override
    public TelephoneService getWrappedService() {
        return _telephoneService;
    }

    @Override
    public void setWrappedService(TelephoneService telephoneService) {
        _telephoneService = telephoneService;
    }
}
