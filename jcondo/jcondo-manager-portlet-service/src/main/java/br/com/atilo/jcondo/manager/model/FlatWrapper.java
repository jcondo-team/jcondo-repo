package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Flat}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Flat
 * @generated
 */
public class FlatWrapper implements Flat, ModelWrapper<Flat> {
    private Flat _flat;

    public FlatWrapper(Flat flat) {
        _flat = flat;
    }

    @Override
    public Class<?> getModelClass() {
        return Flat.class;
    }

    @Override
    public String getModelClassName() {
        return Flat.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("organizationId", getOrganizationId());
        attributes.put("folderId", getFolderId());
        attributes.put("personId", getPersonId());
        attributes.put("block", getBlock());
        attributes.put("number", getNumber());
        attributes.put("pets", getPets());
        attributes.put("disables", getDisables());
        attributes.put("brigade", getBrigade());
        attributes.put("defaulting", getDefaulting());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long organizationId = (Long) attributes.get("organizationId");

        if (organizationId != null) {
            setOrganizationId(organizationId);
        }

        Long folderId = (Long) attributes.get("folderId");

        if (folderId != null) {
            setFolderId(folderId);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Integer block = (Integer) attributes.get("block");

        if (block != null) {
            setBlock(block);
        }

        Integer number = (Integer) attributes.get("number");

        if (number != null) {
            setNumber(number);
        }

        Boolean pets = (Boolean) attributes.get("pets");

        if (pets != null) {
            setPets(pets);
        }

        Boolean disables = (Boolean) attributes.get("disables");

        if (disables != null) {
            setDisables(disables);
        }

        Boolean brigade = (Boolean) attributes.get("brigade");

        if (brigade != null) {
            setBrigade(brigade);
        }

        Boolean defaulting = (Boolean) attributes.get("defaulting");

        if (defaulting != null) {
            setDefaulting(defaulting);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this flat.
    *
    * @return the primary key of this flat
    */
    @Override
    public long getPrimaryKey() {
        return _flat.getPrimaryKey();
    }

    /**
    * Sets the primary key of this flat.
    *
    * @param primaryKey the primary key of this flat
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _flat.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this flat.
    *
    * @return the ID of this flat
    */
    @Override
    public long getId() {
        return _flat.getId();
    }

    /**
    * Sets the ID of this flat.
    *
    * @param id the ID of this flat
    */
    @Override
    public void setId(long id) {
        _flat.setId(id);
    }

    /**
    * Returns the organization ID of this flat.
    *
    * @return the organization ID of this flat
    */
    @Override
    public long getOrganizationId() {
        return _flat.getOrganizationId();
    }

    /**
    * Sets the organization ID of this flat.
    *
    * @param organizationId the organization ID of this flat
    */
    @Override
    public void setOrganizationId(long organizationId) {
        _flat.setOrganizationId(organizationId);
    }

    /**
    * Returns the folder ID of this flat.
    *
    * @return the folder ID of this flat
    */
    @Override
    public long getFolderId() {
        return _flat.getFolderId();
    }

    /**
    * Sets the folder ID of this flat.
    *
    * @param folderId the folder ID of this flat
    */
    @Override
    public void setFolderId(long folderId) {
        _flat.setFolderId(folderId);
    }

    /**
    * Returns the person ID of this flat.
    *
    * @return the person ID of this flat
    */
    @Override
    public long getPersonId() {
        return _flat.getPersonId();
    }

    /**
    * Sets the person ID of this flat.
    *
    * @param personId the person ID of this flat
    */
    @Override
    public void setPersonId(long personId) {
        _flat.setPersonId(personId);
    }

    /**
    * Returns the block of this flat.
    *
    * @return the block of this flat
    */
    @Override
    public int getBlock() {
        return _flat.getBlock();
    }

    /**
    * Sets the block of this flat.
    *
    * @param block the block of this flat
    */
    @Override
    public void setBlock(int block) {
        _flat.setBlock(block);
    }

    /**
    * Returns the number of this flat.
    *
    * @return the number of this flat
    */
    @Override
    public int getNumber() {
        return _flat.getNumber();
    }

    /**
    * Sets the number of this flat.
    *
    * @param number the number of this flat
    */
    @Override
    public void setNumber(int number) {
        _flat.setNumber(number);
    }

    /**
    * Returns the pets of this flat.
    *
    * @return the pets of this flat
    */
    @Override
    public boolean getPets() {
        return _flat.getPets();
    }

    /**
    * Returns <code>true</code> if this flat is pets.
    *
    * @return <code>true</code> if this flat is pets; <code>false</code> otherwise
    */
    @Override
    public boolean isPets() {
        return _flat.isPets();
    }

    /**
    * Sets whether this flat is pets.
    *
    * @param pets the pets of this flat
    */
    @Override
    public void setPets(boolean pets) {
        _flat.setPets(pets);
    }

    /**
    * Returns the disables of this flat.
    *
    * @return the disables of this flat
    */
    @Override
    public boolean getDisables() {
        return _flat.getDisables();
    }

    /**
    * Returns <code>true</code> if this flat is disables.
    *
    * @return <code>true</code> if this flat is disables; <code>false</code> otherwise
    */
    @Override
    public boolean isDisables() {
        return _flat.isDisables();
    }

    /**
    * Sets whether this flat is disables.
    *
    * @param disables the disables of this flat
    */
    @Override
    public void setDisables(boolean disables) {
        _flat.setDisables(disables);
    }

    /**
    * Returns the brigade of this flat.
    *
    * @return the brigade of this flat
    */
    @Override
    public boolean getBrigade() {
        return _flat.getBrigade();
    }

    /**
    * Returns <code>true</code> if this flat is brigade.
    *
    * @return <code>true</code> if this flat is brigade; <code>false</code> otherwise
    */
    @Override
    public boolean isBrigade() {
        return _flat.isBrigade();
    }

    /**
    * Sets whether this flat is brigade.
    *
    * @param brigade the brigade of this flat
    */
    @Override
    public void setBrigade(boolean brigade) {
        _flat.setBrigade(brigade);
    }

    /**
    * Returns the defaulting of this flat.
    *
    * @return the defaulting of this flat
    */
    @Override
    public boolean getDefaulting() {
        return _flat.getDefaulting();
    }

    /**
    * Returns <code>true</code> if this flat is defaulting.
    *
    * @return <code>true</code> if this flat is defaulting; <code>false</code> otherwise
    */
    @Override
    public boolean isDefaulting() {
        return _flat.isDefaulting();
    }

    /**
    * Sets whether this flat is defaulting.
    *
    * @param defaulting the defaulting of this flat
    */
    @Override
    public void setDefaulting(boolean defaulting) {
        _flat.setDefaulting(defaulting);
    }

    /**
    * Returns the opr date of this flat.
    *
    * @return the opr date of this flat
    */
    @Override
    public java.util.Date getOprDate() {
        return _flat.getOprDate();
    }

    /**
    * Sets the opr date of this flat.
    *
    * @param oprDate the opr date of this flat
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _flat.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this flat.
    *
    * @return the opr user of this flat
    */
    @Override
    public long getOprUser() {
        return _flat.getOprUser();
    }

    /**
    * Sets the opr user of this flat.
    *
    * @param oprUser the opr user of this flat
    */
    @Override
    public void setOprUser(long oprUser) {
        _flat.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _flat.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _flat.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _flat.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _flat.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _flat.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _flat.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _flat.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _flat.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _flat.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _flat.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _flat.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new FlatWrapper((Flat) _flat.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Flat flat) {
        return _flat.compareTo(flat);
    }

    @Override
    public int hashCode() {
        return _flat.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Flat> toCacheModel() {
        return _flat.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat toEscapedModel() {
        return new FlatWrapper(_flat.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat toUnescapedModel() {
        return new FlatWrapper(_flat.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _flat.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _flat.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _flat.persist();
    }

    @Override
    public java.lang.String getName()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flat.getName();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flat.getPerson();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.datatype.PetType> getPetTypes()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flat.getPetTypes();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getPeople()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flat.getPeople();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof FlatWrapper)) {
            return false;
        }

        FlatWrapper flatWrapper = (FlatWrapper) obj;

        if (Validator.equals(_flat, flatWrapper._flat)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Flat getWrappedFlat() {
        return _flat;
    }

    @Override
    public Flat getWrappedModel() {
        return _flat;
    }

    @Override
    public void resetOriginalValues() {
        _flat.resetOriginalValues();
    }
}
