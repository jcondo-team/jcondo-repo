package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Kinship;
import br.com.atilo.jcondo.manager.service.KinshipLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class KinshipActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public KinshipActionableDynamicQuery() throws SystemException {
        setBaseLocalService(KinshipLocalServiceUtil.getService());
        setClass(Kinship.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
