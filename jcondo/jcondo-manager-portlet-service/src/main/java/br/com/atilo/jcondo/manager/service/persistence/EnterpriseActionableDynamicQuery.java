package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class EnterpriseActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public EnterpriseActionableDynamicQuery() throws SystemException {
        setBaseLocalService(EnterpriseLocalServiceUtil.getService());
        setClass(Enterprise.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
