package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.AccessPermissionServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.AccessPermissionServiceSoap
 * @generated
 */
public class AccessPermissionSoap implements Serializable {
    private long _id;
    private long _personId;
    private long _domainId;
    private int _weekDay;
    private String _beginTime;
    private String _endTime;
    private Date _beginDate;
    private Date _endDate;
    private Date _oprDate;
    private long _oprUser;

    public AccessPermissionSoap() {
    }

    public static AccessPermissionSoap toSoapModel(AccessPermission model) {
        AccessPermissionSoap soapModel = new AccessPermissionSoap();

        soapModel.setId(model.getId());
        soapModel.setPersonId(model.getPersonId());
        soapModel.setDomainId(model.getDomainId());
        soapModel.setWeekDay(model.getWeekDay());
        soapModel.setBeginTime(model.getBeginTime());
        soapModel.setEndTime(model.getEndTime());
        soapModel.setBeginDate(model.getBeginDate());
        soapModel.setEndDate(model.getEndDate());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static AccessPermissionSoap[] toSoapModels(AccessPermission[] models) {
        AccessPermissionSoap[] soapModels = new AccessPermissionSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static AccessPermissionSoap[][] toSoapModels(
        AccessPermission[][] models) {
        AccessPermissionSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new AccessPermissionSoap[models.length][models[0].length];
        } else {
            soapModels = new AccessPermissionSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static AccessPermissionSoap[] toSoapModels(
        List<AccessPermission> models) {
        List<AccessPermissionSoap> soapModels = new ArrayList<AccessPermissionSoap>(models.size());

        for (AccessPermission model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new AccessPermissionSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getPersonId() {
        return _personId;
    }

    public void setPersonId(long personId) {
        _personId = personId;
    }

    public long getDomainId() {
        return _domainId;
    }

    public void setDomainId(long domainId) {
        _domainId = domainId;
    }

    public int getWeekDay() {
        return _weekDay;
    }

    public void setWeekDay(int weekDay) {
        _weekDay = weekDay;
    }

    public String getBeginTime() {
        return _beginTime;
    }

    public void setBeginTime(String beginTime) {
        _beginTime = beginTime;
    }

    public String getEndTime() {
        return _endTime;
    }

    public void setEndTime(String endTime) {
        _endTime = endTime;
    }

    public Date getBeginDate() {
        return _beginDate;
    }

    public void setBeginDate(Date beginDate) {
        _beginDate = beginDate;
    }

    public Date getEndDate() {
        return _endDate;
    }

    public void setEndDate(Date endDate) {
        _endDate = endDate;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
