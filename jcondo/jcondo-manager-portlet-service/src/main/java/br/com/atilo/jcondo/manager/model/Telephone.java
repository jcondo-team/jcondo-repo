package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Telephone service. Represents a row in the &quot;jco_mgr_Telephone&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see TelephoneModel
 * @see br.com.atilo.jcondo.manager.model.impl.TelephoneImpl
 * @see br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl
 * @generated
 */
public interface Telephone extends TelephoneModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.TelephoneImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.datatype.PhoneType getType();

    public void setType(br.com.atilo.jcondo.datatype.PhoneType type);
}
