package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.ClpSerializer;
import br.com.atilo.jcondo.manager.service.PetLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PetClp extends BaseModelImpl<Pet> implements Pet {
    private long _petId;
    private int _typeId;
    private long _flatId;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _petRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public PetClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Pet.class;
    }

    @Override
    public String getModelClassName() {
        return Pet.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _petId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setPetId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _petId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("petId", getPetId());
        attributes.put("typeId", getTypeId());
        attributes.put("flatId", getFlatId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long petId = (Long) attributes.get("petId");

        if (petId != null) {
            setPetId(petId);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Long flatId = (Long) attributes.get("flatId");

        if (flatId != null) {
            setFlatId(flatId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getPetId() {
        return _petId;
    }

    @Override
    public void setPetId(long petId) {
        _petId = petId;

        if (_petRemoteModel != null) {
            try {
                Class<?> clazz = _petRemoteModel.getClass();

                Method method = clazz.getMethod("setPetId", long.class);

                method.invoke(_petRemoteModel, petId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTypeId() {
        return _typeId;
    }

    @Override
    public void setTypeId(int typeId) {
        _typeId = typeId;

        if (_petRemoteModel != null) {
            try {
                Class<?> clazz = _petRemoteModel.getClass();

                Method method = clazz.getMethod("setTypeId", int.class);

                method.invoke(_petRemoteModel, typeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getFlatId() {
        return _flatId;
    }

    @Override
    public void setFlatId(long flatId) {
        _flatId = flatId;

        if (_petRemoteModel != null) {
            try {
                Class<?> clazz = _petRemoteModel.getClass();

                Method method = clazz.getMethod("setFlatId", long.class);

                method.invoke(_petRemoteModel, flatId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_petRemoteModel != null) {
            try {
                Class<?> clazz = _petRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_petRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_petRemoteModel != null) {
            try {
                Class<?> clazz = _petRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_petRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getPetRemoteModel() {
        return _petRemoteModel;
    }

    public void setPetRemoteModel(BaseModel<?> petRemoteModel) {
        _petRemoteModel = petRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _petRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_petRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            PetLocalServiceUtil.addPet(this);
        } else {
            PetLocalServiceUtil.updatePet(this);
        }
    }

    @Override
    public Pet toEscapedModel() {
        return (Pet) ProxyUtil.newProxyInstance(Pet.class.getClassLoader(),
            new Class[] { Pet.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        PetClp clone = new PetClp();

        clone.setPetId(getPetId());
        clone.setTypeId(getTypeId());
        clone.setFlatId(getFlatId());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Pet pet) {
        long primaryKey = pet.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PetClp)) {
            return false;
        }

        PetClp pet = (PetClp) obj;

        long primaryKey = pet.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{petId=");
        sb.append(getPetId());
        sb.append(", typeId=");
        sb.append(getTypeId());
        sb.append(", flatId=");
        sb.append(getFlatId());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Pet");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>petId</column-name><column-value><![CDATA[");
        sb.append(getPetId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>typeId</column-name><column-value><![CDATA[");
        sb.append(getTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>flatId</column-name><column-value><![CDATA[");
        sb.append(getFlatId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
