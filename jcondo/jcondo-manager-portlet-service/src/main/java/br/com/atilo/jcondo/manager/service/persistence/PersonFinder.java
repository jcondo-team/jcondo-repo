package br.com.atilo.jcondo.manager.service.persistence;

public interface PersonFinder {
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> findPersonByNameIdentityDomain(
        java.lang.String name, java.lang.String identity, long domainId);
}
