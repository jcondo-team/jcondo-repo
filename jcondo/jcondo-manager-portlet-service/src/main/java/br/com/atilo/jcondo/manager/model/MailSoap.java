package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.MailServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.MailServiceSoap
 * @generated
 */
public class MailSoap implements Serializable {
    private long _id;
    private long _recipientId;
    private String _recipientName;
    private long _takerId;
    private String _takerName;
    private int _typeId;
    private String _code;
    private Date _dateIn;
    private Date _dateOut;
    private String _remark;
    private long _signatureId;
    private Date _oprDate;
    private long _oprUser;

    public MailSoap() {
    }

    public static MailSoap toSoapModel(Mail model) {
        MailSoap soapModel = new MailSoap();

        soapModel.setId(model.getId());
        soapModel.setRecipientId(model.getRecipientId());
        soapModel.setRecipientName(model.getRecipientName());
        soapModel.setTakerId(model.getTakerId());
        soapModel.setTakerName(model.getTakerName());
        soapModel.setTypeId(model.getTypeId());
        soapModel.setCode(model.getCode());
        soapModel.setDateIn(model.getDateIn());
        soapModel.setDateOut(model.getDateOut());
        soapModel.setRemark(model.getRemark());
        soapModel.setSignatureId(model.getSignatureId());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static MailSoap[] toSoapModels(Mail[] models) {
        MailSoap[] soapModels = new MailSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static MailSoap[][] toSoapModels(Mail[][] models) {
        MailSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new MailSoap[models.length][models[0].length];
        } else {
            soapModels = new MailSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static MailSoap[] toSoapModels(List<Mail> models) {
        List<MailSoap> soapModels = new ArrayList<MailSoap>(models.size());

        for (Mail model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new MailSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getRecipientId() {
        return _recipientId;
    }

    public void setRecipientId(long recipientId) {
        _recipientId = recipientId;
    }

    public String getRecipientName() {
        return _recipientName;
    }

    public void setRecipientName(String recipientName) {
        _recipientName = recipientName;
    }

    public long getTakerId() {
        return _takerId;
    }

    public void setTakerId(long takerId) {
        _takerId = takerId;
    }

    public String getTakerName() {
        return _takerName;
    }

    public void setTakerName(String takerName) {
        _takerName = takerName;
    }

    public int getTypeId() {
        return _typeId;
    }

    public void setTypeId(int typeId) {
        _typeId = typeId;
    }

    public String getCode() {
        return _code;
    }

    public void setCode(String code) {
        _code = code;
    }

    public Date getDateIn() {
        return _dateIn;
    }

    public void setDateIn(Date dateIn) {
        _dateIn = dateIn;
    }

    public Date getDateOut() {
        return _dateOut;
    }

    public void setDateOut(Date dateOut) {
        _dateOut = dateOut;
    }

    public String getRemark() {
        return _remark;
    }

    public void setRemark(String remark) {
        _remark = remark;
    }

    public long getSignatureId() {
        return _signatureId;
    }

    public void setSignatureId(long signatureId) {
        _signatureId = signatureId;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
