package br.com.atilo.jcondo.manager.service;

public interface EmailProvider {

	public void addGroupMember(String groupName, String memberEmail) throws Exception;

	public void removeGroupMember(String groupName, String memberEmail) throws Exception;

}
