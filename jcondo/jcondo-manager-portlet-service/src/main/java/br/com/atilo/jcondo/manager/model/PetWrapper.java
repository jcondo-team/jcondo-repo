package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Pet}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Pet
 * @generated
 */
public class PetWrapper implements Pet, ModelWrapper<Pet> {
    private Pet _pet;

    public PetWrapper(Pet pet) {
        _pet = pet;
    }

    @Override
    public Class<?> getModelClass() {
        return Pet.class;
    }

    @Override
    public String getModelClassName() {
        return Pet.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("petId", getPetId());
        attributes.put("typeId", getTypeId());
        attributes.put("flatId", getFlatId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long petId = (Long) attributes.get("petId");

        if (petId != null) {
            setPetId(petId);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Long flatId = (Long) attributes.get("flatId");

        if (flatId != null) {
            setFlatId(flatId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this pet.
    *
    * @return the primary key of this pet
    */
    @Override
    public long getPrimaryKey() {
        return _pet.getPrimaryKey();
    }

    /**
    * Sets the primary key of this pet.
    *
    * @param primaryKey the primary key of this pet
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _pet.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the pet ID of this pet.
    *
    * @return the pet ID of this pet
    */
    @Override
    public long getPetId() {
        return _pet.getPetId();
    }

    /**
    * Sets the pet ID of this pet.
    *
    * @param petId the pet ID of this pet
    */
    @Override
    public void setPetId(long petId) {
        _pet.setPetId(petId);
    }

    /**
    * Returns the type ID of this pet.
    *
    * @return the type ID of this pet
    */
    @Override
    public int getTypeId() {
        return _pet.getTypeId();
    }

    /**
    * Sets the type ID of this pet.
    *
    * @param typeId the type ID of this pet
    */
    @Override
    public void setTypeId(int typeId) {
        _pet.setTypeId(typeId);
    }

    /**
    * Returns the flat ID of this pet.
    *
    * @return the flat ID of this pet
    */
    @Override
    public long getFlatId() {
        return _pet.getFlatId();
    }

    /**
    * Sets the flat ID of this pet.
    *
    * @param flatId the flat ID of this pet
    */
    @Override
    public void setFlatId(long flatId) {
        _pet.setFlatId(flatId);
    }

    /**
    * Returns the opr date of this pet.
    *
    * @return the opr date of this pet
    */
    @Override
    public java.util.Date getOprDate() {
        return _pet.getOprDate();
    }

    /**
    * Sets the opr date of this pet.
    *
    * @param oprDate the opr date of this pet
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _pet.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this pet.
    *
    * @return the opr user of this pet
    */
    @Override
    public long getOprUser() {
        return _pet.getOprUser();
    }

    /**
    * Sets the opr user of this pet.
    *
    * @param oprUser the opr user of this pet
    */
    @Override
    public void setOprUser(long oprUser) {
        _pet.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _pet.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _pet.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _pet.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _pet.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _pet.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _pet.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _pet.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _pet.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _pet.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _pet.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _pet.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PetWrapper((Pet) _pet.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Pet pet) {
        return _pet.compareTo(pet);
    }

    @Override
    public int hashCode() {
        return _pet.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Pet> toCacheModel() {
        return _pet.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Pet toEscapedModel() {
        return new PetWrapper(_pet.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Pet toUnescapedModel() {
        return new PetWrapper(_pet.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _pet.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _pet.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _pet.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PetWrapper)) {
            return false;
        }

        PetWrapper petWrapper = (PetWrapper) obj;

        if (Validator.equals(_pet, petWrapper._pet)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Pet getWrappedPet() {
        return _pet;
    }

    @Override
    public Pet getWrappedModel() {
        return _pet;
    }

    @Override
    public void resetOriginalValues() {
        _pet.resetOriginalValues();
    }
}
