package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Flat;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the flat service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see FlatPersistenceImpl
 * @see FlatUtil
 * @generated
 */
public interface FlatPersistence extends BasePersistence<Flat> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link FlatUtil} to access the flat persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the flats where personId = &#63;.
    *
    * @param personId the person ID
    * @return the matching flats
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> findByPerson(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the flats where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of flats
    * @param end the upper bound of the range of flats (not inclusive)
    * @return the range of matching flats
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> findByPerson(
        long personId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the flats where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of flats
    * @param end the upper bound of the range of flats (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching flats
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> findByPerson(
        long personId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first flat in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching flat
    * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a matching flat could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat findByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchFlatException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first flat in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching flat, or <code>null</code> if a matching flat could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat fetchByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last flat in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching flat
    * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a matching flat could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat findByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchFlatException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last flat in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching flat, or <code>null</code> if a matching flat could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat fetchByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the flats before and after the current flat in the ordered set where personId = &#63;.
    *
    * @param id the primary key of the current flat
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next flat
    * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a flat with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat[] findByPerson_PrevAndNext(
        long id, long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchFlatException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the flats where personId = &#63; from the database.
    *
    * @param personId the person ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of flats where personId = &#63;.
    *
    * @param personId the person ID
    * @return the number of matching flats
    * @throws SystemException if a system exception occurred
    */
    public int countByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the flat where organizationId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchFlatException} if it could not be found.
    *
    * @param organizationId the organization ID
    * @return the matching flat
    * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a matching flat could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat findByOrganization(
        long organizationId)
        throws br.com.atilo.jcondo.manager.NoSuchFlatException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the flat where organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param organizationId the organization ID
    * @return the matching flat, or <code>null</code> if a matching flat could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat fetchByOrganization(
        long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the flat where organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param organizationId the organization ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching flat, or <code>null</code> if a matching flat could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat fetchByOrganization(
        long organizationId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the flat where organizationId = &#63; from the database.
    *
    * @param organizationId the organization ID
    * @return the flat that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat removeByOrganization(
        long organizationId)
        throws br.com.atilo.jcondo.manager.NoSuchFlatException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of flats where organizationId = &#63;.
    *
    * @param organizationId the organization ID
    * @return the number of matching flats
    * @throws SystemException if a system exception occurred
    */
    public int countByOrganization(long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the flat in the entity cache if it is enabled.
    *
    * @param flat the flat
    */
    public void cacheResult(br.com.atilo.jcondo.manager.model.Flat flat);

    /**
    * Caches the flats in the entity cache if it is enabled.
    *
    * @param flats the flats
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Flat> flats);

    /**
    * Creates a new flat with the primary key. Does not add the flat to the database.
    *
    * @param id the primary key for the new flat
    * @return the new flat
    */
    public br.com.atilo.jcondo.manager.model.Flat create(long id);

    /**
    * Removes the flat with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the flat
    * @return the flat that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a flat with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchFlatException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Flat updateImpl(
        br.com.atilo.jcondo.manager.model.Flat flat)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the flat with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchFlatException} if it could not be found.
    *
    * @param id the primary key of the flat
    * @return the flat
    * @throws br.com.atilo.jcondo.manager.NoSuchFlatException if a flat with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat findByPrimaryKey(long id)
        throws br.com.atilo.jcondo.manager.NoSuchFlatException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the flat with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the flat
    * @return the flat, or <code>null</code> if a flat with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Flat fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the flats.
    *
    * @return the flats
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the flats.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of flats
    * @param end the upper bound of the range of flats (not inclusive)
    * @return the range of flats
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the flats.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of flats
    * @param end the upper bound of the range of flats (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of flats
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the flats from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of flats.
    *
    * @return the number of flats
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
