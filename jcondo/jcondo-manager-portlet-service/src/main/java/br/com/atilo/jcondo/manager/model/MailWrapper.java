package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Mail}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Mail
 * @generated
 */
public class MailWrapper implements Mail, ModelWrapper<Mail> {
    private Mail _mail;

    public MailWrapper(Mail mail) {
        _mail = mail;
    }

    @Override
    public Class<?> getModelClass() {
        return Mail.class;
    }

    @Override
    public String getModelClassName() {
        return Mail.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("recipientId", getRecipientId());
        attributes.put("recipientName", getRecipientName());
        attributes.put("takerId", getTakerId());
        attributes.put("takerName", getTakerName());
        attributes.put("typeId", getTypeId());
        attributes.put("code", getCode());
        attributes.put("dateIn", getDateIn());
        attributes.put("dateOut", getDateOut());
        attributes.put("remark", getRemark());
        attributes.put("signatureId", getSignatureId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long recipientId = (Long) attributes.get("recipientId");

        if (recipientId != null) {
            setRecipientId(recipientId);
        }

        String recipientName = (String) attributes.get("recipientName");

        if (recipientName != null) {
            setRecipientName(recipientName);
        }

        Long takerId = (Long) attributes.get("takerId");

        if (takerId != null) {
            setTakerId(takerId);
        }

        String takerName = (String) attributes.get("takerName");

        if (takerName != null) {
            setTakerName(takerName);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        String code = (String) attributes.get("code");

        if (code != null) {
            setCode(code);
        }

        Date dateIn = (Date) attributes.get("dateIn");

        if (dateIn != null) {
            setDateIn(dateIn);
        }

        Date dateOut = (Date) attributes.get("dateOut");

        if (dateOut != null) {
            setDateOut(dateOut);
        }

        String remark = (String) attributes.get("remark");

        if (remark != null) {
            setRemark(remark);
        }

        Long signatureId = (Long) attributes.get("signatureId");

        if (signatureId != null) {
            setSignatureId(signatureId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this mail.
    *
    * @return the primary key of this mail
    */
    @Override
    public long getPrimaryKey() {
        return _mail.getPrimaryKey();
    }

    /**
    * Sets the primary key of this mail.
    *
    * @param primaryKey the primary key of this mail
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _mail.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this mail.
    *
    * @return the ID of this mail
    */
    @Override
    public long getId() {
        return _mail.getId();
    }

    /**
    * Sets the ID of this mail.
    *
    * @param id the ID of this mail
    */
    @Override
    public void setId(long id) {
        _mail.setId(id);
    }

    /**
    * Returns the recipient ID of this mail.
    *
    * @return the recipient ID of this mail
    */
    @Override
    public long getRecipientId() {
        return _mail.getRecipientId();
    }

    /**
    * Sets the recipient ID of this mail.
    *
    * @param recipientId the recipient ID of this mail
    */
    @Override
    public void setRecipientId(long recipientId) {
        _mail.setRecipientId(recipientId);
    }

    /**
    * Returns the recipient name of this mail.
    *
    * @return the recipient name of this mail
    */
    @Override
    public java.lang.String getRecipientName() {
        return _mail.getRecipientName();
    }

    /**
    * Sets the recipient name of this mail.
    *
    * @param recipientName the recipient name of this mail
    */
    @Override
    public void setRecipientName(java.lang.String recipientName) {
        _mail.setRecipientName(recipientName);
    }

    /**
    * Returns the taker ID of this mail.
    *
    * @return the taker ID of this mail
    */
    @Override
    public long getTakerId() {
        return _mail.getTakerId();
    }

    /**
    * Sets the taker ID of this mail.
    *
    * @param takerId the taker ID of this mail
    */
    @Override
    public void setTakerId(long takerId) {
        _mail.setTakerId(takerId);
    }

    /**
    * Returns the taker name of this mail.
    *
    * @return the taker name of this mail
    */
    @Override
    public java.lang.String getTakerName() {
        return _mail.getTakerName();
    }

    /**
    * Sets the taker name of this mail.
    *
    * @param takerName the taker name of this mail
    */
    @Override
    public void setTakerName(java.lang.String takerName) {
        _mail.setTakerName(takerName);
    }

    /**
    * Returns the type ID of this mail.
    *
    * @return the type ID of this mail
    */
    @Override
    public int getTypeId() {
        return _mail.getTypeId();
    }

    /**
    * Sets the type ID of this mail.
    *
    * @param typeId the type ID of this mail
    */
    @Override
    public void setTypeId(int typeId) {
        _mail.setTypeId(typeId);
    }

    /**
    * Returns the code of this mail.
    *
    * @return the code of this mail
    */
    @Override
    public java.lang.String getCode() {
        return _mail.getCode();
    }

    /**
    * Sets the code of this mail.
    *
    * @param code the code of this mail
    */
    @Override
    public void setCode(java.lang.String code) {
        _mail.setCode(code);
    }

    /**
    * Returns the date in of this mail.
    *
    * @return the date in of this mail
    */
    @Override
    public java.util.Date getDateIn() {
        return _mail.getDateIn();
    }

    /**
    * Sets the date in of this mail.
    *
    * @param dateIn the date in of this mail
    */
    @Override
    public void setDateIn(java.util.Date dateIn) {
        _mail.setDateIn(dateIn);
    }

    /**
    * Returns the date out of this mail.
    *
    * @return the date out of this mail
    */
    @Override
    public java.util.Date getDateOut() {
        return _mail.getDateOut();
    }

    /**
    * Sets the date out of this mail.
    *
    * @param dateOut the date out of this mail
    */
    @Override
    public void setDateOut(java.util.Date dateOut) {
        _mail.setDateOut(dateOut);
    }

    /**
    * Returns the remark of this mail.
    *
    * @return the remark of this mail
    */
    @Override
    public java.lang.String getRemark() {
        return _mail.getRemark();
    }

    /**
    * Sets the remark of this mail.
    *
    * @param remark the remark of this mail
    */
    @Override
    public void setRemark(java.lang.String remark) {
        _mail.setRemark(remark);
    }

    /**
    * Returns the signature ID of this mail.
    *
    * @return the signature ID of this mail
    */
    @Override
    public long getSignatureId() {
        return _mail.getSignatureId();
    }

    /**
    * Sets the signature ID of this mail.
    *
    * @param signatureId the signature ID of this mail
    */
    @Override
    public void setSignatureId(long signatureId) {
        _mail.setSignatureId(signatureId);
    }

    /**
    * Returns the opr date of this mail.
    *
    * @return the opr date of this mail
    */
    @Override
    public java.util.Date getOprDate() {
        return _mail.getOprDate();
    }

    /**
    * Sets the opr date of this mail.
    *
    * @param oprDate the opr date of this mail
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _mail.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this mail.
    *
    * @return the opr user of this mail
    */
    @Override
    public long getOprUser() {
        return _mail.getOprUser();
    }

    /**
    * Sets the opr user of this mail.
    *
    * @param oprUser the opr user of this mail
    */
    @Override
    public void setOprUser(long oprUser) {
        _mail.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _mail.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _mail.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _mail.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _mail.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _mail.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _mail.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _mail.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _mail.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _mail.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _mail.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _mail.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new MailWrapper((Mail) _mail.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Mail mail) {
        return _mail.compareTo(mail);
    }

    @Override
    public int hashCode() {
        return _mail.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Mail> toCacheModel() {
        return _mail.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Mail toEscapedModel() {
        return new MailWrapper(_mail.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Mail toUnescapedModel() {
        return new MailWrapper(_mail.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _mail.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _mail.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _mail.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MailWrapper)) {
            return false;
        }

        MailWrapper mailWrapper = (MailWrapper) obj;

        if (Validator.equals(_mail, mailWrapper._mail)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Mail getWrappedMail() {
        return _mail;
    }

    @Override
    public Mail getWrappedModel() {
        return _mail;
    }

    @Override
    public void resetOriginalValues() {
        _mail.resetOriginalValues();
    }
}
