package br.com.atilo.jcondo.manager.security;

public enum Role {

	ADMIN_MANAGER("Administration Manager"),
	ADMIN_ASSISTANT("Administration Assistant"),
	ADMIN_MEMBER("Administration Member"),

	SECURITY_ASSISTANT("Security Assistant"),

	SUPPLIER_MANAGER("Supplier Manager"),
	SUPPLIER_MEMBER("Supplier Member"),

	FLAT_MANAGER("Flat Manager"),
	FLAT_ASSISTANT("Flat Assistant"),
	FLAT_MEMBER("Flat Member"),

	CONDOMINIUM_MEMBER("Condominium Member"),
	
	DEPARTMENT_MEMBER("Department Member");

	private String label;

	private Role(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static Role parse(String label) {
		for (Role roleName : values()) {
			if (roleName.getLabel().equalsIgnoreCase(label)) {
				return roleName;
			}
		}

		return null;
	}
	
}
