package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Mail;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the mail service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MailPersistenceImpl
 * @see MailUtil
 * @generated
 */
public interface MailPersistence extends BasePersistence<Mail> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link MailUtil} to access the mail persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the mail in the entity cache if it is enabled.
    *
    * @param mail the mail
    */
    public void cacheResult(br.com.atilo.jcondo.manager.model.Mail mail);

    /**
    * Caches the mails in the entity cache if it is enabled.
    *
    * @param mails the mails
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Mail> mails);

    /**
    * Creates a new mail with the primary key. Does not add the mail to the database.
    *
    * @param id the primary key for the new mail
    * @return the new mail
    */
    public br.com.atilo.jcondo.manager.model.Mail create(long id);

    /**
    * Removes the mail with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the mail
    * @return the mail that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchMailException if a mail with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Mail remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchMailException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Mail updateImpl(
        br.com.atilo.jcondo.manager.model.Mail mail)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the mail with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchMailException} if it could not be found.
    *
    * @param id the primary key of the mail
    * @return the mail
    * @throws br.com.atilo.jcondo.manager.NoSuchMailException if a mail with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Mail findByPrimaryKey(long id)
        throws br.com.atilo.jcondo.manager.NoSuchMailException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the mail with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the mail
    * @return the mail, or <code>null</code> if a mail with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Mail fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the mails.
    *
    * @return the mails
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Mail> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the mails.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of mails
    * @param end the upper bound of the range of mails (not inclusive)
    * @return the range of mails
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Mail> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the mails.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MailModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of mails
    * @param end the upper bound of the range of mails (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of mails
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Mail> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the mails from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of mails.
    *
    * @return the number of mails
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
