package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class PersonActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PersonActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PersonLocalServiceUtil.getService());
        setClass(Person.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
