package br.com.atilo.jcondo.manager.service.permission;

import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.service.permission.OrganizationPermissionUtil;

public class DomainPermission extends BasePermission {

	public static boolean hasDomainPermission(Permission permission, long domainId) throws PortalException, SystemException {
		Organization organization = OrganizationLocalServiceUtil.getOrganization(getOrganizationDomain(domainId));
		return hasPermission(permission, Organization.class, organization.getOrganizationId(), organization);
	}

	public static boolean hasDomainPermission(long personId, Permission permission, long domainId) throws Exception {
		PermissionChecker checker = PermissionCheckerFactoryUtil.create(UserLocalServiceUtil.getUser(PersonLocalServiceUtil.getPerson(personId).getUserId()));
		return OrganizationPermissionUtil.contains(checker, getOrganizationDomain(domainId), permission.name());
	}
}
