package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Flat service. Represents a row in the &quot;jco_flat&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see FlatModel
 * @see br.com.atilo.jcondo.manager.model.impl.FlatImpl
 * @see br.com.atilo.jcondo.manager.model.impl.FlatModelImpl
 * @generated
 */
public interface Flat extends FlatModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.FlatImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public java.lang.String getName()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<br.com.atilo.jcondo.datatype.PetType> getPetTypes()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getPeople()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;
}
