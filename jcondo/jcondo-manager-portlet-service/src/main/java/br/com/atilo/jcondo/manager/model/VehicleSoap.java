package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.VehicleServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.VehicleServiceSoap
 * @generated
 */
public class VehicleSoap implements Serializable {
    private long _id;
    private long _domainId;
    private long _imageId;
    private int _typeId;
    private String _license;
    private String _brand;
    private String _color;
    private String _remark;
    private Date _oprDate;
    private long _oprUser;

    public VehicleSoap() {
    }

    public static VehicleSoap toSoapModel(Vehicle model) {
        VehicleSoap soapModel = new VehicleSoap();

        soapModel.setId(model.getId());
        soapModel.setDomainId(model.getDomainId());
        soapModel.setImageId(model.getImageId());
        soapModel.setTypeId(model.getTypeId());
        soapModel.setLicense(model.getLicense());
        soapModel.setBrand(model.getBrand());
        soapModel.setColor(model.getColor());
        soapModel.setRemark(model.getRemark());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static VehicleSoap[] toSoapModels(Vehicle[] models) {
        VehicleSoap[] soapModels = new VehicleSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static VehicleSoap[][] toSoapModels(Vehicle[][] models) {
        VehicleSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new VehicleSoap[models.length][models[0].length];
        } else {
            soapModels = new VehicleSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static VehicleSoap[] toSoapModels(List<Vehicle> models) {
        List<VehicleSoap> soapModels = new ArrayList<VehicleSoap>(models.size());

        for (Vehicle model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new VehicleSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getDomainId() {
        return _domainId;
    }

    public void setDomainId(long domainId) {
        _domainId = domainId;
    }

    public long getImageId() {
        return _imageId;
    }

    public void setImageId(long imageId) {
        _imageId = imageId;
    }

    public int getTypeId() {
        return _typeId;
    }

    public void setTypeId(int typeId) {
        _typeId = typeId;
    }

    public String getLicense() {
        return _license;
    }

    public void setLicense(String license) {
        _license = license;
    }

    public String getBrand() {
        return _brand;
    }

    public void setBrand(String brand) {
        _brand = brand;
    }

    public String getColor() {
        return _color;
    }

    public void setColor(String color) {
        _color = color;
    }

    public String getRemark() {
        return _remark;
    }

    public void setRemark(String remark) {
        _remark = remark;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
