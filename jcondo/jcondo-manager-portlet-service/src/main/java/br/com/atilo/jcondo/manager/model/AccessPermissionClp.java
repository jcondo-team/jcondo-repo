package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.AccessPermissionLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class AccessPermissionClp extends BaseModelImpl<AccessPermission>
    implements AccessPermission {
    private long _id;
    private long _personId;
    private long _domainId;
    private int _weekDay;
    private String _beginTime;
    private String _endTime;
    private Date _beginDate;
    private Date _endDate;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _accessPermissionRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public AccessPermissionClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return AccessPermission.class;
    }

    @Override
    public String getModelClassName() {
        return AccessPermission.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("personId", getPersonId());
        attributes.put("domainId", getDomainId());
        attributes.put("weekDay", getWeekDay());
        attributes.put("beginTime", getBeginTime());
        attributes.put("endTime", getEndTime());
        attributes.put("beginDate", getBeginDate());
        attributes.put("endDate", getEndDate());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        Integer weekDay = (Integer) attributes.get("weekDay");

        if (weekDay != null) {
            setWeekDay(weekDay);
        }

        String beginTime = (String) attributes.get("beginTime");

        if (beginTime != null) {
            setBeginTime(beginTime);
        }

        String endTime = (String) attributes.get("endTime");

        if (endTime != null) {
            setEndTime(endTime);
        }

        Date beginDate = (Date) attributes.get("beginDate");

        if (beginDate != null) {
            setBeginDate(beginDate);
        }

        Date endDate = (Date) attributes.get("endDate");

        if (endDate != null) {
            setEndDate(endDate);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_accessPermissionRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getPersonId() {
        return _personId;
    }

    @Override
    public void setPersonId(long personId) {
        _personId = personId;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setPersonId", long.class);

                method.invoke(_accessPermissionRemoteModel, personId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getDomainId() {
        return _domainId;
    }

    @Override
    public void setDomainId(long domainId) {
        _domainId = domainId;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setDomainId", long.class);

                method.invoke(_accessPermissionRemoteModel, domainId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getWeekDay() {
        return _weekDay;
    }

    @Override
    public void setWeekDay(int weekDay) {
        _weekDay = weekDay;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setWeekDay", int.class);

                method.invoke(_accessPermissionRemoteModel, weekDay);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getBeginTime() {
        return _beginTime;
    }

    @Override
    public void setBeginTime(String beginTime) {
        _beginTime = beginTime;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setBeginTime", String.class);

                method.invoke(_accessPermissionRemoteModel, beginTime);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getEndTime() {
        return _endTime;
    }

    @Override
    public void setEndTime(String endTime) {
        _endTime = endTime;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setEndTime", String.class);

                method.invoke(_accessPermissionRemoteModel, endTime);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getBeginDate() {
        return _beginDate;
    }

    @Override
    public void setBeginDate(Date beginDate) {
        _beginDate = beginDate;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setBeginDate", Date.class);

                method.invoke(_accessPermissionRemoteModel, beginDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getEndDate() {
        return _endDate;
    }

    @Override
    public void setEndDate(Date endDate) {
        _endDate = endDate;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setEndDate", Date.class);

                method.invoke(_accessPermissionRemoteModel, endDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_accessPermissionRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_accessPermissionRemoteModel != null) {
            try {
                Class<?> clazz = _accessPermissionRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_accessPermissionRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getAccessPermissionRemoteModel() {
        return _accessPermissionRemoteModel;
    }

    public void setAccessPermissionRemoteModel(
        BaseModel<?> accessPermissionRemoteModel) {
        _accessPermissionRemoteModel = accessPermissionRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _accessPermissionRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_accessPermissionRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            AccessPermissionLocalServiceUtil.addAccessPermission(this);
        } else {
            AccessPermissionLocalServiceUtil.updateAccessPermission(this);
        }
    }

    @Override
    public AccessPermission toEscapedModel() {
        return (AccessPermission) ProxyUtil.newProxyInstance(AccessPermission.class.getClassLoader(),
            new Class[] { AccessPermission.class },
            new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        AccessPermissionClp clone = new AccessPermissionClp();

        clone.setId(getId());
        clone.setPersonId(getPersonId());
        clone.setDomainId(getDomainId());
        clone.setWeekDay(getWeekDay());
        clone.setBeginTime(getBeginTime());
        clone.setEndTime(getEndTime());
        clone.setBeginDate(getBeginDate());
        clone.setEndDate(getEndDate());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(AccessPermission accessPermission) {
        long primaryKey = accessPermission.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AccessPermissionClp)) {
            return false;
        }

        AccessPermissionClp accessPermission = (AccessPermissionClp) obj;

        long primaryKey = accessPermission.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", personId=");
        sb.append(getPersonId());
        sb.append(", domainId=");
        sb.append(getDomainId());
        sb.append(", weekDay=");
        sb.append(getWeekDay());
        sb.append(", beginTime=");
        sb.append(getBeginTime());
        sb.append(", endTime=");
        sb.append(getEndTime());
        sb.append(", beginDate=");
        sb.append(getBeginDate());
        sb.append(", endDate=");
        sb.append(getEndDate());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(34);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.AccessPermission");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>personId</column-name><column-value><![CDATA[");
        sb.append(getPersonId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>domainId</column-name><column-value><![CDATA[");
        sb.append(getDomainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>weekDay</column-name><column-value><![CDATA[");
        sb.append(getWeekDay());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>beginTime</column-name><column-value><![CDATA[");
        sb.append(getBeginTime());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>endTime</column-name><column-value><![CDATA[");
        sb.append(getEndTime());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>beginDate</column-name><column-value><![CDATA[");
        sb.append(getBeginDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>endDate</column-name><column-value><![CDATA[");
        sb.append(getEndDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
