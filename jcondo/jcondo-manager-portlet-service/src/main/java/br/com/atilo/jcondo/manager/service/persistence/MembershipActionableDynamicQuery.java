package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.service.MembershipLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class MembershipActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public MembershipActionableDynamicQuery() throws SystemException {
        setBaseLocalService(MembershipLocalServiceUtil.getService());
        setClass(Membership.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
