package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Access}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Access
 * @generated
 */
public class AccessWrapper implements Access, ModelWrapper<Access> {
    private Access _access;

    public AccessWrapper(Access access) {
        _access = access;
    }

    @Override
    public Class<?> getModelClass() {
        return Access.class;
    }

    @Override
    public String getModelClassName() {
        return Access.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("destinationId", getDestinationId());
        attributes.put("visitorId", getVisitorId());
        attributes.put("vehicleId", getVehicleId());
        attributes.put("authorizerId", getAuthorizerId());
        attributes.put("authorizerName", getAuthorizerName());
        attributes.put("badge", getBadge());
        attributes.put("ended", getEnded());
        attributes.put("remark", getRemark());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long destinationId = (Long) attributes.get("destinationId");

        if (destinationId != null) {
            setDestinationId(destinationId);
        }

        Long visitorId = (Long) attributes.get("visitorId");

        if (visitorId != null) {
            setVisitorId(visitorId);
        }

        Long vehicleId = (Long) attributes.get("vehicleId");

        if (vehicleId != null) {
            setVehicleId(vehicleId);
        }

        Long authorizerId = (Long) attributes.get("authorizerId");

        if (authorizerId != null) {
            setAuthorizerId(authorizerId);
        }

        String authorizerName = (String) attributes.get("authorizerName");

        if (authorizerName != null) {
            setAuthorizerName(authorizerName);
        }

        String badge = (String) attributes.get("badge");

        if (badge != null) {
            setBadge(badge);
        }

        Boolean ended = (Boolean) attributes.get("ended");

        if (ended != null) {
            setEnded(ended);
        }

        String remark = (String) attributes.get("remark");

        if (remark != null) {
            setRemark(remark);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this access.
    *
    * @return the primary key of this access
    */
    @Override
    public long getPrimaryKey() {
        return _access.getPrimaryKey();
    }

    /**
    * Sets the primary key of this access.
    *
    * @param primaryKey the primary key of this access
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _access.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this access.
    *
    * @return the ID of this access
    */
    @Override
    public long getId() {
        return _access.getId();
    }

    /**
    * Sets the ID of this access.
    *
    * @param id the ID of this access
    */
    @Override
    public void setId(long id) {
        _access.setId(id);
    }

    /**
    * Returns the destination ID of this access.
    *
    * @return the destination ID of this access
    */
    @Override
    public long getDestinationId() {
        return _access.getDestinationId();
    }

    /**
    * Sets the destination ID of this access.
    *
    * @param destinationId the destination ID of this access
    */
    @Override
    public void setDestinationId(long destinationId) {
        _access.setDestinationId(destinationId);
    }

    /**
    * Returns the visitor ID of this access.
    *
    * @return the visitor ID of this access
    */
    @Override
    public long getVisitorId() {
        return _access.getVisitorId();
    }

    /**
    * Sets the visitor ID of this access.
    *
    * @param visitorId the visitor ID of this access
    */
    @Override
    public void setVisitorId(long visitorId) {
        _access.setVisitorId(visitorId);
    }

    /**
    * Returns the vehicle ID of this access.
    *
    * @return the vehicle ID of this access
    */
    @Override
    public long getVehicleId() {
        return _access.getVehicleId();
    }

    /**
    * Sets the vehicle ID of this access.
    *
    * @param vehicleId the vehicle ID of this access
    */
    @Override
    public void setVehicleId(long vehicleId) {
        _access.setVehicleId(vehicleId);
    }

    /**
    * Returns the authorizer ID of this access.
    *
    * @return the authorizer ID of this access
    */
    @Override
    public long getAuthorizerId() {
        return _access.getAuthorizerId();
    }

    /**
    * Sets the authorizer ID of this access.
    *
    * @param authorizerId the authorizer ID of this access
    */
    @Override
    public void setAuthorizerId(long authorizerId) {
        _access.setAuthorizerId(authorizerId);
    }

    /**
    * Returns the authorizer name of this access.
    *
    * @return the authorizer name of this access
    */
    @Override
    public java.lang.String getAuthorizerName() {
        return _access.getAuthorizerName();
    }

    /**
    * Sets the authorizer name of this access.
    *
    * @param authorizerName the authorizer name of this access
    */
    @Override
    public void setAuthorizerName(java.lang.String authorizerName) {
        _access.setAuthorizerName(authorizerName);
    }

    /**
    * Returns the badge of this access.
    *
    * @return the badge of this access
    */
    @Override
    public java.lang.String getBadge() {
        return _access.getBadge();
    }

    /**
    * Sets the badge of this access.
    *
    * @param badge the badge of this access
    */
    @Override
    public void setBadge(java.lang.String badge) {
        _access.setBadge(badge);
    }

    /**
    * Returns the ended of this access.
    *
    * @return the ended of this access
    */
    @Override
    public boolean getEnded() {
        return _access.getEnded();
    }

    /**
    * Returns <code>true</code> if this access is ended.
    *
    * @return <code>true</code> if this access is ended; <code>false</code> otherwise
    */
    @Override
    public boolean isEnded() {
        return _access.isEnded();
    }

    /**
    * Sets whether this access is ended.
    *
    * @param ended the ended of this access
    */
    @Override
    public void setEnded(boolean ended) {
        _access.setEnded(ended);
    }

    /**
    * Returns the remark of this access.
    *
    * @return the remark of this access
    */
    @Override
    public java.lang.String getRemark() {
        return _access.getRemark();
    }

    /**
    * Sets the remark of this access.
    *
    * @param remark the remark of this access
    */
    @Override
    public void setRemark(java.lang.String remark) {
        _access.setRemark(remark);
    }

    /**
    * Returns the opr date of this access.
    *
    * @return the opr date of this access
    */
    @Override
    public java.util.Date getOprDate() {
        return _access.getOprDate();
    }

    /**
    * Sets the opr date of this access.
    *
    * @param oprDate the opr date of this access
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _access.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this access.
    *
    * @return the opr user of this access
    */
    @Override
    public long getOprUser() {
        return _access.getOprUser();
    }

    /**
    * Sets the opr user of this access.
    *
    * @param oprUser the opr user of this access
    */
    @Override
    public void setOprUser(long oprUser) {
        _access.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _access.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _access.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _access.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _access.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _access.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _access.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _access.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _access.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _access.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _access.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _access.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new AccessWrapper((Access) _access.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Access access) {
        return _access.compareTo(access);
    }

    @Override
    public int hashCode() {
        return _access.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Access> toCacheModel() {
        return _access.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Access toEscapedModel() {
        return new AccessWrapper(_access.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Access toUnescapedModel() {
        return new AccessWrapper(_access.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _access.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _access.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _access.persist();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle getVehicle()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _access.getVehicle();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AccessWrapper)) {
            return false;
        }

        AccessWrapper accessWrapper = (AccessWrapper) obj;

        if (Validator.equals(_access, accessWrapper._access)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Access getWrappedAccess() {
        return _access;
    }

    @Override
    public Access getWrappedModel() {
        return _access;
    }

    @Override
    public void resetOriginalValues() {
        _access.resetOriginalValues();
    }
}
