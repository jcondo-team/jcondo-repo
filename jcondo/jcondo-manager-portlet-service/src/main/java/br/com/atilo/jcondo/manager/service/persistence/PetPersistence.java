package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Pet;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the pet service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PetPersistenceImpl
 * @see PetUtil
 * @generated
 */
public interface PetPersistence extends BasePersistence<Pet> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link PetUtil} to access the pet persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the pets where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @return the matching pets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Pet> findByFlat(
        long flatId) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the pets where flatId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param flatId the flat ID
    * @param start the lower bound of the range of pets
    * @param end the upper bound of the range of pets (not inclusive)
    * @return the range of matching pets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Pet> findByFlat(
        long flatId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the pets where flatId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param flatId the flat ID
    * @param start the lower bound of the range of pets
    * @param end the upper bound of the range of pets (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching pets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Pet> findByFlat(
        long flatId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first pet in the ordered set where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet findByFlat_First(long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first pet in the ordered set where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching pet, or <code>null</code> if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet fetchByFlat_First(
        long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last pet in the ordered set where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet findByFlat_Last(long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last pet in the ordered set where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching pet, or <code>null</code> if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet fetchByFlat_Last(long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the pets before and after the current pet in the ordered set where flatId = &#63;.
    *
    * @param petId the primary key of the current pet
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet[] findByFlat_PrevAndNext(
        long petId, long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the pets where flatId = &#63; from the database.
    *
    * @param flatId the flat ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByFlat(long flatId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of pets where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @return the number of matching pets
    * @throws SystemException if a system exception occurred
    */
    public int countByFlat(long flatId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the pet where typeId = &#63; and flatId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchPetException} if it could not be found.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @return the matching pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet findByTypeAndFlat(int typeId,
        long flatId)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the pet where typeId = &#63; and flatId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @return the matching pet, or <code>null</code> if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet fetchByTypeAndFlat(
        int typeId, long flatId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the pet where typeId = &#63; and flatId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching pet, or <code>null</code> if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet fetchByTypeAndFlat(
        int typeId, long flatId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the pet where typeId = &#63; and flatId = &#63; from the database.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @return the pet that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet removeByTypeAndFlat(
        int typeId, long flatId)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of pets where typeId = &#63; and flatId = &#63;.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @return the number of matching pets
    * @throws SystemException if a system exception occurred
    */
    public int countByTypeAndFlat(int typeId, long flatId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the pet in the entity cache if it is enabled.
    *
    * @param pet the pet
    */
    public void cacheResult(br.com.atilo.jcondo.manager.model.Pet pet);

    /**
    * Caches the pets in the entity cache if it is enabled.
    *
    * @param pets the pets
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Pet> pets);

    /**
    * Creates a new pet with the primary key. Does not add the pet to the database.
    *
    * @param petId the primary key for the new pet
    * @return the new pet
    */
    public br.com.atilo.jcondo.manager.model.Pet create(long petId);

    /**
    * Removes the pet with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param petId the primary key of the pet
    * @return the pet that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet remove(long petId)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Pet updateImpl(
        br.com.atilo.jcondo.manager.model.Pet pet)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the pet with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchPetException} if it could not be found.
    *
    * @param petId the primary key of the pet
    * @return the pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet findByPrimaryKey(long petId)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the pet with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param petId the primary key of the pet
    * @return the pet, or <code>null</code> if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Pet fetchByPrimaryKey(long petId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the pets.
    *
    * @return the pets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Pet> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the pets.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of pets
    * @param end the upper bound of the range of pets (not inclusive)
    * @return the range of pets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Pet> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the pets.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of pets
    * @param end the upper bound of the range of pets (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of pets
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Pet> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the pets from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of pets.
    *
    * @return the number of pets
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
