package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Flat. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.FlatLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see FlatLocalService
 * @see br.com.atilo.jcondo.manager.service.base.FlatLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.FlatLocalServiceImpl
 * @generated
 */
public class FlatLocalServiceUtil {
    private static FlatLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.FlatLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the flat to the database. Also notifies the appropriate model listeners.
    *
    * @param flat the flat
    * @return the flat that was added
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Flat addFlat(
        br.com.atilo.jcondo.manager.model.Flat flat)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addFlat(flat);
    }

    /**
    * Creates a new flat with the primary key. Does not add the flat to the database.
    *
    * @param id the primary key for the new flat
    * @return the new flat
    */
    public static br.com.atilo.jcondo.manager.model.Flat createFlat(long id) {
        return getService().createFlat(id);
    }

    /**
    * Deletes the flat with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the flat
    * @return the flat that was removed
    * @throws PortalException if a flat with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Flat deleteFlat(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteFlat(id);
    }

    /**
    * Deletes the flat from the database. Also notifies the appropriate model listeners.
    *
    * @param flat the flat
    * @return the flat that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Flat deleteFlat(
        br.com.atilo.jcondo.manager.model.Flat flat)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteFlat(flat);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static br.com.atilo.jcondo.manager.model.Flat fetchFlat(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchFlat(id);
    }

    /**
    * Returns the flat with the primary key.
    *
    * @param id the primary key of the flat
    * @return the flat
    * @throws PortalException if a flat with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Flat getFlat(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getFlat(id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the flats.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of flats
    * @param end the upper bound of the range of flats (not inclusive)
    * @return the range of flats
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Flat> getFlats(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getFlats(start, end);
    }

    /**
    * Returns the number of flats.
    *
    * @return the number of flats
    * @throws SystemException if a system exception occurred
    */
    public static int getFlatsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getFlatsCount();
    }

    /**
    * Updates the flat in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param flat the flat
    * @return the flat that was updated
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Flat updateFlat(
        br.com.atilo.jcondo.manager.model.Flat flat)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateFlat(flat);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Flat addFlat(int number,
        int block, boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addFlat(number, block, disables, brigade, pets, petTypes);
    }

    public static br.com.atilo.jcondo.manager.model.Flat updateFlat(long id,
        boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateFlat(id, disables, brigade, pets, petTypes);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Flat> getPersonFlats(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonFlats(personId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Flat> getFlats()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getFlats();
    }

    public static br.com.atilo.jcondo.manager.model.Flat setFlatPerson(
        long flatId, long personId)
        throws br.com.atilo.jcondo.manager.NoSuchFlatException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().setFlatPerson(flatId, personId);
    }

    public static void setFlatDefaulting(long id, boolean isDefaulting)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService().setFlatDefaulting(id, isDefaulting);
    }

    public static void clearService() {
        _service = null;
    }

    public static FlatLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    FlatLocalService.class.getName());

            if (invokableLocalService instanceof FlatLocalService) {
                _service = (FlatLocalService) invokableLocalService;
            } else {
                _service = new FlatLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(FlatLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(FlatLocalService service) {
    }
}
