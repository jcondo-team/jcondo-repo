package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.FlatServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.FlatServiceSoap
 * @generated
 */
public class FlatSoap implements Serializable {
    private long _id;
    private long _organizationId;
    private long _folderId;
    private long _personId;
    private int _block;
    private int _number;
    private boolean _pets;
    private boolean _disables;
    private boolean _brigade;
    private boolean _defaulting;
    private Date _oprDate;
    private long _oprUser;

    public FlatSoap() {
    }

    public static FlatSoap toSoapModel(Flat model) {
        FlatSoap soapModel = new FlatSoap();

        soapModel.setId(model.getId());
        soapModel.setOrganizationId(model.getOrganizationId());
        soapModel.setFolderId(model.getFolderId());
        soapModel.setPersonId(model.getPersonId());
        soapModel.setBlock(model.getBlock());
        soapModel.setNumber(model.getNumber());
        soapModel.setPets(model.getPets());
        soapModel.setDisables(model.getDisables());
        soapModel.setBrigade(model.getBrigade());
        soapModel.setDefaulting(model.getDefaulting());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static FlatSoap[] toSoapModels(Flat[] models) {
        FlatSoap[] soapModels = new FlatSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static FlatSoap[][] toSoapModels(Flat[][] models) {
        FlatSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new FlatSoap[models.length][models[0].length];
        } else {
            soapModels = new FlatSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static FlatSoap[] toSoapModels(List<Flat> models) {
        List<FlatSoap> soapModels = new ArrayList<FlatSoap>(models.size());

        for (Flat model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new FlatSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getOrganizationId() {
        return _organizationId;
    }

    public void setOrganizationId(long organizationId) {
        _organizationId = organizationId;
    }

    public long getFolderId() {
        return _folderId;
    }

    public void setFolderId(long folderId) {
        _folderId = folderId;
    }

    public long getPersonId() {
        return _personId;
    }

    public void setPersonId(long personId) {
        _personId = personId;
    }

    public int getBlock() {
        return _block;
    }

    public void setBlock(int block) {
        _block = block;
    }

    public int getNumber() {
        return _number;
    }

    public void setNumber(int number) {
        _number = number;
    }

    public boolean getPets() {
        return _pets;
    }

    public boolean isPets() {
        return _pets;
    }

    public void setPets(boolean pets) {
        _pets = pets;
    }

    public boolean getDisables() {
        return _disables;
    }

    public boolean isDisables() {
        return _disables;
    }

    public void setDisables(boolean disables) {
        _disables = disables;
    }

    public boolean getBrigade() {
        return _brigade;
    }

    public boolean isBrigade() {
        return _brigade;
    }

    public void setBrigade(boolean brigade) {
        _brigade = brigade;
    }

    public boolean getDefaulting() {
        return _defaulting;
    }

    public boolean isDefaulting() {
        return _defaulting;
    }

    public void setDefaulting(boolean defaulting) {
        _defaulting = defaulting;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
