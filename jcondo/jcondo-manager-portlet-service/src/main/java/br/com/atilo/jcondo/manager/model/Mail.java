package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Mail service. Represents a row in the &quot;jco_mail&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see MailModel
 * @see br.com.atilo.jcondo.manager.model.impl.MailImpl
 * @see br.com.atilo.jcondo.manager.model.impl.MailModelImpl
 * @generated
 */
public interface Mail extends MailModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.MailImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
