package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Parking. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.ParkingLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see ParkingLocalService
 * @see br.com.atilo.jcondo.manager.service.base.ParkingLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.ParkingLocalServiceImpl
 * @generated
 */
public class ParkingLocalServiceUtil {
    private static ParkingLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.ParkingLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the parking to the database. Also notifies the appropriate model listeners.
    *
    * @param parking the parking
    * @return the parking that was added
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking addParking(
        br.com.atilo.jcondo.manager.model.Parking parking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addParking(parking);
    }

    /**
    * Creates a new parking with the primary key. Does not add the parking to the database.
    *
    * @param id the primary key for the new parking
    * @return the new parking
    */
    public static br.com.atilo.jcondo.manager.model.Parking createParking(
        long id) {
        return getService().createParking(id);
    }

    /**
    * Deletes the parking with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the parking
    * @return the parking that was removed
    * @throws PortalException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking deleteParking(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteParking(id);
    }

    /**
    * Deletes the parking from the database. Also notifies the appropriate model listeners.
    *
    * @param parking the parking
    * @return the parking that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking deleteParking(
        br.com.atilo.jcondo.manager.model.Parking parking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteParking(parking);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static br.com.atilo.jcondo.manager.model.Parking fetchParking(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchParking(id);
    }

    /**
    * Returns the parking with the primary key.
    *
    * @param id the primary key of the parking
    * @return the parking
    * @throws PortalException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking getParking(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getParking(id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the parkings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @return the range of parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> getParkings(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getParkings(start, end);
    }

    /**
    * Returns the number of parkings.
    *
    * @return the number of parkings
    * @throws SystemException if a system exception occurred
    */
    public static int getParkingsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getParkingsCount();
    }

    /**
    * Updates the parking in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param parking the parking
    * @return the parking that was updated
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking updateParking(
        br.com.atilo.jcondo.manager.model.Parking parking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateParking(parking);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Parking addParking(
        java.lang.String code, br.com.atilo.jcondo.datatype.ParkingType type,
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addParking(code, type, ownerDomainId);
    }

    public static br.com.atilo.jcondo.manager.model.Parking updateParking(
        long parkingId, java.lang.String code,
        br.com.atilo.jcondo.datatype.ParkingType type, long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateParking(parkingId, code, type, ownerDomainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> getOwnedParkings(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getOwnedParkings(ownerDomainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> getRentedParkings(
        long renterDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getRentedParkings(renterDomainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> getGrantedParkings(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getGrantedParkings(ownerDomainId);
    }

    public static br.com.atilo.jcondo.manager.model.Parking rent(
        long ownerDomainId, long renterDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().rent(ownerDomainId, renterDomainId);
    }

    public static br.com.atilo.jcondo.manager.model.Parking unrent(
        long parkingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().unrent(parkingId);
    }

    public static void checkParkingAvailability(long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService().checkParkingAvailability(domainId);
    }

    public static void clearService() {
        _service = null;
    }

    public static ParkingLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    ParkingLocalService.class.getName());

            if (invokableLocalService instanceof ParkingLocalService) {
                _service = (ParkingLocalService) invokableLocalService;
            } else {
                _service = new ParkingLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(ParkingLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(ParkingLocalService service) {
    }
}
