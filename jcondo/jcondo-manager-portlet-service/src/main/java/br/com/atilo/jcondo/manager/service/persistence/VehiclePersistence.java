package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Vehicle;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the vehicle service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see VehiclePersistenceImpl
 * @see VehicleUtil
 * @generated
 */
public interface VehiclePersistence extends BasePersistence<Vehicle> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link VehicleUtil} to access the vehicle persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns the vehicle where license = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchVehicleException} if it could not be found.
    *
    * @param license the license
    * @return the matching vehicle
    * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle findByLicense(
        java.lang.String license)
        throws br.com.atilo.jcondo.manager.NoSuchVehicleException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle where license = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param license the license
    * @return the matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle fetchByLicense(
        java.lang.String license)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle where license = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param license the license
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle fetchByLicense(
        java.lang.String license, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the vehicle where license = &#63; from the database.
    *
    * @param license the license
    * @return the vehicle that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle removeByLicense(
        java.lang.String license)
        throws br.com.atilo.jcondo.manager.NoSuchVehicleException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where license = &#63;.
    *
    * @param license the license
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByLicense(java.lang.String license)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @return the matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> findByDomain(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles where domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> findByDomain(
        long domainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles where domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> findByDomain(
        long domainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle
    * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle findByDomain_First(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchVehicleException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first vehicle in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle fetchByDomain_First(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle
    * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle findByDomain_Last(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchVehicleException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last vehicle in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching vehicle, or <code>null</code> if a matching vehicle could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle fetchByDomain_Last(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicles before and after the current vehicle in the ordered set where domainId = &#63;.
    *
    * @param id the primary key of the current vehicle
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next vehicle
    * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle[] findByDomain_PrevAndNext(
        long id, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchVehicleException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the vehicles where domainId = &#63; from the database.
    *
    * @param domainId the domain ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByDomain(long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @return the number of matching vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countByDomain(long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the vehicle in the entity cache if it is enabled.
    *
    * @param vehicle the vehicle
    */
    public void cacheResult(br.com.atilo.jcondo.manager.model.Vehicle vehicle);

    /**
    * Caches the vehicles in the entity cache if it is enabled.
    *
    * @param vehicles the vehicles
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> vehicles);

    /**
    * Creates a new vehicle with the primary key. Does not add the vehicle to the database.
    *
    * @param id the primary key for the new vehicle
    * @return the new vehicle
    */
    public br.com.atilo.jcondo.manager.model.Vehicle create(long id);

    /**
    * Removes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the vehicle
    * @return the vehicle that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchVehicleException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Vehicle updateImpl(
        br.com.atilo.jcondo.manager.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchVehicleException} if it could not be found.
    *
    * @param id the primary key of the vehicle
    * @return the vehicle
    * @throws br.com.atilo.jcondo.manager.NoSuchVehicleException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle findByPrimaryKey(long id)
        throws br.com.atilo.jcondo.manager.NoSuchVehicleException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the vehicle with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the vehicle
    * @return the vehicle, or <code>null</code> if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Vehicle fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the vehicles.
    *
    * @return the vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the vehicles.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the vehicles.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of vehicles
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the vehicles from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of vehicles.
    *
    * @return the number of vehicles
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
