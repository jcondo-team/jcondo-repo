package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.PersonServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.PersonServiceSoap
 * @generated
 */
public class PersonSoap implements Serializable {
    private long _id;
    private long _userId;
    private long _domainId;
    private String _identity;
    private String _remark;
    private Date _oprDate;
    private long _oprUser;

    public PersonSoap() {
    }

    public static PersonSoap toSoapModel(Person model) {
        PersonSoap soapModel = new PersonSoap();

        soapModel.setId(model.getId());
        soapModel.setUserId(model.getUserId());
        soapModel.setDomainId(model.getDomainId());
        soapModel.setIdentity(model.getIdentity());
        soapModel.setRemark(model.getRemark());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static PersonSoap[] toSoapModels(Person[] models) {
        PersonSoap[] soapModels = new PersonSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PersonSoap[][] toSoapModels(Person[][] models) {
        PersonSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PersonSoap[models.length][models[0].length];
        } else {
            soapModels = new PersonSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PersonSoap[] toSoapModels(List<Person> models) {
        List<PersonSoap> soapModels = new ArrayList<PersonSoap>(models.size());

        for (Person model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PersonSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getUserId() {
        return _userId;
    }

    public void setUserId(long userId) {
        _userId = userId;
    }

    public long getDomainId() {
        return _domainId;
    }

    public void setDomainId(long domainId) {
        _domainId = domainId;
    }

    public String getIdentity() {
        return _identity;
    }

    public void setIdentity(String identity) {
        _identity = identity;
    }

    public String getRemark() {
        return _remark;
    }

    public void setRemark(String remark) {
        _remark = remark;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
