package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class FlatActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public FlatActionableDynamicQuery() throws SystemException {
        setBaseLocalService(FlatLocalServiceUtil.getService());
        setClass(Flat.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
