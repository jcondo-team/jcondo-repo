package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Department service. Represents a row in the &quot;jco_department&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see DepartmentModel
 * @see br.com.atilo.jcondo.manager.model.impl.DepartmentImpl
 * @see br.com.atilo.jcondo.manager.model.impl.DepartmentModelImpl
 * @generated
 */
public interface Department extends DepartmentModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.DepartmentImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public void setType(br.com.atilo.jcondo.datatype.DomainType type);

    public br.com.atilo.jcondo.datatype.DomainType getType()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setDescription(java.lang.String description);

    public java.lang.String getDescription()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getMembers()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.model.Organization getOrganization();

    public void setOrganization(
        com.liferay.portal.model.Organization organization);
}
