package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Person. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.PersonLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see PersonLocalService
 * @see br.com.atilo.jcondo.manager.service.base.PersonLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.PersonLocalServiceImpl
 * @generated
 */
public class PersonLocalServiceUtil {
    private static PersonLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.PersonLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the person to the database. Also notifies the appropriate model listeners.
    *
    * @param person the person
    * @return the person that was added
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Person addPerson(
        br.com.atilo.jcondo.manager.model.Person person)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addPerson(person);
    }

    /**
    * Creates a new person with the primary key. Does not add the person to the database.
    *
    * @param id the primary key for the new person
    * @return the new person
    */
    public static br.com.atilo.jcondo.manager.model.Person createPerson(long id) {
        return getService().createPerson(id);
    }

    /**
    * Deletes the person with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the person
    * @return the person that was removed
    * @throws PortalException if a person with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Person deletePerson(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePerson(id);
    }

    /**
    * Deletes the person from the database. Also notifies the appropriate model listeners.
    *
    * @param person the person
    * @return the person that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Person deletePerson(
        br.com.atilo.jcondo.manager.model.Person person)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePerson(person);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PersonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PersonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static br.com.atilo.jcondo.manager.model.Person fetchPerson(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchPerson(id);
    }

    /**
    * Returns the person with the primary key.
    *
    * @param id the primary key of the person
    * @return the person
    * @throws PortalException if a person with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Person getPerson(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPerson(id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the persons.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PersonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of persons
    * @param end the upper bound of the range of persons (not inclusive)
    * @return the range of persons
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> getPersons(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersons(start, end);
    }

    /**
    * Returns the number of persons.
    *
    * @return the number of persons
    * @throws SystemException if a system exception occurred
    */
    public static int getPersonsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonsCount();
    }

    /**
    * Updates the person in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param person the person
    * @return the person that was updated
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Person updatePerson(
        br.com.atilo.jcondo.manager.model.Person person)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePerson(person);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Person createPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().createPerson();
    }

    public static br.com.atilo.jcondo.manager.model.Person addPerson(
        java.lang.String name, java.lang.String surname,
        java.lang.String identity, java.lang.String email,
        java.util.Date birthday, br.com.atilo.jcondo.datatype.Gender gender,
        br.com.atilo.jcondo.datatype.PersonType personType,
        java.lang.String remark, long domainId) throws java.lang.Exception {
        return getService()
                   .addPerson(name, surname, identity, email, birthday, gender,
            personType, remark, domainId);
    }

    public static br.com.atilo.jcondo.manager.model.Person updatePerson(
        long id, java.lang.String name, java.lang.String surname,
        java.lang.String identity, java.lang.String email,
        java.util.Date birthday, br.com.atilo.jcondo.datatype.Gender gender,
        java.lang.String remark, long domainId,
        br.com.atilo.jcondo.manager.model.Membership membership)
        throws java.lang.Exception {
        return getService()
                   .updatePerson(id, name, surname, identity, email, birthday,
            gender, remark, domainId, membership);
    }

    public static br.com.atilo.jcondo.manager.model.Person updatePortrait(
        long personId, br.com.atilo.jcondo.Image portrait)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePortrait(personId, portrait);
    }

    public static void updatePassword(long personId, java.lang.String password,
        java.lang.String newPassword)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService().updatePassword(personId, password, newPassword);
    }

    public static br.com.atilo.jcondo.manager.model.Person updateHome(long id,
        long flatId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateHome(id, flatId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> getDomainPeopleByType(
        long domainId, br.com.atilo.jcondo.datatype.PersonType personType)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainPeopleByType(domainId, personType);
    }

    public static br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPerson();
    }

    public static br.com.atilo.jcondo.manager.model.Person getPersonByIdentity(
        java.lang.String identity)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonByIdentity(identity);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> getPeopleByType(
        br.com.atilo.jcondo.datatype.PersonType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPeopleByType(type);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> getDomainPeople(
        long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainPeople(domainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> getDomainPeopleByName(
        long domainId, java.lang.String name)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainPeopleByName(domainId, name);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> getPeople(
        java.lang.String name, java.lang.String identity, long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPeople(name, identity, domainId);
    }

    public static void completePersonRegistration(long companyId,
        java.lang.String emailAddress, boolean autoPassword, boolean sendEmail)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService()
            .completePersonRegistration(companyId, emailAddress, autoPassword,
            sendEmail);
    }

    public static boolean authenticatePerson(long personId,
        java.lang.String password)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().authenticatePerson(personId, password);
    }

    public static void clearService() {
        _service = null;
    }

    public static PersonLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    PersonLocalService.class.getName());

            if (invokableLocalService instanceof PersonLocalService) {
                _service = (PersonLocalService) invokableLocalService;
            } else {
                _service = new PersonLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(PersonLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(PersonLocalService service) {
    }
}
