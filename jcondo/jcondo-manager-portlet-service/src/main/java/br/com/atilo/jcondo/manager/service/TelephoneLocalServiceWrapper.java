package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link TelephoneLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see TelephoneLocalService
 * @generated
 */
public class TelephoneLocalServiceWrapper implements TelephoneLocalService,
    ServiceWrapper<TelephoneLocalService> {
    private TelephoneLocalService _telephoneLocalService;

    public TelephoneLocalServiceWrapper(
        TelephoneLocalService telephoneLocalService) {
        _telephoneLocalService = telephoneLocalService;
    }

    /**
    * Adds the telephone to the database. Also notifies the appropriate model listeners.
    *
    * @param telephone the telephone
    * @return the telephone that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Telephone addTelephone(
        br.com.atilo.jcondo.manager.model.Telephone telephone)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.addTelephone(telephone);
    }

    /**
    * Creates a new telephone with the primary key. Does not add the telephone to the database.
    *
    * @param id the primary key for the new telephone
    * @return the new telephone
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Telephone createTelephone(long id) {
        return _telephoneLocalService.createTelephone(id);
    }

    /**
    * Deletes the telephone with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the telephone
    * @return the telephone that was removed
    * @throws PortalException if a telephone with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Telephone deleteTelephone(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.deleteTelephone(id);
    }

    /**
    * Deletes the telephone from the database. Also notifies the appropriate model listeners.
    *
    * @param telephone the telephone
    * @return the telephone that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Telephone deleteTelephone(
        br.com.atilo.jcondo.manager.model.Telephone telephone)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.deleteTelephone(telephone);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _telephoneLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone fetchTelephone(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.fetchTelephone(id);
    }

    /**
    * Returns the telephone with the primary key.
    *
    * @param id the primary key of the telephone
    * @return the telephone
    * @throws PortalException if a telephone with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Telephone getTelephone(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.getTelephone(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the telephones.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of telephones
    * @param end the upper bound of the range of telephones (not inclusive)
    * @return the range of telephones
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getTelephones(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.getTelephones(start, end);
    }

    /**
    * Returns the number of telephones.
    *
    * @return the number of telephones
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getTelephonesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.getTelephonesCount();
    }

    /**
    * Updates the telephone in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param telephone the telephone
    * @return the telephone that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Telephone updateTelephone(
        br.com.atilo.jcondo.manager.model.Telephone telephone)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.updateTelephone(telephone);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _telephoneLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _telephoneLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _telephoneLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone addDomainPhone(
        long domainId, long number, long extension, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.addDomainPhone(domainId, number,
            extension, primary);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone addPersonPhone(
        long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary,
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.addPersonPhone(number, extension, type,
            primary, personId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone updateEnterprisePhone(
        long phoneId, long number, long extension, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.updateEnterprisePhone(phoneId, number,
            extension, primary);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone updatePersonPhone(
        long phoneId, long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.updatePersonPhone(phoneId, number,
            extension, type, primary);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone deletePhone(long phoneId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.deletePhone(phoneId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getPersonPhones(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.getPersonPhones(personId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getPersonPhones(
        long personId, br.com.atilo.jcondo.datatype.PhoneType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.getPersonPhones(personId, type);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone getPersonPhone(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.getPersonPhone(personId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getDomainPhones(
        long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _telephoneLocalService.getDomainPhones(domainId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public TelephoneLocalService getWrappedTelephoneLocalService() {
        return _telephoneLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedTelephoneLocalService(
        TelephoneLocalService telephoneLocalService) {
        _telephoneLocalService = telephoneLocalService;
    }

    @Override
    public TelephoneLocalService getWrappedService() {
        return _telephoneLocalService;
    }

    @Override
    public void setWrappedService(TelephoneLocalService telephoneLocalService) {
        _telephoneLocalService = telephoneLocalService;
    }
}
