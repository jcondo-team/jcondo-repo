package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.AccessLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class AccessClp extends BaseModelImpl<Access> implements Access {
    private long _id;
    private long _destinationId;
    private long _visitorId;
    private long _vehicleId;
    private long _authorizerId;
    private String _authorizerName;
    private String _badge;
    private boolean _ended;
    private String _remark;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _accessRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public AccessClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Access.class;
    }

    @Override
    public String getModelClassName() {
        return Access.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("destinationId", getDestinationId());
        attributes.put("visitorId", getVisitorId());
        attributes.put("vehicleId", getVehicleId());
        attributes.put("authorizerId", getAuthorizerId());
        attributes.put("authorizerName", getAuthorizerName());
        attributes.put("badge", getBadge());
        attributes.put("ended", getEnded());
        attributes.put("remark", getRemark());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long destinationId = (Long) attributes.get("destinationId");

        if (destinationId != null) {
            setDestinationId(destinationId);
        }

        Long visitorId = (Long) attributes.get("visitorId");

        if (visitorId != null) {
            setVisitorId(visitorId);
        }

        Long vehicleId = (Long) attributes.get("vehicleId");

        if (vehicleId != null) {
            setVehicleId(vehicleId);
        }

        Long authorizerId = (Long) attributes.get("authorizerId");

        if (authorizerId != null) {
            setAuthorizerId(authorizerId);
        }

        String authorizerName = (String) attributes.get("authorizerName");

        if (authorizerName != null) {
            setAuthorizerName(authorizerName);
        }

        String badge = (String) attributes.get("badge");

        if (badge != null) {
            setBadge(badge);
        }

        Boolean ended = (Boolean) attributes.get("ended");

        if (ended != null) {
            setEnded(ended);
        }

        String remark = (String) attributes.get("remark");

        if (remark != null) {
            setRemark(remark);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_accessRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getDestinationId() {
        return _destinationId;
    }

    @Override
    public void setDestinationId(long destinationId) {
        _destinationId = destinationId;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setDestinationId", long.class);

                method.invoke(_accessRemoteModel, destinationId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getVisitorId() {
        return _visitorId;
    }

    @Override
    public void setVisitorId(long visitorId) {
        _visitorId = visitorId;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setVisitorId", long.class);

                method.invoke(_accessRemoteModel, visitorId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getVehicleId() {
        return _vehicleId;
    }

    @Override
    public void setVehicleId(long vehicleId) {
        _vehicleId = vehicleId;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setVehicleId", long.class);

                method.invoke(_accessRemoteModel, vehicleId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getAuthorizerId() {
        return _authorizerId;
    }

    @Override
    public void setAuthorizerId(long authorizerId) {
        _authorizerId = authorizerId;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setAuthorizerId", long.class);

                method.invoke(_accessRemoteModel, authorizerId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getAuthorizerName() {
        return _authorizerName;
    }

    @Override
    public void setAuthorizerName(String authorizerName) {
        _authorizerName = authorizerName;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setAuthorizerName",
                        String.class);

                method.invoke(_accessRemoteModel, authorizerName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getBadge() {
        return _badge;
    }

    @Override
    public void setBadge(String badge) {
        _badge = badge;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setBadge", String.class);

                method.invoke(_accessRemoteModel, badge);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getEnded() {
        return _ended;
    }

    @Override
    public boolean isEnded() {
        return _ended;
    }

    @Override
    public void setEnded(boolean ended) {
        _ended = ended;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setEnded", boolean.class);

                method.invoke(_accessRemoteModel, ended);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getRemark() {
        return _remark;
    }

    @Override
    public void setRemark(String remark) {
        _remark = remark;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setRemark", String.class);

                method.invoke(_accessRemoteModel, remark);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_accessRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_accessRemoteModel != null) {
            try {
                Class<?> clazz = _accessRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_accessRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle getVehicle() {
        try {
            String methodName = "getVehicle";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.manager.model.Vehicle returnObj = (br.com.atilo.jcondo.manager.model.Vehicle) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getAccessRemoteModel() {
        return _accessRemoteModel;
    }

    public void setAccessRemoteModel(BaseModel<?> accessRemoteModel) {
        _accessRemoteModel = accessRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _accessRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_accessRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            AccessLocalServiceUtil.addAccess(this);
        } else {
            AccessLocalServiceUtil.updateAccess(this);
        }
    }

    @Override
    public Access toEscapedModel() {
        return (Access) ProxyUtil.newProxyInstance(Access.class.getClassLoader(),
            new Class[] { Access.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        AccessClp clone = new AccessClp();

        clone.setId(getId());
        clone.setDestinationId(getDestinationId());
        clone.setVisitorId(getVisitorId());
        clone.setVehicleId(getVehicleId());
        clone.setAuthorizerId(getAuthorizerId());
        clone.setAuthorizerName(getAuthorizerName());
        clone.setBadge(getBadge());
        clone.setEnded(getEnded());
        clone.setRemark(getRemark());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Access access) {
        long primaryKey = access.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AccessClp)) {
            return false;
        }

        AccessClp access = (AccessClp) obj;

        long primaryKey = access.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", destinationId=");
        sb.append(getDestinationId());
        sb.append(", visitorId=");
        sb.append(getVisitorId());
        sb.append(", vehicleId=");
        sb.append(getVehicleId());
        sb.append(", authorizerId=");
        sb.append(getAuthorizerId());
        sb.append(", authorizerName=");
        sb.append(getAuthorizerName());
        sb.append(", badge=");
        sb.append(getBadge());
        sb.append(", ended=");
        sb.append(getEnded());
        sb.append(", remark=");
        sb.append(getRemark());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(37);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Access");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>destinationId</column-name><column-value><![CDATA[");
        sb.append(getDestinationId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>visitorId</column-name><column-value><![CDATA[");
        sb.append(getVisitorId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>vehicleId</column-name><column-value><![CDATA[");
        sb.append(getVehicleId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>authorizerId</column-name><column-value><![CDATA[");
        sb.append(getAuthorizerId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>authorizerName</column-name><column-value><![CDATA[");
        sb.append(getAuthorizerName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>badge</column-name><column-value><![CDATA[");
        sb.append(getBadge());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ended</column-name><column-value><![CDATA[");
        sb.append(getEnded());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>remark</column-name><column-value><![CDATA[");
        sb.append(getRemark());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
