package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link AccessPermission}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AccessPermission
 * @generated
 */
public class AccessPermissionWrapper implements AccessPermission,
    ModelWrapper<AccessPermission> {
    private AccessPermission _accessPermission;

    public AccessPermissionWrapper(AccessPermission accessPermission) {
        _accessPermission = accessPermission;
    }

    @Override
    public Class<?> getModelClass() {
        return AccessPermission.class;
    }

    @Override
    public String getModelClassName() {
        return AccessPermission.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("personId", getPersonId());
        attributes.put("domainId", getDomainId());
        attributes.put("weekDay", getWeekDay());
        attributes.put("beginTime", getBeginTime());
        attributes.put("endTime", getEndTime());
        attributes.put("beginDate", getBeginDate());
        attributes.put("endDate", getEndDate());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        Integer weekDay = (Integer) attributes.get("weekDay");

        if (weekDay != null) {
            setWeekDay(weekDay);
        }

        String beginTime = (String) attributes.get("beginTime");

        if (beginTime != null) {
            setBeginTime(beginTime);
        }

        String endTime = (String) attributes.get("endTime");

        if (endTime != null) {
            setEndTime(endTime);
        }

        Date beginDate = (Date) attributes.get("beginDate");

        if (beginDate != null) {
            setBeginDate(beginDate);
        }

        Date endDate = (Date) attributes.get("endDate");

        if (endDate != null) {
            setEndDate(endDate);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this access permission.
    *
    * @return the primary key of this access permission
    */
    @Override
    public long getPrimaryKey() {
        return _accessPermission.getPrimaryKey();
    }

    /**
    * Sets the primary key of this access permission.
    *
    * @param primaryKey the primary key of this access permission
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _accessPermission.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this access permission.
    *
    * @return the ID of this access permission
    */
    @Override
    public long getId() {
        return _accessPermission.getId();
    }

    /**
    * Sets the ID of this access permission.
    *
    * @param id the ID of this access permission
    */
    @Override
    public void setId(long id) {
        _accessPermission.setId(id);
    }

    /**
    * Returns the person ID of this access permission.
    *
    * @return the person ID of this access permission
    */
    @Override
    public long getPersonId() {
        return _accessPermission.getPersonId();
    }

    /**
    * Sets the person ID of this access permission.
    *
    * @param personId the person ID of this access permission
    */
    @Override
    public void setPersonId(long personId) {
        _accessPermission.setPersonId(personId);
    }

    /**
    * Returns the domain ID of this access permission.
    *
    * @return the domain ID of this access permission
    */
    @Override
    public long getDomainId() {
        return _accessPermission.getDomainId();
    }

    /**
    * Sets the domain ID of this access permission.
    *
    * @param domainId the domain ID of this access permission
    */
    @Override
    public void setDomainId(long domainId) {
        _accessPermission.setDomainId(domainId);
    }

    /**
    * Returns the week day of this access permission.
    *
    * @return the week day of this access permission
    */
    @Override
    public int getWeekDay() {
        return _accessPermission.getWeekDay();
    }

    /**
    * Sets the week day of this access permission.
    *
    * @param weekDay the week day of this access permission
    */
    @Override
    public void setWeekDay(int weekDay) {
        _accessPermission.setWeekDay(weekDay);
    }

    /**
    * Returns the begin time of this access permission.
    *
    * @return the begin time of this access permission
    */
    @Override
    public java.lang.String getBeginTime() {
        return _accessPermission.getBeginTime();
    }

    /**
    * Sets the begin time of this access permission.
    *
    * @param beginTime the begin time of this access permission
    */
    @Override
    public void setBeginTime(java.lang.String beginTime) {
        _accessPermission.setBeginTime(beginTime);
    }

    /**
    * Returns the end time of this access permission.
    *
    * @return the end time of this access permission
    */
    @Override
    public java.lang.String getEndTime() {
        return _accessPermission.getEndTime();
    }

    /**
    * Sets the end time of this access permission.
    *
    * @param endTime the end time of this access permission
    */
    @Override
    public void setEndTime(java.lang.String endTime) {
        _accessPermission.setEndTime(endTime);
    }

    /**
    * Returns the begin date of this access permission.
    *
    * @return the begin date of this access permission
    */
    @Override
    public java.util.Date getBeginDate() {
        return _accessPermission.getBeginDate();
    }

    /**
    * Sets the begin date of this access permission.
    *
    * @param beginDate the begin date of this access permission
    */
    @Override
    public void setBeginDate(java.util.Date beginDate) {
        _accessPermission.setBeginDate(beginDate);
    }

    /**
    * Returns the end date of this access permission.
    *
    * @return the end date of this access permission
    */
    @Override
    public java.util.Date getEndDate() {
        return _accessPermission.getEndDate();
    }

    /**
    * Sets the end date of this access permission.
    *
    * @param endDate the end date of this access permission
    */
    @Override
    public void setEndDate(java.util.Date endDate) {
        _accessPermission.setEndDate(endDate);
    }

    /**
    * Returns the opr date of this access permission.
    *
    * @return the opr date of this access permission
    */
    @Override
    public java.util.Date getOprDate() {
        return _accessPermission.getOprDate();
    }

    /**
    * Sets the opr date of this access permission.
    *
    * @param oprDate the opr date of this access permission
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _accessPermission.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this access permission.
    *
    * @return the opr user of this access permission
    */
    @Override
    public long getOprUser() {
        return _accessPermission.getOprUser();
    }

    /**
    * Sets the opr user of this access permission.
    *
    * @param oprUser the opr user of this access permission
    */
    @Override
    public void setOprUser(long oprUser) {
        _accessPermission.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _accessPermission.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _accessPermission.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _accessPermission.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _accessPermission.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _accessPermission.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _accessPermission.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _accessPermission.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _accessPermission.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _accessPermission.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _accessPermission.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _accessPermission.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new AccessPermissionWrapper((AccessPermission) _accessPermission.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.manager.model.AccessPermission accessPermission) {
        return _accessPermission.compareTo(accessPermission);
    }

    @Override
    public int hashCode() {
        return _accessPermission.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.AccessPermission> toCacheModel() {
        return _accessPermission.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission toEscapedModel() {
        return new AccessPermissionWrapper(_accessPermission.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission toUnescapedModel() {
        return new AccessPermissionWrapper(_accessPermission.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _accessPermission.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _accessPermission.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _accessPermission.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AccessPermissionWrapper)) {
            return false;
        }

        AccessPermissionWrapper accessPermissionWrapper = (AccessPermissionWrapper) obj;

        if (Validator.equals(_accessPermission,
                    accessPermissionWrapper._accessPermission)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public AccessPermission getWrappedAccessPermission() {
        return _accessPermission;
    }

    @Override
    public AccessPermission getWrappedModel() {
        return _accessPermission;
    }

    @Override
    public void resetOriginalValues() {
        _accessPermission.resetOriginalValues();
    }
}
