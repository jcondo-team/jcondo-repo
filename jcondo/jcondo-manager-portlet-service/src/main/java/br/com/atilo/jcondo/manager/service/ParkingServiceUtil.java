package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Parking. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.ParkingServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see ParkingService
 * @see br.com.atilo.jcondo.manager.service.base.ParkingServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.ParkingServiceImpl
 * @generated
 */
public class ParkingServiceUtil {
    private static ParkingService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.ParkingServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Parking addParking(
        java.lang.String code, br.com.atilo.jcondo.datatype.ParkingType type,
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addParking(code, type, ownerDomainId);
    }

    public static br.com.atilo.jcondo.manager.model.Parking updateParking(
        long parkingId, java.lang.String code,
        br.com.atilo.jcondo.datatype.ParkingType type, long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateParking(parkingId, code, type, ownerDomainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> getOwnedParkings(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getOwnedParkings(ownerDomainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> getRentedParkings(
        long renterDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getRentedParkings(renterDomainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> getGrantedParkings(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getGrantedParkings(ownerDomainId);
    }

    public static br.com.atilo.jcondo.manager.model.Parking rent(
        long ownerDomainId, long renterDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().rent(ownerDomainId, renterDomainId);
    }

    public static br.com.atilo.jcondo.manager.model.Parking unrent(
        long parkingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().unrent(parkingId);
    }

    public static void clearService() {
        _service = null;
    }

    public static ParkingService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    ParkingService.class.getName());

            if (invokableService instanceof ParkingService) {
                _service = (ParkingService) invokableService;
            } else {
                _service = new ParkingServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(ParkingServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(ParkingService service) {
    }
}
