package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.EnterpriseServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.EnterpriseServiceSoap
 * @generated
 */
public class EnterpriseSoap implements Serializable {
    private long _id;
    private long _organizationId;
    private int _statusId;
    private String _identity;
    private Date _oprDate;
    private long _oprUser;

    public EnterpriseSoap() {
    }

    public static EnterpriseSoap toSoapModel(Enterprise model) {
        EnterpriseSoap soapModel = new EnterpriseSoap();

        soapModel.setId(model.getId());
        soapModel.setOrganizationId(model.getOrganizationId());
        soapModel.setStatusId(model.getStatusId());
        soapModel.setIdentity(model.getIdentity());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static EnterpriseSoap[] toSoapModels(Enterprise[] models) {
        EnterpriseSoap[] soapModels = new EnterpriseSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static EnterpriseSoap[][] toSoapModels(Enterprise[][] models) {
        EnterpriseSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new EnterpriseSoap[models.length][models[0].length];
        } else {
            soapModels = new EnterpriseSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static EnterpriseSoap[] toSoapModels(List<Enterprise> models) {
        List<EnterpriseSoap> soapModels = new ArrayList<EnterpriseSoap>(models.size());

        for (Enterprise model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new EnterpriseSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getOrganizationId() {
        return _organizationId;
    }

    public void setOrganizationId(long organizationId) {
        _organizationId = organizationId;
    }

    public int getStatusId() {
        return _statusId;
    }

    public void setStatusId(int statusId) {
        _statusId = statusId;
    }

    public String getIdentity() {
        return _identity;
    }

    public void setIdentity(String identity) {
        _identity = identity;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
