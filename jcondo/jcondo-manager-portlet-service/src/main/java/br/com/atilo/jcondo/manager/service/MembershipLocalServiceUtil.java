package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Membership. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.MembershipLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see MembershipLocalService
 * @see br.com.atilo.jcondo.manager.service.base.MembershipLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.MembershipLocalServiceImpl
 * @generated
 */
public class MembershipLocalServiceUtil {
    private static MembershipLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.MembershipLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the membership to the database. Also notifies the appropriate model listeners.
    *
    * @param membership the membership
    * @return the membership that was added
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership addMembership(
        br.com.atilo.jcondo.manager.model.Membership membership)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addMembership(membership);
    }

    /**
    * Creates a new membership with the primary key. Does not add the membership to the database.
    *
    * @param id the primary key for the new membership
    * @return the new membership
    */
    public static br.com.atilo.jcondo.manager.model.Membership createMembership(
        long id) {
        return getService().createMembership(id);
    }

    /**
    * Deletes the membership with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the membership
    * @return the membership that was removed
    * @throws PortalException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    * @throws java.lang.Exception
    */
    public static br.com.atilo.jcondo.manager.model.Membership deleteMembership(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException,
            java.lang.Exception {
        return getService().deleteMembership(id);
    }

    /**
    * Deletes the membership from the database. Also notifies the appropriate model listeners.
    *
    * @param membership the membership
    * @return the membership that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership deleteMembership(
        br.com.atilo.jcondo.manager.model.Membership membership)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteMembership(membership);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static br.com.atilo.jcondo.manager.model.Membership fetchMembership(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchMembership(id);
    }

    /**
    * Returns the membership with the primary key.
    *
    * @param id the primary key of the membership
    * @return the membership
    * @throws PortalException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership getMembership(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getMembership(id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the memberships.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> getMemberships(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMemberships(start, end);
    }

    /**
    * Returns the number of memberships.
    *
    * @return the number of memberships
    * @throws SystemException if a system exception occurred
    */
    public static int getMembershipsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getMembershipsCount();
    }

    /**
    * Updates the membership in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param membership the membership
    * @return the membership that was updated
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership updateMembership(
        br.com.atilo.jcondo.manager.model.Membership membership)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateMembership(membership);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Membership addMembership(
        long personId, long domainId,
        br.com.atilo.jcondo.datatype.PersonType type)
        throws java.lang.Exception {
        return getService().addMembership(personId, domainId, type);
    }

    public static br.com.atilo.jcondo.manager.model.Membership updateMembership(
        long id, br.com.atilo.jcondo.datatype.PersonType type)
        throws java.lang.Exception {
        return getService().updateMembership(id, type);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> getPersonMemberships(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonMemberships(personId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> getDomainMembershipsByType(
        long domainId, br.com.atilo.jcondo.datatype.PersonType type)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainMembershipsByType(domainId, type);
    }

    public static br.com.atilo.jcondo.manager.model.Membership getDomainMembershipByPerson(
        long domainId, long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainMembershipByPerson(domainId, personId);
    }

    public static void clearService() {
        _service = null;
    }

    public static MembershipLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    MembershipLocalService.class.getName());

            if (invokableLocalService instanceof MembershipLocalService) {
                _service = (MembershipLocalService) invokableLocalService;
            } else {
                _service = new MembershipLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(MembershipLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(MembershipLocalService service) {
    }
}
