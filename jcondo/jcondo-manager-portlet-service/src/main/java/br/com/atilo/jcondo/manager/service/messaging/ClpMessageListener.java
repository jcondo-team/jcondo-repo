package br.com.atilo.jcondo.manager.service.messaging;

import br.com.atilo.jcondo.manager.service.AccessLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.AccessPermissionLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.AccessPermissionServiceUtil;
import br.com.atilo.jcondo.manager.service.AccessServiceUtil;
import br.com.atilo.jcondo.manager.service.AddressLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.AddressServiceUtil;
import br.com.atilo.jcondo.manager.service.ClpSerializer;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.DepartmentServiceUtil;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.EnterpriseServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatServiceUtil;
import br.com.atilo.jcondo.manager.service.KinshipLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.KinshipServiceUtil;
import br.com.atilo.jcondo.manager.service.MailLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.MailNoteLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.MailNoteServiceUtil;
import br.com.atilo.jcondo.manager.service.MailServiceUtil;
import br.com.atilo.jcondo.manager.service.MembershipLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.MembershipServiceUtil;
import br.com.atilo.jcondo.manager.service.ParkingLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.ParkingServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonServiceUtil;
import br.com.atilo.jcondo.manager.service.PetLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PetServiceUtil;
import br.com.atilo.jcondo.manager.service.TelephoneLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.TelephoneServiceUtil;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.VehicleServiceUtil;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;


public class ClpMessageListener extends BaseMessageListener {
    public static String getServletContextName() {
        return ClpSerializer.getServletContextName();
    }

    @Override
    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(getServletContextName())) {
            AccessLocalServiceUtil.clearService();

            AccessServiceUtil.clearService();
            AccessPermissionLocalServiceUtil.clearService();

            AccessPermissionServiceUtil.clearService();
            AddressLocalServiceUtil.clearService();

            AddressServiceUtil.clearService();
            DepartmentLocalServiceUtil.clearService();

            DepartmentServiceUtil.clearService();
            EnterpriseLocalServiceUtil.clearService();

            EnterpriseServiceUtil.clearService();
            FlatLocalServiceUtil.clearService();

            FlatServiceUtil.clearService();
            KinshipLocalServiceUtil.clearService();

            KinshipServiceUtil.clearService();
            MailLocalServiceUtil.clearService();

            MailServiceUtil.clearService();
            MailNoteLocalServiceUtil.clearService();

            MailNoteServiceUtil.clearService();
            MembershipLocalServiceUtil.clearService();

            MembershipServiceUtil.clearService();
            ParkingLocalServiceUtil.clearService();

            ParkingServiceUtil.clearService();
            PersonLocalServiceUtil.clearService();

            PersonServiceUtil.clearService();
            PetLocalServiceUtil.clearService();

            PetServiceUtil.clearService();
            TelephoneLocalServiceUtil.clearService();

            TelephoneServiceUtil.clearService();
            VehicleLocalServiceUtil.clearService();

            VehicleServiceUtil.clearService();
        }
    }
}
