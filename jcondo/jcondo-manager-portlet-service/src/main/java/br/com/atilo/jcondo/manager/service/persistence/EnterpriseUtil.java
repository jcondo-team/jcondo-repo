package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Enterprise;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the enterprise service. This utility wraps {@link EnterprisePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EnterprisePersistence
 * @see EnterprisePersistenceImpl
 * @generated
 */
public class EnterpriseUtil {
    private static EnterprisePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Enterprise enterprise) {
        getPersistence().clearCache(enterprise);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Enterprise> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Enterprise> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Enterprise> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Enterprise update(Enterprise enterprise)
        throws SystemException {
        return getPersistence().update(enterprise);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Enterprise update(Enterprise enterprise,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(enterprise, serviceContext);
    }

    /**
    * Returns the enterprise where identity = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchEnterpriseException} if it could not be found.
    *
    * @param identity the identity
    * @return the matching enterprise
    * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise findByIdentity(
        java.lang.String identity)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIdentity(identity);
    }

    /**
    * Returns the enterprise where identity = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param identity the identity
    * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise fetchByIdentity(
        java.lang.String identity)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByIdentity(identity);
    }

    /**
    * Returns the enterprise where identity = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param identity the identity
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise fetchByIdentity(
        java.lang.String identity, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByIdentity(identity, retrieveFromCache);
    }

    /**
    * Removes the enterprise where identity = &#63; from the database.
    *
    * @param identity the identity
    * @return the enterprise that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise removeByIdentity(
        java.lang.String identity)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().removeByIdentity(identity);
    }

    /**
    * Returns the number of enterprises where identity = &#63;.
    *
    * @param identity the identity
    * @return the number of matching enterprises
    * @throws SystemException if a system exception occurred
    */
    public static int countByIdentity(java.lang.String identity)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByIdentity(identity);
    }

    /**
    * Returns the enterprise where organizationId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchEnterpriseException} if it could not be found.
    *
    * @param organizationId the organization ID
    * @return the matching enterprise
    * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise findByOrganizationId(
        long organizationId)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByOrganizationId(organizationId);
    }

    /**
    * Returns the enterprise where organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param organizationId the organization ID
    * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise fetchByOrganizationId(
        long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByOrganizationId(organizationId);
    }

    /**
    * Returns the enterprise where organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param organizationId the organization ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise fetchByOrganizationId(
        long organizationId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByOrganizationId(organizationId, retrieveFromCache);
    }

    /**
    * Removes the enterprise where organizationId = &#63; from the database.
    *
    * @param organizationId the organization ID
    * @return the enterprise that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise removeByOrganizationId(
        long organizationId)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().removeByOrganizationId(organizationId);
    }

    /**
    * Returns the number of enterprises where organizationId = &#63;.
    *
    * @param organizationId the organization ID
    * @return the number of matching enterprises
    * @throws SystemException if a system exception occurred
    */
    public static int countByOrganizationId(long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByOrganizationId(organizationId);
    }

    /**
    * Caches the enterprise in the entity cache if it is enabled.
    *
    * @param enterprise the enterprise
    */
    public static void cacheResult(
        br.com.atilo.jcondo.manager.model.Enterprise enterprise) {
        getPersistence().cacheResult(enterprise);
    }

    /**
    * Caches the enterprises in the entity cache if it is enabled.
    *
    * @param enterprises the enterprises
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> enterprises) {
        getPersistence().cacheResult(enterprises);
    }

    /**
    * Creates a new enterprise with the primary key. Does not add the enterprise to the database.
    *
    * @param id the primary key for the new enterprise
    * @return the new enterprise
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the enterprise with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the enterprise
    * @return the enterprise that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a enterprise with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.manager.model.Enterprise updateImpl(
        br.com.atilo.jcondo.manager.model.Enterprise enterprise)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(enterprise);
    }

    /**
    * Returns the enterprise with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchEnterpriseException} if it could not be found.
    *
    * @param id the primary key of the enterprise
    * @return the enterprise
    * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a enterprise with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the enterprise with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the enterprise
    * @return the enterprise, or <code>null</code> if a enterprise with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Enterprise fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the enterprises.
    *
    * @return the enterprises
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the enterprises.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.EnterpriseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of enterprises
    * @param end the upper bound of the range of enterprises (not inclusive)
    * @return the range of enterprises
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the enterprises.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.EnterpriseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of enterprises
    * @param end the upper bound of the range of enterprises (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of enterprises
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the enterprises from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of enterprises.
    *
    * @return the number of enterprises
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static EnterprisePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (EnterprisePersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.manager.service.ClpSerializer.getServletContextName(),
                    EnterprisePersistence.class.getName());

            ReferenceRegistry.registerReference(EnterpriseUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(EnterprisePersistence persistence) {
    }
}
