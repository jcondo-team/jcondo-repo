package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link FlatLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see FlatLocalService
 * @generated
 */
public class FlatLocalServiceWrapper implements FlatLocalService,
    ServiceWrapper<FlatLocalService> {
    private FlatLocalService _flatLocalService;

    public FlatLocalServiceWrapper(FlatLocalService flatLocalService) {
        _flatLocalService = flatLocalService;
    }

    /**
    * Adds the flat to the database. Also notifies the appropriate model listeners.
    *
    * @param flat the flat
    * @return the flat that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Flat addFlat(
        br.com.atilo.jcondo.manager.model.Flat flat)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.addFlat(flat);
    }

    /**
    * Creates a new flat with the primary key. Does not add the flat to the database.
    *
    * @param id the primary key for the new flat
    * @return the new flat
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Flat createFlat(long id) {
        return _flatLocalService.createFlat(id);
    }

    /**
    * Deletes the flat with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the flat
    * @return the flat that was removed
    * @throws PortalException if a flat with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Flat deleteFlat(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.deleteFlat(id);
    }

    /**
    * Deletes the flat from the database. Also notifies the appropriate model listeners.
    *
    * @param flat the flat
    * @return the flat that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Flat deleteFlat(
        br.com.atilo.jcondo.manager.model.Flat flat)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.deleteFlat(flat);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _flatLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat fetchFlat(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.fetchFlat(id);
    }

    /**
    * Returns the flat with the primary key.
    *
    * @param id the primary key of the flat
    * @return the flat
    * @throws PortalException if a flat with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Flat getFlat(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.getFlat(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the flats.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of flats
    * @param end the upper bound of the range of flats (not inclusive)
    * @return the range of flats
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> getFlats(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.getFlats(start, end);
    }

    /**
    * Returns the number of flats.
    *
    * @return the number of flats
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getFlatsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.getFlatsCount();
    }

    /**
    * Updates the flat in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param flat the flat
    * @return the flat that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Flat updateFlat(
        br.com.atilo.jcondo.manager.model.Flat flat)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.updateFlat(flat);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _flatLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _flatLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _flatLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat addFlat(int number,
        int block, boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.addFlat(number, block, disables, brigade,
            pets, petTypes);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat updateFlat(long id,
        boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.updateFlat(id, disables, brigade, pets,
            petTypes);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> getPersonFlats(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.getPersonFlats(personId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> getFlats()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.getFlats();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat setFlatPerson(long flatId,
        long personId)
        throws br.com.atilo.jcondo.manager.NoSuchFlatException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatLocalService.setFlatPerson(flatId, personId);
    }

    @Override
    public void setFlatDefaulting(long id, boolean isDefaulting)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _flatLocalService.setFlatDefaulting(id, isDefaulting);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public FlatLocalService getWrappedFlatLocalService() {
        return _flatLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedFlatLocalService(FlatLocalService flatLocalService) {
        _flatLocalService = flatLocalService;
    }

    @Override
    public FlatLocalService getWrappedService() {
        return _flatLocalService;
    }

    @Override
    public void setWrappedService(FlatLocalService flatLocalService) {
        _flatLocalService = flatLocalService;
    }
}
