package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AddressLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AddressLocalService
 * @generated
 */
public class AddressLocalServiceWrapper implements AddressLocalService,
    ServiceWrapper<AddressLocalService> {
    private AddressLocalService _addressLocalService;

    public AddressLocalServiceWrapper(AddressLocalService addressLocalService) {
        _addressLocalService = addressLocalService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _addressLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _addressLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _addressLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public com.liferay.portal.model.Address createAddress() {
        return _addressLocalService.createAddress();
    }

    @Override
    public com.liferay.portal.model.Address getPersonAddress(long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _addressLocalService.getPersonAddress(personId);
    }

    @Override
    public com.liferay.portal.model.Address addPersonAddress(long personId,
        java.lang.String street, java.lang.String city, long stateId,
        java.lang.String postCode)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _addressLocalService.addPersonAddress(personId, street, city,
            stateId, postCode);
    }

    @Override
    public com.liferay.portal.model.Address getEnterpriseAddress(
        long enterpriseId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _addressLocalService.getEnterpriseAddress(enterpriseId);
    }

    @Override
    public com.liferay.portal.model.Address addEnterpriseAddress(
        long enterpriseId, java.lang.String street, java.lang.String city,
        long stateId, java.lang.String postCode)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _addressLocalService.addEnterpriseAddress(enterpriseId, street,
            city, stateId, postCode);
    }

    @Override
    public com.liferay.portal.model.Address updateAddress(long addressId,
        java.lang.String street, java.lang.String city, long stateId,
        java.lang.String postCode)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _addressLocalService.updateAddress(addressId, street, city,
            stateId, postCode);
    }

    @Override
    public com.liferay.portal.model.Address deleteAddress(long addressId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _addressLocalService.deleteAddress(addressId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public AddressLocalService getWrappedAddressLocalService() {
        return _addressLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedAddressLocalService(
        AddressLocalService addressLocalService) {
        _addressLocalService = addressLocalService;
    }

    @Override
    public AddressLocalService getWrappedService() {
        return _addressLocalService;
    }

    @Override
    public void setWrappedService(AddressLocalService addressLocalService) {
        _addressLocalService = addressLocalService;
    }
}
