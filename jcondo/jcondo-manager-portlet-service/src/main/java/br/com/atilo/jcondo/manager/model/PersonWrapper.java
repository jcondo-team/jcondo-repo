package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Person}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Person
 * @generated
 */
public class PersonWrapper implements Person, ModelWrapper<Person> {
    private Person _person;

    public PersonWrapper(Person person) {
        _person = person;
    }

    @Override
    public Class<?> getModelClass() {
        return Person.class;
    }

    @Override
    public String getModelClassName() {
        return Person.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("userId", getUserId());
        attributes.put("domainId", getDomainId());
        attributes.put("identity", getIdentity());
        attributes.put("remark", getRemark());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        String identity = (String) attributes.get("identity");

        if (identity != null) {
            setIdentity(identity);
        }

        String remark = (String) attributes.get("remark");

        if (remark != null) {
            setRemark(remark);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this person.
    *
    * @return the primary key of this person
    */
    @Override
    public long getPrimaryKey() {
        return _person.getPrimaryKey();
    }

    /**
    * Sets the primary key of this person.
    *
    * @param primaryKey the primary key of this person
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _person.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this person.
    *
    * @return the ID of this person
    */
    @Override
    public long getId() {
        return _person.getId();
    }

    /**
    * Sets the ID of this person.
    *
    * @param id the ID of this person
    */
    @Override
    public void setId(long id) {
        _person.setId(id);
    }

    /**
    * Returns the user ID of this person.
    *
    * @return the user ID of this person
    */
    @Override
    public long getUserId() {
        return _person.getUserId();
    }

    /**
    * Sets the user ID of this person.
    *
    * @param userId the user ID of this person
    */
    @Override
    public void setUserId(long userId) {
        _person.setUserId(userId);
    }

    /**
    * Returns the user uuid of this person.
    *
    * @return the user uuid of this person
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.lang.String getUserUuid()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _person.getUserUuid();
    }

    /**
    * Sets the user uuid of this person.
    *
    * @param userUuid the user uuid of this person
    */
    @Override
    public void setUserUuid(java.lang.String userUuid) {
        _person.setUserUuid(userUuid);
    }

    /**
    * Returns the domain ID of this person.
    *
    * @return the domain ID of this person
    */
    @Override
    public long getDomainId() {
        return _person.getDomainId();
    }

    /**
    * Sets the domain ID of this person.
    *
    * @param domainId the domain ID of this person
    */
    @Override
    public void setDomainId(long domainId) {
        _person.setDomainId(domainId);
    }

    /**
    * Returns the identity of this person.
    *
    * @return the identity of this person
    */
    @Override
    public java.lang.String getIdentity() {
        return _person.getIdentity();
    }

    /**
    * Sets the identity of this person.
    *
    * @param identity the identity of this person
    */
    @Override
    public void setIdentity(java.lang.String identity) {
        _person.setIdentity(identity);
    }

    /**
    * Returns the remark of this person.
    *
    * @return the remark of this person
    */
    @Override
    public java.lang.String getRemark() {
        return _person.getRemark();
    }

    /**
    * Sets the remark of this person.
    *
    * @param remark the remark of this person
    */
    @Override
    public void setRemark(java.lang.String remark) {
        _person.setRemark(remark);
    }

    /**
    * Returns the opr date of this person.
    *
    * @return the opr date of this person
    */
    @Override
    public java.util.Date getOprDate() {
        return _person.getOprDate();
    }

    /**
    * Sets the opr date of this person.
    *
    * @param oprDate the opr date of this person
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _person.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this person.
    *
    * @return the opr user of this person
    */
    @Override
    public long getOprUser() {
        return _person.getOprUser();
    }

    /**
    * Sets the opr user of this person.
    *
    * @param oprUser the opr user of this person
    */
    @Override
    public void setOprUser(long oprUser) {
        _person.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _person.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _person.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _person.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _person.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _person.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _person.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _person.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _person.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _person.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _person.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _person.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new PersonWrapper((Person) _person.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Person person) {
        return _person.compareTo(person);
    }

    @Override
    public int hashCode() {
        return _person.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Person> toCacheModel() {
        return _person.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person toEscapedModel() {
        return new PersonWrapper(_person.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person toUnescapedModel() {
        return new PersonWrapper(_person.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _person.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _person.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _person.persist();
    }

    @Override
    public java.lang.String getName()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getName();
    }

    @Override
    public void setName(java.lang.String name) {
        _person.setName(name);
    }

    @Override
    public java.lang.String getSurname()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getSurname();
    }

    @Override
    public void setSurname(java.lang.String surname) {
        _person.setSurname(surname);
    }

    @Override
    public java.lang.String getFullName()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getFullName();
    }

    @Override
    public br.com.atilo.jcondo.datatype.Gender getGender()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getGender();
    }

    @Override
    public void setGender(br.com.atilo.jcondo.datatype.Gender gender)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _person.setGender(gender);
    }

    @Override
    public java.util.Date getBirthday()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getBirthday();
    }

    @Override
    public void setBirthday(java.util.Date birthday)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _person.setBirthday(birthday);
    }

    @Override
    public java.lang.String getEmail()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getEmail();
    }

    @Override
    public void setEmail(java.lang.String email)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _person.setEmail(email);
    }

    @Override
    public br.com.atilo.jcondo.Image getPortrait()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getPortrait();
    }

    @Override
    public void setPortrait(br.com.atilo.jcondo.Image image) {
        _person.setPortrait(image);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> getMemberships()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _person.getMemberships();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getTelephones()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getTelephones();
    }

    @Override
    public br.com.atilo.jcondo.datatype.PersonType getType(long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getType(domainId);
    }

    @Override
    public com.liferay.portal.model.User getUser() {
        return _person.getUser();
    }

    @Override
    public void setUser(com.liferay.portal.model.User user)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _person.setUser(user);
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _person.getDomain();
    }

    @Override
    public void setDomain(com.liferay.portal.model.BaseModel<?> domain)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _person.setDomain(domain);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PersonWrapper)) {
            return false;
        }

        PersonWrapper personWrapper = (PersonWrapper) obj;

        if (Validator.equals(_person, personWrapper._person)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Person getWrappedPerson() {
        return _person;
    }

    @Override
    public Person getWrappedModel() {
        return _person;
    }

    @Override
    public void resetOriginalValues() {
        _person.resetOriginalValues();
    }
}
