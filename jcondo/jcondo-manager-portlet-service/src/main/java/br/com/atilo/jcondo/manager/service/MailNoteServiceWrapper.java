package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MailNoteService}.
 *
 * @author Brian Wing Shun Chan
 * @see MailNoteService
 * @generated
 */
public class MailNoteServiceWrapper implements MailNoteService,
    ServiceWrapper<MailNoteService> {
    private MailNoteService _mailNoteService;

    public MailNoteServiceWrapper(MailNoteService mailNoteService) {
        _mailNoteService = mailNoteService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _mailNoteService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _mailNoteService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _mailNoteService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public MailNoteService getWrappedMailNoteService() {
        return _mailNoteService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedMailNoteService(MailNoteService mailNoteService) {
        _mailNoteService = mailNoteService;
    }

    @Override
    public MailNoteService getWrappedService() {
        return _mailNoteService;
    }

    @Override
    public void setWrappedService(MailNoteService mailNoteService) {
        _mailNoteService = mailNoteService;
    }
}
