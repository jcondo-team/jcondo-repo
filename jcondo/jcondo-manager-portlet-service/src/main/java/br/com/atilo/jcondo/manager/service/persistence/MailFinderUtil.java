package br.com.atilo.jcondo.manager.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class MailFinderUtil {
    private static MailFinder _finder;

    public static java.util.List<br.com.atilo.jcondo.manager.model.Mail> findMails(
        long id, java.lang.String code, java.lang.String recipientName,
        long flatId) {
        return getFinder().findMails(id, code, recipientName, flatId);
    }

    public static MailFinder getFinder() {
        if (_finder == null) {
            _finder = (MailFinder) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.manager.service.ClpSerializer.getServletContextName(),
                    MailFinder.class.getName());

            ReferenceRegistry.registerReference(MailFinderUtil.class, "_finder");
        }

        return _finder;
    }

    public void setFinder(MailFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(MailFinderUtil.class, "_finder");
    }
}
