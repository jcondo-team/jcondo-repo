package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.service.ServiceContext;

import com.liferay.portlet.expando.model.ExpandoBridge;

import java.io.Serializable;

import java.util.Date;

/**
 * The base model interface for the Flat service. Represents a row in the &quot;jco_flat&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link br.com.atilo.jcondo.manager.model.impl.FlatModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link br.com.atilo.jcondo.manager.model.impl.FlatImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Flat
 * @see br.com.atilo.jcondo.manager.model.impl.FlatImpl
 * @see br.com.atilo.jcondo.manager.model.impl.FlatModelImpl
 * @generated
 */
public interface FlatModel extends BaseModel<Flat> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. All methods that expect a flat model instance should use the {@link Flat} interface instead.
     */

    /**
     * Returns the primary key of this flat.
     *
     * @return the primary key of this flat
     */
    public long getPrimaryKey();

    /**
     * Sets the primary key of this flat.
     *
     * @param primaryKey the primary key of this flat
     */
    public void setPrimaryKey(long primaryKey);

    /**
     * Returns the ID of this flat.
     *
     * @return the ID of this flat
     */
    public long getId();

    /**
     * Sets the ID of this flat.
     *
     * @param id the ID of this flat
     */
    public void setId(long id);

    /**
     * Returns the organization ID of this flat.
     *
     * @return the organization ID of this flat
     */
    public long getOrganizationId();

    /**
     * Sets the organization ID of this flat.
     *
     * @param organizationId the organization ID of this flat
     */
    public void setOrganizationId(long organizationId);

    /**
     * Returns the folder ID of this flat.
     *
     * @return the folder ID of this flat
     */
    public long getFolderId();

    /**
     * Sets the folder ID of this flat.
     *
     * @param folderId the folder ID of this flat
     */
    public void setFolderId(long folderId);

    /**
     * Returns the person ID of this flat.
     *
     * @return the person ID of this flat
     */
    public long getPersonId();

    /**
     * Sets the person ID of this flat.
     *
     * @param personId the person ID of this flat
     */
    public void setPersonId(long personId);

    /**
     * Returns the block of this flat.
     *
     * @return the block of this flat
     */
    public int getBlock();

    /**
     * Sets the block of this flat.
     *
     * @param block the block of this flat
     */
    public void setBlock(int block);

    /**
     * Returns the number of this flat.
     *
     * @return the number of this flat
     */
    public int getNumber();

    /**
     * Sets the number of this flat.
     *
     * @param number the number of this flat
     */
    public void setNumber(int number);

    /**
     * Returns the pets of this flat.
     *
     * @return the pets of this flat
     */
    public boolean getPets();

    /**
     * Returns <code>true</code> if this flat is pets.
     *
     * @return <code>true</code> if this flat is pets; <code>false</code> otherwise
     */
    public boolean isPets();

    /**
     * Sets whether this flat is pets.
     *
     * @param pets the pets of this flat
     */
    public void setPets(boolean pets);

    /**
     * Returns the disables of this flat.
     *
     * @return the disables of this flat
     */
    public boolean getDisables();

    /**
     * Returns <code>true</code> if this flat is disables.
     *
     * @return <code>true</code> if this flat is disables; <code>false</code> otherwise
     */
    public boolean isDisables();

    /**
     * Sets whether this flat is disables.
     *
     * @param disables the disables of this flat
     */
    public void setDisables(boolean disables);

    /**
     * Returns the brigade of this flat.
     *
     * @return the brigade of this flat
     */
    public boolean getBrigade();

    /**
     * Returns <code>true</code> if this flat is brigade.
     *
     * @return <code>true</code> if this flat is brigade; <code>false</code> otherwise
     */
    public boolean isBrigade();

    /**
     * Sets whether this flat is brigade.
     *
     * @param brigade the brigade of this flat
     */
    public void setBrigade(boolean brigade);

    /**
     * Returns the defaulting of this flat.
     *
     * @return the defaulting of this flat
     */
    public boolean getDefaulting();

    /**
     * Returns <code>true</code> if this flat is defaulting.
     *
     * @return <code>true</code> if this flat is defaulting; <code>false</code> otherwise
     */
    public boolean isDefaulting();

    /**
     * Sets whether this flat is defaulting.
     *
     * @param defaulting the defaulting of this flat
     */
    public void setDefaulting(boolean defaulting);

    /**
     * Returns the opr date of this flat.
     *
     * @return the opr date of this flat
     */
    public Date getOprDate();

    /**
     * Sets the opr date of this flat.
     *
     * @param oprDate the opr date of this flat
     */
    public void setOprDate(Date oprDate);

    /**
     * Returns the opr user of this flat.
     *
     * @return the opr user of this flat
     */
    public long getOprUser();

    /**
     * Sets the opr user of this flat.
     *
     * @param oprUser the opr user of this flat
     */
    public void setOprUser(long oprUser);

    @Override
    public boolean isNew();

    @Override
    public void setNew(boolean n);

    @Override
    public boolean isCachedModel();

    @Override
    public void setCachedModel(boolean cachedModel);

    @Override
    public boolean isEscapedModel();

    @Override
    public Serializable getPrimaryKeyObj();

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj);

    @Override
    public ExpandoBridge getExpandoBridge();

    @Override
    public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

    @Override
    public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

    @Override
    public void setExpandoBridgeAttributes(ServiceContext serviceContext);

    @Override
    public Object clone();

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Flat flat);

    @Override
    public int hashCode();

    @Override
    public CacheModel<br.com.atilo.jcondo.manager.model.Flat> toCacheModel();

    @Override
    public br.com.atilo.jcondo.manager.model.Flat toEscapedModel();

    @Override
    public br.com.atilo.jcondo.manager.model.Flat toUnescapedModel();

    @Override
    public String toString();

    @Override
    public String toXmlString();
}
