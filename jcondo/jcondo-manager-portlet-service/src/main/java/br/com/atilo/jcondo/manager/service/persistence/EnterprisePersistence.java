package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Enterprise;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the enterprise service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see EnterprisePersistenceImpl
 * @see EnterpriseUtil
 * @generated
 */
public interface EnterprisePersistence extends BasePersistence<Enterprise> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link EnterpriseUtil} to access the enterprise persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns the enterprise where identity = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchEnterpriseException} if it could not be found.
    *
    * @param identity the identity
    * @return the matching enterprise
    * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise findByIdentity(
        java.lang.String identity)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the enterprise where identity = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param identity the identity
    * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise fetchByIdentity(
        java.lang.String identity)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the enterprise where identity = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param identity the identity
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise fetchByIdentity(
        java.lang.String identity, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the enterprise where identity = &#63; from the database.
    *
    * @param identity the identity
    * @return the enterprise that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise removeByIdentity(
        java.lang.String identity)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of enterprises where identity = &#63;.
    *
    * @param identity the identity
    * @return the number of matching enterprises
    * @throws SystemException if a system exception occurred
    */
    public int countByIdentity(java.lang.String identity)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the enterprise where organizationId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchEnterpriseException} if it could not be found.
    *
    * @param organizationId the organization ID
    * @return the matching enterprise
    * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise findByOrganizationId(
        long organizationId)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the enterprise where organizationId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param organizationId the organization ID
    * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise fetchByOrganizationId(
        long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the enterprise where organizationId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param organizationId the organization ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching enterprise, or <code>null</code> if a matching enterprise could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise fetchByOrganizationId(
        long organizationId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the enterprise where organizationId = &#63; from the database.
    *
    * @param organizationId the organization ID
    * @return the enterprise that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise removeByOrganizationId(
        long organizationId)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of enterprises where organizationId = &#63;.
    *
    * @param organizationId the organization ID
    * @return the number of matching enterprises
    * @throws SystemException if a system exception occurred
    */
    public int countByOrganizationId(long organizationId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the enterprise in the entity cache if it is enabled.
    *
    * @param enterprise the enterprise
    */
    public void cacheResult(
        br.com.atilo.jcondo.manager.model.Enterprise enterprise);

    /**
    * Caches the enterprises in the entity cache if it is enabled.
    *
    * @param enterprises the enterprises
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> enterprises);

    /**
    * Creates a new enterprise with the primary key. Does not add the enterprise to the database.
    *
    * @param id the primary key for the new enterprise
    * @return the new enterprise
    */
    public br.com.atilo.jcondo.manager.model.Enterprise create(long id);

    /**
    * Removes the enterprise with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the enterprise
    * @return the enterprise that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a enterprise with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Enterprise updateImpl(
        br.com.atilo.jcondo.manager.model.Enterprise enterprise)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the enterprise with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchEnterpriseException} if it could not be found.
    *
    * @param id the primary key of the enterprise
    * @return the enterprise
    * @throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException if a enterprise with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.manager.NoSuchEnterpriseException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the enterprise with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the enterprise
    * @return the enterprise, or <code>null</code> if a enterprise with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Enterprise fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the enterprises.
    *
    * @return the enterprises
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the enterprises.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.EnterpriseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of enterprises
    * @param end the upper bound of the range of enterprises (not inclusive)
    * @return the range of enterprises
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the enterprises.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.EnterpriseModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of enterprises
    * @param end the upper bound of the range of enterprises (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of enterprises
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the enterprises from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of enterprises.
    *
    * @return the number of enterprises
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
