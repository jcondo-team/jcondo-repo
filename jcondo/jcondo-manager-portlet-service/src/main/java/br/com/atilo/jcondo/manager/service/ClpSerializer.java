package br.com.atilo.jcondo.manager.service;

import br.com.atilo.jcondo.manager.model.AccessClp;
import br.com.atilo.jcondo.manager.model.AccessPermissionClp;
import br.com.atilo.jcondo.manager.model.DepartmentClp;
import br.com.atilo.jcondo.manager.model.EnterpriseClp;
import br.com.atilo.jcondo.manager.model.FlatClp;
import br.com.atilo.jcondo.manager.model.KinshipClp;
import br.com.atilo.jcondo.manager.model.MailClp;
import br.com.atilo.jcondo.manager.model.MailNoteClp;
import br.com.atilo.jcondo.manager.model.MembershipClp;
import br.com.atilo.jcondo.manager.model.ParkingClp;
import br.com.atilo.jcondo.manager.model.PersonClp;
import br.com.atilo.jcondo.manager.model.PetClp;
import br.com.atilo.jcondo.manager.model.TelephoneClp;
import br.com.atilo.jcondo.manager.model.VehicleClp;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayInputStream;
import com.liferay.portal.kernel.io.unsync.UnsyncByteArrayOutputStream;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.ClassLoaderObjectInputStream;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.BaseModel;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.lang.reflect.Method;

import java.util.ArrayList;
import java.util.List;


public class ClpSerializer {
    private static Log _log = LogFactoryUtil.getLog(ClpSerializer.class);
    private static String _servletContextName;
    private static boolean _useReflectionToTranslateThrowable = true;

    public static String getServletContextName() {
        if (Validator.isNotNull(_servletContextName)) {
            return _servletContextName;
        }

        synchronized (ClpSerializer.class) {
            if (Validator.isNotNull(_servletContextName)) {
                return _servletContextName;
            }

            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Class<?> portletPropsClass = classLoader.loadClass(
                        "com.liferay.util.portlet.PortletProps");

                Method getMethod = portletPropsClass.getMethod("get",
                        new Class<?>[] { String.class });

                String portletPropsServletContextName = (String) getMethod.invoke(null,
                        "jcondo-manager-portlet-deployment-context");

                if (Validator.isNotNull(portletPropsServletContextName)) {
                    _servletContextName = portletPropsServletContextName;
                }
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info(
                        "Unable to locate deployment context from portlet properties");
                }
            }

            if (Validator.isNull(_servletContextName)) {
                try {
                    String propsUtilServletContextName = PropsUtil.get(
                            "jcondo-manager-portlet-deployment-context");

                    if (Validator.isNotNull(propsUtilServletContextName)) {
                        _servletContextName = propsUtilServletContextName;
                    }
                } catch (Throwable t) {
                    if (_log.isInfoEnabled()) {
                        _log.info(
                            "Unable to locate deployment context from portal properties");
                    }
                }
            }

            if (Validator.isNull(_servletContextName)) {
                _servletContextName = "jcondo-manager-portlet";
            }

            return _servletContextName;
        }
    }

    public static Object translateInput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(AccessClp.class.getName())) {
            return translateInputAccess(oldModel);
        }

        if (oldModelClassName.equals(AccessPermissionClp.class.getName())) {
            return translateInputAccessPermission(oldModel);
        }

        if (oldModelClassName.equals(DepartmentClp.class.getName())) {
            return translateInputDepartment(oldModel);
        }

        if (oldModelClassName.equals(EnterpriseClp.class.getName())) {
            return translateInputEnterprise(oldModel);
        }

        if (oldModelClassName.equals(FlatClp.class.getName())) {
            return translateInputFlat(oldModel);
        }

        if (oldModelClassName.equals(KinshipClp.class.getName())) {
            return translateInputKinship(oldModel);
        }

        if (oldModelClassName.equals(MailClp.class.getName())) {
            return translateInputMail(oldModel);
        }

        if (oldModelClassName.equals(MailNoteClp.class.getName())) {
            return translateInputMailNote(oldModel);
        }

        if (oldModelClassName.equals(MembershipClp.class.getName())) {
            return translateInputMembership(oldModel);
        }

        if (oldModelClassName.equals(ParkingClp.class.getName())) {
            return translateInputParking(oldModel);
        }

        if (oldModelClassName.equals(PersonClp.class.getName())) {
            return translateInputPerson(oldModel);
        }

        if (oldModelClassName.equals(PetClp.class.getName())) {
            return translateInputPet(oldModel);
        }

        if (oldModelClassName.equals(TelephoneClp.class.getName())) {
            return translateInputTelephone(oldModel);
        }

        if (oldModelClassName.equals(VehicleClp.class.getName())) {
            return translateInputVehicle(oldModel);
        }

        return oldModel;
    }

    public static Object translateInput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateInput(curObj));
        }

        return newList;
    }

    public static Object translateInputAccess(BaseModel<?> oldModel) {
        AccessClp oldClpModel = (AccessClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getAccessRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputAccessPermission(BaseModel<?> oldModel) {
        AccessPermissionClp oldClpModel = (AccessPermissionClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getAccessPermissionRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputDepartment(BaseModel<?> oldModel) {
        DepartmentClp oldClpModel = (DepartmentClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getDepartmentRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputEnterprise(BaseModel<?> oldModel) {
        EnterpriseClp oldClpModel = (EnterpriseClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getEnterpriseRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputFlat(BaseModel<?> oldModel) {
        FlatClp oldClpModel = (FlatClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getFlatRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputKinship(BaseModel<?> oldModel) {
        KinshipClp oldClpModel = (KinshipClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getKinshipRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputMail(BaseModel<?> oldModel) {
        MailClp oldClpModel = (MailClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getMailRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputMailNote(BaseModel<?> oldModel) {
        MailNoteClp oldClpModel = (MailNoteClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getMailNoteRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputMembership(BaseModel<?> oldModel) {
        MembershipClp oldClpModel = (MembershipClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getMembershipRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputParking(BaseModel<?> oldModel) {
        ParkingClp oldClpModel = (ParkingClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getParkingRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPerson(BaseModel<?> oldModel) {
        PersonClp oldClpModel = (PersonClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPersonRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputPet(BaseModel<?> oldModel) {
        PetClp oldClpModel = (PetClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getPetRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputTelephone(BaseModel<?> oldModel) {
        TelephoneClp oldClpModel = (TelephoneClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getTelephoneRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInputVehicle(BaseModel<?> oldModel) {
        VehicleClp oldClpModel = (VehicleClp) oldModel;

        BaseModel<?> newModel = oldClpModel.getVehicleRemoteModel();

        newModel.setModelAttributes(oldClpModel.getModelAttributes());

        return newModel;
    }

    public static Object translateInput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateInput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateInput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Object translateOutput(BaseModel<?> oldModel) {
        Class<?> oldModelClass = oldModel.getClass();

        String oldModelClassName = oldModelClass.getName();

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.AccessImpl")) {
            return translateOutputAccess(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.AccessPermissionImpl")) {
            return translateOutputAccessPermission(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.DepartmentImpl")) {
            return translateOutputDepartment(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.EnterpriseImpl")) {
            return translateOutputEnterprise(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.FlatImpl")) {
            return translateOutputFlat(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.KinshipImpl")) {
            return translateOutputKinship(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.MailImpl")) {
            return translateOutputMail(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.MailNoteImpl")) {
            return translateOutputMailNote(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.MembershipImpl")) {
            return translateOutputMembership(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.ParkingImpl")) {
            return translateOutputParking(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.PersonImpl")) {
            return translateOutputPerson(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.PetImpl")) {
            return translateOutputPet(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.TelephoneImpl")) {
            return translateOutputTelephone(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        if (oldModelClassName.equals(
                    "br.com.atilo.jcondo.manager.model.impl.VehicleImpl")) {
            return translateOutputVehicle(oldModel);
        } else if (oldModelClassName.endsWith("Clp")) {
            try {
                ClassLoader classLoader = ClpSerializer.class.getClassLoader();

                Method getClpSerializerClassMethod = oldModelClass.getMethod(
                        "getClpSerializerClass");

                Class<?> oldClpSerializerClass = (Class<?>) getClpSerializerClassMethod.invoke(oldModel);

                Class<?> newClpSerializerClass = classLoader.loadClass(oldClpSerializerClass.getName());

                Method translateOutputMethod = newClpSerializerClass.getMethod("translateOutput",
                        BaseModel.class);

                Class<?> oldModelModelClass = oldModel.getModelClass();

                Method getRemoteModelMethod = oldModelClass.getMethod("get" +
                        oldModelModelClass.getSimpleName() + "RemoteModel");

                Object oldRemoteModel = getRemoteModelMethod.invoke(oldModel);

                BaseModel<?> newModel = (BaseModel<?>) translateOutputMethod.invoke(null,
                        oldRemoteModel);

                return newModel;
            } catch (Throwable t) {
                if (_log.isInfoEnabled()) {
                    _log.info("Unable to translate " + oldModelClassName, t);
                }
            }
        }

        return oldModel;
    }

    public static Object translateOutput(List<Object> oldList) {
        List<Object> newList = new ArrayList<Object>(oldList.size());

        for (int i = 0; i < oldList.size(); i++) {
            Object curObj = oldList.get(i);

            newList.add(translateOutput(curObj));
        }

        return newList;
    }

    public static Object translateOutput(Object obj) {
        if (obj instanceof BaseModel<?>) {
            return translateOutput((BaseModel<?>) obj);
        } else if (obj instanceof List<?>) {
            return translateOutput((List<Object>) obj);
        } else {
            return obj;
        }
    }

    public static Throwable translateThrowable(Throwable throwable) {
        if (_useReflectionToTranslateThrowable) {
            try {
                UnsyncByteArrayOutputStream unsyncByteArrayOutputStream = new UnsyncByteArrayOutputStream();
                ObjectOutputStream objectOutputStream = new ObjectOutputStream(unsyncByteArrayOutputStream);

                objectOutputStream.writeObject(throwable);

                objectOutputStream.flush();
                objectOutputStream.close();

                UnsyncByteArrayInputStream unsyncByteArrayInputStream = new UnsyncByteArrayInputStream(unsyncByteArrayOutputStream.unsafeGetByteArray(),
                        0, unsyncByteArrayOutputStream.size());

                Thread currentThread = Thread.currentThread();

                ClassLoader contextClassLoader = currentThread.getContextClassLoader();

                ObjectInputStream objectInputStream = new ClassLoaderObjectInputStream(unsyncByteArrayInputStream,
                        contextClassLoader);

                throwable = (Throwable) objectInputStream.readObject();

                objectInputStream.close();

                return throwable;
            } catch (SecurityException se) {
                if (_log.isInfoEnabled()) {
                    _log.info("Do not use reflection to translate throwable");
                }

                _useReflectionToTranslateThrowable = false;
            } catch (Throwable throwable2) {
                _log.error(throwable2, throwable2);

                return throwable2;
            }
        }

        Class<?> clazz = throwable.getClass();

        String className = clazz.getName();

        if (className.equals(PortalException.class.getName())) {
            return new PortalException();
        }

        if (className.equals(SystemException.class.getName())) {
            return new SystemException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchDomainException")) {
            return new br.com.atilo.jcondo.manager.NoSuchDomainException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchAccessException")) {
            return new br.com.atilo.jcondo.manager.NoSuchAccessException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchAccessPermissionException")) {
            return new br.com.atilo.jcondo.manager.NoSuchAccessPermissionException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchDepartmentException")) {
            return new br.com.atilo.jcondo.manager.NoSuchDepartmentException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchEnterpriseException")) {
            return new br.com.atilo.jcondo.manager.NoSuchEnterpriseException();
        }

        if (className.equals("br.com.atilo.jcondo.manager.NoSuchFlatException")) {
            return new br.com.atilo.jcondo.manager.NoSuchFlatException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchKinshipException")) {
            return new br.com.atilo.jcondo.manager.NoSuchKinshipException();
        }

        if (className.equals("br.com.atilo.jcondo.manager.NoSuchMailException")) {
            return new br.com.atilo.jcondo.manager.NoSuchMailException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchMailNoteException")) {
            return new br.com.atilo.jcondo.manager.NoSuchMailNoteException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchMembershipException")) {
            return new br.com.atilo.jcondo.manager.NoSuchMembershipException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchParkingException")) {
            return new br.com.atilo.jcondo.manager.NoSuchParkingException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchPersonException")) {
            return new br.com.atilo.jcondo.manager.NoSuchPersonException();
        }

        if (className.equals("br.com.atilo.jcondo.manager.NoSuchPetException")) {
            return new br.com.atilo.jcondo.manager.NoSuchPetException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchTelephoneException")) {
            return new br.com.atilo.jcondo.manager.NoSuchTelephoneException();
        }

        if (className.equals(
                    "br.com.atilo.jcondo.manager.NoSuchVehicleException")) {
            return new br.com.atilo.jcondo.manager.NoSuchVehicleException();
        }

        return throwable;
    }

    public static Object translateOutputAccess(BaseModel<?> oldModel) {
        AccessClp newModel = new AccessClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setAccessRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputAccessPermission(BaseModel<?> oldModel) {
        AccessPermissionClp newModel = new AccessPermissionClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setAccessPermissionRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputDepartment(BaseModel<?> oldModel) {
        DepartmentClp newModel = new DepartmentClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setDepartmentRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputEnterprise(BaseModel<?> oldModel) {
        EnterpriseClp newModel = new EnterpriseClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setEnterpriseRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputFlat(BaseModel<?> oldModel) {
        FlatClp newModel = new FlatClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setFlatRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputKinship(BaseModel<?> oldModel) {
        KinshipClp newModel = new KinshipClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setKinshipRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputMail(BaseModel<?> oldModel) {
        MailClp newModel = new MailClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setMailRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputMailNote(BaseModel<?> oldModel) {
        MailNoteClp newModel = new MailNoteClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setMailNoteRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputMembership(BaseModel<?> oldModel) {
        MembershipClp newModel = new MembershipClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setMembershipRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputParking(BaseModel<?> oldModel) {
        ParkingClp newModel = new ParkingClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setParkingRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPerson(BaseModel<?> oldModel) {
        PersonClp newModel = new PersonClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPersonRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputPet(BaseModel<?> oldModel) {
        PetClp newModel = new PetClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setPetRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputTelephone(BaseModel<?> oldModel) {
        TelephoneClp newModel = new TelephoneClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setTelephoneRemoteModel(oldModel);

        return newModel;
    }

    public static Object translateOutputVehicle(BaseModel<?> oldModel) {
        VehicleClp newModel = new VehicleClp();

        newModel.setModelAttributes(oldModel.getModelAttributes());

        newModel.setVehicleRemoteModel(oldModel);

        return newModel;
    }
}
