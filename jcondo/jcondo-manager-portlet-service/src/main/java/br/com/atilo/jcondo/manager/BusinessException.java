package br.com.atilo.jcondo.manager;

import com.liferay.portal.kernel.exception.SystemException;

public class BusinessException extends SystemException {

	private static final long serialVersionUID = 1L;

	protected Object[] args;	

	public BusinessException(String message, Object ... args) {
		super(message);
		this.args = args;
	}

	public Object[] getArgs() {
		return args;
	}

	public void setArgs(Object[] args) {
		this.args = args;
	}
}
