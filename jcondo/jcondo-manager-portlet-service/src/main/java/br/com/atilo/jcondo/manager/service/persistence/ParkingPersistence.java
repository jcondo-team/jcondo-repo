package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Parking;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the parking service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ParkingPersistenceImpl
 * @see ParkingUtil
 * @generated
 */
public interface ParkingPersistence extends BasePersistence<Parking> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link ParkingUtil} to access the parking persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the parkings where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @return the matching parkings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByOwnerDomain(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the parkings where ownerDomainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerDomainId the owner domain ID
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @return the range of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByOwnerDomain(
        long ownerDomainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the parkings where ownerDomainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerDomainId the owner domain ID
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByOwnerDomain(
        long ownerDomainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking findByOwnerDomain_First(
        long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching parking, or <code>null</code> if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking fetchByOwnerDomain_First(
        long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking findByOwnerDomain_Last(
        long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching parking, or <code>null</code> if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking fetchByOwnerDomain_Last(
        long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the parkings before and after the current parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param id the primary key of the current parking
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking[] findByOwnerDomain_PrevAndNext(
        long id, long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the parkings where ownerDomainId = &#63; from the database.
    *
    * @param ownerDomainId the owner domain ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByOwnerDomain(long ownerDomainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of parkings where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @return the number of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public int countByOwnerDomain(long ownerDomainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the parkings where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @return the matching parkings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByRenterDomain(
        long renterDomainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the parkings where renterDomainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param renterDomainId the renter domain ID
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @return the range of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByRenterDomain(
        long renterDomainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the parkings where renterDomainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param renterDomainId the renter domain ID
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByRenterDomain(
        long renterDomainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first parking in the ordered set where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking findByRenterDomain_First(
        long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first parking in the ordered set where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching parking, or <code>null</code> if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking fetchByRenterDomain_First(
        long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last parking in the ordered set where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking findByRenterDomain_Last(
        long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last parking in the ordered set where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching parking, or <code>null</code> if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking fetchByRenterDomain_Last(
        long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the parkings before and after the current parking in the ordered set where renterDomainId = &#63;.
    *
    * @param id the primary key of the current parking
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking[] findByRenterDomain_PrevAndNext(
        long id, long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the parkings where renterDomainId = &#63; from the database.
    *
    * @param renterDomainId the renter domain ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByRenterDomain(long renterDomainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of parkings where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @return the number of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public int countByRenterDomain(long renterDomainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the parking in the entity cache if it is enabled.
    *
    * @param parking the parking
    */
    public void cacheResult(br.com.atilo.jcondo.manager.model.Parking parking);

    /**
    * Caches the parkings in the entity cache if it is enabled.
    *
    * @param parkings the parkings
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Parking> parkings);

    /**
    * Creates a new parking with the primary key. Does not add the parking to the database.
    *
    * @param id the primary key for the new parking
    * @return the new parking
    */
    public br.com.atilo.jcondo.manager.model.Parking create(long id);

    /**
    * Removes the parking with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the parking
    * @return the parking that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Parking updateImpl(
        br.com.atilo.jcondo.manager.model.Parking parking)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the parking with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchParkingException} if it could not be found.
    *
    * @param id the primary key of the parking
    * @return the parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking findByPrimaryKey(long id)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the parking with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the parking
    * @return the parking, or <code>null</code> if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Parking fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the parkings.
    *
    * @return the parkings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the parkings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @return the range of parkings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the parkings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of parkings
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the parkings from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of parkings.
    *
    * @return the number of parkings
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
