package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link ParkingService}.
 *
 * @author Brian Wing Shun Chan
 * @see ParkingService
 * @generated
 */
public class ParkingServiceWrapper implements ParkingService,
    ServiceWrapper<ParkingService> {
    private ParkingService _parkingService;

    public ParkingServiceWrapper(ParkingService parkingService) {
        _parkingService = parkingService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _parkingService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _parkingService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _parkingService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking addParking(
        java.lang.String code, br.com.atilo.jcondo.datatype.ParkingType type,
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingService.addParking(code, type, ownerDomainId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking updateParking(
        long parkingId, java.lang.String code,
        br.com.atilo.jcondo.datatype.ParkingType type, long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingService.updateParking(parkingId, code, type,
            ownerDomainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> getOwnedParkings(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingService.getOwnedParkings(ownerDomainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> getRentedParkings(
        long renterDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingService.getRentedParkings(renterDomainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Parking> getGrantedParkings(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingService.getGrantedParkings(ownerDomainId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking rent(long ownerDomainId,
        long renterDomainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingService.rent(ownerDomainId, renterDomainId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking unrent(long parkingId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parkingService.unrent(parkingId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public ParkingService getWrappedParkingService() {
        return _parkingService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedParkingService(ParkingService parkingService) {
        _parkingService = parkingService;
    }

    @Override
    public ParkingService getWrappedService() {
        return _parkingService;
    }

    @Override
    public void setWrappedService(ParkingService parkingService) {
        _parkingService = parkingService;
    }
}
