package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Person service. Represents a row in the &quot;jco_person&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see PersonModel
 * @see br.com.atilo.jcondo.manager.model.impl.PersonImpl
 * @see br.com.atilo.jcondo.manager.model.impl.PersonModelImpl
 * @generated
 */
public interface Person extends PersonModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.PersonImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public java.lang.String getName()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setName(java.lang.String name);

    public java.lang.String getSurname()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setSurname(java.lang.String surname);

    public java.lang.String getFullName()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.datatype.Gender getGender()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setGender(br.com.atilo.jcondo.datatype.Gender gender)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.util.Date getBirthday()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setBirthday(java.util.Date birthday)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public java.lang.String getEmail()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setEmail(java.lang.String email)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.Image getPortrait()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setPortrait(br.com.atilo.jcondo.Image image);

    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> getMemberships()
        throws com.liferay.portal.kernel.exception.SystemException;

    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getTelephones()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.datatype.PersonType getType(long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.model.User getUser();

    public void setUser(com.liferay.portal.model.User user)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.model.BaseModel<?> getDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setDomain(com.liferay.portal.model.BaseModel<?> domain)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;
}
