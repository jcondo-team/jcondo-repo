package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Department}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Department
 * @generated
 */
public class DepartmentWrapper implements Department, ModelWrapper<Department> {
    private Department _department;

    public DepartmentWrapper(Department department) {
        _department = department;
    }

    @Override
    public Class<?> getModelClass() {
        return Department.class;
    }

    @Override
    public String getModelClassName() {
        return Department.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("organizationId", getOrganizationId());
        attributes.put("name", getName());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long organizationId = (Long) attributes.get("organizationId");

        if (organizationId != null) {
            setOrganizationId(organizationId);
        }

        String name = (String) attributes.get("name");

        if (name != null) {
            setName(name);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this department.
    *
    * @return the primary key of this department
    */
    @Override
    public long getPrimaryKey() {
        return _department.getPrimaryKey();
    }

    /**
    * Sets the primary key of this department.
    *
    * @param primaryKey the primary key of this department
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _department.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this department.
    *
    * @return the ID of this department
    */
    @Override
    public long getId() {
        return _department.getId();
    }

    /**
    * Sets the ID of this department.
    *
    * @param id the ID of this department
    */
    @Override
    public void setId(long id) {
        _department.setId(id);
    }

    /**
    * Returns the organization ID of this department.
    *
    * @return the organization ID of this department
    */
    @Override
    public long getOrganizationId() {
        return _department.getOrganizationId();
    }

    /**
    * Sets the organization ID of this department.
    *
    * @param organizationId the organization ID of this department
    */
    @Override
    public void setOrganizationId(long organizationId) {
        _department.setOrganizationId(organizationId);
    }

    /**
    * Returns the name of this department.
    *
    * @return the name of this department
    */
    @Override
    public java.lang.String getName() {
        return _department.getName();
    }

    /**
    * Sets the name of this department.
    *
    * @param name the name of this department
    */
    @Override
    public void setName(java.lang.String name) {
        _department.setName(name);
    }

    /**
    * Returns the opr date of this department.
    *
    * @return the opr date of this department
    */
    @Override
    public java.util.Date getOprDate() {
        return _department.getOprDate();
    }

    /**
    * Sets the opr date of this department.
    *
    * @param oprDate the opr date of this department
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _department.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this department.
    *
    * @return the opr user of this department
    */
    @Override
    public long getOprUser() {
        return _department.getOprUser();
    }

    /**
    * Sets the opr user of this department.
    *
    * @param oprUser the opr user of this department
    */
    @Override
    public void setOprUser(long oprUser) {
        _department.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _department.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _department.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _department.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _department.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _department.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _department.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _department.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _department.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _department.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _department.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _department.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new DepartmentWrapper((Department) _department.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.manager.model.Department department) {
        return _department.compareTo(department);
    }

    @Override
    public int hashCode() {
        return _department.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Department> toCacheModel() {
        return _department.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Department toEscapedModel() {
        return new DepartmentWrapper(_department.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Department toUnescapedModel() {
        return new DepartmentWrapper(_department.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _department.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _department.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _department.persist();
    }

    @Override
    public void setType(br.com.atilo.jcondo.datatype.DomainType type) {
        _department.setType(type);
    }

    @Override
    public br.com.atilo.jcondo.datatype.DomainType getType()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _department.getType();
    }

    @Override
    public void setDescription(java.lang.String description) {
        _department.setDescription(description);
    }

    @Override
    public java.lang.String getDescription()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _department.getDescription();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getMembers()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _department.getMembers();
    }

    @Override
    public com.liferay.portal.model.Organization getOrganization() {
        return _department.getOrganization();
    }

    @Override
    public void setOrganization(
        com.liferay.portal.model.Organization organization) {
        _department.setOrganization(organization);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof DepartmentWrapper)) {
            return false;
        }

        DepartmentWrapper departmentWrapper = (DepartmentWrapper) obj;

        if (Validator.equals(_department, departmentWrapper._department)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Department getWrappedDepartment() {
        return _department;
    }

    @Override
    public Department getWrappedModel() {
        return _department;
    }

    @Override
    public void resetOriginalValues() {
        _department.resetOriginalValues();
    }
}
