package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Telephone;
import br.com.atilo.jcondo.manager.service.TelephoneLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class TelephoneActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public TelephoneActionableDynamicQuery() throws SystemException {
        setBaseLocalService(TelephoneLocalServiceUtil.getService());
        setClass(Telephone.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
