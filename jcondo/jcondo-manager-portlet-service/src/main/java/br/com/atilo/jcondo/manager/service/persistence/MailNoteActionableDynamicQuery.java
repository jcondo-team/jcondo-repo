package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.MailNote;
import br.com.atilo.jcondo.manager.service.MailNoteLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class MailNoteActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public MailNoteActionableDynamicQuery() throws SystemException {
        setBaseLocalService(MailNoteLocalServiceUtil.getService());
        setClass(MailNote.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
