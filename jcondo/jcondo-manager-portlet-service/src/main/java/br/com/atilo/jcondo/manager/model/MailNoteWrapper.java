package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link MailNote}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MailNote
 * @generated
 */
public class MailNoteWrapper implements MailNote, ModelWrapper<MailNote> {
    private MailNote _mailNote;

    public MailNoteWrapper(MailNote mailNote) {
        _mailNote = mailNote;
    }

    @Override
    public Class<?> getModelClass() {
        return MailNote.class;
    }

    @Override
    public String getModelClassName() {
        return MailNote.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("mailId", getMailId());
        attributes.put("description", getDescription());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long mailId = (Long) attributes.get("mailId");

        if (mailId != null) {
            setMailId(mailId);
        }

        String description = (String) attributes.get("description");

        if (description != null) {
            setDescription(description);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this mail note.
    *
    * @return the primary key of this mail note
    */
    @Override
    public long getPrimaryKey() {
        return _mailNote.getPrimaryKey();
    }

    /**
    * Sets the primary key of this mail note.
    *
    * @param primaryKey the primary key of this mail note
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _mailNote.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this mail note.
    *
    * @return the ID of this mail note
    */
    @Override
    public long getId() {
        return _mailNote.getId();
    }

    /**
    * Sets the ID of this mail note.
    *
    * @param id the ID of this mail note
    */
    @Override
    public void setId(long id) {
        _mailNote.setId(id);
    }

    /**
    * Returns the mail ID of this mail note.
    *
    * @return the mail ID of this mail note
    */
    @Override
    public long getMailId() {
        return _mailNote.getMailId();
    }

    /**
    * Sets the mail ID of this mail note.
    *
    * @param mailId the mail ID of this mail note
    */
    @Override
    public void setMailId(long mailId) {
        _mailNote.setMailId(mailId);
    }

    /**
    * Returns the description of this mail note.
    *
    * @return the description of this mail note
    */
    @Override
    public java.lang.String getDescription() {
        return _mailNote.getDescription();
    }

    /**
    * Sets the description of this mail note.
    *
    * @param description the description of this mail note
    */
    @Override
    public void setDescription(java.lang.String description) {
        _mailNote.setDescription(description);
    }

    /**
    * Returns the opr date of this mail note.
    *
    * @return the opr date of this mail note
    */
    @Override
    public java.util.Date getOprDate() {
        return _mailNote.getOprDate();
    }

    /**
    * Sets the opr date of this mail note.
    *
    * @param oprDate the opr date of this mail note
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _mailNote.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this mail note.
    *
    * @return the opr user of this mail note
    */
    @Override
    public long getOprUser() {
        return _mailNote.getOprUser();
    }

    /**
    * Sets the opr user of this mail note.
    *
    * @param oprUser the opr user of this mail note
    */
    @Override
    public void setOprUser(long oprUser) {
        _mailNote.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _mailNote.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _mailNote.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _mailNote.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _mailNote.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _mailNote.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _mailNote.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _mailNote.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _mailNote.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _mailNote.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _mailNote.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _mailNote.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new MailNoteWrapper((MailNote) _mailNote.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.MailNote mailNote) {
        return _mailNote.compareTo(mailNote);
    }

    @Override
    public int hashCode() {
        return _mailNote.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.MailNote> toCacheModel() {
        return _mailNote.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.MailNote toEscapedModel() {
        return new MailNoteWrapper(_mailNote.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.MailNote toUnescapedModel() {
        return new MailNoteWrapper(_mailNote.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _mailNote.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _mailNote.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _mailNote.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MailNoteWrapper)) {
            return false;
        }

        MailNoteWrapper mailNoteWrapper = (MailNoteWrapper) obj;

        if (Validator.equals(_mailNote, mailNoteWrapper._mailNote)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public MailNote getWrappedMailNote() {
        return _mailNote;
    }

    @Override
    public MailNote getWrappedModel() {
        return _mailNote;
    }

    @Override
    public void resetOriginalValues() {
        _mailNote.resetOriginalValues();
    }
}
