package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Membership}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Membership
 * @generated
 */
public class MembershipWrapper implements Membership, ModelWrapper<Membership> {
    private Membership _membership;

    public MembershipWrapper(Membership membership) {
        _membership = membership;
    }

    @Override
    public Class<?> getModelClass() {
        return Membership.class;
    }

    @Override
    public String getModelClassName() {
        return Membership.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("personId", getPersonId());
        attributes.put("domainId", getDomainId());
        attributes.put("typeId", getTypeId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this membership.
    *
    * @return the primary key of this membership
    */
    @Override
    public long getPrimaryKey() {
        return _membership.getPrimaryKey();
    }

    /**
    * Sets the primary key of this membership.
    *
    * @param primaryKey the primary key of this membership
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _membership.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this membership.
    *
    * @return the ID of this membership
    */
    @Override
    public long getId() {
        return _membership.getId();
    }

    /**
    * Sets the ID of this membership.
    *
    * @param id the ID of this membership
    */
    @Override
    public void setId(long id) {
        _membership.setId(id);
    }

    /**
    * Returns the person ID of this membership.
    *
    * @return the person ID of this membership
    */
    @Override
    public long getPersonId() {
        return _membership.getPersonId();
    }

    /**
    * Sets the person ID of this membership.
    *
    * @param personId the person ID of this membership
    */
    @Override
    public void setPersonId(long personId) {
        _membership.setPersonId(personId);
    }

    /**
    * Returns the domain ID of this membership.
    *
    * @return the domain ID of this membership
    */
    @Override
    public long getDomainId() {
        return _membership.getDomainId();
    }

    /**
    * Sets the domain ID of this membership.
    *
    * @param domainId the domain ID of this membership
    */
    @Override
    public void setDomainId(long domainId) {
        _membership.setDomainId(domainId);
    }

    /**
    * Returns the type ID of this membership.
    *
    * @return the type ID of this membership
    */
    @Override
    public int getTypeId() {
        return _membership.getTypeId();
    }

    /**
    * Sets the type ID of this membership.
    *
    * @param typeId the type ID of this membership
    */
    @Override
    public void setTypeId(int typeId) {
        _membership.setTypeId(typeId);
    }

    /**
    * Returns the opr date of this membership.
    *
    * @return the opr date of this membership
    */
    @Override
    public java.util.Date getOprDate() {
        return _membership.getOprDate();
    }

    /**
    * Sets the opr date of this membership.
    *
    * @param oprDate the opr date of this membership
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _membership.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this membership.
    *
    * @return the opr user of this membership
    */
    @Override
    public long getOprUser() {
        return _membership.getOprUser();
    }

    /**
    * Sets the opr user of this membership.
    *
    * @param oprUser the opr user of this membership
    */
    @Override
    public void setOprUser(long oprUser) {
        _membership.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _membership.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _membership.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _membership.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _membership.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _membership.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _membership.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _membership.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _membership.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _membership.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _membership.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _membership.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new MembershipWrapper((Membership) _membership.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.manager.model.Membership membership) {
        return _membership.compareTo(membership);
    }

    @Override
    public int hashCode() {
        return _membership.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Membership> toCacheModel() {
        return _membership.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Membership toEscapedModel() {
        return new MembershipWrapper(_membership.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Membership toUnescapedModel() {
        return new MembershipWrapper(_membership.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _membership.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _membership.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _membership.persist();
    }

    @Override
    public br.com.atilo.jcondo.datatype.PersonType getType() {
        return _membership.getType();
    }

    @Override
    public void setType(br.com.atilo.jcondo.datatype.PersonType type) {
        _membership.setType(type);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _membership.getPerson();
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _membership.getDomain();
    }

    @Override
    public void setDomain(com.liferay.portal.model.BaseModel<?> domain) {
        _membership.setDomain(domain);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MembershipWrapper)) {
            return false;
        }

        MembershipWrapper membershipWrapper = (MembershipWrapper) obj;

        if (Validator.equals(_membership, membershipWrapper._membership)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Membership getWrappedMembership() {
        return _membership;
    }

    @Override
    public Membership getWrappedModel() {
        return _membership;
    }

    @Override
    public void resetOriginalValues() {
        _membership.resetOriginalValues();
    }
}
