package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Pet service. Represents a row in the &quot;jco_pet&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see PetModel
 * @see br.com.atilo.jcondo.manager.model.impl.PetImpl
 * @see br.com.atilo.jcondo.manager.model.impl.PetModelImpl
 * @generated
 */
public interface Pet extends PetModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.PetImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
