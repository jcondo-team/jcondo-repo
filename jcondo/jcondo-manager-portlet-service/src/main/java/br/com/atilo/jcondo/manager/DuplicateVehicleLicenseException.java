package br.com.atilo.jcondo.manager;

public class DuplicateVehicleLicenseException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public DuplicateVehicleLicenseException(String message, Object ... args) {
		super(message, args);
	}

}
