package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Telephone}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Telephone
 * @generated
 */
public class TelephoneWrapper implements Telephone, ModelWrapper<Telephone> {
    private Telephone _telephone;

    public TelephoneWrapper(Telephone telephone) {
        _telephone = telephone;
    }

    @Override
    public Class<?> getModelClass() {
        return Telephone.class;
    }

    @Override
    public String getModelClassName() {
        return Telephone.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("typeName", getTypeName());
        attributes.put("extension", getExtension());
        attributes.put("number", getNumber());
        attributes.put("primary", getPrimary());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String typeName = (String) attributes.get("typeName");

        if (typeName != null) {
            setTypeName(typeName);
        }

        Long extension = (Long) attributes.get("extension");

        if (extension != null) {
            setExtension(extension);
        }

        Long number = (Long) attributes.get("number");

        if (number != null) {
            setNumber(number);
        }

        Boolean primary = (Boolean) attributes.get("primary");

        if (primary != null) {
            setPrimary(primary);
        }
    }

    /**
    * Returns the primary key of this telephone.
    *
    * @return the primary key of this telephone
    */
    @Override
    public long getPrimaryKey() {
        return _telephone.getPrimaryKey();
    }

    /**
    * Sets the primary key of this telephone.
    *
    * @param primaryKey the primary key of this telephone
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _telephone.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this telephone.
    *
    * @return the ID of this telephone
    */
    @Override
    public long getId() {
        return _telephone.getId();
    }

    /**
    * Sets the ID of this telephone.
    *
    * @param id the ID of this telephone
    */
    @Override
    public void setId(long id) {
        _telephone.setId(id);
    }

    /**
    * Returns the type name of this telephone.
    *
    * @return the type name of this telephone
    */
    @Override
    public java.lang.String getTypeName() {
        return _telephone.getTypeName();
    }

    /**
    * Sets the type name of this telephone.
    *
    * @param typeName the type name of this telephone
    */
    @Override
    public void setTypeName(java.lang.String typeName) {
        _telephone.setTypeName(typeName);
    }

    /**
    * Returns the extension of this telephone.
    *
    * @return the extension of this telephone
    */
    @Override
    public long getExtension() {
        return _telephone.getExtension();
    }

    /**
    * Sets the extension of this telephone.
    *
    * @param extension the extension of this telephone
    */
    @Override
    public void setExtension(long extension) {
        _telephone.setExtension(extension);
    }

    /**
    * Returns the number of this telephone.
    *
    * @return the number of this telephone
    */
    @Override
    public long getNumber() {
        return _telephone.getNumber();
    }

    /**
    * Sets the number of this telephone.
    *
    * @param number the number of this telephone
    */
    @Override
    public void setNumber(long number) {
        _telephone.setNumber(number);
    }

    /**
    * Returns the primary of this telephone.
    *
    * @return the primary of this telephone
    */
    @Override
    public boolean getPrimary() {
        return _telephone.getPrimary();
    }

    /**
    * Returns <code>true</code> if this telephone is primary.
    *
    * @return <code>true</code> if this telephone is primary; <code>false</code> otherwise
    */
    @Override
    public boolean isPrimary() {
        return _telephone.isPrimary();
    }

    /**
    * Sets whether this telephone is primary.
    *
    * @param primary the primary of this telephone
    */
    @Override
    public void setPrimary(boolean primary) {
        _telephone.setPrimary(primary);
    }

    @Override
    public boolean isNew() {
        return _telephone.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _telephone.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _telephone.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _telephone.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _telephone.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _telephone.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _telephone.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _telephone.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _telephone.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _telephone.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _telephone.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new TelephoneWrapper((Telephone) _telephone.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Telephone telephone) {
        return _telephone.compareTo(telephone);
    }

    @Override
    public int hashCode() {
        return _telephone.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Telephone> toCacheModel() {
        return _telephone.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone toEscapedModel() {
        return new TelephoneWrapper(_telephone.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Telephone toUnescapedModel() {
        return new TelephoneWrapper(_telephone.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _telephone.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _telephone.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _telephone.persist();
    }

    @Override
    public br.com.atilo.jcondo.datatype.PhoneType getType() {
        return _telephone.getType();
    }

    @Override
    public void setType(br.com.atilo.jcondo.datatype.PhoneType type) {
        _telephone.setType(type);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TelephoneWrapper)) {
            return false;
        }

        TelephoneWrapper telephoneWrapper = (TelephoneWrapper) obj;

        if (Validator.equals(_telephone, telephoneWrapper._telephone)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Telephone getWrappedTelephone() {
        return _telephone;
    }

    @Override
    public Telephone getWrappedModel() {
        return _telephone;
    }

    @Override
    public void resetOriginalValues() {
        _telephone.resetOriginalValues();
    }
}
