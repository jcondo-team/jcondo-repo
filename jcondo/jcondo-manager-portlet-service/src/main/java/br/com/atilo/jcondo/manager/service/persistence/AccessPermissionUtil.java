package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.AccessPermission;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the access permission service. This utility wraps {@link AccessPermissionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AccessPermissionPersistence
 * @see AccessPermissionPersistenceImpl
 * @generated
 */
public class AccessPermissionUtil {
    private static AccessPermissionPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(AccessPermission accessPermission) {
        getPersistence().clearCache(accessPermission);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<AccessPermission> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<AccessPermission> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<AccessPermission> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static AccessPermission update(AccessPermission accessPermission)
        throws SystemException {
        return getPersistence().update(accessPermission);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static AccessPermission update(AccessPermission accessPermission,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(accessPermission, serviceContext);
    }

    /**
    * Returns all the access permissions where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @return the matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findByPersonAndDomain(
        long personId, long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPersonAndDomain(personId, domainId);
    }

    /**
    * Returns a range of all the access permissions where personId = &#63; and domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param start the lower bound of the range of access permissions
    * @param end the upper bound of the range of access permissions (not inclusive)
    * @return the range of matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findByPersonAndDomain(
        long personId, long domainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndDomain(personId, domainId, start, end);
    }

    /**
    * Returns an ordered range of all the access permissions where personId = &#63; and domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param start the lower bound of the range of access permissions
    * @param end the upper bound of the range of access permissions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findByPersonAndDomain(
        long personId, long domainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndDomain(personId, domainId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission findByPersonAndDomain_First(
        long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndDomain_First(personId, domainId,
            orderByComparator);
    }

    /**
    * Returns the first access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching access permission, or <code>null</code> if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission fetchByPersonAndDomain_First(
        long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPersonAndDomain_First(personId, domainId,
            orderByComparator);
    }

    /**
    * Returns the last access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission findByPersonAndDomain_Last(
        long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndDomain_Last(personId, domainId,
            orderByComparator);
    }

    /**
    * Returns the last access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching access permission, or <code>null</code> if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission fetchByPersonAndDomain_Last(
        long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPersonAndDomain_Last(personId, domainId,
            orderByComparator);
    }

    /**
    * Returns the access permissions before and after the current access permission in the ordered set where personId = &#63; and domainId = &#63;.
    *
    * @param id the primary key of the current access permission
    * @param personId the person ID
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission[] findByPersonAndDomain_PrevAndNext(
        long id, long personId, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndDomain_PrevAndNext(id, personId, domainId,
            orderByComparator);
    }

    /**
    * Removes all the access permissions where personId = &#63; and domainId = &#63; from the database.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByPersonAndDomain(long personId, long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByPersonAndDomain(personId, domainId);
    }

    /**
    * Returns the number of access permissions where personId = &#63; and domainId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @return the number of matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public static int countByPersonAndDomain(long personId, long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByPersonAndDomain(personId, domainId);
    }

    /**
    * Returns the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessPermissionException} if it could not be found.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @return the matching access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission findByPersonDomainAndWeekDay(
        long personId, long domainId, int weekDay)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonDomainAndWeekDay(personId, domainId, weekDay);
    }

    /**
    * Returns the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @return the matching access permission, or <code>null</code> if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission fetchByPersonDomainAndWeekDay(
        long personId, long domainId, int weekDay)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPersonDomainAndWeekDay(personId, domainId, weekDay);
    }

    /**
    * Returns the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching access permission, or <code>null</code> if a matching access permission could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission fetchByPersonDomainAndWeekDay(
        long personId, long domainId, int weekDay, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPersonDomainAndWeekDay(personId, domainId, weekDay,
            retrieveFromCache);
    }

    /**
    * Removes the access permission where personId = &#63; and domainId = &#63; and weekDay = &#63; from the database.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @return the access permission that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission removeByPersonDomainAndWeekDay(
        long personId, long domainId, int weekDay)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .removeByPersonDomainAndWeekDay(personId, domainId, weekDay);
    }

    /**
    * Returns the number of access permissions where personId = &#63; and domainId = &#63; and weekDay = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param weekDay the week day
    * @return the number of matching access permissions
    * @throws SystemException if a system exception occurred
    */
    public static int countByPersonDomainAndWeekDay(long personId,
        long domainId, int weekDay)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByPersonDomainAndWeekDay(personId, domainId, weekDay);
    }

    /**
    * Caches the access permission in the entity cache if it is enabled.
    *
    * @param accessPermission the access permission
    */
    public static void cacheResult(
        br.com.atilo.jcondo.manager.model.AccessPermission accessPermission) {
        getPersistence().cacheResult(accessPermission);
    }

    /**
    * Caches the access permissions in the entity cache if it is enabled.
    *
    * @param accessPermissions the access permissions
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> accessPermissions) {
        getPersistence().cacheResult(accessPermissions);
    }

    /**
    * Creates a new access permission with the primary key. Does not add the access permission to the database.
    *
    * @param id the primary key for the new access permission
    * @return the new access permission
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission create(
        long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the access permission with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the access permission
    * @return the access permission that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission remove(
        long id)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.manager.model.AccessPermission updateImpl(
        br.com.atilo.jcondo.manager.model.AccessPermission accessPermission)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(accessPermission);
    }

    /**
    * Returns the access permission with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessPermissionException} if it could not be found.
    *
    * @param id the primary key of the access permission
    * @return the access permission
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.manager.NoSuchAccessPermissionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the access permission with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the access permission
    * @return the access permission, or <code>null</code> if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.AccessPermission fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the access permissions.
    *
    * @return the access permissions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the access permissions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of access permissions
    * @param end the upper bound of the range of access permissions (not inclusive)
    * @return the range of access permissions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the access permissions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of access permissions
    * @param end the upper bound of the range of access permissions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of access permissions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the access permissions from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of access permissions.
    *
    * @return the number of access permissions
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static AccessPermissionPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (AccessPermissionPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.manager.service.ClpSerializer.getServletContextName(),
                    AccessPermissionPersistence.class.getName());

            ReferenceRegistry.registerReference(AccessPermissionUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(AccessPermissionPersistence persistence) {
    }
}
