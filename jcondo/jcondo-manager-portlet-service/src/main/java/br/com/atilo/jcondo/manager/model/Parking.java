package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Parking service. Represents a row in the &quot;jco_parking&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see ParkingModel
 * @see br.com.atilo.jcondo.manager.model.impl.ParkingImpl
 * @see br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl
 * @generated
 */
public interface Parking extends ParkingModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.ParkingImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.datatype.ParkingType getType();

    public com.liferay.portal.model.BaseModel<?> getOwnerDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.model.BaseModel<?> getRenterDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;
}
