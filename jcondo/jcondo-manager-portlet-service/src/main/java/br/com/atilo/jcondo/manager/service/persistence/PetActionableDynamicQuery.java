package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Pet;
import br.com.atilo.jcondo.manager.service.PetLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class PetActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public PetActionableDynamicQuery() throws SystemException {
        setBaseLocalService(PetLocalServiceUtil.getService());
        setClass(Pet.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("petId");
    }
}
