package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link FlatService}.
 *
 * @author Brian Wing Shun Chan
 * @see FlatService
 * @generated
 */
public class FlatServiceWrapper implements FlatService,
    ServiceWrapper<FlatService> {
    private FlatService _flatService;

    public FlatServiceWrapper(FlatService flatService) {
        _flatService = flatService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _flatService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _flatService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _flatService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat addFlat(int number,
        int block, boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatService.addFlat(number, block, disables, brigade, pets,
            petTypes);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat updateFlat(long id,
        boolean disables, boolean brigade, boolean pets,
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatService.updateFlat(id, disables, brigade, pets, petTypes);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> getPersonFlats(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatService.getPersonFlats(personId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat getFlat(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatService.getFlat(id);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Flat> getFlats()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatService.getFlats();
    }

    @Override
    public void setFlatDefaulting(long flatId, boolean isDefaulting)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _flatService.setFlatDefaulting(flatId, isDefaulting);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Flat setFlatPerson(long flatId,
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _flatService.setFlatPerson(flatId, personId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public FlatService getWrappedFlatService() {
        return _flatService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedFlatService(FlatService flatService) {
        _flatService = flatService;
    }

    @Override
    public FlatService getWrappedService() {
        return _flatService;
    }

    @Override
    public void setWrappedService(FlatService flatService) {
        _flatService = flatService;
    }
}
