package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link EnterpriseService}.
 *
 * @author Brian Wing Shun Chan
 * @see EnterpriseService
 * @generated
 */
public class EnterpriseServiceWrapper implements EnterpriseService,
    ServiceWrapper<EnterpriseService> {
    private EnterpriseService _enterpriseService;

    public EnterpriseServiceWrapper(EnterpriseService enterpriseService) {
        _enterpriseService = enterpriseService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _enterpriseService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _enterpriseService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _enterpriseService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise createEnterprise() {
        return _enterpriseService.createEnterprise();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise addEnterprise(
        java.lang.String identity, java.lang.String name,
        br.com.atilo.jcondo.datatype.DomainType type,
        br.com.atilo.jcondo.datatype.EnterpriseStatus status,
        java.lang.String description)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterpriseService.addEnterprise(identity, name, type, status,
            description);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise updateEnterprise(
        long enterpriseId, java.lang.String identity, java.lang.String name,
        br.com.atilo.jcondo.datatype.DomainType type,
        br.com.atilo.jcondo.datatype.EnterpriseStatus status,
        java.lang.String description)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterpriseService.updateEnterprise(enterpriseId, identity,
            name, type, status, description);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise getEnterprise(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterpriseService.getEnterprise(id);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> getEnterprisesByType(
        br.com.atilo.jcondo.datatype.DomainType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterpriseService.getEnterprisesByType(type);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise getEnterpriseByIdentity(
        java.lang.String identity)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _enterpriseService.getEnterpriseByIdentity(identity);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public EnterpriseService getWrappedEnterpriseService() {
        return _enterpriseService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedEnterpriseService(EnterpriseService enterpriseService) {
        _enterpriseService = enterpriseService;
    }

    @Override
    public EnterpriseService getWrappedService() {
        return _enterpriseService;
    }

    @Override
    public void setWrappedService(EnterpriseService enterpriseService) {
        _enterpriseService = enterpriseService;
    }
}
