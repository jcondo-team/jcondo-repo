package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Vehicle. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.VehicleServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see VehicleService
 * @see br.com.atilo.jcondo.manager.service.base.VehicleServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.VehicleServiceImpl
 * @generated
 */
public class VehicleServiceUtil {
    private static VehicleService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.VehicleServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle createVehicle() {
        return getService().createVehicle();
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle addVehicle(
        java.lang.String license,
        br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addVehicle(license, type, domainId, portrait, brand, color,
            remark);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle updateVehicle(
        long id, br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateVehicle(id, type, domainId, portrait, brand, color,
            remark);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle updateVehiclePortrait(
        long id, br.com.atilo.jcondo.Image portrait)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateVehiclePortrait(id, portrait);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle deleteVehicle(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicle(id);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle getVehicleByLicense(
        java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleByLicense(license);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getDomainVehicles(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainVehicles(domainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getDomainVehiclesByLicense(
        long domainId, java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainVehiclesByLicense(domainId, license);
    }

    public static void clearService() {
        _service = null;
    }

    public static VehicleService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    VehicleService.class.getName());

            if (invokableService instanceof VehicleService) {
                _service = (VehicleService) invokableService;
            } else {
                _service = new VehicleServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(VehicleServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(VehicleService service) {
    }
}
