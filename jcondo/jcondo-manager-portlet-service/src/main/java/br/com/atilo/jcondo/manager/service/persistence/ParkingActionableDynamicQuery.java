package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Parking;
import br.com.atilo.jcondo.manager.service.ParkingLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class ParkingActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public ParkingActionableDynamicQuery() throws SystemException {
        setBaseLocalService(ParkingLocalServiceUtil.getService());
        setClass(Parking.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
