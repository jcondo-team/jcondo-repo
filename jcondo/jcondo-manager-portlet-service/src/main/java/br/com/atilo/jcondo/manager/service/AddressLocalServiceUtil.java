package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Address. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.AddressLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see AddressLocalService
 * @see br.com.atilo.jcondo.manager.service.base.AddressLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.AddressLocalServiceImpl
 * @generated
 */
public class AddressLocalServiceUtil {
    private static AddressLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.AddressLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static com.liferay.portal.model.Address createAddress() {
        return getService().createAddress();
    }

    public static com.liferay.portal.model.Address getPersonAddress(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonAddress(personId);
    }

    public static com.liferay.portal.model.Address addPersonAddress(
        long personId, java.lang.String street, java.lang.String city,
        long stateId, java.lang.String postCode)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addPersonAddress(personId, street, city, stateId, postCode);
    }

    public static com.liferay.portal.model.Address getEnterpriseAddress(
        long enterpriseId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getEnterpriseAddress(enterpriseId);
    }

    public static com.liferay.portal.model.Address addEnterpriseAddress(
        long enterpriseId, java.lang.String street, java.lang.String city,
        long stateId, java.lang.String postCode)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addEnterpriseAddress(enterpriseId, street, city, stateId,
            postCode);
    }

    public static com.liferay.portal.model.Address updateAddress(
        long addressId, java.lang.String street, java.lang.String city,
        long stateId, java.lang.String postCode)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateAddress(addressId, street, city, stateId, postCode);
    }

    public static com.liferay.portal.model.Address deleteAddress(long addressId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteAddress(addressId);
    }

    public static void clearService() {
        _service = null;
    }

    public static AddressLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    AddressLocalService.class.getName());

            if (invokableLocalService instanceof AddressLocalService) {
                _service = (AddressLocalService) invokableLocalService;
            } else {
                _service = new AddressLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(AddressLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(AddressLocalService service) {
    }
}
