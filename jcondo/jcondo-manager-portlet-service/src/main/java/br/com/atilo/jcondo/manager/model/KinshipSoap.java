package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.KinshipServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.KinshipServiceSoap
 * @generated
 */
public class KinshipSoap implements Serializable {
    private long _id;
    private long _personId;
    private long _relativeId;
    private int _typeId;
    private Date _oprDate;
    private long _oprUser;

    public KinshipSoap() {
    }

    public static KinshipSoap toSoapModel(Kinship model) {
        KinshipSoap soapModel = new KinshipSoap();

        soapModel.setId(model.getId());
        soapModel.setPersonId(model.getPersonId());
        soapModel.setRelativeId(model.getRelativeId());
        soapModel.setTypeId(model.getTypeId());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static KinshipSoap[] toSoapModels(Kinship[] models) {
        KinshipSoap[] soapModels = new KinshipSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static KinshipSoap[][] toSoapModels(Kinship[][] models) {
        KinshipSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new KinshipSoap[models.length][models[0].length];
        } else {
            soapModels = new KinshipSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static KinshipSoap[] toSoapModels(List<Kinship> models) {
        List<KinshipSoap> soapModels = new ArrayList<KinshipSoap>(models.size());

        for (Kinship model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new KinshipSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getPersonId() {
        return _personId;
    }

    public void setPersonId(long personId) {
        _personId = personId;
    }

    public long getRelativeId() {
        return _relativeId;
    }

    public void setRelativeId(long relativeId) {
        _relativeId = relativeId;
    }

    public int getTypeId() {
        return _typeId;
    }

    public void setTypeId(int typeId) {
        _typeId = typeId;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
