package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.PetServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.PetServiceSoap
 * @generated
 */
public class PetSoap implements Serializable {
    private long _petId;
    private int _typeId;
    private long _flatId;
    private Date _oprDate;
    private long _oprUser;

    public PetSoap() {
    }

    public static PetSoap toSoapModel(Pet model) {
        PetSoap soapModel = new PetSoap();

        soapModel.setPetId(model.getPetId());
        soapModel.setTypeId(model.getTypeId());
        soapModel.setFlatId(model.getFlatId());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static PetSoap[] toSoapModels(Pet[] models) {
        PetSoap[] soapModels = new PetSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static PetSoap[][] toSoapModels(Pet[][] models) {
        PetSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new PetSoap[models.length][models[0].length];
        } else {
            soapModels = new PetSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static PetSoap[] toSoapModels(List<Pet> models) {
        List<PetSoap> soapModels = new ArrayList<PetSoap>(models.size());

        for (Pet model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new PetSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _petId;
    }

    public void setPrimaryKey(long pk) {
        setPetId(pk);
    }

    public long getPetId() {
        return _petId;
    }

    public void setPetId(long petId) {
        _petId = petId;
    }

    public int getTypeId() {
        return _typeId;
    }

    public void setTypeId(int typeId) {
        _typeId = typeId;
    }

    public long getFlatId() {
        return _flatId;
    }

    public void setFlatId(long flatId) {
        _flatId = flatId;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
