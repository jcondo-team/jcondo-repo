package br.com.atilo.jcondo.manager.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class AccessPermissionFinderUtil {
    private static AccessPermissionFinder _finder;

    public static java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findAccessPermission(
        long personId, long domainId, java.util.Date date) {
        return getFinder().findAccessPermission(personId, domainId, date);
    }

    public static AccessPermissionFinder getFinder() {
        if (_finder == null) {
            _finder = (AccessPermissionFinder) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.manager.service.ClpSerializer.getServletContextName(),
                    AccessPermissionFinder.class.getName());

            ReferenceRegistry.registerReference(AccessPermissionFinderUtil.class,
                "_finder");
        }

        return _finder;
    }

    public void setFinder(AccessPermissionFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(AccessPermissionFinderUtil.class,
            "_finder");
    }
}
