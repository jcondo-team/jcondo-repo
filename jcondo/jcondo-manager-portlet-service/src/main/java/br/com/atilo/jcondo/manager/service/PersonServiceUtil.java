package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Person. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.PersonServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see PersonService
 * @see br.com.atilo.jcondo.manager.service.base.PersonServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.PersonServiceImpl
 * @generated
 */
public class PersonServiceUtil {
    private static PersonService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.PersonServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Person createPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().createPerson();
    }

    public static br.com.atilo.jcondo.manager.model.Person addPerson(
        java.lang.String name, java.lang.String surname,
        java.lang.String identity, java.lang.String email,
        java.util.Date birthday, br.com.atilo.jcondo.datatype.Gender gender,
        java.lang.String remark,
        br.com.atilo.jcondo.datatype.PersonType personType, long domainId)
        throws java.lang.Exception {
        return getService()
                   .addPerson(name, surname, identity, email, birthday, gender,
            remark, personType, domainId);
    }

    public static br.com.atilo.jcondo.manager.model.Person updatePerson(
        long id, java.lang.String name, java.lang.String surname,
        java.lang.String identity, java.lang.String email,
        java.util.Date birthday, br.com.atilo.jcondo.datatype.Gender gender,
        java.lang.String remark, long domainId,
        br.com.atilo.jcondo.manager.model.Membership membership)
        throws java.lang.Exception {
        return getService()
                   .updatePerson(id, name, surname, identity, email, birthday,
            gender, remark, domainId, membership);
    }

    public static br.com.atilo.jcondo.manager.model.Person updatePortrait(
        long personId, br.com.atilo.jcondo.Image portrait)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updatePortrait(personId, portrait);
    }

    public static void updatePassword(long personId, java.lang.String password,
        java.lang.String newPassword)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService().updatePassword(personId, password, newPassword);
    }

    public static br.com.atilo.jcondo.manager.model.Person getPersonByIdentity(
        java.lang.String identity)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonByIdentity(identity);
    }

    public static br.com.atilo.jcondo.manager.model.Person getPerson(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPerson(id);
    }

    public static br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPerson();
    }

    public static java.util.List<br.com.atilo.jcondo.datatype.PersonType> getPersonTypes(
        long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonTypes(domainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> getDomainPeople(
        long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainPeople(domainId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> getDomainPeopleByType(
        long domainId, br.com.atilo.jcondo.datatype.PersonType personType)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainPeopleByType(domainId, personType);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Person> getDomainPeopleByName(
        long domainId, java.lang.String name)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainPeopleByName(domainId, name);
    }

    public static void completeUseRegistration(long companyId,
        java.lang.String emailAddress, boolean autoPassword, boolean sendEmail)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        getService()
            .completeUseRegistration(companyId, emailAddress, autoPassword,
            sendEmail);
    }

    public static boolean authenticatePerson(long personId,
        java.lang.String password)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().authenticatePerson(personId, password);
    }

    public static void clearService() {
        _service = null;
    }

    public static PersonService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    PersonService.class.getName());

            if (invokableService instanceof PersonService) {
                _service = (PersonService) invokableService;
            } else {
                _service = new PersonServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(PersonServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(PersonService service) {
    }
}
