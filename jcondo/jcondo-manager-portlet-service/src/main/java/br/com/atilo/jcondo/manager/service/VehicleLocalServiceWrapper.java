package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link VehicleLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see VehicleLocalService
 * @generated
 */
public class VehicleLocalServiceWrapper implements VehicleLocalService,
    ServiceWrapper<VehicleLocalService> {
    private VehicleLocalService _vehicleLocalService;

    public VehicleLocalServiceWrapper(VehicleLocalService vehicleLocalService) {
        _vehicleLocalService = vehicleLocalService;
    }

    /**
    * Adds the vehicle to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicle the vehicle
    * @return the vehicle that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle addVehicle(
        br.com.atilo.jcondo.manager.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.addVehicle(vehicle);
    }

    /**
    * Creates a new vehicle with the primary key. Does not add the vehicle to the database.
    *
    * @param id the primary key for the new vehicle
    * @return the new vehicle
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle createVehicle(long id) {
        return _vehicleLocalService.createVehicle(id);
    }

    /**
    * Deletes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the vehicle
    * @return the vehicle that was removed
    * @throws PortalException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle deleteVehicle(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.deleteVehicle(id);
    }

    /**
    * Deletes the vehicle from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicle the vehicle
    * @return the vehicle that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle deleteVehicle(
        br.com.atilo.jcondo.manager.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.deleteVehicle(vehicle);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _vehicleLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle fetchVehicle(long id)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.fetchVehicle(id);
    }

    /**
    * Returns the vehicle with the primary key.
    *
    * @param id the primary key of the vehicle
    * @return the vehicle
    * @throws PortalException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle getVehicle(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getVehicle(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicles.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of vehicles
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getVehicles(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getVehicles(start, end);
    }

    /**
    * Returns the number of vehicles.
    *
    * @return the number of vehicles
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getVehiclesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getVehiclesCount();
    }

    /**
    * Updates the vehicle in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicle the vehicle
    * @return the vehicle that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle updateVehicle(
        br.com.atilo.jcondo.manager.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.updateVehicle(vehicle);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _vehicleLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _vehicleLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _vehicleLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle addVehicle(
        java.lang.String license,
        br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.addVehicle(license, type, domainId,
            portrait, brand, color, remark);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle updateVehicle(long id,
        br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.updateVehicle(id, type, domainId, portrait,
            brand, color, remark);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle updateVehiclePortrait(
        long id, br.com.atilo.jcondo.Image portrait)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.updateVehiclePortrait(id, portrait);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle getVehicleByLicense(
        java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getVehicleByLicense(license);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getVehicles(
        java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getVehicles(license);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getVehicles(
        java.util.Map<java.lang.String, java.lang.String> params)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getVehicles(params);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getDomainVehiclesByLicense(
        long domainId, java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getDomainVehiclesByLicense(domainId, license);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getDomainVehicles(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _vehicleLocalService.getDomainVehicles(domainId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public VehicleLocalService getWrappedVehicleLocalService() {
        return _vehicleLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedVehicleLocalService(
        VehicleLocalService vehicleLocalService) {
        _vehicleLocalService = vehicleLocalService;
    }

    @Override
    public VehicleLocalService getWrappedService() {
        return _vehicleLocalService;
    }

    @Override
    public void setWrappedService(VehicleLocalService vehicleLocalService) {
        _vehicleLocalService = vehicleLocalService;
    }
}
