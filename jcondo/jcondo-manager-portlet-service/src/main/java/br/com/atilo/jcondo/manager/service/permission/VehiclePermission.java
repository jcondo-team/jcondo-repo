package br.com.atilo.jcondo.manager.service.permission;

import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.security.Permission;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;

public class VehiclePermission extends BasePermission {

	public static boolean hasPermission(Permission permission, long vehicleId, long domainId) throws PortalException, SystemException {
		Organization organization = OrganizationLocalServiceUtil.getOrganization(getOrganizationDomain(domainId));
		return hasPermission(permission, Vehicle.class, vehicleId, organization);
	}
	
}
