package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.ClpSerializer;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;
import com.liferay.portal.util.PortalUtil;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class PersonClp extends BaseModelImpl<Person> implements Person {
    private long _id;
    private long _userId;
    private String _userUuid;
    private long _domainId;
    private String _identity;
    private String _remark;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _personRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public PersonClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Person.class;
    }

    @Override
    public String getModelClassName() {
        return Person.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("userId", getUserId());
        attributes.put("domainId", getDomainId());
        attributes.put("identity", getIdentity());
        attributes.put("remark", getRemark());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long userId = (Long) attributes.get("userId");

        if (userId != null) {
            setUserId(userId);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        String identity = (String) attributes.get("identity");

        if (identity != null) {
            setIdentity(identity);
        }

        String remark = (String) attributes.get("remark");

        if (remark != null) {
            setRemark(remark);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_personRemoteModel != null) {
            try {
                Class<?> clazz = _personRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_personRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getUserId() {
        return _userId;
    }

    @Override
    public void setUserId(long userId) {
        _userId = userId;

        if (_personRemoteModel != null) {
            try {
                Class<?> clazz = _personRemoteModel.getClass();

                Method method = clazz.getMethod("setUserId", long.class);

                method.invoke(_personRemoteModel, userId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getUserUuid() throws SystemException {
        return PortalUtil.getUserValue(getUserId(), "uuid", _userUuid);
    }

    @Override
    public void setUserUuid(String userUuid) {
        _userUuid = userUuid;
    }

    @Override
    public long getDomainId() {
        return _domainId;
    }

    @Override
    public void setDomainId(long domainId) {
        _domainId = domainId;

        if (_personRemoteModel != null) {
            try {
                Class<?> clazz = _personRemoteModel.getClass();

                Method method = clazz.getMethod("setDomainId", long.class);

                method.invoke(_personRemoteModel, domainId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getIdentity() {
        return _identity;
    }

    @Override
    public void setIdentity(String identity) {
        _identity = identity;

        if (_personRemoteModel != null) {
            try {
                Class<?> clazz = _personRemoteModel.getClass();

                Method method = clazz.getMethod("setIdentity", String.class);

                method.invoke(_personRemoteModel, identity);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getRemark() {
        return _remark;
    }

    @Override
    public void setRemark(String remark) {
        _remark = remark;

        if (_personRemoteModel != null) {
            try {
                Class<?> clazz = _personRemoteModel.getClass();

                Method method = clazz.getMethod("setRemark", String.class);

                method.invoke(_personRemoteModel, remark);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_personRemoteModel != null) {
            try {
                Class<?> clazz = _personRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_personRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_personRemoteModel != null) {
            try {
                Class<?> clazz = _personRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_personRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public br.com.atilo.jcondo.datatype.Gender getGender() {
        try {
            String methodName = "getGender";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.datatype.Gender returnObj = (br.com.atilo.jcondo.datatype.Gender) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> getMemberships() {
        try {
            String methodName = "getMemberships";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.manager.model.Membership> returnObj =
                (java.util.List<br.com.atilo.jcondo.manager.model.Membership>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.Date getBirthday() {
        try {
            String methodName = "getBirthday";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.Date returnObj = (java.util.Date) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setName(java.lang.String name) {
        try {
            String methodName = "setName";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { name };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getTelephones() {
        try {
            String methodName = "getTelephones";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.manager.model.Telephone> returnObj =
                (java.util.List<br.com.atilo.jcondo.manager.model.Telephone>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.lang.String getSurname() {
        try {
            String methodName = "getSurname";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setPortrait(br.com.atilo.jcondo.Image image) {
        try {
            String methodName = "setPortrait";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.Image.class
                };

            Object[] parameterValues = new Object[] { image };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setUser(com.liferay.portal.model.User user) {
        try {
            String methodName = "setUser";

            Class<?>[] parameterTypes = new Class<?>[] {
                    com.liferay.portal.model.User.class
                };

            Object[] parameterValues = new Object[] { user };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setBirthday(java.util.Date birthday) {
        try {
            String methodName = "setBirthday";

            Class<?>[] parameterTypes = new Class<?>[] { java.util.Date.class };

            Object[] parameterValues = new Object[] { birthday };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setGender(br.com.atilo.jcondo.datatype.Gender gender) {
        try {
            String methodName = "setGender";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.datatype.Gender.class
                };

            Object[] parameterValues = new Object[] { gender };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setEmail(java.lang.String email) {
        try {
            String methodName = "setEmail";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { email };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.Image getPortrait() {
        try {
            String methodName = "getPortrait";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.Image returnObj = (br.com.atilo.jcondo.Image) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setSurname(java.lang.String surname) {
        try {
            String methodName = "setSurname";

            Class<?>[] parameterTypes = new Class<?>[] { java.lang.String.class };

            Object[] parameterValues = new Object[] { surname };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.lang.String getFullName() {
        try {
            String methodName = "getFullName";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.lang.String getEmail() {
        try {
            String methodName = "getEmail";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.lang.String getName() {
        try {
            String methodName = "getName";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public com.liferay.portal.model.User getUser() {
        try {
            String methodName = "getUser";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.liferay.portal.model.User returnObj = (com.liferay.portal.model.User) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.datatype.PersonType getType(long domainId) {
        try {
            String methodName = "getType";

            Class<?>[] parameterTypes = new Class<?>[] { long.class };

            Object[] parameterValues = new Object[] { domainId };

            br.com.atilo.jcondo.datatype.PersonType returnObj = (br.com.atilo.jcondo.datatype.PersonType) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getDomain() {
        try {
            String methodName = "getDomain";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.liferay.portal.model.BaseModel<?> returnObj = (com.liferay.portal.model.BaseModel<?>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setDomain(com.liferay.portal.model.BaseModel<?> domain) {
        try {
            String methodName = "setDomain";

            Class<?>[] parameterTypes = new Class<?>[] {
                    com.liferay.portal.model.BaseModel.class
                };

            Object[] parameterValues = new Object[] { domain };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getPersonRemoteModel() {
        return _personRemoteModel;
    }

    public void setPersonRemoteModel(BaseModel<?> personRemoteModel) {
        _personRemoteModel = personRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _personRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_personRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            PersonLocalServiceUtil.addPerson(this);
        } else {
            PersonLocalServiceUtil.updatePerson(this);
        }
    }

    @Override
    public Person toEscapedModel() {
        return (Person) ProxyUtil.newProxyInstance(Person.class.getClassLoader(),
            new Class[] { Person.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        PersonClp clone = new PersonClp();

        clone.setId(getId());
        clone.setUserId(getUserId());
        clone.setDomainId(getDomainId());
        clone.setIdentity(getIdentity());
        clone.setRemark(getRemark());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Person person) {
        long primaryKey = person.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof PersonClp)) {
            return false;
        }

        PersonClp person = (PersonClp) obj;

        long primaryKey = person.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", userId=");
        sb.append(getUserId());
        sb.append(", domainId=");
        sb.append(getDomainId());
        sb.append(", identity=");
        sb.append(getIdentity());
        sb.append(", remark=");
        sb.append(getRemark());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(25);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Person");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>userId</column-name><column-value><![CDATA[");
        sb.append(getUserId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>domainId</column-name><column-value><![CDATA[");
        sb.append(getDomainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>identity</column-name><column-value><![CDATA[");
        sb.append(getIdentity());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>remark</column-name><column-value><![CDATA[");
        sb.append(getRemark());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
