package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Kinship. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.KinshipServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see KinshipService
 * @see br.com.atilo.jcondo.manager.service.base.KinshipServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.KinshipServiceImpl
 * @generated
 */
public class KinshipServiceUtil {
    private static KinshipService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.KinshipServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Kinship createKinship(
        long personId, long relativeId,
        br.com.atilo.jcondo.datatype.KinType type) {
        return getService().createKinship(personId, relativeId, type);
    }

    public static br.com.atilo.jcondo.manager.model.Kinship addKinship(
        long personId, long relativeId,
        br.com.atilo.jcondo.datatype.KinType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addKinship(personId, relativeId, type);
    }

    public static br.com.atilo.jcondo.manager.model.Kinship updateKinship(
        long kinshipId, br.com.atilo.jcondo.datatype.KinType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateKinship(kinshipId, type);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Kinship> getKinships(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getKinships(personId);
    }

    public static br.com.atilo.jcondo.manager.model.Kinship getPersonKinship(
        long personId, long relativeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonKinship(personId, relativeId);
    }

    public static void clearService() {
        _service = null;
    }

    public static KinshipService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    KinshipService.class.getName());

            if (invokableService instanceof KinshipService) {
                _service = (KinshipService) invokableService;
            } else {
                _service = new KinshipServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(KinshipServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(KinshipService service) {
    }
}
