package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class VehicleActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public VehicleActionableDynamicQuery() throws SystemException {
        setBaseLocalService(VehicleLocalServiceUtil.getService());
        setClass(Vehicle.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
