package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Enterprise. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.EnterpriseServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see EnterpriseService
 * @see br.com.atilo.jcondo.manager.service.base.EnterpriseServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.EnterpriseServiceImpl
 * @generated
 */
public class EnterpriseServiceUtil {
    private static EnterpriseService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.EnterpriseServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Enterprise createEnterprise() {
        return getService().createEnterprise();
    }

    public static br.com.atilo.jcondo.manager.model.Enterprise addEnterprise(
        java.lang.String identity, java.lang.String name,
        br.com.atilo.jcondo.datatype.DomainType type,
        br.com.atilo.jcondo.datatype.EnterpriseStatus status,
        java.lang.String description)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addEnterprise(identity, name, type, status, description);
    }

    public static br.com.atilo.jcondo.manager.model.Enterprise updateEnterprise(
        long enterpriseId, java.lang.String identity, java.lang.String name,
        br.com.atilo.jcondo.datatype.DomainType type,
        br.com.atilo.jcondo.datatype.EnterpriseStatus status,
        java.lang.String description)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateEnterprise(enterpriseId, identity, name, type,
            status, description);
    }

    public static br.com.atilo.jcondo.manager.model.Enterprise getEnterprise(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getEnterprise(id);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> getEnterprisesByType(
        br.com.atilo.jcondo.datatype.DomainType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getEnterprisesByType(type);
    }

    public static br.com.atilo.jcondo.manager.model.Enterprise getEnterpriseByIdentity(
        java.lang.String identity)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getEnterpriseByIdentity(identity);
    }

    public static void clearService() {
        _service = null;
    }

    public static EnterpriseService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    EnterpriseService.class.getName());

            if (invokableService instanceof EnterpriseService) {
                _service = (EnterpriseService) invokableService;
            } else {
                _service = new EnterpriseServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(EnterpriseServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(EnterpriseService service) {
    }
}
