package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AccessPermissionService}.
 *
 * @author Brian Wing Shun Chan
 * @see AccessPermissionService
 * @generated
 */
public class AccessPermissionServiceWrapper implements AccessPermissionService,
    ServiceWrapper<AccessPermissionService> {
    private AccessPermissionService _accessPermissionService;

    public AccessPermissionServiceWrapper(
        AccessPermissionService accessPermissionService) {
        _accessPermissionService = accessPermissionService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _accessPermissionService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _accessPermissionService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _accessPermissionService.invokeMethod(name, parameterTypes,
            arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public AccessPermissionService getWrappedAccessPermissionService() {
        return _accessPermissionService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedAccessPermissionService(
        AccessPermissionService accessPermissionService) {
        _accessPermissionService = accessPermissionService;
    }

    @Override
    public AccessPermissionService getWrappedService() {
        return _accessPermissionService;
    }

    @Override
    public void setWrappedService(
        AccessPermissionService accessPermissionService) {
        _accessPermissionService = accessPermissionService;
    }
}
