package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link KinshipService}.
 *
 * @author Brian Wing Shun Chan
 * @see KinshipService
 * @generated
 */
public class KinshipServiceWrapper implements KinshipService,
    ServiceWrapper<KinshipService> {
    private KinshipService _kinshipService;

    public KinshipServiceWrapper(KinshipService kinshipService) {
        _kinshipService = kinshipService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _kinshipService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _kinshipService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _kinshipService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Kinship createKinship(
        long personId, long relativeId,
        br.com.atilo.jcondo.datatype.KinType type) {
        return _kinshipService.createKinship(personId, relativeId, type);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Kinship addKinship(long personId,
        long relativeId, br.com.atilo.jcondo.datatype.KinType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _kinshipService.addKinship(personId, relativeId, type);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Kinship updateKinship(
        long kinshipId, br.com.atilo.jcondo.datatype.KinType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _kinshipService.updateKinship(kinshipId, type);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Kinship> getKinships(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _kinshipService.getKinships(personId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Kinship getPersonKinship(
        long personId, long relativeId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _kinshipService.getPersonKinship(personId, relativeId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public KinshipService getWrappedKinshipService() {
        return _kinshipService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedKinshipService(KinshipService kinshipService) {
        _kinshipService = kinshipService;
    }

    @Override
    public KinshipService getWrappedService() {
        return _kinshipService;
    }

    @Override
    public void setWrappedService(KinshipService kinshipService) {
        _kinshipService = kinshipService;
    }
}
