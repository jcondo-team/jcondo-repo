package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.ClpSerializer;
import br.com.atilo.jcondo.manager.service.MailLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class MailClp extends BaseModelImpl<Mail> implements Mail {
    private long _id;
    private long _recipientId;
    private String _recipientName;
    private long _takerId;
    private String _takerName;
    private int _typeId;
    private String _code;
    private Date _dateIn;
    private Date _dateOut;
    private String _remark;
    private long _signatureId;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _mailRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public MailClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Mail.class;
    }

    @Override
    public String getModelClassName() {
        return Mail.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("recipientId", getRecipientId());
        attributes.put("recipientName", getRecipientName());
        attributes.put("takerId", getTakerId());
        attributes.put("takerName", getTakerName());
        attributes.put("typeId", getTypeId());
        attributes.put("code", getCode());
        attributes.put("dateIn", getDateIn());
        attributes.put("dateOut", getDateOut());
        attributes.put("remark", getRemark());
        attributes.put("signatureId", getSignatureId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long recipientId = (Long) attributes.get("recipientId");

        if (recipientId != null) {
            setRecipientId(recipientId);
        }

        String recipientName = (String) attributes.get("recipientName");

        if (recipientName != null) {
            setRecipientName(recipientName);
        }

        Long takerId = (Long) attributes.get("takerId");

        if (takerId != null) {
            setTakerId(takerId);
        }

        String takerName = (String) attributes.get("takerName");

        if (takerName != null) {
            setTakerName(takerName);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        String code = (String) attributes.get("code");

        if (code != null) {
            setCode(code);
        }

        Date dateIn = (Date) attributes.get("dateIn");

        if (dateIn != null) {
            setDateIn(dateIn);
        }

        Date dateOut = (Date) attributes.get("dateOut");

        if (dateOut != null) {
            setDateOut(dateOut);
        }

        String remark = (String) attributes.get("remark");

        if (remark != null) {
            setRemark(remark);
        }

        Long signatureId = (Long) attributes.get("signatureId");

        if (signatureId != null) {
            setSignatureId(signatureId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_mailRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getRecipientId() {
        return _recipientId;
    }

    @Override
    public void setRecipientId(long recipientId) {
        _recipientId = recipientId;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setRecipientId", long.class);

                method.invoke(_mailRemoteModel, recipientId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getRecipientName() {
        return _recipientName;
    }

    @Override
    public void setRecipientName(String recipientName) {
        _recipientName = recipientName;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setRecipientName", String.class);

                method.invoke(_mailRemoteModel, recipientName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getTakerId() {
        return _takerId;
    }

    @Override
    public void setTakerId(long takerId) {
        _takerId = takerId;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setTakerId", long.class);

                method.invoke(_mailRemoteModel, takerId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTakerName() {
        return _takerName;
    }

    @Override
    public void setTakerName(String takerName) {
        _takerName = takerName;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setTakerName", String.class);

                method.invoke(_mailRemoteModel, takerName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTypeId() {
        return _typeId;
    }

    @Override
    public void setTypeId(int typeId) {
        _typeId = typeId;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setTypeId", int.class);

                method.invoke(_mailRemoteModel, typeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCode() {
        return _code;
    }

    @Override
    public void setCode(String code) {
        _code = code;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setCode", String.class);

                method.invoke(_mailRemoteModel, code);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDateIn() {
        return _dateIn;
    }

    @Override
    public void setDateIn(Date dateIn) {
        _dateIn = dateIn;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setDateIn", Date.class);

                method.invoke(_mailRemoteModel, dateIn);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDateOut() {
        return _dateOut;
    }

    @Override
    public void setDateOut(Date dateOut) {
        _dateOut = dateOut;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setDateOut", Date.class);

                method.invoke(_mailRemoteModel, dateOut);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getRemark() {
        return _remark;
    }

    @Override
    public void setRemark(String remark) {
        _remark = remark;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setRemark", String.class);

                method.invoke(_mailRemoteModel, remark);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getSignatureId() {
        return _signatureId;
    }

    @Override
    public void setSignatureId(long signatureId) {
        _signatureId = signatureId;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setSignatureId", long.class);

                method.invoke(_mailRemoteModel, signatureId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_mailRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_mailRemoteModel != null) {
            try {
                Class<?> clazz = _mailRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_mailRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getMailRemoteModel() {
        return _mailRemoteModel;
    }

    public void setMailRemoteModel(BaseModel<?> mailRemoteModel) {
        _mailRemoteModel = mailRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _mailRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_mailRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            MailLocalServiceUtil.addMail(this);
        } else {
            MailLocalServiceUtil.updateMail(this);
        }
    }

    @Override
    public Mail toEscapedModel() {
        return (Mail) ProxyUtil.newProxyInstance(Mail.class.getClassLoader(),
            new Class[] { Mail.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        MailClp clone = new MailClp();

        clone.setId(getId());
        clone.setRecipientId(getRecipientId());
        clone.setRecipientName(getRecipientName());
        clone.setTakerId(getTakerId());
        clone.setTakerName(getTakerName());
        clone.setTypeId(getTypeId());
        clone.setCode(getCode());
        clone.setDateIn(getDateIn());
        clone.setDateOut(getDateOut());
        clone.setRemark(getRemark());
        clone.setSignatureId(getSignatureId());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Mail mail) {
        long primaryKey = mail.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof MailClp)) {
            return false;
        }

        MailClp mail = (MailClp) obj;

        long primaryKey = mail.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(27);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", recipientId=");
        sb.append(getRecipientId());
        sb.append(", recipientName=");
        sb.append(getRecipientName());
        sb.append(", takerId=");
        sb.append(getTakerId());
        sb.append(", takerName=");
        sb.append(getTakerName());
        sb.append(", typeId=");
        sb.append(getTypeId());
        sb.append(", code=");
        sb.append(getCode());
        sb.append(", dateIn=");
        sb.append(getDateIn());
        sb.append(", dateOut=");
        sb.append(getDateOut());
        sb.append(", remark=");
        sb.append(getRemark());
        sb.append(", signatureId=");
        sb.append(getSignatureId());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(43);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Mail");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>recipientId</column-name><column-value><![CDATA[");
        sb.append(getRecipientId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>recipientName</column-name><column-value><![CDATA[");
        sb.append(getRecipientName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>takerId</column-name><column-value><![CDATA[");
        sb.append(getTakerId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>takerName</column-name><column-value><![CDATA[");
        sb.append(getTakerName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>typeId</column-name><column-value><![CDATA[");
        sb.append(getTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>code</column-name><column-value><![CDATA[");
        sb.append(getCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dateIn</column-name><column-value><![CDATA[");
        sb.append(getDateIn());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>dateOut</column-name><column-value><![CDATA[");
        sb.append(getDateOut());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>remark</column-name><column-value><![CDATA[");
        sb.append(getRemark());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>signatureId</column-name><column-value><![CDATA[");
        sb.append(getSignatureId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
