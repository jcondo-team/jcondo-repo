package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Vehicle service. Represents a row in the &quot;jco_vehicle&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see VehicleModel
 * @see br.com.atilo.jcondo.manager.model.impl.VehicleImpl
 * @see br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl
 * @generated
 */
public interface Vehicle extends VehicleModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.VehicleImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.Image getPortrait();

    public br.com.atilo.jcondo.datatype.VehicleType getType();

    public void setType(br.com.atilo.jcondo.datatype.VehicleType type);

    public com.liferay.portal.model.BaseModel<?> getDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setDomain(com.liferay.portal.model.BaseModel<?> domain);
}
