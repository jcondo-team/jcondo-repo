package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.ParkingServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.ParkingServiceSoap
 * @generated
 */
public class ParkingSoap implements Serializable {
    private long _id;
    private String _code;
    private int _typeId;
    private long _ownerDomainId;
    private long _renterDomainId;
    private Date _oprDate;
    private long _oprUser;

    public ParkingSoap() {
    }

    public static ParkingSoap toSoapModel(Parking model) {
        ParkingSoap soapModel = new ParkingSoap();

        soapModel.setId(model.getId());
        soapModel.setCode(model.getCode());
        soapModel.setTypeId(model.getTypeId());
        soapModel.setOwnerDomainId(model.getOwnerDomainId());
        soapModel.setRenterDomainId(model.getRenterDomainId());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static ParkingSoap[] toSoapModels(Parking[] models) {
        ParkingSoap[] soapModels = new ParkingSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static ParkingSoap[][] toSoapModels(Parking[][] models) {
        ParkingSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new ParkingSoap[models.length][models[0].length];
        } else {
            soapModels = new ParkingSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static ParkingSoap[] toSoapModels(List<Parking> models) {
        List<ParkingSoap> soapModels = new ArrayList<ParkingSoap>(models.size());

        for (Parking model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new ParkingSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public String getCode() {
        return _code;
    }

    public void setCode(String code) {
        _code = code;
    }

    public int getTypeId() {
        return _typeId;
    }

    public void setTypeId(int typeId) {
        _typeId = typeId;
    }

    public long getOwnerDomainId() {
        return _ownerDomainId;
    }

    public void setOwnerDomainId(long ownerDomainId) {
        _ownerDomainId = ownerDomainId;
    }

    public long getRenterDomainId() {
        return _renterDomainId;
    }

    public void setRenterDomainId(long renterDomainId) {
        _renterDomainId = renterDomainId;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
