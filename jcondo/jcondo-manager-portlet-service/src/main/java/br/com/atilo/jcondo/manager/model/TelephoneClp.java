package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.ClpSerializer;
import br.com.atilo.jcondo.manager.service.TelephoneLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.HashMap;
import java.util.Map;


public class TelephoneClp extends BaseModelImpl<Telephone> implements Telephone {
    private long _id;
    private String _typeName;
    private long _extension;
    private long _number;
    private boolean _primary;
    private BaseModel<?> _telephoneRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public TelephoneClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Telephone.class;
    }

    @Override
    public String getModelClassName() {
        return Telephone.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("typeName", getTypeName());
        attributes.put("extension", getExtension());
        attributes.put("number", getNumber());
        attributes.put("primary", getPrimary());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String typeName = (String) attributes.get("typeName");

        if (typeName != null) {
            setTypeName(typeName);
        }

        Long extension = (Long) attributes.get("extension");

        if (extension != null) {
            setExtension(extension);
        }

        Long number = (Long) attributes.get("number");

        if (number != null) {
            setNumber(number);
        }

        Boolean primary = (Boolean) attributes.get("primary");

        if (primary != null) {
            setPrimary(primary);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_telephoneRemoteModel != null) {
            try {
                Class<?> clazz = _telephoneRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_telephoneRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTypeName() {
        return _typeName;
    }

    @Override
    public void setTypeName(String typeName) {
        _typeName = typeName;

        if (_telephoneRemoteModel != null) {
            try {
                Class<?> clazz = _telephoneRemoteModel.getClass();

                Method method = clazz.getMethod("setTypeName", String.class);

                method.invoke(_telephoneRemoteModel, typeName);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getExtension() {
        return _extension;
    }

    @Override
    public void setExtension(long extension) {
        _extension = extension;

        if (_telephoneRemoteModel != null) {
            try {
                Class<?> clazz = _telephoneRemoteModel.getClass();

                Method method = clazz.getMethod("setExtension", long.class);

                method.invoke(_telephoneRemoteModel, extension);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getNumber() {
        return _number;
    }

    @Override
    public void setNumber(long number) {
        _number = number;

        if (_telephoneRemoteModel != null) {
            try {
                Class<?> clazz = _telephoneRemoteModel.getClass();

                Method method = clazz.getMethod("setNumber", long.class);

                method.invoke(_telephoneRemoteModel, number);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getPrimary() {
        return _primary;
    }

    @Override
    public boolean isPrimary() {
        return _primary;
    }

    @Override
    public void setPrimary(boolean primary) {
        _primary = primary;

        if (_telephoneRemoteModel != null) {
            try {
                Class<?> clazz = _telephoneRemoteModel.getClass();

                Method method = clazz.getMethod("setPrimary", boolean.class);

                method.invoke(_telephoneRemoteModel, primary);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public void setType(br.com.atilo.jcondo.datatype.PhoneType type) {
        try {
            String methodName = "setType";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.datatype.PhoneType.class
                };

            Object[] parameterValues = new Object[] { type };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.datatype.PhoneType getType() {
        try {
            String methodName = "getType";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.datatype.PhoneType returnObj = (br.com.atilo.jcondo.datatype.PhoneType) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getTelephoneRemoteModel() {
        return _telephoneRemoteModel;
    }

    public void setTelephoneRemoteModel(BaseModel<?> telephoneRemoteModel) {
        _telephoneRemoteModel = telephoneRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _telephoneRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_telephoneRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            TelephoneLocalServiceUtil.addTelephone(this);
        } else {
            TelephoneLocalServiceUtil.updateTelephone(this);
        }
    }

    @Override
    public Telephone toEscapedModel() {
        return (Telephone) ProxyUtil.newProxyInstance(Telephone.class.getClassLoader(),
            new Class[] { Telephone.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        TelephoneClp clone = new TelephoneClp();

        clone.setId(getId());
        clone.setTypeName(getTypeName());
        clone.setExtension(getExtension());
        clone.setNumber(getNumber());
        clone.setPrimary(getPrimary());

        return clone;
    }

    @Override
    public int compareTo(Telephone telephone) {
        long primaryKey = telephone.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof TelephoneClp)) {
            return false;
        }

        TelephoneClp telephone = (TelephoneClp) obj;

        long primaryKey = telephone.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(11);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", typeName=");
        sb.append(getTypeName());
        sb.append(", extension=");
        sb.append(getExtension());
        sb.append(", number=");
        sb.append(getNumber());
        sb.append(", primary=");
        sb.append(getPrimary());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(19);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Telephone");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>typeName</column-name><column-value><![CDATA[");
        sb.append(getTypeName());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>extension</column-name><column-value><![CDATA[");
        sb.append(getExtension());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>number</column-name><column-value><![CDATA[");
        sb.append(getNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>primary</column-name><column-value><![CDATA[");
        sb.append(getPrimary());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
