package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.ClpSerializer;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class FlatClp extends BaseModelImpl<Flat> implements Flat {
    private long _id;
    private long _organizationId;
    private long _folderId;
    private long _personId;
    private int _block;
    private int _number;
    private boolean _pets;
    private boolean _disables;
    private boolean _brigade;
    private boolean _defaulting;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _flatRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public FlatClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Flat.class;
    }

    @Override
    public String getModelClassName() {
        return Flat.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("organizationId", getOrganizationId());
        attributes.put("folderId", getFolderId());
        attributes.put("personId", getPersonId());
        attributes.put("block", getBlock());
        attributes.put("number", getNumber());
        attributes.put("pets", getPets());
        attributes.put("disables", getDisables());
        attributes.put("brigade", getBrigade());
        attributes.put("defaulting", getDefaulting());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long organizationId = (Long) attributes.get("organizationId");

        if (organizationId != null) {
            setOrganizationId(organizationId);
        }

        Long folderId = (Long) attributes.get("folderId");

        if (folderId != null) {
            setFolderId(folderId);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Integer block = (Integer) attributes.get("block");

        if (block != null) {
            setBlock(block);
        }

        Integer number = (Integer) attributes.get("number");

        if (number != null) {
            setNumber(number);
        }

        Boolean pets = (Boolean) attributes.get("pets");

        if (pets != null) {
            setPets(pets);
        }

        Boolean disables = (Boolean) attributes.get("disables");

        if (disables != null) {
            setDisables(disables);
        }

        Boolean brigade = (Boolean) attributes.get("brigade");

        if (brigade != null) {
            setBrigade(brigade);
        }

        Boolean defaulting = (Boolean) attributes.get("defaulting");

        if (defaulting != null) {
            setDefaulting(defaulting);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_flatRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOrganizationId() {
        return _organizationId;
    }

    @Override
    public void setOrganizationId(long organizationId) {
        _organizationId = organizationId;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setOrganizationId", long.class);

                method.invoke(_flatRemoteModel, organizationId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getFolderId() {
        return _folderId;
    }

    @Override
    public void setFolderId(long folderId) {
        _folderId = folderId;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setFolderId", long.class);

                method.invoke(_flatRemoteModel, folderId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getPersonId() {
        return _personId;
    }

    @Override
    public void setPersonId(long personId) {
        _personId = personId;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setPersonId", long.class);

                method.invoke(_flatRemoteModel, personId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getBlock() {
        return _block;
    }

    @Override
    public void setBlock(int block) {
        _block = block;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setBlock", int.class);

                method.invoke(_flatRemoteModel, block);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getNumber() {
        return _number;
    }

    @Override
    public void setNumber(int number) {
        _number = number;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setNumber", int.class);

                method.invoke(_flatRemoteModel, number);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getPets() {
        return _pets;
    }

    @Override
    public boolean isPets() {
        return _pets;
    }

    @Override
    public void setPets(boolean pets) {
        _pets = pets;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setPets", boolean.class);

                method.invoke(_flatRemoteModel, pets);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getDisables() {
        return _disables;
    }

    @Override
    public boolean isDisables() {
        return _disables;
    }

    @Override
    public void setDisables(boolean disables) {
        _disables = disables;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setDisables", boolean.class);

                method.invoke(_flatRemoteModel, disables);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getBrigade() {
        return _brigade;
    }

    @Override
    public boolean isBrigade() {
        return _brigade;
    }

    @Override
    public void setBrigade(boolean brigade) {
        _brigade = brigade;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setBrigade", boolean.class);

                method.invoke(_flatRemoteModel, brigade);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public boolean getDefaulting() {
        return _defaulting;
    }

    @Override
    public boolean isDefaulting() {
        return _defaulting;
    }

    @Override
    public void setDefaulting(boolean defaulting) {
        _defaulting = defaulting;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setDefaulting", boolean.class);

                method.invoke(_flatRemoteModel, defaulting);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_flatRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_flatRemoteModel != null) {
            try {
                Class<?> clazz = _flatRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_flatRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.datatype.PetType> getPetTypes() {
        try {
            String methodName = "getPetTypes";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.datatype.PetType> returnObj = (java.util.List<br.com.atilo.jcondo.datatype.PetType>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getPeople() {
        try {
            String methodName = "getPeople";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.manager.model.Person> returnObj = (java.util.List<br.com.atilo.jcondo.manager.model.Person>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson() {
        try {
            String methodName = "getPerson";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.manager.model.Person returnObj = (br.com.atilo.jcondo.manager.model.Person) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.lang.String getName() {
        try {
            String methodName = "getName";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.lang.String returnObj = (java.lang.String) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getFlatRemoteModel() {
        return _flatRemoteModel;
    }

    public void setFlatRemoteModel(BaseModel<?> flatRemoteModel) {
        _flatRemoteModel = flatRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _flatRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_flatRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            FlatLocalServiceUtil.addFlat(this);
        } else {
            FlatLocalServiceUtil.updateFlat(this);
        }
    }

    @Override
    public Flat toEscapedModel() {
        return (Flat) ProxyUtil.newProxyInstance(Flat.class.getClassLoader(),
            new Class[] { Flat.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        FlatClp clone = new FlatClp();

        clone.setId(getId());
        clone.setOrganizationId(getOrganizationId());
        clone.setFolderId(getFolderId());
        clone.setPersonId(getPersonId());
        clone.setBlock(getBlock());
        clone.setNumber(getNumber());
        clone.setPets(getPets());
        clone.setDisables(getDisables());
        clone.setBrigade(getBrigade());
        clone.setDefaulting(getDefaulting());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Flat flat) {
        long primaryKey = flat.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof FlatClp)) {
            return false;
        }

        FlatClp flat = (FlatClp) obj;

        long primaryKey = flat.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(25);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", organizationId=");
        sb.append(getOrganizationId());
        sb.append(", folderId=");
        sb.append(getFolderId());
        sb.append(", personId=");
        sb.append(getPersonId());
        sb.append(", block=");
        sb.append(getBlock());
        sb.append(", number=");
        sb.append(getNumber());
        sb.append(", pets=");
        sb.append(getPets());
        sb.append(", disables=");
        sb.append(getDisables());
        sb.append(", brigade=");
        sb.append(getBrigade());
        sb.append(", defaulting=");
        sb.append(getDefaulting());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(40);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Flat");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>organizationId</column-name><column-value><![CDATA[");
        sb.append(getOrganizationId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>folderId</column-name><column-value><![CDATA[");
        sb.append(getFolderId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>personId</column-name><column-value><![CDATA[");
        sb.append(getPersonId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>block</column-name><column-value><![CDATA[");
        sb.append(getBlock());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>number</column-name><column-value><![CDATA[");
        sb.append(getNumber());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>pets</column-name><column-value><![CDATA[");
        sb.append(getPets());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>disables</column-name><column-value><![CDATA[");
        sb.append(getDisables());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>brigade</column-name><column-value><![CDATA[");
        sb.append(getBrigade());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>defaulting</column-name><column-value><![CDATA[");
        sb.append(getDefaulting());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
