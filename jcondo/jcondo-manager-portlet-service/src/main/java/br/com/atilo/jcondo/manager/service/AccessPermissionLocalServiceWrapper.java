package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AccessPermissionLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see AccessPermissionLocalService
 * @generated
 */
public class AccessPermissionLocalServiceWrapper
    implements AccessPermissionLocalService,
        ServiceWrapper<AccessPermissionLocalService> {
    private AccessPermissionLocalService _accessPermissionLocalService;

    public AccessPermissionLocalServiceWrapper(
        AccessPermissionLocalService accessPermissionLocalService) {
        _accessPermissionLocalService = accessPermissionLocalService;
    }

    /**
    * Adds the access permission to the database. Also notifies the appropriate model listeners.
    *
    * @param accessPermission the access permission
    * @return the access permission that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission addAccessPermission(
        br.com.atilo.jcondo.manager.model.AccessPermission accessPermission)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.addAccessPermission(accessPermission);
    }

    /**
    * Creates a new access permission with the primary key. Does not add the access permission to the database.
    *
    * @param id the primary key for the new access permission
    * @return the new access permission
    */
    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission createAccessPermission(
        long id) {
        return _accessPermissionLocalService.createAccessPermission(id);
    }

    /**
    * Deletes the access permission with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the access permission
    * @return the access permission that was removed
    * @throws PortalException if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission deleteAccessPermission(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.deleteAccessPermission(id);
    }

    /**
    * Deletes the access permission from the database. Also notifies the appropriate model listeners.
    *
    * @param accessPermission the access permission
    * @return the access permission that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission deleteAccessPermission(
        br.com.atilo.jcondo.manager.model.AccessPermission accessPermission)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.deleteAccessPermission(accessPermission);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _accessPermissionLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.dynamicQuery(dynamicQuery, start,
            end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.dynamicQuery(dynamicQuery, start,
            end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission fetchAccessPermission(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.fetchAccessPermission(id);
    }

    /**
    * Returns the access permission with the primary key.
    *
    * @param id the primary key of the access permission
    * @return the access permission
    * @throws PortalException if a access permission with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission getAccessPermission(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.getAccessPermission(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the access permissions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of access permissions
    * @param end the upper bound of the range of access permissions (not inclusive)
    * @return the range of access permissions
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> getAccessPermissions(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.getAccessPermissions(start, end);
    }

    /**
    * Returns the number of access permissions.
    *
    * @return the number of access permissions
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getAccessPermissionsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.getAccessPermissionsCount();
    }

    /**
    * Updates the access permission in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param accessPermission the access permission
    * @return the access permission that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission updateAccessPermission(
        br.com.atilo.jcondo.manager.model.AccessPermission accessPermission)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.updateAccessPermission(accessPermission);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _accessPermissionLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _accessPermissionLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _accessPermissionLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission addAccessPermission(
        long personId, long domainId, int weekDay, java.lang.String beginTime,
        java.lang.String endTime, java.util.Date beginDate,
        java.util.Date endDate)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.addAccessPermission(personId,
            domainId, weekDay, beginTime, endTime, beginDate, endDate);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> getPersonAccessPermissions(
        long personId, long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.getPersonAccessPermissions(personId,
            domainId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.AccessPermission getPersonAccessPermission(
        long personId, long domainId, int weekDay)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _accessPermissionLocalService.getPersonAccessPermission(personId,
            domainId, weekDay);
    }

    @Override
    public boolean hasAccess(long personId, long domainId, java.util.Date date)
        throws java.lang.Exception {
        return _accessPermissionLocalService.hasAccess(personId, domainId, date);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public AccessPermissionLocalService getWrappedAccessPermissionLocalService() {
        return _accessPermissionLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedAccessPermissionLocalService(
        AccessPermissionLocalService accessPermissionLocalService) {
        _accessPermissionLocalService = accessPermissionLocalService;
    }

    @Override
    public AccessPermissionLocalService getWrappedService() {
        return _accessPermissionLocalService;
    }

    @Override
    public void setWrappedService(
        AccessPermissionLocalService accessPermissionLocalService) {
        _accessPermissionLocalService = accessPermissionLocalService;
    }
}
