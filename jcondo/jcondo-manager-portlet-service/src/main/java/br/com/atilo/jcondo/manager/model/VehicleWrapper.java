package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Vehicle}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Vehicle
 * @generated
 */
public class VehicleWrapper implements Vehicle, ModelWrapper<Vehicle> {
    private Vehicle _vehicle;

    public VehicleWrapper(Vehicle vehicle) {
        _vehicle = vehicle;
    }

    @Override
    public Class<?> getModelClass() {
        return Vehicle.class;
    }

    @Override
    public String getModelClassName() {
        return Vehicle.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("domainId", getDomainId());
        attributes.put("imageId", getImageId());
        attributes.put("typeId", getTypeId());
        attributes.put("license", getLicense());
        attributes.put("brand", getBrand());
        attributes.put("color", getColor());
        attributes.put("remark", getRemark());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        Long imageId = (Long) attributes.get("imageId");

        if (imageId != null) {
            setImageId(imageId);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        String license = (String) attributes.get("license");

        if (license != null) {
            setLicense(license);
        }

        String brand = (String) attributes.get("brand");

        if (brand != null) {
            setBrand(brand);
        }

        String color = (String) attributes.get("color");

        if (color != null) {
            setColor(color);
        }

        String remark = (String) attributes.get("remark");

        if (remark != null) {
            setRemark(remark);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this vehicle.
    *
    * @return the primary key of this vehicle
    */
    @Override
    public long getPrimaryKey() {
        return _vehicle.getPrimaryKey();
    }

    /**
    * Sets the primary key of this vehicle.
    *
    * @param primaryKey the primary key of this vehicle
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _vehicle.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this vehicle.
    *
    * @return the ID of this vehicle
    */
    @Override
    public long getId() {
        return _vehicle.getId();
    }

    /**
    * Sets the ID of this vehicle.
    *
    * @param id the ID of this vehicle
    */
    @Override
    public void setId(long id) {
        _vehicle.setId(id);
    }

    /**
    * Returns the domain ID of this vehicle.
    *
    * @return the domain ID of this vehicle
    */
    @Override
    public long getDomainId() {
        return _vehicle.getDomainId();
    }

    /**
    * Sets the domain ID of this vehicle.
    *
    * @param domainId the domain ID of this vehicle
    */
    @Override
    public void setDomainId(long domainId) {
        _vehicle.setDomainId(domainId);
    }

    /**
    * Returns the image ID of this vehicle.
    *
    * @return the image ID of this vehicle
    */
    @Override
    public long getImageId() {
        return _vehicle.getImageId();
    }

    /**
    * Sets the image ID of this vehicle.
    *
    * @param imageId the image ID of this vehicle
    */
    @Override
    public void setImageId(long imageId) {
        _vehicle.setImageId(imageId);
    }

    /**
    * Returns the type ID of this vehicle.
    *
    * @return the type ID of this vehicle
    */
    @Override
    public int getTypeId() {
        return _vehicle.getTypeId();
    }

    /**
    * Sets the type ID of this vehicle.
    *
    * @param typeId the type ID of this vehicle
    */
    @Override
    public void setTypeId(int typeId) {
        _vehicle.setTypeId(typeId);
    }

    /**
    * Returns the license of this vehicle.
    *
    * @return the license of this vehicle
    */
    @Override
    public java.lang.String getLicense() {
        return _vehicle.getLicense();
    }

    /**
    * Sets the license of this vehicle.
    *
    * @param license the license of this vehicle
    */
    @Override
    public void setLicense(java.lang.String license) {
        _vehicle.setLicense(license);
    }

    /**
    * Returns the brand of this vehicle.
    *
    * @return the brand of this vehicle
    */
    @Override
    public java.lang.String getBrand() {
        return _vehicle.getBrand();
    }

    /**
    * Sets the brand of this vehicle.
    *
    * @param brand the brand of this vehicle
    */
    @Override
    public void setBrand(java.lang.String brand) {
        _vehicle.setBrand(brand);
    }

    /**
    * Returns the color of this vehicle.
    *
    * @return the color of this vehicle
    */
    @Override
    public java.lang.String getColor() {
        return _vehicle.getColor();
    }

    /**
    * Sets the color of this vehicle.
    *
    * @param color the color of this vehicle
    */
    @Override
    public void setColor(java.lang.String color) {
        _vehicle.setColor(color);
    }

    /**
    * Returns the remark of this vehicle.
    *
    * @return the remark of this vehicle
    */
    @Override
    public java.lang.String getRemark() {
        return _vehicle.getRemark();
    }

    /**
    * Sets the remark of this vehicle.
    *
    * @param remark the remark of this vehicle
    */
    @Override
    public void setRemark(java.lang.String remark) {
        _vehicle.setRemark(remark);
    }

    /**
    * Returns the opr date of this vehicle.
    *
    * @return the opr date of this vehicle
    */
    @Override
    public java.util.Date getOprDate() {
        return _vehicle.getOprDate();
    }

    /**
    * Sets the opr date of this vehicle.
    *
    * @param oprDate the opr date of this vehicle
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _vehicle.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this vehicle.
    *
    * @return the opr user of this vehicle
    */
    @Override
    public long getOprUser() {
        return _vehicle.getOprUser();
    }

    /**
    * Sets the opr user of this vehicle.
    *
    * @param oprUser the opr user of this vehicle
    */
    @Override
    public void setOprUser(long oprUser) {
        _vehicle.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _vehicle.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _vehicle.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _vehicle.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _vehicle.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _vehicle.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _vehicle.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _vehicle.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _vehicle.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _vehicle.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _vehicle.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _vehicle.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new VehicleWrapper((Vehicle) _vehicle.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Vehicle vehicle) {
        return _vehicle.compareTo(vehicle);
    }

    @Override
    public int hashCode() {
        return _vehicle.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Vehicle> toCacheModel() {
        return _vehicle.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle toEscapedModel() {
        return new VehicleWrapper(_vehicle.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Vehicle toUnescapedModel() {
        return new VehicleWrapper(_vehicle.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _vehicle.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _vehicle.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _vehicle.persist();
    }

    @Override
    public br.com.atilo.jcondo.Image getPortrait() {
        return _vehicle.getPortrait();
    }

    @Override
    public br.com.atilo.jcondo.datatype.VehicleType getType() {
        return _vehicle.getType();
    }

    @Override
    public void setType(br.com.atilo.jcondo.datatype.VehicleType type) {
        _vehicle.setType(type);
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _vehicle.getDomain();
    }

    @Override
    public void setDomain(com.liferay.portal.model.BaseModel<?> domain) {
        _vehicle.setDomain(domain);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleWrapper)) {
            return false;
        }

        VehicleWrapper vehicleWrapper = (VehicleWrapper) obj;

        if (Validator.equals(_vehicle, vehicleWrapper._vehicle)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Vehicle getWrappedVehicle() {
        return _vehicle;
    }

    @Override
    public Vehicle getWrappedModel() {
        return _vehicle;
    }

    @Override
    public void resetOriginalValues() {
        _vehicle.resetOriginalValues();
    }
}
