package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Membership service. Represents a row in the &quot;jco_membership&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see MembershipModel
 * @see br.com.atilo.jcondo.manager.model.impl.MembershipImpl
 * @see br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl
 * @generated
 */
public interface Membership extends MembershipModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.MembershipImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.datatype.PersonType getType();

    public void setType(br.com.atilo.jcondo.datatype.PersonType type);

    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public com.liferay.portal.model.BaseModel<?> getDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public void setDomain(com.liferay.portal.model.BaseModel<?> domain);
}
