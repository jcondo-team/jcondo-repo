package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PetLocalService}.
 *
 * @author Brian Wing Shun Chan
 * @see PetLocalService
 * @generated
 */
public class PetLocalServiceWrapper implements PetLocalService,
    ServiceWrapper<PetLocalService> {
    private PetLocalService _petLocalService;

    public PetLocalServiceWrapper(PetLocalService petLocalService) {
        _petLocalService = petLocalService;
    }

    /**
    * Adds the pet to the database. Also notifies the appropriate model listeners.
    *
    * @param pet the pet
    * @return the pet that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Pet addPet(
        br.com.atilo.jcondo.manager.model.Pet pet)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.addPet(pet);
    }

    /**
    * Creates a new pet with the primary key. Does not add the pet to the database.
    *
    * @param petId the primary key for the new pet
    * @return the new pet
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Pet createPet(long petId) {
        return _petLocalService.createPet(petId);
    }

    /**
    * Deletes the pet with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param petId the primary key of the pet
    * @return the pet that was removed
    * @throws PortalException if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Pet deletePet(long petId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.deletePet(petId);
    }

    /**
    * Deletes the pet from the database. Also notifies the appropriate model listeners.
    *
    * @param pet the pet
    * @return the pet that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Pet deletePet(
        br.com.atilo.jcondo.manager.model.Pet pet)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.deletePet(pet);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _petLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.dynamicQueryCount(dynamicQuery, projection);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Pet fetchPet(long petId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.fetchPet(petId);
    }

    /**
    * Returns the pet with the primary key.
    *
    * @param petId the primary key of the pet
    * @return the pet
    * @throws PortalException if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Pet getPet(long petId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.getPet(petId);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the pets.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of pets
    * @param end the upper bound of the range of pets (not inclusive)
    * @return the range of pets
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Pet> getPets(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.getPets(start, end);
    }

    /**
    * Returns the number of pets.
    *
    * @return the number of pets
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getPetsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.getPetsCount();
    }

    /**
    * Updates the pet in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param pet the pet
    * @return the pet that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.manager.model.Pet updatePet(
        br.com.atilo.jcondo.manager.model.Pet pet)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.updatePet(pet);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _petLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _petLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _petLocalService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public void updatePetTypes(
        java.util.List<br.com.atilo.jcondo.datatype.PetType> petTypes,
        long flatId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _petLocalService.updatePetTypes(petTypes, flatId);
    }

    @Override
    public void addPet(br.com.atilo.jcondo.datatype.PetType type, long flatId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _petLocalService.addPet(type, flatId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.datatype.PetType> getPetTypes(
        long flatId) throws com.liferay.portal.kernel.exception.SystemException {
        return _petLocalService.getPetTypes(flatId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PetLocalService getWrappedPetLocalService() {
        return _petLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPetLocalService(PetLocalService petLocalService) {
        _petLocalService = petLocalService;
    }

    @Override
    public PetLocalService getWrappedService() {
        return _petLocalService;
    }

    @Override
    public void setWrappedService(PetLocalService petLocalService) {
        _petLocalService = petLocalService;
    }
}
