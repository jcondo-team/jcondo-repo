package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Membership;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the membership service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MembershipPersistenceImpl
 * @see MembershipUtil
 * @generated
 */
public interface MembershipPersistence extends BasePersistence<Membership> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link MembershipUtil} to access the membership persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the memberships where personId = &#63;.
    *
    * @param personId the person ID
    * @return the matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByPerson(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the memberships where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByPerson(
        long personId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the memberships where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByPerson(
        long personId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first membership in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first membership in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last membership in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last membership in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the memberships before and after the current membership in the ordered set where personId = &#63;.
    *
    * @param id the primary key of the current membership
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership[] findByPerson_PrevAndNext(
        long id, long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the memberships where personId = &#63; from the database.
    *
    * @param personId the person ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of memberships where personId = &#63;.
    *
    * @param personId the person ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public int countByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the memberships where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @return the matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomain(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the memberships where domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomain(
        long domainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the memberships where domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomain(
        long domainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first membership in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByDomain_First(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first membership in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByDomain_First(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last membership in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByDomain_Last(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last membership in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByDomain_Last(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the memberships before and after the current membership in the ordered set where domainId = &#63;.
    *
    * @param id the primary key of the current membership
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership[] findByDomain_PrevAndNext(
        long id, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the memberships where domainId = &#63; from the database.
    *
    * @param domainId the domain ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByDomain(long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of memberships where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public int countByDomain(long domainId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the memberships where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByType(
        int typeId) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the memberships where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByType(
        int typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the memberships where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByType(
        int typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first membership in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByType_First(
        int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first membership in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByType_First(
        int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last membership in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByType_Last(
        int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last membership in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByType_Last(
        int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the memberships before and after the current membership in the ordered set where typeId = &#63;.
    *
    * @param id the primary key of the current membership
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership[] findByType_PrevAndNext(
        long id, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the memberships where typeId = &#63; from the database.
    *
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByType(int typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of memberships where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public int countByType(int typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the membership where domainId = &#63; and personId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchMembershipException} if it could not be found.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @return the matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByDomainAndPerson(
        long domainId, long personId)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the membership where domainId = &#63; and personId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @return the matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByDomainAndPerson(
        long domainId, long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the membership where domainId = &#63; and personId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByDomainAndPerson(
        long domainId, long personId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the membership where domainId = &#63; and personId = &#63; from the database.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @return the membership that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership removeByDomainAndPerson(
        long domainId, long personId)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of memberships where domainId = &#63; and personId = &#63;.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public int countByDomainAndPerson(long domainId, long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the memberships where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomainAndType(
        long domainId, int typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the memberships where domainId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomainAndType(
        long domainId, int typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the memberships where domainId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomainAndType(
        long domainId, int typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByDomainAndType_First(
        long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByDomainAndType_First(
        long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByDomainAndType_Last(
        long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByDomainAndType_Last(
        long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the memberships before and after the current membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param id the primary key of the current membership
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership[] findByDomainAndType_PrevAndNext(
        long id, long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the memberships where domainId = &#63; and typeId = &#63; from the database.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByDomainAndType(long domainId, int typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of memberships where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public int countByDomainAndType(long domainId, int typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the membership where personId = &#63; and domainId = &#63; and typeId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchMembershipException} if it could not be found.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByPersonDomainAndType(
        long personId, long domainId, int typeId)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the membership where personId = &#63; and domainId = &#63; and typeId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByPersonDomainAndType(
        long personId, long domainId, int typeId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the membership where personId = &#63; and domainId = &#63; and typeId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByPersonDomainAndType(
        long personId, long domainId, int typeId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the membership where personId = &#63; and domainId = &#63; and typeId = &#63; from the database.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the membership that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership removeByPersonDomainAndType(
        long personId, long domainId, int typeId)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of memberships where personId = &#63; and domainId = &#63; and typeId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public int countByPersonDomainAndType(long personId, long domainId,
        int typeId) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the membership in the entity cache if it is enabled.
    *
    * @param membership the membership
    */
    public void cacheResult(
        br.com.atilo.jcondo.manager.model.Membership membership);

    /**
    * Caches the memberships in the entity cache if it is enabled.
    *
    * @param memberships the memberships
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Membership> memberships);

    /**
    * Creates a new membership with the primary key. Does not add the membership to the database.
    *
    * @param id the primary key for the new membership
    * @return the new membership
    */
    public br.com.atilo.jcondo.manager.model.Membership create(long id);

    /**
    * Removes the membership with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the membership
    * @return the membership that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Membership updateImpl(
        br.com.atilo.jcondo.manager.model.Membership membership)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the membership with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchMembershipException} if it could not be found.
    *
    * @param id the primary key of the membership
    * @return the membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the membership with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the membership
    * @return the membership, or <code>null</code> if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Membership fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the memberships.
    *
    * @return the memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the memberships.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the memberships.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of memberships
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the memberships from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of memberships.
    *
    * @return the number of memberships
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
