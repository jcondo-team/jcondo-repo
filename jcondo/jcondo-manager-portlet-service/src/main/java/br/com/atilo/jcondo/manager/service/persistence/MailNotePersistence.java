package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.MailNote;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the mail note service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MailNotePersistenceImpl
 * @see MailNoteUtil
 * @generated
 */
public interface MailNotePersistence extends BasePersistence<MailNote> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link MailNoteUtil} to access the mail note persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Caches the mail note in the entity cache if it is enabled.
    *
    * @param mailNote the mail note
    */
    public void cacheResult(br.com.atilo.jcondo.manager.model.MailNote mailNote);

    /**
    * Caches the mail notes in the entity cache if it is enabled.
    *
    * @param mailNotes the mail notes
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.MailNote> mailNotes);

    /**
    * Creates a new mail note with the primary key. Does not add the mail note to the database.
    *
    * @param id the primary key for the new mail note
    * @return the new mail note
    */
    public br.com.atilo.jcondo.manager.model.MailNote create(long id);

    /**
    * Removes the mail note with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the mail note
    * @return the mail note that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchMailNoteException if a mail note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.MailNote remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchMailNoteException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.MailNote updateImpl(
        br.com.atilo.jcondo.manager.model.MailNote mailNote)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the mail note with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchMailNoteException} if it could not be found.
    *
    * @param id the primary key of the mail note
    * @return the mail note
    * @throws br.com.atilo.jcondo.manager.NoSuchMailNoteException if a mail note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.MailNote findByPrimaryKey(long id)
        throws br.com.atilo.jcondo.manager.NoSuchMailNoteException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the mail note with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the mail note
    * @return the mail note, or <code>null</code> if a mail note with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.MailNote fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the mail notes.
    *
    * @return the mail notes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.MailNote> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the mail notes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MailNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of mail notes
    * @param end the upper bound of the range of mail notes (not inclusive)
    * @return the range of mail notes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.MailNote> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the mail notes.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MailNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of mail notes
    * @param end the upper bound of the range of mail notes (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of mail notes
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.MailNote> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the mail notes from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of mail notes.
    *
    * @return the number of mail notes
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
