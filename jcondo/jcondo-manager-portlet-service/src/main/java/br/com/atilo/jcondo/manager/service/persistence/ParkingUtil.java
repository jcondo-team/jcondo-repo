package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Parking;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the parking service. This utility wraps {@link ParkingPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see ParkingPersistence
 * @see ParkingPersistenceImpl
 * @generated
 */
public class ParkingUtil {
    private static ParkingPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Parking parking) {
        getPersistence().clearCache(parking);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Parking> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Parking> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Parking> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Parking update(Parking parking) throws SystemException {
        return getPersistence().update(parking);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Parking update(Parking parking, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(parking, serviceContext);
    }

    /**
    * Returns all the parkings where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @return the matching parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByOwnerDomain(
        long ownerDomainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByOwnerDomain(ownerDomainId);
    }

    /**
    * Returns a range of all the parkings where ownerDomainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerDomainId the owner domain ID
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @return the range of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByOwnerDomain(
        long ownerDomainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByOwnerDomain(ownerDomainId, start, end);
    }

    /**
    * Returns an ordered range of all the parkings where ownerDomainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param ownerDomainId the owner domain ID
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByOwnerDomain(
        long ownerDomainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByOwnerDomain(ownerDomainId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking findByOwnerDomain_First(
        long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByOwnerDomain_First(ownerDomainId, orderByComparator);
    }

    /**
    * Returns the first parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching parking, or <code>null</code> if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking fetchByOwnerDomain_First(
        long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByOwnerDomain_First(ownerDomainId, orderByComparator);
    }

    /**
    * Returns the last parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking findByOwnerDomain_Last(
        long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByOwnerDomain_Last(ownerDomainId, orderByComparator);
    }

    /**
    * Returns the last parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching parking, or <code>null</code> if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking fetchByOwnerDomain_Last(
        long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByOwnerDomain_Last(ownerDomainId, orderByComparator);
    }

    /**
    * Returns the parkings before and after the current parking in the ordered set where ownerDomainId = &#63;.
    *
    * @param id the primary key of the current parking
    * @param ownerDomainId the owner domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking[] findByOwnerDomain_PrevAndNext(
        long id, long ownerDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByOwnerDomain_PrevAndNext(id, ownerDomainId,
            orderByComparator);
    }

    /**
    * Removes all the parkings where ownerDomainId = &#63; from the database.
    *
    * @param ownerDomainId the owner domain ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByOwnerDomain(long ownerDomainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByOwnerDomain(ownerDomainId);
    }

    /**
    * Returns the number of parkings where ownerDomainId = &#63;.
    *
    * @param ownerDomainId the owner domain ID
    * @return the number of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public static int countByOwnerDomain(long ownerDomainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByOwnerDomain(ownerDomainId);
    }

    /**
    * Returns all the parkings where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @return the matching parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByRenterDomain(
        long renterDomainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRenterDomain(renterDomainId);
    }

    /**
    * Returns a range of all the parkings where renterDomainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param renterDomainId the renter domain ID
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @return the range of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByRenterDomain(
        long renterDomainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByRenterDomain(renterDomainId, start, end);
    }

    /**
    * Returns an ordered range of all the parkings where renterDomainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param renterDomainId the renter domain ID
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> findByRenterDomain(
        long renterDomainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRenterDomain(renterDomainId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first parking in the ordered set where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking findByRenterDomain_First(
        long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRenterDomain_First(renterDomainId, orderByComparator);
    }

    /**
    * Returns the first parking in the ordered set where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching parking, or <code>null</code> if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking fetchByRenterDomain_First(
        long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByRenterDomain_First(renterDomainId, orderByComparator);
    }

    /**
    * Returns the last parking in the ordered set where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking findByRenterDomain_Last(
        long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRenterDomain_Last(renterDomainId, orderByComparator);
    }

    /**
    * Returns the last parking in the ordered set where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching parking, or <code>null</code> if a matching parking could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking fetchByRenterDomain_Last(
        long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByRenterDomain_Last(renterDomainId, orderByComparator);
    }

    /**
    * Returns the parkings before and after the current parking in the ordered set where renterDomainId = &#63;.
    *
    * @param id the primary key of the current parking
    * @param renterDomainId the renter domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking[] findByRenterDomain_PrevAndNext(
        long id, long renterDomainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByRenterDomain_PrevAndNext(id, renterDomainId,
            orderByComparator);
    }

    /**
    * Removes all the parkings where renterDomainId = &#63; from the database.
    *
    * @param renterDomainId the renter domain ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByRenterDomain(long renterDomainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByRenterDomain(renterDomainId);
    }

    /**
    * Returns the number of parkings where renterDomainId = &#63;.
    *
    * @param renterDomainId the renter domain ID
    * @return the number of matching parkings
    * @throws SystemException if a system exception occurred
    */
    public static int countByRenterDomain(long renterDomainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByRenterDomain(renterDomainId);
    }

    /**
    * Caches the parking in the entity cache if it is enabled.
    *
    * @param parking the parking
    */
    public static void cacheResult(
        br.com.atilo.jcondo.manager.model.Parking parking) {
        getPersistence().cacheResult(parking);
    }

    /**
    * Caches the parkings in the entity cache if it is enabled.
    *
    * @param parkings the parkings
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Parking> parkings) {
        getPersistence().cacheResult(parkings);
    }

    /**
    * Creates a new parking with the primary key. Does not add the parking to the database.
    *
    * @param id the primary key for the new parking
    * @return the new parking
    */
    public static br.com.atilo.jcondo.manager.model.Parking create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the parking with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the parking
    * @return the parking that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.manager.model.Parking updateImpl(
        br.com.atilo.jcondo.manager.model.Parking parking)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(parking);
    }

    /**
    * Returns the parking with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchParkingException} if it could not be found.
    *
    * @param id the primary key of the parking
    * @return the parking
    * @throws br.com.atilo.jcondo.manager.NoSuchParkingException if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.manager.NoSuchParkingException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the parking with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the parking
    * @return the parking, or <code>null</code> if a parking with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Parking fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the parkings.
    *
    * @return the parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the parkings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @return the range of parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the parkings.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.ParkingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of parkings
    * @param end the upper bound of the range of parkings (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of parkings
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Parking> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the parkings from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of parkings.
    *
    * @return the number of parkings
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static ParkingPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (ParkingPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.manager.service.ClpSerializer.getServletContextName(),
                    ParkingPersistence.class.getName());

            ReferenceRegistry.registerReference(ParkingUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(ParkingPersistence persistence) {
    }
}
