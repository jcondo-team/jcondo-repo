package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AccessService}.
 *
 * @author Brian Wing Shun Chan
 * @see AccessService
 * @generated
 */
public class AccessServiceWrapper implements AccessService,
    ServiceWrapper<AccessService> {
    private AccessService _accessService;

    public AccessServiceWrapper(AccessService accessService) {
        _accessService = accessService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _accessService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _accessService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _accessService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public AccessService getWrappedAccessService() {
        return _accessService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedAccessService(AccessService accessService) {
        _accessService = accessService;
    }

    @Override
    public AccessService getWrappedService() {
        return _accessService;
    }

    @Override
    public void setWrappedService(AccessService accessService) {
        _accessService = accessService;
    }
}
