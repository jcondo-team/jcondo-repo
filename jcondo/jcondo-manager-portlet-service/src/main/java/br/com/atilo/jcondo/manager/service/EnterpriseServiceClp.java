package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.InvokableService;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class EnterpriseServiceClp implements EnterpriseService {
    private InvokableService _invokableService;
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;

    public EnterpriseServiceClp(InvokableService invokableService) {
        _invokableService = invokableService;

        _methodName0 = "getBeanIdentifier";

        _methodParameterTypes0 = new String[] {  };

        _methodName1 = "setBeanIdentifier";

        _methodParameterTypes1 = new String[] { "java.lang.String" };

        _methodName3 = "createEnterprise";

        _methodParameterTypes3 = new String[] {  };

        _methodName4 = "addEnterprise";

        _methodParameterTypes4 = new String[] {
                "java.lang.String", "java.lang.String",
                "br.com.atilo.jcondo.datatype.DomainType",
                "br.com.atilo.jcondo.datatype.EnterpriseStatus",
                "java.lang.String"
            };

        _methodName5 = "updateEnterprise";

        _methodParameterTypes5 = new String[] {
                "long", "java.lang.String", "java.lang.String",
                "br.com.atilo.jcondo.datatype.DomainType",
                "br.com.atilo.jcondo.datatype.EnterpriseStatus",
                "java.lang.String"
            };

        _methodName6 = "getEnterprise";

        _methodParameterTypes6 = new String[] { "long" };

        _methodName7 = "getEnterprisesByType";

        _methodParameterTypes7 = new String[] {
                "br.com.atilo.jcondo.datatype.DomainType"
            };

        _methodName8 = "getEnterpriseByIdentity";

        _methodParameterTypes8 = new String[] { "java.lang.String" };
    }

    @Override
    public java.lang.String getBeanIdentifier() {
        Object returnObj = null;

        try {
            returnObj = _invokableService.invokeMethod(_methodName0,
                    _methodParameterTypes0, new Object[] {  });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.lang.String) ClpSerializer.translateOutput(returnObj);
    }

    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        try {
            _invokableService.invokeMethod(_methodName1,
                _methodParameterTypes1,
                new Object[] { ClpSerializer.translateInput(beanIdentifier) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        throw new UnsupportedOperationException();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise createEnterprise() {
        Object returnObj = null;

        try {
            returnObj = _invokableService.invokeMethod(_methodName3,
                    _methodParameterTypes3, new Object[] {  });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (br.com.atilo.jcondo.manager.model.Enterprise) ClpSerializer.translateOutput(returnObj);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise addEnterprise(
        java.lang.String identity, java.lang.String name,
        br.com.atilo.jcondo.datatype.DomainType type,
        br.com.atilo.jcondo.datatype.EnterpriseStatus status,
        java.lang.String description)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableService.invokeMethod(_methodName4,
                    _methodParameterTypes4,
                    new Object[] {
                        ClpSerializer.translateInput(identity),
                        
                    ClpSerializer.translateInput(name),
                        
                    ClpSerializer.translateInput(type),
                        
                    ClpSerializer.translateInput(status),
                        
                    ClpSerializer.translateInput(description)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
                throw (com.liferay.portal.kernel.exception.PortalException) t;
            }

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (br.com.atilo.jcondo.manager.model.Enterprise) ClpSerializer.translateOutput(returnObj);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise updateEnterprise(
        long enterpriseId, java.lang.String identity, java.lang.String name,
        br.com.atilo.jcondo.datatype.DomainType type,
        br.com.atilo.jcondo.datatype.EnterpriseStatus status,
        java.lang.String description)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableService.invokeMethod(_methodName5,
                    _methodParameterTypes5,
                    new Object[] {
                        enterpriseId,
                        
                    ClpSerializer.translateInput(identity),
                        
                    ClpSerializer.translateInput(name),
                        
                    ClpSerializer.translateInput(type),
                        
                    ClpSerializer.translateInput(status),
                        
                    ClpSerializer.translateInput(description)
                    });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
                throw (com.liferay.portal.kernel.exception.PortalException) t;
            }

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (br.com.atilo.jcondo.manager.model.Enterprise) ClpSerializer.translateOutput(returnObj);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise getEnterprise(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableService.invokeMethod(_methodName6,
                    _methodParameterTypes6, new Object[] { id });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
                throw (com.liferay.portal.kernel.exception.PortalException) t;
            }

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (br.com.atilo.jcondo.manager.model.Enterprise) ClpSerializer.translateOutput(returnObj);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Enterprise> getEnterprisesByType(
        br.com.atilo.jcondo.datatype.DomainType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableService.invokeMethod(_methodName7,
                    _methodParameterTypes7,
                    new Object[] { ClpSerializer.translateInput(type) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
                throw (com.liferay.portal.kernel.exception.PortalException) t;
            }

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (java.util.List<br.com.atilo.jcondo.manager.model.Enterprise>) ClpSerializer.translateOutput(returnObj);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Enterprise getEnterpriseByIdentity(
        java.lang.String identity)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        Object returnObj = null;

        try {
            returnObj = _invokableService.invokeMethod(_methodName8,
                    _methodParameterTypes8,
                    new Object[] { ClpSerializer.translateInput(identity) });
        } catch (Throwable t) {
            t = ClpSerializer.translateThrowable(t);

            if (t instanceof com.liferay.portal.kernel.exception.PortalException) {
                throw (com.liferay.portal.kernel.exception.PortalException) t;
            }

            if (t instanceof com.liferay.portal.kernel.exception.SystemException) {
                throw (com.liferay.portal.kernel.exception.SystemException) t;
            }

            if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new RuntimeException(t.getClass().getName() +
                    " is not a valid exception");
            }
        }

        return (br.com.atilo.jcondo.manager.model.Enterprise) ClpSerializer.translateOutput(returnObj);
    }
}
