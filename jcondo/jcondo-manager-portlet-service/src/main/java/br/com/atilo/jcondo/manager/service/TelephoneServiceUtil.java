package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Telephone. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.TelephoneServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see TelephoneService
 * @see br.com.atilo.jcondo.manager.service.base.TelephoneServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.TelephoneServiceImpl
 * @generated
 */
public class TelephoneServiceUtil {
    private static TelephoneService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.TelephoneServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone addDomainPhone(
        long domainId, long number, long extension, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addDomainPhone(domainId, number, extension, primary);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone addPersonPhone(
        long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary,
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addPersonPhone(number, extension, type, primary, personId);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone updateEnterprisePhone(
        long phoneId, long number, long extension, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateEnterprisePhone(phoneId, number, extension, primary);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone updatePersonPhone(
        long phoneId, long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updatePersonPhone(phoneId, number, extension, type, primary);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone deletePhone(
        long phoneId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePhone(phoneId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getPersonPhones(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonPhones(personId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getPersonPhones(
        long personId, br.com.atilo.jcondo.datatype.PhoneType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonPhones(personId, type);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone getPersonPhone(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonPhone(personId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getDomainPhones(
        long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainPhones(domainId);
    }

    public static void clearService() {
        _service = null;
    }

    public static TelephoneService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    TelephoneService.class.getName());

            if (invokableService instanceof TelephoneService) {
                _service = (TelephoneService) invokableService;
            } else {
                _service = new TelephoneServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(TelephoneServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(TelephoneService service) {
    }
}
