package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.TelephoneServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.TelephoneServiceSoap
 * @generated
 */
public class TelephoneSoap implements Serializable {
    private long _id;
    private String _typeName;
    private long _extension;
    private long _number;
    private boolean _primary;

    public TelephoneSoap() {
    }

    public static TelephoneSoap toSoapModel(Telephone model) {
        TelephoneSoap soapModel = new TelephoneSoap();

        soapModel.setId(model.getId());
        soapModel.setTypeName(model.getTypeName());
        soapModel.setExtension(model.getExtension());
        soapModel.setNumber(model.getNumber());
        soapModel.setPrimary(model.getPrimary());

        return soapModel;
    }

    public static TelephoneSoap[] toSoapModels(Telephone[] models) {
        TelephoneSoap[] soapModels = new TelephoneSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static TelephoneSoap[][] toSoapModels(Telephone[][] models) {
        TelephoneSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new TelephoneSoap[models.length][models[0].length];
        } else {
            soapModels = new TelephoneSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static TelephoneSoap[] toSoapModels(List<Telephone> models) {
        List<TelephoneSoap> soapModels = new ArrayList<TelephoneSoap>(models.size());

        for (Telephone model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new TelephoneSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public String getTypeName() {
        return _typeName;
    }

    public void setTypeName(String typeName) {
        _typeName = typeName;
    }

    public long getExtension() {
        return _extension;
    }

    public void setExtension(long extension) {
        _extension = extension;
    }

    public long getNumber() {
        return _number;
    }

    public void setNumber(long number) {
        _number = number;
    }

    public boolean getPrimary() {
        return _primary;
    }

    public boolean isPrimary() {
        return _primary;
    }

    public void setPrimary(boolean primary) {
        _primary = primary;
    }
}
