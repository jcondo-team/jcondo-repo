package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Mail;
import br.com.atilo.jcondo.manager.service.MailLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class MailActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public MailActionableDynamicQuery() throws SystemException {
        setBaseLocalService(MailLocalServiceUtil.getService());
        setClass(Mail.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
