package br.com.atilo.jcondo.manager.service.persistence;

public interface MailFinder {
    public java.util.List<br.com.atilo.jcondo.manager.model.Mail> findMails(
        long id, java.lang.String code, java.lang.String recipientName,
        long flatId);
}
