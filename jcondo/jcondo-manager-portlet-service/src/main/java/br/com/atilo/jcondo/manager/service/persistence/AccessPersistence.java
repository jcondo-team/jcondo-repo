package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Access;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the access service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see AccessPersistenceImpl
 * @see AccessUtil
 * @generated
 */
public interface AccessPersistence extends BasePersistence<Access> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link AccessUtil} to access the access persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns the access where badge = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessException} if it could not be found.
    *
    * @param badge the badge
    * @return the matching access
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a matching access could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access findByBadge(
        java.lang.String badge)
        throws br.com.atilo.jcondo.manager.NoSuchAccessException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access where badge = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param badge the badge
    * @return the matching access, or <code>null</code> if a matching access could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access fetchByBadge(
        java.lang.String badge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access where badge = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param badge the badge
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching access, or <code>null</code> if a matching access could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access fetchByBadge(
        java.lang.String badge, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the access where badge = &#63; from the database.
    *
    * @param badge the badge
    * @return the access that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access removeByBadge(
        java.lang.String badge)
        throws br.com.atilo.jcondo.manager.NoSuchAccessException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of accesses where badge = &#63;.
    *
    * @param badge the badge
    * @return the number of matching accesses
    * @throws SystemException if a system exception occurred
    */
    public int countByBadge(java.lang.String badge)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access where vehicleId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessException} if it could not be found.
    *
    * @param vehicleId the vehicle ID
    * @return the matching access
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a matching access could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access findByVehicle(
        long vehicleId)
        throws br.com.atilo.jcondo.manager.NoSuchAccessException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access where vehicleId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param vehicleId the vehicle ID
    * @return the matching access, or <code>null</code> if a matching access could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access fetchByVehicle(
        long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access where vehicleId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param vehicleId the vehicle ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching access, or <code>null</code> if a matching access could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access fetchByVehicle(
        long vehicleId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the access where vehicleId = &#63; from the database.
    *
    * @param vehicleId the vehicle ID
    * @return the access that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access removeByVehicle(
        long vehicleId)
        throws br.com.atilo.jcondo.manager.NoSuchAccessException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of accesses where vehicleId = &#63;.
    *
    * @param vehicleId the vehicle ID
    * @return the number of matching accesses
    * @throws SystemException if a system exception occurred
    */
    public int countByVehicle(long vehicleId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access where visitorId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessException} if it could not be found.
    *
    * @param visitorId the visitor ID
    * @return the matching access
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a matching access could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access findByVisitor(
        long visitorId)
        throws br.com.atilo.jcondo.manager.NoSuchAccessException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access where visitorId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param visitorId the visitor ID
    * @return the matching access, or <code>null</code> if a matching access could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access fetchByVisitor(
        long visitorId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access where visitorId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param visitorId the visitor ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching access, or <code>null</code> if a matching access could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access fetchByVisitor(
        long visitorId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the access where visitorId = &#63; from the database.
    *
    * @param visitorId the visitor ID
    * @return the access that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access removeByVisitor(
        long visitorId)
        throws br.com.atilo.jcondo.manager.NoSuchAccessException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of accesses where visitorId = &#63;.
    *
    * @param visitorId the visitor ID
    * @return the number of matching accesses
    * @throws SystemException if a system exception occurred
    */
    public int countByVisitor(long visitorId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the access in the entity cache if it is enabled.
    *
    * @param access the access
    */
    public void cacheResult(br.com.atilo.jcondo.manager.model.Access access);

    /**
    * Caches the accesses in the entity cache if it is enabled.
    *
    * @param accesses the accesses
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Access> accesses);

    /**
    * Creates a new access with the primary key. Does not add the access to the database.
    *
    * @param id the primary key for the new access
    * @return the new access
    */
    public br.com.atilo.jcondo.manager.model.Access create(long id);

    /**
    * Removes the access with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the access
    * @return the access that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a access with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchAccessException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Access updateImpl(
        br.com.atilo.jcondo.manager.model.Access access)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchAccessException} if it could not be found.
    *
    * @param id the primary key of the access
    * @return the access
    * @throws br.com.atilo.jcondo.manager.NoSuchAccessException if a access with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access findByPrimaryKey(long id)
        throws br.com.atilo.jcondo.manager.NoSuchAccessException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the access with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the access
    * @return the access, or <code>null</code> if a access with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Access fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the accesses.
    *
    * @return the accesses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Access> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the accesses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of accesses
    * @param end the upper bound of the range of accesses (not inclusive)
    * @return the range of accesses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Access> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the accesses.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.AccessModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of accesses
    * @param end the upper bound of the range of accesses (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of accesses
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Access> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the accesses from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of accesses.
    *
    * @return the number of accesses
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
