package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.ClpSerializer;
import br.com.atilo.jcondo.manager.service.ParkingLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class ParkingClp extends BaseModelImpl<Parking> implements Parking {
    private long _id;
    private String _code;
    private int _typeId;
    private long _ownerDomainId;
    private long _renterDomainId;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _parkingRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public ParkingClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Parking.class;
    }

    @Override
    public String getModelClassName() {
        return Parking.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("code", getCode());
        attributes.put("typeId", getTypeId());
        attributes.put("ownerDomainId", getOwnerDomainId());
        attributes.put("renterDomainId", getRenterDomainId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String code = (String) attributes.get("code");

        if (code != null) {
            setCode(code);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Long ownerDomainId = (Long) attributes.get("ownerDomainId");

        if (ownerDomainId != null) {
            setOwnerDomainId(ownerDomainId);
        }

        Long renterDomainId = (Long) attributes.get("renterDomainId");

        if (renterDomainId != null) {
            setRenterDomainId(renterDomainId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_parkingRemoteModel != null) {
            try {
                Class<?> clazz = _parkingRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_parkingRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCode() {
        return _code;
    }

    @Override
    public void setCode(String code) {
        _code = code;

        if (_parkingRemoteModel != null) {
            try {
                Class<?> clazz = _parkingRemoteModel.getClass();

                Method method = clazz.getMethod("setCode", String.class);

                method.invoke(_parkingRemoteModel, code);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTypeId() {
        return _typeId;
    }

    @Override
    public void setTypeId(int typeId) {
        _typeId = typeId;

        if (_parkingRemoteModel != null) {
            try {
                Class<?> clazz = _parkingRemoteModel.getClass();

                Method method = clazz.getMethod("setTypeId", int.class);

                method.invoke(_parkingRemoteModel, typeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOwnerDomainId() {
        return _ownerDomainId;
    }

    @Override
    public void setOwnerDomainId(long ownerDomainId) {
        _ownerDomainId = ownerDomainId;

        if (_parkingRemoteModel != null) {
            try {
                Class<?> clazz = _parkingRemoteModel.getClass();

                Method method = clazz.getMethod("setOwnerDomainId", long.class);

                method.invoke(_parkingRemoteModel, ownerDomainId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getRenterDomainId() {
        return _renterDomainId;
    }

    @Override
    public void setRenterDomainId(long renterDomainId) {
        _renterDomainId = renterDomainId;

        if (_parkingRemoteModel != null) {
            try {
                Class<?> clazz = _parkingRemoteModel.getClass();

                Method method = clazz.getMethod("setRenterDomainId", long.class);

                method.invoke(_parkingRemoteModel, renterDomainId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_parkingRemoteModel != null) {
            try {
                Class<?> clazz = _parkingRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_parkingRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_parkingRemoteModel != null) {
            try {
                Class<?> clazz = _parkingRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_parkingRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getRenterDomain() {
        try {
            String methodName = "getRenterDomain";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.liferay.portal.model.BaseModel<?> returnObj = (com.liferay.portal.model.BaseModel<?>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.datatype.ParkingType getType() {
        try {
            String methodName = "getType";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.datatype.ParkingType returnObj = (br.com.atilo.jcondo.datatype.ParkingType) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getOwnerDomain() {
        try {
            String methodName = "getOwnerDomain";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.liferay.portal.model.BaseModel<?> returnObj = (com.liferay.portal.model.BaseModel<?>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getParkingRemoteModel() {
        return _parkingRemoteModel;
    }

    public void setParkingRemoteModel(BaseModel<?> parkingRemoteModel) {
        _parkingRemoteModel = parkingRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _parkingRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_parkingRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            ParkingLocalServiceUtil.addParking(this);
        } else {
            ParkingLocalServiceUtil.updateParking(this);
        }
    }

    @Override
    public Parking toEscapedModel() {
        return (Parking) ProxyUtil.newProxyInstance(Parking.class.getClassLoader(),
            new Class[] { Parking.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        ParkingClp clone = new ParkingClp();

        clone.setId(getId());
        clone.setCode(getCode());
        clone.setTypeId(getTypeId());
        clone.setOwnerDomainId(getOwnerDomainId());
        clone.setRenterDomainId(getRenterDomainId());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Parking parking) {
        long primaryKey = parking.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ParkingClp)) {
            return false;
        }

        ParkingClp parking = (ParkingClp) obj;

        long primaryKey = parking.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", code=");
        sb.append(getCode());
        sb.append(", typeId=");
        sb.append(getTypeId());
        sb.append(", ownerDomainId=");
        sb.append(getOwnerDomainId());
        sb.append(", renterDomainId=");
        sb.append(getRenterDomainId());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(25);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Parking");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>code</column-name><column-value><![CDATA[");
        sb.append(getCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>typeId</column-name><column-value><![CDATA[");
        sb.append(getTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>ownerDomainId</column-name><column-value><![CDATA[");
        sb.append(getOwnerDomainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>renterDomainId</column-name><column-value><![CDATA[");
        sb.append(getRenterDomainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
