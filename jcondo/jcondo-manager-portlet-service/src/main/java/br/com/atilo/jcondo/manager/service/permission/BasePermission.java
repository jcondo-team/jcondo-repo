package br.com.atilo.jcondo.manager.service.permission;

import java.io.Serializable;

import br.com.atilo.jcondo.manager.NoSuchDepartmentException;
import br.com.atilo.jcondo.manager.NoSuchDomainException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.DepartmentLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.security.permission.PermissionChecker;
import com.liferay.portal.security.permission.PermissionCheckerFactoryUtil;
import com.liferay.portal.security.permission.PermissionThreadLocal;

public abstract class BasePermission {

	protected static boolean hasPermission(Permission p, long personId, Class<? extends Serializable> clazz, long modelId, Organization organization) throws Exception {
		PermissionChecker permissionChecker = PermissionCheckerFactoryUtil.create(PersonLocalServiceUtil.getPerson(personId).getUser());

		while (organization != null) {
            if(permissionChecker.hasPermission(organization.getGroup().getGroupId(), clazz.getName(), modelId, p.name())) {
            	return true;
            }
			organization = organization.getParentOrganization();
		}
		return false;
	}

	protected static boolean hasPermission(Permission p, Class<? extends Serializable> clazz, long modelId, Organization organization) throws PortalException, SystemException {
		if (organization == null) {
			return PermissionThreadLocal.getPermissionChecker().hasPermission(0, clazz.getName(), modelId, p.name());
		} else {
			while (organization != null) {
	            if(PermissionThreadLocal.getPermissionChecker().hasPermission(organization.getGroup().getGroupId(), clazz.getName(), modelId, p.name())) {
	            	return true;
	            }
				organization = organization.getParentOrganization();
			}
		}

		return false;
	}

	protected static long getOrganizationDomain(long domainId) throws PortalException, SystemException {
		try {
			return FlatLocalServiceUtil.getFlat(domainId).getOrganizationId();
		} catch (NoSuchFlatException e) {
			try {
				return EnterpriseLocalServiceUtil.getEnterprise(domainId).getOrganizationId();
			} catch (NoSuchEnterpriseException e2) {
				try {
					return DepartmentLocalServiceUtil.getDepartment(domainId).getOrganizationId();
				} catch (NoSuchDepartmentException e3) {
					throw new NoSuchDomainException();
				}
			}
		}
	}
}