package br.com.atilo.jcondo.manager.service.persistence;

public interface AccessPermissionFinder {
    public java.util.List<br.com.atilo.jcondo.manager.model.AccessPermission> findAccessPermission(
        long personId, long domainId, java.util.Date date);
}
