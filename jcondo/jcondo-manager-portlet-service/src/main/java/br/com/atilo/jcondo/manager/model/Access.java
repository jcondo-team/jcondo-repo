package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Access service. Represents a row in the &quot;jco_access&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see AccessModel
 * @see br.com.atilo.jcondo.manager.model.impl.AccessImpl
 * @see br.com.atilo.jcondo.manager.model.impl.AccessModelImpl
 * @generated
 */
public interface Access extends AccessModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.AccessImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.manager.model.Vehicle getVehicle()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;
}
