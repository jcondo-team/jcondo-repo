package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.MembershipServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.MembershipServiceSoap
 * @generated
 */
public class MembershipSoap implements Serializable {
    private long _id;
    private long _personId;
    private long _domainId;
    private int _typeId;
    private Date _oprDate;
    private long _oprUser;

    public MembershipSoap() {
    }

    public static MembershipSoap toSoapModel(Membership model) {
        MembershipSoap soapModel = new MembershipSoap();

        soapModel.setId(model.getId());
        soapModel.setPersonId(model.getPersonId());
        soapModel.setDomainId(model.getDomainId());
        soapModel.setTypeId(model.getTypeId());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static MembershipSoap[] toSoapModels(Membership[] models) {
        MembershipSoap[] soapModels = new MembershipSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static MembershipSoap[][] toSoapModels(Membership[][] models) {
        MembershipSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new MembershipSoap[models.length][models[0].length];
        } else {
            soapModels = new MembershipSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static MembershipSoap[] toSoapModels(List<Membership> models) {
        List<MembershipSoap> soapModels = new ArrayList<MembershipSoap>(models.size());

        for (Membership model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new MembershipSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getPersonId() {
        return _personId;
    }

    public void setPersonId(long personId) {
        _personId = personId;
    }

    public long getDomainId() {
        return _domainId;
    }

    public void setDomainId(long domainId) {
        _domainId = domainId;
    }

    public int getTypeId() {
        return _typeId;
    }

    public void setTypeId(int typeId) {
        _typeId = typeId;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
