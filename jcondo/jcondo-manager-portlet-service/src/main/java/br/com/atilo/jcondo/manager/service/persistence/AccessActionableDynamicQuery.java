package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Access;
import br.com.atilo.jcondo.manager.service.AccessLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class AccessActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public AccessActionableDynamicQuery() throws SystemException {
        setBaseLocalService(AccessLocalServiceUtil.getService());
        setClass(Access.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
