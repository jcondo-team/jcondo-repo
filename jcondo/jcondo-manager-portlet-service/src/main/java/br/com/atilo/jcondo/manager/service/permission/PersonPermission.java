package br.com.atilo.jcondo.manager.service.permission;

import java.util.List;

import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.security.Permission;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.security.permission.ActionKeys;
import com.liferay.portal.security.permission.PermissionThreadLocal;
import com.liferay.portal.service.OrganizationLocalServiceUtil;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.service.permission.PortalPermissionUtil;
import com.liferay.portal.service.permission.UserPermissionUtil;

public class PersonPermission extends BasePermission {

	public static boolean hasPermission(Permission permission, PersonType type, long domainId) throws PortalException, SystemException {
		Permission p = permission;
		if (p == Permission.ADD) {
			PortalPermissionUtil.check(PermissionThreadLocal.getPermissionChecker(), ActionKeys.ADD_USER);
			p = Permission.ASSIGN_MEMBER;
			
		}
		Organization organization = OrganizationLocalServiceUtil.getOrganization(getOrganizationDomain(domainId));
		return hasPermission(p, type.getClass(), type.ordinal(), organization);
	}
	
	public static boolean hasPermission(Permission permission, Person person, long domainId) throws PortalException, SystemException {
		boolean hasPermission = false;

		if (ServiceContextThreadLocal.getServiceContext().getUserId() == person.getUserId()) {
			return true;
		}

		Permission p = permission;
		if (p == Permission.ADD) {
			if (!PortalPermissionUtil.contains(PermissionThreadLocal.getPermissionChecker(), ActionKeys.ADD_USER)) {
				return false;
			}
			p = Permission.ASSIGN_MEMBER;
			hasPermission = true;
		} else {
			hasPermission = UserPermissionUtil.contains(PermissionThreadLocal.getPermissionChecker(), person.getUserId(), p.name());
		}

		List<Membership> memberships = person.getMemberships();

		if (memberships.isEmpty()) {
			hasPermission = true;
		} else {
			if (hasPermission) {
				for (Membership ms : memberships) {
					hasPermission = hasPermission && hasPermission(p, ms.getType(), domainId);
				}
			} else {
				for (Membership ms : memberships) {
					if (!(hasPermission = ms.getType() != PersonType.OWNER && ms.getType() != PersonType.RENTER && ms.getType() != PersonType.DEPENDENT)) {
						break;
					}
				}
			}
		}

		return hasPermission;
	}

}
