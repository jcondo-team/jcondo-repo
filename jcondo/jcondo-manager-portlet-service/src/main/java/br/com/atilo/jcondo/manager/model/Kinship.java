package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Kinship service. Represents a row in the &quot;jco_kinship&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see KinshipModel
 * @see br.com.atilo.jcondo.manager.model.impl.KinshipImpl
 * @see br.com.atilo.jcondo.manager.model.impl.KinshipModelImpl
 * @generated
 */
public interface Kinship extends KinshipModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.KinshipImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.datatype.KinType getType();

    public void setType(br.com.atilo.jcondo.datatype.KinType type);
}
