package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link MembershipService}.
 *
 * @author Brian Wing Shun Chan
 * @see MembershipService
 * @generated
 */
public class MembershipServiceWrapper implements MembershipService,
    ServiceWrapper<MembershipService> {
    private MembershipService _membershipService;

    public MembershipServiceWrapper(MembershipService membershipService) {
        _membershipService = membershipService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _membershipService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _membershipService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _membershipService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Membership createMembership(
        long id) {
        return _membershipService.createMembership(id);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Membership addMembership(
        long personId, long domainId,
        br.com.atilo.jcondo.datatype.PersonType type)
        throws java.lang.Exception {
        return _membershipService.addMembership(personId, domainId, type);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Membership deleteMembership(
        long id) throws java.lang.Exception {
        return _membershipService.deleteMembership(id);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Membership updateMembership(
        long id, br.com.atilo.jcondo.datatype.PersonType type)
        throws java.lang.Exception {
        return _membershipService.updateMembership(id, type);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> getPersonMemberships(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _membershipService.getPersonMemberships(personId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Membership> getDomainMembershipsByType(
        long domainId, br.com.atilo.jcondo.datatype.PersonType type)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _membershipService.getDomainMembershipsByType(domainId, type);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Membership getDomainMembershipByPerson(
        long domainId, long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _membershipService.getDomainMembershipByPerson(domainId, personId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public MembershipService getWrappedMembershipService() {
        return _membershipService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedMembershipService(MembershipService membershipService) {
        _membershipService = membershipService;
    }

    @Override
    public MembershipService getWrappedService() {
        return _membershipService;
    }

    @Override
    public void setWrappedService(MembershipService membershipService) {
        _membershipService = membershipService;
    }
}
