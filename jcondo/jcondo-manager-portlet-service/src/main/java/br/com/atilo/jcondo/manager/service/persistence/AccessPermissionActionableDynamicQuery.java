package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.AccessPermission;
import br.com.atilo.jcondo.manager.service.AccessPermissionLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public abstract class AccessPermissionActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public AccessPermissionActionableDynamicQuery() throws SystemException {
        setBaseLocalService(AccessPermissionLocalServiceUtil.getService());
        setClass(AccessPermission.class);

        setClassLoader(br.com.atilo.jcondo.manager.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
