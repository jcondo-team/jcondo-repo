package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Membership. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.MembershipServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author Brian Wing Shun Chan
 * @see MembershipService
 * @see br.com.atilo.jcondo.manager.service.base.MembershipServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.MembershipServiceImpl
 * @generated
 */
public class MembershipServiceUtil {
    private static MembershipService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.MembershipServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Membership createMembership(
        long id) {
        return getService().createMembership(id);
    }

    public static br.com.atilo.jcondo.manager.model.Membership addMembership(
        long personId, long domainId,
        br.com.atilo.jcondo.datatype.PersonType type)
        throws java.lang.Exception {
        return getService().addMembership(personId, domainId, type);
    }

    public static br.com.atilo.jcondo.manager.model.Membership deleteMembership(
        long id) throws java.lang.Exception {
        return getService().deleteMembership(id);
    }

    public static br.com.atilo.jcondo.manager.model.Membership updateMembership(
        long id, br.com.atilo.jcondo.datatype.PersonType type)
        throws java.lang.Exception {
        return getService().updateMembership(id, type);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> getPersonMemberships(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonMemberships(personId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> getDomainMembershipsByType(
        long domainId, br.com.atilo.jcondo.datatype.PersonType type)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainMembershipsByType(domainId, type);
    }

    public static br.com.atilo.jcondo.manager.model.Membership getDomainMembershipByPerson(
        long domainId, long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainMembershipByPerson(domainId, personId);
    }

    public static void clearService() {
        _service = null;
    }

    public static MembershipService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    MembershipService.class.getName());

            if (invokableService instanceof MembershipService) {
                _service = (MembershipService) invokableService;
            } else {
                _service = new MembershipServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(MembershipServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(MembershipService service) {
    }
}
