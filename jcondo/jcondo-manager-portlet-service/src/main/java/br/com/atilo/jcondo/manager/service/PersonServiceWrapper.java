package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link PersonService}.
 *
 * @author Brian Wing Shun Chan
 * @see PersonService
 * @generated
 */
public class PersonServiceWrapper implements PersonService,
    ServiceWrapper<PersonService> {
    private PersonService _personService;

    public PersonServiceWrapper(PersonService personService) {
        _personService = personService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _personService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _personService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _personService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person createPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.createPerson();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person addPerson(
        java.lang.String name, java.lang.String surname,
        java.lang.String identity, java.lang.String email,
        java.util.Date birthday, br.com.atilo.jcondo.datatype.Gender gender,
        java.lang.String remark,
        br.com.atilo.jcondo.datatype.PersonType personType, long domainId)
        throws java.lang.Exception {
        return _personService.addPerson(name, surname, identity, email,
            birthday, gender, remark, personType, domainId);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person updatePerson(long id,
        java.lang.String name, java.lang.String surname,
        java.lang.String identity, java.lang.String email,
        java.util.Date birthday, br.com.atilo.jcondo.datatype.Gender gender,
        java.lang.String remark, long domainId,
        br.com.atilo.jcondo.manager.model.Membership membership)
        throws java.lang.Exception {
        return _personService.updatePerson(id, name, surname, identity, email,
            birthday, gender, remark, domainId, membership);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person updatePortrait(
        long personId, br.com.atilo.jcondo.Image portrait)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.updatePortrait(personId, portrait);
    }

    @Override
    public void updatePassword(long personId, java.lang.String password,
        java.lang.String newPassword)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _personService.updatePassword(personId, password, newPassword);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPersonByIdentity(
        java.lang.String identity)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.getPersonByIdentity(identity);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.getPerson(id);
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.getPerson();
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.datatype.PersonType> getPersonTypes(
        long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.getPersonTypes(domainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getDomainPeople(
        long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.getDomainPeople(domainId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getDomainPeopleByType(
        long domainId, br.com.atilo.jcondo.datatype.PersonType personType)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.getDomainPeopleByType(domainId, personType);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> getDomainPeopleByName(
        long domainId, java.lang.String name)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.getDomainPeopleByName(domainId, name);
    }

    @Override
    public void completeUseRegistration(long companyId,
        java.lang.String emailAddress, boolean autoPassword, boolean sendEmail)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        _personService.completeUseRegistration(companyId, emailAddress,
            autoPassword, sendEmail);
    }

    @Override
    public boolean authenticatePerson(long personId, java.lang.String password)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _personService.authenticatePerson(personId, password);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public PersonService getWrappedPersonService() {
        return _personService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedPersonService(PersonService personService) {
        _personService = personService;
    }

    @Override
    public PersonService getWrappedService() {
        return _personService;
    }

    @Override
    public void setWrappedService(PersonService personService) {
        _personService = personService;
    }
}
