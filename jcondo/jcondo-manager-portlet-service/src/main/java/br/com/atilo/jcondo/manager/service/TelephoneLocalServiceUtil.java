package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Telephone. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.TelephoneLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see TelephoneLocalService
 * @see br.com.atilo.jcondo.manager.service.base.TelephoneLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.TelephoneLocalServiceImpl
 * @generated
 */
public class TelephoneLocalServiceUtil {
    private static TelephoneLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.TelephoneLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the telephone to the database. Also notifies the appropriate model listeners.
    *
    * @param telephone the telephone
    * @return the telephone that was added
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Telephone addTelephone(
        br.com.atilo.jcondo.manager.model.Telephone telephone)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addTelephone(telephone);
    }

    /**
    * Creates a new telephone with the primary key. Does not add the telephone to the database.
    *
    * @param id the primary key for the new telephone
    * @return the new telephone
    */
    public static br.com.atilo.jcondo.manager.model.Telephone createTelephone(
        long id) {
        return getService().createTelephone(id);
    }

    /**
    * Deletes the telephone with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the telephone
    * @return the telephone that was removed
    * @throws PortalException if a telephone with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Telephone deleteTelephone(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteTelephone(id);
    }

    /**
    * Deletes the telephone from the database. Also notifies the appropriate model listeners.
    *
    * @param telephone the telephone
    * @return the telephone that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Telephone deleteTelephone(
        br.com.atilo.jcondo.manager.model.Telephone telephone)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteTelephone(telephone);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone fetchTelephone(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchTelephone(id);
    }

    /**
    * Returns the telephone with the primary key.
    *
    * @param id the primary key of the telephone
    * @return the telephone
    * @throws PortalException if a telephone with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Telephone getTelephone(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getTelephone(id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the telephones.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.TelephoneModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of telephones
    * @param end the upper bound of the range of telephones (not inclusive)
    * @return the range of telephones
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getTelephones(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getTelephones(start, end);
    }

    /**
    * Returns the number of telephones.
    *
    * @return the number of telephones
    * @throws SystemException if a system exception occurred
    */
    public static int getTelephonesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getTelephonesCount();
    }

    /**
    * Updates the telephone in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param telephone the telephone
    * @return the telephone that was updated
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Telephone updateTelephone(
        br.com.atilo.jcondo.manager.model.Telephone telephone)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateTelephone(telephone);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone addDomainPhone(
        long domainId, long number, long extension, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addDomainPhone(domainId, number, extension, primary);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone addPersonPhone(
        long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary,
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addPersonPhone(number, extension, type, primary, personId);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone updateEnterprisePhone(
        long phoneId, long number, long extension, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateEnterprisePhone(phoneId, number, extension, primary);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone updatePersonPhone(
        long phoneId, long number, long extension,
        br.com.atilo.jcondo.datatype.PhoneType type, boolean primary)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updatePersonPhone(phoneId, number, extension, type, primary);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone deletePhone(
        long phoneId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deletePhone(phoneId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getPersonPhones(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonPhones(personId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getPersonPhones(
        long personId, br.com.atilo.jcondo.datatype.PhoneType type)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonPhones(personId, type);
    }

    public static br.com.atilo.jcondo.manager.model.Telephone getPersonPhone(
        long personId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersonPhone(personId);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Telephone> getDomainPhones(
        long domainId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainPhones(domainId);
    }

    public static void clearService() {
        _service = null;
    }

    public static TelephoneLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    TelephoneLocalService.class.getName());

            if (invokableLocalService instanceof TelephoneLocalService) {
                _service = (TelephoneLocalService) invokableLocalService;
            } else {
                _service = new TelephoneLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(TelephoneLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(TelephoneLocalService service) {
    }
}
