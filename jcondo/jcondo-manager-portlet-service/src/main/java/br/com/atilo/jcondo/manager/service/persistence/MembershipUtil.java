package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Membership;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the membership service. This utility wraps {@link MembershipPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see MembershipPersistence
 * @see MembershipPersistenceImpl
 * @generated
 */
public class MembershipUtil {
    private static MembershipPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Membership membership) {
        getPersistence().clearCache(membership);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Membership> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Membership> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Membership> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Membership update(Membership membership)
        throws SystemException {
        return getPersistence().update(membership);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Membership update(Membership membership,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(membership, serviceContext);
    }

    /**
    * Returns all the memberships where personId = &#63;.
    *
    * @param personId the person ID
    * @return the matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByPerson(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson(personId);
    }

    /**
    * Returns a range of all the memberships where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByPerson(
        long personId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson(personId, start, end);
    }

    /**
    * Returns an ordered range of all the memberships where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByPerson(
        long personId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPerson(personId, start, end, orderByComparator);
    }

    /**
    * Returns the first membership in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson_First(personId, orderByComparator);
    }

    /**
    * Returns the first membership in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPerson_First(personId, orderByComparator);
    }

    /**
    * Returns the last membership in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson_Last(personId, orderByComparator);
    }

    /**
    * Returns the last membership in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPerson_Last(personId, orderByComparator);
    }

    /**
    * Returns the memberships before and after the current membership in the ordered set where personId = &#63;.
    *
    * @param id the primary key of the current membership
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership[] findByPerson_PrevAndNext(
        long id, long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPerson_PrevAndNext(id, personId, orderByComparator);
    }

    /**
    * Removes all the memberships where personId = &#63; from the database.
    *
    * @param personId the person ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByPerson(personId);
    }

    /**
    * Returns the number of memberships where personId = &#63;.
    *
    * @param personId the person ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static int countByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByPerson(personId);
    }

    /**
    * Returns all the memberships where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @return the matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomain(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomain(domainId);
    }

    /**
    * Returns a range of all the memberships where domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomain(
        long domainId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomain(domainId, start, end);
    }

    /**
    * Returns an ordered range of all the memberships where domainId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomain(
        long domainId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDomain(domainId, start, end, orderByComparator);
    }

    /**
    * Returns the first membership in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByDomain_First(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomain_First(domainId, orderByComparator);
    }

    /**
    * Returns the first membership in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByDomain_First(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByDomain_First(domainId, orderByComparator);
    }

    /**
    * Returns the last membership in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByDomain_Last(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomain_Last(domainId, orderByComparator);
    }

    /**
    * Returns the last membership in the ordered set where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByDomain_Last(
        long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByDomain_Last(domainId, orderByComparator);
    }

    /**
    * Returns the memberships before and after the current membership in the ordered set where domainId = &#63;.
    *
    * @param id the primary key of the current membership
    * @param domainId the domain ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership[] findByDomain_PrevAndNext(
        long id, long domainId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDomain_PrevAndNext(id, domainId, orderByComparator);
    }

    /**
    * Removes all the memberships where domainId = &#63; from the database.
    *
    * @param domainId the domain ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDomain(long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByDomain(domainId);
    }

    /**
    * Returns the number of memberships where domainId = &#63;.
    *
    * @param domainId the domain ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static int countByDomain(long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByDomain(domainId);
    }

    /**
    * Returns all the memberships where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByType(
        int typeId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByType(typeId);
    }

    /**
    * Returns a range of all the memberships where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByType(
        int typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByType(typeId, start, end);
    }

    /**
    * Returns an ordered range of all the memberships where typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param typeId the type ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByType(
        int typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByType(typeId, start, end, orderByComparator);
    }

    /**
    * Returns the first membership in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByType_First(
        int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByType_First(typeId, orderByComparator);
    }

    /**
    * Returns the first membership in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByType_First(
        int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByType_First(typeId, orderByComparator);
    }

    /**
    * Returns the last membership in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByType_Last(
        int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByType_Last(typeId, orderByComparator);
    }

    /**
    * Returns the last membership in the ordered set where typeId = &#63;.
    *
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByType_Last(
        int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByType_Last(typeId, orderByComparator);
    }

    /**
    * Returns the memberships before and after the current membership in the ordered set where typeId = &#63;.
    *
    * @param id the primary key of the current membership
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership[] findByType_PrevAndNext(
        long id, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByType_PrevAndNext(id, typeId, orderByComparator);
    }

    /**
    * Removes all the memberships where typeId = &#63; from the database.
    *
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByType(int typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByType(typeId);
    }

    /**
    * Returns the number of memberships where typeId = &#63;.
    *
    * @param typeId the type ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static int countByType(int typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByType(typeId);
    }

    /**
    * Returns the membership where domainId = &#63; and personId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchMembershipException} if it could not be found.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @return the matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByDomainAndPerson(
        long domainId, long personId)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomainAndPerson(domainId, personId);
    }

    /**
    * Returns the membership where domainId = &#63; and personId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @return the matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByDomainAndPerson(
        long domainId, long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByDomainAndPerson(domainId, personId);
    }

    /**
    * Returns the membership where domainId = &#63; and personId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByDomainAndPerson(
        long domainId, long personId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDomainAndPerson(domainId, personId, retrieveFromCache);
    }

    /**
    * Removes the membership where domainId = &#63; and personId = &#63; from the database.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @return the membership that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership removeByDomainAndPerson(
        long domainId, long personId)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().removeByDomainAndPerson(domainId, personId);
    }

    /**
    * Returns the number of memberships where domainId = &#63; and personId = &#63;.
    *
    * @param domainId the domain ID
    * @param personId the person ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static int countByDomainAndPerson(long domainId, long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByDomainAndPerson(domainId, personId);
    }

    /**
    * Returns all the memberships where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomainAndType(
        long domainId, int typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomainAndType(domainId, typeId);
    }

    /**
    * Returns a range of all the memberships where domainId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomainAndType(
        long domainId, int typeId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByDomainAndType(domainId, typeId, start, end);
    }

    /**
    * Returns an ordered range of all the memberships where domainId = &#63; and typeId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findByDomainAndType(
        long domainId, int typeId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDomainAndType(domainId, typeId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByDomainAndType_First(
        long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDomainAndType_First(domainId, typeId,
            orderByComparator);
    }

    /**
    * Returns the first membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByDomainAndType_First(
        long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDomainAndType_First(domainId, typeId,
            orderByComparator);
    }

    /**
    * Returns the last membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByDomainAndType_Last(
        long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDomainAndType_Last(domainId, typeId, orderByComparator);
    }

    /**
    * Returns the last membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByDomainAndType_Last(
        long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByDomainAndType_Last(domainId, typeId,
            orderByComparator);
    }

    /**
    * Returns the memberships before and after the current membership in the ordered set where domainId = &#63; and typeId = &#63;.
    *
    * @param id the primary key of the current membership
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership[] findByDomainAndType_PrevAndNext(
        long id, long domainId, int typeId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByDomainAndType_PrevAndNext(id, domainId, typeId,
            orderByComparator);
    }

    /**
    * Removes all the memberships where domainId = &#63; and typeId = &#63; from the database.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByDomainAndType(long domainId, int typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByDomainAndType(domainId, typeId);
    }

    /**
    * Returns the number of memberships where domainId = &#63; and typeId = &#63;.
    *
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static int countByDomainAndType(long domainId, int typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByDomainAndType(domainId, typeId);
    }

    /**
    * Returns the membership where personId = &#63; and domainId = &#63; and typeId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchMembershipException} if it could not be found.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the matching membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByPersonDomainAndType(
        long personId, long domainId, int typeId)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonDomainAndType(personId, domainId, typeId);
    }

    /**
    * Returns the membership where personId = &#63; and domainId = &#63; and typeId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByPersonDomainAndType(
        long personId, long domainId, int typeId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPersonDomainAndType(personId, domainId, typeId);
    }

    /**
    * Returns the membership where personId = &#63; and domainId = &#63; and typeId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching membership, or <code>null</code> if a matching membership could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByPersonDomainAndType(
        long personId, long domainId, int typeId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPersonDomainAndType(personId, domainId, typeId,
            retrieveFromCache);
    }

    /**
    * Removes the membership where personId = &#63; and domainId = &#63; and typeId = &#63; from the database.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the membership that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership removeByPersonDomainAndType(
        long personId, long domainId, int typeId)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .removeByPersonDomainAndType(personId, domainId, typeId);
    }

    /**
    * Returns the number of memberships where personId = &#63; and domainId = &#63; and typeId = &#63;.
    *
    * @param personId the person ID
    * @param domainId the domain ID
    * @param typeId the type ID
    * @return the number of matching memberships
    * @throws SystemException if a system exception occurred
    */
    public static int countByPersonDomainAndType(long personId, long domainId,
        int typeId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .countByPersonDomainAndType(personId, domainId, typeId);
    }

    /**
    * Caches the membership in the entity cache if it is enabled.
    *
    * @param membership the membership
    */
    public static void cacheResult(
        br.com.atilo.jcondo.manager.model.Membership membership) {
        getPersistence().cacheResult(membership);
    }

    /**
    * Caches the memberships in the entity cache if it is enabled.
    *
    * @param memberships the memberships
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Membership> memberships) {
        getPersistence().cacheResult(memberships);
    }

    /**
    * Creates a new membership with the primary key. Does not add the membership to the database.
    *
    * @param id the primary key for the new membership
    * @return the new membership
    */
    public static br.com.atilo.jcondo.manager.model.Membership create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the membership with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the membership
    * @return the membership that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.manager.model.Membership updateImpl(
        br.com.atilo.jcondo.manager.model.Membership membership)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(membership);
    }

    /**
    * Returns the membership with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchMembershipException} if it could not be found.
    *
    * @param id the primary key of the membership
    * @return the membership
    * @throws br.com.atilo.jcondo.manager.NoSuchMembershipException if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.manager.NoSuchMembershipException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the membership with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the membership
    * @return the membership, or <code>null</code> if a membership with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Membership fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the memberships.
    *
    * @return the memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the memberships.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @return the range of memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the memberships.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.MembershipModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of memberships
    * @param end the upper bound of the range of memberships (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of memberships
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Membership> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the memberships from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of memberships.
    *
    * @return the number of memberships
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static MembershipPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (MembershipPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.manager.service.ClpSerializer.getServletContextName(),
                    MembershipPersistence.class.getName());

            ReferenceRegistry.registerReference(MembershipUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(MembershipPersistence persistence) {
    }
}
