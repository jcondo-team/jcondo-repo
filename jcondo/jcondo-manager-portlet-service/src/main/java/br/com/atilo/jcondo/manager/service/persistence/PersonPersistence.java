package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Person;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the person service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PersonPersistenceImpl
 * @see PersonUtil
 * @generated
 */
public interface PersonPersistence extends BasePersistence<Person> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link PersonUtil} to access the person persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns the person where identity = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchPersonException} if it could not be found.
    *
    * @param identity the identity
    * @return the matching person
    * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a matching person could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person findByIdentity(
        java.lang.String identity)
        throws br.com.atilo.jcondo.manager.NoSuchPersonException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the person where identity = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param identity the identity
    * @return the matching person, or <code>null</code> if a matching person could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person fetchByIdentity(
        java.lang.String identity)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the person where identity = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param identity the identity
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching person, or <code>null</code> if a matching person could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person fetchByIdentity(
        java.lang.String identity, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the person where identity = &#63; from the database.
    *
    * @param identity the identity
    * @return the person that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person removeByIdentity(
        java.lang.String identity)
        throws br.com.atilo.jcondo.manager.NoSuchPersonException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of persons where identity = &#63;.
    *
    * @param identity the identity
    * @return the number of matching persons
    * @throws SystemException if a system exception occurred
    */
    public int countByIdentity(java.lang.String identity)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the person where userId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchPersonException} if it could not be found.
    *
    * @param userId the user ID
    * @return the matching person
    * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a matching person could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person findByUser(long userId)
        throws br.com.atilo.jcondo.manager.NoSuchPersonException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the person where userId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param userId the user ID
    * @return the matching person, or <code>null</code> if a matching person could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person fetchByUser(long userId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the person where userId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param userId the user ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching person, or <code>null</code> if a matching person could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person fetchByUser(long userId,
        boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the person where userId = &#63; from the database.
    *
    * @param userId the user ID
    * @return the person that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person removeByUser(long userId)
        throws br.com.atilo.jcondo.manager.NoSuchPersonException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of persons where userId = &#63;.
    *
    * @param userId the user ID
    * @return the number of matching persons
    * @throws SystemException if a system exception occurred
    */
    public int countByUser(long userId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the person in the entity cache if it is enabled.
    *
    * @param person the person
    */
    public void cacheResult(br.com.atilo.jcondo.manager.model.Person person);

    /**
    * Caches the persons in the entity cache if it is enabled.
    *
    * @param persons the persons
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Person> persons);

    /**
    * Creates a new person with the primary key. Does not add the person to the database.
    *
    * @param id the primary key for the new person
    * @return the new person
    */
    public br.com.atilo.jcondo.manager.model.Person create(long id);

    /**
    * Removes the person with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the person
    * @return the person that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a person with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person remove(long id)
        throws br.com.atilo.jcondo.manager.NoSuchPersonException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Person updateImpl(
        br.com.atilo.jcondo.manager.model.Person person)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the person with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchPersonException} if it could not be found.
    *
    * @param id the primary key of the person
    * @return the person
    * @throws br.com.atilo.jcondo.manager.NoSuchPersonException if a person with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person findByPrimaryKey(long id)
        throws br.com.atilo.jcondo.manager.NoSuchPersonException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the person with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the person
    * @return the person, or <code>null</code> if a person with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.manager.model.Person fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the persons.
    *
    * @return the persons
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the persons.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PersonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of persons
    * @param end the upper bound of the range of persons (not inclusive)
    * @return the range of persons
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the persons.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PersonModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of persons
    * @param end the upper bound of the range of persons (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of persons
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.manager.model.Person> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the persons from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of persons.
    *
    * @return the number of persons
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
