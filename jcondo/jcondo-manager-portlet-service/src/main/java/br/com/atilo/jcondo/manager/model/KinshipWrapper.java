package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Kinship}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Kinship
 * @generated
 */
public class KinshipWrapper implements Kinship, ModelWrapper<Kinship> {
    private Kinship _kinship;

    public KinshipWrapper(Kinship kinship) {
        _kinship = kinship;
    }

    @Override
    public Class<?> getModelClass() {
        return Kinship.class;
    }

    @Override
    public String getModelClassName() {
        return Kinship.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("personId", getPersonId());
        attributes.put("relativeId", getRelativeId());
        attributes.put("typeId", getTypeId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Long relativeId = (Long) attributes.get("relativeId");

        if (relativeId != null) {
            setRelativeId(relativeId);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this kinship.
    *
    * @return the primary key of this kinship
    */
    @Override
    public long getPrimaryKey() {
        return _kinship.getPrimaryKey();
    }

    /**
    * Sets the primary key of this kinship.
    *
    * @param primaryKey the primary key of this kinship
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _kinship.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this kinship.
    *
    * @return the ID of this kinship
    */
    @Override
    public long getId() {
        return _kinship.getId();
    }

    /**
    * Sets the ID of this kinship.
    *
    * @param id the ID of this kinship
    */
    @Override
    public void setId(long id) {
        _kinship.setId(id);
    }

    /**
    * Returns the person ID of this kinship.
    *
    * @return the person ID of this kinship
    */
    @Override
    public long getPersonId() {
        return _kinship.getPersonId();
    }

    /**
    * Sets the person ID of this kinship.
    *
    * @param personId the person ID of this kinship
    */
    @Override
    public void setPersonId(long personId) {
        _kinship.setPersonId(personId);
    }

    /**
    * Returns the relative ID of this kinship.
    *
    * @return the relative ID of this kinship
    */
    @Override
    public long getRelativeId() {
        return _kinship.getRelativeId();
    }

    /**
    * Sets the relative ID of this kinship.
    *
    * @param relativeId the relative ID of this kinship
    */
    @Override
    public void setRelativeId(long relativeId) {
        _kinship.setRelativeId(relativeId);
    }

    /**
    * Returns the type ID of this kinship.
    *
    * @return the type ID of this kinship
    */
    @Override
    public int getTypeId() {
        return _kinship.getTypeId();
    }

    /**
    * Sets the type ID of this kinship.
    *
    * @param typeId the type ID of this kinship
    */
    @Override
    public void setTypeId(int typeId) {
        _kinship.setTypeId(typeId);
    }

    /**
    * Returns the opr date of this kinship.
    *
    * @return the opr date of this kinship
    */
    @Override
    public java.util.Date getOprDate() {
        return _kinship.getOprDate();
    }

    /**
    * Sets the opr date of this kinship.
    *
    * @param oprDate the opr date of this kinship
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _kinship.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this kinship.
    *
    * @return the opr user of this kinship
    */
    @Override
    public long getOprUser() {
        return _kinship.getOprUser();
    }

    /**
    * Sets the opr user of this kinship.
    *
    * @param oprUser the opr user of this kinship
    */
    @Override
    public void setOprUser(long oprUser) {
        _kinship.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _kinship.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _kinship.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _kinship.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _kinship.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _kinship.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _kinship.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _kinship.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _kinship.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _kinship.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _kinship.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _kinship.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new KinshipWrapper((Kinship) _kinship.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Kinship kinship) {
        return _kinship.compareTo(kinship);
    }

    @Override
    public int hashCode() {
        return _kinship.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Kinship> toCacheModel() {
        return _kinship.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Kinship toEscapedModel() {
        return new KinshipWrapper(_kinship.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Kinship toUnescapedModel() {
        return new KinshipWrapper(_kinship.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _kinship.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _kinship.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _kinship.persist();
    }

    @Override
    public br.com.atilo.jcondo.datatype.KinType getType() {
        return _kinship.getType();
    }

    @Override
    public void setType(br.com.atilo.jcondo.datatype.KinType type) {
        _kinship.setType(type);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof KinshipWrapper)) {
            return false;
        }

        KinshipWrapper kinshipWrapper = (KinshipWrapper) obj;

        if (Validator.equals(_kinship, kinshipWrapper._kinship)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Kinship getWrappedKinship() {
        return _kinship;
    }

    @Override
    public Kinship getWrappedModel() {
        return _kinship;
    }

    @Override
    public void resetOriginalValues() {
        _kinship.resetOriginalValues();
    }
}
