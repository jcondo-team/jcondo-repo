package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the AccessPermission service. Represents a row in the &quot;jco_access_permission&quot; database table, with each column mapped to a property of this class.
 *
 * @author Brian Wing Shun Chan
 * @see AccessPermissionModel
 * @see br.com.atilo.jcondo.manager.model.impl.AccessPermissionImpl
 * @see br.com.atilo.jcondo.manager.model.impl.AccessPermissionModelImpl
 * @generated
 */
public interface AccessPermission extends AccessPermissionModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.manager.model.impl.AccessPermissionImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
