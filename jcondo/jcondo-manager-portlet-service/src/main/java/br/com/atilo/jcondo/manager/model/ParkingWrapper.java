package br.com.atilo.jcondo.manager.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Parking}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see Parking
 * @generated
 */
public class ParkingWrapper implements Parking, ModelWrapper<Parking> {
    private Parking _parking;

    public ParkingWrapper(Parking parking) {
        _parking = parking;
    }

    @Override
    public Class<?> getModelClass() {
        return Parking.class;
    }

    @Override
    public String getModelClassName() {
        return Parking.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("code", getCode());
        attributes.put("typeId", getTypeId());
        attributes.put("ownerDomainId", getOwnerDomainId());
        attributes.put("renterDomainId", getRenterDomainId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        String code = (String) attributes.get("code");

        if (code != null) {
            setCode(code);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Long ownerDomainId = (Long) attributes.get("ownerDomainId");

        if (ownerDomainId != null) {
            setOwnerDomainId(ownerDomainId);
        }

        Long renterDomainId = (Long) attributes.get("renterDomainId");

        if (renterDomainId != null) {
            setRenterDomainId(renterDomainId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this parking.
    *
    * @return the primary key of this parking
    */
    @Override
    public long getPrimaryKey() {
        return _parking.getPrimaryKey();
    }

    /**
    * Sets the primary key of this parking.
    *
    * @param primaryKey the primary key of this parking
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _parking.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this parking.
    *
    * @return the ID of this parking
    */
    @Override
    public long getId() {
        return _parking.getId();
    }

    /**
    * Sets the ID of this parking.
    *
    * @param id the ID of this parking
    */
    @Override
    public void setId(long id) {
        _parking.setId(id);
    }

    /**
    * Returns the code of this parking.
    *
    * @return the code of this parking
    */
    @Override
    public java.lang.String getCode() {
        return _parking.getCode();
    }

    /**
    * Sets the code of this parking.
    *
    * @param code the code of this parking
    */
    @Override
    public void setCode(java.lang.String code) {
        _parking.setCode(code);
    }

    /**
    * Returns the type ID of this parking.
    *
    * @return the type ID of this parking
    */
    @Override
    public int getTypeId() {
        return _parking.getTypeId();
    }

    /**
    * Sets the type ID of this parking.
    *
    * @param typeId the type ID of this parking
    */
    @Override
    public void setTypeId(int typeId) {
        _parking.setTypeId(typeId);
    }

    /**
    * Returns the owner domain ID of this parking.
    *
    * @return the owner domain ID of this parking
    */
    @Override
    public long getOwnerDomainId() {
        return _parking.getOwnerDomainId();
    }

    /**
    * Sets the owner domain ID of this parking.
    *
    * @param ownerDomainId the owner domain ID of this parking
    */
    @Override
    public void setOwnerDomainId(long ownerDomainId) {
        _parking.setOwnerDomainId(ownerDomainId);
    }

    /**
    * Returns the renter domain ID of this parking.
    *
    * @return the renter domain ID of this parking
    */
    @Override
    public long getRenterDomainId() {
        return _parking.getRenterDomainId();
    }

    /**
    * Sets the renter domain ID of this parking.
    *
    * @param renterDomainId the renter domain ID of this parking
    */
    @Override
    public void setRenterDomainId(long renterDomainId) {
        _parking.setRenterDomainId(renterDomainId);
    }

    /**
    * Returns the opr date of this parking.
    *
    * @return the opr date of this parking
    */
    @Override
    public java.util.Date getOprDate() {
        return _parking.getOprDate();
    }

    /**
    * Sets the opr date of this parking.
    *
    * @param oprDate the opr date of this parking
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _parking.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this parking.
    *
    * @return the opr user of this parking
    */
    @Override
    public long getOprUser() {
        return _parking.getOprUser();
    }

    /**
    * Sets the opr user of this parking.
    *
    * @param oprUser the opr user of this parking
    */
    @Override
    public void setOprUser(long oprUser) {
        _parking.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _parking.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _parking.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _parking.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _parking.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _parking.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _parking.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _parking.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _parking.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _parking.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _parking.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _parking.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new ParkingWrapper((Parking) _parking.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.manager.model.Parking parking) {
        return _parking.compareTo(parking);
    }

    @Override
    public int hashCode() {
        return _parking.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.manager.model.Parking> toCacheModel() {
        return _parking.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking toEscapedModel() {
        return new ParkingWrapper(_parking.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Parking toUnescapedModel() {
        return new ParkingWrapper(_parking.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _parking.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _parking.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _parking.persist();
    }

    @Override
    public br.com.atilo.jcondo.datatype.ParkingType getType() {
        return _parking.getType();
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getOwnerDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parking.getOwnerDomain();
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getRenterDomain()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _parking.getRenterDomain();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof ParkingWrapper)) {
            return false;
        }

        ParkingWrapper parkingWrapper = (ParkingWrapper) obj;

        if (Validator.equals(_parking, parkingWrapper._parking)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Parking getWrappedParking() {
        return _parking;
    }

    @Override
    public Parking getWrappedModel() {
        return _parking;
    }

    @Override
    public void resetOriginalValues() {
        _parking.resetOriginalValues();
    }
}
