package br.com.atilo.jcondo.manager.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableLocalService;

/**
 * Provides the local service utility for Vehicle. This utility wraps
 * {@link br.com.atilo.jcondo.manager.service.impl.VehicleLocalServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on the local server. Methods of this service will not have security checks
 * based on the propagated JAAS credentials because this service can only be
 * accessed from within the same VM.
 *
 * @author Brian Wing Shun Chan
 * @see VehicleLocalService
 * @see br.com.atilo.jcondo.manager.service.base.VehicleLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.manager.service.impl.VehicleLocalServiceImpl
 * @generated
 */
public class VehicleLocalServiceUtil {
    private static VehicleLocalService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.manager.service.impl.VehicleLocalServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Adds the vehicle to the database. Also notifies the appropriate model listeners.
    *
    * @param vehicle the vehicle
    * @return the vehicle that was added
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Vehicle addVehicle(
        br.com.atilo.jcondo.manager.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().addVehicle(vehicle);
    }

    /**
    * Creates a new vehicle with the primary key. Does not add the vehicle to the database.
    *
    * @param id the primary key for the new vehicle
    * @return the new vehicle
    */
    public static br.com.atilo.jcondo.manager.model.Vehicle createVehicle(
        long id) {
        return getService().createVehicle(id);
    }

    /**
    * Deletes the vehicle with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the vehicle
    * @return the vehicle that was removed
    * @throws PortalException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Vehicle deleteVehicle(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicle(id);
    }

    /**
    * Deletes the vehicle from the database. Also notifies the appropriate model listeners.
    *
    * @param vehicle the vehicle
    * @return the vehicle that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Vehicle deleteVehicle(
        br.com.atilo.jcondo.manager.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().deleteVehicle(vehicle);
    }

    public static com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return getService().dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @SuppressWarnings("rawtypes")
    public static java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .dynamicQuery(dynamicQuery, start, end, orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    public static long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().dynamicQueryCount(dynamicQuery, projection);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle fetchVehicle(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getService().fetchVehicle(id);
    }

    /**
    * Returns the vehicle with the primary key.
    *
    * @param id the primary key of the vehicle
    * @return the vehicle
    * @throws PortalException if a vehicle with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Vehicle getVehicle(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicle(id);
    }

    public static com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the vehicles.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.VehicleModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of vehicles
    * @param end the upper bound of the range of vehicles (not inclusive)
    * @return the range of vehicles
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getVehicles(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicles(start, end);
    }

    /**
    * Returns the number of vehicles.
    *
    * @return the number of vehicles
    * @throws SystemException if a system exception occurred
    */
    public static int getVehiclesCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehiclesCount();
    }

    /**
    * Updates the vehicle in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param vehicle the vehicle
    * @return the vehicle that was updated
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Vehicle updateVehicle(
        br.com.atilo.jcondo.manager.model.Vehicle vehicle)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().updateVehicle(vehicle);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle addVehicle(
        java.lang.String license,
        br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .addVehicle(license, type, domainId, portrait, brand, color,
            remark);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle updateVehicle(
        long id, br.com.atilo.jcondo.datatype.VehicleType type, long domainId,
        br.com.atilo.jcondo.Image portrait, java.lang.String brand,
        java.lang.String color, java.lang.String remark)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService()
                   .updateVehicle(id, type, domainId, portrait, brand, color,
            remark);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle updateVehiclePortrait(
        long id, br.com.atilo.jcondo.Image portrait)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateVehiclePortrait(id, portrait);
    }

    public static br.com.atilo.jcondo.manager.model.Vehicle getVehicleByLicense(
        java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicleByLicense(license);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getVehicles(
        java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicles(license);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getVehicles(
        java.util.Map<java.lang.String, java.lang.String> params)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getVehicles(params);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getDomainVehiclesByLicense(
        long domainId, java.lang.String license)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainVehiclesByLicense(domainId, license);
    }

    public static java.util.List<br.com.atilo.jcondo.manager.model.Vehicle> getDomainVehicles(
        long domainId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getService().getDomainVehicles(domainId);
    }

    public static void clearService() {
        _service = null;
    }

    public static VehicleLocalService getService() {
        if (_service == null) {
            InvokableLocalService invokableLocalService = (InvokableLocalService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    VehicleLocalService.class.getName());

            if (invokableLocalService instanceof VehicleLocalService) {
                _service = (VehicleLocalService) invokableLocalService;
            } else {
                _service = new VehicleLocalServiceClp(invokableLocalService);
            }

            ReferenceRegistry.registerReference(VehicleLocalServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(VehicleLocalService service) {
    }
}
