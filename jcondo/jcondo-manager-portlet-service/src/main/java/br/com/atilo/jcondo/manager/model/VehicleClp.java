package br.com.atilo.jcondo.manager.model;

import br.com.atilo.jcondo.manager.service.ClpSerializer;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class VehicleClp extends BaseModelImpl<Vehicle> implements Vehicle {
    private long _id;
    private long _domainId;
    private long _imageId;
    private int _typeId;
    private String _license;
    private String _brand;
    private String _color;
    private String _remark;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _vehicleRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.manager.service.ClpSerializer.class;

    public VehicleClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Vehicle.class;
    }

    @Override
    public String getModelClassName() {
        return Vehicle.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("domainId", getDomainId());
        attributes.put("imageId", getImageId());
        attributes.put("typeId", getTypeId());
        attributes.put("license", getLicense());
        attributes.put("brand", getBrand());
        attributes.put("color", getColor());
        attributes.put("remark", getRemark());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long domainId = (Long) attributes.get("domainId");

        if (domainId != null) {
            setDomainId(domainId);
        }

        Long imageId = (Long) attributes.get("imageId");

        if (imageId != null) {
            setImageId(imageId);
        }

        Integer typeId = (Integer) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        String license = (String) attributes.get("license");

        if (license != null) {
            setLicense(license);
        }

        String brand = (String) attributes.get("brand");

        if (brand != null) {
            setBrand(brand);
        }

        String color = (String) attributes.get("color");

        if (color != null) {
            setColor(color);
        }

        String remark = (String) attributes.get("remark");

        if (remark != null) {
            setRemark(remark);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_vehicleRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getDomainId() {
        return _domainId;
    }

    @Override
    public void setDomainId(long domainId) {
        _domainId = domainId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setDomainId", long.class);

                method.invoke(_vehicleRemoteModel, domainId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getImageId() {
        return _imageId;
    }

    @Override
    public void setImageId(long imageId) {
        _imageId = imageId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setImageId", long.class);

                method.invoke(_vehicleRemoteModel, imageId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public int getTypeId() {
        return _typeId;
    }

    @Override
    public void setTypeId(int typeId) {
        _typeId = typeId;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setTypeId", int.class);

                method.invoke(_vehicleRemoteModel, typeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getLicense() {
        return _license;
    }

    @Override
    public void setLicense(String license) {
        _license = license;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setLicense", String.class);

                method.invoke(_vehicleRemoteModel, license);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getBrand() {
        return _brand;
    }

    @Override
    public void setBrand(String brand) {
        _brand = brand;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setBrand", String.class);

                method.invoke(_vehicleRemoteModel, brand);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getColor() {
        return _color;
    }

    @Override
    public void setColor(String color) {
        _color = color;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setColor", String.class);

                method.invoke(_vehicleRemoteModel, color);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getRemark() {
        return _remark;
    }

    @Override
    public void setRemark(String remark) {
        _remark = remark;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setRemark", String.class);

                method.invoke(_vehicleRemoteModel, remark);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_vehicleRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_vehicleRemoteModel != null) {
            try {
                Class<?> clazz = _vehicleRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_vehicleRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public br.com.atilo.jcondo.Image getPortrait() {
        try {
            String methodName = "getPortrait";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.Image returnObj = (br.com.atilo.jcondo.Image) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setType(br.com.atilo.jcondo.datatype.VehicleType type) {
        try {
            String methodName = "setType";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.datatype.VehicleType.class
                };

            Object[] parameterValues = new Object[] { type };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.datatype.VehicleType getType() {
        try {
            String methodName = "getType";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.datatype.VehicleType returnObj = (br.com.atilo.jcondo.datatype.VehicleType) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public com.liferay.portal.model.BaseModel<?> getDomain() {
        try {
            String methodName = "getDomain";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            com.liferay.portal.model.BaseModel<?> returnObj = (com.liferay.portal.model.BaseModel<?>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setDomain(com.liferay.portal.model.BaseModel<?> domain) {
        try {
            String methodName = "setDomain";

            Class<?>[] parameterTypes = new Class<?>[] {
                    com.liferay.portal.model.BaseModel.class
                };

            Object[] parameterValues = new Object[] { domain };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getVehicleRemoteModel() {
        return _vehicleRemoteModel;
    }

    public void setVehicleRemoteModel(BaseModel<?> vehicleRemoteModel) {
        _vehicleRemoteModel = vehicleRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _vehicleRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_vehicleRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            VehicleLocalServiceUtil.addVehicle(this);
        } else {
            VehicleLocalServiceUtil.updateVehicle(this);
        }
    }

    @Override
    public Vehicle toEscapedModel() {
        return (Vehicle) ProxyUtil.newProxyInstance(Vehicle.class.getClassLoader(),
            new Class[] { Vehicle.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        VehicleClp clone = new VehicleClp();

        clone.setId(getId());
        clone.setDomainId(getDomainId());
        clone.setImageId(getImageId());
        clone.setTypeId(getTypeId());
        clone.setLicense(getLicense());
        clone.setBrand(getBrand());
        clone.setColor(getColor());
        clone.setRemark(getRemark());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Vehicle vehicle) {
        long primaryKey = vehicle.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof VehicleClp)) {
            return false;
        }

        VehicleClp vehicle = (VehicleClp) obj;

        long primaryKey = vehicle.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", domainId=");
        sb.append(getDomainId());
        sb.append(", imageId=");
        sb.append(getImageId());
        sb.append(", typeId=");
        sb.append(getTypeId());
        sb.append(", license=");
        sb.append(getLicense());
        sb.append(", brand=");
        sb.append(getBrand());
        sb.append(", color=");
        sb.append(getColor());
        sb.append(", remark=");
        sb.append(getRemark());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(34);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.manager.model.Vehicle");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>domainId</column-name><column-value><![CDATA[");
        sb.append(getDomainId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>imageId</column-name><column-value><![CDATA[");
        sb.append(getImageId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>typeId</column-name><column-value><![CDATA[");
        sb.append(getTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>license</column-name><column-value><![CDATA[");
        sb.append(getLicense());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>brand</column-name><column-value><![CDATA[");
        sb.append(getBrand());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>color</column-name><column-value><![CDATA[");
        sb.append(getColor());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>remark</column-name><column-value><![CDATA[");
        sb.append(getRemark());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
