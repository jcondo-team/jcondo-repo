package br.com.atilo.jcondo.manager.service.permission;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Organization;
import com.liferay.portal.service.OrganizationLocalServiceUtil;

import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.security.Permission;

public class FlatPermission extends DomainPermission {

	public static boolean hasFlatPermission(Permission permission, long flatId) throws PortalException, SystemException {
		Organization organization = OrganizationLocalServiceUtil.getOrganization(getOrganizationDomain(flatId));
		return hasPermission(permission, Flat.class, flatId, organization);
	}

}
