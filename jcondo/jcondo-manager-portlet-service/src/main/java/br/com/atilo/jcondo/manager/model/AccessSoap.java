package br.com.atilo.jcondo.manager.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.manager.service.http.AccessServiceSoap}.
 *
 * @author Brian Wing Shun Chan
 * @see br.com.atilo.jcondo.manager.service.http.AccessServiceSoap
 * @generated
 */
public class AccessSoap implements Serializable {
    private long _id;
    private long _destinationId;
    private long _visitorId;
    private long _vehicleId;
    private long _authorizerId;
    private String _authorizerName;
    private String _badge;
    private boolean _ended;
    private String _remark;
    private Date _oprDate;
    private long _oprUser;

    public AccessSoap() {
    }

    public static AccessSoap toSoapModel(Access model) {
        AccessSoap soapModel = new AccessSoap();

        soapModel.setId(model.getId());
        soapModel.setDestinationId(model.getDestinationId());
        soapModel.setVisitorId(model.getVisitorId());
        soapModel.setVehicleId(model.getVehicleId());
        soapModel.setAuthorizerId(model.getAuthorizerId());
        soapModel.setAuthorizerName(model.getAuthorizerName());
        soapModel.setBadge(model.getBadge());
        soapModel.setEnded(model.getEnded());
        soapModel.setRemark(model.getRemark());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static AccessSoap[] toSoapModels(Access[] models) {
        AccessSoap[] soapModels = new AccessSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static AccessSoap[][] toSoapModels(Access[][] models) {
        AccessSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new AccessSoap[models.length][models[0].length];
        } else {
            soapModels = new AccessSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static AccessSoap[] toSoapModels(List<Access> models) {
        List<AccessSoap> soapModels = new ArrayList<AccessSoap>(models.size());

        for (Access model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new AccessSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getDestinationId() {
        return _destinationId;
    }

    public void setDestinationId(long destinationId) {
        _destinationId = destinationId;
    }

    public long getVisitorId() {
        return _visitorId;
    }

    public void setVisitorId(long visitorId) {
        _visitorId = visitorId;
    }

    public long getVehicleId() {
        return _vehicleId;
    }

    public void setVehicleId(long vehicleId) {
        _vehicleId = vehicleId;
    }

    public long getAuthorizerId() {
        return _authorizerId;
    }

    public void setAuthorizerId(long authorizerId) {
        _authorizerId = authorizerId;
    }

    public String getAuthorizerName() {
        return _authorizerName;
    }

    public void setAuthorizerName(String authorizerName) {
        _authorizerName = authorizerName;
    }

    public String getBadge() {
        return _badge;
    }

    public void setBadge(String badge) {
        _badge = badge;
    }

    public boolean getEnded() {
        return _ended;
    }

    public boolean isEnded() {
        return _ended;
    }

    public void setEnded(boolean ended) {
        _ended = ended;
    }

    public String getRemark() {
        return _remark;
    }

    public void setRemark(String remark) {
        _remark = remark;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
