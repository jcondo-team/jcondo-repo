package br.com.atilo.jcondo.manager.service.persistence;

import br.com.atilo.jcondo.manager.model.Pet;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the pet service. This utility wraps {@link PetPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PetPersistence
 * @see PetPersistenceImpl
 * @generated
 */
public class PetUtil {
    private static PetPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Pet pet) {
        getPersistence().clearCache(pet);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Pet> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Pet> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Pet> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Pet update(Pet pet) throws SystemException {
        return getPersistence().update(pet);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Pet update(Pet pet, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(pet, serviceContext);
    }

    /**
    * Returns all the pets where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @return the matching pets
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Pet> findByFlat(
        long flatId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByFlat(flatId);
    }

    /**
    * Returns a range of all the pets where flatId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param flatId the flat ID
    * @param start the lower bound of the range of pets
    * @param end the upper bound of the range of pets (not inclusive)
    * @return the range of matching pets
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Pet> findByFlat(
        long flatId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByFlat(flatId, start, end);
    }

    /**
    * Returns an ordered range of all the pets where flatId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param flatId the flat ID
    * @param start the lower bound of the range of pets
    * @param end the upper bound of the range of pets (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching pets
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Pet> findByFlat(
        long flatId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByFlat(flatId, start, end, orderByComparator);
    }

    /**
    * Returns the first pet in the ordered set where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet findByFlat_First(
        long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByFlat_First(flatId, orderByComparator);
    }

    /**
    * Returns the first pet in the ordered set where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching pet, or <code>null</code> if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet fetchByFlat_First(
        long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByFlat_First(flatId, orderByComparator);
    }

    /**
    * Returns the last pet in the ordered set where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet findByFlat_Last(
        long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByFlat_Last(flatId, orderByComparator);
    }

    /**
    * Returns the last pet in the ordered set where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching pet, or <code>null</code> if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet fetchByFlat_Last(
        long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByFlat_Last(flatId, orderByComparator);
    }

    /**
    * Returns the pets before and after the current pet in the ordered set where flatId = &#63;.
    *
    * @param petId the primary key of the current pet
    * @param flatId the flat ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet[] findByFlat_PrevAndNext(
        long petId, long flatId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByFlat_PrevAndNext(petId, flatId, orderByComparator);
    }

    /**
    * Removes all the pets where flatId = &#63; from the database.
    *
    * @param flatId the flat ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByFlat(long flatId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByFlat(flatId);
    }

    /**
    * Returns the number of pets where flatId = &#63;.
    *
    * @param flatId the flat ID
    * @return the number of matching pets
    * @throws SystemException if a system exception occurred
    */
    public static int countByFlat(long flatId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByFlat(flatId);
    }

    /**
    * Returns the pet where typeId = &#63; and flatId = &#63; or throws a {@link br.com.atilo.jcondo.manager.NoSuchPetException} if it could not be found.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @return the matching pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet findByTypeAndFlat(
        int typeId, long flatId)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByTypeAndFlat(typeId, flatId);
    }

    /**
    * Returns the pet where typeId = &#63; and flatId = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @return the matching pet, or <code>null</code> if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet fetchByTypeAndFlat(
        int typeId, long flatId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByTypeAndFlat(typeId, flatId);
    }

    /**
    * Returns the pet where typeId = &#63; and flatId = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching pet, or <code>null</code> if a matching pet could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet fetchByTypeAndFlat(
        int typeId, long flatId, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByTypeAndFlat(typeId, flatId, retrieveFromCache);
    }

    /**
    * Removes the pet where typeId = &#63; and flatId = &#63; from the database.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @return the pet that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet removeByTypeAndFlat(
        int typeId, long flatId)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().removeByTypeAndFlat(typeId, flatId);
    }

    /**
    * Returns the number of pets where typeId = &#63; and flatId = &#63;.
    *
    * @param typeId the type ID
    * @param flatId the flat ID
    * @return the number of matching pets
    * @throws SystemException if a system exception occurred
    */
    public static int countByTypeAndFlat(int typeId, long flatId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByTypeAndFlat(typeId, flatId);
    }

    /**
    * Caches the pet in the entity cache if it is enabled.
    *
    * @param pet the pet
    */
    public static void cacheResult(br.com.atilo.jcondo.manager.model.Pet pet) {
        getPersistence().cacheResult(pet);
    }

    /**
    * Caches the pets in the entity cache if it is enabled.
    *
    * @param pets the pets
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.manager.model.Pet> pets) {
        getPersistence().cacheResult(pets);
    }

    /**
    * Creates a new pet with the primary key. Does not add the pet to the database.
    *
    * @param petId the primary key for the new pet
    * @return the new pet
    */
    public static br.com.atilo.jcondo.manager.model.Pet create(long petId) {
        return getPersistence().create(petId);
    }

    /**
    * Removes the pet with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param petId the primary key of the pet
    * @return the pet that was removed
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet remove(long petId)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(petId);
    }

    public static br.com.atilo.jcondo.manager.model.Pet updateImpl(
        br.com.atilo.jcondo.manager.model.Pet pet)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(pet);
    }

    /**
    * Returns the pet with the primary key or throws a {@link br.com.atilo.jcondo.manager.NoSuchPetException} if it could not be found.
    *
    * @param petId the primary key of the pet
    * @return the pet
    * @throws br.com.atilo.jcondo.manager.NoSuchPetException if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet findByPrimaryKey(
        long petId)
        throws br.com.atilo.jcondo.manager.NoSuchPetException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(petId);
    }

    /**
    * Returns the pet with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param petId the primary key of the pet
    * @return the pet, or <code>null</code> if a pet with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.manager.model.Pet fetchByPrimaryKey(
        long petId) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(petId);
    }

    /**
    * Returns all the pets.
    *
    * @return the pets
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Pet> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the pets.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of pets
    * @param end the upper bound of the range of pets (not inclusive)
    * @return the range of pets
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Pet> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the pets.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.manager.model.impl.PetModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of pets
    * @param end the upper bound of the range of pets (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of pets
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.manager.model.Pet> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the pets from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of pets.
    *
    * @return the number of pets
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static PetPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (PetPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.manager.service.ClpSerializer.getServletContextName(),
                    PetPersistence.class.getName());

            ReferenceRegistry.registerReference(PetUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(PetPersistence persistence) {
    }
}
