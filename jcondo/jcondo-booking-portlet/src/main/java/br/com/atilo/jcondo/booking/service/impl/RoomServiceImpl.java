/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.io.File;
import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.booking.service.base.RoomServiceBaseImpl;
import br.com.atilo.jcondo.booking.service.permission.RoomPermission;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.Image;
import br.com.atilo.jcondo.manager.security.Permission;

/**
 * The implementation of the room remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.RoomService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.RoomServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.RoomServiceUtil
 */
public class RoomServiceImpl extends RoomServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.RoomServiceUtil} to access the room remote service.
	 */
	public Room addRoom(String name, String description, double price, int capacity, boolean available, boolean bookable, File agreement) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.ADD, 0)) {
			throw new BusinessException("exception.room.add.denied");			
		}
		return roomLocalService.addRoom(name, description, price, capacity, available, bookable, agreement);
	}

	public Room addRoom(Room room) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.ADD, 0)) {
			throw new BusinessException("exception.room.add.denied");			
		}

		return roomLocalService.addRoom(room);
	}	

	public Room updateRoom(long roomId, String name, String description, double price, int capacity, boolean available, boolean bookable, File agreement) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.UPDATE, roomId)) {
			throw new BusinessException("exception.room.update.denied");			
		}

		return roomLocalService.updateRoom(roomId, name, description, price, capacity, available, bookable, agreement);
	}

	public Room updateRoom(Room room) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.UPDATE, room.getId())) {
			throw new BusinessException("exception.room.update.denied");			
		}

		return roomLocalService.updateRoom(room);
	}

	public Room deleteRoom(Room room) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.DELETE, room.getId())) {
			throw new BusinessException("exception.room.delete.denied");			
		}

		return roomLocalService.deleteRoom(room);
	}

	public Image addRoomPicture(long roomId, Image image) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.UPDATE, roomId)) {
			throw new BusinessException("exception.room.update.denied");			
		}
		
		return roomLocalService.addRoomPicture(roomId, image);
	}
	
	public Image deleteRoomPicture(long roomId, long imageId) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.UPDATE, roomId)) {
			throw new BusinessException("exception.room.update.denied");			
		}
		
		return roomLocalService.deleteRoomPicture(roomId, imageId);
	}
	
	public List<Room> getRooms(boolean onlyAvailables, boolean onlyBookables) throws PortalException, SystemException {
		return roomLocalService.getRooms(onlyAvailables, onlyBookables);
	}

	public void checkRoomAvailability(long roomId, Date date) throws PortalException, SystemException {
		roomLocalService.checkRoomAvailability(roomId, date);
	}

}