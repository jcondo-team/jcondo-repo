package br.com.atilo.jcondo.booking.service.http;

import br.com.atilo.jcondo.booking.service.RoomBookingServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.booking.service.RoomBookingServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.booking.model.RoomBookingSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.booking.model.RoomBooking}, that is translated to a
 * {@link br.com.atilo.jcondo.booking.model.RoomBookingSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomBookingServiceHttp
 * @see br.com.atilo.jcondo.booking.model.RoomBookingSoap
 * @see br.com.atilo.jcondo.booking.service.RoomBookingServiceUtil
 * @generated
 */
public class RoomBookingServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(RoomBookingServiceSoap.class);

    public static br.com.atilo.jcondo.booking.model.RoomBookingSoap addRoomBooking(
        long roomId, int weekDay, int openHour, int closeHour, int period)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.RoomBooking returnValue = RoomBookingServiceUtil.addRoomBooking(roomId,
                    weekDay, openHour, closeHour, period);

            return br.com.atilo.jcondo.booking.model.RoomBookingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.RoomBookingSoap updateRoomBooking(
        long roomBookingId, int openHour, int closeHour, int period)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.RoomBooking returnValue = RoomBookingServiceUtil.updateRoomBooking(roomBookingId,
                    openHour, closeHour, period);

            return br.com.atilo.jcondo.booking.model.RoomBookingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.RoomBookingSoap updateRoomBooking(
        br.com.atilo.jcondo.booking.model.RoomBookingSoap roomBooking)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.RoomBooking returnValue = RoomBookingServiceUtil.updateRoomBooking(br.com.atilo.jcondo.booking.model.impl.RoomBookingModelImpl.toModel(
                        roomBooking));

            return br.com.atilo.jcondo.booking.model.RoomBookingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.RoomBookingSoap[] getRoomBookings(
        long roomId, int weekDay) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> returnValue =
                RoomBookingServiceUtil.getRoomBookings(roomId, weekDay);

            return br.com.atilo.jcondo.booking.model.RoomBookingSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.RoomBookingSoap[] getRoomBookings(
        long roomId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.booking.model.RoomBooking> returnValue =
                RoomBookingServiceUtil.getRoomBookings(roomId);

            return br.com.atilo.jcondo.booking.model.RoomBookingSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
