package br.com.atilo.jcondo.booking.service.http;

import br.com.atilo.jcondo.booking.service.BookingServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.booking.service.BookingServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.booking.model.BookingSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.booking.model.Booking}, that is translated to a
 * {@link br.com.atilo.jcondo.booking.model.BookingSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingServiceHttp
 * @see br.com.atilo.jcondo.booking.model.BookingSoap
 * @see br.com.atilo.jcondo.booking.service.BookingServiceUtil
 * @generated
 */
public class BookingServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(BookingServiceSoap.class);

    public static br.com.atilo.jcondo.booking.model.BookingSoap[] getDomainBookings(
        long domainId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.booking.model.Booking> returnValue =
                BookingServiceUtil.getDomainBookings(domainId);

            return br.com.atilo.jcondo.booking.model.BookingSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.BookingSoap[] getPersonBookings(
        long personId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.booking.model.Booking> returnValue =
                BookingServiceUtil.getPersonBookings(personId);

            return br.com.atilo.jcondo.booking.model.BookingSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.BookingSoap[] getBookings(
        long roomId, java.util.Date beginDate) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.booking.model.Booking> returnValue =
                BookingServiceUtil.getBookings(roomId, beginDate);

            return br.com.atilo.jcondo.booking.model.BookingSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.BookingSoap[] getBookings(
        long roomId, java.util.Date fromDate, java.util.Date toDate)
        throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.booking.model.Booking> returnValue =
                BookingServiceUtil.getBookings(roomId, fromDate, toDate);

            return br.com.atilo.jcondo.booking.model.BookingSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.BookingSoap addBooking(
        long roomId, long domainId, long personId, java.util.Date beginDate,
        java.util.Date endDate,
        br.com.atilo.jcondo.booking.model.GuestSoap[] guests)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Booking returnValue = BookingServiceUtil.addBooking(roomId,
                    domainId, personId, beginDate, endDate,
                    br.com.atilo.jcondo.booking.model.impl.GuestModelImpl.toModels(
                        guests));

            return br.com.atilo.jcondo.booking.model.BookingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.BookingSoap cancelBooking(
        long bookingId) throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Booking returnValue = BookingServiceUtil.cancelBooking(bookingId);

            return br.com.atilo.jcondo.booking.model.BookingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.BookingSoap deleteBooking(
        long bookingId) throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Booking returnValue = BookingServiceUtil.deleteBooking(bookingId);

            return br.com.atilo.jcondo.booking.model.BookingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.BookingSoap deleteBooking(
        long bookingId, java.lang.String note) throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Booking returnValue = BookingServiceUtil.deleteBooking(bookingId,
                    note);

            return br.com.atilo.jcondo.booking.model.BookingSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
