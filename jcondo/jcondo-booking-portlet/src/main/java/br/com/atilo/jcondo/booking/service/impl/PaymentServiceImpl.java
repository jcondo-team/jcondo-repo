/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import org.apache.log4j.Logger;

import br.com.atilo.jcondo.booking.model.Payment;
import br.com.atilo.jcondo.booking.payment.PaymentProviderException;
import br.com.atilo.jcondo.booking.payment.PaymentScheduler;
import br.com.atilo.jcondo.booking.service.base.PaymentServiceBaseImpl;

/**
 * The implementation of the payment remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.PaymentService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.PaymentServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.PaymentServiceUtil
 */
public class PaymentServiceImpl extends PaymentServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.PaymentServiceUtil} to access the payment remote service.
	 */

	private static final Logger LOGGER = Logger.getLogger(PaymentServiceImpl.class);

	private static final Logger MAIL_LOGGER = Logger.getLogger("MAILER");

	public String addPayment(String paymentToken, String chargeReference, String chargeCode) {
		Payment payment = null;

		try {
			try {
				paymentLocalService.addPayment(paymentToken, Long.valueOf(chargeReference), chargeCode);
			} catch (PaymentProviderException e) {
				payment = (Payment) e.getArgs()[0];
				LOGGER.error("Fail to add payment " + payment.getId() + ": " + e.getMessage());
				PaymentScheduler.schedule(payment);
			}
		} catch (Exception e) {
			MAIL_LOGGER.error("Failure to schedule add payment " + payment + ": " + e.getMessage());
		}

		return "success";
	}

}