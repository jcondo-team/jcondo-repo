package br.com.atilo.jcondo.booking.service.base;

import br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class RoomLocalServiceClpInvoker {
    private String _methodName0;
    private String[] _methodParameterTypes0;
    private String _methodName1;
    private String[] _methodParameterTypes1;
    private String _methodName2;
    private String[] _methodParameterTypes2;
    private String _methodName3;
    private String[] _methodParameterTypes3;
    private String _methodName4;
    private String[] _methodParameterTypes4;
    private String _methodName5;
    private String[] _methodParameterTypes5;
    private String _methodName6;
    private String[] _methodParameterTypes6;
    private String _methodName7;
    private String[] _methodParameterTypes7;
    private String _methodName8;
    private String[] _methodParameterTypes8;
    private String _methodName9;
    private String[] _methodParameterTypes9;
    private String _methodName10;
    private String[] _methodParameterTypes10;
    private String _methodName11;
    private String[] _methodParameterTypes11;
    private String _methodName12;
    private String[] _methodParameterTypes12;
    private String _methodName13;
    private String[] _methodParameterTypes13;
    private String _methodName14;
    private String[] _methodParameterTypes14;
    private String _methodName15;
    private String[] _methodParameterTypes15;
    private String _methodName78;
    private String[] _methodParameterTypes78;
    private String _methodName79;
    private String[] _methodParameterTypes79;
    private String _methodName84;
    private String[] _methodParameterTypes84;
    private String _methodName85;
    private String[] _methodParameterTypes85;
    private String _methodName86;
    private String[] _methodParameterTypes86;
    private String _methodName87;
    private String[] _methodParameterTypes87;
    private String _methodName88;
    private String[] _methodParameterTypes88;
    private String _methodName89;
    private String[] _methodParameterTypes89;
    private String _methodName90;
    private String[] _methodParameterTypes90;
    private String _methodName91;
    private String[] _methodParameterTypes91;
    private String _methodName92;
    private String[] _methodParameterTypes92;
    private String _methodName93;
    private String[] _methodParameterTypes93;

    public RoomLocalServiceClpInvoker() {
        _methodName0 = "addRoom";

        _methodParameterTypes0 = new String[] {
                "br.com.atilo.jcondo.booking.model.Room"
            };

        _methodName1 = "createRoom";

        _methodParameterTypes1 = new String[] { "long" };

        _methodName2 = "deleteRoom";

        _methodParameterTypes2 = new String[] { "long" };

        _methodName3 = "deleteRoom";

        _methodParameterTypes3 = new String[] {
                "br.com.atilo.jcondo.booking.model.Room"
            };

        _methodName4 = "dynamicQuery";

        _methodParameterTypes4 = new String[] {  };

        _methodName5 = "dynamicQuery";

        _methodParameterTypes5 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName6 = "dynamicQuery";

        _methodParameterTypes6 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int"
            };

        _methodName7 = "dynamicQuery";

        _methodParameterTypes7 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery", "int", "int",
                "com.liferay.portal.kernel.util.OrderByComparator"
            };

        _methodName8 = "dynamicQueryCount";

        _methodParameterTypes8 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery"
            };

        _methodName9 = "dynamicQueryCount";

        _methodParameterTypes9 = new String[] {
                "com.liferay.portal.kernel.dao.orm.DynamicQuery",
                "com.liferay.portal.kernel.dao.orm.Projection"
            };

        _methodName10 = "fetchRoom";

        _methodParameterTypes10 = new String[] { "long" };

        _methodName11 = "getRoom";

        _methodParameterTypes11 = new String[] { "long" };

        _methodName12 = "getPersistedModel";

        _methodParameterTypes12 = new String[] { "java.io.Serializable" };

        _methodName13 = "getRooms";

        _methodParameterTypes13 = new String[] { "int", "int" };

        _methodName14 = "getRoomsCount";

        _methodParameterTypes14 = new String[] {  };

        _methodName15 = "updateRoom";

        _methodParameterTypes15 = new String[] {
                "br.com.atilo.jcondo.booking.model.Room"
            };

        _methodName78 = "getBeanIdentifier";

        _methodParameterTypes78 = new String[] {  };

        _methodName79 = "setBeanIdentifier";

        _methodParameterTypes79 = new String[] { "java.lang.String" };

        _methodName84 = "addRoom";

        _methodParameterTypes84 = new String[] {
                "java.lang.String", "java.lang.String", "double", "int",
                "boolean", "boolean", "java.io.File"
            };

        _methodName85 = "addRoom";

        _methodParameterTypes85 = new String[] {
                "br.com.atilo.jcondo.booking.model.Room"
            };

        _methodName86 = "updateRoom";

        _methodParameterTypes86 = new String[] {
                "long", "java.lang.String", "java.lang.String", "double", "int",
                "boolean", "boolean", "java.io.File"
            };

        _methodName87 = "updateRoom";

        _methodParameterTypes87 = new String[] {
                "br.com.atilo.jcondo.booking.model.Room"
            };

        _methodName88 = "deleteRoom";

        _methodParameterTypes88 = new String[] {
                "br.com.atilo.jcondo.booking.model.Room"
            };

        _methodName89 = "addRoomPicture";

        _methodParameterTypes89 = new String[] {
                "long", "br.com.atilo.jcondo.Image"
            };

        _methodName90 = "deleteRoomPicture";

        _methodParameterTypes90 = new String[] { "long", "long" };

        _methodName91 = "getRoomPictures";

        _methodParameterTypes91 = new String[] { "long" };

        _methodName92 = "getRooms";

        _methodParameterTypes92 = new String[] { "boolean", "boolean" };

        _methodName93 = "checkRoomAvailability";

        _methodParameterTypes93 = new String[] { "long", "java.util.Date" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName0.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes0, parameterTypes)) {
            return RoomLocalServiceUtil.addRoom((br.com.atilo.jcondo.booking.model.Room) arguments[0]);
        }

        if (_methodName1.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes1, parameterTypes)) {
            return RoomLocalServiceUtil.createRoom(((Long) arguments[0]).longValue());
        }

        if (_methodName2.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes2, parameterTypes)) {
            return RoomLocalServiceUtil.deleteRoom(((Long) arguments[0]).longValue());
        }

        if (_methodName3.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes3, parameterTypes)) {
            return RoomLocalServiceUtil.deleteRoom((br.com.atilo.jcondo.booking.model.Room) arguments[0]);
        }

        if (_methodName4.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes4, parameterTypes)) {
            return RoomLocalServiceUtil.dynamicQuery();
        }

        if (_methodName5.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes5, parameterTypes)) {
            return RoomLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName6.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes6, parameterTypes)) {
            return RoomLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue());
        }

        if (_methodName7.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes7, parameterTypes)) {
            return RoomLocalServiceUtil.dynamicQuery((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                (com.liferay.portal.kernel.util.OrderByComparator) arguments[3]);
        }

        if (_methodName8.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes8, parameterTypes)) {
            return RoomLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0]);
        }

        if (_methodName9.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes9, parameterTypes)) {
            return RoomLocalServiceUtil.dynamicQueryCount((com.liferay.portal.kernel.dao.orm.DynamicQuery) arguments[0],
                (com.liferay.portal.kernel.dao.orm.Projection) arguments[1]);
        }

        if (_methodName10.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes10, parameterTypes)) {
            return RoomLocalServiceUtil.fetchRoom(((Long) arguments[0]).longValue());
        }

        if (_methodName11.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes11, parameterTypes)) {
            return RoomLocalServiceUtil.getRoom(((Long) arguments[0]).longValue());
        }

        if (_methodName12.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes12, parameterTypes)) {
            return RoomLocalServiceUtil.getPersistedModel((java.io.Serializable) arguments[0]);
        }

        if (_methodName13.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes13, parameterTypes)) {
            return RoomLocalServiceUtil.getRooms(((Integer) arguments[0]).intValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName14.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes14, parameterTypes)) {
            return RoomLocalServiceUtil.getRoomsCount();
        }

        if (_methodName15.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes15, parameterTypes)) {
            return RoomLocalServiceUtil.updateRoom((br.com.atilo.jcondo.booking.model.Room) arguments[0]);
        }

        if (_methodName78.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes78, parameterTypes)) {
            return RoomLocalServiceUtil.getBeanIdentifier();
        }

        if (_methodName79.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes79, parameterTypes)) {
            RoomLocalServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName84.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes84, parameterTypes)) {
            return RoomLocalServiceUtil.addRoom((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                ((Double) arguments[2]).doubleValue(),
                ((Integer) arguments[3]).intValue(),
                ((Boolean) arguments[4]).booleanValue(),
                ((Boolean) arguments[5]).booleanValue(),
                (java.io.File) arguments[6]);
        }

        if (_methodName85.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes85, parameterTypes)) {
            return RoomLocalServiceUtil.addRoom((br.com.atilo.jcondo.booking.model.Room) arguments[0]);
        }

        if (_methodName86.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes86, parameterTypes)) {
            return RoomLocalServiceUtil.updateRoom(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                ((Double) arguments[3]).doubleValue(),
                ((Integer) arguments[4]).intValue(),
                ((Boolean) arguments[5]).booleanValue(),
                ((Boolean) arguments[6]).booleanValue(),
                (java.io.File) arguments[7]);
        }

        if (_methodName87.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes87, parameterTypes)) {
            return RoomLocalServiceUtil.updateRoom((br.com.atilo.jcondo.booking.model.Room) arguments[0]);
        }

        if (_methodName88.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes88, parameterTypes)) {
            return RoomLocalServiceUtil.deleteRoom((br.com.atilo.jcondo.booking.model.Room) arguments[0]);
        }

        if (_methodName89.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes89, parameterTypes)) {
            return RoomLocalServiceUtil.addRoomPicture(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[1]);
        }

        if (_methodName90.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes90, parameterTypes)) {
            return RoomLocalServiceUtil.deleteRoomPicture(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        if (_methodName91.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes91, parameterTypes)) {
            return RoomLocalServiceUtil.getRoomPictures(((Long) arguments[0]).longValue());
        }

        if (_methodName92.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes92, parameterTypes)) {
            return RoomLocalServiceUtil.getRooms(((Boolean) arguments[0]).booleanValue(),
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName93.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes93, parameterTypes)) {
            RoomLocalServiceUtil.checkRoomAvailability(((Long) arguments[0]).longValue(),
                (java.util.Date) arguments[1]);

            return null;
        }

        throw new UnsupportedOperationException();
    }
}
