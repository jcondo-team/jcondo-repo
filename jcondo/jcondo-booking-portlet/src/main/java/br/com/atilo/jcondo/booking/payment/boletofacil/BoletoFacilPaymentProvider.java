package br.com.atilo.jcondo.booking.payment.boletofacil;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import org.apache.commons.httpclient.DefaultHttpMethodRetryHandler;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.model.Payment;
import br.com.atilo.jcondo.booking.payment.PaymentMode;
import br.com.atilo.jcondo.booking.payment.PaymentProvider;
import br.com.atilo.jcondo.booking.payment.PaymentProviderException;
import br.com.atilo.jcondo.booking.payment.PaymentProviderId;
import br.com.atilo.jcondo.booking.payment.boletofacil.Charge;

public class BoletoFacilPaymentProvider implements PaymentProvider {
	
	private static final Logger LOGGER = Logger.getLogger(BoletoFacilPaymentProvider.class);
	
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("application");
	
	/** Token desenvolvimento */
	//private static final String TOKEN = "1ED85FA568BECB4CBFD7387EDF66D22A5579F1E996FE1139";
	
	/** Token produ��o */
	private static final String TOKEN = "10D0EA59A97042EA1A004BCB665AAE6BA4533AC0813FA3B82ADD52759CD77D3C";

	private static final double MIN_TAX_VALUE = 2.5;
	
	private static Gson GSON;

	private double tax = 0.025;
	
	private HttpClient client;

	public BoletoFacilPaymentProvider() {
		GSON = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
		client = new HttpClient();
		client.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler(0, false));
	}
	
	public PaymentProviderId getKey() {
		return PaymentProviderId.BOLETO_FACIL;
	}

	public PaymentMode getPaymentMode() {
		return PaymentMode.PRE_PAID;
	}

	public void addInvoice(BookingInvoice invoice) throws PaymentProviderException {
		PostMethod method = null;

		try {
			double amount;
			double tax = invoice.getAmount() * getTax();
	
			if (tax <= MIN_TAX_VALUE) {
				amount = invoice.getAmount() + MIN_TAX_VALUE;
				tax = MIN_TAX_VALUE;
			} else {
				amount = Math.round(invoice.getAmount() / (1D - getTax()));
				tax = Math.round(amount * getTax());
	
			}

			method = new PostMethod("https://www.boletobancario.com/boletofacil/integration/api/v1/issue-charge");
			method.addParameter("token", TOKEN);
			method.addParameter("amount", String.valueOf(amount));
			method.addParameter("payerName", invoice.getBooking().getPerson().getName() + " " + invoice.getBooking().getPerson().getSurname());
			method.addParameter("payerCpfCnpj", invoice.getBooking().getPerson().getIdentity());
			method.addParameter("reference", String.valueOf(invoice.getId()));
			method.addParameter("dueDate", DateFormatUtils.format(invoice.getDate(), "dd/MM/yyyy"));
			method.addParameter("description", MessageFormat.format(BUNDLE.getString("booking.invoice.description"),
																	invoice.getBooking().getRoom().getName(), 
																	DateFormatUtils.format(invoice.getBooking().getBeginDate(), "dd/MM/yyyy"),
																	DateFormatUtils.format(invoice.getBooking().getBeginDate(), "HH:mm"),
																	DateFormatUtils.format(invoice.getBooking().getEndDate(), "HH:mm")));


			LOGGER.info("adding invoice " + invoice);
			client.executeMethod(method);
			LOGGER.info("message received from provider: " + method.getResponseBodyAsString());

			ChargeResponse chargeResponse = GSON.fromJson(method.getResponseBodyAsString(), ChargeResponse.class);

			if (method.getStatusCode() == 200) {
				Charge charge = chargeResponse.getData().getCharges().get(0);
				invoice.setCode(charge.getCode());
				invoice.setLink(charge.getLink());
				invoice.setTax(tax);
			} else if (method.getStatusCode() == 400) {
				throw new Exception("provider validation error: " + chargeResponse.getErrorMessage());
			} else {
				throw new Exception("HTTP error: " + method.getStatusCode());
			}
		} catch (Exception e) {
			throw new PaymentProviderException(e.getMessage());
		} finally {
			if (method != null) {
				method.releaseConnection();
			}
		}
	}

	public void addPayment(Payment payment) throws PaymentProviderException {
		PostMethod method = null;

		try {
			method = new PostMethod("https://www.boletobancario.com/boletofacil/integration/api/v1/fetch-payment-details");
			method.addParameter("paymentToken", payment.getToken());

			LOGGER.info("getting payment details of invoice " + payment.getInvoiceId() +" with token " + payment.getToken());
			client.executeMethod(method);
			LOGGER.info("message received from provider: " + method.getResponseBodyAsString());

			PaymentResponse paymentResponse = GSON.fromJson(method.getResponseBodyAsString(), PaymentResponse.class);

			if (method.getStatusCode() == 200) {
				payment.setAmount(paymentResponse.getData().getPayment().getAmount());
				payment.setDate(paymentResponse.getData().getPayment().getDate());
			} else if (method.getStatusCode() == 400) {
				throw new Exception("provider validation error: " + paymentResponse.getErrorMessage());
			} else {
				throw new Exception("HTTP error: " + method.getStatusCode());
			}
		} catch (Exception e) {
			throw new PaymentProviderException(e.getMessage(), payment);
		} finally {
			if (method != null) {
				method.releaseConnection();
			}
		}
	}

	public double getTax() {
		return tax;
	}

	public void setTax(double tax) {
		this.tax = tax;
	}

}
