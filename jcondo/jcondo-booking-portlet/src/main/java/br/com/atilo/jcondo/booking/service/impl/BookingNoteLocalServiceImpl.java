/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.util.Date;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.booking.model.BookingNote;
import br.com.atilo.jcondo.booking.service.base.BookingNoteLocalServiceBaseImpl;

/**
 * The implementation of the booking note local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.BookingNoteLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.BookingNoteLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.BookingNoteLocalServiceUtil
 */
public class BookingNoteLocalServiceImpl extends BookingNoteLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.BookingNoteLocalServiceUtil} to access the booking note local service.
	 */
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public BookingNote addBookingNote(BookingNote bookingNote) throws SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		bookingNote.setOprDate(new Date());
		bookingNote.setOprUser(sc.getUserId());
		return super.addBookingNote(bookingNote);
	}
}