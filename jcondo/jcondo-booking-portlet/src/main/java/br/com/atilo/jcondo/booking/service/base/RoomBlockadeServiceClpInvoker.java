package br.com.atilo.jcondo.booking.service.base;

import br.com.atilo.jcondo.booking.service.RoomBlockadeServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class RoomBlockadeServiceClpInvoker {
    private String _methodName62;
    private String[] _methodParameterTypes62;
    private String _methodName63;
    private String[] _methodParameterTypes63;
    private String _methodName68;
    private String[] _methodParameterTypes68;
    private String _methodName69;
    private String[] _methodParameterTypes69;

    public RoomBlockadeServiceClpInvoker() {
        _methodName62 = "getBeanIdentifier";

        _methodParameterTypes62 = new String[] {  };

        _methodName63 = "setBeanIdentifier";

        _methodParameterTypes63 = new String[] { "java.lang.String" };

        _methodName68 = "addRoomBlockade";

        _methodParameterTypes68 = new String[] {
                "br.com.atilo.jcondo.booking.model.RoomBlockade"
            };

        _methodName69 = "deleteRoomBlockade";

        _methodParameterTypes69 = new String[] {
                "br.com.atilo.jcondo.booking.model.RoomBlockade"
            };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName62.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes62, parameterTypes)) {
            return RoomBlockadeServiceUtil.getBeanIdentifier();
        }

        if (_methodName63.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes63, parameterTypes)) {
            RoomBlockadeServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName68.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes68, parameterTypes)) {
            return RoomBlockadeServiceUtil.addRoomBlockade((br.com.atilo.jcondo.booking.model.RoomBlockade) arguments[0]);
        }

        if (_methodName69.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes69, parameterTypes)) {
            return RoomBlockadeServiceUtil.deleteRoomBlockade((br.com.atilo.jcondo.booking.model.RoomBlockade) arguments[0]);
        }

        throw new UnsupportedOperationException();
    }
}
