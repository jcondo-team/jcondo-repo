/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.booking.model.RoomBlockade;
import br.com.atilo.jcondo.booking.service.base.RoomBlockadeServiceBaseImpl;
import br.com.atilo.jcondo.booking.service.permission.RoomPermission;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.security.Permission;

/**
 * The implementation of the room blockade remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.RoomBlockadeService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.RoomBlockadeServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.RoomBlockadeServiceUtil
 */
public class RoomBlockadeServiceImpl extends RoomBlockadeServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.RoomBlockadeServiceUtil} to access the room blockade remote service.
	 */
	
	public RoomBlockade addRoomBlockade(RoomBlockade roomBlockade) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.BLOCK, roomBlockade.getRoomId())) {
			throw new BusinessException("exception.room.blockade.add.denied");
		}
		return roomBlockadeLocalService.addRoomBlockade(roomBlockade);
	}

	public RoomBlockade deleteRoomBlockade(RoomBlockade roomBlockade) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.UNBLOCK, roomBlockade.getRoomId())) {
			throw new BusinessException("exception.room.blockade.delete.denied");
		}
		return roomBlockadeLocalService.deleteRoomBlockade(roomBlockade);
	}
}