package br.com.atilo.jcondo.booking.model.impl;

import br.com.atilo.jcondo.booking.model.Guest;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Guest in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Guest
 * @generated
 */
public class GuestCacheModel implements CacheModel<Guest>, Externalizable {
    public long guestId;
    public long bookingId;
    public String name;
    public String surname;
    public boolean checkedIn;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(15);

        sb.append("{guestId=");
        sb.append(guestId);
        sb.append(", bookingId=");
        sb.append(bookingId);
        sb.append(", name=");
        sb.append(name);
        sb.append(", surname=");
        sb.append(surname);
        sb.append(", checkedIn=");
        sb.append(checkedIn);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Guest toEntityModel() {
        GuestImpl guestImpl = new GuestImpl();

        guestImpl.setGuestId(guestId);
        guestImpl.setBookingId(bookingId);

        if (name == null) {
            guestImpl.setName(StringPool.BLANK);
        } else {
            guestImpl.setName(name);
        }

        if (surname == null) {
            guestImpl.setSurname(StringPool.BLANK);
        } else {
            guestImpl.setSurname(surname);
        }

        guestImpl.setCheckedIn(checkedIn);

        if (oprDate == Long.MIN_VALUE) {
            guestImpl.setOprDate(null);
        } else {
            guestImpl.setOprDate(new Date(oprDate));
        }

        guestImpl.setOprUser(oprUser);

        guestImpl.resetOriginalValues();

        return guestImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        guestId = objectInput.readLong();
        bookingId = objectInput.readLong();
        name = objectInput.readUTF();
        surname = objectInput.readUTF();
        checkedIn = objectInput.readBoolean();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(guestId);
        objectOutput.writeLong(bookingId);

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        if (surname == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(surname);
        }

        objectOutput.writeBoolean(checkedIn);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
