package br.com.atilo.jcondo.booking.payment;

import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.model.Payment;

public interface PaymentProvider {
	
	public PaymentProviderId getKey();

	public PaymentMode getPaymentMode();

	public void addInvoice(BookingInvoice invoice) throws PaymentProviderException;

	public void addPayment(Payment payment) throws PaymentProviderException;

}
