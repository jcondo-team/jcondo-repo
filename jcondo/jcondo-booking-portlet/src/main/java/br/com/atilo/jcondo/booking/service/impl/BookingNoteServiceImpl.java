/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.booking.model.BookingNote;
import br.com.atilo.jcondo.booking.service.base.BookingNoteServiceBaseImpl;

/**
 * The implementation of the booking note remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.BookingNoteService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.BookingNoteServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.BookingNoteServiceUtil
 */
public class BookingNoteServiceImpl extends BookingNoteServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.BookingNoteServiceUtil} to access the booking note remote service.
	 */
	
	public BookingNote addBookingNote(BookingNote bookingNote) throws SystemException {
		return bookingNoteLocalService.addBookingNote(bookingNote);
	}

}