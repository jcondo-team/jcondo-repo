package br.com.atilo.jcondo.booking.bean;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.datatype.BookingStatus;
import br.com.atilo.jcondo.booking.service.permission.BookingPermission;
import br.com.atilo.jcondo.manager.NoSuchDomainException;
import br.com.atilo.jcondo.datatype.DomainType;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;

@ManagedBean
@SessionScoped
public class BookingBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(BookingBean.class);	

	@ManagedProperty(value="#{roomBean}")
	private RoomBean roomBean;

	@ManagedProperty(value="#{calendarBean}")
	private CalendarBean calendarBean;

	@ManagedProperty(value="#{myBookingsBean}")
	private MyBookingsBean myBookingsBean;

	public BookingBean() {
	}

	@PostConstruct
	public void init() {
		try {
			myBookingsBean.addObserver(calendarBean);
			calendarBean.addObserver(myBookingsBean);
		} catch (Exception e) {
			LOGGER.fatal("Failure on room initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public boolean isCancelEnabled(Booking booking) {
		Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		Date bookingDate = DateUtils.truncate(booking.getBeginDate(), Calendar.DAY_OF_MONTH);
		return !bookingDate.before(today) && booking.getStatus() != BookingStatus.CANCELLED;
	}

	public boolean hasDeadlineExceeded(Booking booking) {
		if (booking.getBeginDate() != null) {
			Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			Date bookingDate = DateUtils.truncate(booking.getBeginDate(), Calendar.DAY_OF_MONTH);
			Date deadline = DateUtils.addDays(bookingDate, -7);
			return today.after(deadline);
		}
		return false;
	}

	public boolean canCancelBooking(Booking booking) {
		try {
			return BookingPermission.hasPermission(Permission.CANCEL, booking);
		} catch (Exception e) {
			LOGGER.error("Failure on booking cancel permission check", e);
		}
		
		return false;
	}

	public boolean canDeleteBooking(Booking booking) {
		try {
			return BookingPermission.hasPermission(Permission.DELETE, booking);
		} catch (Exception e) {
			LOGGER.error("Failure on booking delete permission check", e);
		}
		
		return false;
	}	

	public boolean canAssignPerson(Booking booking) {
		try {
			return BookingPermission.hasPermission(Permission.ASSIGN_PERSON, booking);
		} catch (NoSuchDomainException e) {
			Booking b = (Booking) booking.clone();
			try {
				b.setDomain(EnterpriseLocalServiceUtil.getEnterprisesByType(DomainType.ADMINISTRATION).get(0));
				return BookingPermission.hasPermission(Permission.ASSIGN_PERSON, b);
			} catch (Exception e2) {
				LOGGER.error("Failure on assign person permission check", e2);
			}
		} catch (Exception e) {
			LOGGER.error("Failure on assign person permission check", e);
		}
		
		return false;
	}	

	public RoomBean getRoomBean() {
		return roomBean;
	}

	public void setRoomBean(RoomBean roomBean) {
		this.roomBean = roomBean;
	}

	public CalendarBean getCalendarBean() {
		return calendarBean;
	}

	public void setCalendarBean(CalendarBean calendarBean) {
		this.calendarBean = calendarBean;
	}

	public MyBookingsBean getMyBookingsBean() {
		return myBookingsBean;
	}

	public void setMyBookingsBean(MyBookingsBean myBookingsBean) {
		this.myBookingsBean = myBookingsBean;
	}

}
