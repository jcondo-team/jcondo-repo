package br.com.atilo.jcondo.booking.payment.boletofacil;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class PaymentResponse {

	private boolean success;

	private String errorMessage;
	
	private PaymentData data;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(data).append(success).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof PaymentResponse) == false) {
			return false;
		}
		PaymentResponse rhs = ((PaymentResponse) other);
		return new EqualsBuilder().append(data, rhs.data).append(success, rhs.success).isEquals();
	}	
	
	public PaymentData getData() {
		return data;
	}

	public void setData(PaymentData data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}

class PaymentData {

	private Payment payment = new Payment();
	
	public Payment getPayment() {
		return payment;
	}
	
	public void setPayment(Payment payment) {
		this.payment = payment;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(payment).toHashCode();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof PaymentData) == false) {
			return false;
		}
		PaymentData rhs = ((PaymentData) other);
		return new EqualsBuilder().append(payment, rhs.payment).isEquals();
	}
}