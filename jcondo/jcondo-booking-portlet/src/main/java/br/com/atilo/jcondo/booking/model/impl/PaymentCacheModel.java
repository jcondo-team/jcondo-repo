package br.com.atilo.jcondo.booking.model.impl;

import br.com.atilo.jcondo.booking.model.Payment;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Payment in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Payment
 * @generated
 */
public class PaymentCacheModel implements CacheModel<Payment>, Externalizable {
    public long id;
    public long invoiceId;
    public String token;
    public double amount;
    public long date;
    public boolean checked;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{id=");
        sb.append(id);
        sb.append(", invoiceId=");
        sb.append(invoiceId);
        sb.append(", token=");
        sb.append(token);
        sb.append(", amount=");
        sb.append(amount);
        sb.append(", date=");
        sb.append(date);
        sb.append(", checked=");
        sb.append(checked);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Payment toEntityModel() {
        PaymentImpl paymentImpl = new PaymentImpl();

        paymentImpl.setId(id);
        paymentImpl.setInvoiceId(invoiceId);

        if (token == null) {
            paymentImpl.setToken(StringPool.BLANK);
        } else {
            paymentImpl.setToken(token);
        }

        paymentImpl.setAmount(amount);

        if (date == Long.MIN_VALUE) {
            paymentImpl.setDate(null);
        } else {
            paymentImpl.setDate(new Date(date));
        }

        paymentImpl.setChecked(checked);

        if (oprDate == Long.MIN_VALUE) {
            paymentImpl.setOprDate(null);
        } else {
            paymentImpl.setOprDate(new Date(oprDate));
        }

        paymentImpl.setOprUser(oprUser);

        paymentImpl.resetOriginalValues();

        return paymentImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        invoiceId = objectInput.readLong();
        token = objectInput.readUTF();
        amount = objectInput.readDouble();
        date = objectInput.readLong();
        checked = objectInput.readBoolean();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(invoiceId);

        if (token == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(token);
        }

        objectOutput.writeDouble(amount);
        objectOutput.writeLong(date);
        objectOutput.writeBoolean(checked);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
