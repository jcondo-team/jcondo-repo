package br.com.atilo.jcondo.booking.model.impl;

import br.com.atilo.jcondo.booking.model.RoomBlockade;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing RoomBlockade in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see RoomBlockade
 * @generated
 */
public class RoomBlockadeCacheModel implements CacheModel<RoomBlockade>,
    Externalizable {
    public long id;
    public long roomId;
    public long beginDate;
    public long endDate;
    public String description;
    public boolean recursive;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{id=");
        sb.append(id);
        sb.append(", roomId=");
        sb.append(roomId);
        sb.append(", beginDate=");
        sb.append(beginDate);
        sb.append(", endDate=");
        sb.append(endDate);
        sb.append(", description=");
        sb.append(description);
        sb.append(", recursive=");
        sb.append(recursive);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public RoomBlockade toEntityModel() {
        RoomBlockadeImpl roomBlockadeImpl = new RoomBlockadeImpl();

        roomBlockadeImpl.setId(id);
        roomBlockadeImpl.setRoomId(roomId);

        if (beginDate == Long.MIN_VALUE) {
            roomBlockadeImpl.setBeginDate(null);
        } else {
            roomBlockadeImpl.setBeginDate(new Date(beginDate));
        }

        if (endDate == Long.MIN_VALUE) {
            roomBlockadeImpl.setEndDate(null);
        } else {
            roomBlockadeImpl.setEndDate(new Date(endDate));
        }

        if (description == null) {
            roomBlockadeImpl.setDescription(StringPool.BLANK);
        } else {
            roomBlockadeImpl.setDescription(description);
        }

        roomBlockadeImpl.setRecursive(recursive);

        if (oprDate == Long.MIN_VALUE) {
            roomBlockadeImpl.setOprDate(null);
        } else {
            roomBlockadeImpl.setOprDate(new Date(oprDate));
        }

        roomBlockadeImpl.setOprUser(oprUser);

        roomBlockadeImpl.resetOriginalValues();

        return roomBlockadeImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        roomId = objectInput.readLong();
        beginDate = objectInput.readLong();
        endDate = objectInput.readLong();
        description = objectInput.readUTF();
        recursive = objectInput.readBoolean();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(roomId);
        objectOutput.writeLong(beginDate);
        objectOutput.writeLong(endDate);

        if (description == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(description);
        }

        objectOutput.writeBoolean(recursive);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
