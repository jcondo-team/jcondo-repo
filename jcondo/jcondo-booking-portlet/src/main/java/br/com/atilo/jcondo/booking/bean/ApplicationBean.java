package br.com.atilo.jcondo.booking.bean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import org.apache.log4j.Logger;

import br.com.atilo.jcondo.booking.model.listener.BookingInvoiceListener;
import br.com.atilo.jcondo.booking.model.listener.BookingListener;
import br.com.atilo.jcondo.booking.service.persistence.BookingInvoiceUtil;
import br.com.atilo.jcondo.booking.service.persistence.BookingUtil;

@ManagedBean
@ApplicationScoped
public class ApplicationBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(ApplicationBean.class);	

	@PostConstruct
	public void init() {
		try {
			BookingUtil.getPersistence().registerListener(new BookingListener());
			BookingInvoiceUtil.getPersistence().registerListener(new BookingInvoiceListener());
		} catch (Exception e) {
			LOGGER.error("Failure on application initialization", e);
		}
	}
}
