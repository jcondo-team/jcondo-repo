/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.model.impl;

import java.util.ArrayList;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.Payment;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.PaymentLocalServiceUtil;

/**
 * The extended model implementation for the BookingInvoice service. Represents a row in the &quot;jco_booking_invoice&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.model.BookingInvoice} interface.
 * </p>
 *
 * @author anderson
 */
public class BookingInvoiceImpl extends BookingInvoiceBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a booking invoice model instance should use the {@link br.com.atilo.jcondo.booking.model.BookingInvoice} interface instead.
	 */
	public BookingInvoiceImpl() {
	}
	
	public Booking getBooking() throws PortalException, SystemException {
		return getBookingId() == 0 ? null : BookingLocalServiceUtil.getBooking(getBookingId());
	}

	public void setBooking(Booking booking) throws PortalException, SystemException {
		if (booking == null) {
			setBookingId(0);
		} else {
			setBookingId(booking.getId());
		}
	}

	public List<Payment> getPayments() throws SystemException {
		return getId() == 0 ? new ArrayList<Payment>() : PaymentLocalServiceUtil.getPayments(getId());
	}
}