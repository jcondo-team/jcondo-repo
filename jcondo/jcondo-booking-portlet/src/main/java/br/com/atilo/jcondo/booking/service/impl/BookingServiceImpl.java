/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.util.Date;
import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.service.base.BookingServiceBaseImpl;
import br.com.atilo.jcondo.booking.service.permission.BookingPermission;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.security.Permission;

/**
 * The implementation of the booking remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.BookingService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.BookingServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.BookingServiceUtil
 */
public class BookingServiceImpl extends BookingServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.BookingServiceUtil} to access the booking remote service.
	 */
	
	public List<Booking> getDomainBookings(long domainId) throws SystemException {
		return bookingLocalService.getDomainBookings(domainId);
	}

	public List<Booking> getPersonBookings(long personId) throws SystemException {
		return bookingLocalService.getPersonBookings(personId);
	}

	public List<Booking> getBookings(long roomId, Date beginDate) throws PortalException, SystemException {
		return bookingLocalService.getBookings(roomId, beginDate);
	}

	public List<Booking> getBookings(long roomId, Date fromDate, Date toDate) throws PortalException, SystemException {
		return bookingLocalService.getBookings(roomId, fromDate, toDate);
	}

	public Booking addBooking(long roomId, long domainId, long personId, Date beginDate, Date endDate, List<Guest> guests) throws Exception {
		if (!BookingPermission.hasPermission(Permission.ADD, personId, domainId)) {
			throw new BusinessException("exception.booking.create.denied");
		}
		return bookingLocalService.addBooking(roomId, domainId, personId, beginDate, endDate, guests);
	}

	public Booking cancelBooking(long bookingId) throws Exception {
		Booking booking = bookingLocalService.getBooking(bookingId);
		if (!BookingPermission.hasPermission(Permission.CANCEL, booking)) {
			throw new BusinessException("exception.booking.cancel.denied");
		}
		return bookingLocalService.cancelBooking(booking);
	}

	public Booking deleteBooking(long bookingId) throws PortalException, SystemException {
		Booking booking = bookingLocalService.getBooking(bookingId);
		if (!BookingPermission.hasPermission(Permission.DELETE, booking)) {
			throw new BusinessException("exception.booking.delete.denied");
		}
		return bookingLocalService.deleteBooking(booking);
	}
	
	public Booking deleteBooking(long bookingId, String note) throws PortalException, SystemException {
		Booking booking = bookingLocalService.getBooking(bookingId);
		if (!BookingPermission.hasPermission(Permission.DELETE, booking)) {
			throw new BusinessException("exception.booking.delete.denied");
		}
		return bookingLocalService.deleteBooking(booking, note);
	}

}