package br.com.atilo.jcondo.booking;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import com.liferay.util.portlet.PortletProps;

import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;
import br.com.atilo.jcondo.manager.BusinessException;

public class HomeOfficeBookingRule implements RoomBookingRule {

	private static final int WEEK_LIMIT = Integer.parseInt(PortletProps.get("home.office.week.limit"));

	private static final int MONTH_LIMIT = Integer.parseInt(PortletProps.get("home.office.month.limit"));

	@Override
	public void validate(long personId, Date bookingDate) throws Exception {
		Calendar calendar = DateUtils.toCalendar(bookingDate);
		calendar.setFirstDayOfWeek(Calendar.MONTH);
		calendar.set(Calendar.DAY_OF_WEEK, calendar.getFirstDayOfWeek());

		Date beginDate = calendar.getTime();
		Date endDate = DateUtils.addDays(DateUtils.addWeeks(beginDate, 1), -1);
		
		if (BookingLocalServiceUtil.getPersonBookings(personId, beginDate, endDate).size() >= WEEK_LIMIT) {
			throw new BusinessException("exception.home.office.week.limit", WEEK_LIMIT);
		}

		beginDate = DateUtils.truncate(bookingDate, Calendar.MONTH);
		endDate = DateUtils.addDays(DateUtils.addMonths(beginDate, 1), -1);	

		if (BookingLocalServiceUtil.getPersonBookings(personId, beginDate, endDate).size() >= MONTH_LIMIT) {
			throw new BusinessException("exception.home.office.month.limit", MONTH_LIMIT);
		}

	}

}
