package br.com.atilo.jcondo.booking.bean;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.context.RequestContext;
import org.primefaces.event.TabChangeEvent;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil;
import br.com.atilo.jcondo.commons.collections.VehicleLicenseTransformer;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.service.AccessLocalServiceUtil;

@SessionScoped
@ManagedBean
public class BookingAccessBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(BookingAccessBean.class);
	
	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("Language");
	
	private List<Booking> bookings;
	
	private Date today;
	
	private String guestName;

	private String guestSurname;
	
	private Booking booking;

	private List<Guest> guests;
	
	private int bookingsIndex;

	public BookingAccessBean() {
	}

	@PostConstruct
	public void init() {
		try {
			today = new Date();
			bookings = BookingLocalServiceUtil.getBookings(today);

			if (CollectionUtils.isEmpty(bookings)) {
				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.no.bookings.today", null, ":booking-access-form:no-bookings");
			} else {
				bookingsIndex = 0;
				guests = bookings.get(bookingsIndex).getGuests();
			}

		} catch (Exception e) {
			LOGGER.fatal("Failure on room initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public void onGuestAdd() {
		try {
			GuestLocalServiceUtil.addGuest(booking.getId(), guestName, guestSurname);
			guestName = null;
			guestSurname = null;
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.guest.add", null);
		} catch (Exception e) {
			LOGGER.error("Failure on booking date select", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.guest.add", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onGuestDelete(Guest guest) {
		try {
			GuestLocalServiceUtil.deleteGuest(guest.getGuestId());
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.guest.delete", null);
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on roomBooking save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.guest.delete", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onGuestUpdate(Guest guest) {
		try {
			guest.setCheckedIn(!guest.getCheckedIn());
			GuestLocalServiceUtil.updateGuest(guest);
			guests = bookings.get(bookingsIndex).getGuests();
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.guest.update", null);
		} catch (Exception e) {
			LOGGER.error("Failure on booking guest add", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.guest.update", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}
	
	public void onBookingSelect(TabChangeEvent event) {
		try {
			bookingsIndex = ((AccordionPanel) event.getTab().getParent()).getIndex();
			guests = bookings.get(bookingsIndex).getGuests();
		} catch (Exception e) {
			LOGGER.error("Failure on booking select", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.select", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onBookingRefresh() {
		try {
			guests = bookings.get(bookingsIndex).getGuests();
		} catch (Exception e) {
			LOGGER.error("Failure on booking select", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.refresh", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}	

	public String displayBookingTitle(Booking booking) {
		try {
			Flat flat = (Flat) booking.getDomain();
			return booking.getRoom().getName().concat(" - ")
											  .concat(displayBookingFlat(flat))
											  .concat(" - das ")
											  .concat(DateFormatUtils.format(booking.getBeginDate(), "HH:mm"))
											  .concat(" às ")
											  .concat(DateFormatUtils.format(booking.getEndDate(), "HH:mm"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public String displayBookingFlat(Flat flat) {
		return MessageFormat.format(BUNDLE.getString("flat.and.block"),
									flat.getNumber(), flat.getBlock());
	}

	@SuppressWarnings("unchecked")
	public List<String> displayVehicles() {
		try {
			return (List<String>) CollectionUtils.collect(AccessLocalServiceUtil.getVehicleAccesses(), 
														  new VehicleLicenseTransformer());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public boolean isPrePaid(Booking booking) {
		try {
			BookingInvoice invoice = booking.getInvoice();
			return BookingInvoiceLocalServiceUtil.isPrePaidMode(invoice != null ? invoice.getId() : 0);
		} catch (Exception e) {
			LOGGER.error("Failure on booking pre paid mode checking", e);
		}

		return false;
	}
	
	public boolean isPaid(Booking booking) {
		try {
			return BookingLocalServiceUtil.isPaid(booking.getId());
		} catch (Exception e) {
			LOGGER.error("Failure on booking invoice payment check", e);
		}

		return false;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	public Date getToday() {
		return today;
	}

	public void setToday(Date today) {
		this.today = today;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getGuestSurname() {
		return guestSurname;
	}

	public void setGuestSurname(String guestSurname) {
		this.guestSurname = guestSurname;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	public List<Guest> getGuests() {
		return guests;
	}

	public void setGuests(List<Guest> guests) {
		this.guests = guests;
	}

}
