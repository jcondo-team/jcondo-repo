package br.com.atilo.jcondo.booking.service.http;

import br.com.atilo.jcondo.booking.service.GuestServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.booking.service.GuestServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.booking.model.GuestSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.booking.model.Guest}, that is translated to a
 * {@link br.com.atilo.jcondo.booking.model.GuestSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GuestServiceHttp
 * @see br.com.atilo.jcondo.booking.model.GuestSoap
 * @see br.com.atilo.jcondo.booking.service.GuestServiceUtil
 * @generated
 */
public class GuestServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(GuestServiceSoap.class);

    public static br.com.atilo.jcondo.booking.model.GuestSoap addGuest(
        long bookingId, java.lang.String name, java.lang.String surname)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Guest returnValue = GuestServiceUtil.addGuest(bookingId,
                    name, surname);

            return br.com.atilo.jcondo.booking.model.GuestSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.GuestSoap updateGuest(
        long guestId, long bookingId, java.lang.String name,
        java.lang.String surname, boolean checkedIn) throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Guest returnValue = GuestServiceUtil.updateGuest(guestId,
                    bookingId, name, surname, checkedIn);

            return br.com.atilo.jcondo.booking.model.GuestSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.GuestSoap deleteGuest(
        long guestId) throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Guest returnValue = GuestServiceUtil.deleteGuest(guestId);

            return br.com.atilo.jcondo.booking.model.GuestSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.GuestSoap[] getBookingGuests(
        long bookingId) throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.booking.model.Guest> returnValue = GuestServiceUtil.getBookingGuests(bookingId);

            return br.com.atilo.jcondo.booking.model.GuestSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
