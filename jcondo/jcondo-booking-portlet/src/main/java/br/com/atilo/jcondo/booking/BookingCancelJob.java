package br.com.atilo.jcondo.booking;

import java.util.Calendar;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.payment.PaymentProviderId;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;

import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

public class BookingCancelJob implements MessageListener {

	/** Days to wait for invoice discharge after the ovedue date */
	private static final int DELETE_DAYS = 2;

	private static final int SUSPEND_DAYS = 1;	

	public void receive(Message message) throws MessageListenerException {
		try {
			Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			ServiceContext sc = new ServiceContext();
			sc.setUserId(UserLocalServiceUtil.getDefaultUserId(PortalUtil.getDefaultCompanyId()));
			ServiceContextThreadLocal.pushServiceContext(sc);

			for (Booking booking : BookingLocalServiceUtil.getOverdueBookings()) {
				if (booking.getInvoice().getProviderId() == PaymentProviderId.DEFAULT.ordinal()) {
					continue;
				}

				if (today.after(DateUtils.addDays(booking.getInvoice().getDate(), DELETE_DAYS))) {
					booking.setOprUser(UserLocalServiceUtil.getDefaultUserId(PortalUtil.getDefaultCompanyId()));
					BookingLocalServiceUtil.deleteBooking(booking, "Portal: exclu�do automaticamente devido ao vencimento do boleto.");
				} else if (today.after(DateUtils.addDays(booking.getInvoice().getDate(), SUSPEND_DAYS))) {
					BookingLocalServiceUtil.suspendBooking(booking);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
