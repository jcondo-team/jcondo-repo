package br.com.atilo.jcondo.booking.payment;

import br.com.atilo.jcondo.booking.payment.boletofacil.BoletoFacilPaymentProvider;

public enum PaymentProviderId {

	DEFAULT(DefaultPaymentProvider.class),
	BOLETO_FACIL(BoletoFacilPaymentProvider.class);
	
	private Class<? extends PaymentProvider> providerClass;
	
	private PaymentProviderId(Class<? extends PaymentProvider> providerClass) {
		this.providerClass = providerClass;
	}

	public static PaymentProvider getPaymentProvider(long id) throws InstantiationException, IllegalAccessException {
		for (PaymentProviderId k : values()) {
			if (k.ordinal() == id) {
				return k.getProviderClass().newInstance();
			}
		}

		return null;
	}

	public Class<? extends PaymentProvider> getProviderClass() {
		return providerClass;
	}

}
