package br.com.atilo.jcondo.booking.payment;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.scheduler.SchedulerEngine;
import com.liferay.portal.kernel.scheduler.SchedulerEngineUtil;
import com.liferay.portal.kernel.scheduler.SchedulerException;
import com.liferay.portal.kernel.scheduler.StorageType;
import com.liferay.portal.kernel.scheduler.Trigger;
import com.liferay.portal.kernel.scheduler.TriggerFactoryUtil;
import com.liferay.portal.kernel.scheduler.TriggerType;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.booking.model.Booking;

public class InvoiceScheduler {
	
	private static final Logger LOGGER = Logger.getLogger(InvoiceScheduler.class);

	/** One hour in milisecconds */
	public static final int RETRY_INTERVAL = 120000; //3600000;

	public static final int RETRIES = 3;

	public static final String RETRY_COUNT = "RETRY_COUNT";

	public static final String BOOKING_ID = "BOOKING_ID";
	
	public static void schedule(Booking booking) throws SchedulerException {
		LOGGER.info("Add invoice scheduling");

		Message message = new Message();
		message.put(SchedulerEngine.MESSAGE_LISTENER_CLASS_NAME, InvoiceRetryJob.class.getName());
		message.put(SchedulerEngine.PORTLET_ID, ServiceContextThreadLocal.getServiceContext().getPortletId());
		message.put(SchedulerEngine.JOB_NAME, InvoiceRetryJob.class.getSimpleName().concat(".").concat(String.valueOf(booking.getId())));
		message.put(SchedulerEngine.GROUP_NAME, message.getString(SchedulerEngine.JOB_NAME));
		message.put(BOOKING_ID, booking.getId());
		message.put(RETRY_COUNT, 0);

		schedule(message);

		LOGGER.info("Add invoice scheduled");
	}

	public static void reschedule(Booking booking) throws SchedulerException {
		LOGGER.info("Add invoice rescheduling");

		Message message = SchedulerEngineUtil.getScheduledJob(InvoiceRetryJob.class.getSimpleName().concat(".").concat(String.valueOf(booking.getId())), 
															  InvoiceRetryJob.class.getSimpleName().concat(".").concat(String.valueOf(booking.getId())), 
															  StorageType.PERSISTED).getMessage();

		message.put(RETRY_COUNT, message.getInteger(RETRY_COUNT) + 1);
		message.remove(SchedulerEngine.JOB_STATE);

		schedule(message);

		LOGGER.info("Add invoice rescheduled");
	}

	private static void schedule(Message message) throws SchedulerException  {
		Trigger trigger = TriggerFactoryUtil.buildTrigger(TriggerType.SIMPLE,
														  message.getString(SchedulerEngine.JOB_NAME),
														  message.getString(SchedulerEngine.GROUP_NAME),
											    		  DateUtils.addMilliseconds(new Date(), RETRY_INTERVAL),
											    		  null, RETRY_INTERVAL);

		SchedulerEngineUtil.schedule(trigger, StorageType.PERSISTED, 
									 "Add Invoice Retry Routine", 
									 DestinationNames.SCHEDULER_DISPATCH, 
									 message, 0);
	}

}
