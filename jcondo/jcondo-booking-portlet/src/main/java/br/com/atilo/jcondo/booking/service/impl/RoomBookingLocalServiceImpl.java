/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.booking.model.RoomBooking;
import br.com.atilo.jcondo.booking.service.base.RoomBookingLocalServiceBaseImpl;

/**
 * The implementation of the room booking local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.RoomBookingLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.RoomBookingLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.RoomBookingLocalServiceUtil
 */
public class RoomBookingLocalServiceImpl extends RoomBookingLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.RoomBookingLocalServiceUtil} to access the room booking local service.
	 */

	public RoomBooking addRoomBooking(long roomId, int weekDay, int openHour, int closeHour, int period) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		RoomBooking roomBooking = createRoomBooking(counterLocalService.increment());
		roomBooking.setRoomId(roomId);
		roomBooking.setWeekDay(weekDay);
		roomBooking.setOpenHour(openHour);
		roomBooking.setCloseHour(closeHour);
		roomBooking.setPeriod(period);
		roomBooking.setOprDate(new Date());
		roomBooking.setOprUser(sc.getUserId());
		return addRoomBooking(roomBooking);
	}

	public RoomBooking updateRoomBooking(long roomBookingId, int openHour, int closeHour, int period) throws PortalException, SystemException {
		RoomBooking roomBooking = getRoomBooking(roomBookingId);
		roomBooking.setOpenHour(openHour);
		roomBooking.setCloseHour(closeHour);
		roomBooking.setPeriod(period == closeHour - openHour ? -1 : period);
		return updateRoomBooking(roomBooking);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public RoomBooking updateRoomBooking(RoomBooking roomBooking) throws SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		roomBooking.setOprDate(new Date());
		roomBooking.setOprUser(sc.getUserId());
		return super.updateRoomBooking(roomBooking);
	}
	
	public List<RoomBooking> getRoomBookings(long roomId, int weekDay) throws PortalException, SystemException {
		return new ArrayList<RoomBooking>(roomBookingPersistence.findByRoomAndWeekDay(roomId, weekDay));
	}

	public List<RoomBooking> getRoomBookings(long roomId) throws PortalException, SystemException {
		return new ArrayList<RoomBooking>(roomBookingPersistence.findByRoom(roomId));
	}

	public List<RoomBooking> getDefaultRoomBookings() {
		List<RoomBooking> roomBookings = new ArrayList<RoomBooking>();

		for (int i = 1; i < 7; i++) {
			RoomBooking roomBooking = createRoomBooking(0);
			roomBooking.setWeekDay(i);
			roomBooking.setOpenHour(10);
			roomBooking.setCloseHour(22);
			roomBooking.setPeriod(-1);
			roomBookings.add(roomBooking);
		}

		return roomBookings;
	}
	
	public List<Date> getUnavailableDates(long roomId, Date fromDate, Date toDate) throws PortalException, SystemException {
		List<Date> dates = new ArrayList<Date>();
		Date date = (Date) fromDate.clone();
		while (!date.after(toDate)) {
			for (RoomBooking roomBooking : getRoomBookings(roomId, DateUtils.toCalendar(date).get(Calendar.DAY_OF_WEEK))) {
				if (roomBooking.getCloseHour() - roomBooking.getOpenHour() <= 0) {
					dates.add(date);
				}
			}
			date = DateUtils.addDays(date, 1);
		}
		return dates;
	}
}