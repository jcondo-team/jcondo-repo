package br.com.atilo.jcondo.booking.model.listener;

import java.util.ResourceBundle;

import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.BookingInvoice;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.template.StringTemplateResource;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateManagerUtil;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.util.ContentUtil;

public class BookingInvoiceListener extends BaseModelListener<BookingInvoice> {

	private static final Logger LOGGER = Logger.getLogger(BookingInvoiceListener.class);

	private static final ResourceBundle rb = ResourceBundle.getBundle("application");
	
	private StringTemplateResource invoiceCreatedResource;
	
	public BookingInvoiceListener() {
		String mailBodyTemplate = ContentUtil.get("br/com/atilo/jcondo/booking/mail/prepaid/invoice-created-notify.vm");
		invoiceCreatedResource = new StringTemplateResource("BookingInvoiceCreated", mailBodyTemplate);
	}

	@Override
	public void onAfterCreate(BookingInvoice invoice) throws ModelListenerException {
		notifyBookingInvoiceCreate(invoice);
	}

	private void notifyBookingInvoiceCreate(BookingInvoice invoice) {
		try {
			LOGGER.info("Enviando notificacao de criacao do boleto " + invoice.getId());

			Booking booking = invoice.getBooking();

			Template template = TemplateManagerUtil.getTemplate("", invoiceCreatedResource, false);		
			template.put("booking", booking);
			template.put("date", DateFormatUtils.format(booking.getBeginDate(), "dd/MM/yyyy"));
			template.put("startTime", DateFormatUtils.format(booking.getBeginDate(), "HH:mm'h'"));
			template.put("endTime", DateFormatUtils.format(booking.getEndDate(), "HH:mm'h'"));

			UnsyncStringWriter writer = new UnsyncStringWriter();

			template.processTemplate(writer);

			String mailBody = writer.toString();

			LOGGER.debug(mailBody);

			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(rb.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(booking.getPerson().getEmail()));
			mailMessage.setSubject(rb.getString("booking.invoice.create.subject"));
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de criacao do boleto " + invoice.getId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de criacao do boleto " + invoice.getId());
		}
	}
}
