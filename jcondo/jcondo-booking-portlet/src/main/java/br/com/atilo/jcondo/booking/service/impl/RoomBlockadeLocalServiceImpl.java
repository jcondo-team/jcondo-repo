/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.time.DateUtils;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.booking.model.RoomBlockade;
import br.com.atilo.jcondo.booking.service.base.RoomBlockadeLocalServiceBaseImpl;

/**
 * The implementation of the room blockade local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.RoomBlockadeLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.RoomBlockadeLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.RoomBlockadeLocalServiceUtil
 */
public class RoomBlockadeLocalServiceImpl
	extends RoomBlockadeLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.RoomBlockadeLocalServiceUtil} to access the room blockade local service.
	 */
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public RoomBlockade addRoomBlockade(RoomBlockade roomBlockade) throws SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		roomBlockade.setId(counterLocalService.increment());
		roomBlockade.setOprDate(new Date());
		roomBlockade.setOprUser(sc.getUserId());
		return super.addRoomBlockade(roomBlockade);
	}

	@Override
	public RoomBlockade updateRoomBlockade(RoomBlockade roomBlockade) throws SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		roomBlockade.setOprDate(new Date());
		roomBlockade.setOprUser(sc.getUserId());
		return super.updateRoomBlockade(roomBlockade);
	}

	@Override
	public RoomBlockade deleteRoomBlockade(long id) throws PortalException, SystemException {
		return deleteRoomBlockade(getRoomBlockade(id));
	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public RoomBlockade deleteRoomBlockade(RoomBlockade roomBlockade) throws SystemException {
		return super.deleteRoomBlockade(updateRoomBlockade(roomBlockade));
	}

	public List<RoomBlockade> getRoomBlockades(long roomId) throws PortalException, SystemException {
		return roomBlockadePersistence.findByRoom(roomId);
	}

	@SuppressWarnings("unchecked")
	public List<Date> getBlockedDates(long roomId, Date fromDate, Date toDate) throws PortalException, SystemException {
		Set<Date> dates = new HashSet<Date>();
		Date beginDate = DateUtils.truncate(fromDate, Calendar.DAY_OF_MONTH);
		Date endDate = DateUtils.truncate(toDate, Calendar.DAY_OF_MONTH);

		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.eq("roomId", roomId),
										   RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.eq("recursive", false),
												   					   RestrictionsFactoryUtil.or(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ge("beginDate", beginDate), 
												   																			  RestrictionsFactoryUtil.le("beginDate", endDate)),
												   												  RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ge("endDate", beginDate), 
												   														  					  RestrictionsFactoryUtil.le("endDate", endDate))))));

		List<RoomBlockade> blockades = roomBlockadePersistence.findWithDynamicQuery(dq);
		for (RoomBlockade blockade : blockades) {
			Date beginDt = blockade.getBeginDate();
			while (!beginDt.after(blockade.getEndDate())) {
				if (!beginDt.before(beginDate) && !beginDt.after(endDate)) {
					dates.add(beginDt);
				}
				beginDt = DateUtils.addDays(beginDt, 1);
			}
		}

		blockades = roomBlockadePersistence.findByRecursive(roomId, true);

		int beginYear = DateUtils.toCalendar(beginDate).get(Calendar.YEAR);

		Date beginDt;
		Date endDt;

		for (RoomBlockade blockade : blockades) {
			if (beginDate.after(blockade.getEndDate())) {
				beginDt = DateUtils.setYears(blockade.getBeginDate(), beginYear);
				int diff = DateUtils.toCalendar(blockade.getEndDate()).get(Calendar.YEAR) - DateUtils.toCalendar(blockade.getBeginDate()).get(Calendar.YEAR);
				endDt = DateUtils.setYears(blockade.getEndDate(), beginYear + diff);
			} else {
				beginDt = blockade.getBeginDate();
				endDt = blockade.getEndDate();
			}

			while (!beginDt.after(endDt)) {
				if (!beginDt.before(beginDate) && !beginDt.after(endDate)) {
					dates.add(beginDt);
				}
				beginDt = DateUtils.addDays(beginDt, 1);
			}
		}

		return new ArrayList<Date>(dates);
	}

}