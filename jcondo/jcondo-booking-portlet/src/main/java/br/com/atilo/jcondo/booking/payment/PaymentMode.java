package br.com.atilo.jcondo.booking.payment;

public enum PaymentMode {

	PRE_PAID,
	POST_PAID;

}
