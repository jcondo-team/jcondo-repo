package br.com.atilo.jcondo.booking.payment;

import br.com.atilo.jcondo.manager.BusinessException;

public class PaymentProviderException extends BusinessException {

	private static final long serialVersionUID = 1L;

	public PaymentProviderException(String message, Object ... args) {
		super(message, args);
	}

}
