package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.NoSuchPaymentException;
import br.com.atilo.jcondo.booking.model.Payment;
import br.com.atilo.jcondo.booking.model.impl.PaymentImpl;
import br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl;
import br.com.atilo.jcondo.booking.service.persistence.PaymentPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the payment service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see PaymentPersistence
 * @see PaymentUtil
 * @generated
 */
public class PaymentPersistenceImpl extends BasePersistenceImpl<Payment>
    implements PaymentPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link PaymentUtil} to access the payment persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = PaymentImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentModelImpl.FINDER_CACHE_ENABLED, PaymentImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentModelImpl.FINDER_CACHE_ENABLED, PaymentImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_INVOICE = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentModelImpl.FINDER_CACHE_ENABLED, PaymentImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByInvoice",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INVOICE =
        new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentModelImpl.FINDER_CACHE_ENABLED, PaymentImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByInvoice",
            new String[] { Long.class.getName() },
            PaymentModelImpl.INVOICEID_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_INVOICE = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByInvoice",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_INVOICE_INVOICEID_2 = "payment.invoiceId = ?";
    public static final FinderPath FINDER_PATH_FETCH_BY_TOKEN = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentModelImpl.FINDER_CACHE_ENABLED, PaymentImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByToken",
            new String[] { String.class.getName() },
            PaymentModelImpl.TOKEN_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_TOKEN = new FinderPath(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByToken",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_TOKEN_TOKEN_1 = "payment.token IS NULL";
    private static final String _FINDER_COLUMN_TOKEN_TOKEN_2 = "payment.token = ?";
    private static final String _FINDER_COLUMN_TOKEN_TOKEN_3 = "(payment.token IS NULL OR payment.token = '')";
    private static final String _SQL_SELECT_PAYMENT = "SELECT payment FROM Payment payment";
    private static final String _SQL_SELECT_PAYMENT_WHERE = "SELECT payment FROM Payment payment WHERE ";
    private static final String _SQL_COUNT_PAYMENT = "SELECT COUNT(payment) FROM Payment payment";
    private static final String _SQL_COUNT_PAYMENT_WHERE = "SELECT COUNT(payment) FROM Payment payment WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "payment.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Payment exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Payment exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(PaymentPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Payment _nullPayment = new PaymentImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Payment> toCacheModel() {
                return _nullPaymentCacheModel;
            }
        };

    private static CacheModel<Payment> _nullPaymentCacheModel = new CacheModel<Payment>() {
            @Override
            public Payment toEntityModel() {
                return _nullPayment;
            }
        };

    public PaymentPersistenceImpl() {
        setModelClass(Payment.class);
    }

    /**
     * Returns all the payments where invoiceId = &#63;.
     *
     * @param invoiceId the invoice ID
     * @return the matching payments
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Payment> findByInvoice(long invoiceId)
        throws SystemException {
        return findByInvoice(invoiceId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the payments where invoiceId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param invoiceId the invoice ID
     * @param start the lower bound of the range of payments
     * @param end the upper bound of the range of payments (not inclusive)
     * @return the range of matching payments
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Payment> findByInvoice(long invoiceId, int start, int end)
        throws SystemException {
        return findByInvoice(invoiceId, start, end, null);
    }

    /**
     * Returns an ordered range of all the payments where invoiceId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param invoiceId the invoice ID
     * @param start the lower bound of the range of payments
     * @param end the upper bound of the range of payments (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching payments
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Payment> findByInvoice(long invoiceId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INVOICE;
            finderArgs = new Object[] { invoiceId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_INVOICE;
            finderArgs = new Object[] { invoiceId, start, end, orderByComparator };
        }

        List<Payment> list = (List<Payment>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Payment payment : list) {
                if ((invoiceId != payment.getInvoiceId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_PAYMENT_WHERE);

            query.append(_FINDER_COLUMN_INVOICE_INVOICEID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(PaymentModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(invoiceId);

                if (!pagination) {
                    list = (List<Payment>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Payment>(list);
                } else {
                    list = (List<Payment>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first payment in the ordered set where invoiceId = &#63;.
     *
     * @param invoiceId the invoice ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching payment
     * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a matching payment could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment findByInvoice_First(long invoiceId,
        OrderByComparator orderByComparator)
        throws NoSuchPaymentException, SystemException {
        Payment payment = fetchByInvoice_First(invoiceId, orderByComparator);

        if (payment != null) {
            return payment;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("invoiceId=");
        msg.append(invoiceId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPaymentException(msg.toString());
    }

    /**
     * Returns the first payment in the ordered set where invoiceId = &#63;.
     *
     * @param invoiceId the invoice ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching payment, or <code>null</code> if a matching payment could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment fetchByInvoice_First(long invoiceId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Payment> list = findByInvoice(invoiceId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last payment in the ordered set where invoiceId = &#63;.
     *
     * @param invoiceId the invoice ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching payment
     * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a matching payment could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment findByInvoice_Last(long invoiceId,
        OrderByComparator orderByComparator)
        throws NoSuchPaymentException, SystemException {
        Payment payment = fetchByInvoice_Last(invoiceId, orderByComparator);

        if (payment != null) {
            return payment;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("invoiceId=");
        msg.append(invoiceId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchPaymentException(msg.toString());
    }

    /**
     * Returns the last payment in the ordered set where invoiceId = &#63;.
     *
     * @param invoiceId the invoice ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching payment, or <code>null</code> if a matching payment could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment fetchByInvoice_Last(long invoiceId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByInvoice(invoiceId);

        if (count == 0) {
            return null;
        }

        List<Payment> list = findByInvoice(invoiceId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the payments before and after the current payment in the ordered set where invoiceId = &#63;.
     *
     * @param id the primary key of the current payment
     * @param invoiceId the invoice ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next payment
     * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a payment with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment[] findByInvoice_PrevAndNext(long id, long invoiceId,
        OrderByComparator orderByComparator)
        throws NoSuchPaymentException, SystemException {
        Payment payment = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Payment[] array = new PaymentImpl[3];

            array[0] = getByInvoice_PrevAndNext(session, payment, invoiceId,
                    orderByComparator, true);

            array[1] = payment;

            array[2] = getByInvoice_PrevAndNext(session, payment, invoiceId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Payment getByInvoice_PrevAndNext(Session session,
        Payment payment, long invoiceId, OrderByComparator orderByComparator,
        boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_PAYMENT_WHERE);

        query.append(_FINDER_COLUMN_INVOICE_INVOICEID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(PaymentModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(invoiceId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(payment);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Payment> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the payments where invoiceId = &#63; from the database.
     *
     * @param invoiceId the invoice ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByInvoice(long invoiceId) throws SystemException {
        for (Payment payment : findByInvoice(invoiceId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(payment);
        }
    }

    /**
     * Returns the number of payments where invoiceId = &#63;.
     *
     * @param invoiceId the invoice ID
     * @return the number of matching payments
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByInvoice(long invoiceId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_INVOICE;

        Object[] finderArgs = new Object[] { invoiceId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PAYMENT_WHERE);

            query.append(_FINDER_COLUMN_INVOICE_INVOICEID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(invoiceId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns the payment where token = &#63; or throws a {@link br.com.atilo.jcondo.booking.NoSuchPaymentException} if it could not be found.
     *
     * @param token the token
     * @return the matching payment
     * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a matching payment could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment findByToken(String token)
        throws NoSuchPaymentException, SystemException {
        Payment payment = fetchByToken(token);

        if (payment == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("token=");
            msg.append(token);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchPaymentException(msg.toString());
        }

        return payment;
    }

    /**
     * Returns the payment where token = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param token the token
     * @return the matching payment, or <code>null</code> if a matching payment could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment fetchByToken(String token) throws SystemException {
        return fetchByToken(token, true);
    }

    /**
     * Returns the payment where token = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param token the token
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching payment, or <code>null</code> if a matching payment could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment fetchByToken(String token, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { token };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_TOKEN,
                    finderArgs, this);
        }

        if (result instanceof Payment) {
            Payment payment = (Payment) result;

            if (!Validator.equals(token, payment.getToken())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_PAYMENT_WHERE);

            boolean bindToken = false;

            if (token == null) {
                query.append(_FINDER_COLUMN_TOKEN_TOKEN_1);
            } else if (token.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TOKEN_TOKEN_3);
            } else {
                bindToken = true;

                query.append(_FINDER_COLUMN_TOKEN_TOKEN_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindToken) {
                    qPos.add(token);
                }

                List<Payment> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TOKEN,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "PaymentPersistenceImpl.fetchByToken(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Payment payment = list.get(0);

                    result = payment;

                    cacheResult(payment);

                    if ((payment.getToken() == null) ||
                            !payment.getToken().equals(token)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TOKEN,
                            finderArgs, payment);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TOKEN,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Payment) result;
        }
    }

    /**
     * Removes the payment where token = &#63; from the database.
     *
     * @param token the token
     * @return the payment that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment removeByToken(String token)
        throws NoSuchPaymentException, SystemException {
        Payment payment = findByToken(token);

        return remove(payment);
    }

    /**
     * Returns the number of payments where token = &#63;.
     *
     * @param token the token
     * @return the number of matching payments
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByToken(String token) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_TOKEN;

        Object[] finderArgs = new Object[] { token };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_PAYMENT_WHERE);

            boolean bindToken = false;

            if (token == null) {
                query.append(_FINDER_COLUMN_TOKEN_TOKEN_1);
            } else if (token.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_TOKEN_TOKEN_3);
            } else {
                bindToken = true;

                query.append(_FINDER_COLUMN_TOKEN_TOKEN_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindToken) {
                    qPos.add(token);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the payment in the entity cache if it is enabled.
     *
     * @param payment the payment
     */
    @Override
    public void cacheResult(Payment payment) {
        EntityCacheUtil.putResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentImpl.class, payment.getPrimaryKey(), payment);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TOKEN,
            new Object[] { payment.getToken() }, payment);

        payment.resetOriginalValues();
    }

    /**
     * Caches the payments in the entity cache if it is enabled.
     *
     * @param payments the payments
     */
    @Override
    public void cacheResult(List<Payment> payments) {
        for (Payment payment : payments) {
            if (EntityCacheUtil.getResult(
                        PaymentModelImpl.ENTITY_CACHE_ENABLED,
                        PaymentImpl.class, payment.getPrimaryKey()) == null) {
                cacheResult(payment);
            } else {
                payment.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all payments.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(PaymentImpl.class.getName());
        }

        EntityCacheUtil.clearCache(PaymentImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the payment.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Payment payment) {
        EntityCacheUtil.removeResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentImpl.class, payment.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(payment);
    }

    @Override
    public void clearCache(List<Payment> payments) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Payment payment : payments) {
            EntityCacheUtil.removeResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
                PaymentImpl.class, payment.getPrimaryKey());

            clearUniqueFindersCache(payment);
        }
    }

    protected void cacheUniqueFindersCache(Payment payment) {
        if (payment.isNew()) {
            Object[] args = new Object[] { payment.getToken() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TOKEN, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TOKEN, args, payment);
        } else {
            PaymentModelImpl paymentModelImpl = (PaymentModelImpl) payment;

            if ((paymentModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_TOKEN.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { payment.getToken() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_TOKEN, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_TOKEN, args,
                    payment);
            }
        }
    }

    protected void clearUniqueFindersCache(Payment payment) {
        PaymentModelImpl paymentModelImpl = (PaymentModelImpl) payment;

        Object[] args = new Object[] { payment.getToken() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TOKEN, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TOKEN, args);

        if ((paymentModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_TOKEN.getColumnBitmask()) != 0) {
            args = new Object[] { paymentModelImpl.getOriginalToken() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_TOKEN, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_TOKEN, args);
        }
    }

    /**
     * Creates a new payment with the primary key. Does not add the payment to the database.
     *
     * @param id the primary key for the new payment
     * @return the new payment
     */
    @Override
    public Payment create(long id) {
        Payment payment = new PaymentImpl();

        payment.setNew(true);
        payment.setPrimaryKey(id);

        return payment;
    }

    /**
     * Removes the payment with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the payment
     * @return the payment that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a payment with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment remove(long id)
        throws NoSuchPaymentException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the payment with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the payment
     * @return the payment that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a payment with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment remove(Serializable primaryKey)
        throws NoSuchPaymentException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Payment payment = (Payment) session.get(PaymentImpl.class,
                    primaryKey);

            if (payment == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchPaymentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(payment);
        } catch (NoSuchPaymentException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Payment removeImpl(Payment payment) throws SystemException {
        payment = toUnwrappedModel(payment);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(payment)) {
                payment = (Payment) session.get(PaymentImpl.class,
                        payment.getPrimaryKeyObj());
            }

            if (payment != null) {
                session.delete(payment);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (payment != null) {
            clearCache(payment);
        }

        return payment;
    }

    @Override
    public Payment updateImpl(br.com.atilo.jcondo.booking.model.Payment payment)
        throws SystemException {
        payment = toUnwrappedModel(payment);

        boolean isNew = payment.isNew();

        PaymentModelImpl paymentModelImpl = (PaymentModelImpl) payment;

        Session session = null;

        try {
            session = openSession();

            if (payment.isNew()) {
                session.save(payment);

                payment.setNew(false);
            } else {
                session.merge(payment);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !PaymentModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((paymentModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INVOICE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        paymentModelImpl.getOriginalInvoiceId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_INVOICE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INVOICE,
                    args);

                args = new Object[] { paymentModelImpl.getInvoiceId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_INVOICE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_INVOICE,
                    args);
            }
        }

        EntityCacheUtil.putResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
            PaymentImpl.class, payment.getPrimaryKey(), payment);

        clearUniqueFindersCache(payment);
        cacheUniqueFindersCache(payment);

        return payment;
    }

    protected Payment toUnwrappedModel(Payment payment) {
        if (payment instanceof PaymentImpl) {
            return payment;
        }

        PaymentImpl paymentImpl = new PaymentImpl();

        paymentImpl.setNew(payment.isNew());
        paymentImpl.setPrimaryKey(payment.getPrimaryKey());

        paymentImpl.setId(payment.getId());
        paymentImpl.setInvoiceId(payment.getInvoiceId());
        paymentImpl.setToken(payment.getToken());
        paymentImpl.setAmount(payment.getAmount());
        paymentImpl.setDate(payment.getDate());
        paymentImpl.setChecked(payment.isChecked());
        paymentImpl.setOprDate(payment.getOprDate());
        paymentImpl.setOprUser(payment.getOprUser());

        return paymentImpl;
    }

    /**
     * Returns the payment with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the payment
     * @return the payment
     * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a payment with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment findByPrimaryKey(Serializable primaryKey)
        throws NoSuchPaymentException, SystemException {
        Payment payment = fetchByPrimaryKey(primaryKey);

        if (payment == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchPaymentException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return payment;
    }

    /**
     * Returns the payment with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchPaymentException} if it could not be found.
     *
     * @param id the primary key of the payment
     * @return the payment
     * @throws br.com.atilo.jcondo.booking.NoSuchPaymentException if a payment with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment findByPrimaryKey(long id)
        throws NoSuchPaymentException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the payment with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the payment
     * @return the payment, or <code>null</code> if a payment with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Payment payment = (Payment) EntityCacheUtil.getResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
                PaymentImpl.class, primaryKey);

        if (payment == _nullPayment) {
            return null;
        }

        if (payment == null) {
            Session session = null;

            try {
                session = openSession();

                payment = (Payment) session.get(PaymentImpl.class, primaryKey);

                if (payment != null) {
                    cacheResult(payment);
                } else {
                    EntityCacheUtil.putResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
                        PaymentImpl.class, primaryKey, _nullPayment);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(PaymentModelImpl.ENTITY_CACHE_ENABLED,
                    PaymentImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return payment;
    }

    /**
     * Returns the payment with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the payment
     * @return the payment, or <code>null</code> if a payment with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Payment fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the payments.
     *
     * @return the payments
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Payment> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the payments.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of payments
     * @param end the upper bound of the range of payments (not inclusive)
     * @return the range of payments
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Payment> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the payments.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.PaymentModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of payments
     * @param end the upper bound of the range of payments (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of payments
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Payment> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Payment> list = (List<Payment>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_PAYMENT);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_PAYMENT;

                if (pagination) {
                    sql = sql.concat(PaymentModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Payment>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Payment>(list);
                } else {
                    list = (List<Payment>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the payments from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Payment payment : findAll()) {
            remove(payment);
        }
    }

    /**
     * Returns the number of payments.
     *
     * @return the number of payments
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_PAYMENT);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the payment persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.booking.model.Payment")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Payment>> listenersList = new ArrayList<ModelListener<Payment>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Payment>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(PaymentImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
