package br.com.atilo.jcondo.booking.service.base;

import br.com.atilo.jcondo.booking.service.RoomBookingServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class RoomBookingServiceClpInvoker {
    private String _methodName62;
    private String[] _methodParameterTypes62;
    private String _methodName63;
    private String[] _methodParameterTypes63;
    private String _methodName68;
    private String[] _methodParameterTypes68;
    private String _methodName69;
    private String[] _methodParameterTypes69;
    private String _methodName70;
    private String[] _methodParameterTypes70;
    private String _methodName71;
    private String[] _methodParameterTypes71;
    private String _methodName72;
    private String[] _methodParameterTypes72;

    public RoomBookingServiceClpInvoker() {
        _methodName62 = "getBeanIdentifier";

        _methodParameterTypes62 = new String[] {  };

        _methodName63 = "setBeanIdentifier";

        _methodParameterTypes63 = new String[] { "java.lang.String" };

        _methodName68 = "addRoomBooking";

        _methodParameterTypes68 = new String[] {
                "long", "int", "int", "int", "int"
            };

        _methodName69 = "updateRoomBooking";

        _methodParameterTypes69 = new String[] { "long", "int", "int", "int" };

        _methodName70 = "updateRoomBooking";

        _methodParameterTypes70 = new String[] {
                "br.com.atilo.jcondo.booking.model.RoomBooking"
            };

        _methodName71 = "getRoomBookings";

        _methodParameterTypes71 = new String[] { "long", "int" };

        _methodName72 = "getRoomBookings";

        _methodParameterTypes72 = new String[] { "long" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName62.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes62, parameterTypes)) {
            return RoomBookingServiceUtil.getBeanIdentifier();
        }

        if (_methodName63.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes63, parameterTypes)) {
            RoomBookingServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName68.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes68, parameterTypes)) {
            return RoomBookingServiceUtil.addRoomBooking(((Long) arguments[0]).longValue(),
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                ((Integer) arguments[3]).intValue(),
                ((Integer) arguments[4]).intValue());
        }

        if (_methodName69.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes69, parameterTypes)) {
            return RoomBookingServiceUtil.updateRoomBooking(((Long) arguments[0]).longValue(),
                ((Integer) arguments[1]).intValue(),
                ((Integer) arguments[2]).intValue(),
                ((Integer) arguments[3]).intValue());
        }

        if (_methodName70.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes70, parameterTypes)) {
            return RoomBookingServiceUtil.updateRoomBooking((br.com.atilo.jcondo.booking.model.RoomBooking) arguments[0]);
        }

        if (_methodName71.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes71, parameterTypes)) {
            return RoomBookingServiceUtil.getRoomBookings(((Long) arguments[0]).longValue(),
                ((Integer) arguments[1]).intValue());
        }

        if (_methodName72.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes72, parameterTypes)) {
            return RoomBookingServiceUtil.getRoomBookings(((Long) arguments[0]).longValue());
        }

        throw new UnsupportedOperationException();
    }
}
