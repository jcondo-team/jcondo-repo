/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.WordUtils;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;

import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.service.base.GuestLocalServiceBaseImpl;

/**
 * The implementation of the guest local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.GuestLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.GuestLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil
 */
public class GuestLocalServiceImpl extends GuestLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil} to access the guest local service.
	 */

	public Guest addGuest(long bookingId, String name, String surname) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Guest guest = createGuest(counterLocalService.increment());
		guest.setBookingId(bookingId);
		guest.setName(WordUtils.capitalizeFully(name.trim()));
		guest.setSurname(WordUtils.capitalizeFully(surname.trim()));
		guest.setOprDate(new Date());
		guest.setOprUser(sc.getUserId());
		return super.addGuest(guest);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Guest addGuest(Guest guest) throws SystemException {
		try {
			return addGuest(guest.getBookingId(), guest.getName(), guest.getSurname());
		} catch (PortalException e) {
			throw new SystemException(e);
		}
	}

	public Guest updateGuest(long guestId, long bookingId, String name, String surname, boolean checkedIn) throws PortalException, SystemException {
		Guest guest = getGuest(guestId);
		guest.setBookingId(bookingId);
		guest.setName(WordUtils.capitalizeFully(name.trim()));
		guest.setSurname(WordUtils.capitalizeFully(surname.trim()));
		return updateGuest(guest);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Guest updateGuest(Guest guest) throws SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		guest.setOprDate(new Date());
		guest.setOprUser(sc.getUserId());
		return super.updateGuest(guest);
	}
	
	public List<Guest> getBookingGuests(long bookingId) throws PortalException, SystemException {
		return guestPersistence.findByBooking(bookingId);
	}

}