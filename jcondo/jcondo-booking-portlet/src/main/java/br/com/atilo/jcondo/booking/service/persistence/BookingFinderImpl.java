package br.com.atilo.jcondo.booking.service.persistence;

import java.util.List;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.impl.BookingImpl;

import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.SQLQuery;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;
import com.liferay.util.dao.orm.CustomSQLUtil;

public class BookingFinderImpl extends BasePersistenceImpl<Booking> implements BookingFinder {

	public static final String FIND_OVERDUE_BOOKINGS = BookingFinder.class.getName() + ".findOverdueBookings";	

	@SuppressWarnings("unchecked")
	public List<Booking> findOverdueBookings() {
		Session session = null;

		try {
			session = openSession();

			String sql = CustomSQLUtil.get(FIND_OVERDUE_BOOKINGS);

			SQLQuery q = session.createSQLQuery(sql);
			q.setCacheable(false);
			q.addEntity("jco_booking", BookingImpl.class);

			return (List<Booking>) QueryUtil.list(q, getDialect(), -1, -1);
		} catch (Exception e) {
			try {
				throw new SystemException(e);
			} catch (SystemException se) {
				se.printStackTrace();
			}
		} finally {
			closeSession(session);
		}

		return null;
	}
}
