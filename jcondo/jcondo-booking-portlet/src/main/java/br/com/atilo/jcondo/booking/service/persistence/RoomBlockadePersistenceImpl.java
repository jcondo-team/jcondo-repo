package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException;
import br.com.atilo.jcondo.booking.model.RoomBlockade;
import br.com.atilo.jcondo.booking.model.impl.RoomBlockadeImpl;
import br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl;
import br.com.atilo.jcondo.booking.service.persistence.RoomBlockadePersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the room blockade service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomBlockadePersistence
 * @see RoomBlockadeUtil
 * @generated
 */
public class RoomBlockadePersistenceImpl extends BasePersistenceImpl<RoomBlockade>
    implements RoomBlockadePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link RoomBlockadeUtil} to access the room blockade persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = RoomBlockadeImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeModelImpl.FINDER_CACHE_ENABLED, RoomBlockadeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeModelImpl.FINDER_CACHE_ENABLED, RoomBlockadeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_ROOM = new FinderPath(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeModelImpl.FINDER_CACHE_ENABLED, RoomBlockadeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByRoom",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM = new FinderPath(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeModelImpl.FINDER_CACHE_ENABLED, RoomBlockadeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByRoom",
            new String[] { Long.class.getName() },
            RoomBlockadeModelImpl.ROOMID_COLUMN_BITMASK |
            RoomBlockadeModelImpl.BEGINDATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_ROOM = new FinderPath(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByRoom",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_ROOM_ROOMID_2 = "roomBlockade.roomId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_RECURSIVE =
        new FinderPath(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeModelImpl.FINDER_CACHE_ENABLED, RoomBlockadeImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByRecursive",
            new String[] {
                Long.class.getName(), Boolean.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RECURSIVE =
        new FinderPath(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeModelImpl.FINDER_CACHE_ENABLED, RoomBlockadeImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByRecursive",
            new String[] { Long.class.getName(), Boolean.class.getName() },
            RoomBlockadeModelImpl.ROOMID_COLUMN_BITMASK |
            RoomBlockadeModelImpl.RECURSIVE_COLUMN_BITMASK |
            RoomBlockadeModelImpl.BEGINDATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_RECURSIVE = new FinderPath(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByRecursive",
            new String[] { Long.class.getName(), Boolean.class.getName() });
    private static final String _FINDER_COLUMN_RECURSIVE_ROOMID_2 = "roomBlockade.roomId = ? AND ";
    private static final String _FINDER_COLUMN_RECURSIVE_RECURSIVE_2 = "roomBlockade.recursive = ?";
    private static final String _SQL_SELECT_ROOMBLOCKADE = "SELECT roomBlockade FROM RoomBlockade roomBlockade";
    private static final String _SQL_SELECT_ROOMBLOCKADE_WHERE = "SELECT roomBlockade FROM RoomBlockade roomBlockade WHERE ";
    private static final String _SQL_COUNT_ROOMBLOCKADE = "SELECT COUNT(roomBlockade) FROM RoomBlockade roomBlockade";
    private static final String _SQL_COUNT_ROOMBLOCKADE_WHERE = "SELECT COUNT(roomBlockade) FROM RoomBlockade roomBlockade WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "roomBlockade.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No RoomBlockade exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No RoomBlockade exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(RoomBlockadePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static RoomBlockade _nullRoomBlockade = new RoomBlockadeImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<RoomBlockade> toCacheModel() {
                return _nullRoomBlockadeCacheModel;
            }
        };

    private static CacheModel<RoomBlockade> _nullRoomBlockadeCacheModel = new CacheModel<RoomBlockade>() {
            @Override
            public RoomBlockade toEntityModel() {
                return _nullRoomBlockade;
            }
        };

    public RoomBlockadePersistenceImpl() {
        setModelClass(RoomBlockade.class);
    }

    /**
     * Returns all the room blockades where roomId = &#63;.
     *
     * @param roomId the room ID
     * @return the matching room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<RoomBlockade> findByRoom(long roomId) throws SystemException {
        return findByRoom(roomId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the room blockades where roomId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param roomId the room ID
     * @param start the lower bound of the range of room blockades
     * @param end the upper bound of the range of room blockades (not inclusive)
     * @return the range of matching room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<RoomBlockade> findByRoom(long roomId, int start, int end)
        throws SystemException {
        return findByRoom(roomId, start, end, null);
    }

    /**
     * Returns an ordered range of all the room blockades where roomId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param roomId the room ID
     * @param start the lower bound of the range of room blockades
     * @param end the upper bound of the range of room blockades (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<RoomBlockade> findByRoom(long roomId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM;
            finderArgs = new Object[] { roomId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_ROOM;
            finderArgs = new Object[] { roomId, start, end, orderByComparator };
        }

        List<RoomBlockade> list = (List<RoomBlockade>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (RoomBlockade roomBlockade : list) {
                if ((roomId != roomBlockade.getRoomId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_ROOMBLOCKADE_WHERE);

            query.append(_FINDER_COLUMN_ROOM_ROOMID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(RoomBlockadeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(roomId);

                if (!pagination) {
                    list = (List<RoomBlockade>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<RoomBlockade>(list);
                } else {
                    list = (List<RoomBlockade>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first room blockade in the ordered set where roomId = &#63;.
     *
     * @param roomId the room ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room blockade
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade findByRoom_First(long roomId,
        OrderByComparator orderByComparator)
        throws NoSuchRoomBlockadeException, SystemException {
        RoomBlockade roomBlockade = fetchByRoom_First(roomId, orderByComparator);

        if (roomBlockade != null) {
            return roomBlockade;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("roomId=");
        msg.append(roomId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomBlockadeException(msg.toString());
    }

    /**
     * Returns the first room blockade in the ordered set where roomId = &#63;.
     *
     * @param roomId the room ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room blockade, or <code>null</code> if a matching room blockade could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade fetchByRoom_First(long roomId,
        OrderByComparator orderByComparator) throws SystemException {
        List<RoomBlockade> list = findByRoom(roomId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last room blockade in the ordered set where roomId = &#63;.
     *
     * @param roomId the room ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room blockade
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade findByRoom_Last(long roomId,
        OrderByComparator orderByComparator)
        throws NoSuchRoomBlockadeException, SystemException {
        RoomBlockade roomBlockade = fetchByRoom_Last(roomId, orderByComparator);

        if (roomBlockade != null) {
            return roomBlockade;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("roomId=");
        msg.append(roomId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomBlockadeException(msg.toString());
    }

    /**
     * Returns the last room blockade in the ordered set where roomId = &#63;.
     *
     * @param roomId the room ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room blockade, or <code>null</code> if a matching room blockade could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade fetchByRoom_Last(long roomId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByRoom(roomId);

        if (count == 0) {
            return null;
        }

        List<RoomBlockade> list = findByRoom(roomId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the room blockades before and after the current room blockade in the ordered set where roomId = &#63;.
     *
     * @param id the primary key of the current room blockade
     * @param roomId the room ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next room blockade
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade[] findByRoom_PrevAndNext(long id, long roomId,
        OrderByComparator orderByComparator)
        throws NoSuchRoomBlockadeException, SystemException {
        RoomBlockade roomBlockade = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            RoomBlockade[] array = new RoomBlockadeImpl[3];

            array[0] = getByRoom_PrevAndNext(session, roomBlockade, roomId,
                    orderByComparator, true);

            array[1] = roomBlockade;

            array[2] = getByRoom_PrevAndNext(session, roomBlockade, roomId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected RoomBlockade getByRoom_PrevAndNext(Session session,
        RoomBlockade roomBlockade, long roomId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ROOMBLOCKADE_WHERE);

        query.append(_FINDER_COLUMN_ROOM_ROOMID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(RoomBlockadeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(roomId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(roomBlockade);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<RoomBlockade> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the room blockades where roomId = &#63; from the database.
     *
     * @param roomId the room ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByRoom(long roomId) throws SystemException {
        for (RoomBlockade roomBlockade : findByRoom(roomId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(roomBlockade);
        }
    }

    /**
     * Returns the number of room blockades where roomId = &#63;.
     *
     * @param roomId the room ID
     * @return the number of matching room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByRoom(long roomId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_ROOM;

        Object[] finderArgs = new Object[] { roomId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ROOMBLOCKADE_WHERE);

            query.append(_FINDER_COLUMN_ROOM_ROOMID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(roomId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the room blockades where roomId = &#63; and recursive = &#63;.
     *
     * @param roomId the room ID
     * @param recursive the recursive
     * @return the matching room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<RoomBlockade> findByRecursive(long roomId, boolean recursive)
        throws SystemException {
        return findByRecursive(roomId, recursive, QueryUtil.ALL_POS,
            QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the room blockades where roomId = &#63; and recursive = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param roomId the room ID
     * @param recursive the recursive
     * @param start the lower bound of the range of room blockades
     * @param end the upper bound of the range of room blockades (not inclusive)
     * @return the range of matching room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<RoomBlockade> findByRecursive(long roomId, boolean recursive,
        int start, int end) throws SystemException {
        return findByRecursive(roomId, recursive, start, end, null);
    }

    /**
     * Returns an ordered range of all the room blockades where roomId = &#63; and recursive = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param roomId the room ID
     * @param recursive the recursive
     * @param start the lower bound of the range of room blockades
     * @param end the upper bound of the range of room blockades (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<RoomBlockade> findByRecursive(long roomId, boolean recursive,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RECURSIVE;
            finderArgs = new Object[] { roomId, recursive };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_RECURSIVE;
            finderArgs = new Object[] {
                    roomId, recursive,
                    
                    start, end, orderByComparator
                };
        }

        List<RoomBlockade> list = (List<RoomBlockade>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (RoomBlockade roomBlockade : list) {
                if ((roomId != roomBlockade.getRoomId()) ||
                        (recursive != roomBlockade.getRecursive())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_ROOMBLOCKADE_WHERE);

            query.append(_FINDER_COLUMN_RECURSIVE_ROOMID_2);

            query.append(_FINDER_COLUMN_RECURSIVE_RECURSIVE_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(RoomBlockadeModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(roomId);

                qPos.add(recursive);

                if (!pagination) {
                    list = (List<RoomBlockade>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<RoomBlockade>(list);
                } else {
                    list = (List<RoomBlockade>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
     *
     * @param roomId the room ID
     * @param recursive the recursive
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room blockade
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade findByRecursive_First(long roomId, boolean recursive,
        OrderByComparator orderByComparator)
        throws NoSuchRoomBlockadeException, SystemException {
        RoomBlockade roomBlockade = fetchByRecursive_First(roomId, recursive,
                orderByComparator);

        if (roomBlockade != null) {
            return roomBlockade;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("roomId=");
        msg.append(roomId);

        msg.append(", recursive=");
        msg.append(recursive);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomBlockadeException(msg.toString());
    }

    /**
     * Returns the first room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
     *
     * @param roomId the room ID
     * @param recursive the recursive
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room blockade, or <code>null</code> if a matching room blockade could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade fetchByRecursive_First(long roomId, boolean recursive,
        OrderByComparator orderByComparator) throws SystemException {
        List<RoomBlockade> list = findByRecursive(roomId, recursive, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
     *
     * @param roomId the room ID
     * @param recursive the recursive
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room blockade
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a matching room blockade could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade findByRecursive_Last(long roomId, boolean recursive,
        OrderByComparator orderByComparator)
        throws NoSuchRoomBlockadeException, SystemException {
        RoomBlockade roomBlockade = fetchByRecursive_Last(roomId, recursive,
                orderByComparator);

        if (roomBlockade != null) {
            return roomBlockade;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("roomId=");
        msg.append(roomId);

        msg.append(", recursive=");
        msg.append(recursive);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomBlockadeException(msg.toString());
    }

    /**
     * Returns the last room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
     *
     * @param roomId the room ID
     * @param recursive the recursive
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room blockade, or <code>null</code> if a matching room blockade could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade fetchByRecursive_Last(long roomId, boolean recursive,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByRecursive(roomId, recursive);

        if (count == 0) {
            return null;
        }

        List<RoomBlockade> list = findByRecursive(roomId, recursive, count - 1,
                count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the room blockades before and after the current room blockade in the ordered set where roomId = &#63; and recursive = &#63;.
     *
     * @param id the primary key of the current room blockade
     * @param roomId the room ID
     * @param recursive the recursive
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next room blockade
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade[] findByRecursive_PrevAndNext(long id, long roomId,
        boolean recursive, OrderByComparator orderByComparator)
        throws NoSuchRoomBlockadeException, SystemException {
        RoomBlockade roomBlockade = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            RoomBlockade[] array = new RoomBlockadeImpl[3];

            array[0] = getByRecursive_PrevAndNext(session, roomBlockade,
                    roomId, recursive, orderByComparator, true);

            array[1] = roomBlockade;

            array[2] = getByRecursive_PrevAndNext(session, roomBlockade,
                    roomId, recursive, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected RoomBlockade getByRecursive_PrevAndNext(Session session,
        RoomBlockade roomBlockade, long roomId, boolean recursive,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ROOMBLOCKADE_WHERE);

        query.append(_FINDER_COLUMN_RECURSIVE_ROOMID_2);

        query.append(_FINDER_COLUMN_RECURSIVE_RECURSIVE_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(RoomBlockadeModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(roomId);

        qPos.add(recursive);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(roomBlockade);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<RoomBlockade> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the room blockades where roomId = &#63; and recursive = &#63; from the database.
     *
     * @param roomId the room ID
     * @param recursive the recursive
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByRecursive(long roomId, boolean recursive)
        throws SystemException {
        for (RoomBlockade roomBlockade : findByRecursive(roomId, recursive,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(roomBlockade);
        }
    }

    /**
     * Returns the number of room blockades where roomId = &#63; and recursive = &#63;.
     *
     * @param roomId the room ID
     * @param recursive the recursive
     * @return the number of matching room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByRecursive(long roomId, boolean recursive)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_RECURSIVE;

        Object[] finderArgs = new Object[] { roomId, recursive };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_ROOMBLOCKADE_WHERE);

            query.append(_FINDER_COLUMN_RECURSIVE_ROOMID_2);

            query.append(_FINDER_COLUMN_RECURSIVE_RECURSIVE_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(roomId);

                qPos.add(recursive);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the room blockade in the entity cache if it is enabled.
     *
     * @param roomBlockade the room blockade
     */
    @Override
    public void cacheResult(RoomBlockade roomBlockade) {
        EntityCacheUtil.putResult(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeImpl.class, roomBlockade.getPrimaryKey(), roomBlockade);

        roomBlockade.resetOriginalValues();
    }

    /**
     * Caches the room blockades in the entity cache if it is enabled.
     *
     * @param roomBlockades the room blockades
     */
    @Override
    public void cacheResult(List<RoomBlockade> roomBlockades) {
        for (RoomBlockade roomBlockade : roomBlockades) {
            if (EntityCacheUtil.getResult(
                        RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
                        RoomBlockadeImpl.class, roomBlockade.getPrimaryKey()) == null) {
                cacheResult(roomBlockade);
            } else {
                roomBlockade.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all room blockades.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(RoomBlockadeImpl.class.getName());
        }

        EntityCacheUtil.clearCache(RoomBlockadeImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the room blockade.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(RoomBlockade roomBlockade) {
        EntityCacheUtil.removeResult(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeImpl.class, roomBlockade.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<RoomBlockade> roomBlockades) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (RoomBlockade roomBlockade : roomBlockades) {
            EntityCacheUtil.removeResult(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
                RoomBlockadeImpl.class, roomBlockade.getPrimaryKey());
        }
    }

    /**
     * Creates a new room blockade with the primary key. Does not add the room blockade to the database.
     *
     * @param id the primary key for the new room blockade
     * @return the new room blockade
     */
    @Override
    public RoomBlockade create(long id) {
        RoomBlockade roomBlockade = new RoomBlockadeImpl();

        roomBlockade.setNew(true);
        roomBlockade.setPrimaryKey(id);

        return roomBlockade;
    }

    /**
     * Removes the room blockade with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the room blockade
     * @return the room blockade that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade remove(long id)
        throws NoSuchRoomBlockadeException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the room blockade with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the room blockade
     * @return the room blockade that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade remove(Serializable primaryKey)
        throws NoSuchRoomBlockadeException, SystemException {
        Session session = null;

        try {
            session = openSession();

            RoomBlockade roomBlockade = (RoomBlockade) session.get(RoomBlockadeImpl.class,
                    primaryKey);

            if (roomBlockade == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchRoomBlockadeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(roomBlockade);
        } catch (NoSuchRoomBlockadeException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected RoomBlockade removeImpl(RoomBlockade roomBlockade)
        throws SystemException {
        roomBlockade = toUnwrappedModel(roomBlockade);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(roomBlockade)) {
                roomBlockade = (RoomBlockade) session.get(RoomBlockadeImpl.class,
                        roomBlockade.getPrimaryKeyObj());
            }

            if (roomBlockade != null) {
                session.delete(roomBlockade);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (roomBlockade != null) {
            clearCache(roomBlockade);
        }

        return roomBlockade;
    }

    @Override
    public RoomBlockade updateImpl(
        br.com.atilo.jcondo.booking.model.RoomBlockade roomBlockade)
        throws SystemException {
        roomBlockade = toUnwrappedModel(roomBlockade);

        boolean isNew = roomBlockade.isNew();

        RoomBlockadeModelImpl roomBlockadeModelImpl = (RoomBlockadeModelImpl) roomBlockade;

        Session session = null;

        try {
            session = openSession();

            if (roomBlockade.isNew()) {
                session.save(roomBlockade);

                roomBlockade.setNew(false);
            } else {
                session.merge(roomBlockade);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !RoomBlockadeModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((roomBlockadeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        roomBlockadeModelImpl.getOriginalRoomId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ROOM, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM,
                    args);

                args = new Object[] { roomBlockadeModelImpl.getRoomId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_ROOM, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_ROOM,
                    args);
            }

            if ((roomBlockadeModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RECURSIVE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        roomBlockadeModelImpl.getOriginalRoomId(),
                        roomBlockadeModelImpl.getOriginalRecursive()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RECURSIVE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RECURSIVE,
                    args);

                args = new Object[] {
                        roomBlockadeModelImpl.getRoomId(),
                        roomBlockadeModelImpl.getRecursive()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_RECURSIVE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_RECURSIVE,
                    args);
            }
        }

        EntityCacheUtil.putResult(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
            RoomBlockadeImpl.class, roomBlockade.getPrimaryKey(), roomBlockade);

        return roomBlockade;
    }

    protected RoomBlockade toUnwrappedModel(RoomBlockade roomBlockade) {
        if (roomBlockade instanceof RoomBlockadeImpl) {
            return roomBlockade;
        }

        RoomBlockadeImpl roomBlockadeImpl = new RoomBlockadeImpl();

        roomBlockadeImpl.setNew(roomBlockade.isNew());
        roomBlockadeImpl.setPrimaryKey(roomBlockade.getPrimaryKey());

        roomBlockadeImpl.setId(roomBlockade.getId());
        roomBlockadeImpl.setRoomId(roomBlockade.getRoomId());
        roomBlockadeImpl.setBeginDate(roomBlockade.getBeginDate());
        roomBlockadeImpl.setEndDate(roomBlockade.getEndDate());
        roomBlockadeImpl.setDescription(roomBlockade.getDescription());
        roomBlockadeImpl.setRecursive(roomBlockade.isRecursive());
        roomBlockadeImpl.setOprDate(roomBlockade.getOprDate());
        roomBlockadeImpl.setOprUser(roomBlockade.getOprUser());

        return roomBlockadeImpl;
    }

    /**
     * Returns the room blockade with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the room blockade
     * @return the room blockade
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade findByPrimaryKey(Serializable primaryKey)
        throws NoSuchRoomBlockadeException, SystemException {
        RoomBlockade roomBlockade = fetchByPrimaryKey(primaryKey);

        if (roomBlockade == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchRoomBlockadeException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return roomBlockade;
    }

    /**
     * Returns the room blockade with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException} if it could not be found.
     *
     * @param id the primary key of the room blockade
     * @return the room blockade
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomBlockadeException if a room blockade with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade findByPrimaryKey(long id)
        throws NoSuchRoomBlockadeException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the room blockade with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the room blockade
     * @return the room blockade, or <code>null</code> if a room blockade with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        RoomBlockade roomBlockade = (RoomBlockade) EntityCacheUtil.getResult(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
                RoomBlockadeImpl.class, primaryKey);

        if (roomBlockade == _nullRoomBlockade) {
            return null;
        }

        if (roomBlockade == null) {
            Session session = null;

            try {
                session = openSession();

                roomBlockade = (RoomBlockade) session.get(RoomBlockadeImpl.class,
                        primaryKey);

                if (roomBlockade != null) {
                    cacheResult(roomBlockade);
                } else {
                    EntityCacheUtil.putResult(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
                        RoomBlockadeImpl.class, primaryKey, _nullRoomBlockade);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(RoomBlockadeModelImpl.ENTITY_CACHE_ENABLED,
                    RoomBlockadeImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return roomBlockade;
    }

    /**
     * Returns the room blockade with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the room blockade
     * @return the room blockade, or <code>null</code> if a room blockade with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public RoomBlockade fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the room blockades.
     *
     * @return the room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<RoomBlockade> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the room blockades.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of room blockades
     * @param end the upper bound of the range of room blockades (not inclusive)
     * @return the range of room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<RoomBlockade> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the room blockades.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomBlockadeModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of room blockades
     * @param end the upper bound of the range of room blockades (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<RoomBlockade> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<RoomBlockade> list = (List<RoomBlockade>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ROOMBLOCKADE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ROOMBLOCKADE;

                if (pagination) {
                    sql = sql.concat(RoomBlockadeModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<RoomBlockade>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<RoomBlockade>(list);
                } else {
                    list = (List<RoomBlockade>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the room blockades from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (RoomBlockade roomBlockade : findAll()) {
            remove(roomBlockade);
        }
    }

    /**
     * Returns the number of room blockades.
     *
     * @return the number of room blockades
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ROOMBLOCKADE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the room blockade persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.booking.model.RoomBlockade")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<RoomBlockade>> listenersList = new ArrayList<ModelListener<RoomBlockade>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<RoomBlockade>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(RoomBlockadeImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
