package br.com.atilo.jcondo.booking.service.http;

import br.com.atilo.jcondo.booking.service.RoomServiceUtil;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

import java.rmi.RemoteException;

/**
 * Provides the SOAP utility for the
 * {@link br.com.atilo.jcondo.booking.service.RoomServiceUtil} service utility. The
 * static methods of this class calls the same methods of the service utility.
 * However, the signatures are different because it is difficult for SOAP to
 * support certain types.
 *
 * <p>
 * ServiceBuilder follows certain rules in translating the methods. For example,
 * if the method in the service utility returns a {@link java.util.List}, that
 * is translated to an array of {@link br.com.atilo.jcondo.booking.model.RoomSoap}.
 * If the method in the service utility returns a
 * {@link br.com.atilo.jcondo.booking.model.Room}, that is translated to a
 * {@link br.com.atilo.jcondo.booking.model.RoomSoap}. Methods that SOAP cannot
 * safely wire are skipped.
 * </p>
 *
 * <p>
 * The benefits of using the SOAP utility is that it is cross platform
 * compatible. SOAP allows different languages like Java, .NET, C++, PHP, and
 * even Perl, to call the generated services. One drawback of SOAP is that it is
 * slow because it needs to serialize all calls into a text format (XML).
 * </p>
 *
 * <p>
 * You can see a list of services at http://localhost:8080/api/axis. Set the
 * property <b>axis.servlet.hosts.allowed</b> in portal.properties to configure
 * security.
 * </p>
 *
 * <p>
 * The SOAP utility is only generated for remote services.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomServiceHttp
 * @see br.com.atilo.jcondo.booking.model.RoomSoap
 * @see br.com.atilo.jcondo.booking.service.RoomServiceUtil
 * @generated
 */
public class RoomServiceSoap {
    private static Log _log = LogFactoryUtil.getLog(RoomServiceSoap.class);

    public static br.com.atilo.jcondo.booking.model.RoomSoap addRoom(
        br.com.atilo.jcondo.booking.model.RoomSoap room)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Room returnValue = RoomServiceUtil.addRoom(br.com.atilo.jcondo.booking.model.impl.RoomModelImpl.toModel(
                        room));

            return br.com.atilo.jcondo.booking.model.RoomSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.RoomSoap updateRoom(
        br.com.atilo.jcondo.booking.model.RoomSoap room)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Room returnValue = RoomServiceUtil.updateRoom(br.com.atilo.jcondo.booking.model.impl.RoomModelImpl.toModel(
                        room));

            return br.com.atilo.jcondo.booking.model.RoomSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.RoomSoap deleteRoom(
        br.com.atilo.jcondo.booking.model.RoomSoap room)
        throws RemoteException {
        try {
            br.com.atilo.jcondo.booking.model.Room returnValue = RoomServiceUtil.deleteRoom(br.com.atilo.jcondo.booking.model.impl.RoomModelImpl.toModel(
                        room));

            return br.com.atilo.jcondo.booking.model.RoomSoap.toSoapModel(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.Image addRoomPicture(long roomId,
        br.com.atilo.jcondo.Image image) throws RemoteException {
        try {
            br.com.atilo.jcondo.Image returnValue = RoomServiceUtil.addRoomPicture(roomId,
                    image);

            return returnValue;
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.Image deleteRoomPicture(long roomId,
        long imageId) throws RemoteException {
        try {
            br.com.atilo.jcondo.Image returnValue = RoomServiceUtil.deleteRoomPicture(roomId,
                    imageId);

            return returnValue;
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static br.com.atilo.jcondo.booking.model.RoomSoap[] getRooms(
        boolean onlyAvailables, boolean onlyBookables)
        throws RemoteException {
        try {
            java.util.List<br.com.atilo.jcondo.booking.model.Room> returnValue = RoomServiceUtil.getRooms(onlyAvailables,
                    onlyBookables);

            return br.com.atilo.jcondo.booking.model.RoomSoap.toSoapModels(returnValue);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }

    public static void checkRoomAvailability(long roomId, java.util.Date date)
        throws RemoteException {
        try {
            RoomServiceUtil.checkRoomAvailability(roomId, date);
        } catch (Exception e) {
            _log.error(e, e);

            throw new RemoteException(e.getMessage());
        }
    }
}
