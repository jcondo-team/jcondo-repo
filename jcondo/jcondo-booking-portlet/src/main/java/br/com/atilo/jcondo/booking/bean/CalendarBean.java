package br.com.atilo.jcondo.booking.bean;

import java.io.Serializable;
import java.util.ArrayList;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import java.util.PropertyResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.component.UIInput;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.component.tabview.TabView;
import org.primefaces.context.RequestContext;
import org.primefaces.event.DateViewChangeEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.TabChangeEvent;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.booking.model.RoomBooking;
import br.com.atilo.jcondo.booking.model.datatype.BookingStatus;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingServiceUtil;
import br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomBlockadeLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomBookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomServiceUtil;
import br.com.atilo.jcondo.booking.service.permission.BookingPermission;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.FlatServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonServiceUtil;

import com.liferay.util.portlet.PortletProps;
import com.sun.faces.util.MessageFactory;

@ManagedBean
@SessionScoped
public class CalendarBean extends Observable implements Observer, Serializable {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = Logger.getLogger(CalendarBean.class);

	private Person person;

	private List<Person> people;

	private List<Flat> flats;

	private Booking booking;

	private List<Booking> bookings;

	private List<Booking> bookeds;

	private String bookedDates;
	
	private List<Room> rooms;

	private Room room;

	private RoomBooking roomBooking;

	private List<RoomBooking> roomBookings;

	private boolean deal;

	private String guestName;

	private String guestSurname;

	private List<Guest> guests;

	private int beginHour;

	private int endHour;

	private Date minBookingDate;

	private Date maxBookingDate;

	private String minDate;

	private String maxDate;

	private String bookingNote;

	private int tabIndex;

	public CalendarBean() {
		minBookingDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		maxBookingDate = BookingLocalServiceUtil.getMaxBookingDate();
		minDate = DateFormatUtils.format(minBookingDate, "dd/MM/yyyy");
		maxDate = DateFormatUtils.format(maxBookingDate, "dd/MM/yyyy");
	}
	
	@PostConstruct
	public void init() {
		try {
			rooms = RoomServiceUtil.getRooms(true, false);

			if (!CollectionUtils.isEmpty(rooms)) {
				room = rooms.get(0);
				tabIndex = 0;
			}

			person = PersonLocalServiceUtil.getPerson();
			flats = FlatServiceUtil.getFlats();
			bookings = BookingServiceUtil.getBookings(room.getId(), minBookingDate, maxBookingDate);
			updateBookedDates();
		} catch (Exception e) {
			LOGGER.error("Failure on calendar initialization", e);
		}
	}
	
	public void onFlatSelect() {
		try {
			if (BookingPermission.hasPermission(Permission.ASSIGN_PERSON, booking)) {
				people = PersonServiceUtil.getDomainPeople(booking.getDomainId());
				for (int i = people.size() - 1; i >= 0; i--) {
					if (!BookingPermission.hasPermission(Permission.ADD, 
														 people.get(i).getId(), 
														 booking.getDomainId())) {
						people.remove(i);
					}
				}
			} else {
				booking.setPerson(person);
				people = null;
			}
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.fatal("Failure on book", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onRoomSelect(TabChangeEvent event) {
		try {
			TabView tabView = (TabView) event.getSource();
			tabIndex = tabView.getIndex();
			room = (Room) rooms.get(tabIndex);
			bookings = BookingServiceUtil.getBookings(room.getId(), minBookingDate, maxBookingDate);
			updateBookedDates();
		} catch (Exception e) {
			LOGGER.error("Failure on room select", e);
		}
	}

	public void onBookingAdd() {
		LOGGER.trace("Method in");

		try {
			if (deal) {
				booking = BookingServiceUtil.addBooking(room.getId(), booking.getDomainId(), booking.getPersonId(),
														DateUtils.setHours(booking.getBeginDate(), roomBooking.getOpenHour()),
														DateUtils.setHours(booking.getEndDate(), roomBooking.getCloseHour()), guests);

				bookings = BookingServiceUtil.getBookings(room.getId(), minBookingDate, maxBookingDate);

				deal = false;
				updateBookedDates();				

				setChanged();
				notifyObservers(booking);				
				
				if (booking.getRoom().isBookable() && booking.getInvoice() == null) {
					throw new BusinessException("exception.booking.invoice.add");
				}

				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.add", null);
			} else {
				throw new BusinessException("msg.booking.deal.unchecked");
			}
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.fatal("Failure on book", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.add", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}

		LOGGER.trace("Method out");
	}

	public void onBookingCancel() {
		try { 
			if (booking != null) {
				booking = BookingServiceUtil.cancelBooking(booking.getId());

				if (booking.getStatus() == BookingStatus.CANCELLED) {
					bookings.set(bookings.indexOf(booking), booking);	
				} else {
					bookings.remove(booking);
				}

				updateBookedDates();

				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.cancel", null);
			}
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on booking cancel", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.cancel", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}

		setChanged();
		notifyObservers(booking);
	}

	public void onBookingDelete() {
		try {
			if (booking != null) {
				BookingServiceUtil.deleteBooking(booking.getId(), bookingNote);
				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.delete", null);
				bookings.remove(booking);
				updateBookedDates();
			}
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on booking delete", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.delete", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}

		setChanged();
		notifyObservers(booking);
	}

	public void onBookingSelect(TabChangeEvent event) {
		TabView tabView = (TabView) event.getSource();
		booking = (Booking) bookeds.get(tabView.getIndex());
	}
	
	public void onBookingCreate(Date bookingDate) {
		try {
			booking = BookingLocalServiceUtil.createBooking(0);
			booking.setRoomId(room.getId());
			booking.setBeginDate((Date) bookingDate.clone());
			booking.setEndDate((Date) bookingDate.clone());

			if (flats.size() == 1) {
				booking.setDomain(flats.get(0));
				onFlatSelect();
			} else {
				people = null;
			}

			guests = new ArrayList<Guest>();

			roomBookings = RoomBookingLocalServiceUtil.getRoomBookings(booking.getRoomId(), DateUtils.toCalendar(bookingDate).get(Calendar.DAY_OF_WEEK));

			roomBooking = !CollectionUtils.isEmpty(roomBookings) && roomBookings.size() == 1 ? roomBookings.get(0) : null;

			deal = false;
		} catch (Exception e) {
			LOGGER.error("Failure on booking create", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}
	
	public void onViewChange(DateViewChangeEvent event) {
		try {
			bookings = BookingServiceUtil.getBookings(room.getId(), minBookingDate, maxBookingDate);
			updateBookedDates();
		} catch (Exception e) {
			LOGGER.error("Failure on calendar view change", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}
	
	public void onDateSelect(SelectEvent event) {
		try {
			Date bookingDate = (Date) event.getObject();
			bookeds = BookingServiceUtil.getBookings(room.getId(), bookingDate);

			for (int i = bookeds.size() - 1; i >= 0; i--) {
				try {
					if (!BookingPermission.hasPermission(Permission.VIEW, bookeds.get(i))) {
						bookeds.remove(i);
					}
				} catch (Exception ex) {
					LOGGER.fatal("Failure on booking view permission check", ex);
				}
			}

			if (!CollectionUtils.isEmpty(bookeds)) {
				booking = bookeds.get(0);
				RequestContext.getCurrentInstance().addCallbackParam("hasBookings", true);
			} else {
				RoomLocalServiceUtil.checkRoomAvailability(room.getId(), bookingDate);
				onBookingCreate(bookingDate);
			}
		} catch (BusinessException e) {
			LOGGER.debug("Failure on booking date select", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on booking date select", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onGuestAdd() {
		try {
			if (booking.isNew()) {
				Guest guest = GuestLocalServiceUtil.createGuest(0);
				guest.setName(guestName);
				guest.setSurname(guestSurname);
				guests.add(guest);
			} else {
				GuestLocalServiceUtil.addGuest(booking.getId(), guestName, guestSurname);
			}
			guestName = null;
			guestSurname = null;
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.guest.add", null);
		} catch (Exception e) {
			LOGGER.error("Failure on booking date select", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.guest.add", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onGuestDelete(Guest guest) {
		try {
			if (booking.isNew()) {
				guests.remove(guest);
			} else {
				GuestLocalServiceUtil.deleteGuest(guest.getGuestId());
			}

			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.guest.delete", null);
		} catch (Exception e) {
			LOGGER.error("Failure on booking guest add", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.guest.delete", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void updateBookedDates() throws Exception {
		List<String> dates = new ArrayList<String>();

		for (Date date : RoomBlockadeLocalServiceUtil.getBlockedDates(room.getId(), minBookingDate, maxBookingDate)) {
			dates.add("-1;" + DateFormatUtils.format(date, "yyyy-MM-dd 00:00:00"));
		}

		for (Date date : RoomBookingLocalServiceUtil.getUnavailableDates(room.getId(), minBookingDate, maxBookingDate)) {
			dates.add("-2;" + DateFormatUtils.format(date, "yyyy-MM-dd 00:00:00"));
		}

		for (Booking b : bookings) {
			dates.add(b.getStatusId() + ";" + DateFormatUtils.format(b.getBeginDate(), "yyyy-MM-dd 00:00:00"));
		}

		bookedDates = StringUtils.strip(dates.toString(), "[]");
	}

	public void validateCheckbox(FacesContext context, UIComponent component, Object value) {  
		if (value instanceof Boolean && ((Boolean) value).equals(Boolean.FALSE)) {
			FacesMessage message = MessageUtils.getMessage(UIInput.REQUIRED_MESSAGE_ID, null);
			throw new ValidatorException(message);  
		}  
	}  

	public void validatePassword(FacesContext context, UIComponent component, Object value) {  
		if (value instanceof String) {
			boolean authenticated = false;

			try {
				String pwd = (String) value;
				authenticated = PersonServiceUtil.authenticatePerson(person.getId(), pwd);
			} catch (Exception e) {
			}
			
			if (!authenticated) {
				String clientId = component.getClientId(context);
				FacesMessage message = MessageFactory.getMessage(UIInput.REQUIRED_MESSAGE_ID, clientId);
				throw new ValidatorException(message);  
			}
		}
	}
	
	public boolean hasAvailability() {
		try {
			RoomLocalServiceUtil.checkRoomAvailability(room.getId(), booking.getBeginDate());
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean hasBookings() {
		try {
			return !CollectionUtils.isEmpty(BookingLocalServiceUtil.getBookings(room.getId(), booking.getBeginDate()));
		} catch (Exception e) {
			return false;
		}
	}
	
	public boolean hasBookingAlert() {
		try {
			String[] roomIds = StringUtils.split(PortletProps.get("booking.alert.rooms"), ",");
			return ArrayUtils.contains(roomIds, String.valueOf(room.getId()));
		} catch (Exception e) {
			return false;
		}
	}

	public List<Booking> getBookingsTime() {
		try {
			return BookingLocalServiceUtil.getBookings(room.getId(), booking.getBeginDate()); 
		} catch (Exception e) {
			return null;
		}		
	}

	public String getBookingStyle(Booking booking) {
		try {
			int index = rooms.indexOf(booking.getRoom());
			if (booking.getStatus() == BookingStatus.CANCELLED) {
				return PropertyResourceBundle.getBundle("application").getString("cld.my.bookings.room." + index);
			} else {
				return PropertyResourceBundle.getBundle("application").getString("my.bookings.room." + index);
			}
		} catch (Exception e) {
			return "";
		}
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public List<Person> getPeople() {
		return people;
	}

	public void setPeople(List<Person> people) {
		this.people = people;
	}

	public List<Flat> getFlats() {
		return flats;
	}

	public void setFlats(List<Flat> flats) {
		this.flats = flats;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	public List<Booking> getBookeds() {
		return bookeds;
	}

	public void setBookeds(List<Booking> bookeds) {
		this.bookeds = bookeds;
	}

	public String getBookedDates() {
		return bookedDates;
	}

	public void setBookedDates(String bookedDates) {
		this.bookedDates = bookedDates;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public RoomBooking getRoomBooking() {
		return roomBooking;
	}

	public void setRoomBooking(RoomBooking roomBooking) {
		this.roomBooking = roomBooking;
	}

	
	public List<RoomBooking> getRoomBookings() {
	
		return roomBookings;
	}

	
	public void setRoomBookings(List<RoomBooking> roomBookings) {
	
		this.roomBookings = roomBookings;
	}

	public boolean isDeal() {
		return deal;
	}

	public void setDeal(boolean deal) {
		this.deal = deal;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getGuestSurname() {
		return guestSurname;
	}

	public void setGuestSurname(String guestSurname) {
		this.guestSurname = guestSurname;
	}

	public List<Guest> getGuests() {
		return guests;
	}

	public void setGuests(List<Guest> guests) {
		this.guests = guests;
	}

	public int getBeginHour() {
		return beginHour;
	}

	public void setBeginHour(int beginHour) {
		this.beginHour = beginHour;

		try {
			booking.setBeginDate(DateUtils.setHours(booking.getBeginDate(), beginHour));
		} catch (Exception e) {
			LOGGER.error("Failure on booking date select", e);
		}
	}

	public int getEndHour() {
		return endHour;
	}

	public void setEndHour(int endHour) {
		this.endHour = endHour;
		
		try {
			booking.setEndDate(DateUtils.setHours(booking.getEndDate(), endHour));
		} catch (Exception e) {
			LOGGER.error("Failure on booking date select", e);
		}
	}

	public Date getMinBookingDate() {
		return minBookingDate;
	}

	public void setMinBookingDate(Date minBookingDate) {
		this.minBookingDate = minBookingDate;
	}

	public Date getMaxBookingDate() {
		return maxBookingDate;
	}

	public void setMaxBookingDate(Date maxBookingDate) {
		this.maxBookingDate = maxBookingDate;
	}

	public String getMinDate() {
		return minDate;
	}

	public void setMinDate(String minDate) {
		this.minDate = minDate;
	}

	public String getMaxDate() {
		return maxDate;
	}

	public void setMaxDate(String maxDate) {
		this.maxDate = maxDate;
	}

	public String getBookingNote() {
		return bookingNote;
	}

	public void setBookingNote(String bookingNote) {
		this.bookingNote = bookingNote;
	}

	public int getTabIndex() {
		return tabIndex;
	}

	public void setTabIndex(int tabIndex) {
		this.tabIndex = tabIndex;
	}

	public void update(Observable obs, Object obj) {
		try {
			if(obj instanceof Booking) {
				Booking booking = (Booking) obj;
				if (room.equals(booking.getRoom())) {
					bookings = BookingServiceUtil.getBookings(room.getId(), minBookingDate, maxBookingDate);
					updateBookedDates();
				}
			}
		} catch (Exception e) {
			LOGGER.error("Failure on update calendar view", e);
		}
	}
}
