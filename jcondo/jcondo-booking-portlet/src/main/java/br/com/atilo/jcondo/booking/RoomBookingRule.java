package br.com.atilo.jcondo.booking;

import java.util.Date;

public interface RoomBookingRule {

	public void validate(long personId, Date bookingDate) throws Exception ;

}
