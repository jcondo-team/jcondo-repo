/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.joda.time.Interval;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.repository.model.Folder;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.kernel.util.MimeTypesUtil;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.model.GroupConstants;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.NoSuchFileEntryTypeException;
import com.liferay.portlet.documentlibrary.model.DLFolderConstants;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.service.DLFileEntryTypeLocalServiceUtil;
import com.liferay.portlet.documentlibrary.util.DLUtil;

import br.com.atilo.jcondo.booking.NoSuchRoomException;
import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.booking.model.RoomBooking;
import br.com.atilo.jcondo.booking.model.datatype.BookingStatus;
import br.com.atilo.jcondo.booking.service.base.RoomLocalServiceBaseImpl;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.Image;

/**
 * The implementation of the room local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.RoomLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.RoomLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil
 */
public class RoomLocalServiceImpl extends RoomLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil} to access the room local service.
	 */
	private static final ResourceBundle RB = ResourceBundle.getBundle("application");

	public static final int WHOLE_DAY_PERIOD = -1;

	public static final String TERMS_OF_USE = "Terms of use";

	public Room addRoom(String name, String description, double price, int capacity, boolean available, boolean bookable, File agreement) throws PortalException, SystemException {
		Room room;

		try {
			room = roomPersistence.findByName(name);
			throw new BusinessException("exception.room.duplicate", room);
		} catch (NoSuchRoomException e) {
		}

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		long repositoryId = GroupLocalServiceUtil.getGroup(sc.getCompanyId(), GroupConstants.GUEST).getGroupId();
		Folder parentFolder = DLAppServiceUtil.getFolder(repositoryId,
														 DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, 
														 RB.getString("rooms.root.folder.name"));

		Folder folder = DLAppServiceUtil.addFolder(parentFolder.getRepositoryId(), parentFolder.getFolderId(), name, null, sc);

		FileEntry fileEntry = null;
		try {
			if (agreement != null) {
				long fileEntryTypeId;
				try {
					fileEntryTypeId = DLFileEntryTypeLocalServiceUtil.getFileEntryType(folder.getGroupId(), 
																					   TERMS_OF_USE).getFileEntryTypeId();
				} catch (NoSuchFileEntryTypeException e) {
					fileEntryTypeId = 0;
				}

				sc.setAttribute("fileEntryTypeId", fileEntryTypeId);
				String fileTitle = RB.getString("rooms.agreement.term").concat(name).replaceAll(" ", "_");
				fileEntry = DLAppServiceUtil.addFileEntry(folder.getRepositoryId(), folder.getFolderId(), 
														  agreement.getName(), MimeTypesUtil.getContentType(agreement), 
														  fileTitle, null, StringUtils.EMPTY, agreement, sc);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		room = createRoom(counterLocalService.increment());
		room.setFolderId(folder.getFolderId());
		room.setAgreementId(fileEntry != null ? fileEntry.getFileEntryId(): 0);
		room.setName(name);
		room.setDescription(description);
		room.setAvailable(available);
		room.setBookable(bookable);
		room.setPrice(price);
		room.setCapacity(capacity);
		room.setOprDate(new Date());
		room.setOprUser(sc.getUserId());

		room = super.addRoom(room);

		Map<String, Integer> weekDays = Calendar.getInstance().getDisplayNames(Calendar.DAY_OF_WEEK, 
																			   Calendar.SHORT, Locale.ENGLISH);

		for (Integer weekDay : weekDays.values()) {
			roomBookingLocalService.addRoomBooking(room.getId(), weekDay, 10, 22, -1);
		}

		return room;
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Room addRoom(Room room) throws SystemException {
		try {
			return addRoom(room.getName(), room.getDescription(), room.getPrice(), 
						   room.getCapacity(), room.isAvailable(), room.isBookable(), null);
		} catch (PortalException e) {
			throw new SystemException(e);
		}
	}

	public Room updateRoom(long roomId, String name, String description, double price, int capacity, boolean available, boolean bookable, File agreement) throws PortalException, SystemException {
		Room room;
		
		try {
			room = roomPersistence.findByName(name);

			if (room != null && room.getId() != roomId) {
				throw new BusinessException("exception.room.duplicate", room);
			}
		} catch (NoSuchRoomException e) {
			room = getRoom(roomId);
		}

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();

		FileEntry fileEntry = null;
		try {
			if (agreement != null) {
				if (room.getAgreementId() > 0) {
					fileEntry = DLAppServiceUtil.getFileEntry(room.getAgreementId());
					DLAppServiceUtil.updateFileEntry(fileEntry.getFileEntryId(), agreement.getName(), 
													 MimeTypesUtil.getContentType(agreement),
													 fileEntry.getTitle(), StringUtils.EMPTY, 
													 StringUtils.EMPTY, true, agreement, sc);
				} else {
					Folder folder = DLAppServiceUtil.getFolder(room.getFolderId());
					long fileEntryTypeId;
					try {
						fileEntryTypeId = DLFileEntryTypeLocalServiceUtil.getFileEntryType(folder.getGroupId(), 
																						   TERMS_OF_USE).getFileEntryTypeId();
					} catch (NoSuchFileEntryTypeException e) {
						fileEntryTypeId = 0;
					}

					sc.setAttribute("fileEntryTypeId", fileEntryTypeId);
					String fileTitle = RB.getString("rooms.agreement.term").concat(name).replaceAll(" ", "_");
					fileEntry = DLAppServiceUtil.addFileEntry(folder.getRepositoryId(), folder.getFolderId(), 
															  agreement.getName(), MimeTypesUtil.getContentType(agreement), 
															  fileTitle, null, StringUtils.EMPTY, agreement, sc);

					room.setAgreementId(fileEntry.getFileEntryId());
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		room.setName(name);
		room.setDescription(description);
		room.setAvailable(available);
		room.setBookable(bookable);
		room.setPrice(price);
		room.setCapacity(capacity);

		return updateRoom(room);
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Room updateRoom(Room room) throws SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		room.setOprDate(new Date());
		room.setOprUser(sc.getUserId());
		return super.updateRoom(room);
	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public Room deleteRoom(Room room) throws SystemException {
		try {
			Folder folder = DLAppServiceUtil.getFolder(room.getFolderId());
			String name = new StringBuffer(folder.getName()).append("-").append(new Date().getTime()).toString();
			DLAppServiceUtil.updateFolder(folder.getFolderId(), name, null, null);
			Folder parentFolder = DLAppServiceUtil.getFolder(folder.getRepositoryId(), 
															 DLFolderConstants.DEFAULT_PARENT_FOLDER_ID, 
															 RB.getString("rooms.backup.folder.name"));
			DLAppServiceUtil.moveFolder(folder.getFolderId(), parentFolder.getParentFolderId(), null);
		} catch (PortalException e) {
			throw new SystemException(e);
		}

		return super.deleteRoom(updateRoom(room));
	}

	public Image addRoomPicture(long roomId, Image image) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Room room = getRoom(roomId);
		Folder folder = DLAppServiceUtil.getFolder(room.getFolderId());

		byte[] bytes;
		String contentType;
		String fileName;
		try {
			contentType = new URL(image.getPath()).openConnection().getContentType();
			bytes = IOUtils.toByteArray(new URL(image.getPath()).openStream());
			fileName = FilenameUtils.getName(image.getPath());
		} catch (MalformedURLException e) {
			throw new SystemException(e);
		} catch (IOException e) {
			throw new SystemException(e);
		}

		
		
		FileEntry fileEntry; 
		String fileTitle = "IMG_" + counterLocalService.increment(Image.class.getName());
		fileEntry = DLAppServiceUtil.addFileEntry(folder.getRepositoryId(), folder.getFolderId(), 
												 fileName, contentType, 
												 fileTitle, null, StringUtils.EMPTY, bytes, sc);

		ThemeDisplay td = (ThemeDisplay) sc.getRequest().getAttribute(WebKeys.THEME_DISPLAY);
		String path = DLUtil.getPreviewURL(fileEntry, null, td, null, false, true);

		return new Image(fileEntry.getFileEntryId(), path);
	}

	public Image deleteRoomPicture(long roomId, long imageId) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Room room = getRoom(roomId);
		Folder folder = DLAppServiceUtil.getFolder(room.getFolderId());
		FileEntry fileEntry = DLAppServiceUtil.getFileEntry(imageId);

		if (!fileEntry.getMimeType().startsWith("image")) {
			throw new BusinessException("exception.room.invalid.image");
		}

		if (fileEntry.getFolderId() != folder.getFolderId()) {
			throw new BusinessException("exception.room.image.not.belong", room.getName());
		}

		DLAppServiceUtil.deleteFileEntry(imageId);

		ThemeDisplay td = (ThemeDisplay) sc.getRequest().getAttribute(WebKeys.THEME_DISPLAY);
		String path = DLUtil.getPreviewURL(fileEntry, null, td, null, false, true);

		return new Image(fileEntry.getFileEntryId(), path);
	}

	public List<Image> getRoomPictures(long roomId) throws PortalException, SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		Room room = getRoom(roomId);
		List<Image> images = new ArrayList<Image>();
		Folder folder = DLAppServiceUtil.getFolder(room.getFolderId());

		List<FileEntry> fileEntries = DLAppServiceUtil.getFileEntries(folder.getRepositoryId(), 
																      folder.getFolderId(), 
																      -1, -1, null);

		if (!CollectionUtils.isEmpty(fileEntries)) {
			ThemeDisplay td = (ThemeDisplay) sc.getRequest().getAttribute(WebKeys.THEME_DISPLAY);

			for (FileEntry fileEntry : fileEntries) {
				if (fileEntry.getMimeType().startsWith("image")) {
					images.add(new Image(fileEntry.getFileEntryId(), 
										 DLUtil.getPreviewURL(fileEntry, null, td, "", false, false)));
				}
			}
		}

		return images;
	}
	
	public List<Room> getRooms(boolean onlyAvailables, boolean onlyBookables) throws PortalException, SystemException {
		if (!onlyAvailables && !onlyBookables) {
			return roomPersistence.findAll();
		} else if (onlyAvailables && !onlyBookables) {
			return roomPersistence.findByAvailable(true);
		} else if (!onlyAvailables && onlyBookables) {
			return roomPersistence.findByBookable(true);
		} else {
			return roomPersistence.findByBookableAndAvailable(true, true);
		}
	}

	public void checkRoomAvailability(long roomId, Date date) throws PortalException, SystemException {
		Room room = getRoom(roomId);

		if (!room.isAvailable()) {
			throw new BusinessException("exception.room.not.available", room.getName());
		}

		if (!roomBlockadeLocalService.getBlockedDates(roomId, date, date).isEmpty()) {
			throw new BusinessException("exception.room.is.blocked", room, date);
		}

		long roomBookingHours = 0;
		List<RoomBooking> roomBookings = roomBookingLocalService.getRoomBookings(roomId, DateUtils.toCalendar(date).get(Calendar.DAY_OF_WEEK));

		for (RoomBooking rb : roomBookings) {
			roomBookingHours += rb.getCloseHour() - rb.getOpenHour();
		}

		if (roomBookingHours <= 0) {
			throw new BusinessException("exception.room.not.available", room.getName());
		}

		long bookedHours = 0;
		List<Booking> bookings = bookingLocalService.getBookings(roomId, date);

		for (Booking booking : bookings) {
			if (booking.getStatus() != BookingStatus.CANCELLED && booking.getStatus() != BookingStatus.DELETED) {
				Interval interval = new Interval(booking.getBeginDate().getTime(), booking.getEndDate().getTime());
				bookedHours += interval.toDuration().getStandardHours();
			}
		}

		if (roomBookingHours - bookedHours <= 0) {
			throw new BusinessException("exception.room.no.available.time", room.getName());
		}
	}

}