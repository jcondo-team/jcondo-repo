package br.com.atilo.jcondo.booking.model.impl;

import br.com.atilo.jcondo.booking.model.Booking;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Booking in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Booking
 * @generated
 */
public class BookingCacheModel implements CacheModel<Booking>, Externalizable {
    public long id;
    public long roomId;
    public long domainId;
    public long personId;
    public long beginDate;
    public long endDate;
    public int statusId;
    public long noteId;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(id);
        sb.append(", roomId=");
        sb.append(roomId);
        sb.append(", domainId=");
        sb.append(domainId);
        sb.append(", personId=");
        sb.append(personId);
        sb.append(", beginDate=");
        sb.append(beginDate);
        sb.append(", endDate=");
        sb.append(endDate);
        sb.append(", statusId=");
        sb.append(statusId);
        sb.append(", noteId=");
        sb.append(noteId);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Booking toEntityModel() {
        BookingImpl bookingImpl = new BookingImpl();

        bookingImpl.setId(id);
        bookingImpl.setRoomId(roomId);
        bookingImpl.setDomainId(domainId);
        bookingImpl.setPersonId(personId);

        if (beginDate == Long.MIN_VALUE) {
            bookingImpl.setBeginDate(null);
        } else {
            bookingImpl.setBeginDate(new Date(beginDate));
        }

        if (endDate == Long.MIN_VALUE) {
            bookingImpl.setEndDate(null);
        } else {
            bookingImpl.setEndDate(new Date(endDate));
        }

        bookingImpl.setStatusId(statusId);
        bookingImpl.setNoteId(noteId);

        if (oprDate == Long.MIN_VALUE) {
            bookingImpl.setOprDate(null);
        } else {
            bookingImpl.setOprDate(new Date(oprDate));
        }

        bookingImpl.setOprUser(oprUser);

        bookingImpl.resetOriginalValues();

        return bookingImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        roomId = objectInput.readLong();
        domainId = objectInput.readLong();
        personId = objectInput.readLong();
        beginDate = objectInput.readLong();
        endDate = objectInput.readLong();
        statusId = objectInput.readInt();
        noteId = objectInput.readLong();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(roomId);
        objectOutput.writeLong(domainId);
        objectOutput.writeLong(personId);
        objectOutput.writeLong(beginDate);
        objectOutput.writeLong(endDate);
        objectOutput.writeInt(statusId);
        objectOutput.writeLong(noteId);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
