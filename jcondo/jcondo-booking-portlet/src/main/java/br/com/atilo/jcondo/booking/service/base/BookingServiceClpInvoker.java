package br.com.atilo.jcondo.booking.service.base;

import br.com.atilo.jcondo.booking.service.BookingServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class BookingServiceClpInvoker {
    private String _methodName62;
    private String[] _methodParameterTypes62;
    private String _methodName63;
    private String[] _methodParameterTypes63;
    private String _methodName68;
    private String[] _methodParameterTypes68;
    private String _methodName69;
    private String[] _methodParameterTypes69;
    private String _methodName70;
    private String[] _methodParameterTypes70;
    private String _methodName71;
    private String[] _methodParameterTypes71;
    private String _methodName72;
    private String[] _methodParameterTypes72;
    private String _methodName73;
    private String[] _methodParameterTypes73;
    private String _methodName74;
    private String[] _methodParameterTypes74;
    private String _methodName75;
    private String[] _methodParameterTypes75;

    public BookingServiceClpInvoker() {
        _methodName62 = "getBeanIdentifier";

        _methodParameterTypes62 = new String[] {  };

        _methodName63 = "setBeanIdentifier";

        _methodParameterTypes63 = new String[] { "java.lang.String" };

        _methodName68 = "getDomainBookings";

        _methodParameterTypes68 = new String[] { "long" };

        _methodName69 = "getPersonBookings";

        _methodParameterTypes69 = new String[] { "long" };

        _methodName70 = "getBookings";

        _methodParameterTypes70 = new String[] { "long", "java.util.Date" };

        _methodName71 = "getBookings";

        _methodParameterTypes71 = new String[] {
                "long", "java.util.Date", "java.util.Date"
            };

        _methodName72 = "addBooking";

        _methodParameterTypes72 = new String[] {
                "long", "long", "long", "java.util.Date", "java.util.Date",
                "java.util.List"
            };

        _methodName73 = "cancelBooking";

        _methodParameterTypes73 = new String[] { "long" };

        _methodName74 = "deleteBooking";

        _methodParameterTypes74 = new String[] { "long" };

        _methodName75 = "deleteBooking";

        _methodParameterTypes75 = new String[] { "long", "java.lang.String" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName62.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes62, parameterTypes)) {
            return BookingServiceUtil.getBeanIdentifier();
        }

        if (_methodName63.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes63, parameterTypes)) {
            BookingServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName68.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes68, parameterTypes)) {
            return BookingServiceUtil.getDomainBookings(((Long) arguments[0]).longValue());
        }

        if (_methodName69.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes69, parameterTypes)) {
            return BookingServiceUtil.getPersonBookings(((Long) arguments[0]).longValue());
        }

        if (_methodName70.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes70, parameterTypes)) {
            return BookingServiceUtil.getBookings(((Long) arguments[0]).longValue(),
                (java.util.Date) arguments[1]);
        }

        if (_methodName71.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes71, parameterTypes)) {
            return BookingServiceUtil.getBookings(((Long) arguments[0]).longValue(),
                (java.util.Date) arguments[1], (java.util.Date) arguments[2]);
        }

        if (_methodName72.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes72, parameterTypes)) {
            return BookingServiceUtil.addBooking(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue(),
                ((Long) arguments[2]).longValue(),
                (java.util.Date) arguments[3], (java.util.Date) arguments[4],
                (java.util.List<br.com.atilo.jcondo.booking.model.Guest>) arguments[5]);
        }

        if (_methodName73.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes73, parameterTypes)) {
            return BookingServiceUtil.cancelBooking(((Long) arguments[0]).longValue());
        }

        if (_methodName74.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes74, parameterTypes)) {
            return BookingServiceUtil.deleteBooking(((Long) arguments[0]).longValue());
        }

        if (_methodName75.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes75, parameterTypes)) {
            return BookingServiceUtil.deleteBooking(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1]);
        }

        throw new UnsupportedOperationException();
    }
}
