/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.model.impl;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil;

/**
 * The extended model implementation for the RoomBlockade service. Represents a row in the &quot;jco_room_blockade&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.model.RoomBlockade} interface.
 * </p>
 *
 * @author anderson
 */
public class RoomBlockadeImpl extends RoomBlockadeBaseImpl {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a room blockade model instance should use the {@link br.com.atilo.jcondo.booking.model.RoomBlockade} interface instead.
	 */
	public RoomBlockadeImpl() {
	}
	
	public Room getRoom() throws PortalException, SystemException {
		return getRoomId() == 0 ? null : RoomLocalServiceUtil.getRoom(getRoomId());
	}

	public void setRoom(Room room) throws PortalException, SystemException {
		setRoomId(room == null ? 0 : room.getId());
	}

}