/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.booking.model.RoomBooking;
import br.com.atilo.jcondo.booking.service.base.RoomBookingServiceBaseImpl;
import br.com.atilo.jcondo.booking.service.permission.RoomPermission;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.security.Permission;

/**
 * The implementation of the room booking remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.RoomBookingService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.RoomBookingServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.RoomBookingServiceUtil
 */
public class RoomBookingServiceImpl extends RoomBookingServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.RoomBookingServiceUtil} to access the room booking remote service.
	 */

	public RoomBooking addRoomBooking(long roomId, int weekDay, int openHour, int closeHour, int period) throws PortalException, SystemException {
		return roomBookingLocalService.addRoomBooking(roomId, weekDay, openHour, closeHour, period);
	}

	public RoomBooking updateRoomBooking(long roomBookingId, int openHour, int closeHour, int period) throws PortalException, SystemException {
		RoomBooking roomBooking = roomBookingLocalService.getRoomBooking(roomBookingId);

		if (!RoomPermission.hasPermission(Permission.UPDATE_TIME, roomBooking.getRoomId())) {
			throw new BusinessException("exception.room.booking.update.denied");			
		}

		return roomBookingLocalService.updateRoomBooking(roomBookingId, openHour, closeHour, period);
	}

	public RoomBooking updateRoomBooking(RoomBooking roomBooking) throws PortalException, SystemException {
		if (!RoomPermission.hasPermission(Permission.UPDATE_TIME, roomBooking.getRoomId())) {
			throw new BusinessException("exception.room.booking.update.denied");			
		}

		return roomBookingLocalService.updateRoomBooking(roomBooking);
	}

	public List<RoomBooking> getRoomBookings(long roomId, int weekDay) throws PortalException, SystemException {
		return roomBookingLocalService.getRoomBookings(roomId, weekDay);
	}

	public List<RoomBooking> getRoomBookings(long roomId) throws PortalException, SystemException {
		return roomBookingLocalService.getRoomBookings(roomId);
	}

}