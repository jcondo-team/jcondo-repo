package br.com.atilo.jcondo.booking.payment;

import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.liferay.portal.kernel.messaging.DestinationNames;
import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.scheduler.SchedulerEngine;
import com.liferay.portal.kernel.scheduler.SchedulerEngineUtil;
import com.liferay.portal.kernel.scheduler.SchedulerException;
import com.liferay.portal.kernel.scheduler.StorageType;
import com.liferay.portal.kernel.scheduler.Trigger;
import com.liferay.portal.kernel.scheduler.TriggerFactoryUtil;
import com.liferay.portal.kernel.scheduler.TriggerType;

import br.com.atilo.jcondo.booking.model.Payment;

public class PaymentScheduler {
	
	private static final Logger LOGGER = Logger.getLogger(PaymentScheduler.class);
	
	/** One hour in milisecconds */
	public static final int RETRY_INTERVAL = 120000; //3600000;

	public static final int RETRIES = 5;
	
	public static final String RETRY_COUNT = "RETRY_COUNT";
	
	public static final String PAYMENT_ID = "PAYMENT_ID";
	
	public static void schedule(Payment payment) throws SchedulerException {
		LOGGER.info("Add payment scheduling");

	    Message message = new Message();
	    message.put(SchedulerEngine.MESSAGE_LISTENER_CLASS_NAME, PaymentRetryJob.class.getName());
	    message.put(SchedulerEngine.PORTLET_ID, "jcondobooking_WAR_jcondobookingportlet");
	    message.put(SchedulerEngine.JOB_NAME, PaymentRetryJob.class.getSimpleName().concat(".").concat(String.valueOf(payment.getId())));
	    message.put(SchedulerEngine.GROUP_NAME, message.getString(SchedulerEngine.JOB_NAME));
	    message.put(PAYMENT_ID, payment.getId());
	    message.put(RETRY_COUNT, 0);

		schedule(message);

		LOGGER.info("Add payment scheduled");
	}

	public static void reschedule(Payment payment) throws SchedulerException {
		LOGGER.info("Add payment rescheduling");

		Message message = SchedulerEngineUtil.getScheduledJob(PaymentRetryJob.class.getSimpleName().concat(".").concat(String.valueOf(payment.getId())), 
															  PaymentRetryJob.class.getSimpleName().concat(".").concat(String.valueOf(payment.getId())), 
															  StorageType.PERSISTED).getMessage();

		message.put(RETRY_COUNT, message.getInteger(RETRY_COUNT) + 1);
		message.remove(SchedulerEngine.JOB_STATE);

		schedule(message);

		LOGGER.info("Add payment rescheduled");
	}

	private static void schedule(Message message) throws SchedulerException {
		Trigger trigger = TriggerFactoryUtil.buildTrigger(TriggerType.SIMPLE,
														  message.getString(SchedulerEngine.JOB_NAME),
														  message.getString(SchedulerEngine.GROUP_NAME),
											    		  DateUtils.addMilliseconds(new Date(), RETRY_INTERVAL),
											    		  null, RETRY_INTERVAL);

		SchedulerEngineUtil.schedule(trigger, StorageType.PERSISTED, 
									 "Add Payment Retry Routine", 
									 DestinationNames.SCHEDULER_DISPATCH, 
									 message, 0);		
	}

}
