package br.com.atilo.jcondo.booking.payment;

import org.apache.log4j.Logger;

import br.com.atilo.jcondo.booking.model.Payment;
import br.com.atilo.jcondo.booking.service.PaymentLocalServiceUtil;

import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.scheduler.SchedulerEngine;
import com.liferay.portal.kernel.scheduler.SchedulerEngineUtil;
import com.liferay.portal.kernel.scheduler.SchedulerException;
import com.liferay.portal.kernel.scheduler.StorageType;

public class PaymentRetryJob implements MessageListener {

	private static final Logger LOGGER = Logger.getLogger(PaymentRetryJob.class);
	
	private static final Logger MAIL_LOGGER = Logger.getLogger("MAILER");	
	
	public void receive(Message message) throws MessageListenerException {
		LOGGER.info("Starting to add the payment " + message.getLong(PaymentScheduler.PAYMENT_ID));

		int count = message.getInteger(PaymentScheduler.RETRY_COUNT);

		if (count < PaymentScheduler.RETRIES) {
			Payment payment = null;
			try {
				try {
					payment = PaymentLocalServiceUtil.getPayment(message.getLong(PaymentScheduler.PAYMENT_ID));
					
					LOGGER.info("Retry number: " + count);
	
					PaymentLocalServiceUtil.addPayment(payment);
	
					LOGGER.info("Added the payment " + message.getLong(PaymentScheduler.PAYMENT_ID) + " successfully");
	
					SchedulerEngineUtil.unschedule(message.getString(SchedulerEngine.JOB_NAME), 
												   message.getString(SchedulerEngine.GROUP_NAME), 
												   StorageType.PERSISTED);

					LOGGER.info("Unscheduled the PaymentRetryJob for " + message.getLong(PaymentScheduler.PAYMENT_ID) + " successfully");
				} catch (PaymentProviderException e) {
					LOGGER.info("Fail to add the payment " + message.getLong(PaymentScheduler.PAYMENT_ID) + ": " + e.getMessage());
					PaymentScheduler.reschedule(payment);
				}
			} catch (Exception e) {
				MAIL_LOGGER.error("Unexpected failure to add the payment " + message.getLong(PaymentScheduler.PAYMENT_ID) + ": " + e.getMessage());
			}
		} else {
			try {
				SchedulerEngineUtil.unschedule(message.getString(SchedulerEngine.JOB_NAME), 
											   message.getString(SchedulerEngine.GROUP_NAME), 
											   StorageType.PERSISTED);

				MAIL_LOGGER.error("Fail to add payment " + message.getLong(PaymentScheduler.PAYMENT_ID) +
								  " after " + PaymentScheduler.RETRIES + " retries");			
			} catch (SchedulerException e) {
				MAIL_LOGGER.error("Fail to unschedule the PaymentRetryJob for " + 
							 	  message.getLong(PaymentScheduler.PAYMENT_ID) + 
							 	  " payment after " + count + " retries", e);
			}
		}

		LOGGER.info("Finish to add the invoice " + message.getLong(PaymentScheduler.PAYMENT_ID));
	}

}
