package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.NoSuchGuestException;
import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.model.impl.GuestImpl;
import br.com.atilo.jcondo.booking.model.impl.GuestModelImpl;
import br.com.atilo.jcondo.booking.service.persistence.GuestPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * The persistence implementation for the guest service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see GuestPersistence
 * @see GuestUtil
 * @generated
 */
public class GuestPersistenceImpl extends BasePersistenceImpl<Guest>
    implements GuestPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link GuestUtil} to access the guest persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = GuestImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(GuestModelImpl.ENTITY_CACHE_ENABLED,
            GuestModelImpl.FINDER_CACHE_ENABLED, GuestImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(GuestModelImpl.ENTITY_CACHE_ENABLED,
            GuestModelImpl.FINDER_CACHE_ENABLED, GuestImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(GuestModelImpl.ENTITY_CACHE_ENABLED,
            GuestModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKING = new FinderPath(GuestModelImpl.ENTITY_CACHE_ENABLED,
            GuestModelImpl.FINDER_CACHE_ENABLED, GuestImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBooking",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING =
        new FinderPath(GuestModelImpl.ENTITY_CACHE_ENABLED,
            GuestModelImpl.FINDER_CACHE_ENABLED, GuestImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBooking",
            new String[] { Long.class.getName() },
            GuestModelImpl.BOOKINGID_COLUMN_BITMASK |
            GuestModelImpl.NAME_COLUMN_BITMASK |
            GuestModelImpl.SURNAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_BOOKING = new FinderPath(GuestModelImpl.ENTITY_CACHE_ENABLED,
            GuestModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBooking",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_BOOKING_BOOKINGID_2 = "guest.bookingId = ?";
    private static final String _SQL_SELECT_GUEST = "SELECT guest FROM Guest guest";
    private static final String _SQL_SELECT_GUEST_WHERE = "SELECT guest FROM Guest guest WHERE ";
    private static final String _SQL_COUNT_GUEST = "SELECT COUNT(guest) FROM Guest guest";
    private static final String _SQL_COUNT_GUEST_WHERE = "SELECT COUNT(guest) FROM Guest guest WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "guest.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Guest exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Guest exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(GuestPersistenceImpl.class);
    private static Guest _nullGuest = new GuestImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Guest> toCacheModel() {
                return _nullGuestCacheModel;
            }
        };

    private static CacheModel<Guest> _nullGuestCacheModel = new CacheModel<Guest>() {
            @Override
            public Guest toEntityModel() {
                return _nullGuest;
            }
        };

    public GuestPersistenceImpl() {
        setModelClass(Guest.class);
    }

    /**
     * Returns all the guests where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @return the matching guests
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Guest> findByBooking(long bookingId) throws SystemException {
        return findByBooking(bookingId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the guests where bookingId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param bookingId the booking ID
     * @param start the lower bound of the range of guests
     * @param end the upper bound of the range of guests (not inclusive)
     * @return the range of matching guests
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Guest> findByBooking(long bookingId, int start, int end)
        throws SystemException {
        return findByBooking(bookingId, start, end, null);
    }

    /**
     * Returns an ordered range of all the guests where bookingId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param bookingId the booking ID
     * @param start the lower bound of the range of guests
     * @param end the upper bound of the range of guests (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching guests
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Guest> findByBooking(long bookingId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING;
            finderArgs = new Object[] { bookingId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKING;
            finderArgs = new Object[] { bookingId, start, end, orderByComparator };
        }

        List<Guest> list = (List<Guest>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Guest guest : list) {
                if ((bookingId != guest.getBookingId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_GUEST_WHERE);

            query.append(_FINDER_COLUMN_BOOKING_BOOKINGID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(GuestModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(bookingId);

                if (!pagination) {
                    list = (List<Guest>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Guest>(list);
                } else {
                    list = (List<Guest>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first guest in the ordered set where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching guest
     * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a matching guest could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest findByBooking_First(long bookingId,
        OrderByComparator orderByComparator)
        throws NoSuchGuestException, SystemException {
        Guest guest = fetchByBooking_First(bookingId, orderByComparator);

        if (guest != null) {
            return guest;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("bookingId=");
        msg.append(bookingId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchGuestException(msg.toString());
    }

    /**
     * Returns the first guest in the ordered set where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching guest, or <code>null</code> if a matching guest could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest fetchByBooking_First(long bookingId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Guest> list = findByBooking(bookingId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last guest in the ordered set where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching guest
     * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a matching guest could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest findByBooking_Last(long bookingId,
        OrderByComparator orderByComparator)
        throws NoSuchGuestException, SystemException {
        Guest guest = fetchByBooking_Last(bookingId, orderByComparator);

        if (guest != null) {
            return guest;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("bookingId=");
        msg.append(bookingId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchGuestException(msg.toString());
    }

    /**
     * Returns the last guest in the ordered set where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching guest, or <code>null</code> if a matching guest could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest fetchByBooking_Last(long bookingId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByBooking(bookingId);

        if (count == 0) {
            return null;
        }

        List<Guest> list = findByBooking(bookingId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the guests before and after the current guest in the ordered set where bookingId = &#63;.
     *
     * @param guestId the primary key of the current guest
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next guest
     * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest[] findByBooking_PrevAndNext(long guestId, long bookingId,
        OrderByComparator orderByComparator)
        throws NoSuchGuestException, SystemException {
        Guest guest = findByPrimaryKey(guestId);

        Session session = null;

        try {
            session = openSession();

            Guest[] array = new GuestImpl[3];

            array[0] = getByBooking_PrevAndNext(session, guest, bookingId,
                    orderByComparator, true);

            array[1] = guest;

            array[2] = getByBooking_PrevAndNext(session, guest, bookingId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Guest getByBooking_PrevAndNext(Session session, Guest guest,
        long bookingId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_GUEST_WHERE);

        query.append(_FINDER_COLUMN_BOOKING_BOOKINGID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(GuestModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(bookingId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(guest);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Guest> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the guests where bookingId = &#63; from the database.
     *
     * @param bookingId the booking ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByBooking(long bookingId) throws SystemException {
        for (Guest guest : findByBooking(bookingId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(guest);
        }
    }

    /**
     * Returns the number of guests where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @return the number of matching guests
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByBooking(long bookingId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_BOOKING;

        Object[] finderArgs = new Object[] { bookingId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_GUEST_WHERE);

            query.append(_FINDER_COLUMN_BOOKING_BOOKINGID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(bookingId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the guest in the entity cache if it is enabled.
     *
     * @param guest the guest
     */
    @Override
    public void cacheResult(Guest guest) {
        EntityCacheUtil.putResult(GuestModelImpl.ENTITY_CACHE_ENABLED,
            GuestImpl.class, guest.getPrimaryKey(), guest);

        guest.resetOriginalValues();
    }

    /**
     * Caches the guests in the entity cache if it is enabled.
     *
     * @param guests the guests
     */
    @Override
    public void cacheResult(List<Guest> guests) {
        for (Guest guest : guests) {
            if (EntityCacheUtil.getResult(GuestModelImpl.ENTITY_CACHE_ENABLED,
                        GuestImpl.class, guest.getPrimaryKey()) == null) {
                cacheResult(guest);
            } else {
                guest.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all guests.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(GuestImpl.class.getName());
        }

        EntityCacheUtil.clearCache(GuestImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the guest.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Guest guest) {
        EntityCacheUtil.removeResult(GuestModelImpl.ENTITY_CACHE_ENABLED,
            GuestImpl.class, guest.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Guest> guests) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Guest guest : guests) {
            EntityCacheUtil.removeResult(GuestModelImpl.ENTITY_CACHE_ENABLED,
                GuestImpl.class, guest.getPrimaryKey());
        }
    }

    /**
     * Creates a new guest with the primary key. Does not add the guest to the database.
     *
     * @param guestId the primary key for the new guest
     * @return the new guest
     */
    @Override
    public Guest create(long guestId) {
        Guest guest = new GuestImpl();

        guest.setNew(true);
        guest.setPrimaryKey(guestId);

        return guest;
    }

    /**
     * Removes the guest with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param guestId the primary key of the guest
     * @return the guest that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest remove(long guestId)
        throws NoSuchGuestException, SystemException {
        return remove((Serializable) guestId);
    }

    /**
     * Removes the guest with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the guest
     * @return the guest that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest remove(Serializable primaryKey)
        throws NoSuchGuestException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Guest guest = (Guest) session.get(GuestImpl.class, primaryKey);

            if (guest == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchGuestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(guest);
        } catch (NoSuchGuestException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Guest removeImpl(Guest guest) throws SystemException {
        guest = toUnwrappedModel(guest);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(guest)) {
                guest = (Guest) session.get(GuestImpl.class,
                        guest.getPrimaryKeyObj());
            }

            if (guest != null) {
                session.delete(guest);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (guest != null) {
            clearCache(guest);
        }

        return guest;
    }

    @Override
    public Guest updateImpl(br.com.atilo.jcondo.booking.model.Guest guest)
        throws SystemException {
        guest = toUnwrappedModel(guest);

        boolean isNew = guest.isNew();

        GuestModelImpl guestModelImpl = (GuestModelImpl) guest;

        Session session = null;

        try {
            session = openSession();

            if (guest.isNew()) {
                session.save(guest);

                guest.setNew(false);
            } else {
                session.merge(guest);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !GuestModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((guestModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        guestModelImpl.getOriginalBookingId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKING, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING,
                    args);

                args = new Object[] { guestModelImpl.getBookingId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKING, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING,
                    args);
            }
        }

        EntityCacheUtil.putResult(GuestModelImpl.ENTITY_CACHE_ENABLED,
            GuestImpl.class, guest.getPrimaryKey(), guest);

        return guest;
    }

    protected Guest toUnwrappedModel(Guest guest) {
        if (guest instanceof GuestImpl) {
            return guest;
        }

        GuestImpl guestImpl = new GuestImpl();

        guestImpl.setNew(guest.isNew());
        guestImpl.setPrimaryKey(guest.getPrimaryKey());

        guestImpl.setGuestId(guest.getGuestId());
        guestImpl.setBookingId(guest.getBookingId());
        guestImpl.setName(guest.getName());
        guestImpl.setSurname(guest.getSurname());
        guestImpl.setCheckedIn(guest.isCheckedIn());
        guestImpl.setOprDate(guest.getOprDate());
        guestImpl.setOprUser(guest.getOprUser());

        return guestImpl;
    }

    /**
     * Returns the guest with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the guest
     * @return the guest
     * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest findByPrimaryKey(Serializable primaryKey)
        throws NoSuchGuestException, SystemException {
        Guest guest = fetchByPrimaryKey(primaryKey);

        if (guest == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchGuestException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return guest;
    }

    /**
     * Returns the guest with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchGuestException} if it could not be found.
     *
     * @param guestId the primary key of the guest
     * @return the guest
     * @throws br.com.atilo.jcondo.booking.NoSuchGuestException if a guest with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest findByPrimaryKey(long guestId)
        throws NoSuchGuestException, SystemException {
        return findByPrimaryKey((Serializable) guestId);
    }

    /**
     * Returns the guest with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the guest
     * @return the guest, or <code>null</code> if a guest with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Guest guest = (Guest) EntityCacheUtil.getResult(GuestModelImpl.ENTITY_CACHE_ENABLED,
                GuestImpl.class, primaryKey);

        if (guest == _nullGuest) {
            return null;
        }

        if (guest == null) {
            Session session = null;

            try {
                session = openSession();

                guest = (Guest) session.get(GuestImpl.class, primaryKey);

                if (guest != null) {
                    cacheResult(guest);
                } else {
                    EntityCacheUtil.putResult(GuestModelImpl.ENTITY_CACHE_ENABLED,
                        GuestImpl.class, primaryKey, _nullGuest);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(GuestModelImpl.ENTITY_CACHE_ENABLED,
                    GuestImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return guest;
    }

    /**
     * Returns the guest with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param guestId the primary key of the guest
     * @return the guest, or <code>null</code> if a guest with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Guest fetchByPrimaryKey(long guestId) throws SystemException {
        return fetchByPrimaryKey((Serializable) guestId);
    }

    /**
     * Returns all the guests.
     *
     * @return the guests
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Guest> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the guests.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of guests
     * @param end the upper bound of the range of guests (not inclusive)
     * @return the range of guests
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Guest> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the guests.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.GuestModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of guests
     * @param end the upper bound of the range of guests (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of guests
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Guest> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Guest> list = (List<Guest>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_GUEST);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_GUEST;

                if (pagination) {
                    sql = sql.concat(GuestModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Guest>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Guest>(list);
                } else {
                    list = (List<Guest>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the guests from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Guest guest : findAll()) {
            remove(guest);
        }
    }

    /**
     * Returns the number of guests.
     *
     * @return the number of guests
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_GUEST);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Initializes the guest persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.booking.model.Guest")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Guest>> listenersList = new ArrayList<ModelListener<Guest>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Guest>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(GuestImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
