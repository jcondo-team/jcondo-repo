/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;


import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException;
import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.payment.PaymentMode;
import br.com.atilo.jcondo.booking.payment.PaymentProvider;
import br.com.atilo.jcondo.booking.payment.PaymentProviderId;
import br.com.atilo.jcondo.booking.service.base.BookingInvoiceLocalServiceBaseImpl;

/**
 * The implementation of the booking invoice local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.BookingInvoiceLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.BookingInvoiceLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil
 */
public class BookingInvoiceLocalServiceImpl	extends BookingInvoiceLocalServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil} to access the booking invoice local service.
	 */
	private static final Logger LOGGER = Logger.getLogger(BookingInvoiceLocalServiceImpl.class);
	
	private static final int DISCHARGE_DAYS = 2;

	@BeanReference(name="paymentProvider")
	private PaymentProvider provider;

	/**
	 * Se o prazo para 
	 * @param booking
	 * @return
	 */
	private Date calculateDueDate(Booking booking) {
		if (provider.getPaymentMode() == PaymentMode.PRE_PAID) {
			Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			Date deadLine = bookingLocalService.getDeadlineDate(booking.getBeginDate());
			Date overdue = DateUtils.addDays(deadLine, -DISCHARGE_DAYS);
			
			if (today.after(overdue)) {
				return today;
			} else {
				return overdue;
			}
		} else {
			int bookingDay = DateUtils.toCalendar(booking.getBeginDate()).get(Calendar.DAY_OF_MONTH);
			Date dueDate;

			if (bookingDay <= 20) {
				dueDate = DateUtils.addMonths(booking.getBeginDate(), 1);
			} else {
				dueDate = DateUtils.addMonths(booking.getBeginDate(), 2);
			}

			return DateUtils.setDays(dueDate, 10);
		}
	}
	
	public BookingInvoice addBookingInvoice(Booking booking) throws PortalException, SystemException {
		BookingInvoice invoice = createBookingInvoice(counterLocalService.increment(BookingInvoice.class.getName()));
		invoice.setBookingId(booking.getId());
		invoice.setProviderId(provider.getKey().ordinal());
		invoice.setAmount(booking.getRoom().getPrice());
		invoice.setDate(calculateDueDate(booking));
		return addBookingInvoice(invoice);
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public BookingInvoice addBookingInvoice(BookingInvoice invoice) throws SystemException {
		invoice.setOprDate(new Date());

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		if (sc == null) {
			try {
				invoice.setOprUser(UserLocalServiceUtil.getDefaultUserId(PortalUtil.getDefaultCompanyId()));
			} catch (PortalException e) {
				invoice.setOprUser(0);
				e.printStackTrace();
			}
		} else {
			invoice.setOprUser(sc.getUserId());
		}

		provider.addInvoice(invoice);

		return super.addBookingInvoice(invoice);
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public BookingInvoice updateBookingInvoice(BookingInvoice invoice, boolean merge) throws SystemException {
		invoice.setOprDate(new Date());

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		if (sc == null) {
			try {
				invoice.setOprUser(UserLocalServiceUtil.getDefaultUserId(PortalUtil.getDefaultCompanyId()));
			} catch (PortalException e) {
				invoice.setOprUser(0);
				e.printStackTrace();
			}
		} else {
			invoice.setOprUser(sc.getUserId());
		}

		return super.updateBookingInvoice(invoice);
	}

	public List<BookingInvoice> getByBooking(long bookingId) throws SystemException {
		return bookingInvoicePersistence.findByBooking(bookingId);
	}

	public boolean isPrePaidMode(long invoiceId) throws PortalException, SystemException {
		try {
			BookingInvoice invoice = getBookingInvoice(invoiceId);
			return PaymentProviderId.getPaymentProvider(invoice.getProviderId()).getPaymentMode() == PaymentMode.PRE_PAID;
		} catch (NoSuchBookingInvoiceException e) {
			return provider.getPaymentMode() == PaymentMode.PRE_PAID;
		} catch (Exception e) {
			LOGGER.error("Failure on isPrePaidMode method", e);
		}

		return false;
	}	

}