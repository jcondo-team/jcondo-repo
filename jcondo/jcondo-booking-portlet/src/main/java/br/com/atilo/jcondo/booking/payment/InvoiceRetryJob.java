package br.com.atilo.jcondo.booking.payment;

import org.apache.log4j.Logger;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;

import com.liferay.portal.kernel.messaging.Message;
import com.liferay.portal.kernel.messaging.MessageListener;
import com.liferay.portal.kernel.messaging.MessageListenerException;
import com.liferay.portal.kernel.scheduler.SchedulerEngine;
import com.liferay.portal.kernel.scheduler.SchedulerEngineUtil;
import com.liferay.portal.kernel.scheduler.StorageType;

public class InvoiceRetryJob implements MessageListener {

	private static final Logger LOGGER = Logger.getLogger(InvoiceRetryJob.class);

	private static final Logger MAIL_LOGGER = Logger.getLogger("MAILER");
	
	public void receive(Message message) throws MessageListenerException {
		LOGGER.info("Starting to add the invoice " + message.getLong(InvoiceScheduler.BOOKING_ID));

		int count = message.getInteger(InvoiceScheduler.RETRY_COUNT);

		if (count < InvoiceScheduler.RETRIES) {
			Booking booking = null;
			try {
				try {
					booking = BookingLocalServiceUtil.getBooking(message.getLong(InvoiceScheduler.BOOKING_ID));
	
					LOGGER.info("Retry number: " + count);
	
					BookingInvoiceLocalServiceUtil.addBookingInvoice(booking);
	
					LOGGER.info("Added the invoice for booking " + message.getLong(InvoiceScheduler.BOOKING_ID) + " successfully");
	
					SchedulerEngineUtil.unschedule(message.getString(SchedulerEngine.JOB_NAME), 
												   message.getString(SchedulerEngine.GROUP_NAME), 
												   StorageType.PERSISTED);
					LOGGER.info("Unscheduled the InvoiceRetryJob for booking " + message.getLong(InvoiceScheduler.BOOKING_ID) + " successfully");
				} catch (PaymentProviderException e) {
					LOGGER.info("Fail to add the invoice for booking " + message.getLong(InvoiceScheduler.BOOKING_ID) + ": " + e.getMessage());
					InvoiceScheduler.reschedule(booking);
				}
			} catch (Exception e) {
				MAIL_LOGGER.error("Unexpected failure to add the invoice for booking " + message.getLong(InvoiceScheduler.BOOKING_ID) + ": " + e.getMessage());
			}
		} else {
			try {
				SchedulerEngineUtil.unschedule(message.getString(SchedulerEngine.JOB_NAME), 
											   message.getString(SchedulerEngine.GROUP_NAME), 
											   StorageType.PERSISTED);

				MAIL_LOGGER.error("Fail to add invoice for booking " + message.getLong(InvoiceScheduler.BOOKING_ID) +
								  " after " + InvoiceScheduler.RETRIES + " retries");
			} catch (Exception e) {
				MAIL_LOGGER.error("Fail to unschedule the InvoiceRetryJob for " + 
								  message.getLong(InvoiceScheduler.BOOKING_ID) + 
								  " booking after " + count + " retries", e);
			}
		}
		
		LOGGER.info("Finish to add the invoice " + message.getLong(InvoiceScheduler.BOOKING_ID));
	}

}
