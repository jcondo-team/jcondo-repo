package br.com.atilo.jcondo.booking.model.impl;

import br.com.atilo.jcondo.booking.model.BookingInvoice;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing BookingInvoice in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see BookingInvoice
 * @generated
 */
public class BookingInvoiceCacheModel implements CacheModel<BookingInvoice>,
    Externalizable {
    public long id;
    public long bookingId;
    public long providerId;
    public String code;
    public double amount;
    public double tax;
    public long date;
    public String link;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(21);

        sb.append("{id=");
        sb.append(id);
        sb.append(", bookingId=");
        sb.append(bookingId);
        sb.append(", providerId=");
        sb.append(providerId);
        sb.append(", code=");
        sb.append(code);
        sb.append(", amount=");
        sb.append(amount);
        sb.append(", tax=");
        sb.append(tax);
        sb.append(", date=");
        sb.append(date);
        sb.append(", link=");
        sb.append(link);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public BookingInvoice toEntityModel() {
        BookingInvoiceImpl bookingInvoiceImpl = new BookingInvoiceImpl();

        bookingInvoiceImpl.setId(id);
        bookingInvoiceImpl.setBookingId(bookingId);
        bookingInvoiceImpl.setProviderId(providerId);

        if (code == null) {
            bookingInvoiceImpl.setCode(StringPool.BLANK);
        } else {
            bookingInvoiceImpl.setCode(code);
        }

        bookingInvoiceImpl.setAmount(amount);
        bookingInvoiceImpl.setTax(tax);

        if (date == Long.MIN_VALUE) {
            bookingInvoiceImpl.setDate(null);
        } else {
            bookingInvoiceImpl.setDate(new Date(date));
        }

        if (link == null) {
            bookingInvoiceImpl.setLink(StringPool.BLANK);
        } else {
            bookingInvoiceImpl.setLink(link);
        }

        if (oprDate == Long.MIN_VALUE) {
            bookingInvoiceImpl.setOprDate(null);
        } else {
            bookingInvoiceImpl.setOprDate(new Date(oprDate));
        }

        bookingInvoiceImpl.setOprUser(oprUser);

        bookingInvoiceImpl.resetOriginalValues();

        return bookingInvoiceImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        bookingId = objectInput.readLong();
        providerId = objectInput.readLong();
        code = objectInput.readUTF();
        amount = objectInput.readDouble();
        tax = objectInput.readDouble();
        date = objectInput.readLong();
        link = objectInput.readUTF();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(bookingId);
        objectOutput.writeLong(providerId);

        if (code == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(code);
        }

        objectOutput.writeDouble(amount);
        objectOutput.writeDouble(tax);
        objectOutput.writeLong(date);

        if (link == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(link);
        }

        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
