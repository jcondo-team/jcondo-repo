package br.com.atilo.jcondo.booking.model.impl;

import br.com.atilo.jcondo.booking.model.BookingNote;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing BookingNote in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see BookingNote
 * @generated
 */
public class BookingNoteCacheModel implements CacheModel<BookingNote>,
    Externalizable {
    public long id;
    public String text;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{id=");
        sb.append(id);
        sb.append(", text=");
        sb.append(text);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public BookingNote toEntityModel() {
        BookingNoteImpl bookingNoteImpl = new BookingNoteImpl();

        bookingNoteImpl.setId(id);

        if (text == null) {
            bookingNoteImpl.setText(StringPool.BLANK);
        } else {
            bookingNoteImpl.setText(text);
        }

        if (oprDate == Long.MIN_VALUE) {
            bookingNoteImpl.setOprDate(null);
        } else {
            bookingNoteImpl.setOprDate(new Date(oprDate));
        }

        bookingNoteImpl.setOprUser(oprUser);

        bookingNoteImpl.resetOriginalValues();

        return bookingNoteImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        text = objectInput.readUTF();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);

        if (text == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(text);
        }

        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
