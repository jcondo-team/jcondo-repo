/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.service.base.GuestServiceBaseImpl;

/**
 * The implementation of the guest remote service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.GuestService} interface.
 *
 * <p>
 * This is a remote service. Methods of this service are expected to have security checks based on the propagated JAAS credentials because this service can be accessed remotely.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.GuestServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.GuestServiceUtil
 */
public class GuestServiceImpl extends GuestServiceBaseImpl {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.GuestServiceUtil} to access the guest remote service.
	 */

	public Guest addGuest(long bookingId, String name, String surname) throws PortalException, SystemException {
		return guestLocalService.addGuest(bookingId, name, surname);
	}

	public Guest updateGuest(long guestId, long bookingId, String name, String surname, boolean checkedIn) throws PortalException, SystemException {
		return guestLocalService.updateGuest(guestId, bookingId, name, surname, checkedIn);
	}

	public Guest deleteGuest(long guestId) throws PortalException, SystemException {
		return guestLocalService.deleteGuest(guestId);
	}

	public List<Guest> getBookingGuests(long bookingId) throws PortalException, SystemException {
		return guestLocalService.getBookingGuests(bookingId);
	}

}