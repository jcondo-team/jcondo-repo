package br.com.atilo.jcondo.booking.payment.boletofacil;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Charge {

	/**
	 * C�digo �nico de identifica��o da cobran�a no Boleto F�cil
	 */
	private String code;
	
	/**
	 * C�digo de refer�ncia da cobran�a em seu sistema
	 */
	private String reference;
	
	/**
	 * Data de vencimento do boleto ou parcela
	 */
	private Date dueDate;
	
	/**
	 * Link para visualiza��o/download do boleto ou carnet
	 */
	private String link;
	
	/**
	 * Linha digit�vel para pagamento online
	 */
	private String payNumber;

	/**
	 * Valor cobrado
	 */
	private double amount;
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(code).append(dueDate).append(link).append(payNumber).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Charge) == false) {
			return false;
		}
		Charge rhs = ((Charge) other);
		return new EqualsBuilder().append(code, rhs.code).append(dueDate, rhs.dueDate).append(link, rhs.link).append(payNumber, rhs.payNumber).isEquals();
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getReference() {
		return reference;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getPayNumber() {
		return payNumber;
	}

	public void setPayNumber(String payNumber) {
		this.payNumber = payNumber;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

}