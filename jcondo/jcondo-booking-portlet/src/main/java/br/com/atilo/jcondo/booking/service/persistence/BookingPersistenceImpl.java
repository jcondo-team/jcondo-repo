package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.NoSuchBookingException;
import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.impl.BookingImpl;
import br.com.atilo.jcondo.booking.model.impl.BookingModelImpl;
import br.com.atilo.jcondo.booking.service.persistence.BookingPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the booking service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingPersistence
 * @see BookingUtil
 * @generated
 */
public class BookingPersistenceImpl extends BasePersistenceImpl<Booking>
    implements BookingPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link BookingUtil} to access the booking persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = BookingImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingModelImpl.FINDER_CACHE_ENABLED, BookingImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingModelImpl.FINDER_CACHE_ENABLED, BookingImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_DOMAIN = new FinderPath(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingModelImpl.FINDER_CACHE_ENABLED, BookingImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByDomain",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN =
        new FinderPath(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingModelImpl.FINDER_CACHE_ENABLED, BookingImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByDomain",
            new String[] { Long.class.getName() },
            BookingModelImpl.DOMAINID_COLUMN_BITMASK |
            BookingModelImpl.BEGINDATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_DOMAIN = new FinderPath(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByDomain",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_DOMAIN_DOMAINID_2 = "booking.domainId = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON = new FinderPath(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingModelImpl.FINDER_CACHE_ENABLED, BookingImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByPerson",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON =
        new FinderPath(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingModelImpl.FINDER_CACHE_ENABLED, BookingImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByPerson",
            new String[] { Long.class.getName() },
            BookingModelImpl.PERSONID_COLUMN_BITMASK |
            BookingModelImpl.BEGINDATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_PERSON = new FinderPath(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByPerson",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_PERSON_PERSONID_2 = "booking.personId = ?";
    private static final String _SQL_SELECT_BOOKING = "SELECT booking FROM Booking booking";
    private static final String _SQL_SELECT_BOOKING_WHERE = "SELECT booking FROM Booking booking WHERE ";
    private static final String _SQL_COUNT_BOOKING = "SELECT COUNT(booking) FROM Booking booking";
    private static final String _SQL_COUNT_BOOKING_WHERE = "SELECT COUNT(booking) FROM Booking booking WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "booking.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Booking exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Booking exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(BookingPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Booking _nullBooking = new BookingImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Booking> toCacheModel() {
                return _nullBookingCacheModel;
            }
        };

    private static CacheModel<Booking> _nullBookingCacheModel = new CacheModel<Booking>() {
            @Override
            public Booking toEntityModel() {
                return _nullBooking;
            }
        };

    public BookingPersistenceImpl() {
        setModelClass(Booking.class);
    }

    /**
     * Returns all the bookings where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @return the matching bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Booking> findByDomain(long domainId) throws SystemException {
        return findByDomain(domainId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the bookings where domainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param domainId the domain ID
     * @param start the lower bound of the range of bookings
     * @param end the upper bound of the range of bookings (not inclusive)
     * @return the range of matching bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Booking> findByDomain(long domainId, int start, int end)
        throws SystemException {
        return findByDomain(domainId, start, end, null);
    }

    /**
     * Returns an ordered range of all the bookings where domainId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param domainId the domain ID
     * @param start the lower bound of the range of bookings
     * @param end the upper bound of the range of bookings (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Booking> findByDomain(long domainId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN;
            finderArgs = new Object[] { domainId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_DOMAIN;
            finderArgs = new Object[] { domainId, start, end, orderByComparator };
        }

        List<Booking> list = (List<Booking>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Booking booking : list) {
                if ((domainId != booking.getDomainId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_BOOKING_WHERE);

            query.append(_FINDER_COLUMN_DOMAIN_DOMAINID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(BookingModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                if (!pagination) {
                    list = (List<Booking>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Booking>(list);
                } else {
                    list = (List<Booking>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first booking in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching booking
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a matching booking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking findByDomain_First(long domainId,
        OrderByComparator orderByComparator)
        throws NoSuchBookingException, SystemException {
        Booking booking = fetchByDomain_First(domainId, orderByComparator);

        if (booking != null) {
            return booking;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("domainId=");
        msg.append(domainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchBookingException(msg.toString());
    }

    /**
     * Returns the first booking in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching booking, or <code>null</code> if a matching booking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking fetchByDomain_First(long domainId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Booking> list = findByDomain(domainId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last booking in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching booking
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a matching booking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking findByDomain_Last(long domainId,
        OrderByComparator orderByComparator)
        throws NoSuchBookingException, SystemException {
        Booking booking = fetchByDomain_Last(domainId, orderByComparator);

        if (booking != null) {
            return booking;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("domainId=");
        msg.append(domainId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchBookingException(msg.toString());
    }

    /**
     * Returns the last booking in the ordered set where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching booking, or <code>null</code> if a matching booking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking fetchByDomain_Last(long domainId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByDomain(domainId);

        if (count == 0) {
            return null;
        }

        List<Booking> list = findByDomain(domainId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the bookings before and after the current booking in the ordered set where domainId = &#63;.
     *
     * @param id the primary key of the current booking
     * @param domainId the domain ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next booking
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking[] findByDomain_PrevAndNext(long id, long domainId,
        OrderByComparator orderByComparator)
        throws NoSuchBookingException, SystemException {
        Booking booking = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Booking[] array = new BookingImpl[3];

            array[0] = getByDomain_PrevAndNext(session, booking, domainId,
                    orderByComparator, true);

            array[1] = booking;

            array[2] = getByDomain_PrevAndNext(session, booking, domainId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Booking getByDomain_PrevAndNext(Session session, Booking booking,
        long domainId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_BOOKING_WHERE);

        query.append(_FINDER_COLUMN_DOMAIN_DOMAINID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(BookingModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(domainId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(booking);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Booking> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the bookings where domainId = &#63; from the database.
     *
     * @param domainId the domain ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByDomain(long domainId) throws SystemException {
        for (Booking booking : findByDomain(domainId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(booking);
        }
    }

    /**
     * Returns the number of bookings where domainId = &#63;.
     *
     * @param domainId the domain ID
     * @return the number of matching bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByDomain(long domainId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_DOMAIN;

        Object[] finderArgs = new Object[] { domainId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_BOOKING_WHERE);

            query.append(_FINDER_COLUMN_DOMAIN_DOMAINID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(domainId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the bookings where personId = &#63;.
     *
     * @param personId the person ID
     * @return the matching bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Booking> findByPerson(long personId) throws SystemException {
        return findByPerson(personId, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the bookings where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of bookings
     * @param end the upper bound of the range of bookings (not inclusive)
     * @return the range of matching bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Booking> findByPerson(long personId, int start, int end)
        throws SystemException {
        return findByPerson(personId, start, end, null);
    }

    /**
     * Returns an ordered range of all the bookings where personId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param personId the person ID
     * @param start the lower bound of the range of bookings
     * @param end the upper bound of the range of bookings (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Booking> findByPerson(long personId, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_PERSON;
            finderArgs = new Object[] { personId, start, end, orderByComparator };
        }

        List<Booking> list = (List<Booking>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Booking booking : list) {
                if ((personId != booking.getPersonId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_BOOKING_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(BookingModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                if (!pagination) {
                    list = (List<Booking>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Booking>(list);
                } else {
                    list = (List<Booking>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first booking in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching booking
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a matching booking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking findByPerson_First(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchBookingException, SystemException {
        Booking booking = fetchByPerson_First(personId, orderByComparator);

        if (booking != null) {
            return booking;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchBookingException(msg.toString());
    }

    /**
     * Returns the first booking in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching booking, or <code>null</code> if a matching booking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking fetchByPerson_First(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        List<Booking> list = findByPerson(personId, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last booking in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching booking
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a matching booking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking findByPerson_Last(long personId,
        OrderByComparator orderByComparator)
        throws NoSuchBookingException, SystemException {
        Booking booking = fetchByPerson_Last(personId, orderByComparator);

        if (booking != null) {
            return booking;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("personId=");
        msg.append(personId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchBookingException(msg.toString());
    }

    /**
     * Returns the last booking in the ordered set where personId = &#63;.
     *
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching booking, or <code>null</code> if a matching booking could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking fetchByPerson_Last(long personId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByPerson(personId);

        if (count == 0) {
            return null;
        }

        List<Booking> list = findByPerson(personId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the bookings before and after the current booking in the ordered set where personId = &#63;.
     *
     * @param id the primary key of the current booking
     * @param personId the person ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next booking
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking[] findByPerson_PrevAndNext(long id, long personId,
        OrderByComparator orderByComparator)
        throws NoSuchBookingException, SystemException {
        Booking booking = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Booking[] array = new BookingImpl[3];

            array[0] = getByPerson_PrevAndNext(session, booking, personId,
                    orderByComparator, true);

            array[1] = booking;

            array[2] = getByPerson_PrevAndNext(session, booking, personId,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Booking getByPerson_PrevAndNext(Session session, Booking booking,
        long personId, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_BOOKING_WHERE);

        query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(BookingModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(personId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(booking);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Booking> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the bookings where personId = &#63; from the database.
     *
     * @param personId the person ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByPerson(long personId) throws SystemException {
        for (Booking booking : findByPerson(personId, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(booking);
        }
    }

    /**
     * Returns the number of bookings where personId = &#63;.
     *
     * @param personId the person ID
     * @return the number of matching bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByPerson(long personId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_PERSON;

        Object[] finderArgs = new Object[] { personId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_BOOKING_WHERE);

            query.append(_FINDER_COLUMN_PERSON_PERSONID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(personId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the booking in the entity cache if it is enabled.
     *
     * @param booking the booking
     */
    @Override
    public void cacheResult(Booking booking) {
        EntityCacheUtil.putResult(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingImpl.class, booking.getPrimaryKey(), booking);

        booking.resetOriginalValues();
    }

    /**
     * Caches the bookings in the entity cache if it is enabled.
     *
     * @param bookings the bookings
     */
    @Override
    public void cacheResult(List<Booking> bookings) {
        for (Booking booking : bookings) {
            if (EntityCacheUtil.getResult(
                        BookingModelImpl.ENTITY_CACHE_ENABLED,
                        BookingImpl.class, booking.getPrimaryKey()) == null) {
                cacheResult(booking);
            } else {
                booking.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all bookings.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(BookingImpl.class.getName());
        }

        EntityCacheUtil.clearCache(BookingImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the booking.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Booking booking) {
        EntityCacheUtil.removeResult(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingImpl.class, booking.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<Booking> bookings) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Booking booking : bookings) {
            EntityCacheUtil.removeResult(BookingModelImpl.ENTITY_CACHE_ENABLED,
                BookingImpl.class, booking.getPrimaryKey());
        }
    }

    /**
     * Creates a new booking with the primary key. Does not add the booking to the database.
     *
     * @param id the primary key for the new booking
     * @return the new booking
     */
    @Override
    public Booking create(long id) {
        Booking booking = new BookingImpl();

        booking.setNew(true);
        booking.setPrimaryKey(id);

        return booking;
    }

    /**
     * Removes the booking with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the booking
     * @return the booking that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking remove(long id)
        throws NoSuchBookingException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the booking with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the booking
     * @return the booking that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking remove(Serializable primaryKey)
        throws NoSuchBookingException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Booking booking = (Booking) session.get(BookingImpl.class,
                    primaryKey);

            if (booking == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchBookingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(booking);
        } catch (NoSuchBookingException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Booking removeImpl(Booking booking) throws SystemException {
        booking = toUnwrappedModel(booking);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(booking)) {
                booking = (Booking) session.get(BookingImpl.class,
                        booking.getPrimaryKeyObj());
            }

            if (booking != null) {
                session.delete(booking);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (booking != null) {
            clearCache(booking);
        }

        return booking;
    }

    @Override
    public Booking updateImpl(br.com.atilo.jcondo.booking.model.Booking booking)
        throws SystemException {
        booking = toUnwrappedModel(booking);

        boolean isNew = booking.isNew();

        BookingModelImpl bookingModelImpl = (BookingModelImpl) booking;

        Session session = null;

        try {
            session = openSession();

            if (booking.isNew()) {
                session.save(booking);

                booking.setNew(false);
            } else {
                session.merge(booking);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !BookingModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((bookingModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        bookingModelImpl.getOriginalDomainId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAIN, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN,
                    args);

                args = new Object[] { bookingModelImpl.getDomainId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_DOMAIN, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_DOMAIN,
                    args);
            }

            if ((bookingModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        bookingModelImpl.getOriginalPersonId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);

                args = new Object[] { bookingModelImpl.getPersonId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_PERSON, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_PERSON,
                    args);
            }
        }

        EntityCacheUtil.putResult(BookingModelImpl.ENTITY_CACHE_ENABLED,
            BookingImpl.class, booking.getPrimaryKey(), booking);

        return booking;
    }

    protected Booking toUnwrappedModel(Booking booking) {
        if (booking instanceof BookingImpl) {
            return booking;
        }

        BookingImpl bookingImpl = new BookingImpl();

        bookingImpl.setNew(booking.isNew());
        bookingImpl.setPrimaryKey(booking.getPrimaryKey());

        bookingImpl.setId(booking.getId());
        bookingImpl.setRoomId(booking.getRoomId());
        bookingImpl.setDomainId(booking.getDomainId());
        bookingImpl.setPersonId(booking.getPersonId());
        bookingImpl.setBeginDate(booking.getBeginDate());
        bookingImpl.setEndDate(booking.getEndDate());
        bookingImpl.setStatusId(booking.getStatusId());
        bookingImpl.setNoteId(booking.getNoteId());
        bookingImpl.setOprDate(booking.getOprDate());
        bookingImpl.setOprUser(booking.getOprUser());

        return bookingImpl;
    }

    /**
     * Returns the booking with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the booking
     * @return the booking
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking findByPrimaryKey(Serializable primaryKey)
        throws NoSuchBookingException, SystemException {
        Booking booking = fetchByPrimaryKey(primaryKey);

        if (booking == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchBookingException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return booking;
    }

    /**
     * Returns the booking with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingException} if it could not be found.
     *
     * @param id the primary key of the booking
     * @return the booking
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingException if a booking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking findByPrimaryKey(long id)
        throws NoSuchBookingException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the booking with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the booking
     * @return the booking, or <code>null</code> if a booking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Booking booking = (Booking) EntityCacheUtil.getResult(BookingModelImpl.ENTITY_CACHE_ENABLED,
                BookingImpl.class, primaryKey);

        if (booking == _nullBooking) {
            return null;
        }

        if (booking == null) {
            Session session = null;

            try {
                session = openSession();

                booking = (Booking) session.get(BookingImpl.class, primaryKey);

                if (booking != null) {
                    cacheResult(booking);
                } else {
                    EntityCacheUtil.putResult(BookingModelImpl.ENTITY_CACHE_ENABLED,
                        BookingImpl.class, primaryKey, _nullBooking);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(BookingModelImpl.ENTITY_CACHE_ENABLED,
                    BookingImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return booking;
    }

    /**
     * Returns the booking with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the booking
     * @return the booking, or <code>null</code> if a booking with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Booking fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the bookings.
     *
     * @return the bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Booking> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the bookings.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of bookings
     * @param end the upper bound of the range of bookings (not inclusive)
     * @return the range of bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Booking> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the bookings.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of bookings
     * @param end the upper bound of the range of bookings (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Booking> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Booking> list = (List<Booking>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_BOOKING);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_BOOKING;

                if (pagination) {
                    sql = sql.concat(BookingModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Booking>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Booking>(list);
                } else {
                    list = (List<Booking>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the bookings from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Booking booking : findAll()) {
            remove(booking);
        }
    }

    /**
     * Returns the number of bookings.
     *
     * @return the number of bookings
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_BOOKING);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the booking persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.booking.model.Booking")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Booking>> listenersList = new ArrayList<ModelListener<Booking>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Booking>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(BookingImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
