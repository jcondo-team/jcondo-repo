/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.model.impl;

import java.util.List;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.model.BookingNote;
import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.booking.model.datatype.BookingStatus;
import br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingNoteLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil;
import br.com.atilo.jcondo.manager.NoSuchDomainException;
import br.com.atilo.jcondo.manager.NoSuchEnterpriseException;
import br.com.atilo.jcondo.manager.NoSuchFlatException;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.manager.service.EnterpriseLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

/**
 * The extended model implementation for the Booking service. Represents a row in the &quot;jco_booking&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.model.Booking} interface.
 * </p>
 *
 * @author anderson
 */
public class BookingImpl extends BookingBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a booking model instance should use the {@link br.com.atilo.jcondo.booking.model.Booking} interface instead.
	 */
	public BookingImpl() {
	}

	public BookingStatus getStatus() {
		return BookingStatus.valueOf(getStatusId());
	}

	public void setStatus(BookingStatus status) {
		setStatusId(status == null ? -1 : status.ordinal());
	}

	public BaseModel<?> getDomain() throws PortalException, SystemException {
		if (getDomainId() <= 0) {
			return null;
		}

		try {
			return FlatLocalServiceUtil.getFlat(getDomainId());
		} catch (NoSuchFlatException e) {
			try {
				return EnterpriseLocalServiceUtil.getEnterprise(getDomainId());
			} catch (NoSuchEnterpriseException ex) {
				throw new NoSuchDomainException();
			}
		}
	}

	public void setDomain(BaseModel<?> domain) {
		setDomainId(domain == null ? 0 : (Long) domain.getPrimaryKeyObj());
	}

	public Room getRoom() throws PortalException, SystemException {
		return getRoomId() == 0 ? null : RoomLocalServiceUtil.getRoom(getRoomId());
	}

	public void setRoom(Room room) {
		setRoomId(room == null ? 0 : room.getId());
	}

	public Person getPerson() throws PortalException, SystemException {
		return getPersonId() == 0 ? null : PersonLocalServiceUtil.getPerson(getPersonId());
	}
	
	public void setPerson(Person person) {
		setPersonId(person == null ? 0 : person.getId());
	}

	public List<Guest> getGuests() throws PortalException, SystemException {
		return GuestLocalServiceUtil.getBookingGuests(getId());
	}
	
	public BookingNote getNote() throws PortalException, SystemException {
		return getNoteId() == 0 ? null : BookingNoteLocalServiceUtil.getBookingNote(getNoteId());
	}

	public void setNote(BookingNote note) {
		setNoteId(note == null ? 0 : note.getId());
	}

	public BookingInvoice getInvoice() throws SystemException {
		List<BookingInvoice> list = BookingInvoiceLocalServiceUtil.getByBooking(getId());

		if (list == null || list.isEmpty()) {
			return null;
		}

		return list.get(0);
	}

}