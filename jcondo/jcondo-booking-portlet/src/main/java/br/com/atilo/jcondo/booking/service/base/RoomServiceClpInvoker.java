package br.com.atilo.jcondo.booking.service.base;

import br.com.atilo.jcondo.booking.service.RoomServiceUtil;

import java.util.Arrays;

/**
 * @author Brian Wing Shun Chan
 * @generated
 */
public class RoomServiceClpInvoker {
    private String _methodName62;
    private String[] _methodParameterTypes62;
    private String _methodName63;
    private String[] _methodParameterTypes63;
    private String _methodName68;
    private String[] _methodParameterTypes68;
    private String _methodName69;
    private String[] _methodParameterTypes69;
    private String _methodName70;
    private String[] _methodParameterTypes70;
    private String _methodName71;
    private String[] _methodParameterTypes71;
    private String _methodName72;
    private String[] _methodParameterTypes72;
    private String _methodName73;
    private String[] _methodParameterTypes73;
    private String _methodName74;
    private String[] _methodParameterTypes74;
    private String _methodName75;
    private String[] _methodParameterTypes75;
    private String _methodName76;
    private String[] _methodParameterTypes76;

    public RoomServiceClpInvoker() {
        _methodName62 = "getBeanIdentifier";

        _methodParameterTypes62 = new String[] {  };

        _methodName63 = "setBeanIdentifier";

        _methodParameterTypes63 = new String[] { "java.lang.String" };

        _methodName68 = "addRoom";

        _methodParameterTypes68 = new String[] {
                "java.lang.String", "java.lang.String", "double", "int",
                "boolean", "boolean", "java.io.File"
            };

        _methodName69 = "addRoom";

        _methodParameterTypes69 = new String[] {
                "br.com.atilo.jcondo.booking.model.Room"
            };

        _methodName70 = "updateRoom";

        _methodParameterTypes70 = new String[] {
                "long", "java.lang.String", "java.lang.String", "double", "int",
                "boolean", "boolean", "java.io.File"
            };

        _methodName71 = "updateRoom";

        _methodParameterTypes71 = new String[] {
                "br.com.atilo.jcondo.booking.model.Room"
            };

        _methodName72 = "deleteRoom";

        _methodParameterTypes72 = new String[] {
                "br.com.atilo.jcondo.booking.model.Room"
            };

        _methodName73 = "addRoomPicture";

        _methodParameterTypes73 = new String[] {
                "long", "br.com.atilo.jcondo.Image"
            };

        _methodName74 = "deleteRoomPicture";

        _methodParameterTypes74 = new String[] { "long", "long" };

        _methodName75 = "getRooms";

        _methodParameterTypes75 = new String[] { "boolean", "boolean" };

        _methodName76 = "checkRoomAvailability";

        _methodParameterTypes76 = new String[] { "long", "java.util.Date" };
    }

    public Object invokeMethod(String name, String[] parameterTypes,
        Object[] arguments) throws Throwable {
        if (_methodName62.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes62, parameterTypes)) {
            return RoomServiceUtil.getBeanIdentifier();
        }

        if (_methodName63.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes63, parameterTypes)) {
            RoomServiceUtil.setBeanIdentifier((java.lang.String) arguments[0]);

            return null;
        }

        if (_methodName68.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes68, parameterTypes)) {
            return RoomServiceUtil.addRoom((java.lang.String) arguments[0],
                (java.lang.String) arguments[1],
                ((Double) arguments[2]).doubleValue(),
                ((Integer) arguments[3]).intValue(),
                ((Boolean) arguments[4]).booleanValue(),
                ((Boolean) arguments[5]).booleanValue(),
                (java.io.File) arguments[6]);
        }

        if (_methodName69.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes69, parameterTypes)) {
            return RoomServiceUtil.addRoom((br.com.atilo.jcondo.booking.model.Room) arguments[0]);
        }

        if (_methodName70.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes70, parameterTypes)) {
            return RoomServiceUtil.updateRoom(((Long) arguments[0]).longValue(),
                (java.lang.String) arguments[1],
                (java.lang.String) arguments[2],
                ((Double) arguments[3]).doubleValue(),
                ((Integer) arguments[4]).intValue(),
                ((Boolean) arguments[5]).booleanValue(),
                ((Boolean) arguments[6]).booleanValue(),
                (java.io.File) arguments[7]);
        }

        if (_methodName71.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes71, parameterTypes)) {
            return RoomServiceUtil.updateRoom((br.com.atilo.jcondo.booking.model.Room) arguments[0]);
        }

        if (_methodName72.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes72, parameterTypes)) {
            return RoomServiceUtil.deleteRoom((br.com.atilo.jcondo.booking.model.Room) arguments[0]);
        }

        if (_methodName73.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes73, parameterTypes)) {
            return RoomServiceUtil.addRoomPicture(((Long) arguments[0]).longValue(),
                (br.com.atilo.jcondo.Image) arguments[1]);
        }

        if (_methodName74.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes74, parameterTypes)) {
            return RoomServiceUtil.deleteRoomPicture(((Long) arguments[0]).longValue(),
                ((Long) arguments[1]).longValue());
        }

        if (_methodName75.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes75, parameterTypes)) {
            return RoomServiceUtil.getRooms(((Boolean) arguments[0]).booleanValue(),
                ((Boolean) arguments[1]).booleanValue());
        }

        if (_methodName76.equals(name) &&
                Arrays.deepEquals(_methodParameterTypes76, parameterTypes)) {
            RoomServiceUtil.checkRoomAvailability(((Long) arguments[0]).longValue(),
                (java.util.Date) arguments[1]);

            return null;
        }

        throw new UnsupportedOperationException();
    }
}
