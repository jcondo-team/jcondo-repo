package br.com.atilo.jcondo.booking.payment.boletofacil;

import java.util.Date;

public class Payment {

	/**
	 * Identificador unico do pagamento no Boleto Facil
	 */
	private long id;
	
	/**
	 * Valor pago
	 */
	private double amount;
	
	
	/**
	 * Data do registro do pagamento no banco
	 */
	private Date date;
	
	/**
	 * Taxa sobre o pagamento, em Reais
	 */
	private double fee;
	
	/**
	 * Dados da cobranca associada a este pagamento
	 */
	private Charge charge;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public Charge getCharge() {
		return charge;
	}

	public void setCharge(Charge charge) {
		this.charge = charge;
	}

}
