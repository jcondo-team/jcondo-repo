package br.com.atilo.jcondo.booking.model.impl;

import br.com.atilo.jcondo.booking.model.RoomBooking;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing RoomBooking in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see RoomBooking
 * @generated
 */
public class RoomBookingCacheModel implements CacheModel<RoomBooking>,
    Externalizable {
    public long id;
    public long roomId;
    public int weekDay;
    public int openHour;
    public int closeHour;
    public int period;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(17);

        sb.append("{id=");
        sb.append(id);
        sb.append(", roomId=");
        sb.append(roomId);
        sb.append(", weekDay=");
        sb.append(weekDay);
        sb.append(", openHour=");
        sb.append(openHour);
        sb.append(", closeHour=");
        sb.append(closeHour);
        sb.append(", period=");
        sb.append(period);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public RoomBooking toEntityModel() {
        RoomBookingImpl roomBookingImpl = new RoomBookingImpl();

        roomBookingImpl.setId(id);
        roomBookingImpl.setRoomId(roomId);
        roomBookingImpl.setWeekDay(weekDay);
        roomBookingImpl.setOpenHour(openHour);
        roomBookingImpl.setCloseHour(closeHour);
        roomBookingImpl.setPeriod(period);

        if (oprDate == Long.MIN_VALUE) {
            roomBookingImpl.setOprDate(null);
        } else {
            roomBookingImpl.setOprDate(new Date(oprDate));
        }

        roomBookingImpl.setOprUser(oprUser);

        roomBookingImpl.resetOriginalValues();

        return roomBookingImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        roomId = objectInput.readLong();
        weekDay = objectInput.readInt();
        openHour = objectInput.readInt();
        closeHour = objectInput.readInt();
        period = objectInput.readInt();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(roomId);
        objectOutput.writeInt(weekDay);
        objectOutput.writeInt(openHour);
        objectOutput.writeInt(closeHour);
        objectOutput.writeInt(period);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
