package br.com.atilo.jcondo.booking.bean;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.model.datatype.BookingStatus;
import br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingServiceUtil;
import br.com.atilo.jcondo.booking.service.GuestLocalServiceUtil;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.service.FlatServiceUtil;

@ManagedBean
@SessionScoped
public class MyBookingsBean extends Observable implements Observer, Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(MyBookingsBean.class);

	private List<Flat> flats;

	private Flat flat;

	private List<Booking> bookings;

	private Booking booking;

	private String guestName;

	private String guestSurname;

	private String bookingNote;

	@PostConstruct
	public void init() {
		try {
			flats = FlatServiceUtil.getFlats();
			flat = flats.get(0);
			onFlatSelect();
		} catch (Exception e) {
			LOGGER.fatal("Failure on room initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public void onFlatSelect() {
		try {
			bookings = BookingServiceUtil.getDomainBookings(flat.getId());
		} catch (Exception e) {
			LOGGER.error("Failure on flat select", e);
		}
	}

	public void onBookingInvoiceUpdate(Booking booking) {
		try {
			setBooking(booking); 
			BookingInvoiceLocalServiceUtil.addBookingInvoice(booking);
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on booking cancel", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.invoice.add", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}
	
	public void onBookingCancel() {
		try {
			BookingServiceUtil.cancelBooking(booking.getId());
			bookings = BookingServiceUtil.getDomainBookings(flat.getId());
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.cancel", null);
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on booking cancel", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.cancel", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}

		setChanged();
		notifyObservers(booking);
	}

	public void onBookingDelete() {
		try {
			if (booking != null) {
				BookingServiceUtil.deleteBooking(booking.getId(), bookingNote);
				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.delete", null);
				bookings.remove(booking);
			}
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on booking delete", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.delete", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}

		setChanged();
		notifyObservers(booking);
	}

	public void onGuestAdd() {
		try {
			GuestLocalServiceUtil.addGuest(booking.getId(), guestName, guestSurname);
			guestName = null;
			guestSurname = null;
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.guest.add", null);
		} catch (Exception e) {
			LOGGER.error("Failure on booking date select", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.guest.add", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onGuestDelete(Guest guest) {
		try {
			GuestLocalServiceUtil.deleteGuest(guest.getGuestId());
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.booking.guest.delete", null);
		} catch (Exception e) {
			LOGGER.error("Failure on booking guest add", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.booking.guest.delete", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}	

	public boolean isCancelEnabled(Booking booking) {
		Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		Date bookingDate = DateUtils.truncate(booking.getBeginDate(), Calendar.DAY_OF_MONTH);
		return !today.after(bookingDate) && booking.getStatus() != BookingStatus.CANCELLED;
	}

	public boolean hasDeadlineExceeded(Booking booking) {
		if (booking.getBeginDate() != null) {
			Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
			Date bookingDate = DateUtils.truncate(booking.getBeginDate(), Calendar.DAY_OF_MONTH);
			Date deadline = DateUtils.addDays(bookingDate, -7);
			return today.after(deadline);
		}
		return false;
	}
	
	public boolean hasBookingInvoiceOverdue(Booking booking) {
		try {
			BookingInvoice invoice = booking.getInvoice();
			if (invoice != null) {
				Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
				return today.after(invoice.getDate());
			}
		} catch (Exception e) {
			LOGGER.error("Failure on hasBookingInvoiceOverdue", e);
		}

		return false;
	}

	public boolean isPrePaid(Booking booking) {
		try {
			BookingInvoice invoice = booking.getInvoice();
			return BookingInvoiceLocalServiceUtil.isPrePaidMode(invoice != null ? invoice.getId() : 0);
		} catch (Exception e) {
			LOGGER.error("Failure on booking pre paid mode checking", e);
		}

		return false;
	}

	public boolean isInvoiceEnabled(Booking booking) {
		try {
			return booking.getRoom().isBookable() && isPrePaid(booking) && !isPaid(booking);
		} catch (Exception e) {
			LOGGER.error("Failure on booking pre paid mode checking", e);
		}
		
		return false;
	}
	
	public boolean isPaid(Booking booking) {
		try {
			return BookingLocalServiceUtil.isPaid(booking.getId());
		} catch (Exception e) {
			LOGGER.error("Failure on booking invoice payment check", e);
		}

		return false;
	}

	public List<Flat> getFlats() {
		return flats;
	}

	public void setFlats(List<Flat> flats) {
		this.flats = flats;
	}

	public Flat getFlat() {
		return flat;
	}

	public void setFlat(Flat flat) {
		this.flat = flat;
	}

	public List<Booking> getBookings() {
		return bookings;
	}

	public void setBookings(List<Booking> bookings) {
		this.bookings = bookings;
	}

	public Booking getBooking() {
		return booking;
	}

	public void setBooking(Booking booking) {
		this.booking = booking;
	}

	public String getGuestName() {
		return guestName;
	}

	public void setGuestName(String guestName) {
		this.guestName = guestName;
	}

	public String getGuestSurname() {
		return guestSurname;
	}

	public void setGuestSurname(String guestSurname) {
		this.guestSurname = guestSurname;
	}

	public String getBookingNote() {
		return bookingNote;
	}

	public void setBookingNote(String bookingNote) {
		this.bookingNote = bookingNote;
	}

	public void update(Observable obs, Object obj) {
		try {
			if (obj instanceof Booking) {
				Booking booking = (Booking) obj;
				if (flat.equals(booking.getDomain())) {
					bookings = BookingServiceUtil.getDomainBookings(flat.getId());
				}
				this.booking = booking;
			}
		} catch (Exception e) {
			LOGGER.error("Failure on update my bookings view", e);
		}
	}
}
