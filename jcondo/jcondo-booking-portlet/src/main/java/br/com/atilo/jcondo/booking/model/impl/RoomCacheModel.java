package br.com.atilo.jcondo.booking.model.impl;

import br.com.atilo.jcondo.booking.model.Room;

import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.model.CacheModel;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing Room in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see Room
 * @generated
 */
public class RoomCacheModel implements CacheModel<Room>, Externalizable {
    public long id;
    public long folderId;
    public long agreementId;
    public String name;
    public String description;
    public boolean available;
    public boolean bookable;
    public double price;
    public int capacity;
    public long oprDate;
    public long oprUser;

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(23);

        sb.append("{id=");
        sb.append(id);
        sb.append(", folderId=");
        sb.append(folderId);
        sb.append(", agreementId=");
        sb.append(agreementId);
        sb.append(", name=");
        sb.append(name);
        sb.append(", description=");
        sb.append(description);
        sb.append(", available=");
        sb.append(available);
        sb.append(", bookable=");
        sb.append(bookable);
        sb.append(", price=");
        sb.append(price);
        sb.append(", capacity=");
        sb.append(capacity);
        sb.append(", oprDate=");
        sb.append(oprDate);
        sb.append(", oprUser=");
        sb.append(oprUser);
        sb.append("}");

        return sb.toString();
    }

    @Override
    public Room toEntityModel() {
        RoomImpl roomImpl = new RoomImpl();

        roomImpl.setId(id);
        roomImpl.setFolderId(folderId);
        roomImpl.setAgreementId(agreementId);

        if (name == null) {
            roomImpl.setName(StringPool.BLANK);
        } else {
            roomImpl.setName(name);
        }

        if (description == null) {
            roomImpl.setDescription(StringPool.BLANK);
        } else {
            roomImpl.setDescription(description);
        }

        roomImpl.setAvailable(available);
        roomImpl.setBookable(bookable);
        roomImpl.setPrice(price);
        roomImpl.setCapacity(capacity);

        if (oprDate == Long.MIN_VALUE) {
            roomImpl.setOprDate(null);
        } else {
            roomImpl.setOprDate(new Date(oprDate));
        }

        roomImpl.setOprUser(oprUser);

        roomImpl.resetOriginalValues();

        return roomImpl;
    }

    @Override
    public void readExternal(ObjectInput objectInput) throws IOException {
        id = objectInput.readLong();
        folderId = objectInput.readLong();
        agreementId = objectInput.readLong();
        name = objectInput.readUTF();
        description = objectInput.readUTF();
        available = objectInput.readBoolean();
        bookable = objectInput.readBoolean();
        price = objectInput.readDouble();
        capacity = objectInput.readInt();
        oprDate = objectInput.readLong();
        oprUser = objectInput.readLong();
    }

    @Override
    public void writeExternal(ObjectOutput objectOutput)
        throws IOException {
        objectOutput.writeLong(id);
        objectOutput.writeLong(folderId);
        objectOutput.writeLong(agreementId);

        if (name == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(name);
        }

        if (description == null) {
            objectOutput.writeUTF(StringPool.BLANK);
        } else {
            objectOutput.writeUTF(description);
        }

        objectOutput.writeBoolean(available);
        objectOutput.writeBoolean(bookable);
        objectOutput.writeDouble(price);
        objectOutput.writeInt(capacity);
        objectOutput.writeLong(oprDate);
        objectOutput.writeLong(oprUser);
    }
}
