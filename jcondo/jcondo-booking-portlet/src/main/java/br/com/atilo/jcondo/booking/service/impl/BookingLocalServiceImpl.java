/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.Interval;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.OrderFactoryUtil;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactoryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.util.portlet.PortletProps;

import br.com.atilo.jcondo.booking.RoomBookingRule;
import br.com.atilo.jcondo.booking.RoomBookingRuleFactory;
import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.model.BookingNote;
import br.com.atilo.jcondo.booking.model.Guest;
import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.booking.model.RoomBooking;
import br.com.atilo.jcondo.booking.model.datatype.BookingStatus;
import br.com.atilo.jcondo.booking.payment.InvoiceScheduler;
import br.com.atilo.jcondo.booking.payment.PaymentProviderException;
import br.com.atilo.jcondo.booking.service.base.BookingLocalServiceBaseImpl;
import br.com.atilo.jcondo.booking.service.persistence.BookingFinderUtil;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.service.FlatLocalServiceUtil;

/**
 * The implementation of the booking local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.BookingLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.BookingLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil
 */

public class BookingLocalServiceImpl extends BookingLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil} to access the booking local service.
	 */
	private static final Logger LOGGER = Logger.getLogger(BookingLocalServiceImpl.class);
	
	private static final Logger MAIL_LOGGER = Logger.getLogger("MAILER");

	private static final ResourceBundle rb = ResourceBundle.getBundle("application");

	public static final int WHOLE_DAY = -1;

	public static final int BKG_CANCEL_DEADLINE = Integer.parseInt(PortletProps.get("booking.cancel.deadline"));

	public static final int BKG_MAX_DAYS = Integer.parseInt(PortletProps.get("booking.max.days"));
	
	@BeanReference(name="roomBookingRuleFactory")
	private RoomBookingRuleFactory roomBookingRuleFactory;
	
	public Booking createBooking(long roomId, Date date) throws PortalException, SystemException {
		Booking booking = super.createBooking(0);
		booking.setRoomId(roomId);

		Date bookingDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
		List<RoomBooking> rbs = roomBookingLocalService.getRoomBookings(roomId, DateUtils.toCalendar(date).get(Calendar.DAY_OF_WEEK));

		if (rbs.size() == 1) {
			RoomBooking rb = rbs.get(0);
			Date beginDate = DateUtils.setHours(bookingDate, rb.getOpenHour());
			Date endDate;

			if (rb.getCloseHour() < 24) {
				endDate = DateUtils.setHours(bookingDate, rb.getCloseHour());
			} else {
				endDate = DateUtils.addDays(bookingDate, 1);
			}

			booking.setBeginDate(beginDate);
			booking.setEndDate(endDate);
		} else {
			booking.setBeginDate((Date) bookingDate.clone());
			booking.setEndDate((Date) bookingDate.clone());
		}

		return booking;
	}

	public Date getDeadlineDate(Date beginDate) {
		return DateUtils.truncate(DateUtils.addDays(beginDate, -BKG_CANCEL_DEADLINE), Calendar.DAY_OF_MONTH);
	}

	public Date getMaxBookingDate() {
		return DateUtils.truncate(DateUtils.addDays(new Date(), BKG_MAX_DAYS), Calendar.DAY_OF_MONTH);
	}

	@SuppressWarnings("unchecked")
	public List<Booking> getDomainBookings(long domainId) throws SystemException {
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ne("statusId", BookingStatus.DELETED.ordinal()), 
										   RestrictionsFactoryUtil.eq("domainId", domainId)));
		dq.addOrder(OrderFactoryUtil.desc("beginDate"));
		return new ArrayList<Booking>(bookingPersistence.findWithDynamicQuery(dq));
	}

	@SuppressWarnings("unchecked")
	public List<Booking> getPersonBookings(long personId) throws SystemException {
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ne("statusId", BookingStatus.DELETED.ordinal()), 
										   RestrictionsFactoryUtil.eq("personId", personId)));
		dq.addOrder(OrderFactoryUtil.desc("beginDate"));
		return new ArrayList<Booking>(bookingPersistence.findWithDynamicQuery(dq));
	}

	@SuppressWarnings("unchecked")
	public List<Booking> getPersonBookings(long personId, Date fromDate, Date toDate) throws SystemException {
		Date beginDate = DateUtils.truncate(fromDate, Calendar.DAY_OF_MONTH);
		Date endDate = DateUtils.addSeconds(DateUtils.addDays(toDate, 1), -1);
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ne("statusId", BookingStatus.DELETED.ordinal()), 
										   RestrictionsFactoryUtil.eq("personId", personId)));
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ge("beginDate", beginDate), 
										   RestrictionsFactoryUtil.le("endDate", endDate)));

		dq.addOrder(OrderFactoryUtil.desc("beginDate"));
		return new ArrayList<Booking>(bookingPersistence.findWithDynamicQuery(dq));
	}
	
	public List<Booking> getBookings(long roomId, Date date) throws SystemException {
		return getBookings(roomId, date, date);
	}

	@SuppressWarnings("unchecked")
	public List<Booking> getBookings(long roomId, Date fromDate, Date toDate) throws SystemException {
		Date beginDate = DateUtils.truncate(fromDate, Calendar.DAY_OF_MONTH);
		Date endDate = DateUtils.addDays(DateUtils.truncate(toDate, Calendar.DAY_OF_MONTH), 1);
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ne("statusId", BookingStatus.DELETED.ordinal()),
										   RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.eq("roomId", roomId), 
																   	   RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ge("beginDate", beginDate), 
																		   					   	   RestrictionsFactoryUtil.le("endDate", endDate)))));
		dq.addOrder(OrderFactoryUtil.desc("beginDate"));
		return new ArrayList<Booking>(bookingPersistence.findWithDynamicQuery(dq));
	}

	public List<Booking> getBookings(Date date) throws SystemException {
		return getBookings(date, date);
	}
	
	@SuppressWarnings("unchecked")
	public List<Booking> getBookings(Date fromDate, Date toDate) throws SystemException {
		Date beginDate = DateUtils.truncate(fromDate, Calendar.DAY_OF_MONTH);
		Date endDate = DateUtils.addSeconds(DateUtils.addDays(toDate, 1), -1);
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.eq("statusId", BookingStatus.BOOKED.ordinal()),
										   RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ge("beginDate", beginDate), 
											   					   	   RestrictionsFactoryUtil.le("endDate", endDate))));
		dq.addOrder(OrderFactoryUtil.desc("beginDate"));
		return new ArrayList<Booking>(bookingPersistence.findWithDynamicQuery(dq));
	}	

	private void validate(long roomId, Date beginDate, Date endDate) throws PortalException, SystemException {
		Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);

		if (beginDate.before(today)) {
			throw new BusinessException("exception.booking.past.date");
		}

		if (beginDate.after(getMaxBookingDate())) {
			throw new BusinessException("exception.booking.over.date", BKG_MAX_DAYS);
		}

		if (!beginDate.before(endDate)) {
			throw new BusinessException("exception.booking.invalid.time.range");
		}
		
		boolean validBookingTime = false;
		Interval bookingInterval = new Interval(beginDate.getTime(), endDate.getTime());
		List<RoomBooking> roomBookings = roomBookingLocalService.getRoomBookings(roomId, DateUtils.toCalendar(beginDate).get(Calendar.DAY_OF_WEEK));

		for (RoomBooking roomBooking : roomBookings) {
			Date openDate = DateUtils.setHours(DateUtils.truncate(beginDate, Calendar.DAY_OF_MONTH), roomBooking.getOpenHour());
			Date closeDate;
			if (roomBooking.getCloseHour() < 24) {
				closeDate = DateUtils.setHours(DateUtils.truncate(endDate, Calendar.DAY_OF_MONTH), roomBooking.getCloseHour());
			} else {
				closeDate = DateUtils.addDays(DateUtils.truncate(endDate, Calendar.DAY_OF_MONTH), 1);
			}

			Interval roomBookingInterval = new Interval(openDate.getTime(), closeDate.getTime());

			if (roomBookingInterval.contains(bookingInterval)) {
				if (roomBooking.getPeriod() != WHOLE_DAY) {
					if (bookingInterval.toDuration().getStandardHours() > roomBooking.getPeriod()) {
						Room room = roomLocalService.getRoom(roomId);
						throw new BusinessException("exception.booking.over.period", room.getName(), roomBooking.getPeriod());
					}
				}

				validBookingTime = true;
				break;
			}
		}

		if (!validBookingTime) {
			throw new BusinessException("exception.booking.time.not.valid");
		}
	}

	public Booking addBooking(long roomId, long domainId, long personId, Date beginDate, Date endDate, List<Guest> guests) throws Exception {
		roomLocalService.checkRoomAvailability(roomId, beginDate);

		validate(roomId, beginDate, endDate);

		RoomBookingRule rbr = roomBookingRuleFactory.getRoomBookingRule(roomId);

		if (rbr != null) {
			rbr.validate(personId, beginDate);
		}

		List<Booking> overlapCancelledBkgs = new ArrayList<Booking>();
		Interval interval = new Interval(beginDate.getTime(), endDate.getTime());

		for (Booking booking : getBookings(roomId, beginDate)) {
			Interval itv = new Interval(booking.getBeginDate().getTime(), 
									    booking.getEndDate().getTime());
			if (itv.overlaps(interval)) {
				if(booking.getStatus() != BookingStatus.CANCELLED && booking.getStatus() != BookingStatus.DELETED) {
					throw new BusinessException("exception.booking.overlap", booking);
				} else {
					overlapCancelledBkgs.add(booking);
				}
			}
		}

		if (!CollectionUtils.isEmpty(overlapCancelledBkgs)) {
			Flat flat = FlatLocalServiceUtil.getFlat(domainId);
			for (Booking booking : overlapCancelledBkgs) {
				deleteBooking(booking, MessageFormat.format(rb.getString("booking.deleted.due.other.booking"), 
															flat.getNumber(), flat.getBlock()));
			}
		}

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();		
		Booking booking = createBooking(counterLocalService.increment());
		booking.setRoomId(roomId);
		booking.setDomainId(domainId);
		booking.setPersonId(personId);
		booking.setBeginDate(beginDate);
		booking.setEndDate(endDate);
		booking.setStatus(BookingStatus.BOOKED);
		booking.setOprDate(new Date());
		booking.setOprUser(sc.getUserId());

		booking = super.addBooking(booking);

		if (guests != null) {
			for (Guest guest : guests) {
				guestLocalService.addGuest(booking.getId(), guest.getName(), guest.getSurname());
			}
		}

		if (booking.getRoom().isBookable()) {
			try {
				try {
					bookingInvoiceLocalService.addBookingInvoice(booking);
				} catch (PaymentProviderException e) {
					LOGGER.error("Fail to add invoice for booking " + booking.getId() + ": " + e.getMessage());
					InvoiceScheduler.schedule(booking);
				}
			} catch (Exception e) {
				MAIL_LOGGER.error("Failure to add invoice for booking " + booking.getId() + ": " + e.getMessage());
			}
		}

		return booking;
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Booking addBooking(Booking booking) throws SystemException {
		try {
			return addBooking(booking.getRoomId(), booking.getDomainId(), booking.getPersonId(), 
							  booking.getBeginDate(), booking.getEndDate(), null);
		} catch (Exception e) {
			throw new SystemException(e);
		}
	}

	public Booking suspendBooking(Booking booking) throws SystemException {
		booking.setStatus(BookingStatus.SUSPENDED);
		return updateBooking(booking);
	}	

	public Booking cancelBooking(Booking booking) throws Exception {
		Date today = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
		Date bookingDate = DateUtils.truncate(booking.getBeginDate(), Calendar.DAY_OF_MONTH);
		Date deadline = DateUtils.addDays(bookingDate, -BKG_CANCEL_DEADLINE);

		booking.setStatus(BookingStatus.CANCELLED);
		Booking b = updateBooking(booking);

		if (today.after(deadline)) {
			return b;
		} else {
			b.setOprUser(userLocalService.getDefaultUserId(ServiceContextThreadLocal.getServiceContext().getCompanyId()));
			return deleteBooking(b, rb.getString("booking.cancelled.within.deadline"));
		}
	}
	
	public Booking deleteBooking(Booking booking, String note) throws SystemException {
		if (StringUtils.isEmpty(note)) {
			throw new BusinessException("exception.booking.note.empty");
		}

		BookingNote bookingNote = bookingNoteLocalService.createBookingNote(counterLocalService.increment());
		bookingNote.setText(note);

		bookingNote = bookingNoteLocalService.addBookingNote(bookingNote);
		booking.setNoteId(bookingNote.getId());

		return deleteBooking(booking);
	}

	@Override
	@Indexable(type = IndexableType.DELETE)
	public Booking deleteBooking(Booking booking) throws SystemException {
		booking.setStatus(BookingStatus.DELETED);
		return updateBooking(booking);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Booking updateBooking(Booking booking) throws SystemException {
		booking.setOprDate(new Date());
		if (booking.getOprUser() <= 0) {
			booking.setOprUser(ServiceContextThreadLocal.getServiceContext().getUserId());
		}
		return super.updateBooking(booking);
	}
	
	public List<Booking> getOverdueBookings() {
		return BookingFinderUtil.findOverdueBookings();
	}

	public boolean isPaid(long bookingId) throws PortalException, SystemException {
		Booking booking = getBooking(bookingId);

		try {
			BookingInvoice invoice = booking.getInvoice();
			return invoice != null && !CollectionUtils.isEmpty(paymentLocalService.getPayments(invoice.getId()));
		} catch (Exception e) {
			LOGGER.error("Fail to check if the booking " + bookingId + " is paid: " + e.getMessage());
		}

		return false;
	}

	@SuppressWarnings("unchecked")
	public List<Booking> getDomainBookings(long domainId, Date date) throws SystemException {
		Date beginDate = DateUtils.truncate(date, Calendar.DAY_OF_MONTH);
		Date endDate = DateUtils.addSeconds(DateUtils.addDays(date, 1), -1);
		DynamicQuery dq = dynamicQuery();
		dq.add(RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ne("statusId", BookingStatus.DELETED.ordinal()),
										   RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.eq("domainId", domainId), 
																   	   RestrictionsFactoryUtil.and(RestrictionsFactoryUtil.ge("beginDate", beginDate), 
																		   					   	   RestrictionsFactoryUtil.le("endDate", endDate)))));
		return bookingPersistence.findWithDynamicQuery(dq);
	}

}