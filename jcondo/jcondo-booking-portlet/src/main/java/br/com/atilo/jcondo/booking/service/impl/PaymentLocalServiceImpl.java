/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import com.liferay.portal.kernel.bean.BeanReference;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.search.Indexable;
import com.liferay.portal.kernel.search.IndexableType;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portal.util.PortalUtil;

import br.com.atilo.jcondo.booking.NoSuchPaymentException;
import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.Payment;
import br.com.atilo.jcondo.booking.model.datatype.BookingStatus;
import br.com.atilo.jcondo.booking.payment.PaymentProvider;
import br.com.atilo.jcondo.booking.service.base.PaymentLocalServiceBaseImpl;

/**
 * The implementation of the payment local service.
 *
 * <p>
 * All custom service methods should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.service.PaymentLocalService} interface.
 *
 * <p>
 * This is a local service. Methods of this service will not have security checks based on the propagated JAAS credentials because this service can only be accessed from within the same VM.
 * </p>
 *
 * @author anderson
 * @see br.com.atilo.jcondo.booking.service.base.PaymentLocalServiceBaseImpl
 * @see br.com.atilo.jcondo.booking.service.PaymentLocalServiceUtil
 */
public class PaymentLocalServiceImpl extends PaymentLocalServiceBaseImpl {

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this interface directly. Always use {@link br.com.atilo.jcondo.booking.service.PaymentLocalServiceUtil} to access the payment local service.
	 */
	private static final Logger LOGGER = Logger.getLogger(PaymentLocalServiceImpl.class);

	@BeanReference(name="paymentProvider")
	private PaymentProvider provider;

	public Payment addPayment(String token, long invoiceId, String invoiceCode) throws SystemException {
		LOGGER.info("Adding payment: token=" + token + ", invoiceId=" + invoiceId + ", invoiceCode=" + invoiceCode);

		Payment payment;

		try {
			payment = paymentPersistence.findByToken(token);
			if (payment.getDate() != null) {
				LOGGER.info("the payment " + token  + "is already checked. It will be ignored");
				return null;
			}
		} catch (NoSuchPaymentException e) {
			payment = createPayment(counterLocalService.increment());
			payment.setToken(token);
		}

		payment.setInvoiceId(invoiceId);

		try {
			provider.addPayment(payment);
		} finally {
			payment.setOprDate(new Date());
			setOprUser(payment);

			if (payment.isNew()) {
				payment = super.addPayment(payment);
			} else {
				payment = updatePayment(payment);
			}
			
			try {
				Booking booking = bookingInvoiceLocalService.getBookingInvoice(invoiceId).getBooking();
				if (booking.getStatus() != BookingStatus.BOOKED) {
					booking.setStatus(BookingStatus.BOOKED);
					bookingLocalService.updateBooking(booking);
				}
			} catch (Exception ex) {
				LOGGER.error("Fail to update booking status to BOOKED", ex);
			}
		}

		return payment;
	}
	
	private void setOprUser(Payment payment) throws SystemException {
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		if (sc == null) {
			try {
				payment.setOprUser(UserLocalServiceUtil.getDefaultUserId(PortalUtil.getDefaultCompanyId()));
			} catch (PortalException e) {
				payment.setOprUser(0);
				e.printStackTrace();
			}
		} else {
			payment.setOprUser(sc.getUserId());
		}
	}

	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Payment addPayment(Payment payment) throws SystemException {
		return addPayment(payment.getToken(), payment.getInvoiceId(), null);
	}
	
	@Override
	@Indexable(type = IndexableType.REINDEX)
	public Payment updatePayment(Payment payment, boolean merge) throws SystemException {
		payment.setOprDate(new Date());

		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		if (sc == null) {
			try {
				payment.setOprUser(UserLocalServiceUtil.getDefaultUserId(PortalUtil.getDefaultCompanyId()));
			} catch (PortalException e) {
				payment.setOprUser(0);
				e.printStackTrace();
			}
		} else {
			payment.setOprUser(sc.getUserId());
		}

		return super.updatePayment(payment);
	}

	public List<Payment> getPayments(long invoiceId) throws SystemException {
		return paymentPersistence.findByInvoice(invoiceId);
	}
}