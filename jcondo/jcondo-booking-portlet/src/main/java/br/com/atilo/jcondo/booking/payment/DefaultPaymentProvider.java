package br.com.atilo.jcondo.booking.payment;

import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.model.Payment;

public class DefaultPaymentProvider implements PaymentProvider {

	public PaymentProviderId getKey() {
		return PaymentProviderId.DEFAULT;
	}

	public PaymentMode getPaymentMode() {
		return PaymentMode.POST_PAID;
	}

	public void addInvoice(BookingInvoice invoice) throws PaymentProviderException {
	}

	public void addPayment(Payment payment) throws PaymentProviderException {
	}

}
