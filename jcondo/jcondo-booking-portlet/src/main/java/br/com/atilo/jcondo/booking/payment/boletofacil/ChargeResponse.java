package br.com.atilo.jcondo.booking.payment.boletofacil;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class ChargeResponse {

	private boolean success;

	private String errorMessage;
	
	private ChargeData data;

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(data).append(success).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ChargeResponse) == false) {
			return false;
		}
		ChargeResponse rhs = ((ChargeResponse) other);
		return new EqualsBuilder().append(data, rhs.data).append(success, rhs.success).isEquals();
	}	
	
	public ChargeData getData() {
		return data;
	}

	public void setData(ChargeData data) {
		this.data = data;
	}

	public boolean isSuccess() {
		return success;
	}

	public void setSuccess(boolean success) {
		this.success = success;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

}

class ChargeData {

	private List<Charge> charges = new ArrayList<Charge>();
	
	public List<Charge> getCharges() {
		return charges;
	}
	
	public void setCharges(List<Charge> charges) {
		this.charges = charges;
	}
	
	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
	
	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(charges).toHashCode();
	}
	
	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof ChargeData) == false) {
			return false;
		}
		ChargeData rhs = ((ChargeData) other);
		return new EqualsBuilder().append(charges, rhs.charges).isEquals();
	}
}