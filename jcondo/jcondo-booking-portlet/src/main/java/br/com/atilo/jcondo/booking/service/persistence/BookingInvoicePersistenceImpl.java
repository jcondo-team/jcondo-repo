package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException;
import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.model.impl.BookingInvoiceImpl;
import br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl;
import br.com.atilo.jcondo.booking.service.persistence.BookingInvoicePersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the booking invoice service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingInvoicePersistence
 * @see BookingInvoiceUtil
 * @generated
 */
public class BookingInvoicePersistenceImpl extends BasePersistenceImpl<BookingInvoice>
    implements BookingInvoicePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link BookingInvoiceUtil} to access the booking invoice persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = BookingInvoiceImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceModelImpl.FINDER_CACHE_ENABLED,
            BookingInvoiceImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceModelImpl.FINDER_CACHE_ENABLED,
            BookingInvoiceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_CODE = new FinderPath(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceModelImpl.FINDER_CACHE_ENABLED,
            BookingInvoiceImpl.class, FINDER_CLASS_NAME_ENTITY, "fetchByCode",
            new String[] { String.class.getName() },
            BookingInvoiceModelImpl.CODE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_CODE = new FinderPath(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByCode",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_CODE_CODE_1 = "bookingInvoice.code IS NULL";
    private static final String _FINDER_COLUMN_CODE_CODE_2 = "bookingInvoice.code = ?";
    private static final String _FINDER_COLUMN_CODE_CODE_3 = "(bookingInvoice.code IS NULL OR bookingInvoice.code = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKING = new FinderPath(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceModelImpl.FINDER_CACHE_ENABLED,
            BookingInvoiceImpl.class, FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByBooking",
            new String[] {
                Long.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING =
        new FinderPath(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceModelImpl.FINDER_CACHE_ENABLED,
            BookingInvoiceImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBooking",
            new String[] { Long.class.getName() },
            BookingInvoiceModelImpl.BOOKINGID_COLUMN_BITMASK |
            BookingInvoiceModelImpl.DATE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_BOOKING = new FinderPath(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBooking",
            new String[] { Long.class.getName() });
    private static final String _FINDER_COLUMN_BOOKING_BOOKINGID_2 = "bookingInvoice.bookingId = ?";
    private static final String _SQL_SELECT_BOOKINGINVOICE = "SELECT bookingInvoice FROM BookingInvoice bookingInvoice";
    private static final String _SQL_SELECT_BOOKINGINVOICE_WHERE = "SELECT bookingInvoice FROM BookingInvoice bookingInvoice WHERE ";
    private static final String _SQL_COUNT_BOOKINGINVOICE = "SELECT COUNT(bookingInvoice) FROM BookingInvoice bookingInvoice";
    private static final String _SQL_COUNT_BOOKINGINVOICE_WHERE = "SELECT COUNT(bookingInvoice) FROM BookingInvoice bookingInvoice WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "bookingInvoice.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BookingInvoice exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No BookingInvoice exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(BookingInvoicePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static BookingInvoice _nullBookingInvoice = new BookingInvoiceImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<BookingInvoice> toCacheModel() {
                return _nullBookingInvoiceCacheModel;
            }
        };

    private static CacheModel<BookingInvoice> _nullBookingInvoiceCacheModel = new CacheModel<BookingInvoice>() {
            @Override
            public BookingInvoice toEntityModel() {
                return _nullBookingInvoice;
            }
        };

    public BookingInvoicePersistenceImpl() {
        setModelClass(BookingInvoice.class);
    }

    /**
     * Returns the booking invoice where code = &#63; or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException} if it could not be found.
     *
     * @param code the code
     * @return the matching booking invoice
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a matching booking invoice could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice findByCode(String code)
        throws NoSuchBookingInvoiceException, SystemException {
        BookingInvoice bookingInvoice = fetchByCode(code);

        if (bookingInvoice == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("code=");
            msg.append(code);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchBookingInvoiceException(msg.toString());
        }

        return bookingInvoice;
    }

    /**
     * Returns the booking invoice where code = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param code the code
     * @return the matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice fetchByCode(String code) throws SystemException {
        return fetchByCode(code, true);
    }

    /**
     * Returns the booking invoice where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param code the code
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice fetchByCode(String code, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { code };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_CODE,
                    finderArgs, this);
        }

        if (result instanceof BookingInvoice) {
            BookingInvoice bookingInvoice = (BookingInvoice) result;

            if (!Validator.equals(code, bookingInvoice.getCode())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_BOOKINGINVOICE_WHERE);

            boolean bindCode = false;

            if (code == null) {
                query.append(_FINDER_COLUMN_CODE_CODE_1);
            } else if (code.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODE_CODE_3);
            } else {
                bindCode = true;

                query.append(_FINDER_COLUMN_CODE_CODE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCode) {
                    qPos.add(code);
                }

                List<BookingInvoice> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "BookingInvoicePersistenceImpl.fetchByCode(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    BookingInvoice bookingInvoice = list.get(0);

                    result = bookingInvoice;

                    cacheResult(bookingInvoice);

                    if ((bookingInvoice.getCode() == null) ||
                            !bookingInvoice.getCode().equals(code)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE,
                            finderArgs, bookingInvoice);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODE,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (BookingInvoice) result;
        }
    }

    /**
     * Removes the booking invoice where code = &#63; from the database.
     *
     * @param code the code
     * @return the booking invoice that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice removeByCode(String code)
        throws NoSuchBookingInvoiceException, SystemException {
        BookingInvoice bookingInvoice = findByCode(code);

        return remove(bookingInvoice);
    }

    /**
     * Returns the number of booking invoices where code = &#63;.
     *
     * @param code the code
     * @return the number of matching booking invoices
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByCode(String code) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_CODE;

        Object[] finderArgs = new Object[] { code };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_BOOKINGINVOICE_WHERE);

            boolean bindCode = false;

            if (code == null) {
                query.append(_FINDER_COLUMN_CODE_CODE_1);
            } else if (code.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_CODE_CODE_3);
            } else {
                bindCode = true;

                query.append(_FINDER_COLUMN_CODE_CODE_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindCode) {
                    qPos.add(code);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the booking invoices where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @return the matching booking invoices
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<BookingInvoice> findByBooking(long bookingId)
        throws SystemException {
        return findByBooking(bookingId, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the booking invoices where bookingId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param bookingId the booking ID
     * @param start the lower bound of the range of booking invoices
     * @param end the upper bound of the range of booking invoices (not inclusive)
     * @return the range of matching booking invoices
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<BookingInvoice> findByBooking(long bookingId, int start, int end)
        throws SystemException {
        return findByBooking(bookingId, start, end, null);
    }

    /**
     * Returns an ordered range of all the booking invoices where bookingId = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param bookingId the booking ID
     * @param start the lower bound of the range of booking invoices
     * @param end the upper bound of the range of booking invoices (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching booking invoices
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<BookingInvoice> findByBooking(long bookingId, int start,
        int end, OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING;
            finderArgs = new Object[] { bookingId };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKING;
            finderArgs = new Object[] { bookingId, start, end, orderByComparator };
        }

        List<BookingInvoice> list = (List<BookingInvoice>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (BookingInvoice bookingInvoice : list) {
                if ((bookingId != bookingInvoice.getBookingId())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_BOOKINGINVOICE_WHERE);

            query.append(_FINDER_COLUMN_BOOKING_BOOKINGID_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(BookingInvoiceModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(bookingId);

                if (!pagination) {
                    list = (List<BookingInvoice>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<BookingInvoice>(list);
                } else {
                    list = (List<BookingInvoice>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first booking invoice in the ordered set where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching booking invoice
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a matching booking invoice could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice findByBooking_First(long bookingId,
        OrderByComparator orderByComparator)
        throws NoSuchBookingInvoiceException, SystemException {
        BookingInvoice bookingInvoice = fetchByBooking_First(bookingId,
                orderByComparator);

        if (bookingInvoice != null) {
            return bookingInvoice;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("bookingId=");
        msg.append(bookingId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchBookingInvoiceException(msg.toString());
    }

    /**
     * Returns the first booking invoice in the ordered set where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice fetchByBooking_First(long bookingId,
        OrderByComparator orderByComparator) throws SystemException {
        List<BookingInvoice> list = findByBooking(bookingId, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last booking invoice in the ordered set where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching booking invoice
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a matching booking invoice could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice findByBooking_Last(long bookingId,
        OrderByComparator orderByComparator)
        throws NoSuchBookingInvoiceException, SystemException {
        BookingInvoice bookingInvoice = fetchByBooking_Last(bookingId,
                orderByComparator);

        if (bookingInvoice != null) {
            return bookingInvoice;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("bookingId=");
        msg.append(bookingId);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchBookingInvoiceException(msg.toString());
    }

    /**
     * Returns the last booking invoice in the ordered set where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching booking invoice, or <code>null</code> if a matching booking invoice could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice fetchByBooking_Last(long bookingId,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByBooking(bookingId);

        if (count == 0) {
            return null;
        }

        List<BookingInvoice> list = findByBooking(bookingId, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the booking invoices before and after the current booking invoice in the ordered set where bookingId = &#63;.
     *
     * @param id the primary key of the current booking invoice
     * @param bookingId the booking ID
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next booking invoice
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice[] findByBooking_PrevAndNext(long id, long bookingId,
        OrderByComparator orderByComparator)
        throws NoSuchBookingInvoiceException, SystemException {
        BookingInvoice bookingInvoice = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            BookingInvoice[] array = new BookingInvoiceImpl[3];

            array[0] = getByBooking_PrevAndNext(session, bookingInvoice,
                    bookingId, orderByComparator, true);

            array[1] = bookingInvoice;

            array[2] = getByBooking_PrevAndNext(session, bookingInvoice,
                    bookingId, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected BookingInvoice getByBooking_PrevAndNext(Session session,
        BookingInvoice bookingInvoice, long bookingId,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_BOOKINGINVOICE_WHERE);

        query.append(_FINDER_COLUMN_BOOKING_BOOKINGID_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(BookingInvoiceModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(bookingId);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(bookingInvoice);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<BookingInvoice> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the booking invoices where bookingId = &#63; from the database.
     *
     * @param bookingId the booking ID
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByBooking(long bookingId) throws SystemException {
        for (BookingInvoice bookingInvoice : findByBooking(bookingId,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(bookingInvoice);
        }
    }

    /**
     * Returns the number of booking invoices where bookingId = &#63;.
     *
     * @param bookingId the booking ID
     * @return the number of matching booking invoices
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByBooking(long bookingId) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_BOOKING;

        Object[] finderArgs = new Object[] { bookingId };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_BOOKINGINVOICE_WHERE);

            query.append(_FINDER_COLUMN_BOOKING_BOOKINGID_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(bookingId);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the booking invoice in the entity cache if it is enabled.
     *
     * @param bookingInvoice the booking invoice
     */
    @Override
    public void cacheResult(BookingInvoice bookingInvoice) {
        EntityCacheUtil.putResult(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceImpl.class, bookingInvoice.getPrimaryKey(),
            bookingInvoice);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE,
            new Object[] { bookingInvoice.getCode() }, bookingInvoice);

        bookingInvoice.resetOriginalValues();
    }

    /**
     * Caches the booking invoices in the entity cache if it is enabled.
     *
     * @param bookingInvoices the booking invoices
     */
    @Override
    public void cacheResult(List<BookingInvoice> bookingInvoices) {
        for (BookingInvoice bookingInvoice : bookingInvoices) {
            if (EntityCacheUtil.getResult(
                        BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
                        BookingInvoiceImpl.class, bookingInvoice.getPrimaryKey()) == null) {
                cacheResult(bookingInvoice);
            } else {
                bookingInvoice.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all booking invoices.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(BookingInvoiceImpl.class.getName());
        }

        EntityCacheUtil.clearCache(BookingInvoiceImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the booking invoice.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(BookingInvoice bookingInvoice) {
        EntityCacheUtil.removeResult(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceImpl.class, bookingInvoice.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(bookingInvoice);
    }

    @Override
    public void clearCache(List<BookingInvoice> bookingInvoices) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (BookingInvoice bookingInvoice : bookingInvoices) {
            EntityCacheUtil.removeResult(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
                BookingInvoiceImpl.class, bookingInvoice.getPrimaryKey());

            clearUniqueFindersCache(bookingInvoice);
        }
    }

    protected void cacheUniqueFindersCache(BookingInvoice bookingInvoice) {
        if (bookingInvoice.isNew()) {
            Object[] args = new Object[] { bookingInvoice.getCode() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODE, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE, args,
                bookingInvoice);
        } else {
            BookingInvoiceModelImpl bookingInvoiceModelImpl = (BookingInvoiceModelImpl) bookingInvoice;

            if ((bookingInvoiceModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_CODE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { bookingInvoice.getCode() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_CODE, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_CODE, args,
                    bookingInvoice);
            }
        }
    }

    protected void clearUniqueFindersCache(BookingInvoice bookingInvoice) {
        BookingInvoiceModelImpl bookingInvoiceModelImpl = (BookingInvoiceModelImpl) bookingInvoice;

        Object[] args = new Object[] { bookingInvoice.getCode() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODE, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODE, args);

        if ((bookingInvoiceModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_CODE.getColumnBitmask()) != 0) {
            args = new Object[] { bookingInvoiceModelImpl.getOriginalCode() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_CODE, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_CODE, args);
        }
    }

    /**
     * Creates a new booking invoice with the primary key. Does not add the booking invoice to the database.
     *
     * @param id the primary key for the new booking invoice
     * @return the new booking invoice
     */
    @Override
    public BookingInvoice create(long id) {
        BookingInvoice bookingInvoice = new BookingInvoiceImpl();

        bookingInvoice.setNew(true);
        bookingInvoice.setPrimaryKey(id);

        return bookingInvoice;
    }

    /**
     * Removes the booking invoice with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the booking invoice
     * @return the booking invoice that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice remove(long id)
        throws NoSuchBookingInvoiceException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the booking invoice with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the booking invoice
     * @return the booking invoice that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice remove(Serializable primaryKey)
        throws NoSuchBookingInvoiceException, SystemException {
        Session session = null;

        try {
            session = openSession();

            BookingInvoice bookingInvoice = (BookingInvoice) session.get(BookingInvoiceImpl.class,
                    primaryKey);

            if (bookingInvoice == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchBookingInvoiceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(bookingInvoice);
        } catch (NoSuchBookingInvoiceException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected BookingInvoice removeImpl(BookingInvoice bookingInvoice)
        throws SystemException {
        bookingInvoice = toUnwrappedModel(bookingInvoice);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(bookingInvoice)) {
                bookingInvoice = (BookingInvoice) session.get(BookingInvoiceImpl.class,
                        bookingInvoice.getPrimaryKeyObj());
            }

            if (bookingInvoice != null) {
                session.delete(bookingInvoice);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (bookingInvoice != null) {
            clearCache(bookingInvoice);
        }

        return bookingInvoice;
    }

    @Override
    public BookingInvoice updateImpl(
        br.com.atilo.jcondo.booking.model.BookingInvoice bookingInvoice)
        throws SystemException {
        bookingInvoice = toUnwrappedModel(bookingInvoice);

        boolean isNew = bookingInvoice.isNew();

        BookingInvoiceModelImpl bookingInvoiceModelImpl = (BookingInvoiceModelImpl) bookingInvoice;

        Session session = null;

        try {
            session = openSession();

            if (bookingInvoice.isNew()) {
                session.save(bookingInvoice);

                bookingInvoice.setNew(false);
            } else {
                session.merge(bookingInvoice);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !BookingInvoiceModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((bookingInvoiceModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        bookingInvoiceModelImpl.getOriginalBookingId()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKING, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING,
                    args);

                args = new Object[] { bookingInvoiceModelImpl.getBookingId() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKING, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKING,
                    args);
            }
        }

        EntityCacheUtil.putResult(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
            BookingInvoiceImpl.class, bookingInvoice.getPrimaryKey(),
            bookingInvoice);

        clearUniqueFindersCache(bookingInvoice);
        cacheUniqueFindersCache(bookingInvoice);

        return bookingInvoice;
    }

    protected BookingInvoice toUnwrappedModel(BookingInvoice bookingInvoice) {
        if (bookingInvoice instanceof BookingInvoiceImpl) {
            return bookingInvoice;
        }

        BookingInvoiceImpl bookingInvoiceImpl = new BookingInvoiceImpl();

        bookingInvoiceImpl.setNew(bookingInvoice.isNew());
        bookingInvoiceImpl.setPrimaryKey(bookingInvoice.getPrimaryKey());

        bookingInvoiceImpl.setId(bookingInvoice.getId());
        bookingInvoiceImpl.setBookingId(bookingInvoice.getBookingId());
        bookingInvoiceImpl.setProviderId(bookingInvoice.getProviderId());
        bookingInvoiceImpl.setCode(bookingInvoice.getCode());
        bookingInvoiceImpl.setAmount(bookingInvoice.getAmount());
        bookingInvoiceImpl.setTax(bookingInvoice.getTax());
        bookingInvoiceImpl.setDate(bookingInvoice.getDate());
        bookingInvoiceImpl.setLink(bookingInvoice.getLink());
        bookingInvoiceImpl.setOprDate(bookingInvoice.getOprDate());
        bookingInvoiceImpl.setOprUser(bookingInvoice.getOprUser());

        return bookingInvoiceImpl;
    }

    /**
     * Returns the booking invoice with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the booking invoice
     * @return the booking invoice
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice findByPrimaryKey(Serializable primaryKey)
        throws NoSuchBookingInvoiceException, SystemException {
        BookingInvoice bookingInvoice = fetchByPrimaryKey(primaryKey);

        if (bookingInvoice == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchBookingInvoiceException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return bookingInvoice;
    }

    /**
     * Returns the booking invoice with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException} if it could not be found.
     *
     * @param id the primary key of the booking invoice
     * @return the booking invoice
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingInvoiceException if a booking invoice with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice findByPrimaryKey(long id)
        throws NoSuchBookingInvoiceException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the booking invoice with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the booking invoice
     * @return the booking invoice, or <code>null</code> if a booking invoice with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        BookingInvoice bookingInvoice = (BookingInvoice) EntityCacheUtil.getResult(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
                BookingInvoiceImpl.class, primaryKey);

        if (bookingInvoice == _nullBookingInvoice) {
            return null;
        }

        if (bookingInvoice == null) {
            Session session = null;

            try {
                session = openSession();

                bookingInvoice = (BookingInvoice) session.get(BookingInvoiceImpl.class,
                        primaryKey);

                if (bookingInvoice != null) {
                    cacheResult(bookingInvoice);
                } else {
                    EntityCacheUtil.putResult(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
                        BookingInvoiceImpl.class, primaryKey,
                        _nullBookingInvoice);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(BookingInvoiceModelImpl.ENTITY_CACHE_ENABLED,
                    BookingInvoiceImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return bookingInvoice;
    }

    /**
     * Returns the booking invoice with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the booking invoice
     * @return the booking invoice, or <code>null</code> if a booking invoice with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingInvoice fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the booking invoices.
     *
     * @return the booking invoices
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<BookingInvoice> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the booking invoices.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of booking invoices
     * @param end the upper bound of the range of booking invoices (not inclusive)
     * @return the range of booking invoices
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<BookingInvoice> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the booking invoices.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingInvoiceModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of booking invoices
     * @param end the upper bound of the range of booking invoices (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of booking invoices
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<BookingInvoice> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<BookingInvoice> list = (List<BookingInvoice>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_BOOKINGINVOICE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_BOOKINGINVOICE;

                if (pagination) {
                    sql = sql.concat(BookingInvoiceModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<BookingInvoice>) QueryUtil.list(q,
                            getDialect(), start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<BookingInvoice>(list);
                } else {
                    list = (List<BookingInvoice>) QueryUtil.list(q,
                            getDialect(), start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the booking invoices from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (BookingInvoice bookingInvoice : findAll()) {
            remove(bookingInvoice);
        }
    }

    /**
     * Returns the number of booking invoices.
     *
     * @return the number of booking invoices
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_BOOKINGINVOICE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the booking invoice persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.booking.model.BookingInvoice")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<BookingInvoice>> listenersList = new ArrayList<ModelListener<BookingInvoice>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<BookingInvoice>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(BookingInvoiceImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
