package br.com.atilo.jcondo.booking.bean;

import java.io.Serializable;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.PropertyResourceBundle;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.apache.myfaces.commons.util.MessageUtils;
import org.primefaces.context.RequestContext;
import org.primefaces.event.RowEditEvent;

import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.booking.model.RoomBlockade;
import br.com.atilo.jcondo.booking.model.RoomBooking;
import br.com.atilo.jcondo.booking.service.RoomBlockadeLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomBlockadeServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomBookingServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomServiceUtil;
import br.com.atilo.jcondo.manager.BusinessException;
import br.com.atilo.jcondo.Image;

@ManagedBean
@SessionScoped
public class RoomBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private static Logger LOGGER = Logger.getLogger(RoomBean.class);	

	private ImageUploadBean imageUploadBean;

	private FileUploadBean fileUploadBean;

	private List<Room> rooms;

	private Room room;

	private RoomBlockade roomBlockade;
	
	private List<RoomBooking> roomBookings;

	private Date minBlockadeDate;

	public RoomBean() {
		this.imageUploadBean = new ImageUploadBean(640, 480);
		this.fileUploadBean = new FileUploadBean();
		this.minBlockadeDate = DateUtils.truncate(new Date(), Calendar.DAY_OF_MONTH);
	}

	@PostConstruct
	public void init() {
		try {
			rooms = RoomServiceUtil.getRooms(true, false);
		} catch (Exception e) {
			LOGGER.fatal("Failure on room initialization", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
		}
	}

	public void onRoomCreate() {
		room = RoomLocalServiceUtil.createRoom(0);
		room.setAvailable(true);
		roomBlockade = RoomBlockadeLocalServiceUtil.createRoomBlockade(0);
	}

	public void onRoomEdit(Room room) {
		try {
			this.room = room;
			this.roomBlockade = RoomBlockadeLocalServiceUtil.createRoomBlockade(0);
			this.roomBookings = room.getRoomBookings();
		} catch (Exception e) {
			LOGGER.fatal("Failure on room edit", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.unexpected.failure", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}
	
	public void onRoomSave() {
		try {
			if (room.isNew()) {
				room = RoomServiceUtil.addRoom(room.getName(), room.getDescription(), room.getPrice(), 
											   room.getCapacity(), room.isAvailable(), room.isBookable(), 
											   fileUploadBean.getFile());

				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.room.add", null);
			} else {
				room = RoomServiceUtil.updateRoom(room.getId(), room.getName(), room.getDescription(), 
												  room.getPrice(), room.getCapacity(), room.isAvailable(), 
												  room.isBookable(), fileUploadBean.getFile());

				MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.room.update", null);
			}


			rooms = RoomServiceUtil.getRooms(true, false);
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.fatal("Failure on room save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.room.save", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onRoomImageAdd() {
		try {
			imageUploadBean.onCropp();
			RoomServiceUtil.addRoomPicture(room.getId(), imageUploadBean.getImage());
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.room.image.add", null);
		} catch (BusinessException e) {
			LOGGER.fatal("Failure on room image save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.fatal("Failure on room image save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.room.image.add", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onRoomImageDelete(Image image) {
		try {
			RoomServiceUtil.deleteRoomPicture(room.getId(), image.getId());
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.room.image.delete", null);
		} catch (BusinessException e) {
			LOGGER.fatal("Failure on room image save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.fatal("Failure on room image save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.room.image.delete", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}	

	public void onRoomBookingUpdate(RowEditEvent event) {
		try {
			RoomBooking roomBooking = (RoomBooking) event.getObject();
			RoomBookingServiceUtil.updateRoomBooking(roomBooking);
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.room.booking.update", null);
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on roomBooking save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.room.booking.update", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onRoomBlokadeAdd() {
		try {
			roomBlockade.setRoomId(room.getId());
			RoomBlockadeServiceUtil.addRoomBlockade(roomBlockade);
			roomBlockade = RoomBlockadeLocalServiceUtil.createRoomBlockade(0);
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.room.blockade.add", null);
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on roomBooking save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.room.blockade.add", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}

	public void onRoomBlokadeDelete(RoomBlockade roomBlockade) {
		try {
			RoomBlockadeServiceUtil.deleteRoomBlockade(roomBlockade);
			MessageUtils.addMessage(FacesMessage.SEVERITY_INFO, "msg.room.blockade.delete", null);
		} catch (BusinessException e) {
			MessageUtils.addMessage(FacesMessage.SEVERITY_WARN, e.getMessage(), e.getArgs());
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		} catch (Exception e) {
			LOGGER.error("Failure on roomBooking save", e);
			MessageUtils.addMessage(FacesMessage.SEVERITY_FATAL, "exception.room.blockade.delete", null);
			RequestContext.getCurrentInstance().addCallbackParam("exception", true);
		}
	}	

	public String getRoomStyle(Room room) {
		return PropertyResourceBundle.getBundle("application").getString("room." + rooms.indexOf(room));
	}

	public String displayWeekDay(int weekDay) {
		return DateFormatSymbols.getInstance().getWeekdays()[weekDay];
	}
	
	public List<Integer> rangeArray(int min, int max) {
		List<Integer> array = new ArrayList<Integer>();
		for (int i = min; i <= max; i++) {
			array.add(i);
		}
		return array;
	}

	public ImageUploadBean getImageUploadBean() {
		return imageUploadBean;
	}

	public void setImageUploadBean(ImageUploadBean imageUploadBean) {
		this.imageUploadBean = imageUploadBean;
	}

	public FileUploadBean getFileUploadBean() {
		return fileUploadBean;
	}

	public void setFileUploadBean(FileUploadBean fileUploadBean) {
		this.fileUploadBean = fileUploadBean;
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public RoomBlockade getRoomBlockade() {
		return roomBlockade;
	}

	public void setRoomBlockade(RoomBlockade roomBlockade) {
		this.roomBlockade = roomBlockade;
	}

	public List<RoomBooking> getRoomBookings() {
		return roomBookings;
	}

	public void setRoomBookings(List<RoomBooking> roomBookings) {
		this.roomBookings = roomBookings;
	}

	public Date getMinBlockadeDate() {
		return minBlockadeDate;
	}

	public void setMinBlockadeDate(Date minBlockadeDate) {
		this.minBlockadeDate = minBlockadeDate;
	}

}