package br.com.atilo.jcondo.booking;

public class RoomBookingRuleFactory {

	public RoomBookingRule getRoomBookingRule(long roomId) {
		if (roomId == 6) {
			return new HomeOfficeBookingRule();
		}

		return null;
	}
}
