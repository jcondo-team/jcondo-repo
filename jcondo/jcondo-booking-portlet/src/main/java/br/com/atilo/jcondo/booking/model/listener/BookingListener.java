package br.com.atilo.jcondo.booking.model.listener;

import java.util.Date;
import java.util.ResourceBundle;

import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;

import br.com.atilo.jcondo.booking.model.Booking;
import br.com.atilo.jcondo.booking.model.BookingInvoice;
import br.com.atilo.jcondo.booking.model.BookingNote;
import br.com.atilo.jcondo.booking.model.datatype.BookingStatus;
import br.com.atilo.jcondo.booking.service.BookingInvoiceLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.BookingLocalServiceUtil;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.template.StringTemplateResource;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateManagerUtil;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.util.ContentUtil;

public class BookingListener extends BaseModelListener<Booking> {

	private static final Logger LOGGER = Logger.getLogger(BookingListener.class);

	private static final ResourceBundle rb = ResourceBundle.getBundle("application");
	
	private StringTemplateResource bookingPrePaidCreatedResource;
	
	private StringTemplateResource bookingPostPaidCreatedResource;

	private StringTemplateResource bookingPrePaidCancelledResource;

	private StringTemplateResource bookingPostPaidCancelledResource;

	private StringTemplateResource bookingPrePaidDeletedResource;

	private StringTemplateResource bookingPostPaidDeletedResource;

	private StringTemplateResource bookingNotPaidResource;

	
	public BookingListener() {
		bookingPrePaidCreatedResource = new StringTemplateResource("BookingPrePaidCreated", 
																	ContentUtil.get("br/com/atilo/jcondo/booking/mail/prepaid/booking-created-notify.vm"));
		bookingPostPaidCreatedResource = new StringTemplateResource("BookingPostPaidCreated", 
																	ContentUtil.get("br/com/atilo/jcondo/booking/mail/booking-created-notify.vm"));
		bookingPrePaidCancelledResource = new StringTemplateResource("BookingPrePaidCancelled", 
																	ContentUtil.get("br/com/atilo/jcondo/booking/mail/prepaid/booking-cancelled-notify.vm"));
		bookingPostPaidCancelledResource = new StringTemplateResource("BookingPostPaidCancelled", 
				   													ContentUtil.get("br/com/atilo/jcondo/booking/mail/booking-cancelled-notify.vm"));
		bookingPrePaidDeletedResource = new StringTemplateResource("BookingPrePaidDeleted", 
				   													ContentUtil.get("br/com/atilo/jcondo/booking/mail/prepaid/booking-deleted-notify.vm"));
		bookingPostPaidDeletedResource = new StringTemplateResource("BookingPostPaidDeleted", 
				   													ContentUtil.get("br/com/atilo/jcondo/booking/mail/booking-deleted-notify.vm"));
		bookingNotPaidResource = new StringTemplateResource("BookingNotPaid", 
				   											ContentUtil.get("br/com/atilo/jcondo/booking/mail/prepaid/booking-notpaid-notify.vm"));
	}

	@Override
	public void onAfterCreate(Booking booking) throws ModelListenerException {
		notifyBookingCreate(booking);
	}

	@Override
	public void onAfterUpdate(Booking booking) throws ModelListenerException {
		
		if(booking.getStatus() == BookingStatus.SUSPENDED) {
			notifyBookingSuspend(booking);
		} else if(booking.getStatus() == BookingStatus.CANCELLED) {
			notifyBookingCancel(booking);
		} if(booking.getStatus() == BookingStatus.DELETED) {
			notifyBookingDelete(booking);
		}
	}

	@Override
	public void onAfterRemove(Booking booking) throws ModelListenerException {
		try {
			BookingNote note = booking.getNote();
			if (note != null && !note.getText().startsWith("Portal:")) {
				notifyBookingDelete(booking);
			}
		} catch (Exception e) {
			LOGGER.debug(e);
		}
	}

	private void notifyBookingCreate(Booking booking) {
		try {
			LOGGER.info("Enviando notificacao de criacao da reserva " + booking.getId());

			Template template;
			BookingInvoice invoice = booking.getInvoice();

			if (BookingInvoiceLocalServiceUtil.isPrePaidMode(invoice != null ? invoice.getId() : 0)) {
				template = TemplateManagerUtil.getTemplate("", bookingPrePaidCreatedResource, false);
			} else {
				template = TemplateManagerUtil.getTemplate("", bookingPostPaidCreatedResource, false);
			}

			template.put("booking", booking);
			template.put("date", DateFormatUtils.format(booking.getBeginDate(), "dd/MM/yyyy"));
			template.put("startTime", DateFormatUtils.format(booking.getBeginDate(), "HH:mm'h'"));
			template.put("endTime", DateFormatUtils.format(booking.getEndDate(), "HH:mm'h'"));
			template.put("deadline", DateFormatUtils.format(BookingLocalServiceUtil.getDeadlineDate(booking.getBeginDate()), "dd/MM/yyyy"));

			UnsyncStringWriter writer = new UnsyncStringWriter();

			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);

			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(rb.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(booking.getPerson().getEmail()));
			mailMessage.setSubject(rb.getString("booking.create.subject"));
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de criacao da reserva " + booking.getId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de criacao da reserva " + booking.getId());
		}
	}

	private void notifyBookingCancel(Booking booking) {
		try {
			LOGGER.info("Enviando notificacao de cancelamento da reserva " + booking.getId());

			Template template;
			BookingInvoice invoice = booking.getInvoice();

			if (BookingInvoiceLocalServiceUtil.isPrePaidMode(invoice != null ? invoice.getId() : 0)) {
				template = TemplateManagerUtil.getTemplate("", bookingPrePaidCancelledResource, false);
			} else {
				template = TemplateManagerUtil.getTemplate("", bookingPostPaidCancelledResource, false);
			}

			template.put("booking", booking);
			template.put("date", DateFormatUtils.format(booking.getBeginDate(), "dd/MM/yyyy"));
			template.put("startTime", DateFormatUtils.format(booking.getBeginDate(), "HH:mm'h'"));
			template.put("endTime", DateFormatUtils.format(booking.getEndDate(), "HH:mm'h'"));
			template.put("showDeadline", new Date().after(BookingLocalServiceUtil.getDeadlineDate(booking.getBeginDate())));
			template.put("deadline", DateFormatUtils.format(BookingLocalServiceUtil.getDeadlineDate(booking.getBeginDate()), "dd/MM/yyyy"));

			UnsyncStringWriter writer = new UnsyncStringWriter();

			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);

			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(rb.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(booking.getPerson().getEmail()));
			mailMessage.setSubject(rb.getString("booking.cancel.subject"));
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de cancelamento da reserva " + booking.getId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de cancelamento da reserva " + booking.getId());
		}
	}

	private void notifyBookingDelete(Booking booking) {
		try {
			LOGGER.info("Enviando notificacao de exclusao da reserva " + booking.getId());

			Template template;
			BookingInvoice invoice = booking.getInvoice();

			if (BookingInvoiceLocalServiceUtil.isPrePaidMode(invoice != null ? invoice.getId() : 0)) {
				template = TemplateManagerUtil.getTemplate("", bookingPrePaidDeletedResource, false);
			} else {
				template = TemplateManagerUtil.getTemplate("", bookingPostPaidDeletedResource, false);
			}

			template.put("booking", booking);
			template.put("date", DateFormatUtils.format(booking.getBeginDate(), "dd/MM/yyyy"));
			template.put("startTime", DateFormatUtils.format(booking.getBeginDate(), "HH:mm'h'"));
			template.put("endTime", DateFormatUtils.format(booking.getEndDate(), "HH:mm'h'"));
			template.put("deadline", DateFormatUtils.format(BookingLocalServiceUtil.getDeadlineDate(booking.getBeginDate()), "dd/MM/yyyy"));

			UnsyncStringWriter writer = new UnsyncStringWriter();

			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);

			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(rb.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(booking.getPerson().getEmail()));
			mailMessage.setSubject(rb.getString("booking.delete.subject"));
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de exclusao da reserva " + booking.getId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de exclusao da reserva " + booking.getId());
		}
	}

	private void notifyBookingSuspend(Booking booking) {
		try {
			LOGGER.info("Enviando notificacao de suspensao da reserva " + booking.getId());

			Template template = TemplateManagerUtil.getTemplate("", bookingNotPaidResource, false);

			template.put("booking", booking);
			template.put("date", DateFormatUtils.format(booking.getBeginDate(), "dd/MM/yyyy"));
			template.put("startTime", DateFormatUtils.format(booking.getBeginDate(), "HH:mm'h'"));
			template.put("endTime", DateFormatUtils.format(booking.getEndDate(), "HH:mm'h'"));

			UnsyncStringWriter writer = new UnsyncStringWriter();

			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);

			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(rb.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(booking.getPerson().getEmail()));
			mailMessage.setSubject(rb.getString("booking.suspend.subject"));
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de suspensao da reserva " + booking.getId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de suspensao da reserva " + booking.getId());
		}
	}
}
