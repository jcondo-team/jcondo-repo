package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.NoSuchBookingNoteException;
import br.com.atilo.jcondo.booking.model.BookingNote;
import br.com.atilo.jcondo.booking.model.impl.BookingNoteImpl;
import br.com.atilo.jcondo.booking.model.impl.BookingNoteModelImpl;
import br.com.atilo.jcondo.booking.service.persistence.BookingNotePersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the booking note service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see BookingNotePersistence
 * @see BookingNoteUtil
 * @generated
 */
public class BookingNotePersistenceImpl extends BasePersistenceImpl<BookingNote>
    implements BookingNotePersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link BookingNoteUtil} to access the booking note persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = BookingNoteImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
            BookingNoteModelImpl.FINDER_CACHE_ENABLED, BookingNoteImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
            BookingNoteModelImpl.FINDER_CACHE_ENABLED, BookingNoteImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
            BookingNoteModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    private static final String _SQL_SELECT_BOOKINGNOTE = "SELECT bookingNote FROM BookingNote bookingNote";
    private static final String _SQL_COUNT_BOOKINGNOTE = "SELECT COUNT(bookingNote) FROM BookingNote bookingNote";
    private static final String _ORDER_BY_ENTITY_ALIAS = "bookingNote.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No BookingNote exists with the primary key ";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(BookingNotePersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static BookingNote _nullBookingNote = new BookingNoteImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<BookingNote> toCacheModel() {
                return _nullBookingNoteCacheModel;
            }
        };

    private static CacheModel<BookingNote> _nullBookingNoteCacheModel = new CacheModel<BookingNote>() {
            @Override
            public BookingNote toEntityModel() {
                return _nullBookingNote;
            }
        };

    public BookingNotePersistenceImpl() {
        setModelClass(BookingNote.class);
    }

    /**
     * Caches the booking note in the entity cache if it is enabled.
     *
     * @param bookingNote the booking note
     */
    @Override
    public void cacheResult(BookingNote bookingNote) {
        EntityCacheUtil.putResult(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
            BookingNoteImpl.class, bookingNote.getPrimaryKey(), bookingNote);

        bookingNote.resetOriginalValues();
    }

    /**
     * Caches the booking notes in the entity cache if it is enabled.
     *
     * @param bookingNotes the booking notes
     */
    @Override
    public void cacheResult(List<BookingNote> bookingNotes) {
        for (BookingNote bookingNote : bookingNotes) {
            if (EntityCacheUtil.getResult(
                        BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
                        BookingNoteImpl.class, bookingNote.getPrimaryKey()) == null) {
                cacheResult(bookingNote);
            } else {
                bookingNote.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all booking notes.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(BookingNoteImpl.class.getName());
        }

        EntityCacheUtil.clearCache(BookingNoteImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the booking note.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(BookingNote bookingNote) {
        EntityCacheUtil.removeResult(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
            BookingNoteImpl.class, bookingNote.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    @Override
    public void clearCache(List<BookingNote> bookingNotes) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (BookingNote bookingNote : bookingNotes) {
            EntityCacheUtil.removeResult(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
                BookingNoteImpl.class, bookingNote.getPrimaryKey());
        }
    }

    /**
     * Creates a new booking note with the primary key. Does not add the booking note to the database.
     *
     * @param id the primary key for the new booking note
     * @return the new booking note
     */
    @Override
    public BookingNote create(long id) {
        BookingNote bookingNote = new BookingNoteImpl();

        bookingNote.setNew(true);
        bookingNote.setPrimaryKey(id);

        return bookingNote;
    }

    /**
     * Removes the booking note with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the booking note
     * @return the booking note that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingNoteException if a booking note with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingNote remove(long id)
        throws NoSuchBookingNoteException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the booking note with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the booking note
     * @return the booking note that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingNoteException if a booking note with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingNote remove(Serializable primaryKey)
        throws NoSuchBookingNoteException, SystemException {
        Session session = null;

        try {
            session = openSession();

            BookingNote bookingNote = (BookingNote) session.get(BookingNoteImpl.class,
                    primaryKey);

            if (bookingNote == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchBookingNoteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(bookingNote);
        } catch (NoSuchBookingNoteException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected BookingNote removeImpl(BookingNote bookingNote)
        throws SystemException {
        bookingNote = toUnwrappedModel(bookingNote);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(bookingNote)) {
                bookingNote = (BookingNote) session.get(BookingNoteImpl.class,
                        bookingNote.getPrimaryKeyObj());
            }

            if (bookingNote != null) {
                session.delete(bookingNote);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (bookingNote != null) {
            clearCache(bookingNote);
        }

        return bookingNote;
    }

    @Override
    public BookingNote updateImpl(
        br.com.atilo.jcondo.booking.model.BookingNote bookingNote)
        throws SystemException {
        bookingNote = toUnwrappedModel(bookingNote);

        boolean isNew = bookingNote.isNew();

        Session session = null;

        try {
            session = openSession();

            if (bookingNote.isNew()) {
                session.save(bookingNote);

                bookingNote.setNew(false);
            } else {
                session.merge(bookingNote);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }

        EntityCacheUtil.putResult(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
            BookingNoteImpl.class, bookingNote.getPrimaryKey(), bookingNote);

        return bookingNote;
    }

    protected BookingNote toUnwrappedModel(BookingNote bookingNote) {
        if (bookingNote instanceof BookingNoteImpl) {
            return bookingNote;
        }

        BookingNoteImpl bookingNoteImpl = new BookingNoteImpl();

        bookingNoteImpl.setNew(bookingNote.isNew());
        bookingNoteImpl.setPrimaryKey(bookingNote.getPrimaryKey());

        bookingNoteImpl.setId(bookingNote.getId());
        bookingNoteImpl.setText(bookingNote.getText());
        bookingNoteImpl.setOprDate(bookingNote.getOprDate());
        bookingNoteImpl.setOprUser(bookingNote.getOprUser());

        return bookingNoteImpl;
    }

    /**
     * Returns the booking note with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the booking note
     * @return the booking note
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingNoteException if a booking note with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingNote findByPrimaryKey(Serializable primaryKey)
        throws NoSuchBookingNoteException, SystemException {
        BookingNote bookingNote = fetchByPrimaryKey(primaryKey);

        if (bookingNote == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchBookingNoteException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return bookingNote;
    }

    /**
     * Returns the booking note with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchBookingNoteException} if it could not be found.
     *
     * @param id the primary key of the booking note
     * @return the booking note
     * @throws br.com.atilo.jcondo.booking.NoSuchBookingNoteException if a booking note with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingNote findByPrimaryKey(long id)
        throws NoSuchBookingNoteException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the booking note with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the booking note
     * @return the booking note, or <code>null</code> if a booking note with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingNote fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        BookingNote bookingNote = (BookingNote) EntityCacheUtil.getResult(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
                BookingNoteImpl.class, primaryKey);

        if (bookingNote == _nullBookingNote) {
            return null;
        }

        if (bookingNote == null) {
            Session session = null;

            try {
                session = openSession();

                bookingNote = (BookingNote) session.get(BookingNoteImpl.class,
                        primaryKey);

                if (bookingNote != null) {
                    cacheResult(bookingNote);
                } else {
                    EntityCacheUtil.putResult(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
                        BookingNoteImpl.class, primaryKey, _nullBookingNote);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(BookingNoteModelImpl.ENTITY_CACHE_ENABLED,
                    BookingNoteImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return bookingNote;
    }

    /**
     * Returns the booking note with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the booking note
     * @return the booking note, or <code>null</code> if a booking note with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public BookingNote fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the booking notes.
     *
     * @return the booking notes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<BookingNote> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the booking notes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of booking notes
     * @param end the upper bound of the range of booking notes (not inclusive)
     * @return the range of booking notes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<BookingNote> findAll(int start, int end)
        throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the booking notes.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.BookingNoteModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of booking notes
     * @param end the upper bound of the range of booking notes (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of booking notes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<BookingNote> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<BookingNote> list = (List<BookingNote>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_BOOKINGNOTE);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_BOOKINGNOTE;

                if (pagination) {
                    sql = sql.concat(BookingNoteModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<BookingNote>) QueryUtil.list(q, getDialect(),
                            start, end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<BookingNote>(list);
                } else {
                    list = (List<BookingNote>) QueryUtil.list(q, getDialect(),
                            start, end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the booking notes from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (BookingNote bookingNote : findAll()) {
            remove(bookingNote);
        }
    }

    /**
     * Returns the number of booking notes.
     *
     * @return the number of booking notes
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_BOOKINGNOTE);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the booking note persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.booking.model.BookingNote")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<BookingNote>> listenersList = new ArrayList<ModelListener<BookingNote>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<BookingNote>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(BookingNoteImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
