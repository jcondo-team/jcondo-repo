package br.com.atilo.jcondo.booking.service.persistence;

import br.com.atilo.jcondo.booking.NoSuchRoomException;
import br.com.atilo.jcondo.booking.model.Room;
import br.com.atilo.jcondo.booking.model.impl.RoomImpl;
import br.com.atilo.jcondo.booking.model.impl.RoomModelImpl;
import br.com.atilo.jcondo.booking.service.persistence.RoomPersistence;

import com.liferay.portal.kernel.cache.CacheRegistryUtil;
import com.liferay.portal.kernel.dao.orm.EntityCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderCacheUtil;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.util.GetterUtil;
import com.liferay.portal.kernel.util.InstanceFactory;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.PropsKeys;
import com.liferay.portal.kernel.util.PropsUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.kernel.util.StringPool;
import com.liferay.portal.kernel.util.StringUtil;
import com.liferay.portal.kernel.util.UnmodifiableList;
import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.CacheModel;
import com.liferay.portal.model.ModelListener;
import com.liferay.portal.service.persistence.impl.BasePersistenceImpl;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * The persistence implementation for the room service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see RoomPersistence
 * @see RoomUtil
 * @generated
 */
public class RoomPersistenceImpl extends BasePersistenceImpl<Room>
    implements RoomPersistence {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this class directly. Always use {@link RoomUtil} to access the room persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */
    public static final String FINDER_CLASS_NAME_ENTITY = RoomImpl.class.getName();
    public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List1";
    public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
        ".List2";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
    public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
    public static final FinderPath FINDER_PATH_FETCH_BY_NAME = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
            FINDER_CLASS_NAME_ENTITY, "fetchByName",
            new String[] { String.class.getName() },
            RoomModelImpl.NAME_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_NAME = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByName",
            new String[] { String.class.getName() });
    private static final String _FINDER_COLUMN_NAME_NAME_1 = "room.name IS NULL";
    private static final String _FINDER_COLUMN_NAME_NAME_2 = "room.name = ?";
    private static final String _FINDER_COLUMN_NAME_NAME_3 = "(room.name IS NULL OR room.name = '')";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKABLE = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByBookable",
            new String[] {
                Boolean.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLE =
        new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByBookable",
            new String[] { Boolean.class.getName() },
            RoomModelImpl.BOOKABLE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_BOOKABLE = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByBookable",
            new String[] { Boolean.class.getName() });
    private static final String _FINDER_COLUMN_BOOKABLE_BOOKABLE_2 = "room.bookable = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_AVAILABLE =
        new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByAvailable",
            new String[] {
                Boolean.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AVAILABLE =
        new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByAvailable",
            new String[] { Boolean.class.getName() },
            RoomModelImpl.AVAILABLE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_AVAILABLE = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByAvailable",
            new String[] { Boolean.class.getName() });
    private static final String _FINDER_COLUMN_AVAILABLE_AVAILABLE_2 = "room.available = ?";
    public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKABLEANDAVAILABLE =
        new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
            FINDER_CLASS_NAME_LIST_WITH_PAGINATION,
            "findByBookableAndAvailable",
            new String[] {
                Boolean.class.getName(), Boolean.class.getName(),
                
            Integer.class.getName(), Integer.class.getName(),
                OrderByComparator.class.getName()
            });
    public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLEANDAVAILABLE =
        new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, RoomImpl.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "findByBookableAndAvailable",
            new String[] { Boolean.class.getName(), Boolean.class.getName() },
            RoomModelImpl.BOOKABLE_COLUMN_BITMASK |
            RoomModelImpl.AVAILABLE_COLUMN_BITMASK);
    public static final FinderPath FINDER_PATH_COUNT_BY_BOOKABLEANDAVAILABLE = new FinderPath(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomModelImpl.FINDER_CACHE_ENABLED, Long.class,
            FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION,
            "countByBookableAndAvailable",
            new String[] { Boolean.class.getName(), Boolean.class.getName() });
    private static final String _FINDER_COLUMN_BOOKABLEANDAVAILABLE_BOOKABLE_2 = "room.bookable = ? AND ";
    private static final String _FINDER_COLUMN_BOOKABLEANDAVAILABLE_AVAILABLE_2 = "room.available = ?";
    private static final String _SQL_SELECT_ROOM = "SELECT room FROM Room room";
    private static final String _SQL_SELECT_ROOM_WHERE = "SELECT room FROM Room room WHERE ";
    private static final String _SQL_COUNT_ROOM = "SELECT COUNT(room) FROM Room room";
    private static final String _SQL_COUNT_ROOM_WHERE = "SELECT COUNT(room) FROM Room room WHERE ";
    private static final String _ORDER_BY_ENTITY_ALIAS = "room.";
    private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No Room exists with the primary key ";
    private static final String _NO_SUCH_ENTITY_WITH_KEY = "No Room exists with the key {";
    private static final boolean _HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE = GetterUtil.getBoolean(PropsUtil.get(
                PropsKeys.HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE));
    private static Log _log = LogFactoryUtil.getLog(RoomPersistenceImpl.class);
    private static Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
                "id"
            });
    private static Room _nullRoom = new RoomImpl() {
            @Override
            public Object clone() {
                return this;
            }

            @Override
            public CacheModel<Room> toCacheModel() {
                return _nullRoomCacheModel;
            }
        };

    private static CacheModel<Room> _nullRoomCacheModel = new CacheModel<Room>() {
            @Override
            public Room toEntityModel() {
                return _nullRoom;
            }
        };

    public RoomPersistenceImpl() {
        setModelClass(Room.class);
    }

    /**
     * Returns the room where name = &#63; or throws a {@link br.com.atilo.jcondo.booking.NoSuchRoomException} if it could not be found.
     *
     * @param name the name
     * @return the matching room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room findByName(String name)
        throws NoSuchRoomException, SystemException {
        Room room = fetchByName(name);

        if (room == null) {
            StringBundler msg = new StringBundler(4);

            msg.append(_NO_SUCH_ENTITY_WITH_KEY);

            msg.append("name=");
            msg.append(name);

            msg.append(StringPool.CLOSE_CURLY_BRACE);

            if (_log.isWarnEnabled()) {
                _log.warn(msg.toString());
            }

            throw new NoSuchRoomException(msg.toString());
        }

        return room;
    }

    /**
     * Returns the room where name = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
     *
     * @param name the name
     * @return the matching room, or <code>null</code> if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByName(String name) throws SystemException {
        return fetchByName(name, true);
    }

    /**
     * Returns the room where name = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
     *
     * @param name the name
     * @param retrieveFromCache whether to use the finder cache
     * @return the matching room, or <code>null</code> if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByName(String name, boolean retrieveFromCache)
        throws SystemException {
        Object[] finderArgs = new Object[] { name };

        Object result = null;

        if (retrieveFromCache) {
            result = FinderCacheUtil.getResult(FINDER_PATH_FETCH_BY_NAME,
                    finderArgs, this);
        }

        if (result instanceof Room) {
            Room room = (Room) result;

            if (!Validator.equals(name, room.getName())) {
                result = null;
            }
        }

        if (result == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_SELECT_ROOM_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name);
                }

                List<Room> list = q.list();

                if (list.isEmpty()) {
                    FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME,
                        finderArgs, list);
                } else {
                    if ((list.size() > 1) && _log.isWarnEnabled()) {
                        _log.warn(
                            "RoomPersistenceImpl.fetchByName(String, boolean) with parameters (" +
                            StringUtil.merge(finderArgs) +
                            ") yields a result set with more than 1 result. This violates the logical unique restriction. There is no order guarantee on which result is returned by this finder.");
                    }

                    Room room = list.get(0);

                    result = room;

                    cacheResult(room);

                    if ((room.getName() == null) ||
                            !room.getName().equals(name)) {
                        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME,
                            finderArgs, room);
                    }
                }
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NAME,
                    finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        if (result instanceof List<?>) {
            return null;
        } else {
            return (Room) result;
        }
    }

    /**
     * Removes the room where name = &#63; from the database.
     *
     * @param name the name
     * @return the room that was removed
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room removeByName(String name)
        throws NoSuchRoomException, SystemException {
        Room room = findByName(name);

        return remove(room);
    }

    /**
     * Returns the number of rooms where name = &#63;.
     *
     * @param name the name
     * @return the number of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByName(String name) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_NAME;

        Object[] finderArgs = new Object[] { name };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ROOM_WHERE);

            boolean bindName = false;

            if (name == null) {
                query.append(_FINDER_COLUMN_NAME_NAME_1);
            } else if (name.equals(StringPool.BLANK)) {
                query.append(_FINDER_COLUMN_NAME_NAME_3);
            } else {
                bindName = true;

                query.append(_FINDER_COLUMN_NAME_NAME_2);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                if (bindName) {
                    qPos.add(name);
                }

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the rooms where bookable = &#63;.
     *
     * @param bookable the bookable
     * @return the matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findByBookable(boolean bookable)
        throws SystemException {
        return findByBookable(bookable, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the rooms where bookable = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param bookable the bookable
     * @param start the lower bound of the range of rooms
     * @param end the upper bound of the range of rooms (not inclusive)
     * @return the range of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findByBookable(boolean bookable, int start, int end)
        throws SystemException {
        return findByBookable(bookable, start, end, null);
    }

    /**
     * Returns an ordered range of all the rooms where bookable = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param bookable the bookable
     * @param start the lower bound of the range of rooms
     * @param end the upper bound of the range of rooms (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findByBookable(boolean bookable, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLE;
            finderArgs = new Object[] { bookable };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKABLE;
            finderArgs = new Object[] { bookable, start, end, orderByComparator };
        }

        List<Room> list = (List<Room>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Room room : list) {
                if ((bookable != room.getBookable())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_ROOM_WHERE);

            query.append(_FINDER_COLUMN_BOOKABLE_BOOKABLE_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(RoomModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(bookable);

                if (!pagination) {
                    list = (List<Room>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Room>(list);
                } else {
                    list = (List<Room>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first room in the ordered set where bookable = &#63;.
     *
     * @param bookable the bookable
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room findByBookable_First(boolean bookable,
        OrderByComparator orderByComparator)
        throws NoSuchRoomException, SystemException {
        Room room = fetchByBookable_First(bookable, orderByComparator);

        if (room != null) {
            return room;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("bookable=");
        msg.append(bookable);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomException(msg.toString());
    }

    /**
     * Returns the first room in the ordered set where bookable = &#63;.
     *
     * @param bookable the bookable
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room, or <code>null</code> if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByBookable_First(boolean bookable,
        OrderByComparator orderByComparator) throws SystemException {
        List<Room> list = findByBookable(bookable, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last room in the ordered set where bookable = &#63;.
     *
     * @param bookable the bookable
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room findByBookable_Last(boolean bookable,
        OrderByComparator orderByComparator)
        throws NoSuchRoomException, SystemException {
        Room room = fetchByBookable_Last(bookable, orderByComparator);

        if (room != null) {
            return room;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("bookable=");
        msg.append(bookable);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomException(msg.toString());
    }

    /**
     * Returns the last room in the ordered set where bookable = &#63;.
     *
     * @param bookable the bookable
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room, or <code>null</code> if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByBookable_Last(boolean bookable,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByBookable(bookable);

        if (count == 0) {
            return null;
        }

        List<Room> list = findByBookable(bookable, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the rooms before and after the current room in the ordered set where bookable = &#63;.
     *
     * @param id the primary key of the current room
     * @param bookable the bookable
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room[] findByBookable_PrevAndNext(long id, boolean bookable,
        OrderByComparator orderByComparator)
        throws NoSuchRoomException, SystemException {
        Room room = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Room[] array = new RoomImpl[3];

            array[0] = getByBookable_PrevAndNext(session, room, bookable,
                    orderByComparator, true);

            array[1] = room;

            array[2] = getByBookable_PrevAndNext(session, room, bookable,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Room getByBookable_PrevAndNext(Session session, Room room,
        boolean bookable, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ROOM_WHERE);

        query.append(_FINDER_COLUMN_BOOKABLE_BOOKABLE_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(RoomModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(bookable);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(room);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Room> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the rooms where bookable = &#63; from the database.
     *
     * @param bookable the bookable
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByBookable(boolean bookable) throws SystemException {
        for (Room room : findByBookable(bookable, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(room);
        }
    }

    /**
     * Returns the number of rooms where bookable = &#63;.
     *
     * @param bookable the bookable
     * @return the number of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByBookable(boolean bookable) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_BOOKABLE;

        Object[] finderArgs = new Object[] { bookable };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ROOM_WHERE);

            query.append(_FINDER_COLUMN_BOOKABLE_BOOKABLE_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(bookable);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the rooms where available = &#63;.
     *
     * @param available the available
     * @return the matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findByAvailable(boolean available)
        throws SystemException {
        return findByAvailable(available, QueryUtil.ALL_POS, QueryUtil.ALL_POS,
            null);
    }

    /**
     * Returns a range of all the rooms where available = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param available the available
     * @param start the lower bound of the range of rooms
     * @param end the upper bound of the range of rooms (not inclusive)
     * @return the range of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findByAvailable(boolean available, int start, int end)
        throws SystemException {
        return findByAvailable(available, start, end, null);
    }

    /**
     * Returns an ordered range of all the rooms where available = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param available the available
     * @param start the lower bound of the range of rooms
     * @param end the upper bound of the range of rooms (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findByAvailable(boolean available, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AVAILABLE;
            finderArgs = new Object[] { available };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_AVAILABLE;
            finderArgs = new Object[] { available, start, end, orderByComparator };
        }

        List<Room> list = (List<Room>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Room room : list) {
                if ((available != room.getAvailable())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(3 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(3);
            }

            query.append(_SQL_SELECT_ROOM_WHERE);

            query.append(_FINDER_COLUMN_AVAILABLE_AVAILABLE_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(RoomModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(available);

                if (!pagination) {
                    list = (List<Room>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Room>(list);
                } else {
                    list = (List<Room>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first room in the ordered set where available = &#63;.
     *
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room findByAvailable_First(boolean available,
        OrderByComparator orderByComparator)
        throws NoSuchRoomException, SystemException {
        Room room = fetchByAvailable_First(available, orderByComparator);

        if (room != null) {
            return room;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("available=");
        msg.append(available);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomException(msg.toString());
    }

    /**
     * Returns the first room in the ordered set where available = &#63;.
     *
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room, or <code>null</code> if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByAvailable_First(boolean available,
        OrderByComparator orderByComparator) throws SystemException {
        List<Room> list = findByAvailable(available, 0, 1, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last room in the ordered set where available = &#63;.
     *
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room findByAvailable_Last(boolean available,
        OrderByComparator orderByComparator)
        throws NoSuchRoomException, SystemException {
        Room room = fetchByAvailable_Last(available, orderByComparator);

        if (room != null) {
            return room;
        }

        StringBundler msg = new StringBundler(4);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("available=");
        msg.append(available);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomException(msg.toString());
    }

    /**
     * Returns the last room in the ordered set where available = &#63;.
     *
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room, or <code>null</code> if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByAvailable_Last(boolean available,
        OrderByComparator orderByComparator) throws SystemException {
        int count = countByAvailable(available);

        if (count == 0) {
            return null;
        }

        List<Room> list = findByAvailable(available, count - 1, count,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the rooms before and after the current room in the ordered set where available = &#63;.
     *
     * @param id the primary key of the current room
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room[] findByAvailable_PrevAndNext(long id, boolean available,
        OrderByComparator orderByComparator)
        throws NoSuchRoomException, SystemException {
        Room room = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Room[] array = new RoomImpl[3];

            array[0] = getByAvailable_PrevAndNext(session, room, available,
                    orderByComparator, true);

            array[1] = room;

            array[2] = getByAvailable_PrevAndNext(session, room, available,
                    orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Room getByAvailable_PrevAndNext(Session session, Room room,
        boolean available, OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ROOM_WHERE);

        query.append(_FINDER_COLUMN_AVAILABLE_AVAILABLE_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(RoomModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(available);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(room);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Room> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the rooms where available = &#63; from the database.
     *
     * @param available the available
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByAvailable(boolean available) throws SystemException {
        for (Room room : findByAvailable(available, QueryUtil.ALL_POS,
                QueryUtil.ALL_POS, null)) {
            remove(room);
        }
    }

    /**
     * Returns the number of rooms where available = &#63;.
     *
     * @param available the available
     * @return the number of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByAvailable(boolean available) throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_AVAILABLE;

        Object[] finderArgs = new Object[] { available };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(2);

            query.append(_SQL_COUNT_ROOM_WHERE);

            query.append(_FINDER_COLUMN_AVAILABLE_AVAILABLE_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(available);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Returns all the rooms where bookable = &#63; and available = &#63;.
     *
     * @param bookable the bookable
     * @param available the available
     * @return the matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findByBookableAndAvailable(boolean bookable,
        boolean available) throws SystemException {
        return findByBookableAndAvailable(bookable, available,
            QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the rooms where bookable = &#63; and available = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param bookable the bookable
     * @param available the available
     * @param start the lower bound of the range of rooms
     * @param end the upper bound of the range of rooms (not inclusive)
     * @return the range of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findByBookableAndAvailable(boolean bookable,
        boolean available, int start, int end) throws SystemException {
        return findByBookableAndAvailable(bookable, available, start, end, null);
    }

    /**
     * Returns an ordered range of all the rooms where bookable = &#63; and available = &#63;.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param bookable the bookable
     * @param available the available
     * @param start the lower bound of the range of rooms
     * @param end the upper bound of the range of rooms (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findByBookableAndAvailable(boolean bookable,
        boolean available, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLEANDAVAILABLE;
            finderArgs = new Object[] { bookable, available };
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_BOOKABLEANDAVAILABLE;
            finderArgs = new Object[] {
                    bookable, available,
                    
                    start, end, orderByComparator
                };
        }

        List<Room> list = (List<Room>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if ((list != null) && !list.isEmpty()) {
            for (Room room : list) {
                if ((bookable != room.getBookable()) ||
                        (available != room.getAvailable())) {
                    list = null;

                    break;
                }
            }
        }

        if (list == null) {
            StringBundler query = null;

            if (orderByComparator != null) {
                query = new StringBundler(4 +
                        (orderByComparator.getOrderByFields().length * 3));
            } else {
                query = new StringBundler(4);
            }

            query.append(_SQL_SELECT_ROOM_WHERE);

            query.append(_FINDER_COLUMN_BOOKABLEANDAVAILABLE_BOOKABLE_2);

            query.append(_FINDER_COLUMN_BOOKABLEANDAVAILABLE_AVAILABLE_2);

            if (orderByComparator != null) {
                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);
            } else
             if (pagination) {
                query.append(RoomModelImpl.ORDER_BY_JPQL);
            }

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(bookable);

                qPos.add(available);

                if (!pagination) {
                    list = (List<Room>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Room>(list);
                } else {
                    list = (List<Room>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Returns the first room in the ordered set where bookable = &#63; and available = &#63;.
     *
     * @param bookable the bookable
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room findByBookableAndAvailable_First(boolean bookable,
        boolean available, OrderByComparator orderByComparator)
        throws NoSuchRoomException, SystemException {
        Room room = fetchByBookableAndAvailable_First(bookable, available,
                orderByComparator);

        if (room != null) {
            return room;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("bookable=");
        msg.append(bookable);

        msg.append(", available=");
        msg.append(available);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomException(msg.toString());
    }

    /**
     * Returns the first room in the ordered set where bookable = &#63; and available = &#63;.
     *
     * @param bookable the bookable
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the first matching room, or <code>null</code> if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByBookableAndAvailable_First(boolean bookable,
        boolean available, OrderByComparator orderByComparator)
        throws SystemException {
        List<Room> list = findByBookableAndAvailable(bookable, available, 0, 1,
                orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the last room in the ordered set where bookable = &#63; and available = &#63;.
     *
     * @param bookable the bookable
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room findByBookableAndAvailable_Last(boolean bookable,
        boolean available, OrderByComparator orderByComparator)
        throws NoSuchRoomException, SystemException {
        Room room = fetchByBookableAndAvailable_Last(bookable, available,
                orderByComparator);

        if (room != null) {
            return room;
        }

        StringBundler msg = new StringBundler(6);

        msg.append(_NO_SUCH_ENTITY_WITH_KEY);

        msg.append("bookable=");
        msg.append(bookable);

        msg.append(", available=");
        msg.append(available);

        msg.append(StringPool.CLOSE_CURLY_BRACE);

        throw new NoSuchRoomException(msg.toString());
    }

    /**
     * Returns the last room in the ordered set where bookable = &#63; and available = &#63;.
     *
     * @param bookable the bookable
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the last matching room, or <code>null</code> if a matching room could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByBookableAndAvailable_Last(boolean bookable,
        boolean available, OrderByComparator orderByComparator)
        throws SystemException {
        int count = countByBookableAndAvailable(bookable, available);

        if (count == 0) {
            return null;
        }

        List<Room> list = findByBookableAndAvailable(bookable, available,
                count - 1, count, orderByComparator);

        if (!list.isEmpty()) {
            return list.get(0);
        }

        return null;
    }

    /**
     * Returns the rooms before and after the current room in the ordered set where bookable = &#63; and available = &#63;.
     *
     * @param id the primary key of the current room
     * @param bookable the bookable
     * @param available the available
     * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
     * @return the previous, current, and next room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room[] findByBookableAndAvailable_PrevAndNext(long id,
        boolean bookable, boolean available, OrderByComparator orderByComparator)
        throws NoSuchRoomException, SystemException {
        Room room = findByPrimaryKey(id);

        Session session = null;

        try {
            session = openSession();

            Room[] array = new RoomImpl[3];

            array[0] = getByBookableAndAvailable_PrevAndNext(session, room,
                    bookable, available, orderByComparator, true);

            array[1] = room;

            array[2] = getByBookableAndAvailable_PrevAndNext(session, room,
                    bookable, available, orderByComparator, false);

            return array;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    protected Room getByBookableAndAvailable_PrevAndNext(Session session,
        Room room, boolean bookable, boolean available,
        OrderByComparator orderByComparator, boolean previous) {
        StringBundler query = null;

        if (orderByComparator != null) {
            query = new StringBundler(6 +
                    (orderByComparator.getOrderByFields().length * 6));
        } else {
            query = new StringBundler(3);
        }

        query.append(_SQL_SELECT_ROOM_WHERE);

        query.append(_FINDER_COLUMN_BOOKABLEANDAVAILABLE_BOOKABLE_2);

        query.append(_FINDER_COLUMN_BOOKABLEANDAVAILABLE_AVAILABLE_2);

        if (orderByComparator != null) {
            String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

            if (orderByConditionFields.length > 0) {
                query.append(WHERE_AND);
            }

            for (int i = 0; i < orderByConditionFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByConditionFields[i]);

                if ((i + 1) < orderByConditionFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN_HAS_NEXT);
                    } else {
                        query.append(WHERE_LESSER_THAN_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(WHERE_GREATER_THAN);
                    } else {
                        query.append(WHERE_LESSER_THAN);
                    }
                }
            }

            query.append(ORDER_BY_CLAUSE);

            String[] orderByFields = orderByComparator.getOrderByFields();

            for (int i = 0; i < orderByFields.length; i++) {
                query.append(_ORDER_BY_ENTITY_ALIAS);
                query.append(orderByFields[i]);

                if ((i + 1) < orderByFields.length) {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC_HAS_NEXT);
                    } else {
                        query.append(ORDER_BY_DESC_HAS_NEXT);
                    }
                } else {
                    if (orderByComparator.isAscending() ^ previous) {
                        query.append(ORDER_BY_ASC);
                    } else {
                        query.append(ORDER_BY_DESC);
                    }
                }
            }
        } else {
            query.append(RoomModelImpl.ORDER_BY_JPQL);
        }

        String sql = query.toString();

        Query q = session.createQuery(sql);

        q.setFirstResult(0);
        q.setMaxResults(2);

        QueryPos qPos = QueryPos.getInstance(q);

        qPos.add(bookable);

        qPos.add(available);

        if (orderByComparator != null) {
            Object[] values = orderByComparator.getOrderByConditionValues(room);

            for (Object value : values) {
                qPos.add(value);
            }
        }

        List<Room> list = q.list();

        if (list.size() == 2) {
            return list.get(1);
        } else {
            return null;
        }
    }

    /**
     * Removes all the rooms where bookable = &#63; and available = &#63; from the database.
     *
     * @param bookable the bookable
     * @param available the available
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeByBookableAndAvailable(boolean bookable, boolean available)
        throws SystemException {
        for (Room room : findByBookableAndAvailable(bookable, available,
                QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
            remove(room);
        }
    }

    /**
     * Returns the number of rooms where bookable = &#63; and available = &#63;.
     *
     * @param bookable the bookable
     * @param available the available
     * @return the number of matching rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countByBookableAndAvailable(boolean bookable, boolean available)
        throws SystemException {
        FinderPath finderPath = FINDER_PATH_COUNT_BY_BOOKABLEANDAVAILABLE;

        Object[] finderArgs = new Object[] { bookable, available };

        Long count = (Long) FinderCacheUtil.getResult(finderPath, finderArgs,
                this);

        if (count == null) {
            StringBundler query = new StringBundler(3);

            query.append(_SQL_COUNT_ROOM_WHERE);

            query.append(_FINDER_COLUMN_BOOKABLEANDAVAILABLE_BOOKABLE_2);

            query.append(_FINDER_COLUMN_BOOKABLEANDAVAILABLE_AVAILABLE_2);

            String sql = query.toString();

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                QueryPos qPos = QueryPos.getInstance(q);

                qPos.add(bookable);

                qPos.add(available);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(finderPath, finderArgs, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    /**
     * Caches the room in the entity cache if it is enabled.
     *
     * @param room the room
     */
    @Override
    public void cacheResult(Room room) {
        EntityCacheUtil.putResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomImpl.class, room.getPrimaryKey(), room);

        FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME,
            new Object[] { room.getName() }, room);

        room.resetOriginalValues();
    }

    /**
     * Caches the rooms in the entity cache if it is enabled.
     *
     * @param rooms the rooms
     */
    @Override
    public void cacheResult(List<Room> rooms) {
        for (Room room : rooms) {
            if (EntityCacheUtil.getResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
                        RoomImpl.class, room.getPrimaryKey()) == null) {
                cacheResult(room);
            } else {
                room.resetOriginalValues();
            }
        }
    }

    /**
     * Clears the cache for all rooms.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache() {
        if (_HIBERNATE_CACHE_USE_SECOND_LEVEL_CACHE) {
            CacheRegistryUtil.clear(RoomImpl.class.getName());
        }

        EntityCacheUtil.clearCache(RoomImpl.class.getName());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }

    /**
     * Clears the cache for the room.
     *
     * <p>
     * The {@link com.liferay.portal.kernel.dao.orm.EntityCache} and {@link com.liferay.portal.kernel.dao.orm.FinderCache} are both cleared by this method.
     * </p>
     */
    @Override
    public void clearCache(Room room) {
        EntityCacheUtil.removeResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomImpl.class, room.getPrimaryKey());

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        clearUniqueFindersCache(room);
    }

    @Override
    public void clearCache(List<Room> rooms) {
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

        for (Room room : rooms) {
            EntityCacheUtil.removeResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
                RoomImpl.class, room.getPrimaryKey());

            clearUniqueFindersCache(room);
        }
    }

    protected void cacheUniqueFindersCache(Room room) {
        if (room.isNew()) {
            Object[] args = new Object[] { room.getName() };

            FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NAME, args,
                Long.valueOf(1));
            FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME, args, room);
        } else {
            RoomModelImpl roomModelImpl = (RoomModelImpl) room;

            if ((roomModelImpl.getColumnBitmask() &
                    FINDER_PATH_FETCH_BY_NAME.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { room.getName() };

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_BY_NAME, args,
                    Long.valueOf(1));
                FinderCacheUtil.putResult(FINDER_PATH_FETCH_BY_NAME, args, room);
            }
        }
    }

    protected void clearUniqueFindersCache(Room room) {
        RoomModelImpl roomModelImpl = (RoomModelImpl) room;

        Object[] args = new Object[] { room.getName() };

        FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
        FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NAME, args);

        if ((roomModelImpl.getColumnBitmask() &
                FINDER_PATH_FETCH_BY_NAME.getColumnBitmask()) != 0) {
            args = new Object[] { roomModelImpl.getOriginalName() };

            FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_NAME, args);
            FinderCacheUtil.removeResult(FINDER_PATH_FETCH_BY_NAME, args);
        }
    }

    /**
     * Creates a new room with the primary key. Does not add the room to the database.
     *
     * @param id the primary key for the new room
     * @return the new room
     */
    @Override
    public Room create(long id) {
        Room room = new RoomImpl();

        room.setNew(true);
        room.setPrimaryKey(id);

        return room;
    }

    /**
     * Removes the room with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param id the primary key of the room
     * @return the room that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room remove(long id) throws NoSuchRoomException, SystemException {
        return remove((Serializable) id);
    }

    /**
     * Removes the room with the primary key from the database. Also notifies the appropriate model listeners.
     *
     * @param primaryKey the primary key of the room
     * @return the room that was removed
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room remove(Serializable primaryKey)
        throws NoSuchRoomException, SystemException {
        Session session = null;

        try {
            session = openSession();

            Room room = (Room) session.get(RoomImpl.class, primaryKey);

            if (room == null) {
                if (_log.isWarnEnabled()) {
                    _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
                }

                throw new NoSuchRoomException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                    primaryKey);
            }

            return remove(room);
        } catch (NoSuchRoomException nsee) {
            throw nsee;
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }
    }

    @Override
    protected Room removeImpl(Room room) throws SystemException {
        room = toUnwrappedModel(room);

        Session session = null;

        try {
            session = openSession();

            if (!session.contains(room)) {
                room = (Room) session.get(RoomImpl.class,
                        room.getPrimaryKeyObj());
            }

            if (room != null) {
                session.delete(room);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        if (room != null) {
            clearCache(room);
        }

        return room;
    }

    @Override
    public Room updateImpl(br.com.atilo.jcondo.booking.model.Room room)
        throws SystemException {
        room = toUnwrappedModel(room);

        boolean isNew = room.isNew();

        RoomModelImpl roomModelImpl = (RoomModelImpl) room;

        Session session = null;

        try {
            session = openSession();

            if (room.isNew()) {
                session.save(room);

                room.setNew(false);
            } else {
                session.merge(room);
            }
        } catch (Exception e) {
            throw processException(e);
        } finally {
            closeSession(session);
        }

        FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

        if (isNew || !RoomModelImpl.COLUMN_BITMASK_ENABLED) {
            FinderCacheUtil.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
        }
        else {
            if ((roomModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] { roomModelImpl.getOriginalBookable() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKABLE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLE,
                    args);

                args = new Object[] { roomModelImpl.getBookable() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKABLE, args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLE,
                    args);
            }

            if ((roomModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AVAILABLE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        roomModelImpl.getOriginalAvailable()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_AVAILABLE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AVAILABLE,
                    args);

                args = new Object[] { roomModelImpl.getAvailable() };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_AVAILABLE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_AVAILABLE,
                    args);
            }

            if ((roomModelImpl.getColumnBitmask() &
                    FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLEANDAVAILABLE.getColumnBitmask()) != 0) {
                Object[] args = new Object[] {
                        roomModelImpl.getOriginalBookable(),
                        roomModelImpl.getOriginalAvailable()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKABLEANDAVAILABLE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLEANDAVAILABLE,
                    args);

                args = new Object[] {
                        roomModelImpl.getBookable(),
                        roomModelImpl.getAvailable()
                    };

                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_BY_BOOKABLEANDAVAILABLE,
                    args);
                FinderCacheUtil.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_BOOKABLEANDAVAILABLE,
                    args);
            }
        }

        EntityCacheUtil.putResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
            RoomImpl.class, room.getPrimaryKey(), room);

        clearUniqueFindersCache(room);
        cacheUniqueFindersCache(room);

        return room;
    }

    protected Room toUnwrappedModel(Room room) {
        if (room instanceof RoomImpl) {
            return room;
        }

        RoomImpl roomImpl = new RoomImpl();

        roomImpl.setNew(room.isNew());
        roomImpl.setPrimaryKey(room.getPrimaryKey());

        roomImpl.setId(room.getId());
        roomImpl.setFolderId(room.getFolderId());
        roomImpl.setAgreementId(room.getAgreementId());
        roomImpl.setName(room.getName());
        roomImpl.setDescription(room.getDescription());
        roomImpl.setAvailable(room.isAvailable());
        roomImpl.setBookable(room.isBookable());
        roomImpl.setPrice(room.getPrice());
        roomImpl.setCapacity(room.getCapacity());
        roomImpl.setOprDate(room.getOprDate());
        roomImpl.setOprUser(room.getOprUser());

        return roomImpl;
    }

    /**
     * Returns the room with the primary key or throws a {@link com.liferay.portal.NoSuchModelException} if it could not be found.
     *
     * @param primaryKey the primary key of the room
     * @return the room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room findByPrimaryKey(Serializable primaryKey)
        throws NoSuchRoomException, SystemException {
        Room room = fetchByPrimaryKey(primaryKey);

        if (room == null) {
            if (_log.isWarnEnabled()) {
                _log.warn(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
            }

            throw new NoSuchRoomException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
                primaryKey);
        }

        return room;
    }

    /**
     * Returns the room with the primary key or throws a {@link br.com.atilo.jcondo.booking.NoSuchRoomException} if it could not be found.
     *
     * @param id the primary key of the room
     * @return the room
     * @throws br.com.atilo.jcondo.booking.NoSuchRoomException if a room with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room findByPrimaryKey(long id)
        throws NoSuchRoomException, SystemException {
        return findByPrimaryKey((Serializable) id);
    }

    /**
     * Returns the room with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param primaryKey the primary key of the room
     * @return the room, or <code>null</code> if a room with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByPrimaryKey(Serializable primaryKey)
        throws SystemException {
        Room room = (Room) EntityCacheUtil.getResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
                RoomImpl.class, primaryKey);

        if (room == _nullRoom) {
            return null;
        }

        if (room == null) {
            Session session = null;

            try {
                session = openSession();

                room = (Room) session.get(RoomImpl.class, primaryKey);

                if (room != null) {
                    cacheResult(room);
                } else {
                    EntityCacheUtil.putResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
                        RoomImpl.class, primaryKey, _nullRoom);
                }
            } catch (Exception e) {
                EntityCacheUtil.removeResult(RoomModelImpl.ENTITY_CACHE_ENABLED,
                    RoomImpl.class, primaryKey);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return room;
    }

    /**
     * Returns the room with the primary key or returns <code>null</code> if it could not be found.
     *
     * @param id the primary key of the room
     * @return the room, or <code>null</code> if a room with the primary key could not be found
     * @throws SystemException if a system exception occurred
     */
    @Override
    public Room fetchByPrimaryKey(long id) throws SystemException {
        return fetchByPrimaryKey((Serializable) id);
    }

    /**
     * Returns all the rooms.
     *
     * @return the rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findAll() throws SystemException {
        return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
    }

    /**
     * Returns a range of all the rooms.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of rooms
     * @param end the upper bound of the range of rooms (not inclusive)
     * @return the range of rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findAll(int start, int end) throws SystemException {
        return findAll(start, end, null);
    }

    /**
     * Returns an ordered range of all the rooms.
     *
     * <p>
     * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.booking.model.impl.RoomModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
     * </p>
     *
     * @param start the lower bound of the range of rooms
     * @param end the upper bound of the range of rooms (not inclusive)
     * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
     * @return the ordered range of rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public List<Room> findAll(int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        boolean pagination = true;
        FinderPath finderPath = null;
        Object[] finderArgs = null;

        if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
                (orderByComparator == null)) {
            pagination = false;
            finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
            finderArgs = FINDER_ARGS_EMPTY;
        } else {
            finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
            finderArgs = new Object[] { start, end, orderByComparator };
        }

        List<Room> list = (List<Room>) FinderCacheUtil.getResult(finderPath,
                finderArgs, this);

        if (list == null) {
            StringBundler query = null;
            String sql = null;

            if (orderByComparator != null) {
                query = new StringBundler(2 +
                        (orderByComparator.getOrderByFields().length * 3));

                query.append(_SQL_SELECT_ROOM);

                appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
                    orderByComparator);

                sql = query.toString();
            } else {
                sql = _SQL_SELECT_ROOM;

                if (pagination) {
                    sql = sql.concat(RoomModelImpl.ORDER_BY_JPQL);
                }
            }

            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(sql);

                if (!pagination) {
                    list = (List<Room>) QueryUtil.list(q, getDialect(), start,
                            end, false);

                    Collections.sort(list);

                    list = new UnmodifiableList<Room>(list);
                } else {
                    list = (List<Room>) QueryUtil.list(q, getDialect(), start,
                            end);
                }

                cacheResult(list);

                FinderCacheUtil.putResult(finderPath, finderArgs, list);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(finderPath, finderArgs);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return list;
    }

    /**
     * Removes all the rooms from the database.
     *
     * @throws SystemException if a system exception occurred
     */
    @Override
    public void removeAll() throws SystemException {
        for (Room room : findAll()) {
            remove(room);
        }
    }

    /**
     * Returns the number of rooms.
     *
     * @return the number of rooms
     * @throws SystemException if a system exception occurred
     */
    @Override
    public int countAll() throws SystemException {
        Long count = (Long) FinderCacheUtil.getResult(FINDER_PATH_COUNT_ALL,
                FINDER_ARGS_EMPTY, this);

        if (count == null) {
            Session session = null;

            try {
                session = openSession();

                Query q = session.createQuery(_SQL_COUNT_ROOM);

                count = (Long) q.uniqueResult();

                FinderCacheUtil.putResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY, count);
            } catch (Exception e) {
                FinderCacheUtil.removeResult(FINDER_PATH_COUNT_ALL,
                    FINDER_ARGS_EMPTY);

                throw processException(e);
            } finally {
                closeSession(session);
            }
        }

        return count.intValue();
    }

    @Override
    protected Set<String> getBadColumnNames() {
        return _badColumnNames;
    }

    /**
     * Initializes the room persistence.
     */
    public void afterPropertiesSet() {
        String[] listenerClassNames = StringUtil.split(GetterUtil.getString(
                    com.liferay.util.service.ServiceProps.get(
                        "value.object.listener.br.com.atilo.jcondo.booking.model.Room")));

        if (listenerClassNames.length > 0) {
            try {
                List<ModelListener<Room>> listenersList = new ArrayList<ModelListener<Room>>();

                for (String listenerClassName : listenerClassNames) {
                    listenersList.add((ModelListener<Room>) InstanceFactory.newInstance(
                            getClassLoader(), listenerClassName));
                }

                listeners = listenersList.toArray(new ModelListener[listenersList.size()]);
            } catch (Exception e) {
                _log.error(e);
            }
        }
    }

    public void destroy() {
        EntityCacheUtil.removeCache(RoomImpl.class.getName());
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_ENTITY);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
        FinderCacheUtil.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
    }
}
