package br.com.atilo.jcondo.booking.bean;

import java.io.File;
import java.io.Serializable;
import java.util.PropertyResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.primefaces.event.FileUploadEvent;

@ManagedBean
@ViewScoped
public class FileUploadBean implements Serializable {

	private static final long serialVersionUID = 1L;

	protected static final String TEMP_DIR = PropertyResourceBundle.getBundle("application").getString("file.upload.temp.dir");

	private File file;

	public FileUploadBean() {
		super();
	}

	public void onUpload(FileUploadEvent event) {
		try {
			ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
			file = File.createTempFile("file_", "." + FilenameUtils.getExtension(event.getFile().getFileName()), 
									   new File(ec.getRealPath("") + TEMP_DIR));
			FileUtils.copyInputStreamToFile(event.getFile().getInputstream(), file);
			file.deleteOnExit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public File getFile() {
		return file;
	}

}
