/**
 * Copyright (c) 2000-2012 Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package br.com.atilo.jcondo.booking.model.impl;

import java.util.ArrayList;
import java.util.List;

import br.com.atilo.jcondo.booking.model.RoomBlockade;
import br.com.atilo.jcondo.booking.model.RoomBooking;
import br.com.atilo.jcondo.booking.service.RoomBlockadeLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomBookingLocalServiceUtil;
import br.com.atilo.jcondo.booking.service.RoomLocalServiceUtil;
import br.com.atilo.jcondo.Image;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.repository.model.FileEntry;
import com.liferay.portal.kernel.util.WebKeys;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.portal.theme.ThemeDisplay;
import com.liferay.portlet.documentlibrary.service.DLAppServiceUtil;
import com.liferay.portlet.documentlibrary.util.DLUtil;

/**
 * The extended model implementation for the Room service. Represents a row in the &quot;jco_room&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * Helper methods and all application logic should be put in this class. Whenever methods are added, rerun ServiceBuilder to copy their definitions into the {@link br.com.atilo.jcondo.booking.model.Room} interface.
 * </p>
 *
 * @author anderson
 */
public class RoomImpl extends RoomBaseImpl {

	private static final long serialVersionUID = 1L;

	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never reference this class directly. All methods that expect a room model instance should use the {@link br.com.atilo.jcondo.booking.model.Room} interface instead.
	 */
	public RoomImpl() {
	}
	
	public String getAgreementName() throws PortalException, SystemException {
		if (getAgreementId() == 0) {
			return null;
		}

		FileEntry fileEntry = DLAppServiceUtil.getFileEntry(getAgreementId());
		return  fileEntry.getTitle() + "." +fileEntry.getExtension();
	}

	public String getAgreementURL() throws PortalException, SystemException {
		if (getAgreementId() == 0) {
			return null;
		}
		FileEntry fileEntry = DLAppServiceUtil.getFileEntry(getAgreementId());
		ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
		ThemeDisplay td = (ThemeDisplay) sc.getRequest().getAttribute(WebKeys.THEME_DISPLAY);
		return DLUtil.getPreviewURL(fileEntry, null, td, "", false, true);
	}
	
	public List<Image> getImages() throws PortalException, SystemException {
		return getId() == 0 ? new ArrayList<Image>() : RoomLocalServiceUtil.getRoomPictures(getId());
	}
	
	public List<RoomBooking> getRoomBookings() throws PortalException, SystemException {
		return getId() == 0 ? new ArrayList<RoomBooking>() : RoomBookingLocalServiceUtil.getRoomBookings(getId());
	}

	public List<RoomBlockade> getRoomBlockades() throws PortalException, SystemException {
		return getId() == 0 ? new ArrayList<RoomBlockade>() : RoomBlockadeLocalServiceUtil.getRoomBlockades(getId());
	}

}