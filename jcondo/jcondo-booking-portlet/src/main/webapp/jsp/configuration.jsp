<%@ taglib uri="http://liferay.com/tld/portlet" prefix="liferay-portlet" %>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayPortletMode"%>
<%@ page import="com.liferay.portal.kernel.portlet.LiferayWindowState"%>

<liferay-portlet:renderURL portletName="jcondorooms_WAR_jcondobookingportlet" var="renderUrl" windowState="<%=LiferayWindowState.NORMAL.toString() %>" 
				   portletMode="<%=LiferayPortletMode.VIEW.toString()%>">
</liferay-portlet:renderURL>

<iframe src="${renderUrl}" scrolling="no" width="100%" height="480px" style="border: 1px solid #cccccc; border-top: none;"/>