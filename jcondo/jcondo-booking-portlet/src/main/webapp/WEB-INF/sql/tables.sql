create table jco_booking (
	bookingId LONG not null primary key,
	roomId LONG,
	domainId LONG,
	personId LONG,
	beginDate DATE null,
	endDate DATE null,
	statusId INTEGER,
	noteId LONG,
	oprDate DATE null,
	oprUser LONG
);

create table jco_booking_guest (
	guestId LONG not null primary key,
	bookingId LONG,
	name VARCHAR(75) null,
	surname VARCHAR(75) null,
	checkedIn BOOLEAN,
	oprDate DATE null,
	oprUser LONG
);

create table jco_booking_invoice (
	invoiceId LONG not null primary key,
	bookingId LONG,
	providerId LONG,
	code VARCHAR(75) null,
	amount DOUBLE,
	tax DOUBLE,
	date DATE null,
	link VARCHAR(75) null,
	oprDate DATE null,
	oprUser LONG
);

create table jco_booking_note (
	noteId LONG not null primary key,
	text VARCHAR(75) null,
	oprDate DATE null,
	oprUser LONG
);

create table jco_payment (
	paymentId LONG not null primary key,
	invoiceId LONG,
	token VARCHAR(75) null,
	amount DOUBLE,
	date DATE null,
	checked BOOLEAN,
	oprDate DATE null,
	oprUser LONG
);

create table jco_room (
	roomId LONG not null primary key,
	folderId LONG,
	agreementId LONG,
	name VARCHAR(75) null,
	description VARCHAR(75) null,
	available BOOLEAN,
	bookable BOOLEAN,
	price DOUBLE,
	capacity INTEGER,
	oprDate DATE null,
	oprUser LONG
);

create table jco_room_blockade (
	blockadeId LONG not null primary key,
	roomId LONG,
	beginDate DATE null,
	endDate DATE null,
	description VARCHAR(75) null,
	recursive BOOLEAN,
	oprDate DATE null,
	oprUser LONG
);

create table jco_room_booking (
	roomBookingId LONG not null primary key,
	roomId LONG,
	weekDay INTEGER,
	openHour INTEGER,
	closeHour INTEGER,
	period INTEGER,
	oprDate DATE null,
	oprUser LONG
);