create index IX_308503E9 on jco_booking (domainId);
create index IX_A31D19FA on jco_booking (personId);

create index IX_1AC534D5 on jco_booking_guest (bookingId);

create index IX_AA8CA1E0 on jco_booking_invoice (bookingId);
create index IX_6C609945 on jco_booking_invoice (code);

create index IX_54467155 on jco_payment (invoiceId);
create index IX_DBF2BB66 on jco_payment (token);

create index IX_93917E0F on jco_room (available);
create index IX_5BF1FD61 on jco_room (bookable);
create index IX_B4043038 on jco_room (bookable, available);
create index IX_A2A82829 on jco_room (name);

create index IX_14C1C8C0 on jco_room_blockade (roomId);
create index IX_7ACE06A2 on jco_room_blockade (roomId, recursive);

create index IX_C131FDCE on jco_room_booking (roomId);
create index IX_A9C12E2A on jco_room_booking (roomId, weekDay);