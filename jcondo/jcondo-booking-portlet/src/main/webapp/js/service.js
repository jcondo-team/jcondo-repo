Liferay.Service.register("Liferay.Service.jco", "br.com.atilo.jcondo.booking.service", "jcondo-booking-portlet");

Liferay.Service.registerClass(
	Liferay.Service.jco, "Booking",
	{
		getDomainBookings: true,
		getPersonBookings: true,
		getBookings: true,
		addBooking: true,
		cancelBooking: true,
		deleteBooking: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.jco, "BookingNote",
	{
		addBookingNote: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.jco, "Guest",
	{
		addGuest: true,
		updateGuest: true,
		deleteGuest: true,
		getBookingGuests: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.jco, "Payment",
	{
		addPayment: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.jco, "Room",
	{
		addRoom: true,
		updateRoom: true,
		deleteRoom: true,
		addRoomPicture: true,
		deleteRoomPicture: true,
		getRooms: true,
		checkRoomAvailability: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.jco, "RoomBlockade",
	{
		addRoomBlockade: true,
		deleteRoomBlockade: true
	}
);

Liferay.Service.registerClass(
	Liferay.Service.jco, "RoomBooking",
	{
		addRoomBooking: true,
		updateRoomBooking: true,
		getRoomBooking: true,
		getRoomBookings: true
	}
);