function saveTabIndex(event, index) {
	//alert("index: " + index);
	tabIndex = index;
}

function bookedDays(date) {
	//alert(document.getElementById("_jcondobooking_WAR_jcondobookingportlet_:tabs:" + tabIndex + ":booking-form:bookedDates").value);
	//alert(date);
	
	if (document.getElementById("_jcondobooking_WAR_jcondobookingportlet_:tabs:" + tabIndex + ":booking-form:bookedDates") != null) {
		var bookedDates = document.getElementById("_jcondobooking_WAR_jcondobookingportlet_:tabs:" + tabIndex + ":booking-form:bookedDates").value.split(",");

	    for (var i = 0; i < bookedDates.length; i++) {
	    	var bookedDate = bookedDates[i].split(";");
	        if (date.getTime() == new Date(bookedDate[1]).getTime()) {
	        	if (bookedDate[0] == 2) {
	        		return [true, 'cld-calendar-room room-' + tabIndex];	
	        	} else if (bookedDate[0] == -1) {
	        		return [false, 'room-blockade'];
	        	} else if (bookedDate[0] == -2) {
	        		return [false, ''];
	        	} else {
	        		return [true, 'calendar-room room-' + tabIndex];
	        	}
	        }
	    }
	}

	return [true, ''];
}