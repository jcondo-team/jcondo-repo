package br.com.atilo.jcondo.support.model.datatype;

public enum IssueType {

	COMPLAINT("issue.type.complaint"),
	SUGGESTION("issue.type.suggestion");

	private String label;
	
	private IssueType(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static IssueType parse(long id) {
		for (IssueType type : values()) {
			if (type.ordinal() == id) {
				return type;
			}
		}
		return null;
	}
	
}
