package br.com.atilo.jcondo.support.service.persistence;

public interface IssueFinder {
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findFlatIssues(
        long flatId);

    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findPersonIssues(
        java.lang.String name);

    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findPersonFlatIssues(
        java.lang.String name, long flatId);
}
