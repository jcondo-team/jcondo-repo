package br.com.atilo.jcondo.support.model;

import br.com.atilo.jcondo.support.service.ClpSerializer;
import br.com.atilo.jcondo.support.service.InteractionLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class InteractionClp extends BaseModelImpl<Interaction>
    implements Interaction {
    private long _id;
    private long _issueId;
    private long _personId;
    private String _text;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _interactionRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.support.service.ClpSerializer.class;

    public InteractionClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Interaction.class;
    }

    @Override
    public String getModelClassName() {
        return Interaction.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("issueId", getIssueId());
        attributes.put("personId", getPersonId());
        attributes.put("text", getText());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long issueId = (Long) attributes.get("issueId");

        if (issueId != null) {
            setIssueId(issueId);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        String text = (String) attributes.get("text");

        if (text != null) {
            setText(text);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_interactionRemoteModel != null) {
            try {
                Class<?> clazz = _interactionRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_interactionRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIssueId() {
        return _issueId;
    }

    @Override
    public void setIssueId(long issueId) {
        _issueId = issueId;

        if (_interactionRemoteModel != null) {
            try {
                Class<?> clazz = _interactionRemoteModel.getClass();

                Method method = clazz.getMethod("setIssueId", long.class);

                method.invoke(_interactionRemoteModel, issueId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getPersonId() {
        return _personId;
    }

    @Override
    public void setPersonId(long personId) {
        _personId = personId;

        if (_interactionRemoteModel != null) {
            try {
                Class<?> clazz = _interactionRemoteModel.getClass();

                Method method = clazz.getMethod("setPersonId", long.class);

                method.invoke(_interactionRemoteModel, personId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getText() {
        return _text;
    }

    @Override
    public void setText(String text) {
        _text = text;

        if (_interactionRemoteModel != null) {
            try {
                Class<?> clazz = _interactionRemoteModel.getClass();

                Method method = clazz.getMethod("setText", String.class);

                method.invoke(_interactionRemoteModel, text);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_interactionRemoteModel != null) {
            try {
                Class<?> clazz = _interactionRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_interactionRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_interactionRemoteModel != null) {
            try {
                Class<?> clazz = _interactionRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_interactionRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public br.com.atilo.jcondo.support.model.Issue getIssue() {
        try {
            String methodName = "getIssue";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.support.model.Issue returnObj = (br.com.atilo.jcondo.support.model.Issue) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson() {
        try {
            String methodName = "getPerson";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.manager.model.Person returnObj = (br.com.atilo.jcondo.manager.model.Person) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getInteractionRemoteModel() {
        return _interactionRemoteModel;
    }

    public void setInteractionRemoteModel(BaseModel<?> interactionRemoteModel) {
        _interactionRemoteModel = interactionRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _interactionRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_interactionRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            InteractionLocalServiceUtil.addInteraction(this);
        } else {
            InteractionLocalServiceUtil.updateInteraction(this);
        }
    }

    @Override
    public Interaction toEscapedModel() {
        return (Interaction) ProxyUtil.newProxyInstance(Interaction.class.getClassLoader(),
            new Class[] { Interaction.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        InteractionClp clone = new InteractionClp();

        clone.setId(getId());
        clone.setIssueId(getIssueId());
        clone.setPersonId(getPersonId());
        clone.setText(getText());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Interaction interaction) {
        int value = 0;

        value = DateUtil.compareTo(getOprDate(), interaction.getOprDate());

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof InteractionClp)) {
            return false;
        }

        InteractionClp interaction = (InteractionClp) obj;

        long primaryKey = interaction.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(13);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", issueId=");
        sb.append(getIssueId());
        sb.append(", personId=");
        sb.append(getPersonId());
        sb.append(", text=");
        sb.append(getText());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(22);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.support.model.Interaction");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>issueId</column-name><column-value><![CDATA[");
        sb.append(getIssueId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>personId</column-name><column-value><![CDATA[");
        sb.append(getPersonId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>text</column-name><column-value><![CDATA[");
        sb.append(getText());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
