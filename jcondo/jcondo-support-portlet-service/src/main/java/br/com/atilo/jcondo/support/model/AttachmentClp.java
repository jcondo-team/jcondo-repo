package br.com.atilo.jcondo.support.model;

import br.com.atilo.jcondo.support.service.AttachmentLocalServiceUtil;
import br.com.atilo.jcondo.support.service.ClpSerializer;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class AttachmentClp extends BaseModelImpl<Attachment>
    implements Attachment {
    private long _attachmentId;
    private long _issueId;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _attachmentRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.support.service.ClpSerializer.class;

    public AttachmentClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Attachment.class;
    }

    @Override
    public String getModelClassName() {
        return Attachment.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _attachmentId;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setAttachmentId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _attachmentId;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("attachmentId", getAttachmentId());
        attributes.put("issueId", getIssueId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long attachmentId = (Long) attributes.get("attachmentId");

        if (attachmentId != null) {
            setAttachmentId(attachmentId);
        }

        Long issueId = (Long) attributes.get("issueId");

        if (issueId != null) {
            setIssueId(issueId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getAttachmentId() {
        return _attachmentId;
    }

    @Override
    public void setAttachmentId(long attachmentId) {
        _attachmentId = attachmentId;

        if (_attachmentRemoteModel != null) {
            try {
                Class<?> clazz = _attachmentRemoteModel.getClass();

                Method method = clazz.getMethod("setAttachmentId", long.class);

                method.invoke(_attachmentRemoteModel, attachmentId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getIssueId() {
        return _issueId;
    }

    @Override
    public void setIssueId(long issueId) {
        _issueId = issueId;

        if (_attachmentRemoteModel != null) {
            try {
                Class<?> clazz = _attachmentRemoteModel.getClass();

                Method method = clazz.getMethod("setIssueId", long.class);

                method.invoke(_attachmentRemoteModel, issueId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_attachmentRemoteModel != null) {
            try {
                Class<?> clazz = _attachmentRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_attachmentRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_attachmentRemoteModel != null) {
            try {
                Class<?> clazz = _attachmentRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_attachmentRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    public BaseModel<?> getAttachmentRemoteModel() {
        return _attachmentRemoteModel;
    }

    public void setAttachmentRemoteModel(BaseModel<?> attachmentRemoteModel) {
        _attachmentRemoteModel = attachmentRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _attachmentRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_attachmentRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            AttachmentLocalServiceUtil.addAttachment(this);
        } else {
            AttachmentLocalServiceUtil.updateAttachment(this);
        }
    }

    @Override
    public Attachment toEscapedModel() {
        return (Attachment) ProxyUtil.newProxyInstance(Attachment.class.getClassLoader(),
            new Class[] { Attachment.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        AttachmentClp clone = new AttachmentClp();

        clone.setAttachmentId(getAttachmentId());
        clone.setIssueId(getIssueId());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Attachment attachment) {
        long primaryKey = attachment.getPrimaryKey();

        if (getPrimaryKey() < primaryKey) {
            return -1;
        } else if (getPrimaryKey() > primaryKey) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AttachmentClp)) {
            return false;
        }

        AttachmentClp attachment = (AttachmentClp) obj;

        long primaryKey = attachment.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(9);

        sb.append("{attachmentId=");
        sb.append(getAttachmentId());
        sb.append(", issueId=");
        sb.append(getIssueId());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(16);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.support.model.Attachment");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>attachmentId</column-name><column-value><![CDATA[");
        sb.append(getAttachmentId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>issueId</column-name><column-value><![CDATA[");
        sb.append(getIssueId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
