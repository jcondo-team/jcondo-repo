package br.com.atilo.jcondo.support.model;

import br.com.atilo.jcondo.support.service.ClpSerializer;
import br.com.atilo.jcondo.support.service.IssueLocalServiceUtil;

import com.liferay.portal.kernel.bean.AutoEscapeBeanHandler;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.DateUtil;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.model.BaseModel;
import com.liferay.portal.model.impl.BaseModelImpl;

import java.io.Serializable;

import java.lang.reflect.Method;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public class IssueClp extends BaseModelImpl<Issue> implements Issue {
    private long _id;
    private long _personId;
    private long _assigneeId;
    private long _spaceId;
    private long _typeId;
    private long _statusId;
    private String _code;
    private String _title;
    private String _text;
    private Date _date;
    private Date _oprDate;
    private long _oprUser;
    private BaseModel<?> _issueRemoteModel;
    private Class<?> _clpSerializerClass = br.com.atilo.jcondo.support.service.ClpSerializer.class;

    public IssueClp() {
    }

    @Override
    public Class<?> getModelClass() {
        return Issue.class;
    }

    @Override
    public String getModelClassName() {
        return Issue.class.getName();
    }

    @Override
    public long getPrimaryKey() {
        return _id;
    }

    @Override
    public void setPrimaryKey(long primaryKey) {
        setId(primaryKey);
    }

    @Override
    public Serializable getPrimaryKeyObj() {
        return _id;
    }

    @Override
    public void setPrimaryKeyObj(Serializable primaryKeyObj) {
        setPrimaryKey(((Long) primaryKeyObj).longValue());
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("personId", getPersonId());
        attributes.put("assigneeId", getAssigneeId());
        attributes.put("spaceId", getSpaceId());
        attributes.put("typeId", getTypeId());
        attributes.put("statusId", getStatusId());
        attributes.put("code", getCode());
        attributes.put("title", getTitle());
        attributes.put("text", getText());
        attributes.put("date", getDate());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Long assigneeId = (Long) attributes.get("assigneeId");

        if (assigneeId != null) {
            setAssigneeId(assigneeId);
        }

        Long spaceId = (Long) attributes.get("spaceId");

        if (spaceId != null) {
            setSpaceId(spaceId);
        }

        Long typeId = (Long) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Long statusId = (Long) attributes.get("statusId");

        if (statusId != null) {
            setStatusId(statusId);
        }

        String code = (String) attributes.get("code");

        if (code != null) {
            setCode(code);
        }

        String title = (String) attributes.get("title");

        if (title != null) {
            setTitle(title);
        }

        String text = (String) attributes.get("text");

        if (text != null) {
            setText(text);
        }

        Date date = (Date) attributes.get("date");

        if (date != null) {
            setDate(date);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    @Override
    public long getId() {
        return _id;
    }

    @Override
    public void setId(long id) {
        _id = id;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setId", long.class);

                method.invoke(_issueRemoteModel, id);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getPersonId() {
        return _personId;
    }

    @Override
    public void setPersonId(long personId) {
        _personId = personId;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setPersonId", long.class);

                method.invoke(_issueRemoteModel, personId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getAssigneeId() {
        return _assigneeId;
    }

    @Override
    public void setAssigneeId(long assigneeId) {
        _assigneeId = assigneeId;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setAssigneeId", long.class);

                method.invoke(_issueRemoteModel, assigneeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getSpaceId() {
        return _spaceId;
    }

    @Override
    public void setSpaceId(long spaceId) {
        _spaceId = spaceId;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setSpaceId", long.class);

                method.invoke(_issueRemoteModel, spaceId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getTypeId() {
        return _typeId;
    }

    @Override
    public void setTypeId(long typeId) {
        _typeId = typeId;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setTypeId", long.class);

                method.invoke(_issueRemoteModel, typeId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getStatusId() {
        return _statusId;
    }

    @Override
    public void setStatusId(long statusId) {
        _statusId = statusId;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setStatusId", long.class);

                method.invoke(_issueRemoteModel, statusId);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getCode() {
        return _code;
    }

    @Override
    public void setCode(String code) {
        _code = code;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setCode", String.class);

                method.invoke(_issueRemoteModel, code);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getTitle() {
        return _title;
    }

    @Override
    public void setTitle(String title) {
        _title = title;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setTitle", String.class);

                method.invoke(_issueRemoteModel, title);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public String getText() {
        return _text;
    }

    @Override
    public void setText(String text) {
        _text = text;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setText", String.class);

                method.invoke(_issueRemoteModel, text);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getDate() {
        return _date;
    }

    @Override
    public void setDate(Date date) {
        _date = date;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setDate", Date.class);

                method.invoke(_issueRemoteModel, date);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public Date getOprDate() {
        return _oprDate;
    }

    @Override
    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setOprDate", Date.class);

                method.invoke(_issueRemoteModel, oprDate);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public long getOprUser() {
        return _oprUser;
    }

    @Override
    public void setOprUser(long oprUser) {
        _oprUser = oprUser;

        if (_issueRemoteModel != null) {
            try {
                Class<?> clazz = _issueRemoteModel.getClass();

                Method method = clazz.getMethod("setOprUser", long.class);

                method.invoke(_issueRemoteModel, oprUser);
            } catch (Exception e) {
                throw new UnsupportedOperationException(e);
            }
        }
    }

    @Override
    public void setStatus(
        br.com.atilo.jcondo.support.model.datatype.IssueSpace space) {
        try {
            String methodName = "setStatus";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.support.model.datatype.IssueSpace.class
                };

            Object[] parameterValues = new Object[] { space };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.support.model.datatype.IssueType getType() {
        try {
            String methodName = "getType";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.support.model.datatype.IssueType returnObj = (br.com.atilo.jcondo.support.model.datatype.IssueType) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setType(
        br.com.atilo.jcondo.support.model.datatype.IssueType type) {
        try {
            String methodName = "setType";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.support.model.datatype.IssueType.class
                };

            Object[] parameterValues = new Object[] { type };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getAssignee() {
        try {
            String methodName = "getAssignee";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.manager.model.Person returnObj = (br.com.atilo.jcondo.manager.model.Person) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.support.model.datatype.IssueSpace getSpace() {
        try {
            String methodName = "getSpace";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.support.model.datatype.IssueSpace returnObj = (br.com.atilo.jcondo.support.model.datatype.IssueSpace) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public void setStatus(
        br.com.atilo.jcondo.support.model.datatype.IssueStatus status) {
        try {
            String methodName = "setStatus";

            Class<?>[] parameterTypes = new Class<?>[] {
                    br.com.atilo.jcondo.support.model.datatype.IssueStatus.class
                };

            Object[] parameterValues = new Object[] { status };

            invokeOnRemoteModel(methodName, parameterTypes, parameterValues);
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> getInteractions() {
        try {
            String methodName = "getInteractions";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            java.util.List<br.com.atilo.jcondo.support.model.Interaction> returnObj =
                (java.util.List<br.com.atilo.jcondo.support.model.Interaction>) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.support.model.datatype.IssueStatus getStatus() {
        try {
            String methodName = "getStatus";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.support.model.datatype.IssueStatus returnObj = (br.com.atilo.jcondo.support.model.datatype.IssueStatus) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson() {
        try {
            String methodName = "getPerson";

            Class<?>[] parameterTypes = new Class<?>[] {  };

            Object[] parameterValues = new Object[] {  };

            br.com.atilo.jcondo.manager.model.Person returnObj = (br.com.atilo.jcondo.manager.model.Person) invokeOnRemoteModel(methodName,
                    parameterTypes, parameterValues);

            return returnObj;
        } catch (Exception e) {
            throw new UnsupportedOperationException(e);
        }
    }

    public BaseModel<?> getIssueRemoteModel() {
        return _issueRemoteModel;
    }

    public void setIssueRemoteModel(BaseModel<?> issueRemoteModel) {
        _issueRemoteModel = issueRemoteModel;
    }

    public Object invokeOnRemoteModel(String methodName,
        Class<?>[] parameterTypes, Object[] parameterValues)
        throws Exception {
        Object[] remoteParameterValues = new Object[parameterValues.length];

        for (int i = 0; i < parameterValues.length; i++) {
            if (parameterValues[i] != null) {
                remoteParameterValues[i] = ClpSerializer.translateInput(parameterValues[i]);
            }
        }

        Class<?> remoteModelClass = _issueRemoteModel.getClass();

        ClassLoader remoteModelClassLoader = remoteModelClass.getClassLoader();

        Class<?>[] remoteParameterTypes = new Class[parameterTypes.length];

        for (int i = 0; i < parameterTypes.length; i++) {
            if (parameterTypes[i].isPrimitive()) {
                remoteParameterTypes[i] = parameterTypes[i];
            } else {
                String parameterTypeName = parameterTypes[i].getName();

                remoteParameterTypes[i] = remoteModelClassLoader.loadClass(parameterTypeName);
            }
        }

        Method method = remoteModelClass.getMethod(methodName,
                remoteParameterTypes);

        Object returnValue = method.invoke(_issueRemoteModel,
                remoteParameterValues);

        if (returnValue != null) {
            returnValue = ClpSerializer.translateOutput(returnValue);
        }

        return returnValue;
    }

    @Override
    public void persist() throws SystemException {
        if (this.isNew()) {
            IssueLocalServiceUtil.addIssue(this);
        } else {
            IssueLocalServiceUtil.updateIssue(this);
        }
    }

    @Override
    public Issue toEscapedModel() {
        return (Issue) ProxyUtil.newProxyInstance(Issue.class.getClassLoader(),
            new Class[] { Issue.class }, new AutoEscapeBeanHandler(this));
    }

    @Override
    public Object clone() {
        IssueClp clone = new IssueClp();

        clone.setId(getId());
        clone.setPersonId(getPersonId());
        clone.setAssigneeId(getAssigneeId());
        clone.setSpaceId(getSpaceId());
        clone.setTypeId(getTypeId());
        clone.setStatusId(getStatusId());
        clone.setCode(getCode());
        clone.setTitle(getTitle());
        clone.setText(getText());
        clone.setDate(getDate());
        clone.setOprDate(getOprDate());
        clone.setOprUser(getOprUser());

        return clone;
    }

    @Override
    public int compareTo(Issue issue) {
        int value = 0;

        value = DateUtil.compareTo(getDate(), issue.getDate());

        value = value * -1;

        if (value != 0) {
            return value;
        }

        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof IssueClp)) {
            return false;
        }

        IssueClp issue = (IssueClp) obj;

        long primaryKey = issue.getPrimaryKey();

        if (getPrimaryKey() == primaryKey) {
            return true;
        } else {
            return false;
        }
    }

    public Class<?> getClpSerializerClass() {
        return _clpSerializerClass;
    }

    @Override
    public int hashCode() {
        return (int) getPrimaryKey();
    }

    @Override
    public String toString() {
        StringBundler sb = new StringBundler(25);

        sb.append("{id=");
        sb.append(getId());
        sb.append(", personId=");
        sb.append(getPersonId());
        sb.append(", assigneeId=");
        sb.append(getAssigneeId());
        sb.append(", spaceId=");
        sb.append(getSpaceId());
        sb.append(", typeId=");
        sb.append(getTypeId());
        sb.append(", statusId=");
        sb.append(getStatusId());
        sb.append(", code=");
        sb.append(getCode());
        sb.append(", title=");
        sb.append(getTitle());
        sb.append(", text=");
        sb.append(getText());
        sb.append(", date=");
        sb.append(getDate());
        sb.append(", oprDate=");
        sb.append(getOprDate());
        sb.append(", oprUser=");
        sb.append(getOprUser());
        sb.append("}");

        return sb.toString();
    }

    @Override
    public String toXmlString() {
        StringBundler sb = new StringBundler(40);

        sb.append("<model><model-name>");
        sb.append("br.com.atilo.jcondo.support.model.Issue");
        sb.append("</model-name>");

        sb.append(
            "<column><column-name>id</column-name><column-value><![CDATA[");
        sb.append(getId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>personId</column-name><column-value><![CDATA[");
        sb.append(getPersonId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>assigneeId</column-name><column-value><![CDATA[");
        sb.append(getAssigneeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>spaceId</column-name><column-value><![CDATA[");
        sb.append(getSpaceId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>typeId</column-name><column-value><![CDATA[");
        sb.append(getTypeId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>statusId</column-name><column-value><![CDATA[");
        sb.append(getStatusId());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>code</column-name><column-value><![CDATA[");
        sb.append(getCode());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>title</column-name><column-value><![CDATA[");
        sb.append(getTitle());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>text</column-name><column-value><![CDATA[");
        sb.append(getText());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>date</column-name><column-value><![CDATA[");
        sb.append(getDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprDate</column-name><column-value><![CDATA[");
        sb.append(getOprDate());
        sb.append("]]></column-value></column>");
        sb.append(
            "<column><column-name>oprUser</column-name><column-value><![CDATA[");
        sb.append(getOprUser());
        sb.append("]]></column-value></column>");

        sb.append("</model>");

        return sb.toString();
    }
}
