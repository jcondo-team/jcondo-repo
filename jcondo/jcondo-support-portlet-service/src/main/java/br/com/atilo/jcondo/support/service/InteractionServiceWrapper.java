package br.com.atilo.jcondo.support.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InteractionService}.
 *
 * @author anderson
 * @see InteractionService
 * @generated
 */
public class InteractionServiceWrapper implements InteractionService,
    ServiceWrapper<InteractionService> {
    private InteractionService _interactionService;

    public InteractionServiceWrapper(InteractionService interactionService) {
        _interactionService = interactionService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _interactionService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _interactionService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _interactionService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.support.model.Interaction createInteraction() {
        return _interactionService.createInteraction();
    }

    @Override
    public br.com.atilo.jcondo.support.model.Interaction addInteraction(
        br.com.atilo.jcondo.support.model.Interaction interaction)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _interactionService.addInteraction(interaction);
    }

    @Override
    public br.com.atilo.jcondo.support.model.Interaction updateInteraction(
        br.com.atilo.jcondo.support.model.Interaction interaction)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _interactionService.updateInteraction(interaction);
    }

    @Override
    public br.com.atilo.jcondo.support.model.Interaction getInteraction(
        long issueId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _interactionService.getInteraction(issueId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public InteractionService getWrappedInteractionService() {
        return _interactionService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedInteractionService(
        InteractionService interactionService) {
        _interactionService = interactionService;
    }

    @Override
    public InteractionService getWrappedService() {
        return _interactionService;
    }

    @Override
    public void setWrappedService(InteractionService interactionService) {
        _interactionService = interactionService;
    }
}
