package br.com.atilo.jcondo.support.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.support.service.http.IssueServiceSoap}.
 *
 * @author anderson
 * @see br.com.atilo.jcondo.support.service.http.IssueServiceSoap
 * @generated
 */
public class IssueSoap implements Serializable {
    private long _id;
    private long _personId;
    private long _assigneeId;
    private long _spaceId;
    private long _typeId;
    private long _statusId;
    private String _code;
    private String _title;
    private String _text;
    private Date _date;
    private Date _oprDate;
    private long _oprUser;

    public IssueSoap() {
    }

    public static IssueSoap toSoapModel(Issue model) {
        IssueSoap soapModel = new IssueSoap();

        soapModel.setId(model.getId());
        soapModel.setPersonId(model.getPersonId());
        soapModel.setAssigneeId(model.getAssigneeId());
        soapModel.setSpaceId(model.getSpaceId());
        soapModel.setTypeId(model.getTypeId());
        soapModel.setStatusId(model.getStatusId());
        soapModel.setCode(model.getCode());
        soapModel.setTitle(model.getTitle());
        soapModel.setText(model.getText());
        soapModel.setDate(model.getDate());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static IssueSoap[] toSoapModels(Issue[] models) {
        IssueSoap[] soapModels = new IssueSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static IssueSoap[][] toSoapModels(Issue[][] models) {
        IssueSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new IssueSoap[models.length][models[0].length];
        } else {
            soapModels = new IssueSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static IssueSoap[] toSoapModels(List<Issue> models) {
        List<IssueSoap> soapModels = new ArrayList<IssueSoap>(models.size());

        for (Issue model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new IssueSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getPersonId() {
        return _personId;
    }

    public void setPersonId(long personId) {
        _personId = personId;
    }

    public long getAssigneeId() {
        return _assigneeId;
    }

    public void setAssigneeId(long assigneeId) {
        _assigneeId = assigneeId;
    }

    public long getSpaceId() {
        return _spaceId;
    }

    public void setSpaceId(long spaceId) {
        _spaceId = spaceId;
    }

    public long getTypeId() {
        return _typeId;
    }

    public void setTypeId(long typeId) {
        _typeId = typeId;
    }

    public long getStatusId() {
        return _statusId;
    }

    public void setStatusId(long statusId) {
        _statusId = statusId;
    }

    public String getCode() {
        return _code;
    }

    public void setCode(String code) {
        _code = code;
    }

    public String getTitle() {
        return _title;
    }

    public void setTitle(String title) {
        _title = title;
    }

    public String getText() {
        return _text;
    }

    public void setText(String text) {
        _text = text;
    }

    public Date getDate() {
        return _date;
    }

    public void setDate(Date date) {
        _date = date;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
