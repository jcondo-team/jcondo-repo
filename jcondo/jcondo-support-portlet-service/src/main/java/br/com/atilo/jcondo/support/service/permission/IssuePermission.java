package br.com.atilo.jcondo.support.service.permission;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;

import br.com.atilo.jcondo.manager.security.Permission;
import br.com.atilo.jcondo.manager.service.permission.BasePermission;
import br.com.atilo.jcondo.support.model.Issue;

public class IssuePermission extends BasePermission {

	public static boolean hasPermission(Permission permission, long issueId) throws PortalException, SystemException {
		return hasPermission(permission, Issue.class, issueId, null);
	}

}
