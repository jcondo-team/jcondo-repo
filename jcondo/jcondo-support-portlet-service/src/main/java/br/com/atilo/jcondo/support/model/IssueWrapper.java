package br.com.atilo.jcondo.support.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Issue}.
 * </p>
 *
 * @author anderson
 * @see Issue
 * @generated
 */
public class IssueWrapper implements Issue, ModelWrapper<Issue> {
    private Issue _issue;

    public IssueWrapper(Issue issue) {
        _issue = issue;
    }

    @Override
    public Class<?> getModelClass() {
        return Issue.class;
    }

    @Override
    public String getModelClassName() {
        return Issue.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("personId", getPersonId());
        attributes.put("assigneeId", getAssigneeId());
        attributes.put("spaceId", getSpaceId());
        attributes.put("typeId", getTypeId());
        attributes.put("statusId", getStatusId());
        attributes.put("code", getCode());
        attributes.put("title", getTitle());
        attributes.put("text", getText());
        attributes.put("date", getDate());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        Long assigneeId = (Long) attributes.get("assigneeId");

        if (assigneeId != null) {
            setAssigneeId(assigneeId);
        }

        Long spaceId = (Long) attributes.get("spaceId");

        if (spaceId != null) {
            setSpaceId(spaceId);
        }

        Long typeId = (Long) attributes.get("typeId");

        if (typeId != null) {
            setTypeId(typeId);
        }

        Long statusId = (Long) attributes.get("statusId");

        if (statusId != null) {
            setStatusId(statusId);
        }

        String code = (String) attributes.get("code");

        if (code != null) {
            setCode(code);
        }

        String title = (String) attributes.get("title");

        if (title != null) {
            setTitle(title);
        }

        String text = (String) attributes.get("text");

        if (text != null) {
            setText(text);
        }

        Date date = (Date) attributes.get("date");

        if (date != null) {
            setDate(date);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this issue.
    *
    * @return the primary key of this issue
    */
    @Override
    public long getPrimaryKey() {
        return _issue.getPrimaryKey();
    }

    /**
    * Sets the primary key of this issue.
    *
    * @param primaryKey the primary key of this issue
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _issue.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this issue.
    *
    * @return the ID of this issue
    */
    @Override
    public long getId() {
        return _issue.getId();
    }

    /**
    * Sets the ID of this issue.
    *
    * @param id the ID of this issue
    */
    @Override
    public void setId(long id) {
        _issue.setId(id);
    }

    /**
    * Returns the person ID of this issue.
    *
    * @return the person ID of this issue
    */
    @Override
    public long getPersonId() {
        return _issue.getPersonId();
    }

    /**
    * Sets the person ID of this issue.
    *
    * @param personId the person ID of this issue
    */
    @Override
    public void setPersonId(long personId) {
        _issue.setPersonId(personId);
    }

    /**
    * Returns the assignee ID of this issue.
    *
    * @return the assignee ID of this issue
    */
    @Override
    public long getAssigneeId() {
        return _issue.getAssigneeId();
    }

    /**
    * Sets the assignee ID of this issue.
    *
    * @param assigneeId the assignee ID of this issue
    */
    @Override
    public void setAssigneeId(long assigneeId) {
        _issue.setAssigneeId(assigneeId);
    }

    /**
    * Returns the space ID of this issue.
    *
    * @return the space ID of this issue
    */
    @Override
    public long getSpaceId() {
        return _issue.getSpaceId();
    }

    /**
    * Sets the space ID of this issue.
    *
    * @param spaceId the space ID of this issue
    */
    @Override
    public void setSpaceId(long spaceId) {
        _issue.setSpaceId(spaceId);
    }

    /**
    * Returns the type ID of this issue.
    *
    * @return the type ID of this issue
    */
    @Override
    public long getTypeId() {
        return _issue.getTypeId();
    }

    /**
    * Sets the type ID of this issue.
    *
    * @param typeId the type ID of this issue
    */
    @Override
    public void setTypeId(long typeId) {
        _issue.setTypeId(typeId);
    }

    /**
    * Returns the status ID of this issue.
    *
    * @return the status ID of this issue
    */
    @Override
    public long getStatusId() {
        return _issue.getStatusId();
    }

    /**
    * Sets the status ID of this issue.
    *
    * @param statusId the status ID of this issue
    */
    @Override
    public void setStatusId(long statusId) {
        _issue.setStatusId(statusId);
    }

    /**
    * Returns the code of this issue.
    *
    * @return the code of this issue
    */
    @Override
    public java.lang.String getCode() {
        return _issue.getCode();
    }

    /**
    * Sets the code of this issue.
    *
    * @param code the code of this issue
    */
    @Override
    public void setCode(java.lang.String code) {
        _issue.setCode(code);
    }

    /**
    * Returns the title of this issue.
    *
    * @return the title of this issue
    */
    @Override
    public java.lang.String getTitle() {
        return _issue.getTitle();
    }

    /**
    * Sets the title of this issue.
    *
    * @param title the title of this issue
    */
    @Override
    public void setTitle(java.lang.String title) {
        _issue.setTitle(title);
    }

    /**
    * Returns the text of this issue.
    *
    * @return the text of this issue
    */
    @Override
    public java.lang.String getText() {
        return _issue.getText();
    }

    /**
    * Sets the text of this issue.
    *
    * @param text the text of this issue
    */
    @Override
    public void setText(java.lang.String text) {
        _issue.setText(text);
    }

    /**
    * Returns the date of this issue.
    *
    * @return the date of this issue
    */
    @Override
    public java.util.Date getDate() {
        return _issue.getDate();
    }

    /**
    * Sets the date of this issue.
    *
    * @param date the date of this issue
    */
    @Override
    public void setDate(java.util.Date date) {
        _issue.setDate(date);
    }

    /**
    * Returns the opr date of this issue.
    *
    * @return the opr date of this issue
    */
    @Override
    public java.util.Date getOprDate() {
        return _issue.getOprDate();
    }

    /**
    * Sets the opr date of this issue.
    *
    * @param oprDate the opr date of this issue
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _issue.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this issue.
    *
    * @return the opr user of this issue
    */
    @Override
    public long getOprUser() {
        return _issue.getOprUser();
    }

    /**
    * Sets the opr user of this issue.
    *
    * @param oprUser the opr user of this issue
    */
    @Override
    public void setOprUser(long oprUser) {
        _issue.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _issue.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _issue.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _issue.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _issue.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _issue.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _issue.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _issue.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _issue.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _issue.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _issue.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _issue.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new IssueWrapper((Issue) _issue.clone());
    }

    @Override
    public int compareTo(br.com.atilo.jcondo.support.model.Issue issue) {
        return _issue.compareTo(issue);
    }

    @Override
    public int hashCode() {
        return _issue.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.support.model.Issue> toCacheModel() {
        return _issue.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.support.model.Issue toEscapedModel() {
        return new IssueWrapper(_issue.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.support.model.Issue toUnescapedModel() {
        return new IssueWrapper(_issue.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _issue.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _issue.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _issue.persist();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _issue.getPerson();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getAssignee()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _issue.getAssignee();
    }

    @Override
    public br.com.atilo.jcondo.support.model.datatype.IssueType getType() {
        return _issue.getType();
    }

    @Override
    public void setType(
        br.com.atilo.jcondo.support.model.datatype.IssueType type) {
        _issue.setType(type);
    }

    @Override
    public br.com.atilo.jcondo.support.model.datatype.IssueStatus getStatus() {
        return _issue.getStatus();
    }

    @Override
    public void setStatus(
        br.com.atilo.jcondo.support.model.datatype.IssueStatus status) {
        _issue.setStatus(status);
    }

    @Override
    public br.com.atilo.jcondo.support.model.datatype.IssueSpace getSpace() {
        return _issue.getSpace();
    }

    @Override
    public void setStatus(
        br.com.atilo.jcondo.support.model.datatype.IssueSpace space) {
        _issue.setStatus(space);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> getInteractions()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _issue.getInteractions();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof IssueWrapper)) {
            return false;
        }

        IssueWrapper issueWrapper = (IssueWrapper) obj;

        if (Validator.equals(_issue, issueWrapper._issue)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Issue getWrappedIssue() {
        return _issue;
    }

    @Override
    public Issue getWrappedModel() {
        return _issue;
    }

    @Override
    public void resetOriginalValues() {
        _issue.resetOriginalValues();
    }
}
