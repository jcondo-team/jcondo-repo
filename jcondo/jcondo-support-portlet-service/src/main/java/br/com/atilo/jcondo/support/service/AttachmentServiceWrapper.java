package br.com.atilo.jcondo.support.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link AttachmentService}.
 *
 * @author anderson
 * @see AttachmentService
 * @generated
 */
public class AttachmentServiceWrapper implements AttachmentService,
    ServiceWrapper<AttachmentService> {
    private AttachmentService _attachmentService;

    public AttachmentServiceWrapper(AttachmentService attachmentService) {
        _attachmentService = attachmentService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _attachmentService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _attachmentService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _attachmentService.invokeMethod(name, parameterTypes, arguments);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public AttachmentService getWrappedAttachmentService() {
        return _attachmentService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedAttachmentService(AttachmentService attachmentService) {
        _attachmentService = attachmentService;
    }

    @Override
    public AttachmentService getWrappedService() {
        return _attachmentService;
    }

    @Override
    public void setWrappedService(AttachmentService attachmentService) {
        _attachmentService = attachmentService;
    }
}
