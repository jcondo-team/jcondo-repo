package br.com.atilo.jcondo.support.model.datatype;

public enum IssueSpace {

	COMPLAINT_BOOK("issue.space.complaint.book");

	private String label;

	private IssueSpace(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static IssueSpace parse(long id) {
		for (IssueSpace space : values()) {
			if (space.ordinal() == id) {
				return space;
			}
		}
		return null;
	}
}
