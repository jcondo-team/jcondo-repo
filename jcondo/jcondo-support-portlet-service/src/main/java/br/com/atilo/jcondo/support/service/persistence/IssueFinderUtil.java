package br.com.atilo.jcondo.support.service.persistence;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;


public class IssueFinderUtil {
    private static IssueFinder _finder;

    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findFlatIssues(
        long flatId) {
        return getFinder().findFlatIssues(flatId);
    }

    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findPersonIssues(
        java.lang.String name) {
        return getFinder().findPersonIssues(name);
    }

    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findPersonFlatIssues(
        java.lang.String name, long flatId) {
        return getFinder().findPersonFlatIssues(name, flatId);
    }

    public static IssueFinder getFinder() {
        if (_finder == null) {
            _finder = (IssueFinder) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.support.service.ClpSerializer.getServletContextName(),
                    IssueFinder.class.getName());

            ReferenceRegistry.registerReference(IssueFinderUtil.class, "_finder");
        }

        return _finder;
    }

    public void setFinder(IssueFinder finder) {
        _finder = finder;

        ReferenceRegistry.registerReference(IssueFinderUtil.class, "_finder");
    }
}
