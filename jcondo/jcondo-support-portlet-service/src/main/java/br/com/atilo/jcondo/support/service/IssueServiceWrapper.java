package br.com.atilo.jcondo.support.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link IssueService}.
 *
 * @author anderson
 * @see IssueService
 * @generated
 */
public class IssueServiceWrapper implements IssueService,
    ServiceWrapper<IssueService> {
    private IssueService _issueService;

    public IssueServiceWrapper(IssueService issueService) {
        _issueService = issueService;
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _issueService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _issueService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _issueService.invokeMethod(name, parameterTypes, arguments);
    }

    @Override
    public br.com.atilo.jcondo.support.model.Issue addIssue(
        java.lang.String title, java.lang.String text,
        br.com.atilo.jcondo.support.model.datatype.IssueType type,
        br.com.atilo.jcondo.support.model.datatype.IssueSpace space,
        long assigneeId, java.util.List<java.io.File> attachments)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _issueService.addIssue(title, text, type, space, assigneeId,
            attachments);
    }

    @Override
    public br.com.atilo.jcondo.support.model.Issue updateIssue(
        br.com.atilo.jcondo.support.model.Issue issue)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _issueService.updateIssue(issue);
    }

    @Override
    public br.com.atilo.jcondo.support.model.Issue updateIssueStatus(
        long issueId,
        br.com.atilo.jcondo.support.model.datatype.IssueStatus status)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _issueService.updateIssueStatus(issueId, status);
    }

    @Override
    public br.com.atilo.jcondo.support.model.Issue getIssue(
        java.lang.String code)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _issueService.getIssue(code);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getSpaceIssues(
        long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _issueService.getSpaceIssues(spaceId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getPersonIssues(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _issueService.getPersonIssues(personId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getPersonIssuesBySpace(
        long personId, long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _issueService.getPersonIssuesBySpace(personId, spaceId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getFlatIssues(
        long flatId) throws com.liferay.portal.kernel.exception.SystemException {
        return _issueService.getFlatIssues(flatId);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getPersonIssues(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _issueService.getPersonIssues(name);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getPersonFlatIssues(
        java.lang.String name, long flatId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _issueService.getPersonFlatIssues(name, flatId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public IssueService getWrappedIssueService() {
        return _issueService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedIssueService(IssueService issueService) {
        _issueService = issueService;
    }

    @Override
    public IssueService getWrappedService() {
        return _issueService;
    }

    @Override
    public void setWrappedService(IssueService issueService) {
        _issueService = issueService;
    }
}
