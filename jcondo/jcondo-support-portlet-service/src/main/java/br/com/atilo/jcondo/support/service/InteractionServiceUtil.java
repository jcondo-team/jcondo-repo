package br.com.atilo.jcondo.support.service;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service utility for Interaction. This utility wraps
 * {@link br.com.atilo.jcondo.support.service.impl.InteractionServiceImpl} and is the
 * primary access point for service operations in application layer code running
 * on a remote server. Methods of this service are expected to have security
 * checks based on the propagated JAAS credentials because this service can be
 * accessed remotely.
 *
 * @author anderson
 * @see InteractionService
 * @see br.com.atilo.jcondo.support.service.base.InteractionServiceBaseImpl
 * @see br.com.atilo.jcondo.support.service.impl.InteractionServiceImpl
 * @generated
 */
public class InteractionServiceUtil {
    private static InteractionService _service;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Add custom service methods to {@link br.com.atilo.jcondo.support.service.impl.InteractionServiceImpl} and rerun ServiceBuilder to regenerate this class.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public static java.lang.String getBeanIdentifier() {
        return getService().getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public static void setBeanIdentifier(java.lang.String beanIdentifier) {
        getService().setBeanIdentifier(beanIdentifier);
    }

    public static java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return getService().invokeMethod(name, parameterTypes, arguments);
    }

    public static br.com.atilo.jcondo.support.model.Interaction createInteraction() {
        return getService().createInteraction();
    }

    public static br.com.atilo.jcondo.support.model.Interaction addInteraction(
        br.com.atilo.jcondo.support.model.Interaction interaction)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().addInteraction(interaction);
    }

    public static br.com.atilo.jcondo.support.model.Interaction updateInteraction(
        br.com.atilo.jcondo.support.model.Interaction interaction)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().updateInteraction(interaction);
    }

    public static br.com.atilo.jcondo.support.model.Interaction getInteraction(
        long issueId)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return getService().getInteraction(issueId);
    }

    public static void clearService() {
        _service = null;
    }

    public static InteractionService getService() {
        if (_service == null) {
            InvokableService invokableService = (InvokableService) PortletBeanLocatorUtil.locate(ClpSerializer.getServletContextName(),
                    InteractionService.class.getName());

            if (invokableService instanceof InteractionService) {
                _service = (InteractionService) invokableService;
            } else {
                _service = new InteractionServiceClp(invokableService);
            }

            ReferenceRegistry.registerReference(InteractionServiceUtil.class,
                "_service");
        }

        return _service;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setService(InteractionService service) {
    }
}
