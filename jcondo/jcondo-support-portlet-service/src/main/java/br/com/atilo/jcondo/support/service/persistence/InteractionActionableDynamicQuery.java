package br.com.atilo.jcondo.support.service.persistence;

import br.com.atilo.jcondo.support.model.Interaction;
import br.com.atilo.jcondo.support.service.InteractionLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author anderson
 * @generated
 */
public abstract class InteractionActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public InteractionActionableDynamicQuery() throws SystemException {
        setBaseLocalService(InteractionLocalServiceUtil.getService());
        setClass(Interaction.class);

        setClassLoader(br.com.atilo.jcondo.support.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
