package br.com.atilo.jcondo.support.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Attachment service. Represents a row in the &quot;jco_issue_attachment&quot; database table, with each column mapped to a property of this class.
 *
 * @author anderson
 * @see AttachmentModel
 * @see br.com.atilo.jcondo.support.model.impl.AttachmentImpl
 * @see br.com.atilo.jcondo.support.model.impl.AttachmentModelImpl
 * @generated
 */
public interface Attachment extends AttachmentModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.support.model.impl.AttachmentImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
}
