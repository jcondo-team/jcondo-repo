package br.com.atilo.jcondo.support.service.persistence;

import br.com.atilo.jcondo.support.model.Attachment;
import br.com.atilo.jcondo.support.service.AttachmentLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author anderson
 * @generated
 */
public abstract class AttachmentActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public AttachmentActionableDynamicQuery() throws SystemException {
        setBaseLocalService(AttachmentLocalServiceUtil.getService());
        setClass(Attachment.class);

        setClassLoader(br.com.atilo.jcondo.support.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("attachmentId");
    }
}
