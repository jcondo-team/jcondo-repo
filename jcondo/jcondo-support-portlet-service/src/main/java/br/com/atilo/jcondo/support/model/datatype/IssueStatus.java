package br.com.atilo.jcondo.support.model.datatype;

public enum IssueStatus {

	OPEN("issue.status.open"),
	CLOSE("issue.status.close");

	private String label;
	
	private IssueStatus(String label) {
		this.label = label;
	}

	public String getLabel() {
		return label;
	}

	public static IssueStatus parse(long id) {
		for (IssueStatus status : values()) {
			if (status.ordinal() == id) {
				return status;
			}
		}
		return null;
	}

}
