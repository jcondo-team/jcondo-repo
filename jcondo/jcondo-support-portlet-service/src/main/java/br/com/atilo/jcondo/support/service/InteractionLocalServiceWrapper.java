package br.com.atilo.jcondo.support.service;

import com.liferay.portal.service.ServiceWrapper;

/**
 * Provides a wrapper for {@link InteractionLocalService}.
 *
 * @author anderson
 * @see InteractionLocalService
 * @generated
 */
public class InteractionLocalServiceWrapper implements InteractionLocalService,
    ServiceWrapper<InteractionLocalService> {
    private InteractionLocalService _interactionLocalService;

    public InteractionLocalServiceWrapper(
        InteractionLocalService interactionLocalService) {
        _interactionLocalService = interactionLocalService;
    }

    /**
    * Adds the interaction to the database. Also notifies the appropriate model listeners.
    *
    * @param interaction the interaction
    * @return the interaction that was added
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.support.model.Interaction addInteraction(
        br.com.atilo.jcondo.support.model.Interaction interaction)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.addInteraction(interaction);
    }

    /**
    * Creates a new interaction with the primary key. Does not add the interaction to the database.
    *
    * @param id the primary key for the new interaction
    * @return the new interaction
    */
    @Override
    public br.com.atilo.jcondo.support.model.Interaction createInteraction(
        long id) {
        return _interactionLocalService.createInteraction(id);
    }

    /**
    * Deletes the interaction with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the interaction
    * @return the interaction that was removed
    * @throws PortalException if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.support.model.Interaction deleteInteraction(
        long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.deleteInteraction(id);
    }

    /**
    * Deletes the interaction from the database. Also notifies the appropriate model listeners.
    *
    * @param interaction the interaction
    * @return the interaction that was removed
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.support.model.Interaction deleteInteraction(
        br.com.atilo.jcondo.support.model.Interaction interaction)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.deleteInteraction(interaction);
    }

    @Override
    public com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery() {
        return _interactionLocalService.dynamicQuery();
    }

    /**
    * Performs a dynamic query on the database and returns the matching rows.
    *
    * @param dynamicQuery the dynamic query
    * @return the matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.dynamicQuery(dynamicQuery);
    }

    /**
    * Performs a dynamic query on the database and returns a range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @return the range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end) throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.dynamicQuery(dynamicQuery, start, end);
    }

    /**
    * Performs a dynamic query on the database and returns an ordered range of the matching rows.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param dynamicQuery the dynamic query
    * @param start the lower bound of the range of model instances
    * @param end the upper bound of the range of model instances (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching rows
    * @throws SystemException if a system exception occurred
    */
    @Override
    @SuppressWarnings("rawtypes")
    public java.util.List dynamicQuery(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery, int start,
        int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.dynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.dynamicQueryCount(dynamicQuery);
    }

    /**
    * Returns the number of rows that match the dynamic query.
    *
    * @param dynamicQuery the dynamic query
    * @param projection the projection to apply to the query
    * @return the number of rows that match the dynamic query
    * @throws SystemException if a system exception occurred
    */
    @Override
    public long dynamicQueryCount(
        com.liferay.portal.kernel.dao.orm.DynamicQuery dynamicQuery,
        com.liferay.portal.kernel.dao.orm.Projection projection)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.dynamicQueryCount(dynamicQuery,
            projection);
    }

    @Override
    public br.com.atilo.jcondo.support.model.Interaction fetchInteraction(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.fetchInteraction(id);
    }

    /**
    * Returns the interaction with the primary key.
    *
    * @param id the primary key of the interaction
    * @return the interaction
    * @throws PortalException if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.support.model.Interaction getInteraction(long id)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.getInteraction(id);
    }

    @Override
    public com.liferay.portal.model.PersistedModel getPersistedModel(
        java.io.Serializable primaryKeyObj)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.getPersistedModel(primaryKeyObj);
    }

    /**
    * Returns a range of all the interactions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of interactions
    * @param end the upper bound of the range of interactions (not inclusive)
    * @return the range of interactions
    * @throws SystemException if a system exception occurred
    */
    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> getInteractions(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.getInteractions(start, end);
    }

    /**
    * Returns the number of interactions.
    *
    * @return the number of interactions
    * @throws SystemException if a system exception occurred
    */
    @Override
    public int getInteractionsCount()
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.getInteractionsCount();
    }

    /**
    * Updates the interaction in the database or adds it if it does not yet exist. Also notifies the appropriate model listeners.
    *
    * @param interaction the interaction
    * @return the interaction that was updated
    * @throws SystemException if a system exception occurred
    */
    @Override
    public br.com.atilo.jcondo.support.model.Interaction updateInteraction(
        br.com.atilo.jcondo.support.model.Interaction interaction)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.updateInteraction(interaction);
    }

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    @Override
    public java.lang.String getBeanIdentifier() {
        return _interactionLocalService.getBeanIdentifier();
    }

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    @Override
    public void setBeanIdentifier(java.lang.String beanIdentifier) {
        _interactionLocalService.setBeanIdentifier(beanIdentifier);
    }

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable {
        return _interactionLocalService.invokeMethod(name, parameterTypes,
            arguments);
    }

    @Override
    public br.com.atilo.jcondo.support.model.Interaction updateInteraction(
        br.com.atilo.jcondo.support.model.Interaction interaction, boolean merge)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.updateInteraction(interaction, merge);
    }

    @Override
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> getIssueInteractions(
        long issueId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return _interactionLocalService.getIssueInteractions(issueId);
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedService}
     */
    public InteractionLocalService getWrappedInteractionLocalService() {
        return _interactionLocalService;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #setWrappedService}
     */
    public void setWrappedInteractionLocalService(
        InteractionLocalService interactionLocalService) {
        _interactionLocalService = interactionLocalService;
    }

    @Override
    public InteractionLocalService getWrappedService() {
        return _interactionLocalService;
    }

    @Override
    public void setWrappedService(
        InteractionLocalService interactionLocalService) {
        _interactionLocalService = interactionLocalService;
    }
}
