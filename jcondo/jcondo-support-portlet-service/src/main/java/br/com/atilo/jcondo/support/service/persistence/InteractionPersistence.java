package br.com.atilo.jcondo.support.service.persistence;

import br.com.atilo.jcondo.support.model.Interaction;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the interaction service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author anderson
 * @see InteractionPersistenceImpl
 * @see InteractionUtil
 * @generated
 */
public interface InteractionPersistence extends BasePersistence<Interaction> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link InteractionUtil} to access the interaction persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns all the interactions where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @return the matching interactions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> findByIssue(
        long issueId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the interactions where issueId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param issueId the issue ID
    * @param start the lower bound of the range of interactions
    * @param end the upper bound of the range of interactions (not inclusive)
    * @return the range of matching interactions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> findByIssue(
        long issueId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the interactions where issueId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param issueId the issue ID
    * @param start the lower bound of the range of interactions
    * @param end the upper bound of the range of interactions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching interactions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> findByIssue(
        long issueId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first interaction in the ordered set where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching interaction
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a matching interaction could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Interaction findByIssue_First(
        long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first interaction in the ordered set where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching interaction, or <code>null</code> if a matching interaction could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Interaction fetchByIssue_First(
        long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last interaction in the ordered set where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching interaction
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a matching interaction could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Interaction findByIssue_Last(
        long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last interaction in the ordered set where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching interaction, or <code>null</code> if a matching interaction could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Interaction fetchByIssue_Last(
        long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the interactions before and after the current interaction in the ordered set where issueId = &#63;.
    *
    * @param id the primary key of the current interaction
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next interaction
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Interaction[] findByIssue_PrevAndNext(
        long id, long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the interactions where issueId = &#63; from the database.
    *
    * @param issueId the issue ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByIssue(long issueId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of interactions where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @return the number of matching interactions
    * @throws SystemException if a system exception occurred
    */
    public int countByIssue(long issueId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the interaction in the entity cache if it is enabled.
    *
    * @param interaction the interaction
    */
    public void cacheResult(
        br.com.atilo.jcondo.support.model.Interaction interaction);

    /**
    * Caches the interactions in the entity cache if it is enabled.
    *
    * @param interactions the interactions
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.support.model.Interaction> interactions);

    /**
    * Creates a new interaction with the primary key. Does not add the interaction to the database.
    *
    * @param id the primary key for the new interaction
    * @return the new interaction
    */
    public br.com.atilo.jcondo.support.model.Interaction create(long id);

    /**
    * Removes the interaction with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the interaction
    * @return the interaction that was removed
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Interaction remove(long id)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.support.model.Interaction updateImpl(
        br.com.atilo.jcondo.support.model.Interaction interaction)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the interaction with the primary key or throws a {@link br.com.atilo.jcondo.support.NoSuchInteractionException} if it could not be found.
    *
    * @param id the primary key of the interaction
    * @return the interaction
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Interaction findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the interaction with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the interaction
    * @return the interaction, or <code>null</code> if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Interaction fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the interactions.
    *
    * @return the interactions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the interactions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of interactions
    * @param end the upper bound of the range of interactions (not inclusive)
    * @return the range of interactions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the interactions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of interactions
    * @param end the upper bound of the range of interactions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of interactions
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the interactions from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of interactions.
    *
    * @return the number of interactions
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
