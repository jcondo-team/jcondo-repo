package br.com.atilo.jcondo.support.service.persistence;

import br.com.atilo.jcondo.support.model.Issue;

import com.liferay.portal.service.persistence.BasePersistence;

/**
 * The persistence interface for the issue service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author anderson
 * @see IssuePersistenceImpl
 * @see IssueUtil
 * @generated
 */
public interface IssuePersistence extends BasePersistence<Issue> {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link IssueUtil} to access the issue persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this interface.
     */

    /**
    * Returns the issue where code = &#63; or throws a {@link br.com.atilo.jcondo.support.NoSuchIssueException} if it could not be found.
    *
    * @param code the code
    * @return the matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue findByCode(
        java.lang.String code)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the issue where code = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param code the code
    * @return the matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue fetchByCode(
        java.lang.String code)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the issue where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param code the code
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue fetchByCode(
        java.lang.String code, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes the issue where code = &#63; from the database.
    *
    * @param code the code
    * @return the issue that was removed
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue removeByCode(
        java.lang.String code)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of issues where code = &#63;.
    *
    * @param code the code
    * @return the number of matching issues
    * @throws SystemException if a system exception occurred
    */
    public int countByCode(java.lang.String code)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the issues where personId = &#63;.
    *
    * @param personId the person ID
    * @return the matching issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPerson(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the issues where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @return the range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPerson(
        long personId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the issues where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPerson(
        long personId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first issue in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue findByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first issue in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue fetchByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last issue in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue findByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last issue in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue fetchByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the issues before and after the current issue in the ordered set where personId = &#63;.
    *
    * @param id the primary key of the current issue
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue[] findByPerson_PrevAndNext(
        long id, long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the issues where personId = &#63; from the database.
    *
    * @param personId the person ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of issues where personId = &#63;.
    *
    * @param personId the person ID
    * @return the number of matching issues
    * @throws SystemException if a system exception occurred
    */
    public int countByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the issues where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @return the matching issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findBySpace(
        long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the issues where spaceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param spaceId the space ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @return the range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findBySpace(
        long spaceId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the issues where spaceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param spaceId the space ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findBySpace(
        long spaceId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first issue in the ordered set where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue findBySpace_First(
        long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first issue in the ordered set where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue fetchBySpace_First(
        long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last issue in the ordered set where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue findBySpace_Last(
        long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last issue in the ordered set where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue fetchBySpace_Last(
        long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the issues before and after the current issue in the ordered set where spaceId = &#63;.
    *
    * @param id the primary key of the current issue
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue[] findBySpace_PrevAndNext(
        long id, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the issues where spaceId = &#63; from the database.
    *
    * @param spaceId the space ID
    * @throws SystemException if a system exception occurred
    */
    public void removeBySpace(long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of issues where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @return the number of matching issues
    * @throws SystemException if a system exception occurred
    */
    public int countBySpace(long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the issues where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @return the matching issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPersonAndSpace(
        long personId, long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the issues where personId = &#63; and spaceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @return the range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPersonAndSpace(
        long personId, long spaceId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the issues where personId = &#63; and spaceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPersonAndSpace(
        long personId, long spaceId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue findByPersonAndSpace_First(
        long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the first issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue fetchByPersonAndSpace_First(
        long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue findByPersonAndSpace_Last(
        long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the last issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue fetchByPersonAndSpace_Last(
        long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the issues before and after the current issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param id the primary key of the current issue
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue[] findByPersonAndSpace_PrevAndNext(
        long id, long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the issues where personId = &#63; and spaceId = &#63; from the database.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @throws SystemException if a system exception occurred
    */
    public void removeByPersonAndSpace(long personId, long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of issues where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @return the number of matching issues
    * @throws SystemException if a system exception occurred
    */
    public int countByPersonAndSpace(long personId, long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Caches the issue in the entity cache if it is enabled.
    *
    * @param issue the issue
    */
    public void cacheResult(br.com.atilo.jcondo.support.model.Issue issue);

    /**
    * Caches the issues in the entity cache if it is enabled.
    *
    * @param issues the issues
    */
    public void cacheResult(
        java.util.List<br.com.atilo.jcondo.support.model.Issue> issues);

    /**
    * Creates a new issue with the primary key. Does not add the issue to the database.
    *
    * @param id the primary key for the new issue
    * @return the new issue
    */
    public br.com.atilo.jcondo.support.model.Issue create(long id);

    /**
    * Removes the issue with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the issue
    * @return the issue that was removed
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue remove(long id)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.support.model.Issue updateImpl(
        br.com.atilo.jcondo.support.model.Issue issue)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the issue with the primary key or throws a {@link br.com.atilo.jcondo.support.NoSuchIssueException} if it could not be found.
    *
    * @param id the primary key of the issue
    * @return the issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue findByPrimaryKey(long id)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the issue with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the issue
    * @return the issue, or <code>null</code> if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public br.com.atilo.jcondo.support.model.Issue fetchByPrimaryKey(long id)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns all the issues.
    *
    * @return the issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns a range of all the issues.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @return the range of issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns an ordered range of all the issues.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of issues
    * @throws SystemException if a system exception occurred
    */
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Removes all the issues from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException;

    /**
    * Returns the number of issues.
    *
    * @return the number of issues
    * @throws SystemException if a system exception occurred
    */
    public int countAll()
        throws com.liferay.portal.kernel.exception.SystemException;
}
