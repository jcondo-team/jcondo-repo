package br.com.atilo.jcondo.support.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.support.service.http.InteractionServiceSoap}.
 *
 * @author anderson
 * @see br.com.atilo.jcondo.support.service.http.InteractionServiceSoap
 * @generated
 */
public class InteractionSoap implements Serializable {
    private long _id;
    private long _issueId;
    private long _personId;
    private String _text;
    private Date _oprDate;
    private long _oprUser;

    public InteractionSoap() {
    }

    public static InteractionSoap toSoapModel(Interaction model) {
        InteractionSoap soapModel = new InteractionSoap();

        soapModel.setId(model.getId());
        soapModel.setIssueId(model.getIssueId());
        soapModel.setPersonId(model.getPersonId());
        soapModel.setText(model.getText());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static InteractionSoap[] toSoapModels(Interaction[] models) {
        InteractionSoap[] soapModels = new InteractionSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static InteractionSoap[][] toSoapModels(Interaction[][] models) {
        InteractionSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new InteractionSoap[models.length][models[0].length];
        } else {
            soapModels = new InteractionSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static InteractionSoap[] toSoapModels(List<Interaction> models) {
        List<InteractionSoap> soapModels = new ArrayList<InteractionSoap>(models.size());

        for (Interaction model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new InteractionSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _id;
    }

    public void setPrimaryKey(long pk) {
        setId(pk);
    }

    public long getId() {
        return _id;
    }

    public void setId(long id) {
        _id = id;
    }

    public long getIssueId() {
        return _issueId;
    }

    public void setIssueId(long issueId) {
        _issueId = issueId;
    }

    public long getPersonId() {
        return _personId;
    }

    public void setPersonId(long personId) {
        _personId = personId;
    }

    public String getText() {
        return _text;
    }

    public void setText(String text) {
        _text = text;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
