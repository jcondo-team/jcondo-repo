package br.com.atilo.jcondo.support.service.persistence;

import br.com.atilo.jcondo.support.model.Interaction;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the interaction service. This utility wraps {@link InteractionPersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author anderson
 * @see InteractionPersistence
 * @see InteractionPersistenceImpl
 * @generated
 */
public class InteractionUtil {
    private static InteractionPersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Interaction interaction) {
        getPersistence().clearCache(interaction);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Interaction> findWithDynamicQuery(
        DynamicQuery dynamicQuery) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Interaction> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Interaction> findWithDynamicQuery(
        DynamicQuery dynamicQuery, int start, int end,
        OrderByComparator orderByComparator) throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Interaction update(Interaction interaction)
        throws SystemException {
        return getPersistence().update(interaction);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Interaction update(Interaction interaction,
        ServiceContext serviceContext) throws SystemException {
        return getPersistence().update(interaction, serviceContext);
    }

    /**
    * Returns all the interactions where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @return the matching interactions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Interaction> findByIssue(
        long issueId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIssue(issueId);
    }

    /**
    * Returns a range of all the interactions where issueId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param issueId the issue ID
    * @param start the lower bound of the range of interactions
    * @param end the upper bound of the range of interactions (not inclusive)
    * @return the range of matching interactions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Interaction> findByIssue(
        long issueId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIssue(issueId, start, end);
    }

    /**
    * Returns an ordered range of all the interactions where issueId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param issueId the issue ID
    * @param start the lower bound of the range of interactions
    * @param end the upper bound of the range of interactions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching interactions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Interaction> findByIssue(
        long issueId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByIssue(issueId, start, end, orderByComparator);
    }

    /**
    * Returns the first interaction in the ordered set where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching interaction
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a matching interaction could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Interaction findByIssue_First(
        long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIssue_First(issueId, orderByComparator);
    }

    /**
    * Returns the first interaction in the ordered set where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching interaction, or <code>null</code> if a matching interaction could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Interaction fetchByIssue_First(
        long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByIssue_First(issueId, orderByComparator);
    }

    /**
    * Returns the last interaction in the ordered set where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching interaction
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a matching interaction could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Interaction findByIssue_Last(
        long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByIssue_Last(issueId, orderByComparator);
    }

    /**
    * Returns the last interaction in the ordered set where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching interaction, or <code>null</code> if a matching interaction could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Interaction fetchByIssue_Last(
        long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByIssue_Last(issueId, orderByComparator);
    }

    /**
    * Returns the interactions before and after the current interaction in the ordered set where issueId = &#63;.
    *
    * @param id the primary key of the current interaction
    * @param issueId the issue ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next interaction
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Interaction[] findByIssue_PrevAndNext(
        long id, long issueId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByIssue_PrevAndNext(id, issueId, orderByComparator);
    }

    /**
    * Removes all the interactions where issueId = &#63; from the database.
    *
    * @param issueId the issue ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByIssue(long issueId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByIssue(issueId);
    }

    /**
    * Returns the number of interactions where issueId = &#63;.
    *
    * @param issueId the issue ID
    * @return the number of matching interactions
    * @throws SystemException if a system exception occurred
    */
    public static int countByIssue(long issueId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByIssue(issueId);
    }

    /**
    * Caches the interaction in the entity cache if it is enabled.
    *
    * @param interaction the interaction
    */
    public static void cacheResult(
        br.com.atilo.jcondo.support.model.Interaction interaction) {
        getPersistence().cacheResult(interaction);
    }

    /**
    * Caches the interactions in the entity cache if it is enabled.
    *
    * @param interactions the interactions
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.support.model.Interaction> interactions) {
        getPersistence().cacheResult(interactions);
    }

    /**
    * Creates a new interaction with the primary key. Does not add the interaction to the database.
    *
    * @param id the primary key for the new interaction
    * @return the new interaction
    */
    public static br.com.atilo.jcondo.support.model.Interaction create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the interaction with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the interaction
    * @return the interaction that was removed
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Interaction remove(long id)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.support.model.Interaction updateImpl(
        br.com.atilo.jcondo.support.model.Interaction interaction)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(interaction);
    }

    /**
    * Returns the interaction with the primary key or throws a {@link br.com.atilo.jcondo.support.NoSuchInteractionException} if it could not be found.
    *
    * @param id the primary key of the interaction
    * @return the interaction
    * @throws br.com.atilo.jcondo.support.NoSuchInteractionException if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Interaction findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.support.NoSuchInteractionException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the interaction with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the interaction
    * @return the interaction, or <code>null</code> if a interaction with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Interaction fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the interactions.
    *
    * @return the interactions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Interaction> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the interactions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of interactions
    * @param end the upper bound of the range of interactions (not inclusive)
    * @return the range of interactions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Interaction> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the interactions.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.InteractionModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of interactions
    * @param end the upper bound of the range of interactions (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of interactions
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Interaction> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the interactions from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of interactions.
    *
    * @return the number of interactions
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static InteractionPersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (InteractionPersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.support.service.ClpSerializer.getServletContextName(),
                    InteractionPersistence.class.getName());

            ReferenceRegistry.registerReference(InteractionUtil.class,
                "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(InteractionPersistence persistence) {
    }
}
