package br.com.atilo.jcondo.support.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Interaction}.
 * </p>
 *
 * @author anderson
 * @see Interaction
 * @generated
 */
public class InteractionWrapper implements Interaction,
    ModelWrapper<Interaction> {
    private Interaction _interaction;

    public InteractionWrapper(Interaction interaction) {
        _interaction = interaction;
    }

    @Override
    public Class<?> getModelClass() {
        return Interaction.class;
    }

    @Override
    public String getModelClassName() {
        return Interaction.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("id", getId());
        attributes.put("issueId", getIssueId());
        attributes.put("personId", getPersonId());
        attributes.put("text", getText());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long id = (Long) attributes.get("id");

        if (id != null) {
            setId(id);
        }

        Long issueId = (Long) attributes.get("issueId");

        if (issueId != null) {
            setIssueId(issueId);
        }

        Long personId = (Long) attributes.get("personId");

        if (personId != null) {
            setPersonId(personId);
        }

        String text = (String) attributes.get("text");

        if (text != null) {
            setText(text);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this interaction.
    *
    * @return the primary key of this interaction
    */
    @Override
    public long getPrimaryKey() {
        return _interaction.getPrimaryKey();
    }

    /**
    * Sets the primary key of this interaction.
    *
    * @param primaryKey the primary key of this interaction
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _interaction.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the ID of this interaction.
    *
    * @return the ID of this interaction
    */
    @Override
    public long getId() {
        return _interaction.getId();
    }

    /**
    * Sets the ID of this interaction.
    *
    * @param id the ID of this interaction
    */
    @Override
    public void setId(long id) {
        _interaction.setId(id);
    }

    /**
    * Returns the issue ID of this interaction.
    *
    * @return the issue ID of this interaction
    */
    @Override
    public long getIssueId() {
        return _interaction.getIssueId();
    }

    /**
    * Sets the issue ID of this interaction.
    *
    * @param issueId the issue ID of this interaction
    */
    @Override
    public void setIssueId(long issueId) {
        _interaction.setIssueId(issueId);
    }

    /**
    * Returns the person ID of this interaction.
    *
    * @return the person ID of this interaction
    */
    @Override
    public long getPersonId() {
        return _interaction.getPersonId();
    }

    /**
    * Sets the person ID of this interaction.
    *
    * @param personId the person ID of this interaction
    */
    @Override
    public void setPersonId(long personId) {
        _interaction.setPersonId(personId);
    }

    /**
    * Returns the text of this interaction.
    *
    * @return the text of this interaction
    */
    @Override
    public java.lang.String getText() {
        return _interaction.getText();
    }

    /**
    * Sets the text of this interaction.
    *
    * @param text the text of this interaction
    */
    @Override
    public void setText(java.lang.String text) {
        _interaction.setText(text);
    }

    /**
    * Returns the opr date of this interaction.
    *
    * @return the opr date of this interaction
    */
    @Override
    public java.util.Date getOprDate() {
        return _interaction.getOprDate();
    }

    /**
    * Sets the opr date of this interaction.
    *
    * @param oprDate the opr date of this interaction
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _interaction.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this interaction.
    *
    * @return the opr user of this interaction
    */
    @Override
    public long getOprUser() {
        return _interaction.getOprUser();
    }

    /**
    * Sets the opr user of this interaction.
    *
    * @param oprUser the opr user of this interaction
    */
    @Override
    public void setOprUser(long oprUser) {
        _interaction.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _interaction.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _interaction.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _interaction.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _interaction.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _interaction.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _interaction.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _interaction.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _interaction.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _interaction.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _interaction.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _interaction.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new InteractionWrapper((Interaction) _interaction.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.support.model.Interaction interaction) {
        return _interaction.compareTo(interaction);
    }

    @Override
    public int hashCode() {
        return _interaction.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.support.model.Interaction> toCacheModel() {
        return _interaction.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.support.model.Interaction toEscapedModel() {
        return new InteractionWrapper(_interaction.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.support.model.Interaction toUnescapedModel() {
        return new InteractionWrapper(_interaction.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _interaction.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _interaction.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _interaction.persist();
    }

    @Override
    public br.com.atilo.jcondo.support.model.Issue getIssue()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _interaction.getIssue();
    }

    @Override
    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException {
        return _interaction.getPerson();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof InteractionWrapper)) {
            return false;
        }

        InteractionWrapper interactionWrapper = (InteractionWrapper) obj;

        if (Validator.equals(_interaction, interactionWrapper._interaction)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Interaction getWrappedInteraction() {
        return _interaction;
    }

    @Override
    public Interaction getWrappedModel() {
        return _interaction;
    }

    @Override
    public void resetOriginalValues() {
        _interaction.resetOriginalValues();
    }
}
