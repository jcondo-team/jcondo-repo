package br.com.atilo.jcondo.support.service.persistence;

import br.com.atilo.jcondo.support.model.Issue;

import com.liferay.portal.kernel.bean.PortletBeanLocatorUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ReferenceRegistry;
import com.liferay.portal.service.ServiceContext;

import java.util.List;

/**
 * The persistence utility for the issue service. This utility wraps {@link IssuePersistenceImpl} and provides direct access to the database for CRUD operations. This utility should only be used by the service layer, as it must operate within a transaction. Never access this utility in a JSP, controller, model, or other front-end class.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author anderson
 * @see IssuePersistence
 * @see IssuePersistenceImpl
 * @generated
 */
public class IssueUtil {
    private static IssuePersistence _persistence;

    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this class directly. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
     */

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache()
     */
    public static void clearCache() {
        getPersistence().clearCache();
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#clearCache(com.liferay.portal.model.BaseModel)
     */
    public static void clearCache(Issue issue) {
        getPersistence().clearCache(issue);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#countWithDynamicQuery(DynamicQuery)
     */
    public static long countWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().countWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery)
     */
    public static List<Issue> findWithDynamicQuery(DynamicQuery dynamicQuery)
        throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int)
     */
    public static List<Issue> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end) throws SystemException {
        return getPersistence().findWithDynamicQuery(dynamicQuery, start, end);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#findWithDynamicQuery(DynamicQuery, int, int, OrderByComparator)
     */
    public static List<Issue> findWithDynamicQuery(DynamicQuery dynamicQuery,
        int start, int end, OrderByComparator orderByComparator)
        throws SystemException {
        return getPersistence()
                   .findWithDynamicQuery(dynamicQuery, start, end,
            orderByComparator);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel)
     */
    public static Issue update(Issue issue) throws SystemException {
        return getPersistence().update(issue);
    }

    /**
     * @see com.liferay.portal.service.persistence.BasePersistence#update(com.liferay.portal.model.BaseModel, ServiceContext)
     */
    public static Issue update(Issue issue, ServiceContext serviceContext)
        throws SystemException {
        return getPersistence().update(issue, serviceContext);
    }

    /**
    * Returns the issue where code = &#63; or throws a {@link br.com.atilo.jcondo.support.NoSuchIssueException} if it could not be found.
    *
    * @param code the code
    * @return the matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue findByCode(
        java.lang.String code)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByCode(code);
    }

    /**
    * Returns the issue where code = &#63; or returns <code>null</code> if it could not be found. Uses the finder cache.
    *
    * @param code the code
    * @return the matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue fetchByCode(
        java.lang.String code)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByCode(code);
    }

    /**
    * Returns the issue where code = &#63; or returns <code>null</code> if it could not be found, optionally using the finder cache.
    *
    * @param code the code
    * @param retrieveFromCache whether to use the finder cache
    * @return the matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue fetchByCode(
        java.lang.String code, boolean retrieveFromCache)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByCode(code, retrieveFromCache);
    }

    /**
    * Removes the issue where code = &#63; from the database.
    *
    * @param code the code
    * @return the issue that was removed
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue removeByCode(
        java.lang.String code)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().removeByCode(code);
    }

    /**
    * Returns the number of issues where code = &#63;.
    *
    * @param code the code
    * @return the number of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static int countByCode(java.lang.String code)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByCode(code);
    }

    /**
    * Returns all the issues where personId = &#63;.
    *
    * @param personId the person ID
    * @return the matching issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPerson(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson(personId);
    }

    /**
    * Returns a range of all the issues where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @return the range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPerson(
        long personId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson(personId, start, end);
    }

    /**
    * Returns an ordered range of all the issues where personId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPerson(
        long personId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPerson(personId, start, end, orderByComparator);
    }

    /**
    * Returns the first issue in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue findByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson_First(personId, orderByComparator);
    }

    /**
    * Returns the first issue in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue fetchByPerson_First(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPerson_First(personId, orderByComparator);
    }

    /**
    * Returns the last issue in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue findByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPerson_Last(personId, orderByComparator);
    }

    /**
    * Returns the last issue in the ordered set where personId = &#63;.
    *
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue fetchByPerson_Last(
        long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPerson_Last(personId, orderByComparator);
    }

    /**
    * Returns the issues before and after the current issue in the ordered set where personId = &#63;.
    *
    * @param id the primary key of the current issue
    * @param personId the person ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue[] findByPerson_PrevAndNext(
        long id, long personId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPerson_PrevAndNext(id, personId, orderByComparator);
    }

    /**
    * Removes all the issues where personId = &#63; from the database.
    *
    * @param personId the person ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByPerson(personId);
    }

    /**
    * Returns the number of issues where personId = &#63;.
    *
    * @param personId the person ID
    * @return the number of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static int countByPerson(long personId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByPerson(personId);
    }

    /**
    * Returns all the issues where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @return the matching issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findBySpace(
        long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBySpace(spaceId);
    }

    /**
    * Returns a range of all the issues where spaceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param spaceId the space ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @return the range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findBySpace(
        long spaceId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBySpace(spaceId, start, end);
    }

    /**
    * Returns an ordered range of all the issues where spaceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param spaceId the space ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findBySpace(
        long spaceId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBySpace(spaceId, start, end, orderByComparator);
    }

    /**
    * Returns the first issue in the ordered set where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue findBySpace_First(
        long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBySpace_First(spaceId, orderByComparator);
    }

    /**
    * Returns the first issue in the ordered set where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue fetchBySpace_First(
        long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchBySpace_First(spaceId, orderByComparator);
    }

    /**
    * Returns the last issue in the ordered set where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue findBySpace_Last(
        long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findBySpace_Last(spaceId, orderByComparator);
    }

    /**
    * Returns the last issue in the ordered set where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue fetchBySpace_Last(
        long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchBySpace_Last(spaceId, orderByComparator);
    }

    /**
    * Returns the issues before and after the current issue in the ordered set where spaceId = &#63;.
    *
    * @param id the primary key of the current issue
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue[] findBySpace_PrevAndNext(
        long id, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findBySpace_PrevAndNext(id, spaceId, orderByComparator);
    }

    /**
    * Removes all the issues where spaceId = &#63; from the database.
    *
    * @param spaceId the space ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeBySpace(long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeBySpace(spaceId);
    }

    /**
    * Returns the number of issues where spaceId = &#63;.
    *
    * @param spaceId the space ID
    * @return the number of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static int countBySpace(long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countBySpace(spaceId);
    }

    /**
    * Returns all the issues where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @return the matching issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPersonAndSpace(
        long personId, long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPersonAndSpace(personId, spaceId);
    }

    /**
    * Returns a range of all the issues where personId = &#63; and spaceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @return the range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPersonAndSpace(
        long personId, long spaceId, int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndSpace(personId, spaceId, start, end);
    }

    /**
    * Returns an ordered range of all the issues where personId = &#63; and spaceId = &#63;.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findByPersonAndSpace(
        long personId, long spaceId, int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndSpace(personId, spaceId, start, end,
            orderByComparator);
    }

    /**
    * Returns the first issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue findByPersonAndSpace_First(
        long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndSpace_First(personId, spaceId,
            orderByComparator);
    }

    /**
    * Returns the first issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the first matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue fetchByPersonAndSpace_First(
        long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPersonAndSpace_First(personId, spaceId,
            orderByComparator);
    }

    /**
    * Returns the last issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue findByPersonAndSpace_Last(
        long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndSpace_Last(personId, spaceId,
            orderByComparator);
    }

    /**
    * Returns the last issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the last matching issue, or <code>null</code> if a matching issue could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue fetchByPersonAndSpace_Last(
        long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .fetchByPersonAndSpace_Last(personId, spaceId,
            orderByComparator);
    }

    /**
    * Returns the issues before and after the current issue in the ordered set where personId = &#63; and spaceId = &#63;.
    *
    * @param id the primary key of the current issue
    * @param personId the person ID
    * @param spaceId the space ID
    * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
    * @return the previous, current, and next issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue[] findByPersonAndSpace_PrevAndNext(
        long id, long personId, long spaceId,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence()
                   .findByPersonAndSpace_PrevAndNext(id, personId, spaceId,
            orderByComparator);
    }

    /**
    * Removes all the issues where personId = &#63; and spaceId = &#63; from the database.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @throws SystemException if a system exception occurred
    */
    public static void removeByPersonAndSpace(long personId, long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeByPersonAndSpace(personId, spaceId);
    }

    /**
    * Returns the number of issues where personId = &#63; and spaceId = &#63;.
    *
    * @param personId the person ID
    * @param spaceId the space ID
    * @return the number of matching issues
    * @throws SystemException if a system exception occurred
    */
    public static int countByPersonAndSpace(long personId, long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countByPersonAndSpace(personId, spaceId);
    }

    /**
    * Caches the issue in the entity cache if it is enabled.
    *
    * @param issue the issue
    */
    public static void cacheResult(
        br.com.atilo.jcondo.support.model.Issue issue) {
        getPersistence().cacheResult(issue);
    }

    /**
    * Caches the issues in the entity cache if it is enabled.
    *
    * @param issues the issues
    */
    public static void cacheResult(
        java.util.List<br.com.atilo.jcondo.support.model.Issue> issues) {
        getPersistence().cacheResult(issues);
    }

    /**
    * Creates a new issue with the primary key. Does not add the issue to the database.
    *
    * @param id the primary key for the new issue
    * @return the new issue
    */
    public static br.com.atilo.jcondo.support.model.Issue create(long id) {
        return getPersistence().create(id);
    }

    /**
    * Removes the issue with the primary key from the database. Also notifies the appropriate model listeners.
    *
    * @param id the primary key of the issue
    * @return the issue that was removed
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue remove(long id)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().remove(id);
    }

    public static br.com.atilo.jcondo.support.model.Issue updateImpl(
        br.com.atilo.jcondo.support.model.Issue issue)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().updateImpl(issue);
    }

    /**
    * Returns the issue with the primary key or throws a {@link br.com.atilo.jcondo.support.NoSuchIssueException} if it could not be found.
    *
    * @param id the primary key of the issue
    * @return the issue
    * @throws br.com.atilo.jcondo.support.NoSuchIssueException if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue findByPrimaryKey(
        long id)
        throws br.com.atilo.jcondo.support.NoSuchIssueException,
            com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findByPrimaryKey(id);
    }

    /**
    * Returns the issue with the primary key or returns <code>null</code> if it could not be found.
    *
    * @param id the primary key of the issue
    * @return the issue, or <code>null</code> if a issue with the primary key could not be found
    * @throws SystemException if a system exception occurred
    */
    public static br.com.atilo.jcondo.support.model.Issue fetchByPrimaryKey(
        long id) throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().fetchByPrimaryKey(id);
    }

    /**
    * Returns all the issues.
    *
    * @return the issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll();
    }

    /**
    * Returns a range of all the issues.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @return the range of issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findAll(
        int start, int end)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end);
    }

    /**
    * Returns an ordered range of all the issues.
    *
    * <p>
    * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link com.liferay.portal.kernel.dao.orm.QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link br.com.atilo.jcondo.support.model.impl.IssueModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
    * </p>
    *
    * @param start the lower bound of the range of issues
    * @param end the upper bound of the range of issues (not inclusive)
    * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
    * @return the ordered range of issues
    * @throws SystemException if a system exception occurred
    */
    public static java.util.List<br.com.atilo.jcondo.support.model.Issue> findAll(
        int start, int end,
        com.liferay.portal.kernel.util.OrderByComparator orderByComparator)
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().findAll(start, end, orderByComparator);
    }

    /**
    * Removes all the issues from the database.
    *
    * @throws SystemException if a system exception occurred
    */
    public static void removeAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        getPersistence().removeAll();
    }

    /**
    * Returns the number of issues.
    *
    * @return the number of issues
    * @throws SystemException if a system exception occurred
    */
    public static int countAll()
        throws com.liferay.portal.kernel.exception.SystemException {
        return getPersistence().countAll();
    }

    public static IssuePersistence getPersistence() {
        if (_persistence == null) {
            _persistence = (IssuePersistence) PortletBeanLocatorUtil.locate(br.com.atilo.jcondo.support.service.ClpSerializer.getServletContextName(),
                    IssuePersistence.class.getName());

            ReferenceRegistry.registerReference(IssueUtil.class, "_persistence");
        }

        return _persistence;
    }

    /**
     * @deprecated As of 6.2.0
     */
    public void setPersistence(IssuePersistence persistence) {
    }
}
