package br.com.atilo.jcondo.support.service.persistence;

import br.com.atilo.jcondo.support.model.Issue;
import br.com.atilo.jcondo.support.service.IssueLocalServiceUtil;

import com.liferay.portal.kernel.dao.orm.BaseActionableDynamicQuery;
import com.liferay.portal.kernel.exception.SystemException;

/**
 * @author anderson
 * @generated
 */
public abstract class IssueActionableDynamicQuery
    extends BaseActionableDynamicQuery {
    public IssueActionableDynamicQuery() throws SystemException {
        setBaseLocalService(IssueLocalServiceUtil.getService());
        setClass(Issue.class);

        setClassLoader(br.com.atilo.jcondo.support.service.ClpSerializer.class.getClassLoader());

        setPrimaryKeyPropertyName("id");
    }
}
