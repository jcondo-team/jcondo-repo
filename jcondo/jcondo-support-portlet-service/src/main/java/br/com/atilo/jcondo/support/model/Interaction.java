package br.com.atilo.jcondo.support.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Interaction service. Represents a row in the &quot;jco_issue_interaction&quot; database table, with each column mapped to a property of this class.
 *
 * @author anderson
 * @see InteractionModel
 * @see br.com.atilo.jcondo.support.model.impl.InteractionImpl
 * @see br.com.atilo.jcondo.support.model.impl.InteractionModelImpl
 * @generated
 */
public interface Interaction extends InteractionModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.support.model.impl.InteractionImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.support.model.Issue getIssue()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;
}
