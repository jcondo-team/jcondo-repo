package br.com.atilo.jcondo.support.service.messaging;

import br.com.atilo.jcondo.support.service.AttachmentLocalServiceUtil;
import br.com.atilo.jcondo.support.service.AttachmentServiceUtil;
import br.com.atilo.jcondo.support.service.ClpSerializer;
import br.com.atilo.jcondo.support.service.InteractionLocalServiceUtil;
import br.com.atilo.jcondo.support.service.InteractionServiceUtil;
import br.com.atilo.jcondo.support.service.IssueLocalServiceUtil;
import br.com.atilo.jcondo.support.service.IssueServiceUtil;

import com.liferay.portal.kernel.messaging.BaseMessageListener;
import com.liferay.portal.kernel.messaging.Message;


public class ClpMessageListener extends BaseMessageListener {
    public static String getServletContextName() {
        return ClpSerializer.getServletContextName();
    }

    @Override
    protected void doReceive(Message message) throws Exception {
        String command = message.getString("command");
        String servletContextName = message.getString("servletContextName");

        if (command.equals("undeploy") &&
                servletContextName.equals(getServletContextName())) {
            AttachmentLocalServiceUtil.clearService();

            AttachmentServiceUtil.clearService();
            InteractionLocalServiceUtil.clearService();

            InteractionServiceUtil.clearService();
            IssueLocalServiceUtil.clearService();

            IssueServiceUtil.clearService();
        }
    }
}
