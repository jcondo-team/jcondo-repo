package br.com.atilo.jcondo.support.model;

import com.liferay.portal.kernel.util.Validator;
import com.liferay.portal.model.ModelWrapper;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * This class is a wrapper for {@link Attachment}.
 * </p>
 *
 * @author anderson
 * @see Attachment
 * @generated
 */
public class AttachmentWrapper implements Attachment, ModelWrapper<Attachment> {
    private Attachment _attachment;

    public AttachmentWrapper(Attachment attachment) {
        _attachment = attachment;
    }

    @Override
    public Class<?> getModelClass() {
        return Attachment.class;
    }

    @Override
    public String getModelClassName() {
        return Attachment.class.getName();
    }

    @Override
    public Map<String, Object> getModelAttributes() {
        Map<String, Object> attributes = new HashMap<String, Object>();

        attributes.put("attachmentId", getAttachmentId());
        attributes.put("issueId", getIssueId());
        attributes.put("oprDate", getOprDate());
        attributes.put("oprUser", getOprUser());

        return attributes;
    }

    @Override
    public void setModelAttributes(Map<String, Object> attributes) {
        Long attachmentId = (Long) attributes.get("attachmentId");

        if (attachmentId != null) {
            setAttachmentId(attachmentId);
        }

        Long issueId = (Long) attributes.get("issueId");

        if (issueId != null) {
            setIssueId(issueId);
        }

        Date oprDate = (Date) attributes.get("oprDate");

        if (oprDate != null) {
            setOprDate(oprDate);
        }

        Long oprUser = (Long) attributes.get("oprUser");

        if (oprUser != null) {
            setOprUser(oprUser);
        }
    }

    /**
    * Returns the primary key of this attachment.
    *
    * @return the primary key of this attachment
    */
    @Override
    public long getPrimaryKey() {
        return _attachment.getPrimaryKey();
    }

    /**
    * Sets the primary key of this attachment.
    *
    * @param primaryKey the primary key of this attachment
    */
    @Override
    public void setPrimaryKey(long primaryKey) {
        _attachment.setPrimaryKey(primaryKey);
    }

    /**
    * Returns the attachment ID of this attachment.
    *
    * @return the attachment ID of this attachment
    */
    @Override
    public long getAttachmentId() {
        return _attachment.getAttachmentId();
    }

    /**
    * Sets the attachment ID of this attachment.
    *
    * @param attachmentId the attachment ID of this attachment
    */
    @Override
    public void setAttachmentId(long attachmentId) {
        _attachment.setAttachmentId(attachmentId);
    }

    /**
    * Returns the issue ID of this attachment.
    *
    * @return the issue ID of this attachment
    */
    @Override
    public long getIssueId() {
        return _attachment.getIssueId();
    }

    /**
    * Sets the issue ID of this attachment.
    *
    * @param issueId the issue ID of this attachment
    */
    @Override
    public void setIssueId(long issueId) {
        _attachment.setIssueId(issueId);
    }

    /**
    * Returns the opr date of this attachment.
    *
    * @return the opr date of this attachment
    */
    @Override
    public java.util.Date getOprDate() {
        return _attachment.getOprDate();
    }

    /**
    * Sets the opr date of this attachment.
    *
    * @param oprDate the opr date of this attachment
    */
    @Override
    public void setOprDate(java.util.Date oprDate) {
        _attachment.setOprDate(oprDate);
    }

    /**
    * Returns the opr user of this attachment.
    *
    * @return the opr user of this attachment
    */
    @Override
    public long getOprUser() {
        return _attachment.getOprUser();
    }

    /**
    * Sets the opr user of this attachment.
    *
    * @param oprUser the opr user of this attachment
    */
    @Override
    public void setOprUser(long oprUser) {
        _attachment.setOprUser(oprUser);
    }

    @Override
    public boolean isNew() {
        return _attachment.isNew();
    }

    @Override
    public void setNew(boolean n) {
        _attachment.setNew(n);
    }

    @Override
    public boolean isCachedModel() {
        return _attachment.isCachedModel();
    }

    @Override
    public void setCachedModel(boolean cachedModel) {
        _attachment.setCachedModel(cachedModel);
    }

    @Override
    public boolean isEscapedModel() {
        return _attachment.isEscapedModel();
    }

    @Override
    public java.io.Serializable getPrimaryKeyObj() {
        return _attachment.getPrimaryKeyObj();
    }

    @Override
    public void setPrimaryKeyObj(java.io.Serializable primaryKeyObj) {
        _attachment.setPrimaryKeyObj(primaryKeyObj);
    }

    @Override
    public com.liferay.portlet.expando.model.ExpandoBridge getExpandoBridge() {
        return _attachment.getExpandoBridge();
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.model.BaseModel<?> baseModel) {
        _attachment.setExpandoBridgeAttributes(baseModel);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portlet.expando.model.ExpandoBridge expandoBridge) {
        _attachment.setExpandoBridgeAttributes(expandoBridge);
    }

    @Override
    public void setExpandoBridgeAttributes(
        com.liferay.portal.service.ServiceContext serviceContext) {
        _attachment.setExpandoBridgeAttributes(serviceContext);
    }

    @Override
    public java.lang.Object clone() {
        return new AttachmentWrapper((Attachment) _attachment.clone());
    }

    @Override
    public int compareTo(
        br.com.atilo.jcondo.support.model.Attachment attachment) {
        return _attachment.compareTo(attachment);
    }

    @Override
    public int hashCode() {
        return _attachment.hashCode();
    }

    @Override
    public com.liferay.portal.model.CacheModel<br.com.atilo.jcondo.support.model.Attachment> toCacheModel() {
        return _attachment.toCacheModel();
    }

    @Override
    public br.com.atilo.jcondo.support.model.Attachment toEscapedModel() {
        return new AttachmentWrapper(_attachment.toEscapedModel());
    }

    @Override
    public br.com.atilo.jcondo.support.model.Attachment toUnescapedModel() {
        return new AttachmentWrapper(_attachment.toUnescapedModel());
    }

    @Override
    public java.lang.String toString() {
        return _attachment.toString();
    }

    @Override
    public java.lang.String toXmlString() {
        return _attachment.toXmlString();
    }

    @Override
    public void persist()
        throws com.liferay.portal.kernel.exception.SystemException {
        _attachment.persist();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof AttachmentWrapper)) {
            return false;
        }

        AttachmentWrapper attachmentWrapper = (AttachmentWrapper) obj;

        if (Validator.equals(_attachment, attachmentWrapper._attachment)) {
            return true;
        }

        return false;
    }

    /**
     * @deprecated As of 6.1.0, replaced by {@link #getWrappedModel}
     */
    public Attachment getWrappedAttachment() {
        return _attachment;
    }

    @Override
    public Attachment getWrappedModel() {
        return _attachment;
    }

    @Override
    public void resetOriginalValues() {
        _attachment.resetOriginalValues();
    }
}
