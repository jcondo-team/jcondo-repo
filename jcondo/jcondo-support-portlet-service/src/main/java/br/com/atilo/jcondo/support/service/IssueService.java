package br.com.atilo.jcondo.support.service;

import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.jsonwebservice.JSONWebService;
import com.liferay.portal.kernel.transaction.Isolation;
import com.liferay.portal.kernel.transaction.Propagation;
import com.liferay.portal.kernel.transaction.Transactional;
import com.liferay.portal.security.ac.AccessControlled;
import com.liferay.portal.service.BaseService;
import com.liferay.portal.service.InvokableService;

/**
 * Provides the remote service interface for Issue. Methods of this
 * service are expected to have security checks based on the propagated JAAS
 * credentials because this service can be accessed remotely.
 *
 * @author anderson
 * @see IssueServiceUtil
 * @see br.com.atilo.jcondo.support.service.base.IssueServiceBaseImpl
 * @see br.com.atilo.jcondo.support.service.impl.IssueServiceImpl
 * @generated
 */
@AccessControlled
@JSONWebService
@Transactional(isolation = Isolation.PORTAL, rollbackFor =  {
    PortalException.class, SystemException.class}
)
public interface IssueService extends BaseService, InvokableService {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify or reference this interface directly. Always use {@link IssueServiceUtil} to access the issue remote service. Add custom service methods to {@link br.com.atilo.jcondo.support.service.impl.IssueServiceImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */

    /**
    * Returns the Spring bean ID for this bean.
    *
    * @return the Spring bean ID for this bean
    */
    public java.lang.String getBeanIdentifier();

    /**
    * Sets the Spring bean ID for this bean.
    *
    * @param beanIdentifier the Spring bean ID for this bean
    */
    public void setBeanIdentifier(java.lang.String beanIdentifier);

    @Override
    public java.lang.Object invokeMethod(java.lang.String name,
        java.lang.String[] parameterTypes, java.lang.Object[] arguments)
        throws java.lang.Throwable;

    public br.com.atilo.jcondo.support.model.Issue addIssue(
        java.lang.String title, java.lang.String text,
        br.com.atilo.jcondo.support.model.datatype.IssueType type,
        br.com.atilo.jcondo.support.model.datatype.IssueSpace space,
        long assigneeId, java.util.List<java.io.File> attachments)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.support.model.Issue updateIssue(
        br.com.atilo.jcondo.support.model.Issue issue)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.support.model.Issue updateIssueStatus(
        long issueId,
        br.com.atilo.jcondo.support.model.datatype.IssueStatus status)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public br.com.atilo.jcondo.support.model.Issue getIssue(
        java.lang.String code)
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getSpaceIssues(
        long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getPersonIssues(
        long personId)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getPersonIssuesBySpace(
        long personId, long spaceId)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getFlatIssues(
        long flatId) throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getPersonIssues(
        java.lang.String name)
        throws com.liferay.portal.kernel.exception.SystemException;

    @Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
    public java.util.List<br.com.atilo.jcondo.support.model.Issue> getPersonFlatIssues(
        java.lang.String name, long flatId)
        throws com.liferay.portal.kernel.exception.SystemException;
}
