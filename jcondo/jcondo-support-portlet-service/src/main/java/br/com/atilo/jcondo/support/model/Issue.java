package br.com.atilo.jcondo.support.model;

import com.liferay.portal.model.PersistedModel;

/**
 * The extended model interface for the Issue service. Represents a row in the &quot;jco_issue&quot; database table, with each column mapped to a property of this class.
 *
 * @author anderson
 * @see IssueModel
 * @see br.com.atilo.jcondo.support.model.impl.IssueImpl
 * @see br.com.atilo.jcondo.support.model.impl.IssueModelImpl
 * @generated
 */
public interface Issue extends IssueModel, PersistedModel {
    /*
     * NOTE FOR DEVELOPERS:
     *
     * Never modify this interface directly. Add methods to {@link br.com.atilo.jcondo.support.model.impl.IssueImpl} and rerun ServiceBuilder to automatically copy the method declarations to this interface.
     */
    public br.com.atilo.jcondo.manager.model.Person getPerson()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.manager.model.Person getAssignee()
        throws com.liferay.portal.kernel.exception.PortalException,
            com.liferay.portal.kernel.exception.SystemException;

    public br.com.atilo.jcondo.support.model.datatype.IssueType getType();

    public void setType(
        br.com.atilo.jcondo.support.model.datatype.IssueType type);

    public br.com.atilo.jcondo.support.model.datatype.IssueStatus getStatus();

    public void setStatus(
        br.com.atilo.jcondo.support.model.datatype.IssueStatus status);

    public br.com.atilo.jcondo.support.model.datatype.IssueSpace getSpace();

    public void setStatus(
        br.com.atilo.jcondo.support.model.datatype.IssueSpace space);

    public java.util.List<br.com.atilo.jcondo.support.model.Interaction> getInteractions()
        throws com.liferay.portal.kernel.exception.SystemException;
}
