package br.com.atilo.jcondo.support.model;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services, specifically {@link br.com.atilo.jcondo.support.service.http.AttachmentServiceSoap}.
 *
 * @author anderson
 * @see br.com.atilo.jcondo.support.service.http.AttachmentServiceSoap
 * @generated
 */
public class AttachmentSoap implements Serializable {
    private long _attachmentId;
    private long _issueId;
    private Date _oprDate;
    private long _oprUser;

    public AttachmentSoap() {
    }

    public static AttachmentSoap toSoapModel(Attachment model) {
        AttachmentSoap soapModel = new AttachmentSoap();

        soapModel.setAttachmentId(model.getAttachmentId());
        soapModel.setIssueId(model.getIssueId());
        soapModel.setOprDate(model.getOprDate());
        soapModel.setOprUser(model.getOprUser());

        return soapModel;
    }

    public static AttachmentSoap[] toSoapModels(Attachment[] models) {
        AttachmentSoap[] soapModels = new AttachmentSoap[models.length];

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModel(models[i]);
        }

        return soapModels;
    }

    public static AttachmentSoap[][] toSoapModels(Attachment[][] models) {
        AttachmentSoap[][] soapModels = null;

        if (models.length > 0) {
            soapModels = new AttachmentSoap[models.length][models[0].length];
        } else {
            soapModels = new AttachmentSoap[0][0];
        }

        for (int i = 0; i < models.length; i++) {
            soapModels[i] = toSoapModels(models[i]);
        }

        return soapModels;
    }

    public static AttachmentSoap[] toSoapModels(List<Attachment> models) {
        List<AttachmentSoap> soapModels = new ArrayList<AttachmentSoap>(models.size());

        for (Attachment model : models) {
            soapModels.add(toSoapModel(model));
        }

        return soapModels.toArray(new AttachmentSoap[soapModels.size()]);
    }

    public long getPrimaryKey() {
        return _attachmentId;
    }

    public void setPrimaryKey(long pk) {
        setAttachmentId(pk);
    }

    public long getAttachmentId() {
        return _attachmentId;
    }

    public void setAttachmentId(long attachmentId) {
        _attachmentId = attachmentId;
    }

    public long getIssueId() {
        return _issueId;
    }

    public void setIssueId(long issueId) {
        _issueId = issueId;
    }

    public Date getOprDate() {
        return _oprDate;
    }

    public void setOprDate(Date oprDate) {
        _oprDate = oprDate;
    }

    public long getOprUser() {
        return _oprUser;
    }

    public void setOprUser(long oprUser) {
        _oprUser = oprUser;
    }
}
