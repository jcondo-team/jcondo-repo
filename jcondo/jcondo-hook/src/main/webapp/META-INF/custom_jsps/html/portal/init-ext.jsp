<aui:script use="aui-base">
        var portletMsgError = A.all('.portlet-msg-error');
        var portletMsgSuccess = A.all('.portlet-msg-success');
       
        if (portletMsgError) {
                var icon = A.Node.create('<div class="portlet-msg-icon portlet-msg-error-icon"></div>');
                portletMsgError.insert(icon, 0);
                setTimeout(function() {
					portletMsgError.remove(true);
                }, 3000);
        }
       
        if (portletMsgSuccess) {
                var icon = A.Node.create('<div class="portlet-msg-icon portlet-msg-success-icon"></div>');
                portletMsgSuccess.insert(icon, 0);
                setTimeout(function() {
					portletMsgSuccess.remove(true);
                }, 3000);
        }  
</aui:script>