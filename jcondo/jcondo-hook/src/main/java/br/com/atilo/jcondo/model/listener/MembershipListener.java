package br.com.atilo.jcondo.model.listener;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import javax.mail.internet.InternetAddress;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.atilo.jcondo.DomainUtils;
import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.service.EmailProvider;
import br.com.atilo.jcondo.manager.service.MembershipLocalServiceUtil;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.bean.PortalBeanLocatorUtil;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.servlet.PortalSessionThreadLocal;
import com.liferay.portal.kernel.template.StringTemplateResource;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateManagerUtil;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.util.ContentUtil;

public class MembershipListener extends BaseModelListener<Membership> {
	
	private static final Logger LOGGER = Logger.getLogger(MembershipListener.class);

	private static final ResourceBundle rb = ResourceBundle.getBundle("Language");

	private static EmailProvider emailProvider = (EmailProvider) PortalBeanLocatorUtil.locate("emailProvider");
	
	private StringTemplateResource membershipUpdatedResource;
	
	private StringTemplateResource membershipRemovedResource;
	
	public MembershipListener() {
		membershipUpdatedResource = new StringTemplateResource("MembershipUpdated", 
																ContentUtil.get("br/com/atilo/jcondo/mail/membership/membership-updated-notify.vm"));
		membershipRemovedResource = new StringTemplateResource("MembershipRemoved", 
																ContentUtil.get("br/com/atilo/jcondo/mail/membership/membership-removed-notify.vm"));
	}

    public void onAfterCreate(Membership membership) throws ModelListenerException {
    	try {
    		Person person = membership.getPerson();

	    	if (!StringUtils.isEmpty(person.getEmail())) {
       			emailProvider.addGroupMember(getGroupName(membership), person.getEmail());
			}
		} catch (Exception e) {
			LOGGER.error("Fail to add person e-mail to mail group. Membership: " + membership, e);
		}
    }

    public void onBeforeUpdate(Membership membership) throws ModelListenerException {
    	try {
    		Membership m = MembershipLocalServiceUtil.getMembership(membership.getId());
    		PortalSessionThreadLocal.getHttpSession().setAttribute("membership", m);
		} catch (Exception e) {
			LOGGER.error("Fail to get membership before update", e);
		}
    }

    public void onAfterUpdate(Membership membership) throws ModelListenerException {
		Membership oldMembership = (Membership) PortalSessionThreadLocal.getHttpSession().getAttribute("membership");

    	try {
    		if (membership.getType() != oldMembership.getType()) {
    			if (membership.getType() == PersonType.VISITOR) {
                	notifyMembershipUpdated(membership, oldMembership, "Desative");
    			} else if (oldMembership.getType() == PersonType.VISITOR) {
    				notifyMembershipUpdated(membership, oldMembership, "Ative");
    			}
    		}
		} catch (Exception e) {
			LOGGER.error("Fail to notify membership update", e);
		}

    	try {
    		Person person = membership.getPerson();

    		if (!StringUtils.isEmpty(person.getEmail())) {
    			if (membership.getDomain() instanceof Flat) {
    				Flat flat = (Flat) membership.getDomain();
					if(!DomainUtils.isMemberOfBlock(person, flat.getBlock())) {
						emailProvider.removeGroupMember(getGroupName(membership), person.getEmail());
					} else {
						emailProvider.addGroupMember(getGroupName(membership), person.getEmail());
					}
    			} else if (membership.getDomain() instanceof Department) {
					emailProvider.removeGroupMember(getGroupName(oldMembership), person.getEmail());
					emailProvider.addGroupMember(getGroupName(membership), person.getEmail());
    			}
    		}
		} catch (Exception e) {
			LOGGER.error("Fail to update person e-mail on mail group. Membership: " + membership, e);

		}

    	PortalSessionThreadLocal.getHttpSession().removeAttribute("membership");
    }

    public void onAfterRemove(Membership membership) throws ModelListenerException {
    	try {
        	if (!(membership.getDomain() instanceof Enterprise)) {
            	List<PersonType> types = Arrays.asList(PersonType.SYNCDIC, PersonType.SUB_SYNDIC, 
            										   PersonType.ADMIN_ADVISOR, PersonType.TAX_ADVISOR);

            	if (!CollectionUtils.exists(types, new EqualPredicate(membership.getType()))) {
            		notifyMembershipRemoved(membership);	
            	}
        	}
		} catch (Exception e) {
			LOGGER.error("Fail to notify membership remove. Membership: " + membership, e);
		}

    	try {
    		Person person = membership.getPerson();

    		if (!StringUtils.isEmpty(person.getEmail())) {
    			if (membership.getDomain() instanceof Flat &&
    					DomainUtils.isMemberOfBlock(person, ((Flat) membership.getDomain()).getBlock())) {
    				return;
    			}

    			emailProvider.removeGroupMember(getGroupName(membership), person.getEmail());
    		}
		} catch (Exception e) {
			LOGGER.error("Fail to remove person e-mail from mail group. Membership: " + membership, e);
		}
    }

    private String getGroupName(Membership membership) throws Exception {
		if (membership.getDomain() instanceof Flat) {
			Flat flat = (Flat) membership.getDomain();
			return "bloco" + flat.getBlock() + "@ventanasresidencial.com.br";
		} else if (membership.getDomain() instanceof Department) {
			if (membership.getType() == PersonType.ADMIN_ADVISOR) {
				return "conselho.administrativo@ventanasresidencial.com.br";
			} else if (membership.getType() == PersonType.TAX_ADVISOR) {
				return "conselho.fiscal@ventanasresidencial.com.br";
			}
		}

		return null;
    }
    
    private void notifyMembershipUpdated(Membership membership, Membership oldMembership, String action) {
		try {
			LOGGER.info("Enviando notificacao de alteracao de membership " + membership.getPersonId());

			Template template = TemplateManagerUtil.getTemplate("", membershipUpdatedResource, false);

			template.put("action", action);
			template.put("newType", rb.getString(membership.getType().getLabel()));
			template.put("oldType", rb.getString(oldMembership.getType().getLabel()));
			template.put("person", membership.getPerson().getFullName());
			template.put("domain", DomainUtils.getDomainName(membership.getDomain()));
			template.put("operator", UserLocalServiceUtil.getUser(membership.getOprUser()).getFullName());

			UnsyncStringWriter writer = new UnsyncStringWriter();

			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);
			
			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(rb.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(rb.getString("admin.mail")));
			mailMessage.setSubject(rb.getString("person.update.subject"));		
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de alteracao de membership " + membership.getPersonId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de alteracao de membership " + membership.getPersonId());
		}
	}

    private void notifyMembershipRemoved(Membership membership) {
		try {
			LOGGER.info("Enviando notificacao de exclusao de membership " + membership.getPersonId());

			Template template = TemplateManagerUtil.getTemplate("", membershipRemovedResource, false);

			template.put("person", membership.getPerson().getFullName());
			template.put("domain", DomainUtils.getDomainName(membership.getDomain()));
			template.put("operator", UserLocalServiceUtil.getUser(membership.getOprUser()).getFullName());

			UnsyncStringWriter writer = new UnsyncStringWriter();

			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);
			
			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(rb.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(rb.getString("admin.mail")));
			mailMessage.setSubject(rb.getString("person.remove.subject"));		
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);

			MailServiceUtil.sendEmail(mailMessage);

			LOGGER.info("Sucesso no envio da notificacao de exclusao de membership " + membership.getPersonId());
		} catch (Exception e) {
			LOGGER.info("Falha no envio da notificacao de exclusao de membership " + membership.getPersonId());
		}
	}

}
