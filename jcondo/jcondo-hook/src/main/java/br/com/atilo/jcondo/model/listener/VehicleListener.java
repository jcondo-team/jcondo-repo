package br.com.atilo.jcondo.model.listener;

import java.util.ResourceBundle;

import javax.mail.internet.InternetAddress;

import org.apache.log4j.Logger;

import br.com.atilo.jcondo.manager.model.Vehicle;
import br.com.atilo.jcondo.manager.service.VehicleLocalServiceUtil;

import com.liferay.mail.service.MailServiceUtil;
import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.io.unsync.UnsyncStringWriter;
import com.liferay.portal.kernel.mail.MailMessage;
import com.liferay.portal.kernel.template.StringTemplateResource;
import com.liferay.portal.kernel.template.Template;
import com.liferay.portal.kernel.template.TemplateManagerUtil;
import com.liferay.portal.model.BaseModelListener;
import com.liferay.portal.service.ServiceContext;
import com.liferay.portal.service.ServiceContextThreadLocal;
import com.liferay.util.ContentUtil;

public class VehicleListener extends BaseModelListener<Vehicle> {

	private static final Logger LOGGER = Logger.getLogger(VehicleListener.class);

	private static final ResourceBundle rb = ResourceBundle.getBundle("Language");

	public static final String OLD_VEHICLE = "OldVehicle";	

	private StringTemplateResource vehicleUpdatedResource;
	
	public VehicleListener() {
		vehicleUpdatedResource = new StringTemplateResource("VehicleUpdated", 
															ContentUtil.get("br/com/atilo/jcondo/mail/vehicle/vehicle-updated-notify.vm"));
	}
	
    public void onBeforeUpdate(Vehicle vehicle) throws ModelListenerException {
    	try {
        	ServiceContext sc = ServiceContextThreadLocal.getServiceContext();
        	sc.getRequest().getSession().setAttribute(OLD_VEHICLE, 
        											  VehicleLocalServiceUtil.getVehicle(vehicle.getId()));
		} catch (Exception e) {
			LOGGER.error("Fail to save vehicle into session", e);
		}
    }

    public void onAfterUpdate(Vehicle vehicle) throws ModelListenerException {
    	ServiceContext sc = ServiceContextThreadLocal.getServiceContext();

    	try {
        	Vehicle oldVehicle = (Vehicle) sc.getRequest().getSession().getAttribute(OLD_VEHICLE);

        	if (oldVehicle.getDomainId() != vehicle.getDomainId() && vehicle.getDomainId() == 0) {
        		notifyVehicleUpdated(vehicle, oldVehicle);
        	}
		} catch (Exception e) {
			LOGGER.error("Fail to notify person update", e);
		} finally {
			sc.getRequest().getSession().removeAttribute(OLD_VEHICLE);
		}
    }

	private void notifyVehicleUpdated(Vehicle vehicle, Vehicle oldVehicle) {
		try {
			LOGGER.info("vehicle update notify begin: " + vehicle.getId());
			
			Template template = TemplateManagerUtil.getTemplate("", vehicleUpdatedResource, false);
	
			template.put("vehicle", vehicle);
			template.put("oldVehicle", oldVehicle);
			UnsyncStringWriter writer = new UnsyncStringWriter();
			template.processTemplate(writer);

			String mailBody = writer.toString();
			LOGGER.debug(mailBody);
	
			MailMessage mailMessage = new MailMessage();
			mailMessage.setFrom(new InternetAddress(rb.getString("noreply.mail")));
			mailMessage.setTo(new InternetAddress(rb.getString("admin.mail")));
			mailMessage.setSubject(rb.getString("vehicle.update.subject"));		
			mailMessage.setBody(mailBody);
			mailMessage.setHTMLFormat(true);
	
			MailServiceUtil.sendEmail(mailMessage);
	
			LOGGER.info("vehicle update notify end: " + vehicle.getId());
		} catch (Exception e) {
			LOGGER.info("Fail vehicle update notify " + vehicle.getId());
		}
	}

}
