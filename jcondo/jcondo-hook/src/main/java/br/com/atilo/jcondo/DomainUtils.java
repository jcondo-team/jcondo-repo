package br.com.atilo.jcondo;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.EqualPredicate;

import com.liferay.portal.model.BaseModel;

import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.model.Enterprise;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;

public class DomainUtils {

	private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("Language");

	public static String getDomainName(BaseModel<?> domain) {
		if (domain instanceof Flat) {
			Flat flat = (Flat) domain;
			return MessageFormat.format(BUNDLE.getString("flat.name"), 
										flat.getNumber(), flat.getBlock());
		} else if (domain instanceof Department) {
			return ((Department) domain).getName();
		} if (domain instanceof Enterprise) {
			return ((Enterprise) domain).getName();
		}

		return null;
	}

	/*
	 * Verifica se a pessoa estah cadastrada em mais de um apartamento no mesmo bloco, como um
	 * Propriet�rio, Locat�rio ou Morador. Caso esteja, n�o remove o e-mail do grupo de e-mail.
	 */
	public static boolean isMemberOfBlock(Person person, int block) throws Exception {
		List<PersonType> types = Arrays.asList(PersonType.OWNER, 
											   PersonType.RENTER, PersonType.DEPENDENT);

		for (Membership m : person.getMemberships()) {
			if (m.getDomain() instanceof Flat) {
				Flat f = (Flat) m.getDomain();
				if (f.getBlock() == block && 
						CollectionUtils.exists(types, new EqualPredicate(m.getType()))) {
					return true;
				}
			}
		}

		return false;
	}

}
