package br.com.atilo.jcondo.model.listener;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import br.com.atilo.jcondo.manager.model.Department;
import br.com.atilo.jcondo.manager.model.Flat;
import br.com.atilo.jcondo.manager.model.Membership;
import br.com.atilo.jcondo.manager.model.Person;
import br.com.atilo.jcondo.datatype.PersonType;
import br.com.atilo.jcondo.manager.service.EmailProvider;
import br.com.atilo.jcondo.manager.service.PersonLocalServiceUtil;

import com.liferay.portal.ModelListenerException;
import com.liferay.portal.kernel.bean.PortalBeanLocatorUtil;
import com.liferay.portal.kernel.servlet.PortalSessionThreadLocal;
import com.liferay.portal.model.BaseModelListener;

public class PersonListener extends BaseModelListener<Person> {

	private static final Logger LOGGER = Logger.getLogger(PersonListener.class);

	private static EmailProvider emailProvider = (EmailProvider) PortalBeanLocatorUtil.locate("emailProvider");
	
	@Override
	public void onAfterUpdate(Person person) throws ModelListenerException {
    	try {
    		Person p = PersonLocalServiceUtil.getPerson(person.getId());

    		if (!StringUtils.equalsIgnoreCase(p.getEmail(), person.getEmail())) {
    			if (!StringUtils.isEmpty(person.getEmail())) {
	    			for (Membership m : p.getMemberships()) {
	    				try {
		        			emailProvider.removeGroupMember(getGroupName(m), person.getEmail());
						} catch (Exception e) {
							LOGGER.error("Fail to remove person e-mail from mail group. personId: " + 
										 person.getId(), e);
						}
	    			}
    			}

    			if (!StringUtils.isEmpty(p.getEmail())) {
        			for (Membership m : p.getMemberships()) {
	    				try {
	    					emailProvider.addGroupMember(getGroupName(m), p.getEmail());
						} catch (Exception e) {
							LOGGER.error("Fail to add person e-mail to mail group. personId: " + 
										 person.getId(), e);
						}
        			}
        		}
    		}

    		PortalSessionThreadLocal.getHttpSession().setAttribute("person.status", person.getUser().getStatus());
		} catch (Exception e) {
			LOGGER.error("Fail to update person email on mail groups. personId: " + person.getId(), e);
		}
	}
	
    private String getGroupName(Membership membership) throws Exception {
		if (membership.getDomain() instanceof Flat) {
			Flat flat = (Flat) membership.getDomain();
			return "bloco" + flat.getBlock() + "@ventanasresidencial.com.br";
		} else if (membership.getDomain() instanceof Department) {
			if (membership.getType() == PersonType.ADMIN_ADVISOR) {
				return "conselho.administrativo@ventanasresidencial.com.br";
			} else if (membership.getType() == PersonType.TAX_ADVISOR) {
				return "conselho.fiscal@ventanasresidencial.com.br";
			}
		}

		return null;
    }

}
